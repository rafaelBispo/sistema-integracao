
Imports System.Windows.Forms

''' <summary>
''' Classe auxiliar com m�todos simplificados para exibi��es de mensagens no sistema
''' </summary>
''' <analyst>
''' Rafael Bispo Martins
''' </analyst>
''' <date>15/11/2016</date>
''' <remarks></remarks>
Public Class MessageHelper

#Region "Vari�veis"


#End Region

#Region "M�todos"

    Public Sub ShowErrorMessage(ByVal message As String, ByVal titulo As String)

        MessageBox.Show(message, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error)

    End Sub

    Public Sub ShowErrorMessage(ByVal exception As Exception)

        ShowErrorMessage(exception.Message, My.Settings.ErrorTitle)

    End Sub

    Public Sub ShowErrorMessage(ByVal mensagem As String)

        ShowErrorMessage(mensagem, My.Settings.ErrorTitle)

    End Sub

    Public Function ShowErrorInfoMessage(ByVal message As String, ByVal titulo As String) As Windows.Forms.DialogResult

        Return MessageBox.Show(message, titulo, MessageBoxButtons.OK, MessageBoxIcon.Warning)

    End Function

    Public Function ShowConfirmMessage(ByVal message As String, ByVal titulo As String) As Windows.Forms.DialogResult
        Return MessageBox.Show(message, titulo, MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
    End Function

    Public Sub ShowAlertMessage(ByVal menssagem As String, ByVal titulo As String)

        MessageBox.Show(menssagem, titulo, MessageBoxButtons.OK, MessageBoxIcon.Warning)

    End Sub

    Public Sub ShowInformationMessage(ByVal menssagem As String, ByVal titulo As String)

        MessageBox.Show(menssagem, titulo, MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub

    Public Function ShowConfirmYesNoMessage(ByVal message As String, ByVal titulo As String) As Windows.Forms.DialogResult
        Return MessageBox.Show(message, titulo, MessageBoxButtons.YesNo, MessageBoxIcon.Information)
    End Function


#End Region

End Class
