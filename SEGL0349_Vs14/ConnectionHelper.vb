﻿Imports Alianca.Seguranca.BancoDados
Imports System.Configuration
Imports system.Data.SqlClient
Imports System.Data.OleDb
Imports Alianca.Sistema.Funcoes.TrataParametros

''' <summary>
''' Classe auxiliar para simplificar os acessos a base de dados utilizando os métodos de 
''' segurança contidos na namespace "Alianca.Seguranca.BancoDados"
''' </summary>
''' <flow>3273725</flow>
''' <analyst>
''' Rafael Bispo Martins
''' </analyst>
''' <date>15/11/2016</date>
''' <remarks></remarks>
Public Class ConnectionHelper
    Implements IDisposable


    Public Overloads Sub Dispose() Implements IDisposable.Dispose
        GC.SuppressFinalize(Me)
    End Sub

    Private transaction As SqlTransaction = Nothing
    Private iTransactionNumber As Integer = -1

    Public Sub New()
        Call ConfigurarConexao()
    End Sub

    Public Sub New(ByVal assemblyinfo As System.Reflection.Assembly)
        Call ConfigurarConexao(assemblyinfo)
    End Sub
    '
    '

    ''' <summary>
    ''' Usuário logado no sistema
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public ReadOnly Property GetUsuario() As String
        Get
            Try
                Return Alianca.Sistema.Funcoes.TrataParametros.NomeUsuario
            Catch ex As Exception
                Return ""
            End Try
        End Get

    End Property


    Public Shared ReadOnly Property GetUsuarioCPF() As String
        Get
            Try
                Return Alianca.Sistema.Funcoes.TrataParametros.gsCPF
            Catch ex As Exception
                Return ""
            End Try
        End Get

    End Property

    Public Function RetornaMenssage() As String

        Dim Ambiente_id As String

        If Not Trata_Parametros(Microsoft.VisualBasic.Command, System.Reflection.Assembly.GetExecutingAssembly) Then
            Ambiente_id = GetEnvironment()
        Else
            Ambiente_id = glAmbiente_id.ToString()
        End If

        Return Ambiente_id

    End Function

    Private Sub ConfigurarConexao()

        Try

            If Not Trata_Parametros(Microsoft.VisualBasic.Command, System.Reflection.Assembly.GetExecutingAssembly) Then
                If Not cCon.configurado Then
                    cCon.ConfiguraConexao(Reflection.Assembly.GetExecutingAssembly(), GetEnvironment())
                Else
                    cCon.ReConfiguraConexao(Reflection.Assembly.GetExecutingAssembly(), GetEnvironment())
                End If
            Else
                If Not cCon.configurado Then
                    cCon.ConfiguraConexao(Reflection.Assembly.GetExecutingAssembly(), glAmbiente_id)
                Else
                    cCon.ReConfiguraConexao(Reflection.Assembly.GetExecutingAssembly(), glAmbiente_id)
                End If
            End If



            cCon.ConnectionTimeout = cCon.TimeTimeout.Infinito

            cCon.BancodeDados = "DIGITAL"

            ' Trata a mensagem de erro para que apareça com mais informações na Message Box de erro
        Catch ex As Exception

            Dim errMessage As String = String.Empty
            'Dim tempException As Exception = ex

            'While (Not tempException Is Nothing)
            '    errMessage += tempException.Message + Environment.NewLine + tempException.StackTrace + Environment.NewLine
            '    tempException = tempException.InnerException
            'End While

            'Throw New Exception(errMessage)
            'Throw New Exception("Erro ao configurar conexão")

        End Try
    End Sub

    Private Sub ConfigurarConexao(ByVal assemblyinfo As System.Reflection.Assembly)

        Try

            If Not Trata_Parametros(Microsoft.VisualBasic.Command, System.Reflection.Assembly.GetExecutingAssembly) Then
                If Not cCon.configurado Then
                    cCon.ConfiguraConexao(Reflection.Assembly.GetExecutingAssembly(), GetEnvironment())
                Else
                    cCon.ReConfiguraConexao(Reflection.Assembly.GetExecutingAssembly(), GetEnvironment())
                End If
            Else
                If Not cCon.configurado Then
                    cCon.ConfiguraConexao(Reflection.Assembly.GetExecutingAssembly(), glAmbiente_id)
                Else
                    cCon.ReConfiguraConexao(Reflection.Assembly.GetExecutingAssembly(), glAmbiente_id)
                End If
            End If




            cCon.ConnectionTimeout = cCon.TimeTimeout.Infinito

            ' cCon.BancodeDados = "DIGITAL"

            ' Trata a mensagem de erro para que apareça com mais informações na Message Box de erro
        Catch ex As Exception

            Dim errMessage As String = String.Empty

            Throw New Exception("Erro ao configurar conexão")
        End Try
    End Sub

    Public Sub BeginTransaction()

        Try

            iTransactionNumber = cCon.BeginTransaction()

        Catch ex As Exception

            Dim errMessage As String = String.Empty
            Dim tempException As Exception = ex

            While (Not tempException Is Nothing)
                errMessage += tempException.Message + Environment.NewLine + tempException.StackTrace + Environment.NewLine
                tempException = tempException.InnerException
            End While

            Throw New Exception(errMessage)

        End Try
    End Sub
 
    Public Sub RollBackTransaction()

        Try
            cCon.RollBackTransaction(iTransactionNumber)

        Catch ex As Exception

            Dim errMessage As String = String.Empty
            Dim tempException As Exception = ex

            While (Not tempException Is Nothing)
                errMessage += tempException.Message + Environment.NewLine + tempException.StackTrace + Environment.NewLine
                tempException = tempException.InnerException
            End While

            Throw New Exception(errMessage)

        End Try
    End Sub
   
    Public Sub CommitTransaction()

        Try
            cCon.CommitTransaction(iTransactionNumber)

        Catch ex As Exception

            Dim errMessage As String = String.Empty
            Dim tempException As Exception = ex

            While (Not tempException Is Nothing)
                errMessage += tempException.Message + Environment.NewLine + tempException.StackTrace + Environment.NewLine
                tempException = tempException.InnerException
            End While

            Throw New Exception(errMessage)

        End Try
    End Sub
   
    Public Function ExecuteSQL(ByVal commandType As CommandType, ByVal SQL As String) As DataSet

        Return ExecuteSQL(commandType, SQL, Nothing)

    End Function

    Public Function ExecuteSQL(ByVal commandType As CommandType, ByVal SQL As String, ByVal param() As SqlParameter) As DataSet

        Try
            If Not param Is Nothing Then
                Return cCon.ExecuteDataset(commandType, SQL, param)
            Else
                Return cCon.ExecuteDataset(commandType, SQL)
            End If
        Catch ex As Exception
            'Throw New Exception("Problema ao executar o comando no banco de dados.", ex)
            Throw New Exception("Problema ao executar o comando no banco de dados. " & vbCrLf & vbCrLf & SQL & vbCrLf & vbCrLf & ex.InnerException.ToString(), ex)
        End Try

    End Function

    Public Function ExecuteSQLTransaction(ByVal commandType As CommandType, ByVal SQL As String, ByVal param() As SqlParameter) As DataSet

        Try
            If Not param Is Nothing Then
                Return cCon.ExecuteDataset(iTransactionNumber, commandType, SQL, param)
            Else
                Return cCon.ExecuteDataset(iTransactionNumber, commandType, SQL)
            End If
        Catch ex As Exception
            'Throw New Exception("Problema ao executar o comando no banco de dados.", ex)
            Throw New Exception("Problema ao executar o comando no banco de dados. " & vbCrLf & vbCrLf & SQL & vbCrLf & vbCrLf & ex.InnerException.ToString(), ex)
        End Try

    End Function

    Public Function ExecuteSQLTransaction(ByVal commandType As CommandType, ByVal SQL As String) As DataSet

        Try

            Return cCon.ExecuteDataset(iTransactionNumber, commandType, SQL, Nothing)

        Catch ex As Exception
            'Throw New Exception("Problema ao executar o comando no banco de dados.", ex)
            Throw New Exception("Problema ao executar o comando no banco de dados. " & vbCrLf & vbCrLf & SQL & vbCrLf & vbCrLf & ex.InnerException.ToString(), ex)
        End Try

    End Function

    Public Function ExecuteScalarSQL(ByVal commandType As CommandType, ByVal SQL As String) As Object

        Return ExecuteScalarSQL(commandType, SQL, Nothing)

    End Function
   
    Public Function ExecuteScalarSQL(ByVal commandType As CommandType, ByVal SQL As String, ByVal param() As SqlParameter) As Object

        Try
            If Not param Is Nothing Then
                Return cCon.ExecuteScalar(commandType, SQL, param)
            Else
                Return cCon.ExecuteScalar(commandType, SQL)
            End If
        Catch ex As Exception
            'Throw New Exception("Problema ao executar o comando no banco de dados.", ex)
            Throw New Exception("Problema ao executar o comando no banco de dados. " & vbCrLf & vbCrLf & SQL & vbCrLf & vbCrLf & ex.InnerException.ToString(), ex)
        End Try

    End Function

    Public Function ExecuteScalarSQLTansaction(ByVal commandType As CommandType, ByVal SQL As String, ByVal param() As SqlParameter) As Object

        Try
            If Not param Is Nothing Then
                Return cCon.ExecuteScalar(iTransactionNumber, commandType, SQL, param)
            Else
                Return cCon.ExecuteScalar(iTransactionNumber, commandType, SQL)
            End If
        Catch ex As Exception
            'Throw New Exception("Problema ao executar o comando no banco de dados.", ex)
            Throw New Exception("Problema ao executar o comando no banco de dados. " & vbCrLf & vbCrLf & SQL & vbCrLf & vbCrLf & ex.InnerException.ToString(), ex)
        End Try

    End Function
  
    Public Function ExecuteScalarSQLTansaction(ByVal commandType As CommandType, ByVal SQL As String) As Object

        Try
            Return cCon.ExecuteScalar(iTransactionNumber, commandType, SQL)
        Catch ex As Exception
            'Throw New Exception("Problema ao executar o comando no banco de dados.", ex)
            Throw New Exception("Problema ao executar o comando no banco de dados. " & vbCrLf & vbCrLf & SQL & vbCrLf & vbCrLf & ex.InnerException.ToString(), ex)
        End Try

    End Function

    Public Shared Function GetEnvironment() As Alianca.Seguranca.BancoDados.cCon.Ambientes

        Dim returnValue As Alianca.Seguranca.BancoDados.cCon.Ambientes = Alianca.Seguranca.BancoDados.cCon.Ambientes.Produção

        Try


            Select Case My.Settings.Enviroment

                Case "PROD" : returnValue = Alianca.Seguranca.BancoDados.cCon.Ambientes.Produção
                Case Else : returnValue = Alianca.Seguranca.BancoDados.cCon.Ambientes.Produção
            End Select

        Catch ex As Exception
            returnValue = Alianca.Seguranca.BancoDados.cCon.Ambientes.Produção
        End Try

        Return returnValue

    End Function

    ''' <summary>
    ''' Retorna o id do ambiente atual em forma de string
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetEnvironmentString() As String
        Return CType(GetEnvironment(), Integer).ToString()
    End Function


End Class
