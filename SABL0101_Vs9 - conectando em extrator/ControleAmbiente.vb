Imports Alianca.Seguranca.BancoDados
Namespace Alianca
    Namespace Seguranca
        Namespace Web
            Public Class ControleAmbiente
                Private _Usuario_ID As Integer
                Private _AssemblInfo As System.Reflection.Assembly
                Private _ambiente As cCon.Ambientes
                Private _bancodados As String
                Private _post As String
                Private Shared _URL As String
                Private Shared _ambiente_id As Boolean
                Public Function ObterAmbiente(ByVal URL As String) As cCon.Ambientes
                    Dim v_sUrl As String = URL.ToUpper
                    Dim v_sAmbiente As String
                    v_sUrl = v_sUrl.Replace("HTTP://", "")
                    v_sUrl = v_sUrl.Replace("HTTPS://", "")
                    v_sAmbiente = Left(v_sUrl, v_sUrl.IndexOf("/"))
                    configura_conexao()
                    Dim ds As DataSet
                    ds = cCon.ExecuteDataset(CommandType.Text, "controle_sistema_db..ambiente_web_sps '" & v_sAmbiente & "'")
                    reload_conexao()
                    _ambiente = CType(ds.Tables(0).Rows(0).Item(0), cCon.Ambientes)
                    _post = CType(ds.Tables(0).Rows(0).Item("post"), String)


                    Return CType(_ambiente, cCon.Ambientes)
                End Function

                Private Sub configura_conexao()
                    If cCon.configurado Then
                        _AssemblInfo = cCon.AssemblyInfo
                        _ambiente = cCon.Ambiente
                        _bancodados = cCon.BancodeDados
                        cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cCon.Ambientes.Produ��o)
                    Else
                        cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cCon.Ambientes.Produ��o)
                    End If
                End Sub

                Private Sub reload_conexao()
                    If Not _AssemblInfo Is Nothing Then
                        cCon.Reset(_AssemblInfo, _ambiente)
                        cCon.BancodeDados = _bancodados
                    Else
                        cCon.Reset()
                    End If
                End Sub
                Public ReadOnly Property Ambiente() As cCon.Ambientes
                    Get
                        Ambiente = _ambiente
                    End Get
                End Property
                Public ReadOnly Property Post() As String
                    Get
                        Post = _post
                    End Get
                End Property
            End Class
        End Namespace
    End Namespace
End Namespace
