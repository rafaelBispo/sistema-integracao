﻿using SISSocial.RN;
using System;
using System.Windows.Forms;

namespace SISSocial
{
    public partial class FrmCadastroUsuario : Form
    {
        public FrmCadastroUsuario()
        {
            InitializeComponent();
        }

        private void btnGravar_Click(object sender, EventArgs e)
        {
            RNUsuario rn = new RNUsuario();

            try
            {
                if (txtUsuario.Text == "")
                {
                    MessageBox.Show("Por favor preencher o usuario!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                }
                else if (txtSenha.Text == "")
                {
                    MessageBox.Show("Por favor preencher a senha!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                }
                else if (cboTipoAcesso.SelectedIndex == 0)
                {
                    MessageBox.Show("Por favor preencher o tipo de acesso!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                }

                bool valida = rn.CadastraUsuario(txtUsuario.Text, txtSenha.Text, cboTipoAcesso.SelectedIndex);
                if (valida == true)
                {
                    MessageBox.Show("Registro incluido com sucesso!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                }
                else
                {
                    MessageBox.Show("Ocorreu um erro ao cadastrar o usuario!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void FrmCadastroUsuario_Load(object sender, EventArgs e)
        {
            cboTipoAcesso.SelectedIndex = 0;
        }
    }
}
