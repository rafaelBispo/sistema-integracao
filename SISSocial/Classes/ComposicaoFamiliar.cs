﻿using System;

namespace SISSocial.Classes
{
    public class ComposicaoFamiliar
    {
        private string codigo;
        private string nome;
        private int sexo;
        private int estadoCivil;
        private string cpf;
        private string rg;
        private DateTime dtNascimento;
        private string telefone;
        private int idade;
        private string escolaridade;
        private int carteiraAssinada;
        private string profissao;
        private int possuiDeficiencia;
        private string cartaoSUS;
        private string certidaoNascimento;
        private string filiacao;
        private string laudo;
        private int problemaSaude;
        private string qualProblemaSaude;
        private int alergiaAlimento;
        private string qualAlergiaAlimento;
        private int usoMedicamento;
        private string qualUsoMedicamento;
        private int atividadeAlemSCFV;
        private string qualAtividadeAlemSCFV;
        private int restricaoParticiparAtividade;
        private string qualRestricaoParticiparAtividade;
        private int possuiFonteRenda;
        private string qualPossuiFonteRenda;
        private int cadastroCras;
        private string qualCadastroCras;

        public string Codigo { get => codigo; set => codigo = value; }
        public string Nome { get => nome; set => nome = value; }
        public int Sexo { get => sexo; set => sexo = (int)value; }
        public int EstadoCivil { get => estadoCivil; set => estadoCivil = (int)value; }
        public string Cpf { get => cpf; set => cpf = value; }
        public string Rg { get => rg; set => rg = value; }
        public DateTime DtNascimento { get => dtNascimento; set => dtNascimento = (DateTime)value; }
        public string Telefone { get => telefone; set => telefone = value; }
        public int? Idade { get => idade; set => idade = (int)value; }
        public string? Escolaridade { get => escolaridade; set => escolaridade = (string)value; }
        public int? CarteiraAssinada { get => carteiraAssinada; set => carteiraAssinada = (int)value; }
        public string Profissao { get => profissao; set => profissao = value; }
        public int? PossuiDeficiencia { get => possuiDeficiencia; set => possuiDeficiencia = (int)value; }
        public string CartaoSUS { get => cartaoSUS; set => cartaoSUS = value; }
        public string CertidaoNascimento { get => certidaoNascimento; set => certidaoNascimento = value; }
        public string Filiacao { get => filiacao; set => filiacao = value; }
        public string Laudo { get => laudo; set => laudo = value; }
        public int ProblemaSaude { get => problemaSaude; set => problemaSaude = value; }
        public string QualProblemaSaude { get => qualProblemaSaude; set => qualProblemaSaude = value; }
        public int AlergiaAlimento { get => alergiaAlimento; set => alergiaAlimento = value; }
        public string QualAlergiaAlimento { get => qualAlergiaAlimento; set => qualAlergiaAlimento = value; }
        public int UsoMedicamento { get => usoMedicamento; set => usoMedicamento = value; }
        public string QualUsoMedicamento { get => qualUsoMedicamento; set => qualUsoMedicamento = value; }
        public int AtividadeAlemSCFV { get => atividadeAlemSCFV; set => atividadeAlemSCFV = value; }
        public string QualAtividadeAlemSCFV { get => qualAtividadeAlemSCFV; set => qualAtividadeAlemSCFV = value; }
        public int RestricaoParticiparAtividade { get => restricaoParticiparAtividade; set => restricaoParticiparAtividade = value; }
        public string QualRestricaoParticiparAtividade { get => qualRestricaoParticiparAtividade; set => qualRestricaoParticiparAtividade = value; }
        public int PossuiFonteRenda { get => possuiFonteRenda; set => possuiFonteRenda = value; }
        public string QualPossuiFonteRenda { get => qualPossuiFonteRenda; set => qualRestricaoParticiparAtividade = value; }
        public int CadastroCras { get => cadastroCras; set => cadastroCras = value; }
        public string QualCadastroCras { get => qualCadastroCras; set => qualCadastroCras = value; }
    }
}
