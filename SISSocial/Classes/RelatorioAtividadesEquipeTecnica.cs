﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISSocial.Classes
{
    internal class RelatorioAtividadesEquipeTecnica
    {
        private int id;
        private string nomeServico;
        private string nome;
        private string funcao;
        private string mesRef;
        private string dataEntrega;
        private string descricao;
        private string publicoAlvo;
        private string pessoasAtendidas;
        private string periodicidade;
        private string recursosHumanos;
        private string abrangencia;
        private string resultados;
        private string recursosFinanceiros;
        private string destacar;
        private string parcerias;

        public int Id { get => id; set => id = value; }
        public string NomeServico { get => nomeServico; set => nomeServico = value; }
        public string Nome { get => nome; set => nome = value; }
        public string Funcao { get => funcao; set => funcao = value; }
        public string MesRef { get => mesRef; set => mesRef = value; }
        public string DataEntrega { get => dataEntrega; set => dataEntrega = value; }
        public string Descricao { get => descricao; set => descricao = value; }
        public string PublicoAlvo { get => publicoAlvo; set => publicoAlvo = value; }
        public string PessoasAtendidas { get => pessoasAtendidas; set => pessoasAtendidas = value; }
        public string Periodicidade { get => periodicidade; set => periodicidade = value; }
        public string RecursosHumanos { get => recursosHumanos; set => recursosHumanos = value; }
        public string Abrangencia { get => abrangencia; set => abrangencia = value; }
        public string Resultados { get => resultados; set => resultados = value; }
        public string RecursosFinanceiros { get => recursosFinanceiros; set => recursosFinanceiros = value; }
        public string Destacar { get => destacar; set => destacar = value; }
        public string Parcerias { get => parcerias; set => parcerias = value; }

    }
}