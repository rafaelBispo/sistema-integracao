﻿using System;

namespace SISSocial.Classes
{
    public class Assistido
    {
        private string codigo;
        private int orientacao;
        private string nome;
        private int sexo;
        private int estadoCivil;
        private string cpf;
        private string rg;
        private string nis;
        private DateTime dtNascimento;
        private string endereco;
        private string telefone;
        private string mae;
        private string pai;
        private int escolaridade;
        private int problemaSaude;
        private string qualProblemaSaude;
        private int possuiDeficiencia;
        private int idade;
        private int filhos;
        private int qtdFilhos;
        private int recebePensao;
        private int maeCriaFilhosSemApoioPais;
        private int maePresenteFamilia;
        private int idadeMae;
        private int recebeBolsaFamilia;
        private decimal valorBolsaFamilia;
        private int recebeLoas;
        private string valorLoas;
        private int casaE;
        private decimal valorAluguel;
        private int qtdPessoasCasa;
        private int carteiraAssinada;
        private string profissao;
        private int auxilioPrevidenciario;
        private int cadastroCras;
        private DateTime dtCadastro;
        private Int32 bairro;
        private string responsavel;

        public string Codigo { get => codigo; set => codigo = value; }
        public string Nome { get => nome; set => nome = value; }
        public int? Sexo { get => sexo; set => sexo = (int)value; }
        public int? EstadoCivil { get => estadoCivil; set => estadoCivil = (int)value; }
        public string Cpf { get => cpf; set => cpf = value; }
        public string Rg { get => rg; set => rg = value; }
        public string Nis { get => nis; set => nis = value; }
        public DateTime DtNascimento { get => dtNascimento; set => dtNascimento = (DateTime)value; }
        public string Endereco { get => endereco; set => endereco = value; }
        public string Telefone { get => telefone; set => telefone = value; }
        public string Mae { get => mae; set => mae = value; }
        public string Pai { get => pai; set => pai = value; }
        public int Escolaridade { get => escolaridade; set => escolaridade = (int)value; }
        public int ProblemaSaude { get => problemaSaude; set => problemaSaude = (int)value; }
        public string QualProblemaSaude { get => qualProblemaSaude; set => qualProblemaSaude = value; }
        public int PossuiDeficiencia { get => possuiDeficiencia; set => possuiDeficiencia = (int)value; }
        public int Idade { get => idade; set => idade = (int)value; }
        public int Filhos { get => filhos; set => filhos = (int)value; }
        public int QtdFilhos { get => qtdFilhos; set => qtdFilhos = (int)value; }
        public int RecebePensao { get => recebePensao; set => recebePensao = (int)value; }
        public int MaeCriaFilhosSemApoioPais { get => maeCriaFilhosSemApoioPais; set => maeCriaFilhosSemApoioPais = (int)value; }
        public int MaePresenteFamilia { get => maePresenteFamilia; set => maePresenteFamilia = (int)value; }
        public int IdadeMae { get => idadeMae; set => idadeMae = (int)value; }
        public int RecebeBolsaFamilia { get => recebeBolsaFamilia; set => recebeBolsaFamilia = (int)value; }
        public decimal? ValorBolsaFamilia { get => valorBolsaFamilia; set => valorBolsaFamilia = (int)value; }
        public int RecebeLoas { get => recebeLoas; set => recebeLoas = (int)value; }
        public string ValorLoas { get => valorLoas; set => valorLoas = value; }
        public int? CasaE { get => casaE; set => casaE = (int)value; }
        public decimal? ValorAluguel { get => valorAluguel; set => valorAluguel = (int)value; }
        public int? QtdPessoasCasa { get => qtdPessoasCasa; set => qtdPessoasCasa = (int)value; }
        public int? CarteiraAssinada { get => carteiraAssinada; set => carteiraAssinada = (int)value; }
        public string Profissao { get => profissao; set => profissao = value; }
        public int? AuxilioPrevidenciario { get => auxilioPrevidenciario; set => auxilioPrevidenciario = (int)value; }
        public int? CadastroCras { get => cadastroCras; set => cadastroCras = (int)value; }
        public int? Orientacao { get => orientacao; set => orientacao = (int)value; }
        public DateTime? DtCadastro { get => dtCadastro; set => dtCadastro = (DateTime)value; }
        public int Bairro { get => bairro; set => bairro = value; }
        public string Responsavel { get => responsavel; set => responsavel = value; }
    }
}
