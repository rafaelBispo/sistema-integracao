﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SISSocial.Classes
{
    internal class RelatorioIntervencoesDiarias
    {
        private int id;
        private string tecnicoResponsavel;
        private string data;
        private string paraEncaminhamentos1;
        private string paraEncaminhamentos2;
        private string paraEncaminhamentos3;
        private string paraEncaminhamentos4;
        private string qtdEncaminhamentos1;
        private string qtdEncaminhamentos2;
        private string qtdEncaminhamentos3;
        private string qtdEncaminhamentos4;
        private string paraRelatorios1;
        private string paraRelatorios2;
        private string paraRelatorios3;
        private string paraRelatorios4;
        private string qtdRelatorios1;
        private string qtdRelatorios2;
        private string qtdRelatorios3;
        private string qtdRelatorios4;
        private string paraVisitas1;
        private string paraVisitas2;
        private string paraVisitas3;
        private string paraVisitas4;
        private string qtdVisitas1;
        private string qtdVisitas2;
        private string qtdVisitas3;
        private string qtdVisitas4;
        private string prontuariosNovo1;
        private string prontuariosNovo2;
        private string prontuariosNovo3;
        private string prontuariosNovo4;
        private string prontuariosNovo5;
        private string prontuariosNovo6;
        private string prontuariosNovo7;
        private string prontuariosNovo8;
        private string prontuariosNovo9;
        private string prontuariosNovo10;
        private string numProntuarioNovos1;
        private string numProntuarioNovos2;
        private string numProntuarioNovos3;
        private string numProntuarioNovos4;
        private string numProntuarioNovos5;
        private string numProntuarioNovos6;
        private string numProntuarioNovos7;
        private string numProntuarioNovos8;
        private string numProntuarioNovos9;
        private string numProntuarioNovos10;
        private string reincidentesNome1;
        private string reincidentesNome2;
        private string reincidentesNome3;
        private string reincidentesNome4;
        private string reincidentesNome5;
        private string reincidentesNome6;
        private string reincidentesNome7;
        private string reincidentesNome8;
        private string reincidentesNome9;
        private string reincidentesNome10;
        private string reincidentesNum1;
        private string reincidentesNum2;
        private string reincidentesNum3;
        private string reincidentesNum4;
        private string reincidentesNum5;
        private string reincidentesNum6;
        private string reincidentesNum7;
        private string reincidentesNum8;
        private string reincidentesNum9;
        private string reincidentesNum10;
        private string desligadosNome1;
        private string desligadosNome2;
        private string desligadosNome3;
        private string desligadosNome4;
        private string desligadosNome5;
        private string desligadosNome6;
        private string desligadosNome7;
        private string desligadosNome8;
        private string desligadosNome9;
        private string desligadosNome10;
        private string desligadosNum1;
        private string desligadosNum2;
        private string desligadosNum3;
        private string desligadosNum4;
        private string desligadosNum5;
        private string desligadosNum6;
        private string desligadosNum7;
        private string desligadosNum8;
        private string desligadosNum9;
        private string desligadosNum10;

        public int Id { get => id; set => id = value; }
        public string TecnicoResponsavel { get => tecnicoResponsavel; set => tecnicoResponsavel = value; }
        public string Data { get => data; set => data = value; }
        public string ParaEncaminhamentos1 { get => paraEncaminhamentos1; set => paraEncaminhamentos1 = value; }
        public string ParaEncaminhamentos2 { get => paraEncaminhamentos2; set => paraEncaminhamentos2 = value; }
        public string ParaEncaminhamentos3 { get => paraEncaminhamentos3; set => paraEncaminhamentos3 = value; }
        public string ParaEncaminhamentos4 { get => paraEncaminhamentos4; set => paraEncaminhamentos4 = value; }
        public string QtdEncaminhamentos1 { get => qtdEncaminhamentos1; set => qtdEncaminhamentos1 = value; }
        public string QtdEncaminhamentos2 { get => qtdEncaminhamentos2; set => qtdEncaminhamentos2 = value; }
        public string QtdEncaminhamentos3 { get => qtdEncaminhamentos3; set => qtdEncaminhamentos3 = value; }
        public string QtdEncaminhamentos4 { get => qtdEncaminhamentos4; set => qtdEncaminhamentos4 = value; }
        public string ParaRelatorios1 { get => paraRelatorios1; set => paraRelatorios1 = value; }
        public string ParaRelatorios2 { get => paraRelatorios2; set => paraRelatorios2 = value; }
        public string ParaRelatorios3 { get => paraRelatorios3; set => paraRelatorios3 = value; }
        public string ParaRelatorios4 { get => paraRelatorios4; set => paraRelatorios4 = value; }
        public string QtdRelatorios1 { get => qtdRelatorios1; set => qtdRelatorios1 = value; }
        public string QtdRelatorios2 { get => qtdRelatorios2; set => qtdRelatorios2 = value; }
        public string QtdRelatorios3 { get => qtdRelatorios3; set => qtdRelatorios3 = value; }
        public string QtdRelatorios4 { get => qtdRelatorios4; set => qtdRelatorios4 = value; }
        public string ParaVisitas1 { get => paraVisitas1; set => paraVisitas1 = value; }
        public string ParaVisitas2 { get => paraVisitas2; set => paraVisitas2 = value; }
        public string ParaVisitas3 { get => paraVisitas3; set => paraVisitas3 = value; }
        public string ParaVisitas4 { get => paraVisitas4; set => paraVisitas4 = value; }
        public string QtdVisitas1 { get => qtdVisitas1; set => qtdVisitas1 = value; }
        public string QtdVisitas2 { get => qtdVisitas2; set => qtdVisitas2 = value; }
        public string QtdVisitas3 { get => qtdVisitas3; set => qtdVisitas3 = value; }
        public string QtdVisitas4 { get => qtdVisitas4; set => qtdVisitas4 = value; }
        public string ProntuariosNovo1 { get => prontuariosNovo1; set => prontuariosNovo1 = value; }
        public string ProntuariosNovo2 { get => prontuariosNovo2; set => prontuariosNovo2 = value; }
        public string ProntuariosNovo3 { get => prontuariosNovo3; set => prontuariosNovo3 = value; }
        public string ProntuariosNovo4 { get => prontuariosNovo4; set => prontuariosNovo4 = value; }
        public string ProntuariosNovo5 { get => prontuariosNovo5; set => prontuariosNovo5 = value; }
        public string ProntuariosNovo6 { get => prontuariosNovo6; set => prontuariosNovo6 = value; }
        public string ProntuariosNovo7 { get => prontuariosNovo7; set => prontuariosNovo7 = value; }
        public string ProntuariosNovo8 { get => prontuariosNovo8; set => prontuariosNovo8 = value; }
        public string ProntuariosNovo9 { get => prontuariosNovo9; set => prontuariosNovo9 = value; }
        public string ProntuariosNovo10 { get => prontuariosNovo10; set => prontuariosNovo10 = value; }
        public string NumProntuarioNovos1 { get => numProntuarioNovos1; set => numProntuarioNovos1 = value; }
        public string NumProntuarioNovos2 { get => numProntuarioNovos2; set => numProntuarioNovos2 = value; }
        public string NumProntuarioNovos3 { get => numProntuarioNovos3; set => numProntuarioNovos3 = value; }
        public string NumProntuarioNovos4 { get => numProntuarioNovos4; set => numProntuarioNovos4 = value; }
        public string NumProntuarioNovos5 { get => numProntuarioNovos5; set => numProntuarioNovos5 = value; }
        public string NumProntuarioNovos6 { get => numProntuarioNovos6; set => numProntuarioNovos6 = value; }
        public string NumProntuarioNovos7 { get => numProntuarioNovos7; set => numProntuarioNovos7 = value; }
        public string NumProntuarioNovos8 { get => numProntuarioNovos8; set => numProntuarioNovos8 = value; }
        public string NumProntuarioNovos9 { get => numProntuarioNovos9; set => numProntuarioNovos9 = value; }
        public string NumProntuarioNovos10 { get => numProntuarioNovos10; set => numProntuarioNovos10 = value; }
        public string ReincidentesNome1 { get => reincidentesNome1; set => reincidentesNome1 = value; }
        public string ReincidentesNome2 { get => reincidentesNome2; set => reincidentesNome2 = value; }
        public string ReincidentesNome3 { get => reincidentesNome3; set => reincidentesNome3 = value; }
        public string ReincidentesNome4 { get => reincidentesNome4; set => reincidentesNome4 = value; }
        public string ReincidentesNome5 { get => reincidentesNome5; set => reincidentesNome5 = value; }
        public string ReincidentesNome6 { get => reincidentesNome6; set => reincidentesNome6 = value; }
        public string ReincidentesNome7 { get => reincidentesNome7; set => reincidentesNome7 = value; }
        public string ReincidentesNome8 { get => reincidentesNome8; set => reincidentesNome8 = value; }
        public string ReincidentesNome9 { get => reincidentesNome9; set => reincidentesNome9 = value; }
        public string ReincidentesNome10 { get => reincidentesNome10; set => reincidentesNome10 = value; }
        public string ReincidentesNum1 { get => reincidentesNum1; set => reincidentesNum1 = value; }
        public string ReincidentesNum2 { get => reincidentesNum2; set => reincidentesNum2 = value; }
        public string ReincidentesNum3 { get => reincidentesNum3; set => reincidentesNum3 = value; }
        public string ReincidentesNum4 { get => reincidentesNum4; set => reincidentesNum4 = value; }
        public string ReincidentesNum5 { get => reincidentesNum5; set => reincidentesNum5 = value; }
        public string ReincidentesNum6 { get => reincidentesNum6; set => reincidentesNum6 = value; }
        public string ReincidentesNum7 { get => reincidentesNum7; set => reincidentesNum7 = value; }
        public string ReincidentesNum8 { get => reincidentesNum8; set => reincidentesNum8 = value; }
        public string ReincidentesNum9 { get => reincidentesNum9; set => reincidentesNum9 = value; }
        public string ReincidentesNum10 { get => reincidentesNum10; set => reincidentesNum10 = value; }
        public string DesligadosNome1 { get => desligadosNome1; set => desligadosNome1 = value; }
        public string DesligadosNome2 { get => desligadosNome2; set => desligadosNome2 = value; }
        public string DesligadosNome3 { get => desligadosNome3; set => desligadosNome3 = value; }
        public string DesligadosNome4 { get => desligadosNome4; set => desligadosNome4 = value; }
        public string DesligadosNome5 { get => desligadosNome5; set => desligadosNome5 = value; }
        public string DesligadosNome6 { get => desligadosNome6; set => desligadosNome6 = value; }
        public string DesligadosNome7 { get => desligadosNome7; set => desligadosNome7 = value; }
        public string DesligadosNome8 { get => desligadosNome8; set => desligadosNome8 = value; }
        public string DesligadosNome9 { get => desligadosNome9; set => desligadosNome9 = value; }
        public string DesligadosNome10 { get => desligadosNome10; set => desligadosNome10 = value; }
        public string DesligadosNum1 { get => desligadosNum1; set => desligadosNum1 = value; }
        public string DesligadosNum2 { get => desligadosNum2; set => desligadosNum2 = value; }
        public string DesligadosNum3 { get => desligadosNum3; set => desligadosNum3 = value; }
        public string DesligadosNum4 { get => desligadosNum4; set => desligadosNum4 = value; }
        public string DesligadosNum5 { get => desligadosNum5; set => desligadosNum5 = value; }
        public string DesligadosNum6 { get => desligadosNum6; set => desligadosNum6 = value; }
        public string DesligadosNum7 { get => desligadosNum7; set => desligadosNum7 = value; }
        public string DesligadosNum8 { get => desligadosNum8; set => desligadosNum8 = value; }
        public string DesligadosNum9 { get => desligadosNum9; set => desligadosNum9 = value; }
        public string DesligadosNum10 { get => desligadosNum10; set => desligadosNum10 = value; }
    }
}
