﻿using SISSocial.Classes;
using SISSocial.RN;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SISSocial
{
    public partial class frmRelatorioIntervençõesDiarias : Form
    {
        List<RelatorioIntervencoesDiarias> ListaIntervencoes = new List<RelatorioIntervencoesDiarias>();
        bool relatoriosCarregados = false;
        bool updateRelatorio = false;
        public frmRelatorioIntervençõesDiarias()
        {
            InitializeComponent();
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            limparTextBoxes(this.Controls);
        }

        private void limparTextBoxes(Control.ControlCollection controles)
        {
            //Faz um laço para todos os controles passados no parâmetro
            foreach (Control ctrl in controles)
            {
                //Se o contorle for um TextBox...
                if (ctrl is TextBox)
                {
                    ((TextBox)(ctrl)).Text = String.Empty;
                }
            }
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtTecnicoResponsavel.Text == string.Empty)
                {
                    lblOut.Text = "Não há informações suficientes para gerar o arquivo!";
                    return;
                }
                string path = System.AppDomain.CurrentDomain.BaseDirectory.ToString();
                path = path.Replace("\\", "/");
                string templatePath = $"{path}Resources Base/Relatório de Intervenções Diárias.docx";
                string outputPath = $"{path}Arquivos saida/Relatório de Intervenções Diárias {txtTecnicoResponsavel.Text}.docx";
                var fields = new Dictionary<string, string>
                {
                    { "{{Responsavel}}", txtTecnicoResponsavel.Text },
                    { "{{Data}}", txtData.Text },

                    { "{{ParaEncaminhamentos1}}", txtParaEncaminhamentos1.Text },
                    { "{{ParaEncaminhamentos2}}", txtParaEncaminhamentos2.Text },
                    { "{{ParaEncaminhamentos3}}", txtParaEncaminhamentos3.Text },
                    { "{{ParaEncaminhamentos4}}", txtParaEncaminhamentos4.Text },
                    { "{{QtdEncaminhamentos1}}", txtQtdEncaminhamentos1.Text },
                    { "{{QtdEncaminhamentos2}}", txtQtdEncaminhamentos2.Text },
                    { "{{QtdEncaminhamentos3}}", txtQtdEncaminhamentos3.Text },
                    { "{{QtdEncaminhamentos4}}", txtQtdEncaminhamentos4.Text },

                    { "{{ParaRelatorios1}}", txtParaRelatorios1.Text },
                    { "{{ParaRelatorios2}}", txtParaRelatorios2.Text },
                    { "{{ParaRelatorios3}}", txtParaRelatorios3.Text },
                    { "{{ParaRelatorios4}}", txtParaRelatorios4.Text },
                    { "{{QtdRelatorios1}}", txtQtdRelatorios1.Text },
                    { "{{QtdRelatorios2}}", txtQtdRelatorios2.Text },
                    { "{{QtdRelatorios3}}", txtQtdRelatorios3.Text },
                    { "{{QtdRelatorios4}}", txtQtdRelatorios4.Text },

                    { "{{ParaVisitas1}}", txtParaVisitas1.Text },
                    { "{{ParaVisitas2}}", txtParaVisitas2.Text },
                    { "{{ParaVisitas3}}", txtParaVisitas3.Text },
                    { "{{ParaVisitas4}}", txtParaVisitas4.Text },
                    { "{{QtdVisitas1}}", txtQtdVisitas1.Text },
                    { "{{QtdVisitas2}}", txtQtdVisitas2.Text },
                    { "{{QtdVisitas3}}", txtQtdVisitas3.Text },
                    { "{{QtdVisitas4}}", txtQtdVisitas4.Text },

                    { "{{ProntuariosNovo1}}", txtProntuariosNovo1.Text },
                    { "{{ProntuariosNovo2}}", txtProntuariosNovo2.Text },
                    { "{{ProntuariosNovo3}}", txtProntuariosNovo3.Text },
                    { "{{ProntuariosNovo4}}", txtProntuariosNovo4.Text },
                    { "{{ProntuariosNovo5}}", txtProntuariosNovo5.Text },
                    { "{{ProntuariosNovo6}}", txtProntuariosNovo6.Text },
                    { "{{ProntuariosNovo7}}", txtProntuariosNovo7.Text },
                    { "{{ProntuariosNovo8}}", txtProntuariosNovo8.Text },
                    { "{{ProntuariosNovo9}}", txtProntuariosNovo9.Text },
                    { "{{ProntuariosNovo10}}", txtProntuariosNovo10.Text },
                    { "{{NumProntuarioNovos1}}", txtNumProntuarioNovos1.Text },
                    { "{{NumProntuarioNovos2}}", txtNumProntuarioNovos2.Text },
                    { "{{NumProntuarioNovos3}}", txtNumProntuarioNovos3.Text },
                    { "{{NumProntuarioNovos4}}", txtNumProntuarioNovos4.Text },
                    { "{{NumProntuarioNovos5}}", txtNumProntuarioNovos5.Text },
                    { "{{NumProntuarioNovos6}}", txtNumProntuarioNovos6.Text },
                    { "{{NumProntuarioNovos7}}", txtNumProntuarioNovos7.Text },
                    { "{{NumProntuarioNovos8}}", txtNumProntuarioNovos8.Text },
                    { "{{NumProntuarioNovos9}}", txtNumProntuarioNovos9.Text },
                    { "{{NumProntuarioNovos10}}", txtNumProntuarioNovos10.Text },

                    { "{{ReincidentesNome1}}", txtReincidentesNome1.Text },
                    { "{{ReincidentesNome2}}", txtReincidentesNome2.Text },
                    { "{{ReincidentesNome3}}", txtReincidentesNome3.Text },
                    { "{{ReincidentesNome4}}", txtReincidentesNome4.Text },
                    { "{{ReincidentesNome5}}", txtReincidentesNome5.Text },
                    { "{{ReincidentesNome6}}", txtReincidentesNome6.Text },
                    { "{{ReincidentesNome7}}", txtReincidentesNome7.Text },
                    { "{{ReincidentesNome8}}", txtReincidentesNome8.Text },
                    { "{{ReincidentesNome9}}", txtReincidentesNome9.Text },
                    { "{{ReincidentesNome10}}", txtReincidentesNome10.Text },
                    { "{{ReincidentesNum1}}", txtReincidentesNum1.Text },
                    { "{{ReincidentesNum2}}", txtReincidentesNum2.Text },
                    { "{{ReincidentesNum3}}", txtReincidentesNum3.Text },
                    { "{{ReincidentesNum4}}", txtReincidentesNum4.Text },
                    { "{{ReincidentesNum5}}", txtReincidentesNum5.Text },
                    { "{{ReincidentesNum6}}", txtReincidentesNum6.Text },
                    { "{{ReincidentesNum7}}", txtReincidentesNum7.Text },
                    { "{{ReincidentesNum8}}", txtReincidentesNum8.Text },
                    { "{{ReincidentesNum9}}", txtReincidentesNum9.Text },
                    { "{{ReincidentesNum10}}", txtReincidentesNum10.Text },

                    { "{{DesligadosNome1}}", txtDesligadosNome1.Text },
                    { "{{DesligadosNome2}}", txtDesligadosNome2.Text },
                    { "{{DesligadosNome3}}", txtDesligadosNome3.Text },
                    { "{{DesligadosNome4}}", txtDesligadosNome4.Text },
                    { "{{DesligadosNome5}}", txtDesligadosNome5.Text },
                    { "{{DesligadosNome6}}", txtDesligadosNome6.Text },
                    { "{{DesligadosNome7}}", txtDesligadosNome7.Text },
                    { "{{DesligadosNome8}}", txtDesligadosNome8.Text },
                    { "{{DesligadosNome9}}", txtDesligadosNome9.Text },
                    { "{{DesligadosNome10}}", txtDesligadosNome10.Text },
                    { "{{DesligadosNum1}}", txtDesligadosNum1.Text },
                    { "{{DesligadosNum2}}", txtDesligadosNum2.Text },
                    { "{{DesligadosNum3}}", txtDesligadosNum3.Text },
                    { "{{DesligadosNum4}}", txtDesligadosNum4.Text },
                    { "{{DesligadosNum5}}", txtDesligadosNum5.Text },
                    { "{{DesligadosNum6}}", txtDesligadosNum6.Text },
                    { "{{DesligadosNum7}}", txtDesligadosNum7.Text },
                    { "{{DesligadosNum8}}", txtDesligadosNum8.Text },
                    { "{{DesligadosNum9}}", txtDesligadosNum9.Text },
                    { "{{DesligadosNum10}}", txtDesligadosNum10.Text },

                };
                lblOut.Visible = true;
                if (GerarWord.PreencherCampos(templatePath, outputPath, fields))
                {
                    lblOut.Text = $"Arquivo gerado com sucesso na pasta {path} Arquivos saida";
                }
                else
                {
                    lblOut.Text = $"Erro ao gerar o arquivo por favor entre em contato com o administrador do sistema";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private void cboRelatorios_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!relatoriosCarregados)
                {
                    return;
                }
                int Id = (int)cboRelatorios.SelectedValue;
                if (Id == 0)
                {
                    return;
                }
                foreach (RelatorioIntervencoesDiarias resultado in ListaIntervencoes)
                {
                    if (resultado.Id == Id)
                    {
                        CarregaTelaRelatorio(resultado);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private void CarregaTelaRelatorio(RelatorioIntervencoesDiarias intervencoes)
        {
            try
            {
                txtTecnicoResponsavel.Text = intervencoes.TecnicoResponsavel;
                txtData.Text = intervencoes.Data;
                txtParaEncaminhamentos1.Text = intervencoes.ParaEncaminhamentos1;
                txtParaEncaminhamentos2.Text = intervencoes.ParaEncaminhamentos2;
                txtParaEncaminhamentos3.Text = intervencoes.ParaEncaminhamentos3;
                txtParaEncaminhamentos4.Text = intervencoes.ParaEncaminhamentos4;
                txtQtdEncaminhamentos1.Text = intervencoes.QtdEncaminhamentos1;
                txtQtdEncaminhamentos2.Text =   intervencoes.QtdEncaminhamentos2;
                txtQtdEncaminhamentos3.Text =   intervencoes.QtdEncaminhamentos3;
                txtQtdEncaminhamentos4.Text =   intervencoes.QtdEncaminhamentos4;
                txtParaRelatorios1.Text = intervencoes.ParaRelatorios1;
                txtParaRelatorios2.Text = intervencoes.ParaRelatorios2;
                txtParaRelatorios3.Text = intervencoes.ParaRelatorios3;
                txtParaRelatorios4.Text = intervencoes.ParaRelatorios4;
                txtQtdRelatorios1.Text = intervencoes.QtdRelatorios1;
                txtQtdRelatorios2.Text = intervencoes.QtdRelatorios2;
                txtQtdRelatorios3.Text = intervencoes.QtdRelatorios3;
                txtQtdRelatorios4.Text = intervencoes.QtdRelatorios4;
                txtParaVisitas1.Text = intervencoes.ParaVisitas1;
                txtParaVisitas2.Text = intervencoes.ParaVisitas2;
                txtParaVisitas3.Text = intervencoes.ParaVisitas3;
                txtParaVisitas4.Text = intervencoes.ParaVisitas4;
                txtQtdVisitas1.Text = intervencoes.QtdVisitas1;
                txtQtdVisitas2.Text = intervencoes.QtdVisitas2;
                txtQtdVisitas3.Text = intervencoes.QtdVisitas3;
                txtQtdVisitas4.Text = intervencoes.QtdVisitas4;
                txtProntuariosNovo1.Text = intervencoes.ProntuariosNovo1;
                txtProntuariosNovo2.Text = intervencoes.ProntuariosNovo2;
                txtProntuariosNovo3.Text = intervencoes.ProntuariosNovo3;
                txtProntuariosNovo4.Text = intervencoes.ProntuariosNovo4;
                txtProntuariosNovo5.Text = intervencoes.ProntuariosNovo5;
                txtProntuariosNovo6.Text = intervencoes.ProntuariosNovo6;
                txtProntuariosNovo7.Text = intervencoes.ProntuariosNovo7;
                txtProntuariosNovo8.Text = intervencoes.ProntuariosNovo8;
                txtProntuariosNovo9.Text = intervencoes.ProntuariosNovo9;
                txtProntuariosNovo10.Text = intervencoes.ProntuariosNovo10;
                txtNumProntuarioNovos1.Text = intervencoes.NumProntuarioNovos1;
                txtNumProntuarioNovos2.Text = intervencoes.NumProntuarioNovos2;
                txtNumProntuarioNovos3.Text = intervencoes.NumProntuarioNovos3;
                txtNumProntuarioNovos4.Text = intervencoes.NumProntuarioNovos4;
                txtNumProntuarioNovos5.Text = intervencoes.NumProntuarioNovos5;
                txtNumProntuarioNovos6.Text = intervencoes.NumProntuarioNovos6;
                txtNumProntuarioNovos7.Text = intervencoes.NumProntuarioNovos7;
                txtNumProntuarioNovos8.Text = intervencoes.NumProntuarioNovos8;
                txtNumProntuarioNovos9.Text = intervencoes.NumProntuarioNovos9;
                txtNumProntuarioNovos10.Text = intervencoes.NumProntuarioNovos10;
                txtReincidentesNome1.Text = intervencoes.ReincidentesNome1;
                txtReincidentesNome2.Text = intervencoes.ReincidentesNome2;
                txtReincidentesNome3.Text = intervencoes.ReincidentesNome3;
                txtReincidentesNome4.Text = intervencoes.ReincidentesNome4;
                txtReincidentesNome5.Text = intervencoes.ReincidentesNome5;
                txtReincidentesNome6.Text = intervencoes.ReincidentesNome6;
                txtReincidentesNome7.Text = intervencoes.ReincidentesNome7;
                txtReincidentesNome8.Text = intervencoes.ReincidentesNome8;
                txtReincidentesNome9.Text = intervencoes.ReincidentesNome9;
                txtReincidentesNome10.Text = intervencoes.ReincidentesNome10;
                txtReincidentesNum1.Text = intervencoes.ReincidentesNum1;
                txtReincidentesNum2.Text = intervencoes.ReincidentesNum2;
                txtReincidentesNum3.Text = intervencoes.ReincidentesNum3;
                txtReincidentesNum4.Text = intervencoes.ReincidentesNum4;
                txtReincidentesNum5.Text = intervencoes.ReincidentesNum5;
                txtReincidentesNum6.Text = intervencoes.ReincidentesNum6;
                txtReincidentesNum7.Text = intervencoes.ReincidentesNum7;
                txtReincidentesNum8.Text = intervencoes.ReincidentesNum8;
                txtReincidentesNum9.Text = intervencoes.ReincidentesNum9;
                txtReincidentesNum10.Text = intervencoes.ReincidentesNum10;
                txtDesligadosNome1.Text = intervencoes.DesligadosNome1;
                txtDesligadosNome2.Text = intervencoes.DesligadosNome2;
                txtDesligadosNome3.Text = intervencoes.DesligadosNome3;
                txtDesligadosNome4.Text = intervencoes.DesligadosNome4;
                txtDesligadosNome5.Text = intervencoes.DesligadosNome5;
                txtDesligadosNome6.Text = intervencoes.DesligadosNome6;
                txtDesligadosNome7.Text = intervencoes.DesligadosNome7;
                txtDesligadosNome8.Text = intervencoes.DesligadosNome8;
                txtDesligadosNome9.Text = intervencoes.DesligadosNome9;
                txtDesligadosNome10.Text = intervencoes.DesligadosNome10;
                txtDesligadosNum1.Text = intervencoes.DesligadosNum1;
                txtDesligadosNum2.Text = intervencoes.DesligadosNum2;
                txtDesligadosNum3.Text = intervencoes.DesligadosNum3;
                txtDesligadosNum4.Text = intervencoes.DesligadosNum4;
                txtDesligadosNum5.Text = intervencoes.DesligadosNum5;
                txtDesligadosNum6.Text = intervencoes.DesligadosNum6;
                txtDesligadosNum7.Text = intervencoes.DesligadosNum7;
                txtDesligadosNum8.Text = intervencoes.DesligadosNum8;
                txtDesligadosNum9.Text = intervencoes.DesligadosNum9;
                txtDesligadosNum10.Text = intervencoes.DesligadosNum10;
                btnImprimir.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                RNRelatorioIntervencoesDiarias rn = new RNRelatorioIntervencoesDiarias();
                RelatorioIntervencoesDiarias intervencoes = new RelatorioIntervencoesDiarias();
                intervencoes.TecnicoResponsavel = txtTecnicoResponsavel.Text;
                intervencoes.Data = txtData.Text;
                intervencoes.ParaEncaminhamentos1 = txtParaEncaminhamentos1.Text;
                intervencoes.ParaEncaminhamentos2 = txtParaEncaminhamentos2.Text;
                intervencoes.ParaEncaminhamentos3 = txtParaEncaminhamentos3.Text;
                intervencoes.ParaEncaminhamentos4 = txtParaEncaminhamentos4.Text;
                intervencoes.QtdEncaminhamentos1 = txtQtdEncaminhamentos1.Text;
                intervencoes.QtdEncaminhamentos2 = txtQtdEncaminhamentos2.Text;
                intervencoes.QtdEncaminhamentos3 = txtQtdEncaminhamentos3.Text;
                intervencoes.QtdEncaminhamentos4 = txtQtdEncaminhamentos4.Text;
                intervencoes.ParaRelatorios1 = txtParaRelatorios1.Text;
                intervencoes.ParaRelatorios2 = txtParaRelatorios2.Text;
                intervencoes.ParaRelatorios3 = txtParaRelatorios3.Text;
                intervencoes.ParaRelatorios4 = txtParaRelatorios4.Text;
                intervencoes.QtdRelatorios1 = txtQtdRelatorios1.Text;
                intervencoes.QtdRelatorios2 = txtQtdRelatorios2.Text;
                intervencoes.QtdRelatorios3 = txtQtdRelatorios3.Text;
                intervencoes.QtdRelatorios4 = txtQtdRelatorios4.Text;
                intervencoes.ParaVisitas1 = txtParaVisitas1.Text;
                intervencoes.ParaVisitas2 = txtParaVisitas2.Text;
                intervencoes.ParaVisitas3 = txtParaVisitas3.Text;
                intervencoes.ParaVisitas4 = txtParaVisitas4.Text;
                intervencoes.QtdVisitas1 = txtQtdVisitas1.Text;
                intervencoes.QtdVisitas2 = txtQtdVisitas2.Text;
                intervencoes.QtdVisitas3 = txtQtdVisitas3.Text;
                intervencoes.QtdVisitas4 = txtQtdVisitas4.Text;
                intervencoes.ProntuariosNovo1 = txtProntuariosNovo1.Text;
                intervencoes.ProntuariosNovo2 = txtProntuariosNovo2.Text;
                intervencoes.ProntuariosNovo3 = txtProntuariosNovo3.Text;
                intervencoes.ProntuariosNovo4 = txtProntuariosNovo4.Text;
                intervencoes.ProntuariosNovo5 = txtProntuariosNovo5.Text;
                intervencoes.ProntuariosNovo6 = txtProntuariosNovo6.Text;
                intervencoes.ProntuariosNovo7 = txtProntuariosNovo7.Text;
                intervencoes.ProntuariosNovo8 = txtProntuariosNovo8.Text;
                intervencoes.ProntuariosNovo9 = txtProntuariosNovo9.Text;
                intervencoes.ProntuariosNovo10 = txtProntuariosNovo10.Text;
                intervencoes.NumProntuarioNovos1 = txtNumProntuarioNovos1.Text;
                intervencoes.NumProntuarioNovos2 = txtNumProntuarioNovos2.Text;
                intervencoes.NumProntuarioNovos3 = txtNumProntuarioNovos3.Text;
                intervencoes.NumProntuarioNovos4 = txtNumProntuarioNovos4.Text;
                intervencoes.NumProntuarioNovos5 = txtNumProntuarioNovos5.Text;
                intervencoes.NumProntuarioNovos6 = txtNumProntuarioNovos6.Text;
                intervencoes.NumProntuarioNovos7 = txtNumProntuarioNovos7.Text;
                intervencoes.NumProntuarioNovos8 = txtNumProntuarioNovos8.Text;
                intervencoes.NumProntuarioNovos9 = txtNumProntuarioNovos9.Text;
                intervencoes.NumProntuarioNovos10 = txtNumProntuarioNovos10.Text;
                intervencoes.ReincidentesNome1 = txtReincidentesNome1.Text;
                intervencoes.ReincidentesNome2 = txtReincidentesNome2.Text;
                intervencoes.ReincidentesNome3 = txtReincidentesNome3.Text;
                intervencoes.ReincidentesNome4 = txtReincidentesNome4.Text;
                intervencoes.ReincidentesNome5 = txtReincidentesNome5.Text;
                intervencoes.ReincidentesNome6 = txtReincidentesNome6.Text;
                intervencoes.ReincidentesNome7 = txtReincidentesNome7.Text;
                intervencoes.ReincidentesNome8 = txtReincidentesNome8.Text;
                intervencoes.ReincidentesNome9 = txtReincidentesNome9.Text;
                intervencoes.ReincidentesNome10 = txtReincidentesNome10.Text;
                intervencoes.ReincidentesNum1 = txtReincidentesNum1.Text;
                intervencoes.ReincidentesNum2 = txtReincidentesNum2.Text;
                intervencoes.ReincidentesNum3 = txtReincidentesNum3.Text;
                intervencoes.ReincidentesNum4 = txtReincidentesNum4.Text;
                intervencoes.ReincidentesNum5 = txtReincidentesNum5.Text;
                intervencoes.ReincidentesNum6 = txtReincidentesNum6.Text;
                intervencoes.ReincidentesNum7 = txtReincidentesNum7.Text;
                intervencoes.ReincidentesNum8 = txtReincidentesNum8.Text;
                intervencoes.ReincidentesNum9 = txtReincidentesNum9.Text;
                intervencoes.ReincidentesNum10 = txtReincidentesNum10.Text;
                intervencoes.DesligadosNome1 = txtDesligadosNome1.Text;
                intervencoes.DesligadosNome2 = txtDesligadosNome2.Text;
                intervencoes.DesligadosNome3 = txtDesligadosNome3.Text;
                intervencoes.DesligadosNome4 = txtDesligadosNome4.Text;
                intervencoes.DesligadosNome5 = txtDesligadosNome5.Text;
                intervencoes.DesligadosNome6 = txtDesligadosNome6.Text;
                intervencoes.DesligadosNome7 = txtDesligadosNome7.Text;
                intervencoes.DesligadosNome8 = txtDesligadosNome8.Text;
                intervencoes.DesligadosNome9 = txtDesligadosNome9.Text;
                intervencoes.DesligadosNome10 = txtDesligadosNome10.Text;
                intervencoes.DesligadosNum1 = txtDesligadosNum1.Text;
                intervencoes.DesligadosNum2 = txtDesligadosNum2.Text;
                intervencoes.DesligadosNum3 = txtDesligadosNum3.Text;
                intervencoes.DesligadosNum4 = txtDesligadosNum4.Text;
                intervencoes.DesligadosNum5 = txtDesligadosNum5.Text;
                intervencoes.DesligadosNum6 = txtDesligadosNum6.Text;
                intervencoes.DesligadosNum7 = txtDesligadosNum7.Text;
                intervencoes.DesligadosNum8 = txtDesligadosNum8.Text;
                intervencoes.DesligadosNum9 = txtDesligadosNum9.Text;
                intervencoes.DesligadosNum10 = txtDesligadosNum10.Text;
                //intervencoes.Id = (int)cboRelatorios.SelectedValue;
                


                if (updateRelatorio || intervencoes.Id != 0)
                {
                    if (rn.UpdateRelatorioAtividadesEquipeTecnica(intervencoes))
                    {
                        MessageBox.Show("Registro atualizado com sucesso!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                    }
                }
                else if (rn.InsereRelatorioAtividadesEquipeTecnica(intervencoes))
                {
                    MessageBox.Show("Registro salvo com sucesso!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                }
                else
                {
                    MessageBox.Show("Erro ao gravar relatorio!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                }
                cboRelatorios.DataSource = null;
                ListaIntervencoes.Clear();
                relatoriosCarregados = false;
                CarregaRelatorios();
                btnImprimir.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private void CarregaRelatorios()
        {
            try
            {
                RNRelatorioIntervencoesDiarias rn = new RNRelatorioIntervencoesDiarias();
                DataSet ds = new DataSet();
                ds = rn.SelectRelatorioIntervencoesDiarias();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        RelatorioIntervencoesDiarias relatorio = new RelatorioIntervencoesDiarias();
                        relatorio.Id = (int)dr["ID"];
                        relatorio.TecnicoResponsavel = dr["TecnicoResponsavel"].ToString();
                        relatorio.Data = dr["Data"].ToString();
                        relatorio.ParaEncaminhamentos1 = dr["ParaEncaminhamentos1"].ToString();
                        relatorio.ParaEncaminhamentos2 = dr["ParaEncaminhamentos2"].ToString();
                        relatorio.ParaEncaminhamentos3 = dr["ParaEncaminhamentos3"].ToString();
                        relatorio.ParaEncaminhamentos4 = dr["ParaEncaminhamentos4"].ToString();
                        relatorio.QtdEncaminhamentos1 = dr["QtdEncaminhamentos1"].ToString();
                        relatorio.QtdEncaminhamentos2 = dr["QtdEncaminhamentos2"].ToString();
                        relatorio.QtdEncaminhamentos3 = dr["QtdEncaminhamentos3"].ToString();
                        relatorio.QtdEncaminhamentos4 = dr["QtdEncaminhamentos4"].ToString();
                        relatorio.ParaRelatorios1 = dr["ParaRelatorios1"].ToString();
                        relatorio.ParaRelatorios2 = dr["ParaRelatorios2"].ToString();
                        relatorio.ParaRelatorios3 = dr["ParaRelatorios3"].ToString();
                        relatorio.ParaRelatorios4 = dr["ParaRelatorios4"].ToString();
                        relatorio.QtdRelatorios1 = dr["QtdRelatorios1"].ToString();
                        relatorio.QtdRelatorios2 = dr["QtdRelatorios2"].ToString();
                        relatorio.QtdRelatorios3 = dr["QtdRelatorios3"].ToString();
                        relatorio.QtdRelatorios4 = dr["QtdRelatorios4"].ToString();
                        relatorio.ParaVisitas1 = dr["ParaVisitas1"].ToString();
                        relatorio.ParaVisitas2 = dr["ParaVisitas2"].ToString();
                        relatorio.ParaVisitas3 = dr["ParaVisitas3"].ToString();
                        relatorio.ParaVisitas4 = dr["ParaVisitas4"].ToString();
                        relatorio.QtdVisitas1 = dr["QtdVisitas1"].ToString();
                        relatorio.QtdVisitas2 = dr["QtdVisitas2"].ToString();
                        relatorio.QtdVisitas3 = dr["QtdVisitas3"].ToString();
                        relatorio.QtdVisitas4 = dr["QtdVisitas4"].ToString();
                        relatorio.ProntuariosNovo1 = dr["ProntuariosNovo1"].ToString();
                        relatorio.ProntuariosNovo2 = dr["ProntuariosNovo2"].ToString();
                        relatorio.ProntuariosNovo3 = dr["ProntuariosNovo3"].ToString();
                        relatorio.ProntuariosNovo4 = dr["ProntuariosNovo4"].ToString();
                        relatorio.ProntuariosNovo5 = dr["ProntuariosNovo5"].ToString();
                        relatorio.ProntuariosNovo6 = dr["ProntuariosNovo6"].ToString();
                        relatorio.ProntuariosNovo7 = dr["ProntuariosNovo7"].ToString();
                        relatorio.ProntuariosNovo8 = dr["ProntuariosNovo8"].ToString();
                        relatorio.ProntuariosNovo9 = dr["ProntuariosNovo9"].ToString();
                        relatorio.ProntuariosNovo10 = dr["ProntuariosNovo10"].ToString();
                        relatorio.NumProntuarioNovos1 = dr["NumProntuarioNovos1"].ToString();
                        relatorio.NumProntuarioNovos2 = dr["NumProntuarioNovos2"].ToString();
                        relatorio.NumProntuarioNovos3 = dr["NumProntuarioNovos3"].ToString();
                        relatorio.NumProntuarioNovos4 = dr["NumProntuarioNovos4"].ToString();
                        relatorio.NumProntuarioNovos5 = dr["NumProntuarioNovos5"].ToString();
                        relatorio.NumProntuarioNovos6 = dr["NumProntuarioNovos6"].ToString();
                        relatorio.NumProntuarioNovos7 = dr["NumProntuarioNovos7"].ToString();
                        relatorio.NumProntuarioNovos8 = dr["NumProntuarioNovos8"].ToString();
                        relatorio.NumProntuarioNovos9 = dr["NumProntuarioNovos9"].ToString();
                        relatorio.NumProntuarioNovos10 = dr["NumProntuarioNovos10"].ToString();
                        relatorio.ReincidentesNome1 = dr["ReincidentesNome1"].ToString();
                        relatorio.ReincidentesNome2 = dr["ReincidentesNome2"].ToString();
                        relatorio.ReincidentesNome3 = dr["ReincidentesNome3"].ToString();
                        relatorio.ReincidentesNome4 = dr["ReincidentesNome4"].ToString();
                        relatorio.ReincidentesNome5 = dr["ReincidentesNome5"].ToString();
                        relatorio.ReincidentesNome6 = dr["ReincidentesNome6"].ToString();
                        relatorio.ReincidentesNome7 = dr["ReincidentesNome7"].ToString();
                        relatorio.ReincidentesNome8 = dr["ReincidentesNome8"].ToString();
                        relatorio.ReincidentesNome9 = dr["ReincidentesNome9"].ToString();
                        relatorio.ReincidentesNome10 = dr["ReincidentesNome10"].ToString();
                        relatorio.ReincidentesNum1 = dr["ReincidentesNum1"].ToString();
                        relatorio.ReincidentesNum2 = dr["ReincidentesNum2"].ToString();
                        relatorio.ReincidentesNum3 = dr["ReincidentesNum3"].ToString();
                        relatorio.ReincidentesNum4 = dr["ReincidentesNum4"].ToString();
                        relatorio.ReincidentesNum5 = dr["ReincidentesNum5"].ToString();
                        relatorio.ReincidentesNum6 = dr["ReincidentesNum6"].ToString();
                        relatorio.ReincidentesNum7 = dr["ReincidentesNum7"].ToString();
                        relatorio.ReincidentesNum8 = dr["ReincidentesNum8"].ToString();
                        relatorio.ReincidentesNum9 = dr["ReincidentesNum9"].ToString();
                        relatorio.ReincidentesNum10 = dr["ReincidentesNum10"].ToString();
                        relatorio.DesligadosNome1 = dr["DesligadosNome1"].ToString();
                        relatorio.DesligadosNome2 = dr["DesligadosNome2"].ToString();
                        relatorio.DesligadosNome3 = dr["DesligadosNome3"].ToString();
                        relatorio.DesligadosNome4 = dr["DesligadosNome4"].ToString();
                        relatorio.DesligadosNome5 = dr["DesligadosNome5"].ToString();
                        relatorio.DesligadosNome6 = dr["DesligadosNome6"].ToString();
                        relatorio.DesligadosNome7 = dr["DesligadosNome7"].ToString();
                        relatorio.DesligadosNome8 = dr["DesligadosNome8"].ToString();
                        relatorio.DesligadosNome9 = dr["DesligadosNome9"].ToString();
                        relatorio.DesligadosNome10 = dr["DesligadosNome10"].ToString();
                        relatorio.DesligadosNum1 = dr["DesligadosNum1"].ToString();
                        relatorio.DesligadosNum2 = dr["DesligadosNum2"].ToString();
                        relatorio.DesligadosNum3 = dr["DesligadosNum3"].ToString();
                        relatorio.DesligadosNum4 = dr["DesligadosNum4"].ToString();
                        relatorio.DesligadosNum5 = dr["DesligadosNum5"].ToString();
                        relatorio.DesligadosNum6 = dr["DesligadosNum6"].ToString();
                        relatorio.DesligadosNum7 = dr["DesligadosNum7"].ToString();
                        relatorio.DesligadosNum8 = dr["DesligadosNum8"].ToString();
                        relatorio.DesligadosNum9 = dr["DesligadosNum9"].ToString();
                        relatorio.DesligadosNum10 = dr["DesligadosNum10"].ToString();
                        ListaIntervencoes.Add(relatorio);
                    }
                    cboRelatorios.DataSource = ds.Tables[0];
                    cboRelatorios.ValueMember = "ID";
                    cboRelatorios.DisplayMember = "TecnicoResponsavel";
                    cboRelatorios.SelectedIndex = 0;
                    relatoriosCarregados = true;
                    updateRelatorio = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private void frmRelatorioIntervençõesDiarias_Load(object sender, EventArgs e)
        {
            try
            {
                CarregaRelatorios();

                foreach (Control c in this.Controls)
                {
                    foreach (Control y in c.Controls)
                    {
                        foreach (Control x in y.Controls)
                        {
                            if (x.GetType().ToString() == "System.Windows.Forms.TextBox")
                            {
                                TextBox tx = (TextBox)x;
                                tx.ShortcutsEnabled = true;
                            }
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }
    }
}
