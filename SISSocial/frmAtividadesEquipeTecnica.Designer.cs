﻿namespace SISSocial
{
    partial class frmAtividadesEquipeTecnica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnNovo = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.lblOut = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cboRelatorios = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtDataEntrega = new System.Windows.Forms.MaskedTextBox();
            this.txtMesReferencia = new System.Windows.Forms.MaskedTextBox();
            this.txtFuncao = new System.Windows.Forms.TextBox();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.txtNomeServico = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.txtParcerias = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtAtividadesGratuitas = new System.Windows.Forms.TextBox();
            this.txtOrigemRecursosFinanceiros = new System.Windows.Forms.TextBox();
            this.txtResultadosObtidos = new System.Windows.Forms.TextBox();
            this.txtAbrangenciaTerritorial = new System.Windows.Forms.TextBox();
            this.txtRecursosHumanos = new System.Windows.Forms.TextBox();
            this.txtEncontrosRealizados = new System.Windows.Forms.TextBox();
            this.txtQtdPessoasAtendidas = new System.Windows.Forms.TextBox();
            this.txtPublicoAlvo = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDescricaoAtividade = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnNovo);
            this.groupBox1.Controls.Add(this.btnSalvar);
            this.groupBox1.Controls.Add(this.lblOut);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.btnCancelar);
            this.groupBox1.Controls.Add(this.btnImprimir);
            this.groupBox1.Controls.Add(this.txtParcerias);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txtAtividadesGratuitas);
            this.groupBox1.Controls.Add(this.txtOrigemRecursosFinanceiros);
            this.groupBox1.Controls.Add(this.txtResultadosObtidos);
            this.groupBox1.Controls.Add(this.txtAbrangenciaTerritorial);
            this.groupBox1.Controls.Add(this.txtRecursosHumanos);
            this.groupBox1.Controls.Add(this.txtEncontrosRealizados);
            this.groupBox1.Controls.Add(this.txtQtdPessoasAtendidas);
            this.groupBox1.Controls.Add(this.txtPublicoAlvo);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtDescricaoAtividade);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1239, 651);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Relatório Mensal de Atividades Equipe Técnica";
            // 
            // btnNovo
            // 
            this.btnNovo.Location = new System.Drawing.Point(824, 615);
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(89, 36);
            this.btnNovo.TabIndex = 50;
            this.btnNovo.Text = "Novo";
            this.btnNovo.UseVisualStyleBackColor = true;
            this.btnNovo.Click += new System.EventHandler(this.btnNovo_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(919, 615);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(89, 36);
            this.btnSalvar.TabIndex = 49;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // lblOut
            // 
            this.lblOut.AutoSize = true;
            this.lblOut.BackColor = System.Drawing.Color.LightGray;
            this.lblOut.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOut.ForeColor = System.Drawing.Color.Red;
            this.lblOut.Location = new System.Drawing.Point(826, 543);
            this.lblOut.Name = "lblOut";
            this.lblOut.Size = new System.Drawing.Size(110, 18);
            this.lblOut.TabIndex = 48;
            this.lblOut.Text = "arquivo Saida";
            this.lblOut.Visible = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cboRelatorios);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.txtDataEntrega);
            this.groupBox2.Controls.Add(this.txtMesReferencia);
            this.groupBox2.Controls.Add(this.txtFuncao);
            this.groupBox2.Controls.Add(this.txtNome);
            this.groupBox2.Controls.Add(this.label20);
            this.groupBox2.Controls.Add(this.txtNomeServico);
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Location = new System.Drawing.Point(11, 18);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1222, 75);
            this.groupBox2.TabIndex = 33;
            this.groupBox2.TabStop = false;
            // 
            // cboRelatorios
            // 
            this.cboRelatorios.FormattingEnabled = true;
            this.cboRelatorios.Location = new System.Drawing.Point(923, 40);
            this.cboRelatorios.Name = "cboRelatorios";
            this.cboRelatorios.Size = new System.Drawing.Size(274, 21);
            this.cboRelatorios.TabIndex = 44;
            this.cboRelatorios.SelectedIndexChanged += new System.EventHandler(this.cboRelatorios_SelectedIndexChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(920, 19);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(155, 15);
            this.label17.TabIndex = 43;
            this.label17.Text = "Relatorios Disponiveis:";
            // 
            // txtDataEntrega
            // 
            this.txtDataEntrega.Location = new System.Drawing.Point(829, 39);
            this.txtDataEntrega.Mask = "00/00/0000";
            this.txtDataEntrega.Name = "txtDataEntrega";
            this.txtDataEntrega.Size = new System.Drawing.Size(75, 20);
            this.txtDataEntrega.TabIndex = 42;
            // 
            // txtMesReferencia
            // 
            this.txtMesReferencia.Location = new System.Drawing.Point(829, 15);
            this.txtMesReferencia.Mask = "00/00/0000";
            this.txtMesReferencia.Name = "txtMesReferencia";
            this.txtMesReferencia.Size = new System.Drawing.Size(75, 20);
            this.txtMesReferencia.TabIndex = 41;
            // 
            // txtFuncao
            // 
            this.txtFuncao.Location = new System.Drawing.Point(455, 40);
            this.txtFuncao.Name = "txtFuncao";
            this.txtFuncao.Size = new System.Drawing.Size(239, 20);
            this.txtFuncao.TabIndex = 38;
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(124, 40);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(251, 20);
            this.txtNome.TabIndex = 37;
            // 
            // txtNomeServico
            // 
            this.txtNomeServico.Location = new System.Drawing.Point(124, 11);
            this.txtNomeServico.Name = "txtNomeServico";
            this.txtNomeServico.Size = new System.Drawing.Size(251, 20);
            this.txtNomeServico.TabIndex = 36;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(69, 39);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 15);
            this.label13.TabIndex = 32;
            this.label13.Text = "Nome:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 12);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(122, 15);
            this.label11.TabIndex = 31;
            this.label11.Text = "Nome do serviço: ";
            this.label11.Click += new System.EventHandler(this.label11_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(1105, 615);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(89, 36);
            this.btnCancelar.TabIndex = 30;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // btnImprimir
            // 
            this.btnImprimir.Enabled = false;
            this.btnImprimir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImprimir.Location = new System.Drawing.Point(1014, 615);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(85, 36);
            this.btnImprimir.TabIndex = 29;
            this.btnImprimir.Text = "Imprimir";
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // txtParcerias
            // 
            this.txtParcerias.AcceptsReturn = true;
            this.txtParcerias.Location = new System.Drawing.Point(6, 564);
            this.txtParcerias.Multiline = true;
            this.txtParcerias.Name = "txtParcerias";
            this.txtParcerias.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtParcerias.Size = new System.Drawing.Size(792, 83);
            this.txtParcerias.TabIndex = 28;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(8, 543);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(104, 18);
            this.label12.TabIndex = 27;
            this.label12.Text = "2.\tParcerias: ";
            // 
            // txtAtividadesGratuitas
            // 
            this.txtAtividadesGratuitas.AcceptsReturn = true;
            this.txtAtividadesGratuitas.Location = new System.Drawing.Point(824, 417);
            this.txtAtividadesGratuitas.Multiline = true;
            this.txtAtividadesGratuitas.Name = "txtAtividadesGratuitas";
            this.txtAtividadesGratuitas.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtAtividadesGratuitas.Size = new System.Drawing.Size(388, 116);
            this.txtAtividadesGratuitas.TabIndex = 25;
            this.txtAtividadesGratuitas.TextChanged += new System.EventHandler(this.textBox9_TextChanged);
            // 
            // txtOrigemRecursosFinanceiros
            // 
            this.txtOrigemRecursosFinanceiros.AcceptsReturn = true;
            this.txtOrigemRecursosFinanceiros.Location = new System.Drawing.Point(824, 275);
            this.txtOrigemRecursosFinanceiros.Multiline = true;
            this.txtOrigemRecursosFinanceiros.Name = "txtOrigemRecursosFinanceiros";
            this.txtOrigemRecursosFinanceiros.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtOrigemRecursosFinanceiros.Size = new System.Drawing.Size(388, 119);
            this.txtOrigemRecursosFinanceiros.TabIndex = 24;
            // 
            // txtResultadosObtidos
            // 
            this.txtResultadosObtidos.AcceptsReturn = true;
            this.txtResultadosObtidos.Location = new System.Drawing.Point(824, 140);
            this.txtResultadosObtidos.Multiline = true;
            this.txtResultadosObtidos.Name = "txtResultadosObtidos";
            this.txtResultadosObtidos.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtResultadosObtidos.Size = new System.Drawing.Size(384, 113);
            this.txtResultadosObtidos.TabIndex = 23;
            // 
            // txtAbrangenciaTerritorial
            // 
            this.txtAbrangenciaTerritorial.AcceptsReturn = true;
            this.txtAbrangenciaTerritorial.Location = new System.Drawing.Point(414, 417);
            this.txtAbrangenciaTerritorial.Multiline = true;
            this.txtAbrangenciaTerritorial.Name = "txtAbrangenciaTerritorial";
            this.txtAbrangenciaTerritorial.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtAbrangenciaTerritorial.Size = new System.Drawing.Size(384, 116);
            this.txtAbrangenciaTerritorial.TabIndex = 22;
            this.txtAbrangenciaTerritorial.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
            // 
            // txtRecursosHumanos
            // 
            this.txtRecursosHumanos.AcceptsReturn = true;
            this.txtRecursosHumanos.Location = new System.Drawing.Point(414, 275);
            this.txtRecursosHumanos.Multiline = true;
            this.txtRecursosHumanos.Name = "txtRecursosHumanos";
            this.txtRecursosHumanos.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRecursosHumanos.Size = new System.Drawing.Size(384, 119);
            this.txtRecursosHumanos.TabIndex = 21;
            // 
            // txtEncontrosRealizados
            // 
            this.txtEncontrosRealizados.AcceptsReturn = true;
            this.txtEncontrosRealizados.Location = new System.Drawing.Point(414, 140);
            this.txtEncontrosRealizados.Multiline = true;
            this.txtEncontrosRealizados.Name = "txtEncontrosRealizados";
            this.txtEncontrosRealizados.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtEncontrosRealizados.Size = new System.Drawing.Size(384, 113);
            this.txtEncontrosRealizados.TabIndex = 20;
            // 
            // txtQtdPessoasAtendidas
            // 
            this.txtQtdPessoasAtendidas.AcceptsReturn = true;
            this.txtQtdPessoasAtendidas.Location = new System.Drawing.Point(11, 417);
            this.txtQtdPessoasAtendidas.Multiline = true;
            this.txtQtdPessoasAtendidas.Name = "txtQtdPessoasAtendidas";
            this.txtQtdPessoasAtendidas.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtQtdPessoasAtendidas.Size = new System.Drawing.Size(384, 116);
            this.txtQtdPessoasAtendidas.TabIndex = 19;
            this.txtQtdPessoasAtendidas.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // txtPublicoAlvo
            // 
            this.txtPublicoAlvo.AcceptsReturn = true;
            this.txtPublicoAlvo.Location = new System.Drawing.Point(11, 275);
            this.txtPublicoAlvo.Multiline = true;
            this.txtPublicoAlvo.Name = "txtPublicoAlvo";
            this.txtPublicoAlvo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtPublicoAlvo.Size = new System.Drawing.Size(384, 119);
            this.txtPublicoAlvo.TabIndex = 18;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(821, 398);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(353, 16);
            this.label10.TabIndex = 17;
            this.label10.Text = "Destacar se as atividades são gratuitas para os usuários. ";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(821, 256);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(206, 16);
            this.label9.TabIndex = 15;
            this.label9.Text = "Origem dos recursos financeiros: ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(821, 121);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(308, 16);
            this.label8.TabIndex = 13;
            this.label8.Text = "Resultados obtidos a partir da atividade realizada:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(411, 398);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(150, 16);
            this.label7.TabIndex = 11;
            this.label7.Text = "Abrangência Territorial: ";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 398);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(221, 16);
            this.label6.TabIndex = 9;
            this.label6.Text = "Quantidade de pessoas atendidas: ";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(411, 256);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(132, 16);
            this.label5.TabIndex = 7;
            this.label5.Text = "Recursos Humanos: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(411, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(331, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "Dia/horário/periodicidade/nº de encontros realizados: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 256);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "Público-Alvo: ";
            // 
            // txtDescricaoAtividade
            // 
            this.txtDescricaoAtividade.AcceptsReturn = true;
            this.txtDescricaoAtividade.Location = new System.Drawing.Point(11, 140);
            this.txtDescricaoAtividade.Multiline = true;
            this.txtDescricaoAtividade.Name = "txtDescricaoAtividade";
            this.txtDescricaoAtividade.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtDescricaoAtividade.Size = new System.Drawing.Size(384, 113);
            this.txtDescricaoAtividade.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(212, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Descrição da atividade realizada: ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 96);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(360, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "1.\tAtividades, Serviços, Programas e Projetos: ";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(704, 40);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(119, 15);
            this.label18.TabIndex = 33;
            this.label18.Text = "Data de Entrega: ";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(391, 40);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(58, 15);
            this.label19.TabIndex = 34;
            this.label19.Text = "Função:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(698, 16);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(136, 15);
            this.label20.TabIndex = 35;
            this.label20.Text = "Mês de Referência: ";
            // 
            // frmAtividadesEquipeTecnica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1261, 675);
            this.Controls.Add(this.groupBox1);
            this.Name = "frmAtividadesEquipeTecnica";
            this.Text = "Atividades Equipe Técnica";
            this.Load += new System.EventHandler(this.frmAtividadesEquipeTecnica_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtFuncao;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.TextBox txtNomeServico;
        private System.Windows.Forms.MaskedTextBox txtDataEntrega;
        private System.Windows.Forms.MaskedTextBox txtMesReferencia;
        private System.Windows.Forms.Label lblOut;
        private System.Windows.Forms.Button btnSalvar;
        public System.Windows.Forms.TextBox txtDescricaoAtividade;
        public System.Windows.Forms.TextBox txtAtividadesGratuitas;
        public System.Windows.Forms.TextBox txtOrigemRecursosFinanceiros;
        public System.Windows.Forms.TextBox txtResultadosObtidos;
        public System.Windows.Forms.TextBox txtAbrangenciaTerritorial;
        public System.Windows.Forms.TextBox txtRecursosHumanos;
        public System.Windows.Forms.TextBox txtEncontrosRealizados;
        public System.Windows.Forms.TextBox txtQtdPessoasAtendidas;
        public System.Windows.Forms.TextBox txtPublicoAlvo;
        public System.Windows.Forms.TextBox txtParcerias;
        private System.Windows.Forms.ComboBox cboRelatorios;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnNovo;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
    }
}