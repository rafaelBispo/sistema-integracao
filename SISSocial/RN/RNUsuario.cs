﻿using System;
using System.Data;
using Util;

namespace SISSocial.RN
{
    public class RNUsuario
    {
        public string erro = "";
        private ConnectionHelper ConnectionHelper = new ConnectionHelper();

        public bool VerificaAcesso(string pUsuario, string pSenha)
        {
            DataSet ds = new DataSet();
            string sSql = "";
            try
            {
                sSql += "select * from sisSocial_db..usuario_tb ";
                sSql += " where usuario = '" + pUsuario + "'";
                sSql += " and senha = '" + pSenha + "'";

                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public string RetornaUsuarioAcesso(string pUsuario)
        {
            DataSet ds = new DataSet();
            string sSql = "";
            string usuario = "";
            try
            {
                sSql += "select * from sisSocial_db..usuario_tb ";
                sSql += " where usuario = '" + pUsuario + "'";

                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    usuario = ds.Tables[0].Rows[0].ItemArray[1].ToString();
                    return usuario;
                }
                else
                {
                    return usuario;
                }
            }
            catch (Exception )
            {
                return usuario;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public Int32 RetornaEmpresaUsuarioAcesso(string pUsuario)
        {
            DataSet ds = new DataSet();
            string sSql = "";
            int cod_empresa = 0;
            try
            {
                sSql += "select * from sisSocial_db..usuario_tb ";
                sSql += " where usuario = '" + pUsuario + "'";

                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    cod_empresa = Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[4].ToString());
                    return cod_empresa;
                }
                else
                {
                    return cod_empresa;
                }
            }
            catch (Exception )
            {
                return cod_empresa;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public DataSet RetornaUsuariosEmpresa(Int32 pcod_empresa)
        {
            DataSet ds = new DataSet();
            string sSql = "";
            try
            {
                sSql += "select usuario from sisSocial_db..usuario_tb ";
                sSql += " where cod_empresa = " + pcod_empresa + "";

                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                if (ds.Tables[0].Rows.Count > 0)
                {

                    return ds;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception )
            {
                return null;
            }
            finally
            {
                ds.Dispose();
            }
        }


        public Int32 RetornaTipoAcesso(string pUsuario)
        {
            DataSet ds = new DataSet();
            string sSql = "";
            try
            {
                sSql += "select cod_acessos from sisSocial_db..usuario_tb ";
                sSql += " where usuario = '" + pUsuario + "'";

                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    return Convert.ToInt32(ds.Tables[0].Rows[0].ItemArray[0].ToString());
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception)
            {
                return 0;
            }
            finally
            {
                ds.Dispose();
            }
        }

        public bool CadastraUsuario(string pUsuario, string pSenha, Int32 pTipoAcesso)
        {
            DataSet ds = new DataSet();
            string sSql = "";
            try
            {
                sSql += "insert into sisSocial_db..usuario_tb (usuario, senha,cod_acessos, cod_empresa, dt_inclusao)";
                sSql += " values ('" + pUsuario + "',";
                sSql += "'" + pSenha + "', 0, " + pTipoAcesso + ", GETDATE())";

                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                return true;
            }
            catch (Exception ex)
            {
                erro = ex.Message;
                return false;
            }
            finally
            {
                ds.Dispose();
            }
        }

    }
}
