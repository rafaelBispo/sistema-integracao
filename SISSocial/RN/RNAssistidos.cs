﻿using SISSocial.Classes;
using System;
using System.Data;
using System.Security.AccessControl;
using Util;

namespace SISSocial.RN
{
    public class RNAssistidos
    {

        private ConnectionHelper ConnectionHelper = new ConnectionHelper();

        public string NumeroCadastro()
        {
            string numCadastro = "0";
            DataSet ds = new DataSet();
            string sSql = "";
            try
            {

                sSql += "select max(cod_id) + 1 from sisSocial_db..codigo_assistido_tb";

                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                if (ds.Tables[0].Rows[0].ItemArray[0].ToString() != "")
                {
                    numCadastro = ds.Tables[0].Rows[0].ItemArray[0].ToString();
                }
                else
                {
                    numCadastro = "1";
                }

                numCadastro = numCadastro.ToString().PadLeft(5, '0');

                return numCadastro;

            }
            catch (Exception )
            {
                return "0";
            }

        }

        public void insereCodigoFicha(Assistido pAssis, string usuario)
        {
            try
            {
                string sSql = "";
                sSql += "insert into sisSocial_db..codigo_assistido_tb (cpf_assistido,qtd_composicao_familiar,dtCadastro, usuario)";
                sSql += "values ('" + pAssis.Cpf + "',1, '" + pAssis.DtCadastro + "', '" + usuario + "' )";

                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

            }

            catch (Exception)
            {
            }
        }

        public bool InsereAssistido(Assistido pAssis, string pUsuarioLogado)
        {
            try
            {
                string sSql = "";
                sSql += "exec sisSocial_db..INSERE_ASSISTIDO_SPI ";
                sSql += "@codigo = '" + pAssis.Codigo + "' ";
                sSql += ",@orientacao = " + pAssis.Orientacao + " ";
                sSql += ",@nome = '" + pAssis.Nome + "' ";
                sSql += ",@sexo = '" + pAssis.Sexo + "' ";
                sSql += ",@estadoCivil = " + pAssis.EstadoCivil + " ";
                sSql += ",@cpf = '" + pAssis.Cpf + "' ";
                sSql += ",@rg = '" + pAssis.Rg + "' ";
                sSql += ",@nis = '" + pAssis.Nis + "' ";
                sSql += ",@dtNascimento= '" + pAssis.DtNascimento + "' ";
                sSql += ",@endereco = '" + pAssis.Endereco + "' ";
                sSql += ",@telefone = '" + pAssis.Telefone + "' ";
                sSql += ",@mae = '" + pAssis.Mae + "' ";
                sSql += ",@pai = '" + pAssis.Pai + "' ";
                sSql += ",@escolaridade = " + pAssis.Escolaridade + " ";
                sSql += ",@problemaSaude = " + pAssis.ProblemaSaude + " ";
                sSql += ",@qualProblemaSaude = '" + pAssis.QualProblemaSaude + "' ";
                sSql += ",@possuiDeficiencia = " + pAssis.PossuiDeficiencia + " ";
                sSql += ",@idade = " + pAssis.Idade + " ";
                sSql += ",@filhos = " + pAssis.Filhos + " ";
                sSql += ",@qtdFilhos = " + pAssis.QtdFilhos + " ";
                sSql += ",@recebePensao = " + pAssis.RecebePensao + " ";
                sSql += ",@maeCriaFilhosSemApoioPais = " + pAssis.MaeCriaFilhosSemApoioPais + " ";
                sSql += ",@maePresenteFamilia = " + pAssis.MaePresenteFamilia + " ";
                sSql += ",@idadeMae = " + pAssis.IdadeMae + " ";
                sSql += ",@recebeBolsaFamilia = " + pAssis.RecebeBolsaFamilia + " ";
                sSql += ",@valorBolsaFamilia = " + pAssis.ValorBolsaFamilia + " ";
                sSql += ",@recebeLoas = " + pAssis.RecebeLoas + " ";
                sSql += ",@valorLoas = '" + pAssis.ValorLoas + "' ";
                sSql += ",@casaE = " + pAssis.CasaE + " ";
                sSql += ",@valorAluguel = " + pAssis.ValorAluguel + " ";
                sSql += ",@qtdPessoasCasa = " + pAssis.QtdPessoasCasa + " ";
                sSql += ",@carteiraAssinada = " + pAssis.CarteiraAssinada + " ";
                sSql += ",@profissao = '" + pAssis.Profissao + "' ";
                sSql += ",@auxilioPrevidenciario = " + pAssis.AuxilioPrevidenciario + " ";
                sSql += ",@cadastroCras = " + pAssis.CadastroCras + " ";
                sSql += ",@dtCadastro = '" + pAssis.DtCadastro + "' ";
                sSql += ",@bairro = '" + pAssis.Bairro + "' ";
                sSql += ",@usuario = '" + pUsuarioLogado + "' ";
                sSql += ",@responsavel = '" + pAssis.Responsavel + "' ";

                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                return true;
            }

            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateAssistido(Assistido pAssis, string pUsuarioLogado)
        {
            try
            {
                string sSql = "";
                sSql += "exec sisSocial_db..ALTERA_ASSISTIDO_SPU ";
                sSql += "@codigo = '" + pAssis.Codigo + "' ";
                sSql += ",@orientacao = " + pAssis.Orientacao + " ";
                sSql += ",@nome = '" + pAssis.Nome + "' ";
                sSql += ",@sexo = '" + pAssis.Sexo + "' ";
                sSql += ",@estadoCivil = " + pAssis.EstadoCivil + " ";
                sSql += ",@cpf = '" + pAssis.Cpf + "' ";
                sSql += ",@rg = '" + pAssis.Rg + "' ";
                sSql += ",@nis = '" + pAssis.Nis + "' ";
                sSql += ",@dtNascimento= '" + pAssis.DtNascimento + "' ";
                sSql += ",@endereco = '" + pAssis.Endereco + "' ";
                sSql += ",@telefone = '" + pAssis.Telefone + "' ";
                sSql += ",@mae = '" + pAssis.Mae + "' ";
                sSql += ",@pai = '" + pAssis.Pai + "' ";
                sSql += ",@escolaridade = " + pAssis.Escolaridade + " ";
                sSql += ",@problemaSaude = " + pAssis.ProblemaSaude + " ";
                sSql += ",@qualProblemaSaude = '" + pAssis.QualProblemaSaude + "' ";
                sSql += ",@possuiDeficiencia = " + pAssis.PossuiDeficiencia + " ";
                sSql += ",@idade = " + pAssis.Idade + " ";
                sSql += ",@filhos = " + pAssis.Filhos + " ";
                sSql += ",@qtdFilhos = " + pAssis.QtdFilhos + " ";
                sSql += ",@recebePensao = " + pAssis.RecebePensao + " ";
                sSql += ",@maeCriaFilhosSemApoioPais = " + pAssis.MaeCriaFilhosSemApoioPais + " ";
                sSql += ",@maePresenteFamilia = " + pAssis.MaePresenteFamilia + " ";
                sSql += ",@idadeMae = " + pAssis.IdadeMae + " ";
                sSql += ",@recebeBolsaFamilia = " + pAssis.RecebeBolsaFamilia + " ";
                sSql += ",@valorBolsaFamilia = " + pAssis.ValorBolsaFamilia + " ";
                sSql += ",@recebeLoas = " + pAssis.RecebeLoas + " ";
                sSql += ",@valorLoas =  '" + pAssis.ValorLoas + "' ";
                sSql += ",@casaE = " + pAssis.CasaE + " ";
                sSql += ",@valorAluguel = " + pAssis.ValorAluguel + " ";
                sSql += ",@qtdPessoasCasa = " + pAssis.QtdPessoasCasa + " ";
                sSql += ",@carteiraAssinada = " + pAssis.CarteiraAssinada + " ";
                sSql += ",@profissao = '" + pAssis.Profissao + "' ";
                sSql += ",@auxilioPrevidenciario = " + pAssis.AuxilioPrevidenciario + " ";
                sSql += ",@cadastroCras = " + pAssis.CadastroCras + " ";
                sSql += ",@dtCadastro = '" + pAssis.DtCadastro + "' ";
                sSql += ",@bairro = '" + pAssis.Bairro + "' ";
                sSql += ",@usuario = '" + pUsuarioLogado + "' ";
                sSql += ",@responsavel = '" + pAssis.Responsavel + "' ";

                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                return true;
            }

            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteAssistido(Assistido pAssis)
        {
            try
            {
                string sSql = "";
                sSql += "exec sisSocial_db..DELETE_ASSISTIDO_SPD ";
                sSql += "@cpf = '" + pAssis.Cpf + "' ";

                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                return true;
            }

            catch (Exception )
            {
                return false;
            }
        }

        public bool InsereComposicaoFamiliar(ComposicaoFamiliar pComposicao, string pUsuarioLogado)
        {
            try
            {
                string sSql = "";
                sSql += "exec sisSocial_db..INSERE_composicao_SPI ";
                sSql += "@codigo = '" + pComposicao.Codigo + "' ";
                sSql += ",@nome = '" + pComposicao.Nome + "' ";
                sSql += ",@sexo = '" + pComposicao.Sexo + "' ";
                sSql += ",@estadoCivil = " + pComposicao.EstadoCivil + " ";
                sSql += ",@cpf = '" + pComposicao.Cpf + "' ";
                sSql += ",@rg = '" + pComposicao.Rg + "' ";
                sSql += ",@dtNascimento= '" + pComposicao.DtNascimento + "' ";
                sSql += ",@telefone = '" + pComposicao.Telefone + "' ";
                sSql += ",@escolaridade = '" + pComposicao.Escolaridade + "' ";
                sSql += ",@possuiDeficiencia = " + pComposicao.PossuiDeficiencia + " ";
                sSql += ",@idade = " + pComposicao.Idade + " ";
                sSql += ",@carteiraAssinada = " + pComposicao.CarteiraAssinada + " ";
                sSql += ",@profissao = '" + pComposicao.Profissao + "' ";
                sSql += ",@usuario = '" + pUsuarioLogado + "' ";
                sSql += ",@certidaoNasc = '" + pComposicao.CertidaoNascimento + "' ";
                sSql += ",@CartaoSUS = '" + pComposicao.CartaoSUS + "' ";
                sSql += ",@Filiacao = '" + pComposicao.Filiacao + "' ";
                sSql += ",@Laudo = '" + pComposicao.Laudo + "' ";
                sSql += ",@ProblemaSaude = '" + pComposicao.ProblemaSaude+ "' ";
                sSql += ",@QualProblemaSaude = '" + pComposicao.QualProblemaSaude + "' ";
                sSql += ",@AlergiaAlimento = '" + pComposicao.AlergiaAlimento + "' ";
                sSql += ",@QualAlergiaAlimento = '" + pComposicao.QualAlergiaAlimento + "' ";
                sSql += ",@UsoMedicamento = '" + pComposicao.UsoMedicamento + "' ";
                sSql += ",@QualUsoMedicamento = '" + pComposicao.QualUsoMedicamento + "' ";
                sSql += ",@AtividadeAlemSCFV = '" + pComposicao.AtividadeAlemSCFV + "' ";
                sSql += ",@QualAtividadeAlemSCFV = '" + pComposicao.QualAtividadeAlemSCFV+ "' ";
                sSql += ",@RestricaoParticiparAtividade = '" + pComposicao.RestricaoParticiparAtividade+ "' ";
                sSql += ",@QualRestricaoParticiparAtividade = '" + pComposicao.QualRestricaoParticiparAtividade + "' ";
                sSql += ",@CadastroCRAS = '" + pComposicao.CadastroCras + "' ";
                sSql += ",@QualCadastroCRAS = '" + pComposicao.QualCadastroCras+ "' ";


                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                return true;
            }

            catch (Exception )
            {
                return false;
            }
        }

        public bool AlterarComposicaoFamiliar(ComposicaoFamiliar pComposicao, string pUsuarioLogado)
        {
            try
            {
                string sSql = "";
                sSql += "exec sisSocial_db..ALTERAR_COMPOSICAO_SPU ";
                sSql += "@codigo = '" + pComposicao.Codigo + "' ";
                sSql += ",@nome = '" + pComposicao.Nome + "' ";
                sSql += ",@sexo = '" + pComposicao.Sexo + "' ";
                sSql += ",@estadoCivil = " + pComposicao.EstadoCivil + " ";
                sSql += ",@cpf = '" + pComposicao.Cpf + "' ";
                sSql += ",@rg = '" + pComposicao.Rg + "' ";
                sSql += ",@dtNascimento= '" + pComposicao.DtNascimento + "' ";
                sSql += ",@telefone = '" + pComposicao.Telefone + "' ";
                sSql += ",@escolaridade = '" + pComposicao.Escolaridade + "' ";
                sSql += ",@possuiDeficiencia = " + pComposicao.PossuiDeficiencia + " ";
                sSql += ",@idade = " + pComposicao.Idade + " ";
                sSql += ",@carteiraAssinada = " + pComposicao.CarteiraAssinada + " ";
                sSql += ",@profissao = '" + pComposicao.Profissao + "' ";
                sSql += ",@usuario = '" + pUsuarioLogado + "' ";
                sSql += ",@certidaoNasc = '" + pComposicao.CertidaoNascimento + "' ";
                sSql += ",@CartaoSUS = '" + pComposicao.CartaoSUS + "' ";
                sSql += ",@Filiacao = '" + pComposicao.Filiacao + "' ";
                sSql += ",@Laudo = '" + pComposicao.Laudo + "' ";
                sSql += ",@ProblemaSaude = '" + pComposicao.ProblemaSaude + "' ";
                sSql += ",@QualProblemaSaude = '" + pComposicao.QualProblemaSaude + "' ";
                sSql += ",@AlergiaAlimento = '" + pComposicao.AlergiaAlimento + "' ";
                sSql += ",@QualAlergiaAlimento = '" + pComposicao.QualAlergiaAlimento + "' ";
                sSql += ",@UsoMedicamento = '" + pComposicao.UsoMedicamento + "' ";
                sSql += ",@QualUsoMedicamento = '" + pComposicao.QualUsoMedicamento + "' ";
                sSql += ",@AtividadeAlemSCFV = '" + pComposicao.AtividadeAlemSCFV + "' ";
                sSql += ",@QualAtividadeAlemSCFV = '" + pComposicao.QualAtividadeAlemSCFV + "' ";
                sSql += ",@RestricaoParticiparAtividade = '" + pComposicao.RestricaoParticiparAtividade + "' ";
                sSql += ",@QualRestricaoParticiparAtividade = '" + pComposicao.QualRestricaoParticiparAtividade + "' ";
                sSql += ",@CadastroCRAS = '" + pComposicao.CadastroCras + "' ";
                sSql += ",@QualCadastroCRAS = '" + pComposicao.QualCadastroCras+ "' ";

                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                return true;
            }

            catch (Exception )
            {
                return false;
            }
        }

        public bool DeleteComposicaoFamiliar(ComposicaoFamiliar pComposicao)
        {
            try
            {
                string sSql = "";
                sSql += "exec sisSocial_db..delete_composicao_SPD ";
                sSql += "@cpf = '" + pComposicao.Cpf + "' ";

                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                return true;
            }

            catch (Exception )
            {
                return false;
            }
        }

        public DataSet BuscarAssistidoPorCPF(string pCpf)
        {
            DataSet ds = new DataSet();

            try
            {
                string sSql = "";
                sSql += "select top 1 * from sisSocial_db..assistido_tb ";
                sSql += "where cpf = '" + pCpf + "' ";

                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                return ds;

            }
            catch (Exception )
            {
                return ds;
            }

        }

        public DataSet BuscarAssistidoPorNome(string pNome)
        {
            DataSet ds = new DataSet();

            try
            {
                string sSql = "";
                sSql += "select * from sisSocial_db..assistido_tb ";
                sSql += "where nome  like '" + pNome + "%' ";

                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                return ds;

            }
            catch (Exception )
            {
                return ds;
            }

        }

        public DataSet BuscarAssistidoPorNumeroFicha(string pFicha)
        {
            DataSet ds = new DataSet();

            try
            {
                string sSql = "";
                sSql += "select top 1 * from sisSocial_db..assistido_tb ";
                sSql += "where convert(int, codigo) = '" + pFicha + "' ";

                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                return ds;

            }
            catch (Exception )
            {
                return ds;
            }

        }

        public DataSet BuscarComposicaoFamiliarPorCodigo(string pNumeroFicha)
        {
            DataSet ds = new DataSet();

            try
            {
                string sSql = "";
                sSql += "select * from sisSocial_db..composicao_familiar_tb ";
                sSql += "where codigo = '" + pNumeroFicha + "' ";

                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                return ds;

            }
            catch (Exception )
            {
                return ds;
            }

        }

        public DataSet BuscarComposicaoFamiliarPorNome(string pNome)
        {
            DataSet ds = new DataSet();

            try
            {
                string sSql = "";
                sSql += "select * from sisSocial_db..composicao_familiar_tb ";
                sSql += "where nome = '" + pNome + "' ";

                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                return ds;

            }
            catch (Exception )
            {
                return ds;
            }

        }

        public DataSet BuscarComposicaoFamiliarPorCPF(string pCPF, string pNome)
        {
            DataSet ds = new DataSet();

            try
            {
                string sSql = "";
                sSql += "select * from sisSocial_db..composicao_familiar_tb ";
                sSql += "where cpf = '" + pCPF + "' ";
                sSql += "and nome = '" + pNome + "' ";

                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                return ds;

            }
            catch (Exception)
            {
                return ds;
            }

        }
        public DataSet BuscarAssistidoPorBairro(string pcodBairro)
        {
            DataSet ds = new DataSet();

            try
            {
                string sSql = "";
                sSql += "select a.codigo, a.nome,a.cpf, a.rg, a.dtNascimento, a.endereco, a.telefone, a.profissao, a.dtCadastro  ";
                sSql += "from sisSocial_db..assistido_tb a ";
                sSql += "inner join sisSocial_db..bairro_tb b ";
                sSql += "on a.bairro = b.cod_bairro ";
                sSql += "where nome_bairro = '" + pcodBairro + "'";

                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                return ds;

            }
            catch (Exception )
            {
                return ds;
            }

        }

        public DataSet BuscarAssistidoPorData(string data)
        {
            try
            {
                DataSet ds = new DataSet();
                string sSql = "";
                sSql += "select a.codigo, a.nome,a.cpf, a.rg, a.dtNascimento, a.endereco, a.telefone, a.profissao, a.dtCadastro  ";
                sSql += "from sisSocial_db..assistido_tb a ";
                sSql += $"where convert(date,dtCadastro) = '{data}'";
                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool InsereAnotacao(string pCodigoFicha, string pTexto, DateTime pDataAnotacao)
        {
            try
            {
                pTexto = pTexto.Replace("'", "");

                string sSql = "";
                sSql += "insert into sissocial_db..acompanhamento_tb (numeroFicha,dataAcompanhamento,observacao)";
                sSql += "values ('" + pCodigoFicha + "','" + pDataAnotacao + "','" + pTexto + "' )";

                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                return true;

            }

            catch (Exception )
            {
                return false;
            }
        }

        public DataSet BuscaAnotacao(string pNumFicha)
        {
            DataSet ds = new DataSet();

            try
            {
                string sSql = "";
                sSql += "select * from sisSocial_db..acompanhamento_tb ";
                sSql += "where numeroFicha = '" + pNumFicha + "' ";

                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                return ds;

            }
            catch (Exception )
            {
                return ds;
            }

        }

        public DataSet BuscaAnotacaoPorData(string pNumFicha, DateTime pDtAnotacao)
        {
            DataSet ds = new DataSet();

            try
            {
                string sSql = "";
                sSql += "select * from sisSocial_db..acompanhamento_tb ";
                sSql += "where numeroFicha = '" + pNumFicha + "' ";
                sSql += "and convert(date,dataAcompanhamento) = '" + pDtAnotacao + "' ";

                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                return ds;

            }
            catch (Exception )
            {
                return ds;
            }

        }

        public DataTable BuscarBairros(int _cod)
        {
            DataSet ds = new DataSet();

            try
            {
                String sSql = "";
                sSql += "select cod_bairro, nome_bairro ";
                sSql += "from sissocial_db..bairro_tb with (nolock) ";
                if (_cod != 0)
                {
                    sSql += " where cod_bairro = " + _cod + " order by nome_bairro";
                }

                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                return ds.Tables[0];

            }
            catch (Exception )
            {
                return ds.Tables[0];
            }
        }

    }
}
