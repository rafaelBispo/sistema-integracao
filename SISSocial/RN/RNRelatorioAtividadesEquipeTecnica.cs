﻿using SISSocial.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util;

namespace SISSocial.RN
{
    internal class RNRelatorioAtividadesEquipeTecnica
    {

        private ConnectionHelper ConnectionHelper = new ConnectionHelper();

        public DataSet SelectRelatorioAtividadesEquipeTecnica()
        {
            try
            {
                DataSet ds = new DataSet();
                string sSql = $" select 0 as id,'Selecione...' as NomeServico,null as Nome,null as Funcao," +
                                $"null as MesRef,null as DataEntrega,null as Descricao,null as PublicoAlvo," +
                                $"null as PessoasAtendidas,null as Periodicidade,null as RecursosHumanos," +
                                $"null as Abrangencia,null as Resultados,null as RecursosFinanceiros," +
                                $"null as Destacar,null as Parcerias " +
                                $"union " +
                                $"select * from sisSocial_db..RelatorioAtividadesEquipeTecnica_tb ";
                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool InsereRelatorioAtividadesEquipeTecnica(RelatorioAtividadesEquipeTecnica pEquipeTecnica)
        {
            try
            {
                string sSql = $"exec sisSocial_db..RelatorioAtividadesEquipeTecnica_spi " +
                                $"@NomeServico = '{pEquipeTecnica.NomeServico}', " +
                                $"@Nome = '{pEquipeTecnica.Nome}', " +
                                $"@Funcao = '{pEquipeTecnica.Funcao}', " +
                                $"@MesRef = '{pEquipeTecnica.MesRef}', " +
                                $"@DataEntrega = '{pEquipeTecnica.DataEntrega}', " +
                                $"@Descricao = '{pEquipeTecnica.Descricao}', " +
                                $"@PublicoAlvo = '{pEquipeTecnica.PublicoAlvo}', " +
                                $"@PessoasAtendidas = '{pEquipeTecnica.PessoasAtendidas}', " +
                                $"@Periodicidade = '{pEquipeTecnica.Periodicidade}', " +
                                $"@RecursosHumanos = '{pEquipeTecnica.RecursosHumanos}', " +
                                $"@Abrangencia = '{pEquipeTecnica.Abrangencia}', " +
                                $"@Resultados = '{pEquipeTecnica.Resultados}', " +
                                $"@RecursosFinanceiros = '{pEquipeTecnica.RecursosFinanceiros}', " +
                                $"@Destacar = '{pEquipeTecnica.Abrangencia}', " +
                                $"@Parcerias = '{pEquipeTecnica.Parcerias}' ";
                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                return true;
            }

            catch (Exception)
            {
                return false;
            }
        }


        public bool UpdateRelatorioAtividadesEquipeTecnica(RelatorioAtividadesEquipeTecnica pEquipeTecnica)
        {
            try
            {
                string sSql = $"exec sisSocial_db..RelatorioAtividadesEquipeTecnica_spu " +
                                $"@NomeServico = '{pEquipeTecnica.NomeServico}', " +
                                $"@Nome = '{pEquipeTecnica.Nome}', " +
                                $"@Funcao = '{pEquipeTecnica.Funcao}', " +
                                $"@MesRef = '{pEquipeTecnica.MesRef}', " +
                                $"@DataEntrega = '{pEquipeTecnica.DataEntrega}', " +
                                $"@Descricao = '{pEquipeTecnica.Descricao}', " +
                                $"@PublicoAlvo = '{pEquipeTecnica.PublicoAlvo}', " +
                                $"@PessoasAtendidas = '{pEquipeTecnica.PessoasAtendidas}', " +
                                $"@Periodicidade = '{pEquipeTecnica.Periodicidade}', " +
                                $"@RecursosHumanos = '{pEquipeTecnica.RecursosHumanos}', " +
                                $"@Abrangencia = '{pEquipeTecnica.Abrangencia}', " +
                                $"@Resultados = '{pEquipeTecnica.Resultados}', " +
                                $"@RecursosFinanceiros = '{pEquipeTecnica.RecursosFinanceiros}', " +
                                $"@Destacar = '{pEquipeTecnica.Abrangencia}', " +
                                $"@Parcerias = '{pEquipeTecnica.Parcerias}', " +
                                $"@Id = '{pEquipeTecnica.Id}' ";

                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                return true;
            }

            catch (Exception)
            {
                return false;
            }
        }

    }
}
