﻿using SISSocial.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Util;

namespace SISSocial.RN
{
    internal class RNRelatorioIntervencoesDiarias
    {
        private ConnectionHelper ConnectionHelper = new ConnectionHelper();

        public DataSet SelectRelatorioIntervencoesDiarias()
        {
            try
            {
                DataSet ds = new DataSet();
                string sSql = $"select * from sisSocial_db..RelatorioIntervencaoDiario_tb ";
                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);
                return ds;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool InsereRelatorioAtividadesEquipeTecnica(RelatorioIntervencoesDiarias pIntervencoes)
        {
            try
            {
                string sSql = $"exec sisSocial_db..RelatorioIntervencaoDiario_spi " +
                                $"@TecnicoResponsavel  = '{pIntervencoes.TecnicoResponsavel}', " +
                                $"@Data                = '{pIntervencoes.Data}', " +
                                $"@ParaEncaminhamentos1 = '{pIntervencoes.ParaEncaminhamentos1}', " +
                                $"@ParaEncaminhamentos2 = '{pIntervencoes.ParaEncaminhamentos2}', " +
                                $"@ParaEncaminhamentos3 = '{pIntervencoes.ParaEncaminhamentos3}', " +
                                $"@ParaEncaminhamentos4 = '{pIntervencoes.ParaEncaminhamentos4}', " +
                                $"@QtdEncaminhamentos1 = '{pIntervencoes.QtdEncaminhamentos1}', " +
                                $"@QtdEncaminhamentos2 = '{pIntervencoes.QtdEncaminhamentos2}', " +
                                $"@QtdEncaminhamentos3 = '{pIntervencoes.QtdEncaminhamentos3}', " +
                                $"@QtdEncaminhamentos4 = '{pIntervencoes.QtdEncaminhamentos4}', " +
                                $"@ParaRelatorios1     = '{pIntervencoes.ParaRelatorios1}', " +
                                $"@ParaRelatorios2     = '{pIntervencoes.ParaRelatorios2}', " +
                                $"@ParaRelatorios3     = '{pIntervencoes.ParaRelatorios3}', " +
                                $"@ParaRelatorios4     = '{pIntervencoes.ParaRelatorios4}', " +
                                $"@QtdRelatorios1      = '{pIntervencoes.QtdRelatorios1}', " +
                                $"@QtdRelatorios2      = '{pIntervencoes.QtdRelatorios2}', " +
                                $"@QtdRelatorios3      = '{pIntervencoes.QtdRelatorios3}', " +
                                $"@QtdRelatorios4      = '{pIntervencoes.QtdRelatorios4}', " +
                                $"@ParaVisitas1        = '{pIntervencoes.ParaVisitas1}', " +
                                $"@ParaVisitas2        = '{pIntervencoes.ParaVisitas2}', " +
                                $"@ParaVisitas3        = '{pIntervencoes.ParaVisitas3}', " +
                                $"@ParaVisitas4        = '{pIntervencoes.ParaVisitas4}', " +
                                $"@QtdVisitas1         = '{pIntervencoes.QtdVisitas1}', " +
                                $"@QtdVisitas2         = '{pIntervencoes.QtdVisitas2}', " +
                                $"@QtdVisitas3         = '{pIntervencoes.QtdVisitas3}', " +
                                $"@QtdVisitas4         = '{pIntervencoes.QtdVisitas4}', " +
                                $"@ProntuariosNovo1    = '{pIntervencoes.ProntuariosNovo1}', " +
                                $"@ProntuariosNovo2    = '{pIntervencoes.ProntuariosNovo2}', " +
                                $"@ProntuariosNovo3    = '{pIntervencoes.ProntuariosNovo3}', " +
                                $"@ProntuariosNovo4    = '{pIntervencoes.ProntuariosNovo4}', " +
                                $"@ProntuariosNovo5    = '{pIntervencoes.ProntuariosNovo5}', " +
                                $"@ProntuariosNovo6    = '{pIntervencoes.ProntuariosNovo6}', " +
                                $"@ProntuariosNovo7    = '{pIntervencoes.ProntuariosNovo7}', " +
                                $"@ProntuariosNovo8    = '{pIntervencoes.ProntuariosNovo8}', " +
                                $"@ProntuariosNovo9    = '{pIntervencoes.ProntuariosNovo9}', " +
                                $"@ProntuariosNovo10   = '{pIntervencoes.ProntuariosNovo10}', " +
                                $"@NumProntuarioNovos1 = '{pIntervencoes.NumProntuarioNovos1}', " +
                                $"@NumProntuarioNovos2 = '{pIntervencoes.NumProntuarioNovos2}', " +
                                $"@NumProntuarioNovos3 = '{pIntervencoes.NumProntuarioNovos3}', " +
                                $"@NumProntuarioNovos4 = '{pIntervencoes.NumProntuarioNovos4}', " +
                                $"@NumProntuarioNovos5 = '{pIntervencoes.NumProntuarioNovos5}', " +
                                $"@NumProntuarioNovos6 = '{pIntervencoes.NumProntuarioNovos6}', " +
                                $"@NumProntuarioNovos7 = '{pIntervencoes.NumProntuarioNovos7}', " +
                                $"@NumProntuarioNovos8 = '{pIntervencoes.NumProntuarioNovos8}', " +
                                $"@NumProntuarioNovos9 = '{pIntervencoes.NumProntuarioNovos9}', " +
                                $"@NumProntuarioNovos10 = '{pIntervencoes.NumProntuarioNovos10}', " +
                                $"@ReincidentesNome1   = '{pIntervencoes.ReincidentesNome1}', " +
                                $"@ReincidentesNome2   = '{pIntervencoes.ReincidentesNome2}', " +
                                $"@ReincidentesNome3   = '{pIntervencoes.ReincidentesNome3}', " +
                                $"@ReincidentesNome4   = '{pIntervencoes.ReincidentesNome4}', " +
                                $"@ReincidentesNome5   = '{pIntervencoes.ReincidentesNome5}', " +
                                $"@ReincidentesNome6   = '{pIntervencoes.ReincidentesNome6}', " +
                                $"@ReincidentesNome7   = '{pIntervencoes.ReincidentesNome7}', " +
                                $"@ReincidentesNome8   = '{pIntervencoes.ReincidentesNome8}', " +
                                $"@ReincidentesNome9   = '{pIntervencoes.ReincidentesNome9}', " +
                                $"@ReincidentesNome10  = '{pIntervencoes.ReincidentesNome10}', " +
                                $"@ReincidentesNum1    = '{pIntervencoes.ReincidentesNum1}', " +
                                $"@ReincidentesNum2    = '{pIntervencoes.ReincidentesNum2}', " +
                                $"@ReincidentesNum3    = '{pIntervencoes.ReincidentesNum3}', " +
                                $"@ReincidentesNum4    = '{pIntervencoes.ReincidentesNum4}', " +
                                $"@ReincidentesNum5    = '{pIntervencoes.ReincidentesNum5}', " +
                                $"@ReincidentesNum6    = '{pIntervencoes.ReincidentesNum6}', " +
                                $"@ReincidentesNum7    = '{pIntervencoes.ReincidentesNum7}', " +
                                $"@ReincidentesNum8    = '{pIntervencoes.ReincidentesNum8}', " +
                                $"@ReincidentesNum9    = '{pIntervencoes.ReincidentesNum9}', " +
                                $"@ReincidentesNum10   = '{pIntervencoes.ReincidentesNum10}', " +
                                $"@DesligadosNome1     = '{pIntervencoes.DesligadosNome1}', " +
                                $"@DesligadosNome2     = '{pIntervencoes.DesligadosNome2}', " +
                                $"@DesligadosNome3     = '{pIntervencoes.DesligadosNome3}', " +
                                $"@DesligadosNome4     = '{pIntervencoes.DesligadosNome4}', " +
                                $"@DesligadosNome5     = '{pIntervencoes.DesligadosNome5}', " +
                                $"@DesligadosNome6     = '{pIntervencoes.DesligadosNome6}', " +
                                $"@DesligadosNome7     = '{pIntervencoes.DesligadosNome7}', " +
                                $"@DesligadosNome8     = '{pIntervencoes.DesligadosNome8}', " +
                                $"@DesligadosNome9     = '{pIntervencoes.DesligadosNome9}', " +
                                $"@DesligadosNome10    = '{pIntervencoes.DesligadosNome10}', " +
                                $"@DesligadosNum1      = '{pIntervencoes.DesligadosNum1}', " +
                                $"@DesligadosNum2      = '{pIntervencoes.DesligadosNum2}', " +
                                $"@DesligadosNum3      = '{pIntervencoes.DesligadosNum3}', " +
                                $"@DesligadosNum4      = '{pIntervencoes.DesligadosNum4}', " +
                                $"@DesligadosNum5      = '{pIntervencoes.DesligadosNum5}', " +
                                $"@DesligadosNum6      = '{pIntervencoes.DesligadosNum6}', " +
                                $"@DesligadosNum7      = '{pIntervencoes.DesligadosNum7}', " +
                                $"@DesligadosNum8      = '{pIntervencoes.DesligadosNum8}', " +
                                $"@DesligadosNum9      = '{pIntervencoes.DesligadosNum9}', " +
                                $"@DesligadosNum10     = '{pIntervencoes.DesligadosNum10}' ";
                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                return true;
            }

            catch (Exception)
            {
                return false;
            }
        }


        public bool UpdateRelatorioAtividadesEquipeTecnica(RelatorioIntervencoesDiarias pIntervencoes)
        {
            try
            {
                string sSql = $"exec sisSocial_db..RelatorioIntervencaoDiario_spu " +
                                $"@TecnicoResponsavel  = '{pIntervencoes.TecnicoResponsavel}', " +
                                $"@Data                = '{pIntervencoes.Data}', " +
                                $"@ParaEncaminhamentos1 = '{pIntervencoes.ParaEncaminhamentos1}', " +
                                $"@ParaEncaminhamentos2 = '{pIntervencoes.ParaEncaminhamentos2}', " +
                                $"@ParaEncaminhamentos3 = '{pIntervencoes.ParaEncaminhamentos3}', " +
                                $"@ParaEncaminhamentos4 = '{pIntervencoes.ParaEncaminhamentos4}', " +
                                $"@QtdEncaminhamentos1 = '{pIntervencoes.QtdEncaminhamentos1}', " +
                                $"@QtdEncaminhamentos2 = '{pIntervencoes.QtdEncaminhamentos2}', " +
                                $"@QtdEncaminhamentos3 = '{pIntervencoes.QtdEncaminhamentos3}', " +
                                $"@QtdEncaminhamentos4 = '{pIntervencoes.QtdEncaminhamentos4}', " +
                                $"@ParaRelatorios1     = '{pIntervencoes.ParaRelatorios1}', " +
                                $"@ParaRelatorios2     = '{pIntervencoes.ParaRelatorios2}', " +
                                $"@ParaRelatorios3     = '{pIntervencoes.ParaRelatorios3}', " +
                                $"@ParaRelatorios4     = '{pIntervencoes.ParaRelatorios4}', " +
                                $"@QtdRelatorios1      = '{pIntervencoes.QtdRelatorios1}', " +
                                $"@QtdRelatorios2      = '{pIntervencoes.QtdRelatorios2}', " +
                                $"@QtdRelatorios3      = '{pIntervencoes.QtdRelatorios3}', " +
                                $"@QtdRelatorios4      = '{pIntervencoes.QtdRelatorios4}', " +
                                $"@ParaVisitas1        = '{pIntervencoes.ParaVisitas1}', " +
                                $"@ParaVisitas2        = '{pIntervencoes.ParaVisitas2}', " +
                                $"@ParaVisitas3        = '{pIntervencoes.ParaVisitas3}', " +
                                $"@ParaVisitas4        = '{pIntervencoes.ParaVisitas4}', " +
                                $"@QtdVisitas1         = '{pIntervencoes.QtdVisitas1}', " +
                                $"@QtdVisitas2         = '{pIntervencoes.QtdVisitas2}', " +
                                $"@QtdVisitas3         = '{pIntervencoes.QtdVisitas3}', " +
                                $"@QtdVisitas4         = '{pIntervencoes.QtdVisitas4}', " +
                                $"@ProntuariosNovo1    = '{pIntervencoes.ProntuariosNovo1}', " +
                                $"@ProntuariosNovo2    = '{pIntervencoes.ProntuariosNovo2}', " +
                                $"@ProntuariosNovo3    = '{pIntervencoes.ProntuariosNovo3}', " +
                                $"@ProntuariosNovo4    = '{pIntervencoes.ProntuariosNovo4}', " +
                                $"@ProntuariosNovo5    = '{pIntervencoes.ProntuariosNovo5}', " +
                                $"@ProntuariosNovo6    = '{pIntervencoes.ProntuariosNovo6}', " +
                                $"@ProntuariosNovo7    = '{pIntervencoes.ProntuariosNovo7}', " +
                                $"@ProntuariosNovo8    = '{pIntervencoes.ProntuariosNovo8}', " +
                                $"@ProntuariosNovo9    = '{pIntervencoes.ProntuariosNovo9}', " +
                                $"@ProntuariosNovo10   = '{pIntervencoes.ProntuariosNovo10}', " +
                                $"@NumProntuarioNovos1 = '{pIntervencoes.NumProntuarioNovos1}', " +
                                $"@NumProntuarioNovos2 = '{pIntervencoes.NumProntuarioNovos2}', " +
                                $"@NumProntuarioNovos3 = '{pIntervencoes.NumProntuarioNovos3}', " +
                                $"@NumProntuarioNovos4 = '{pIntervencoes.NumProntuarioNovos4}', " +
                                $"@NumProntuarioNovos5 = '{pIntervencoes.NumProntuarioNovos5}', " +
                                $"@NumProntuarioNovos6 = '{pIntervencoes.NumProntuarioNovos6}', " +
                                $"@NumProntuarioNovos7 = '{pIntervencoes.NumProntuarioNovos7}', " +
                                $"@NumProntuarioNovos8 = '{pIntervencoes.NumProntuarioNovos8}', " +
                                $"@NumProntuarioNovos9 = '{pIntervencoes.NumProntuarioNovos9}', " +
                                $"@NumProntuarioNovos10 = '{pIntervencoes.NumProntuarioNovos10}', " +
                                $"@ReincidentesNome1   = '{pIntervencoes.ReincidentesNome1}', " +
                                $"@ReincidentesNome2   = '{pIntervencoes.ReincidentesNome2}', " +
                                $"@ReincidentesNome3   = '{pIntervencoes.ReincidentesNome3}', " +
                                $"@ReincidentesNome4   = '{pIntervencoes.ReincidentesNome4}', " +
                                $"@ReincidentesNome5   = '{pIntervencoes.ReincidentesNome5}', " +
                                $"@ReincidentesNome6   = '{pIntervencoes.ReincidentesNome6}', " +
                                $"@ReincidentesNome7   = '{pIntervencoes.ReincidentesNome7}', " +
                                $"@ReincidentesNome8   = '{pIntervencoes.ReincidentesNome8}', " +
                                $"@ReincidentesNome9   = '{pIntervencoes.ReincidentesNome9}', " +
                                $"@ReincidentesNome10  = '{pIntervencoes.ReincidentesNome10}', " +
                                $"@ReincidentesNum1    = '{pIntervencoes.ReincidentesNum1}', " +
                                $"@ReincidentesNum2    = '{pIntervencoes.ReincidentesNum2}', " +
                                $"@ReincidentesNum3    = '{pIntervencoes.ReincidentesNum3}', " +
                                $"@ReincidentesNum4    = '{pIntervencoes.ReincidentesNum4}', " +
                                $"@ReincidentesNum5    = '{pIntervencoes.ReincidentesNum5}', " +
                                $"@ReincidentesNum6    = '{pIntervencoes.ReincidentesNum6}', " +
                                $"@ReincidentesNum7    = '{pIntervencoes.ReincidentesNum7}', " +
                                $"@ReincidentesNum8    = '{pIntervencoes.ReincidentesNum8}', " +
                                $"@ReincidentesNum9    = '{pIntervencoes.ReincidentesNum9}', " +
                                $"@ReincidentesNum10   = '{pIntervencoes.ReincidentesNum10}', " +
                                $"@DesligadosNome1     = '{pIntervencoes.DesligadosNome1}', " +
                                $"@DesligadosNome2     = '{pIntervencoes.DesligadosNome2}', " +
                                $"@DesligadosNome3     = '{pIntervencoes.DesligadosNome3}', " +
                                $"@DesligadosNome4     = '{pIntervencoes.DesligadosNome4}', " +
                                $"@DesligadosNome5     = '{pIntervencoes.DesligadosNome5}', " +
                                $"@DesligadosNome6     = '{pIntervencoes.DesligadosNome6}', " +
                                $"@DesligadosNome7     = '{pIntervencoes.DesligadosNome7}', " +
                                $"@DesligadosNome8     = '{pIntervencoes.DesligadosNome8}', " +
                                $"@DesligadosNome9     = '{pIntervencoes.DesligadosNome9}', " +
                                $"@DesligadosNome10    = '{pIntervencoes.DesligadosNome10}', " +
                                $"@DesligadosNum1      = '{pIntervencoes.DesligadosNum1}', " +
                                $"@DesligadosNum2      = '{pIntervencoes.DesligadosNum2}', " +
                                $"@DesligadosNum3      = '{pIntervencoes.DesligadosNum3}', " +
                                $"@DesligadosNum4      = '{pIntervencoes.DesligadosNum4}', " +
                                $"@DesligadosNum5      = '{pIntervencoes.DesligadosNum5}', " +
                                $"@DesligadosNum6      = '{pIntervencoes.DesligadosNum6}', " +
                                $"@DesligadosNum7      = '{pIntervencoes.DesligadosNum7}', " +
                                $"@DesligadosNum8      = '{pIntervencoes.DesligadosNum8}', " +
                                $"@DesligadosNum9      = '{pIntervencoes.DesligadosNum9}', " +
                                $"@DesligadosNum10     = '{pIntervencoes.DesligadosNum10}' ";

                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql);

                return true;
            }

            catch (Exception)
            {
                return false;
            }
        }
    }
}
