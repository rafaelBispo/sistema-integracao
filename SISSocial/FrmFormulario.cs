﻿using Microsoft.Office.Interop.Word;
using SISSocial.Classes;
using SISSocial.RN;
using SISSocial.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DataTable = System.Data.DataTable;

namespace SISSocial
{
    public partial class FrmFormulario : Form
    {
        public string usuarioLogado = "";
        public bool clickNovo = false;
        public string numFicha = "";
        private string escolaridade = string.Empty;
        private string problemaSaude = string.Empty;
        private string temFilhos = string.Empty;
        private string recebePensao = string.Empty;
        private string maeApoioPais = string.Empty;
        private string maePresente = string.Empty;
        private string idadeMae = string.Empty;
        private string recebeBolsaFamilia = string.Empty;
        private string recebeLoas = string.Empty;
        private string possuiDeficiencia = string.Empty;
        private string dadosCasa = string.Empty;
        private string possuiFonteRenda = string.Empty;
        private string auxilioPrevidenciario = string.Empty;
        private string cadastroCras = string.Empty;
        private string cadastroCrasComposicao = string.Empty;

        private string escolaridadeCF = string.Empty;
        private string problemaSaudeCF = string.Empty;
        private string restricaoAtividadeCF = string.Empty;
        private string usaMedicamentoCF = string.Empty;
        private string possuiDeficienciaCF = string.Empty;
        private string alergiaAlimentoCF = string.Empty;
        private string outrasSCFVCF = string.Empty;
        private string fonteRendaCF = string.Empty;
        private string acompanhamentoTexto = string.Empty;
        DataSet dsHistoricoAnotacoes = new DataSet();
        private bool usuarioJaSalvo = false;

        public FrmFormulario()
        {
            InitializeComponent();
        }

        #region "Logica RadioButton"


        private void rdFilhosSim_CheckedChanged(object sender, EventArgs e)
        {
            if (rdFilhosSim.Checked == true)
            {
                lblQtdFilhos.Visible = true;
                txtQtdFilhos.Visible = true;
            }
            else
            {
                lblQtdFilhos.Visible = false;
                txtQtdFilhos.Visible = false;
            }
        }

        private void rdProbSaudeSim_CheckedChanged(object sender, EventArgs e)
        {
            if (rdProbSaudeSim.Checked == true)
            {
                lblQualDoenca.Visible = true;
                txtQualDoenca.Visible = true;
            }
            else
            {
                lblQualDoenca.Visible = false;
                txtQualDoenca.Visible = false;
            }
        }

        private void rbBolsaFamiliaSim_CheckedChanged(object sender, EventArgs e)
        {
            if (rdBolsaFamiliaSim.Checked == true)
            {
                lblValorBolsaFamilia.Visible = true;
                txtvalorBolsaFamilia.Visible = true;
            }
            else
            {
                lblValorBolsaFamilia.Visible = false;
                txtvalorBolsaFamilia.Visible = false;
            }
        }

        private void rdAlugada_CheckedChanged(object sender, EventArgs e)
        {
            if (rdAlugada.Checked == true)
            {
                lblValorAluguel.Visible = true;
                txtValorAluguel.Visible = true;
            }
            else
            {
                lblValorAluguel.Visible = false;
                txtValorAluguel.Visible = false;
            }
        }

        private void rdCarteiraAssinadaSim_CheckedChanged(object sender, EventArgs e)
        {
            if (rdCarteiraAssinadaSim.Checked == true)
            {
                lblProfissao.Visible = true;
                txtProfissao.Visible = true;
            }
            else
            {
                lblProfissao.Visible = false;
                txtProfissao.Visible = false;
            }
        }

        private void rdLoasSim_CheckedChanged(object sender, EventArgs e)
        {
            if (rdLoasSim.Checked == true)
            {
                lblLoas.Visible = true;
                txtLoas.Visible = true;
            }
            else
            {
                lblLoas.Visible = false;
                txtLoas.Visible = false;
            }
        }

        private void rdCarteiraAssinadaSimComposicao_CheckedChanged(object sender, EventArgs e)
        {
            if (rdCarteiraAssinadaSimCF.Checked == true)
            {
                lblProfissaoComposicao.Visible = true;
                txtProfissaoComposicao.Visible = true;
            }
            else
            {
                lblProfissaoComposicao.Visible = false;
                txtProfissaoComposicao.Visible = false;
            }
        }


        #endregion

        #region "VALIDAÇÕES"

        bool validaCamposAssistido()
        {
            try
            {
                if (txtCPF.Text == "" || txtEndereco.Text == "" || txtRG.Text == "" || txtNome.Text == "")
                {
                    MessageBox.Show("Por favor preencher os campos obrigatórios!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                    return false;
                }

                if (cboSexo.SelectedIndex == 0)
                {
                    MessageBox.Show("Por favor selecionar o sexo", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                    cboSexo.Focus();
                    return false;
                }
                if (cboEstadoCivil.SelectedIndex == 0)
                {
                    MessageBox.Show("Por favor selecionar o estado cívil", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                    cboEstadoCivil.Focus();
                    return false;
                }
                if (cboOrientacao.SelectedIndex <= 0)
                {
                    MessageBox.Show("Por favor selecionar o encaminhamento", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                    cboOrientacao.Focus();
                    return false;
                }
                else
                {
                    return true;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return false;
            }
        }

        bool validaCamposComposicaoFamiliar()
        {
            try
            {
                if (txtNomeComposicao.Text == "")
                {
                    MessageBox.Show("Por favor informe o nome", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                    cbSexoComposicao.Focus();
                    return false;
                }

                if (cbSexoComposicao.SelectedIndex == 0)
                {
                    MessageBox.Show("Por favor selecionar o sexo", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                    cbSexoComposicao.Focus();
                    return false;
                }
                if (cbEstadoCivilComposicao.SelectedIndex == 0)
                {
                    MessageBox.Show("Por favor selecionar o estado cívil", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                    cbEstadoCivilComposicao.Focus();
                    return false;
                }
                else
                {
                    return true;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return false;
            }
        }
        public int ValidaEscolaridade()
        {
            int opcao = -1;
            try
            {
                if (rdEnsFundamentalComp.Checked)
                    opcao = 0;
                else if (rdEnsFundamentalincomp.Checked)
                    opcao = 1;
                else if (rdEnsMedioComp.Checked)
                    opcao = 2;
                else if (rdEnsMedioIncomp.Checked)
                    opcao = 3;
                else if (rdEnsSuperiorComp.Checked)
                    opcao = 4;
                else if (rdEnsSuperiorIncomp.Checked)
                    opcao = 5;
                else if (rdPrimario.Checked)
                    opcao = 6;
                else if (rdAnalfabeto.Checked)
                    opcao = 7;

                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public void CarregaEscolaridade(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                {
                    rdEnsFundamentalComp.Checked = true;
                    escolaridade = rdEnsFundamentalComp.Text;
                }
                else if (opcao == 1)
                {
                    rdEnsFundamentalincomp.Checked = true;
                    escolaridade = rdEnsFundamentalincomp.Text;
                }
                else if (opcao == 2)
                {
                    rdEnsMedioComp.Checked = true;
                    escolaridade = rdEnsMedioComp.Text;
                }
                else if (opcao == 3)
                {
                    rdEnsMedioIncomp.Checked = true;
                    escolaridade = rdEnsMedioIncomp.Text;
                }
                else if (opcao == 4)
                {
                    rdEnsSuperiorComp.Checked = true;
                    escolaridade = rdEnsSuperiorComp.Text;
                }
                else if (opcao == 5)
                {
                    rdEnsSuperiorIncomp.Checked = true;
                    escolaridade = rdEnsSuperiorIncomp.Text;
                }
                else if (opcao == 6)
                {
                    rdPrimario.Checked = true;
                    escolaridade = rdPrimario.Text;
                }
                else if (opcao == 7)
                {
                    rdAnalfabeto.Checked = true;
                    escolaridade = rdAnalfabeto.Text;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        public int ValidaProblemaSaude()
        {
            int opcao = -1;
            try
            {
                if (rdProbSaudeSim.Checked)
                {
                    opcao = 0;         
                }
                else if (rdProbSaudeNao.Checked)
                {
                    opcao = 1;
                }
                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public void CarregaProblemaSaude(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                {
                    rdProbSaudeSim.Checked = true;
                    problemaSaude = "Sim";
                }
                else if (opcao == 1)
                {
                    rdProbSaudeNao.Checked = true;
                    problemaSaude = "Não";
                }
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        public void CarregaAlergiaAlimentoComposicao(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                {
                    rdAlergiaAlimentoSimCF.Checked = true;
                    alergiaAlimentoCF = "Sim";
                }
                    
                else if (opcao == 1)
                {
                    rdAlergiaAlimentoNaoCF.Checked = true;
                    alergiaAlimentoCF = "Não";
                }
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        public void CarregaUsoMedicamentoComposicao(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                {
                    rdUsoMedicamentoSimCF.Checked = true;
                    usaMedicamentoCF = "Sim";
                }
                else if (opcao == 1)
                {
                    rdUsoMedicamentoNaoCF.Checked = true;
                    usaMedicamentoCF = "Não";
                }
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        public void CarregaAtividadeAlemSCFVComposicao(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                {
                    rdAtividadesAlemScfvSimCF.Checked = true;
                    outrasSCFVCF = "Sim";
                }
                    
                else if (opcao == 1)
                {
                    rdAtividadesAlemScfvNaoCF.Checked = true;
                    outrasSCFVCF = "Não";
                }
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        public void CarregaRestricaoParticiparAtividadeComposicao(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                {
                    rdAlgumaAtividadeSimCF.Checked = true;
                    restricaoAtividadeCF = "Sim";
                }
                    
                else if (opcao == 1)
                {
                    rdAlgumaAtividadeNaoCF.Checked = true;
                    restricaoAtividadeCF = "Não";
                }
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        public void CarregaCadastroCrasComposicao(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                {
                    rdCadastroCrasSimCF.Checked = true;
                    cadastroCrasComposicao = "Sim";
                }
                    
                else if (opcao == 1)
                {
                    rdCadastroCrasNaoCF.Checked = true;
                    cadastroCrasComposicao = "Não";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        public int ValidaPossuiDeficiencia()
        {
            int opcao = -1;
            try
            {
                if (rdPossuiDeficienciaSim.Checked)
                    opcao = 0;
                else if (rdPossuiDeficienciaNao.Checked)
                    opcao = 1;

                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public void CarregaPossuiDeficiencia(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                {
                    rdPossuiDeficienciaSim.Checked = true;
                    possuiDeficiencia = "Sim";
                }
                    
                else if (opcao == 1)
                {
                    rdPossuiDeficienciaNao.Checked = true;
                    possuiDeficiencia = "Não";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        public int ValidaIdade()
        {
            int opcao = -1;
            try
            {
                if (rdIdade0a6.Checked)
                    opcao = 0;
                else if (rdIdade7a11.Checked)
                    opcao = 1;
                else if (rdIdade12a14.Checked)
                    opcao = 2;
                else if (rdIdade15a17.Checked)
                    opcao = 3;
                else if (rdIdade18a29.Checked)
                    opcao = 4;
                else if (rdIdade30a59.Checked)
                    opcao = 5;
                else if (rdIdadeMaior60.Checked)
                    opcao = 6;


                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public void CarregaIdade(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                    rdIdade0a6.Checked = true;
                else if (opcao == 1)
                    rdIdade7a11.Checked = true;
                else if (opcao == 2)
                    rdIdade12a14.Checked = true;
                else if (opcao == 3)
                    rdIdade15a17.Checked = true;
                else if (opcao == 4)
                    rdIdade18a29.Checked = true;
                else if (opcao == 5)
                    rdIdade30a59.Checked = true;
                else if (opcao == 6)
                    rdIdadeMaior60.Checked = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        public int ValidaFilhos()
        {
            int opcao = -1;
            try
            {
                if (rdFilhosSim.Checked)
                {
                    opcao = 0;
                }
                else if (rdFilhosNao.Checked)
                {
                    opcao = 1;
                }


                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public void CarregaFilhos(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                {
                    rdFilhosSim.Checked = true;
                    temFilhos = rdFilhosSim.Text;
                }
                    
                else if (opcao == 1)
                {
                    rdFilhosNao.Checked = true;
                    temFilhos = rdFilhosNao.Text;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        public int ValidaRecebePensao()
        {
            int opcao = -1;
            try
            {
                if (rdRecebePensaoSim.Checked)
                {
                    opcao = 0;
                    recebePensao = "Sim";
                }
                    
                else if (rdRecebePensaoNao.Checked)
                {
                    opcao = 1;
                    recebePensao = "Não";
                }
                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public void CarregaRecebePensao(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                {
                    rdRecebePensaoSim.Checked = true;
                    recebePensao = "Sim";
                }
                else if (opcao == 1)
                {
                    rdRecebePensaoNao.Checked = true;
                    recebePensao = "Não";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        public int ValidaMaeCriaFilhosSemApoioPais()
        {
            int opcao = -1;
            try
            {
                if (rdMaeApoioPaisSim.Checked)
                    opcao = 0;
                else if (rdMaeApoioPaisNao.Checked)
                    opcao = 1;

                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public void CarreagaMaeCriaFilhosSemApoioPais(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                {
                    rdMaeApoioPaisSim.Checked = true;
                    maeApoioPais = "Sim";
                }
                else if (opcao == 1)
                {
                    rdMaeApoioPaisNao.Checked = true;
                    maeApoioPais = "Não";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        public int ValidaMaePresenteFamilia()
        {
            int opcao = -1;
            try
            {
                if (rdMaePresenteSim.Checked)
                    opcao = 0;
                else if (rdMaePresenteNao.Checked)
                    opcao = 1;

                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public void CarregaMaePresenteFamilia(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                {
                    rdMaePresenteSim.Checked = true;
                    maePresente = "Sim";
                }
                else if (opcao == 1)
                {
                    rdMaePresenteNao.Checked = true;
                    maePresente = "Não";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        public int ValidaIdadeMae()
        {
            int opcao = -1;
            try
            {
                if (rdIdadeMaeMenor18.Checked)
                    opcao = 0;
                else if (rdIdadeMae18a21.Checked)
                    opcao = 1;
                else if (rdIdadeMaeMaior21.Checked)
                    opcao = 2;

                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public void CarregaIdadeMae(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                {
                    rdIdadeMaeMenor18.Checked = true;
                    idadeMae = rdIdadeMaeMenor18.Text;
                }
                else if (opcao == 1)
                {
                    rdIdadeMae18a21.Checked = true;
                    idadeMae = rdIdadeMae18a21.Text;
                }
                else if (opcao == 2)
                {
                    rdIdadeMaeMaior21.Checked = true;
                    idadeMae = rdIdadeMaeMaior21.Text;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        public int ValidaRecebeBolsaFamilia()
        {
            int opcao = -1;
            try
            {
                if (rdBolsaFamiliaSim.Checked)
                    opcao = 0;
                else if (rdBolsaFamiliaNao.Checked)
                    opcao = 1;

                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public void CarregaRecebeBolsaFamilia(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                {
                    rdBolsaFamiliaSim.Checked = true;
                    recebeBolsaFamilia = "Sim";
                }
                else if (opcao == 1)
                {
                    rdBolsaFamiliaNao.Checked = true;
                    recebeBolsaFamilia = "Não";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        public int ValidaRecebeLoas()
        {
            int opcao = -1;
            try
            {
                if (rdLoasSim.Checked)
                    opcao = 0;
                else if (rdLoasNao.Checked)
                    opcao = 1;

                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public void CarregaRecebeLoas(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                {
                    rdLoasSim.Checked = true;
                    recebeLoas = "Sim";
                }
                else if (opcao == 1)
                {
                    rdLoasNao.Checked = true;
                    recebeLoas = "Não";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        public int ValidaCasaE()
        {
            int opcao = -1;
            try
            {
                if (rdPropria.Checked)
                    opcao = 0;
                else if (rdCedida.Checked)
                    opcao = 1;
                else if (rdInvadida.Checked)
                    opcao = 2;
                else if (rdAlugada.Checked)
                    opcao = 3;

                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public void CarregaCasaE(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                {
                    rdPropria.Checked = true;
                    dadosCasa = rdPropria.Text;
                }
                    
                else if (opcao == 1)
                {
                    rdCedida.Checked = true;
                    dadosCasa = rdCedida.Text;
                }
                    
                else if (opcao == 2)
                {
                    rdInvadida.Checked = true;
                    dadosCasa = rdInvadida.Text;
                }
                    
                else if (opcao == 3)
                {
                    rdAlugada.Checked = true;
                    dadosCasa = rdAlugada.Text;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        public int ValidaCarteiraAssinada()
        {
            int opcao = -1;
            try
            {
                if (rdCarteiraAssinadaSim.Checked)
                    opcao = 0;
                else if (rdCarteiraAssinadaNao.Checked)
                    opcao = 1;

                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public void CarregaCarteiraAssinada(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                {
                    rdCarteiraAssinadaSim.Checked = true;
                    possuiFonteRenda = "Sim";
                }
                    
                else if (opcao == 1)
                {
                    rdCarteiraAssinadaNao.Checked = true;
                    possuiFonteRenda = "Não";
                }
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        public int ValidaAuxilioPrevidenciario()
        {
            int opcao = -1;
            try
            {
                if (rdAuxilioPrevSim.Checked)
                    opcao = 0;
                else if (rdAuxilioPrevNao.Checked)
                    opcao = 1;

                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public void CarregaAuxilioPrevidenciario(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                {
                    rdAuxilioPrevSim.Checked = true;
                    auxilioPrevidenciario = "Sim";
                }
                else if (opcao == 1)
                {
                    rdAuxilioPrevNao.Checked = true;
                    auxilioPrevidenciario = "Não";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        public int ValidaCadastroCras()
        {
            int opcao = -1;
            try
            {
                if (rdCrasSim.Checked)
                    opcao = 0;
                else if (rdCrasNao.Checked)
                    opcao = 1;

                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public void CarregaCadastroCras(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                {
                    rdCrasSim.Checked = true;
                    cadastroCras = "Sim";
                }
                else if (opcao == 1)
                {
                    rdCrasNao.Checked = true;
                    cadastroCras = "Não";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        public int ValidaPossuiDeficienciaComposicao()
        {
            int opcao = -1;
            try
            {
                if (rdDeficienciaSimComposicao.Checked)
                    opcao = 0;
                else if (rdDeficienciaNaoComposicao.Checked)
                    opcao = 1;

                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public void CarregaPossuiDeficienciaComposicao(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                {
                    rdDeficienciaSimComposicao.Checked = true;
                    possuiDeficienciaCF = "Sim";
                }
                else if (opcao == 1)
                {
                    rdDeficienciaNaoComposicao.Checked = true;
                    possuiDeficienciaCF = "Sim";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        public int ValidaCarteiraAssinadaComposicao()
        {
            int opcao = -1;
            try
            {
                if (rdCarteiraAssinadaSimCF.Checked)
                    opcao = 0;
                else if (rdCarteiraAssinadaNaoCF.Checked)
                    opcao = 1;

                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public int ValidaProblemaSaudeComposicao()
        {
            int opcao = -1;
            try
            {
                if (rdProblemaSaudeSimCF.Checked)
                    opcao = 0;
                else if (rdProblemaSaudeNaoCF.Checked)
                    opcao = 1;

                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public int ValidaAlergiaAlimentoComposicao()
        {
            int opcao = -1;
            try
            {
                if (rdAlergiaAlimentoSimCF.Checked)
                    opcao = 0;
                else if (rdAlergiaAlimentoNaoCF.Checked)
                    opcao = 1;

                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public int ValidaUsoMedicamentoComposicao()
        {
            int opcao = -1;
            try
            {
                if (rdUsoMedicamentoSimCF.Checked)
                    opcao = 0;
                else if (rdUsoMedicamentoNaoCF.Checked)
                    opcao = 1;

                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public int ValidaAtividadeAlemSCFVComposicao()
        {
            int opcao = -1;
            try
            {
                if (rdAtividadesAlemScfvSimCF.Checked)
                    opcao = 0;
                else if (rdAtividadesAlemScfvNaoCF.Checked)
                    opcao = 1;

                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public int ValidaRestricaoParticiparAtividade()
        {
            int opcao = -1;
            try
            {
                if (rdAlgumaAtividadeSimCF.Checked)
                    opcao = 0;
                else if (rdAlgumaAtividadeNaoCF.Checked)
                    opcao = 1;

                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public int ValidaCadastroCrasComposicao()
        {
            int opcao = -1;
            try
            {
                if (rdCadastroCrasSimCF.Checked)
                    opcao = 0;
                else if (rdCadastroCrasNaoCF.Checked)
                    opcao = 1;

                return opcao;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return opcao;
            }
        }

        public void CarregaCarteiraAssinadaComposicao(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                {
                    rdCarteiraAssinadaSimCF.Checked = true;
                    fonteRendaCF = "Sim";
                }
                else if (opcao == 1)
                {
                    rdCarteiraAssinadaNaoCF.Checked = true;
                    fonteRendaCF = "Não";
                }
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        public void CarregaProblemaSaudeComposicao(Int32 opcao)
        {
            try
            {
                if (opcao == 0)
                {
                    rdProblemaSaudeSimCF.Checked = true;
                    problemaSaudeCF = "Sim";
                }

                else if (opcao == 1)
                {
                    rdProblemaSaudeNaoCF.Checked = true;
                    problemaSaudeCF = "Sim";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        #endregion

        private void btnGravar_Click(object sender, EventArgs e)
        {
            try
            {
                Assistido assis = new Assistido();
                RNAssistidos rn = new RNAssistidos();
                DataSet ds = new DataSet();
                if (validaCamposAssistido() == false)
                {
                    return;
                }
                assis.Codigo = lblNumCadastro.Text;
                assis.Orientacao = cboOrientacao.SelectedIndex;
                assis.Nome = txtNome.Text;
                assis.Sexo = cboSexo.SelectedIndex;
                assis.EstadoCivil = cboEstadoCivil.SelectedIndex;
                assis.Cpf = txtCPF.Text;
                assis.Rg = txtRG.Text;
                assis.Nis = txtNis.Text;
                assis.DtNascimento = dtDataNasc.Value;
                assis.Endereco = txtEndereco.Text;
                assis.Telefone = txtTelefone.Text;
                assis.Mae = txtMae.Text;
                assis.Pai = txtPai.Text;
                assis.Escolaridade = ValidaEscolaridade();
                assis.ProblemaSaude = ValidaProblemaSaude();
                assis.QualProblemaSaude = txtQualDoenca.Text;
                assis.PossuiDeficiencia = ValidaPossuiDeficiencia();
                assis.Idade = ValidaIdade();
                assis.Filhos = ValidaFilhos();
                assis.QtdFilhos = (txtQtdFilhos.Text == "" ? 0 : Int32.Parse(txtQtdFilhos.Text));
                assis.RecebePensao = ValidaRecebePensao();
                assis.MaeCriaFilhosSemApoioPais = ValidaMaeCriaFilhosSemApoioPais();
                assis.MaePresenteFamilia = ValidaMaePresenteFamilia();
                assis.IdadeMae = ValidaIdadeMae();
                assis.RecebeBolsaFamilia = ValidaRecebeBolsaFamilia();
                assis.ValorBolsaFamilia = (txtvalorBolsaFamilia.Text == "" ? 0 : Decimal.Parse(txtvalorBolsaFamilia.Text));
                assis.RecebeLoas = ValidaRecebeLoas();
                assis.ValorLoas = txtLoas.Text;
                assis.CasaE = ValidaCasaE();
                assis.ValorAluguel = (txtValorAluguel.Text == "" ? 0 : Decimal.Parse(txtValorAluguel.Text));
                assis.QtdPessoasCasa = (txtQtdPessoasCasa.Text == "" ? 0 : Int32.Parse(txtQtdPessoasCasa.Text));
                assis.CarteiraAssinada = ValidaCarteiraAssinada();
                assis.Profissao = txtProfissao.Text;
                assis.AuxilioPrevidenciario = ValidaAuxilioPrevidenciario();
                assis.CadastroCras = ValidaCadastroCras();
                assis.DtCadastro = DateTime.Parse(lblDataCadastro.Text);
                assis.Bairro = cboBairro.SelectedIndex;
                assis.Responsavel = txtResponsavel.Text;

        ds = rn.BuscarAssistidoPorCPF(txtCPF.Text);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DialogResult confirm = new DialogResult(); 
                    if (!usuarioJaSalvo)
                    {
                        confirm = MessageBox.Show("Já existe um cadastro realizado com esse CPF, deseja atualizar os dados?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                    }

                    if (confirm.ToString().ToUpper().Equals("YES") || usuarioJaSalvo)
                    {
                        bool atualizado = rn.UpdateAssistido(assis, usuarioLogado);
                        if (atualizado == true)
                            MessageBox.Show("Registro atualizado com sucesso!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                        else
                            MessageBox.Show("Ocorreu um erro na atualização do cadastro!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                    }
                    DialogResult acrescentar = MessageBox.Show("Deseja incluir composição familiar?", "Composição Familiar", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);

                    if (acrescentar.ToString().ToUpper() == "YES")
                    {
                        btnGravarDependente.Enabled = true;
                        tabControl1.TabPages[1].Focus();
                        LimpaCamposComposicaoFamiliar();
                    }
                }
                else
                {

                    bool inserido = rn.InsereAssistido(assis, usuarioLogado);

                    if (inserido == true)
                    {
                        rn.insereCodigoFicha(assis, usuarioLogado);
                        RegistraLog.textoLogInsereAssistido(assis);
                        MessageBox.Show("Registro inserido com sucesso!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                        DialogResult confirm = MessageBox.Show("Deseja incluir composição familiar?", "Composição Familiar", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);

                        if (confirm.ToString().ToUpper() == "YES")
                        {
                            LimpaCamposComposicaoFamiliar();
                            tabControl1.TabPages[1].Focus();
                        }
                    }
                    else
                        MessageBox.Show("Ocorreu um erro na inserção do cadastro!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                    usuarioJaSalvo = true;
                }

                clickNovo = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }


        }

        private void btnNovo_Click(object sender, EventArgs e)
        {

            MDIPrincipal mdi = new MDIPrincipal();

            FrmTermosLGPD lgpd = new FrmTermosLGPD();
            lgpd.ShowDialog();
            if (lgpd.isAceite == true)
            {
                LimpaCampos();
                HabilitaCampos();
                NumeroCadastro();
                lblDataCadastro.Text = DateTime.Now.ToString("dd/MM/yyyy");
                btnGravar.Enabled = true;
                btnGravarDependente.Enabled = true;
                dtAcompanhamento.Value = DateTime.Now;
            }
            else
            {
                LimpaCampos();
                BloqueiaCampos();
                groupBox22.Enabled = true;
                btnGravar.Enabled = false;
                btnGravarDependente.Enabled = true;
            }

            clickNovo = true;
            usuarioJaSalvo = false;
        }

        private void LimpaCampos()
        {
            foreach (Control c in this.Controls)
            {
                if (c.GetType().ToString() == "System.Windows.Form.Textbox")
                {
                    c.Text = "";
                }
                if (c is RadioButton)
                {
                    RadioButton r = (RadioButton)c;
                    r.Checked = false;
                }
                if (c is ComboBox)
                {
                    ComboBox cb = (ComboBox)c;
                    cb.SelectedIndex = 0;
                }

                txtBusca.Text = "";
                txtCPF.Text = "";
                txtEndereco.Text = "";
                txtLoas.Text = "";
                txtMae.Text = "";
                txtNome.Text = "";
                txtObservacao.Text = "";
                txtPai.Text = "";
                txtProfissao.Text = "";
                txtQtdFilhos.Text = "";
                txtQtdPessoasCasa.Text = "";
                txtQualDoenca.Text = "";
                txtRG.Text = "";
                txtTelefone.Text = "";
                txtValorAluguel.Text = "";
                txtvalorBolsaFamilia.Text = "";

                txtNomeComposicao.Text = "";
                txtProfissaoComposicao.Text = "";
                txtTelefoneComposicao.Text = "";
                txtCPFComposicao.Text = "";
                txtRGComposicao.Text = "";
                cboBairro.SelectedIndex = 0;


                rdAlugada.Checked = false;
                rdAuxilioPrevNao.Checked = false;
                rdAuxilioPrevSim.Checked = false;
                rdBolsaFamiliaNao.Checked = false;
                rdBolsaFamiliaSim.Checked = false;
                rdCarteiraAssinadaNao.Checked = false;
                rdCarteiraAssinadaSim.Checked = false;
                rdCedida.Checked = false;
                rdCrasNao.Checked = false;
                rdCrasSim.Checked = false;
                rdEnsFundamentalComp.Checked = false;
                rdEnsFundamentalincomp.Checked = false;
                rdEnsMedioComp.Checked = false;
                rdEnsMedioIncomp.Checked = false;
                rdEnsSuperiorComp.Checked = false;
                rdEnsSuperiorIncomp.Checked = false;
                rdFilhosNao.Checked = false;
                rdFilhosSim.Checked = false;
                rdIdade0a6.Checked = false;
                rdIdade12a14.Checked = false;
                rdIdade15a17.Checked = false;
                rdIdade18a29.Checked = false;
                rdIdade30a59.Checked = false;
                rdIdade7a11.Checked = false;
                rdIdadeMae18a21.Checked = false;
                rdIdadeMaeMaior21.Checked = false;
                rdIdadeMaeMenor18.Checked = false;
                rdIdadeMaior60.Checked = false;
                rdInvadida.Checked = false;
                rdLoasNao.Checked = false;
                rdLoasSim.Checked = false;
                rdMaeApoioPaisNao.Checked = false;
                rdMaeApoioPaisSim.Checked = false;
                rdMaePresenteNao.Checked = false;
                rdMaePresenteSim.Checked = false;
                rdPossuiDeficienciaNao.Checked = false;
                rdPossuiDeficienciaSim.Checked = false;
                rdProbSaudeNao.Checked = false;
                rdProbSaudeSim.Checked = false;
                rdPropria.Checked = false;
                rdRecebePensaoNao.Checked = false;
                rdRecebePensaoSim.Checked = false;


                rdCarteiraAssinadaSimCF.Checked = false;
                rdDeficienciaNaoComposicao.Checked = false;
                rdDeficienciaSimComposicao.Checked = false;
                rdCarteiraAssinadaNaoCF.Checked = false;

            }

            foreach (Control c in this.tabControl1.Controls)
            {
                foreach (Control y in c.Controls)
                {
                    foreach (Control x in y.Controls)
                    {
                        if (x.GetType().ToString() == "System.Windows.Forms.TextBox")
                        {
                            TextBox tx = (TextBox)x;
                            tx.Text = "";
                        }
                        if (x is RadioButton)
                        {
                            RadioButton r = (RadioButton)x;
                            r.Checked = false;
                        }
                        if (x is ComboBox)
                        {
                            ComboBox cb = (ComboBox)x;
                            cb.SelectedIndex = -1;
                        }
                    }
                }

            }

            dtDataNasc.Value = DateTime.Today;

        }

        private void HabilitaCampos()
        {
            foreach (Control c in Controls)
            {
                if (c is TextBox)
                {
                    TextBox tx = (TextBox)c;
                    tx.Enabled = true;
                }
                if (c is GroupBox)
                {
                    GroupBox g = (GroupBox)c;
                    g.Enabled = true;
                }
                if (c is ComboBox)
                {
                    ComboBox cb = (ComboBox)c;
                    cb.Enabled = true;
                }

                if (c is DateTimePicker)
                {
                    DateTimePicker dt = (DateTimePicker)c;
                    dt.Enabled = true;
                }

            }

            foreach (Control c in this.tabControl1.Controls)
            {
                foreach (Control x in c.Controls)
                {
                    if (x is TextBox)
                    {
                        TextBox tx = (TextBox)x;
                        tx.Enabled = true;
                    }
                    if (x is GroupBox)
                    {
                        GroupBox g = (GroupBox)x;
                        g.Enabled = true;
                    }
                    if (x is ComboBox)
                    {
                        ComboBox cb = (ComboBox)x;
                        cb.Enabled = true;
                    }

                    if (x is DateTimePicker)
                    {
                        DateTimePicker dt = (DateTimePicker)x;
                        dt.Enabled = true;
                    }
                }
            }
        }

        private void BloqueiaCampos()
        {
            foreach (Control c in Controls)
            {
                if (c is TextBox)
                {
                    TextBox tx = (TextBox)c;
                    tx.Enabled = false;
                }
                if (c is GroupBox)
                {
                    GroupBox g = (GroupBox)c;
                    g.Enabled = false;
                }
                if (c is ComboBox)
                {
                    ComboBox cb = (ComboBox)c;
                    cb.Enabled = false;
                }

                if (c is DateTimePicker)
                {
                    DateTimePicker dt = (DateTimePicker)c;
                    dt.Enabled = false;
                }

            }

            foreach (Control c in this.tabControl1.Controls)
            {
                foreach (Control x in c.Controls)
                {
                    if (x is TextBox)
                    {
                        TextBox tx = (TextBox)x;
                        tx.Enabled = false;
                    }
                    if (x is GroupBox)
                    {
                        GroupBox g = (GroupBox)x;
                        g.Enabled = false;
                    }
                    if (x is ComboBox)
                    {
                        ComboBox cb = (ComboBox)x;
                        cb.Enabled = false;
                    }

                    if (x is DateTimePicker)
                    {
                        DateTimePicker dt = (DateTimePicker)x;
                        dt.Enabled = false;
                    }
                }
            }
        }

        private void NumeroCadastro()
        {
            RNAssistidos rn = new RNAssistidos();
            try
            {
                lblNumCadastro.Text = rn.NumeroCadastro().ToString();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private void btnGravarDependente_Click(object sender, EventArgs e)
        {
            ComposicaoFamiliar composicao = new ComposicaoFamiliar();
            RNAssistidos rn = new RNAssistidos();
            DataSet ds = new DataSet();
            try
            {

                if (validaCamposComposicaoFamiliar() == false)
                {
                    return;
                }

                composicao.Codigo = lblNumCadastro.Text;
                composicao.Nome = txtNomeComposicao.Text;
                composicao.Sexo = cbSexoComposicao.SelectedIndex;
                composicao.EstadoCivil = cbEstadoCivilComposicao.SelectedIndex;
                composicao.Cpf = txtCPFComposicao.Text;
                composicao.Rg = txtRGComposicao.Text;
                composicao.DtNascimento = dtNascimentoComposicao.Value;
                composicao.Telefone = txtTelefoneComposicao.Text;
                composicao.Escolaridade = txtEscolaridadeCF.Text;
                composicao.PossuiDeficiencia = ValidaPossuiDeficienciaComposicao();
                composicao.Idade = Convert.ToInt32( txtIdadeCF.Text);
                composicao.CarteiraAssinada = ValidaCarteiraAssinadaComposicao();
                composicao.Profissao = txtProfissao.Text;
                composicao.CartaoSUS = txtCartaoSUS.Text;
                composicao.CertidaoNascimento = txtCertidaoNasc.Text;
                composicao.Filiacao = txtFiliacao.Text;
                composicao.Laudo = txtLaudo.Text;
                composicao.ProblemaSaude = ValidaProblemaSaudeComposicao();
                composicao.QualProblemaSaude = txtProblemaSaudeCF.Text;
                composicao.AlergiaAlimento = ValidaAlergiaAlimentoComposicao();
                composicao.QualAlergiaAlimento = txtAlergiaAlimentoCF.Text;
                composicao.UsoMedicamento = ValidaUsoMedicamentoComposicao();
                composicao.QualUsoMedicamento = txtUsoMedicamentoCF.Text;
                composicao.AtividadeAlemSCFV = ValidaAtividadeAlemSCFVComposicao();
                composicao.QualAtividadeAlemSCFV = txtAtividadesAlemScfvCF.Text;
                composicao.RestricaoParticiparAtividade = ValidaRestricaoParticiparAtividade();
                composicao.QualRestricaoParticiparAtividade = txtAlgumaAtividadeCF.Text;
                composicao.CadastroCras = ValidaCadastroCrasComposicao();
                composicao.QualCadastroCras = txtCadastroCrasCF.Text;

                ds = rn.BuscarComposicaoFamiliarPorCPF(txtCPFComposicao.Text, txtNomeComposicao.Text);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DialogResult confirm = MessageBox.Show("Já existe um cadastro de composição familiar realizado com esse CPF, deseja atualizar os dados?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);

                    if (confirm.ToString().ToUpper() == "YES")
                    {
                        bool atualizado = rn.AlterarComposicaoFamiliar(composicao, usuarioLogado);
                        if (atualizado == true)
                            MessageBox.Show("Registro atualizado com sucesso!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                        else
                            MessageBox.Show("Ocorreu um erro na atualização do cadastro!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                    }
                }
                else
                {
                    bool inserido = rn.InsereComposicaoFamiliar(composicao, usuarioLogado);

                    if (inserido)
                    {
                        RegistraLog.textoLogInsereComposicaoFamiliar(composicao);
                        DialogResult confirm = MessageBox.Show("Deseja incluir mais dependentes?", "Composição Familiar", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);

                        if (confirm.ToString().ToUpper() == "YES")
                        {
                            LimpaCamposComposicaoFamiliar();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }

        }

        void LimpaCamposComposicaoFamiliar()
        {
            try
            {

                txtNomeComposicao.Text = "";
                cbSexoComposicao.SelectedIndex = 0;
                cbEstadoCivilComposicao.SelectedIndex = 0;
                txtCPFComposicao.Text = "";
                txtRGComposicao.Text = "";
                dtNascimentoComposicao.Value = DateTime.Today;
                txtTelefoneComposicao.Text = "";
                rdDeficienciaSimComposicao.Checked = false;
                rdDeficienciaNaoComposicao.Checked = false;
                rdCarteiraAssinadaNaoCF.Checked = false;
                rdCarteiraAssinadaSimCF.Checked = false;
                txtProfissaoComposicao.Text = "";
                txtCertidaoNasc.Text = "";
                txtCartaoSUS.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private void FrmFormulario_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            RNAssistidos rn = new RNAssistidos();
            try
            {
                cbEstadoCivilComposicao.SelectedIndex = 0;
                cboEstadoCivil.SelectedIndex = 0;
                cboOrientacao.SelectedIndex = 0;
                cboSexo.SelectedIndex = 0;
                cbSexoComposicao.SelectedIndex = 0;
                cboBuscarPor.SelectedIndex = 0;
                cboBuscarPor.Enabled = true;
                dt = rn.BuscarBairros(0);
                foreach (DataRow item in dt.Rows)
                {
                    cboBairro.Items.Add(item.ItemArray[1]);

                }

                if (numFicha != "")
                {
                    BuscaInfo("Número da Ficha", numFicha);
                }

                foreach (Control c in this.Controls)
                {
                    foreach (Control y in c.Controls)
                    {
                        foreach (Control x in y.Controls)
                        {
                            if (x.GetType().ToString() == "System.Windows.Forms.TextBox")
                            {
                                TextBox tx = (TextBox)x;
                                tx.ShortcutsEnabled = true;
                            }
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void txtCPF_Leave(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            RNAssistidos rn = new RNAssistidos();
            try
            {
                ds = rn.BuscarAssistidoPorCPF(txtCPF.Text);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    CarregaDadosAssistido(ds.Tables[0]);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }


        }


        private void CarregaDadosAssistido(DataTable dt)
        {

            try
            {

                foreach (DataRow dr in dt.Rows)
                {
                    lblNumCadastro.Text = dr["codigo"].ToString();
                    cboOrientacao.SelectedIndex = Int32.Parse(dr["orientacao"].ToString());
                    txtNome.Text = dr["nome"].ToString();
                    cboSexo.SelectedIndex = Int32.Parse(dr["sexo"].ToString());
                    cboEstadoCivil.SelectedIndex = Int32.Parse(dr["estadoCivil"].ToString());
                    txtCPF.Text = dr["cpf"].ToString();
                    txtRG.Text = dr["rg"].ToString();
                    txtNis.Text = dr["nis"].ToString();
                    dtDataNasc.Value = DateTime.Parse(dr["dtNascimento"].ToString());
                    txtEndereco.Text = dr["endereco"].ToString();
                    txtTelefone.Text = dr["telefone"].ToString();
                    txtMae.Text = dr["mae"].ToString();
                    txtPai.Text = dr["pai"].ToString();
                    CarregaEscolaridade(Int32.Parse(dr["escolaridade"].ToString()));
                    CarregaProblemaSaude(Int32.Parse(dr["problemaSaude"].ToString()));
                    txtQualDoenca.Text = dr["qualProblemaSaude"].ToString();
                    CarregaPossuiDeficiencia(Int32.Parse(dr["possuiDeficiencia"].ToString()));
                    CarregaIdade(Int32.Parse(dr["idade"].ToString()));
                    CarregaFilhos(Int32.Parse(dr["filhos"].ToString()));
                    txtQtdFilhos.Text = dr["qtdFilhos"].ToString();
                    CarregaRecebePensao(Int32.Parse(dr["recebePensao"].ToString()));
                    CarreagaMaeCriaFilhosSemApoioPais(Int32.Parse(dr["maeCriaFilhosSemApoioPais"].ToString()));
                    CarregaMaePresenteFamilia(Int32.Parse(dr["maePresenteFamilia"].ToString()));
                    CarregaIdadeMae(Int32.Parse(dr["idadeMae"].ToString()));
                    CarregaRecebeLoas(Int32.Parse(dr["recebeLoas"].ToString()));
                    txtLoas.Text = dr["valorLoas"].ToString();
                    CarregaCasaE(Int32.Parse(dr["casaE"].ToString()));
                    txtValorAluguel.Text = dr["valorAluguel"].ToString();
                    txtQtdPessoasCasa.Text = dr["qtdPessoasCasa"].ToString();
                    CarregaCarteiraAssinada(Int32.Parse(dr["carteiraAssinada"].ToString()));
                    txtProfissao.Text = dr["profissao"].ToString();
                    CarregaAuxilioPrevidenciario(Int32.Parse(dr["auxilioPrevidenciario"].ToString()));
                    CarregaCadastroCras(Int32.Parse(dr["cadastroCras"].ToString()));
                    CarregaRecebeBolsaFamilia(Int32.Parse(dr["recebeBolsaFamilia"].ToString()));
                    txtvalorBolsaFamilia.Text = dr["valorBolsaFamilia"].ToString();
                    lblDataCadastro.Text = (dr["dtCadastro"].ToString() == "" ? "" : DateTime.Parse(dr["dtCadastro"].ToString()).ToString("dd/MM/yyyy"));
                    cboBairro.SelectedIndex = Convert.ToInt32(dr["bairro"]);
                    txtResponsavel.Text = dr["responsavel"].ToString();
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }


        }

        private void CarregaDadosComposicaoFamiliar(DataTable dt)
        {

            try
            {

                foreach (DataRow dr in dt.Rows)
                {
                    lblNumCadastro.Text = dr["codigo"].ToString();
                    //cboOrientacao.SelectedIndex = Int32.Parse(dr["orientacao"].ToString());
                    txtNomeComposicao.Text = dr["nome"].ToString();
                    cbSexoComposicao.SelectedIndex = Int32.Parse(dr["sexo"].ToString());
                    cbEstadoCivilComposicao.SelectedIndex = Int32.Parse(dr["estadoCivil"].ToString());
                    txtCPFComposicao.Text = dr["cpf"].ToString();
                    txtRGComposicao.Text = dr["rg"].ToString();
                    dtNascimentoComposicao.Value = DateTime.Parse(dr["dtNascimento"].ToString());
                    txtTelefoneComposicao.Text = dr["telefone"].ToString();
                    txtEscolaridadeCF.Text = ((dr["escolaridade"].ToString()));
                    CarregaPossuiDeficienciaComposicao(Int32.Parse(dr["possuiDeficiencia"].ToString()));
                    txtIdadeCF.Text = dr["idade"].ToString();
                    CarregaCarteiraAssinadaComposicao(Int32.Parse(dr["carteiraAssinada"].ToString()));
                    txtProfissaoComposicao.Text = dr["profissao"].ToString();
                    txtCartaoSUS.Text = dr["cartaoSUS"].ToString();
                    txtCertidaoNasc.Text = dr["certidaoNasc"].ToString();
                    txtFiliacao.Text  = dr["filiacao"].ToString();
                    txtLaudo.Text = dr["laudo"].ToString();
                    CarregaProblemaSaudeComposicao(Int32.Parse(dr["ProblemaSaude"].ToString()));
                    txtProblemaSaudeCF.Text = dr["QualProblemaSaude"].ToString();
                    CarregaAlergiaAlimentoComposicao(Int32.Parse(dr["AlergiaAlimento"].ToString()));
                    txtAlergiaAlimentoCF.Text = dr["QualAlergiaAlimento"].ToString();
                    CarregaUsoMedicamentoComposicao(Int32.Parse(dr["UsoMedicamento"].ToString()));
                    txtUsoMedicamentoCF.Text = dr["QualUsoMedicamento"].ToString();
                    CarregaAtividadeAlemSCFVComposicao(Int32.Parse(dr["AtividadeAlemSCFV"].ToString()));
                    txtAtividadesAlemScfvCF.Text = dr["QualAtividadeAlemSCFV"].ToString();
                    CarregaRestricaoParticiparAtividadeComposicao(Int32.Parse(dr["RestricaoParticiparAtividade"].ToString()));
                    txtAlgumaAtividadeCF.Text = dr["QualRestricaoParticiparAtividade"].ToString();
                    CarregaCadastroCrasComposicao(Int32.Parse(dr["CadastroCRAS"].ToString()));
                    txtCadastroCrasCF.Text = dr["QualCadastroCRAS"].ToString();

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private void CarregaDadosAcompanhamento(DataTable pDt, bool pIsComboBox)
        {

            try
            {

                if (pIsComboBox == true)
                {
                    foreach (DataRow dr in pDt.Rows)
                    {
                        cboHistoricoAcompanhamento.Items.Add(DateTime.Parse(dr["dataAcompanhamento"].ToString()));
                    }
                }
                else
                {
                    foreach (DataRow dr in pDt.Rows)
                    {
                        dtAcompanhamento.Value = DateTime.Parse(dr["dataAcompanhamento"].ToString());
                        txtObservacao.Text = dr["observacao"].ToString();
                    }
                }



            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }



        private void btnBuscar_Click(object sender, EventArgs e)
        {

            try
            {
                lblOut.Visible = false;
                lblOutCF.Visible = false;
                cboComposicaoFamiliar.Items.Clear();
                
                if (cboBuscarPor.SelectedIndex == 0)
                {
                    MessageBox.Show("Por favor selecione uma opção na busca!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                    cboBuscarPor.Focus();
                    return;
                }

                if (txtBusca.Text == "")
                {
                    MessageBox.Show("Por favor preencha o campo de busca!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                    txtBusca.Focus();
                    return;
                }

                BuscaInfo(cboBuscarPor.Text, txtBusca.Text);
                usuarioJaSalvo = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }

        }

        private void BuscaInfo(string pTipBusca, string pValor)
        {
            DataSet dsAssistido = new DataSet();
            DataSet dsComposicao = new DataSet();
            DataSet dsHistorico = new DataSet();
            DataSet ds = new DataSet();
            RNAssistidos rn = new RNAssistidos();
            RNUsuario rnUsuario = new RNUsuario();
            Int32 cod_empresa = FrmLogin.cod_empresa;

            try
            {
                if (pTipBusca == "CPF")
                    dsAssistido = rn.BuscarAssistidoPorCPF(pValor);
                if (pTipBusca == "Nome")
                    dsAssistido = rn.BuscarAssistidoPorNome(pValor);
                if (pTipBusca == "Número da Ficha")
                    dsAssistido = rn.BuscarAssistidoPorNumeroFicha(pValor);

                cbUsuariosEncontrados.Items.Clear();
                if (dsAssistido.Tables[0].Rows.Count > 0)
                {
                    if (pTipBusca == "Nome" &&
                            dsAssistido.Tables[0].Rows.Count > 1)
                    {
                        cbUsuariosEncontrados.Items.Add("Selecione...");
                        foreach (DataRow usuario in dsAssistido.Tables[0].Rows)
                        {
                            cbUsuariosEncontrados.Items.Add(usuario.ItemArray[2].ToString());
                        }
                        cbUsuariosEncontrados.SelectedIndex = 0;
                        cbUsuariosEncontrados.Visible = true;
                        MessageBox.Show("Por favor selecione o Assistido!");
                        cbUsuariosEncontrados.Focus();
                        return;
                    }
                    cbUsuariosEncontrados.Visible = false;
                    CarregaTelaAssistidos(cod_empresa, dsAssistido);
                    //if (cod_empresa != 3)
                    //{
                    //        //VERIFICA SE O USUARIO TEM PERIMISSÃO DE ACESSAR A FICHA - inicio
                    //        bool isFichaValida = false;
                    //    Int32 cont = 0;
                    //    string usuarioFicha = dsAssistido.Tables[0].Rows[0].ItemArray[36].ToString();

                    //    ds = rnUsuario.RetornaUsuariosEmpresa(cod_empresa);
                    //    if (ds.Tables[0].Rows.Count > 0)
                    //    {
                    //        foreach (DataRow dr in ds.Tables[0].Rows)
                    //        {
                    //            if (usuarioFicha == dr.ItemArray[0].ToString())
                    //            {
                    //                isFichaValida = true;
                    //                break;
                    //            }
                    //            cont += 1;
                    //        }
                    //        if (isFichaValida == false)
                    //        {
                    //            MessageBox.Show("O Usuário logado não tem permissão para acessar essa ficha!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                    //            return;
                    //        }
                    //    }
                    //    //VERIFICA SE O USUARIO TEM PERIMISSÃO DE ACESSAR A FICHA - fim
                    //}

                    //CarregaDadosAssistido(dsAssistido.Tables[0]);
                    //HabilitaCampos();
                    //btnGravar.Enabled = true;
                    CarregaTelaComposicao();
                }
                //dsComposicao = rn.BuscarComposicaoFamiliarPorCodigo(lblNumCadastro.Text);
                //if (dsComposicao.Tables[0].Rows.Count > 0)
                //{
                //    CarregaDadosComposicaoFamiliar(dsComposicao.Tables[0]);
                //    HabilitaCampos();
                //    btnGravarDependente.Enabled = true;
                //}
                //if (dsComposicao.Tables[0].Rows.Count > 1)
                //{
                //    lblComposicaoFamiliar.Visible = true;
                //    cboComposicaoFamiliar.Visible = true;
                //    CarregaComboComposicao(dsComposicao.Tables[0]);
                //}
                //else
                //{
                //    lblComposicaoFamiliar.Visible = true;
                //    cboComposicaoFamiliar.Visible = true;
                //}

                //cboHistoricoAcompanhamento.Items.Clear();
                //dsHistorico = rn.BuscaAnotacao(lblNumCadastro.Text);
                //if (dsHistorico.Tables[0].Rows.Count > 0)
                //{
                //    lblHistoricoAcompanhamento.Visible = true;
                //    cboHistoricoAcompanhamento.Visible = true;
                //    CarregaDadosAcompanhamento(dsHistorico.Tables[0], true);
                //}
                //else
                //{
                //    lblHistoricoAcompanhamento.Visible = false;
                //    cboHistoricoAcompanhamento.Visible = false;
                //}


                if (dsAssistido.Tables[0].Rows.Count == 0)
                {
                    MessageBox.Show($"Não foi encontrado registros com o {cboBuscarPor.Text} informado!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
            finally
            {
                dsAssistido.Dispose();
                dsComposicao.Dispose();
                dsHistorico.Dispose();
                ds.Dispose();
            }
        }

        void CarregaTelaAssistidos(int cod_empresa, DataSet dsAssistido)
        {
            try
            {
                DataSet ds = new DataSet();
                RNAssistidos rn = new RNAssistidos();
                RNUsuario rnUsuario = new RNUsuario();

                if (cod_empresa != 3 || usuarioLogado == "admin")
                {
                    //VERIFICA SE O USUARIO TEM PERIMISSÃO DE ACESSAR A FICHA - inicio
                    bool isFichaValida = false;
                    Int32 cont = 0;
                    string usuarioFicha = dsAssistido.Tables[0].Rows[0].ItemArray[36].ToString();

                    ds = rnUsuario.RetornaUsuariosEmpresa(cod_empresa);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            if (usuarioFicha == dr.ItemArray[0].ToString())
                            {
                                isFichaValida = true;
                                break;
                            }
                            cont += 1;
                        }
                        if (isFichaValida == false)
                        {
                            MessageBox.Show("O Usuário logado não tem permissão para acessar essa ficha!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                            return;
                        }
                    }
                    //VERIFICA SE O USUARIO TEM PERIMISSÃO DE ACESSAR A FICHA - fim
                }
                CarregaDadosAssistido(dsAssistido.Tables[0]);
                HabilitaCampos();
                btnGravar.Enabled = true;
                btnImprimirAssistido.Enabled = true;
                
            }
                catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }

        }

        void CarregaTelaComposicao()
        {
            try
            {
                DataSet dsComposicao = new DataSet();
                
                RNAssistidos rn = new RNAssistidos();

                dsComposicao = rn.BuscarComposicaoFamiliarPorCodigo(lblNumCadastro.Text);
                if (dsComposicao.Tables[0].Rows.Count > 0)
                {
                    CarregaDadosComposicaoFamiliar(dsComposicao.Tables[0]);
                    HabilitaCampos();
                    btnGravarDependente.Enabled = true;
                }
                if (dsComposicao.Tables[0].Rows.Count > 1)
                {
                    lblComposicaoFamiliar.Visible = true;
                    cboComposicaoFamiliar.Visible = true;
                    CarregaComboComposicao(dsComposicao.Tables[0]);
                }
                else
                {
                    lblComposicaoFamiliar.Visible = true;
                    cboComposicaoFamiliar.Visible = true;
                }

                cboHistoricoAcompanhamento.Items.Clear();
                dsHistoricoAnotacoes = rn.BuscaAnotacao(lblNumCadastro.Text);
                if (dsHistoricoAnotacoes.Tables[0].Rows.Count > 0)
                {
                    lblHistoricoAcompanhamento.Visible = true;
                    cboHistoricoAcompanhamento.Visible = true;
                    CarregaDadosAcompanhamento(dsHistoricoAnotacoes.Tables[0], true);
                }
                else
                {
                    lblHistoricoAcompanhamento.Visible = false;
                    cboHistoricoAcompanhamento.Visible = false;
                }
                btnImprimiComposicao.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                btnImprimiComposicao.Enabled = false;
            }
        }


        void CarregaComboComposicao(DataTable dt)
        {
            int contador = 0;
            try
            {
                foreach (DataRow dr in dt.Rows)
                {
                    cboComposicaoFamiliar.Items.Add(dr["nome"].ToString());
                    contador += contador;
                }
                cboComposicaoFamiliar.SelectedIndex = contador;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private void cboComposicaoFamiliar_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet dsComposicao = new DataSet();
            RNAssistidos rn = new RNAssistidos();

            try
            {
                dsComposicao = rn.BuscarComposicaoFamiliarPorNome(cboComposicaoFamiliar.Text);
                if (dsComposicao.Tables[0].Rows.Count > 0)
                {
                    CarregaDadosComposicaoFamiliar(dsComposicao.Tables[0]);
                    HabilitaCampos();
                    btnGravarDependente.Enabled = true;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private void cboComposicaoFamiliar_SelectedValueChanged(object sender, EventArgs e)
        {
            DataSet dsComposicao = new DataSet();
            RNAssistidos rn = new RNAssistidos();

            try
            {
                dsComposicao = rn.BuscarComposicaoFamiliarPorNome(cboComposicaoFamiliar.Text);
                if (dsComposicao.Tables[0].Rows.Count > 0)
                {
                    CarregaDadosComposicaoFamiliar(dsComposicao.Tables[0]);
                    HabilitaCampos();
                    btnGravarDependente.Enabled = true;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private void btnSalvarAnotacao_Click(object sender, EventArgs e)
        {
            RNAssistidos rn = new RNAssistidos();
            try
            {
                if (rn.InsereAnotacao(lblNumCadastro.Text, txtObservacao.Text, dtAcompanhamento.Value) == true)
                {
                    MessageBox.Show("Registro atualizado com sucesso!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                    txtObservacao.Text = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private void cboHistoricoAcompanhamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DataSet dsHistorico = new DataSet();
                RNAssistidos rn = new RNAssistidos();
                dsHistorico = rn.BuscaAnotacaoPorData(lblNumCadastro.Text, DateTime.Parse(cboHistoricoAcompanhamento.Text));
                if (dsHistorico.Tables[0].Rows.Count > 0)
                {
                    CarregaDadosAcompanhamento(dsHistorico.Tables[0], false);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            txtObservacao.Text = "";
            dtAcompanhamento.Value = DateTime.Now;
        }

        private void cboBuscarPor_SelectedIndexChanged(object sender, EventArgs e)
        {
            cboBuscarPor.Visible = true;
            dtBusca.Visible = false;

            if (cboBuscarPor.Text == "CPF")
                lblBuscarPor.Text = cboBuscarPor.Text;
            if (cboBuscarPor.Text == "Nome")
                lblBuscarPor.Text = cboBuscarPor.Text;
            if (cboBuscarPor.Text == "Número da Ficha")
                lblBuscarPor.Text = cboBuscarPor.Text;
            if (cboBuscarPor.Text == "Data")
            {
                lblBuscarPor.Text = cboBuscarPor.Text;
                cboBuscarPor.Visible = false;
                dtBusca.Visible = true;
            }
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtNome.Text == string.Empty)
                {
                    lblOut.Text = $"Não há informações suficientes para gerar o arquivo!";
                    return;
                }
                string path = System.AppDomain.CurrentDomain.BaseDirectory.ToString();
                path = path.Replace("\\", "/");
                string templatePath = $"{path}Resources Base/Assistido.docx";
                string outputPath = $"{path}Arquivos saida/Assistido - {txtNome.Text}.docx";
                var idade = CalCulaIdade(dtDataNasc.Value);
                acompanhamentoTexto = InsereAcompanhamentos();
                var fields = new Dictionary<string, string>
                {
                    { "{{Nome}}", txtNome.Text },
                    { "{{Nascimento}}", dtDataNasc.Text },
                    { "{{Idade}}", idade.ToString() },
                    { "{{Sexo}}", cboSexo.Text },
                    { "{{Civil}}", cboEstadoCivil.Text },
                    { "{{CPF}}", txtCPF.Text }, 
                    { "{{RG}}", txtRG.Text },
                    { "{{Mae}}", txtMae.Text },
                    { "{{Pai}}", txtPai.Text },
                    { "{{NIS}}", txtNis.Text },
                    { "{{Endereco}}", txtEndereco.Text },
                    { "{{Bairro}}", cboBairro.Text },
                    { "{{Telefone}}", txtTelefone.Text },
                    { "{{Responsavel}}", txtResponsavel.Text },
                    { "{{Escolaridade}}", escolaridade },

                    { "{{ProblemaSaude}}", problemaSaude},
                    { "{{QualProblemaSaude}}", txtQualDoenca.Text},
                    { "{{Filhos}}", temFilhos},
                    { "{{QtsFilhos}}", txtQtdFilhos.Text },
                    { "{{Pensao}}", recebePensao},
                    { "{{MaeApoioPais}}", maeApoioPais},
                    { "{{MaePresente}}", maePresente},
                    { "{{IdadeMae}}", idadeMae},
                    { "{{RecebeBolsaFamilia}}", recebeBolsaFamilia },
                    { "{{ValorBolsaFamilia}}", txtvalorBolsaFamilia.Text},
                    { "{{RecebeLoas}}", recebeLoas},
                    { "{{DequemRecebeLoas}}", txtLoas.Text},
                    { "{{PossuiDeficiencia}}", possuiDeficiencia },
                    { "{{DadosCasa}}", dadosCasa },
                    { "{{ValorCasa}}", txtValorAluguel.Text},
                    { "{{QtdPessoasCasa}}", txtQtdPessoasCasa.Text },
                    { "{{PossuiFonteRenda}}", possuiFonteRenda },
                    { "{{Profissao}}", txtProfissao.Text },
                    { "{{AuxilioPrevidenciario}}", auxilioPrevidenciario },
                    { "{{CadastroCras}}", cadastroCras },
                    { "{{Acompanhamento}}", acompanhamentoTexto },
                };
                lblOut.Visible = true;
               if (GerarWord.PreencherCampos(templatePath, outputPath, fields))
                {
                    lblOut.Text = $"Arquivo gerado com sucesso na pasta {path}Arquivos saida";
                }
               else
                {
                    lblOut.Text = $"Erro ao gerar o arquivo por favor entre em contato com o administrador do sistema";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private String  InsereAcompanhamentos()  
        {
            try
            {
                //StringBuilder textoAcompanhamento = new StringBuilder();
                //if (dsHistoricoAnotacoes.Tables[0].Rows.Count > 0)
                //{
                //    foreach (DataRow dr in dsHistoricoAnotacoes.Tables[0].Rows)
                //    {
                //        textoAcompanhamento.Append(string.Format("<br/><b>Data Acompanhamento:</b> {0} <b> Acompanhamento: :</b>{1} <b>", 
                //            DateTime.Parse(dr["dataAcompanhamento"].ToString()), dr["observacao"].ToString()));
                //    }
                //}
                string textoAcompanhamento = string.Empty;
                if (dsHistoricoAnotacoes.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsHistoricoAnotacoes.Tables[0].Rows)
                    {
                        textoAcompanhamento += $"Data:{DateTime.Parse(dr["dataAcompanhamento"].ToString())}\n";
                        textoAcompanhamento += $"Acompanhamento:\n {dr["observacao"]}";
                        textoAcompanhamento += "\n";
                    }
                }
                return textoAcompanhamento;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return null;
            }
        }

        private int CalCulaIdade(DateTime dateOfBirth)
        {
            try
            {
                var today = DateTime.Today;
                var a = (today.Year * 100 + today.Month) * 100 + today.Day;
                var b = (dateOfBirth.Year * 100 + dateOfBirth.Month) * 100 + dateOfBirth.Day;

                return (a - b) / 10000;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return 0;
            }
        }


        private void btnNovoDependente_Click(object sender, EventArgs e)
        {
            LimpaCamposComposicaoFamiliar();
        }

        private void txtNomeComposicao_TextChanged(object sender, EventArgs e)
        {
            if (txtNomeComposicao.Text != "")
            {
                btnGravarDependente.Enabled = true;
            }
            else
            {
                btnGravarDependente.Enabled = false;
            }
        }

        private void cboOrientacao_SelectedIndexChanged(object sender, EventArgs e)
        {
            Int32 cod_empresa = FrmLogin.cod_empresa;
            try
            {
                if (clickNovo == true)
                {
                    if (cod_empresa != 3)
                    {
                        if (cboOrientacao.SelectedIndex != 0 && (cboOrientacao.SelectedIndex == 5 && cod_empresa != 2))
                        {
                            MessageBox.Show("O usuário não tem permissão para criar fichas do tipo " + cboOrientacao.Text + ", por favor selecione novamente a orientação da ficha", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                            cboOrientacao.SelectedIndex = 0;
                        }
                        else if (cboOrientacao.SelectedIndex != 0 && (cboOrientacao.SelectedIndex != 5 && cod_empresa == 2))
                        {
                            MessageBox.Show("O usuário não tem permissão para criar fichas do tipo " + cboOrientacao.Text + ", por favor selecione novamente a orientação da ficha", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                            cboOrientacao.SelectedIndex = 0;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }


        }

        private void rdProblemaSaudeSimCF_CheckedChanged(object sender, EventArgs e)
        {
            if (rdProblemaSaudeSimCF.Checked == true)
            {
                label31.Visible = true;
                txtProblemaSaudeCF.Visible = true;
            }
            else
            {
                label31.Visible = false;
                txtProblemaSaudeCF.Visible = false;
            }
        }

        private void rdAlergiaAlimentoSimCF_CheckedChanged(object sender, EventArgs e)
        {
            if (rdAlergiaAlimentoSimCF.Checked == true)
            {
                label34.Visible = true;
                txtAlergiaAlimentoCF.Visible = true;
            }
            else
            {
                label34.Visible = false;
                txtAlergiaAlimentoCF.Visible = false;
            }

        }

        private void rdUsoMedicamentoSimCF_CheckedChanged(object sender, EventArgs e)
        {
            if (rdUsoMedicamentoSimCF.Checked == true)
            {
                label35.Visible = true;
                txtUsoMedicamentoCF.Visible = true;
            }
            else
            {
                label35.Visible = false;
                txtUsoMedicamentoCF.Visible = false;
            }
        }

        private void rdAtividadesAlemScfvSimCF_CheckedChanged(object sender, EventArgs e)
        {
            if (rdAtividadesAlemScfvSimCF.Checked == true)
            {
                label36.Visible = true;
                txtAtividadesAlemScfvCF.Visible = true;
            }
            else
            {
                label36.Visible = false;
                txtAtividadesAlemScfvCF.Visible = false;
            }
        }

        private void rdAlgumaAtividadeSimCF_CheckedChanged(object sender, EventArgs e)
        {
            if (rdAlgumaAtividadeSimCF.Checked == true)
            {
                label37.Visible = true;
                txtAlgumaAtividadeCF.Visible = true;
            }
            else
            {
                label37.Visible = false;
                txtAlgumaAtividadeCF.Visible = false;
            }
        }

        private void rdCadastroCrasSimCF_CheckedChanged(object sender, EventArgs e)
        {
            if (rdCadastroCrasSimCF.Checked == true)
            {
                label32.Visible = true;
                txtCadastroCrasCF.Visible = true;
            }
            else
            {
                label32.Visible = false;
                txtCadastroCrasCF.Visible = false;
            }

        }

        private void cbUsuariosEncontrados_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbUsuariosEncontrados.SelectedIndex != 0)
                {
                DataSet ds = new DataSet();
                RNAssistidos rn = new RNAssistidos();
                DataSet dsAssistido = new DataSet();
                Int32 cod_empresa = FrmLogin.cod_empresa;
                dsAssistido = rn.BuscarAssistidoPorNome(cbUsuariosEncontrados.Text);
                CarregaTelaAssistidos(cod_empresa, dsAssistido);
                cbUsuariosEncontrados.Items.Clear();
                cbUsuariosEncontrados.Visible = false;
                txtBusca.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtNomeComposicao.Text == string.Empty)
                {
                    lblOutCF.Text = $"Não há informações suficientes para gerar o arquivo!";
                    return;
                }
                string path = System.AppDomain.CurrentDomain.BaseDirectory.ToString();
                path = path.Replace("\\", "/");
                string templatePath = $"{path}Resources Base/Composicao.docx";
                string outputPath = $"{path}Arquivos saida/Composição - {txtNome.Text}.docx";
                var idade = CalCulaIdade(dtNascimentoComposicao.Value);
                var fields = new Dictionary<string, string>
                {
                    { "{{Nome}}", txtNomeComposicao.Text },
                    { "{{Nascimento}}", dtNascimentoComposicao.Text },
                    { "{{Idade}}", idade.ToString() },
                    { "{{Sexo}}", cbSexoComposicao.Text },
                    { "{{Civil}}", cbEstadoCivilComposicao.Text },
                    { "{{CPF}}", txtCPFComposicao.Text },
                    { "{{RG}}", txtRGComposicao.Text },
                    { "{{Telefone}}", txtTelefoneComposicao.Text },
                    { "{{Filiacao}}", txtFiliacao.Text },
                    { "{{CertidaoNascimento}}", txtCertidaoNasc.Text },
                    { "{{SUS}}", txtCertidaoNasc.Text },
                    { "{{Escolaridade}}", txtEscolaridadeCF.Text},
                    { "{{ProblemaSaude}}", problemaSaudeCF},
                    { "{{QualProblemaSaude}}", txtProblemaSaudeCF.Text},
                    { "{{UsaMedicamento}}", usaMedicamentoCF},
                    { "{{QualMedicamento}}", txtUsoMedicamentoCF.Text},
                    { "{{RestricaoAtividade}}", restricaoAtividadeCF},
                    { "{{QualRestricaoAtividade}}", txtAlgumaAtividadeCF.Text},
                    { "{{CadastroCras}}", cadastroCrasComposicao },
                    { "{{QualCras}}", txtCadastroCrasCF.Text},
                    { "{{PossuiDeficiencia}}", possuiDeficienciaCF},
                    { "{{AlergiaAlimento}}", alergiaAlimentoCF},
                    { "{{QualAlergiaAlimento}}", txtAlergiaAlimentoCF.Text},
                    { "{{OutrasSCFV}}", outrasSCFVCF},
                    { "{{QualOutrasSCFV}}", txtAtividadesAlemScfvCF.Text},
                    { "{{FonteRenda}}", fonteRendaCF},
                    { "{{QualFonteRenda}}", txtProfissaoComposicao.Text},
                    { "{{Laudo}}", txtLaudo.Text},
                };
                lblOutCF.Visible = true;
                if (GerarWord.PreencherCampos(templatePath, outputPath, fields))
                {
                    lblOutCF.Text = $"Arquivo gerado com sucesso na pasta {path}Arquivos saida";
                }
                else
                {
                    lblOutCF.Text = $"Erro ao gerar o arquivo por favor entre em contato com o administrador do sistema";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }
    }
}
