﻿using SISSocial.RN;
using System;
using System.Data;
using System.Diagnostics.Eventing.Reader;
using System.Windows.Forms;

namespace SISSocial
{
    public partial class FrmPesquisa : Form
    {
        public string usuarioLogado = "";
        public FrmPesquisa()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            DataSet dsAssistido = new DataSet();
            DataSet dsComposicao = new DataSet();
            DataSet dsHistorico = new DataSet();
            RNAssistidos rn = new RNAssistidos();
            try
            {
                if (cboBuscarPor.SelectedIndex == 0)
                {
                    MessageBox.Show("Por favor selecione uma opção na busca!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                    cboBuscarPor.Focus();
                    return;
                }


                if (cboBuscarPor.Text == "CPF")
                    dsAssistido = rn.BuscarAssistidoPorCPF(txtBusca.Text);
                if (cboBuscarPor.Text == "Nome")
                    dsAssistido = rn.BuscarAssistidoPorNome(txtBusca.Text);
                if (cboBuscarPor.Text == "Número da Ficha")
                    dsAssistido = rn.BuscarAssistidoPorNumeroFicha(txtBusca.Text);
                if (cboBuscarPor.Text == "Bairro")
                    dsAssistido = rn.BuscarAssistidoPorBairro(cboPesquisa.Text);
                if (cboBuscarPor.Text == "Data de Cadastro")
                    dsAssistido = rn.BuscarAssistidoPorData(dtPesquisa.Text);




                if (dsAssistido.Tables[0].Rows.Count > 0)
                {
                    dtGridResultado.DataSource = dsAssistido.Tables[0];
                    lblNumRegistros.Text = "Num. Registros: " + dsAssistido.Tables[0].Rows.Count.ToString();
                }
                else
                {
                    MessageBox.Show("Não foram encontrados registros!");
                    lblNumRegistros.Text = "Num. Registros:";
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
            finally
            {
                dsAssistido.Dispose();
                dsComposicao.Dispose();
                dsHistorico.Dispose();
            }

        }

        private void cboBuscarPor_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblBuscarPor.Text = cboBuscarPor.Text;
            dtPesquisa.Visible = false;
            cboPesquisa.Visible = false;

            if (cboBuscarPor.Text == "Bairro")
            {
                cboPesquisa.Visible = true;
                txtBusca.Visible = false;
            }
            else if (cboBuscarPor.Text == "Data de Cadastro")
            {
                dtPesquisa.Visible = true;
                cboPesquisa.Visible = false;
                txtBusca.Visible = false;
            }
            else
            {
                cboPesquisa.Visible = false;
                txtBusca.Visible = true;
            }
        }

        private void FrmPesquisa_Load(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            RNAssistidos rn = new RNAssistidos();
            try
            {
                dtPesquisa.Visible = false;
                dt = rn.BuscarBairros(0);
                foreach (DataRow item in dt.Rows)
                {
                    cboPesquisa.Items.Add(item.ItemArray[1]);
                }

                foreach (Control c in this.Controls)
                {
                    foreach (Control y in c.Controls)
                    {
                        foreach (Control x in y.Controls)
                        {
                            if (x.GetType().ToString() == "System.Windows.Forms.TextBox")
                            {
                                TextBox tx = (TextBox)x;
                                tx.ShortcutsEnabled = true;
                            }
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void dtGridResultado_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int indice = 0;
            string numFicha = "";
            try
            {
                DataGridViewRow linhaAtual = dtGridResultado.CurrentRow;
                indice = linhaAtual.Index;

                numFicha = dtGridResultado.Rows[indice].Cells[0].Value.ToString();

                FrmFormulario frm = new FrmFormulario();
                frm.numFicha = numFicha;
                frm.usuarioLogado = FrmLogin.usuarioLogado;
                frm.Show();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }


    }
}
