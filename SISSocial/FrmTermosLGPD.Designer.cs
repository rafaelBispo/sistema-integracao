﻿namespace SISSocial
{
    partial class FrmTermosLGPD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmTermosLGPD));
            this.rTextLGPD = new System.Windows.Forms.RichTextBox();
            this.btnAceitoOK = new System.Windows.Forms.Button();
            this.btnAceitoNOK = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // rTextLGPD
            // 
            this.rTextLGPD.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rTextLGPD.Location = new System.Drawing.Point(12, 12);
            this.rTextLGPD.Name = "rTextLGPD";
            this.rTextLGPD.ReadOnly = true;
            this.rTextLGPD.Size = new System.Drawing.Size(651, 297);
            this.rTextLGPD.TabIndex = 1;
            this.rTextLGPD.Text = resources.GetString("rTextLGPD.Text");
            this.rTextLGPD.VScroll += new System.EventHandler(this.rTextLGPD_VScroll);
            // 
            // btnAceitoOK
            // 
            this.btnAceitoOK.Location = new System.Drawing.Point(498, 345);
            this.btnAceitoOK.Name = "btnAceitoOK";
            this.btnAceitoOK.Size = new System.Drawing.Size(75, 23);
            this.btnAceitoOK.TabIndex = 2;
            this.btnAceitoOK.Text = "Eu aceito";
            this.btnAceitoOK.UseVisualStyleBackColor = true;
            this.btnAceitoOK.Click += new System.EventHandler(this.btnAceitoOK_Click);
            // 
            // btnAceitoNOK
            // 
            this.btnAceitoNOK.Location = new System.Drawing.Point(588, 345);
            this.btnAceitoNOK.Name = "btnAceitoNOK";
            this.btnAceitoNOK.Size = new System.Drawing.Size(75, 23);
            this.btnAceitoNOK.TabIndex = 3;
            this.btnAceitoNOK.Text = "Não aceito";
            this.btnAceitoNOK.UseVisualStyleBackColor = true;
            this.btnAceitoNOK.Click += new System.EventHandler(this.btnAceitoNOK_Click);
            // 
            // btnImprimir
            // 
            this.btnImprimir.Location = new System.Drawing.Point(12, 345);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(75, 23);
            this.btnImprimir.TabIndex = 4;
            this.btnImprimir.Text = "Imprimir";
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // FrmTermosLGPD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 394);
            this.Controls.Add(this.btnImprimir);
            this.Controls.Add(this.btnAceitoNOK);
            this.Controls.Add(this.btnAceitoOK);
            this.Controls.Add(this.rTextLGPD);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmTermosLGPD";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Termos LGPD";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox rTextLGPD;
        private System.Windows.Forms.Button btnAceitoOK;
        private System.Windows.Forms.Button btnAceitoNOK;
        private System.Windows.Forms.Button btnImprimir;
    }
}