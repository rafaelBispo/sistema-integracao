﻿using DocumentFormat.OpenXml.Packaging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace SISSocial
{
    public class GerarWord
    {

        public void CriaArquivo()
        {
            var wordApp = new Microsoft.Office.Interop.Word.Application();
            wordApp.Visible = false;
            var wordDoc = wordApp.Documents.Add();

            var paragrafo0 = wordDoc.Paragraphs.Add();
            var tabelaP0 = wordDoc.Tables.Add(paragrafo0.Range, 1, 1);
            tabelaP0.Rows[1].Cells[1].Range.Text = "Associação de Apoio a Crianças e Idosos - AACI ";
            tabelaP0.Rows[1].Cells[1].Range.InlineShapes.AddPicture(System.IO.Path.Combine(Application.StartupPath, "aaci.jpg"));
            tabelaP0.Borders.InsideLineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
            tabelaP0.Borders.OutsideLineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
            tabelaP0.Select();
            wordApp.Selection.ClearFormatting();
            wordApp.Selection.Collapse();
            paragrafo0.Range.InsertParagraphAfter();

            var paragrafo1 = wordDoc.Content.Paragraphs.Add();
            paragrafo1.Range.Text = "Texto centralizado";
            paragrafo1.Range.ParagraphFormat.Alignment = Microsoft.Office.Interop.Word.WdParagraphAlignment.wdAlignParagraphCenter;
            paragrafo1.Range.InsertParagraphAfter();

            var paragrafo2 = wordDoc.Paragraphs.Add();
            paragrafo2.Range.Text = "Fonte Arial Negrito, Itálico, Sublinhado, Tamanho 18";
            paragrafo2.Range.Font.Name = "Arial";
            paragrafo2.Range.Font.Bold = 1;
            paragrafo2.Range.Font.Italic = 1;
            paragrafo2.Range.Font.Underline = Microsoft.Office.Interop.Word.WdUnderline.wdUnderlineSingle;
            paragrafo2.Range.Font.Size = 18;
            paragrafo2.Range.InsertParagraphAfter();

            var paragrafo3 = wordDoc.Paragraphs.Add();
            paragrafo3.Range.Text = "Um pedaço da frase normal, outro pedaço negrito, outro sublinhado";
            paragrafo3.Range.Select();
            wordApp.Selection.ClearFormatting();
            wordApp.Selection.Collapse();
            var rangeNegrito = wordDoc.Range(paragrafo3.Range.Start + 27, paragrafo3.Range.Start + 47);
            rangeNegrito.Bold = 1;
            var rangeSublinhado = wordDoc.Range(paragrafo3.Range.Start + 49);
            rangeSublinhado.Underline = Microsoft.Office.Interop.Word.WdUnderline.wdUnderlineSingle;
            paragrafo3.Range.InsertParagraphAfter();

            var paragrafo4 = wordDoc.Paragraphs.Add();
            var tabela = wordDoc.Tables.Add(paragrafo4.Range, 2, 3);
            tabela.Rows[1].Cells[1].Range.Text = "Col1";
            tabela.Rows[1].Cells[2].Range.Text = "Col2";
            tabela.Rows[2].Cells[1].Range.Text = "A";
            tabela.Rows[2].Cells[2].Range.Text = "B";
            tabela.Rows[1].Cells[1].Range.InlineShapes.AddPicture(System.IO.Path.Combine(Application.StartupPath, "aaci.jpg"));
            tabela.Rows[1].Cells[2].Range.Text = "Col2";
            tabela.Borders.InsideLineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
            tabela.Borders.OutsideLineStyle = Microsoft.Office.Interop.Word.WdLineStyle.wdLineStyleSingle;
            tabela.Select();
            wordApp.Selection.ClearFormatting();
            wordApp.Selection.Collapse();
            paragrafo4.Range.InsertParagraphAfter();



            wordDoc.SaveAs2(System.IO.Path.Combine(Application.StartupPath, "documentoAutomation.docx"));
            wordApp.DisplayAlerts = Microsoft.Office.Interop.Word.WdAlertLevel.wdAlertsNone;
            wordApp.Quit();

        }

        public static Boolean PreencherCampos(string templatePath, string outputPath, Dictionary<string, string> fields)
        {
            try
            {
                File.Copy(templatePath, outputPath, true);

                using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(outputPath, true))
                {
                    string docText = null;
                    using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
                    {
                        docText = sr.ReadToEnd();
                    }

                    foreach (var field in fields)
                    {
                        docText = docText.Replace(field.Key, field.Value);
                    }

                    using (StreamWriter sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
                    {
                        sw.Write(docText);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
                return false;
            }
            
        }

    }
}
