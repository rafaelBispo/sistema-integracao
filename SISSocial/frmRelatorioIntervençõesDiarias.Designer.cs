﻿namespace SISSocial
{
    partial class frmRelatorioIntervençõesDiarias
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cboRelatorios = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtData = new System.Windows.Forms.MaskedTextBox();
            this.txtTecnicoResponsavel = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtQtdEncaminhamentos4 = new System.Windows.Forms.TextBox();
            this.txtParaEncaminhamentos4 = new System.Windows.Forms.TextBox();
            this.txtQtdEncaminhamentos3 = new System.Windows.Forms.TextBox();
            this.txtParaEncaminhamentos3 = new System.Windows.Forms.TextBox();
            this.txtQtdEncaminhamentos2 = new System.Windows.Forms.TextBox();
            this.txtParaEncaminhamentos2 = new System.Windows.Forms.TextBox();
            this.txtQtdEncaminhamentos1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtParaEncaminhamentos1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.txtDesligadosNum10 = new System.Windows.Forms.TextBox();
            this.txtDesligadosNome10 = new System.Windows.Forms.TextBox();
            this.txtDesligadosNum9 = new System.Windows.Forms.TextBox();
            this.txtDesligadosNome9 = new System.Windows.Forms.TextBox();
            this.txtDesligadosNum8 = new System.Windows.Forms.TextBox();
            this.txtDesligadosNome8 = new System.Windows.Forms.TextBox();
            this.txtDesligadosNum7 = new System.Windows.Forms.TextBox();
            this.txtDesligadosNome7 = new System.Windows.Forms.TextBox();
            this.txtDesligadosNum6 = new System.Windows.Forms.TextBox();
            this.txtDesligadosNome6 = new System.Windows.Forms.TextBox();
            this.txtDesligadosNum5 = new System.Windows.Forms.TextBox();
            this.txtDesligadosNome5 = new System.Windows.Forms.TextBox();
            this.txtDesligadosNum4 = new System.Windows.Forms.TextBox();
            this.txtDesligadosNome4 = new System.Windows.Forms.TextBox();
            this.txtDesligadosNum3 = new System.Windows.Forms.TextBox();
            this.txtDesligadosNome3 = new System.Windows.Forms.TextBox();
            this.txtDesligadosNum2 = new System.Windows.Forms.TextBox();
            this.txtDesligadosNome2 = new System.Windows.Forms.TextBox();
            this.txtDesligadosNum1 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtDesligadosNome1 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtReincidentesNum10 = new System.Windows.Forms.TextBox();
            this.txtReincidentesNome10 = new System.Windows.Forms.TextBox();
            this.txtReincidentesNum9 = new System.Windows.Forms.TextBox();
            this.txtReincidentesNome9 = new System.Windows.Forms.TextBox();
            this.txtReincidentesNum8 = new System.Windows.Forms.TextBox();
            this.txtReincidentesNome8 = new System.Windows.Forms.TextBox();
            this.txtReincidentesNum7 = new System.Windows.Forms.TextBox();
            this.txtReincidentesNome7 = new System.Windows.Forms.TextBox();
            this.txtReincidentesNum6 = new System.Windows.Forms.TextBox();
            this.txtReincidentesNome6 = new System.Windows.Forms.TextBox();
            this.txtReincidentesNum5 = new System.Windows.Forms.TextBox();
            this.txtReincidentesNome5 = new System.Windows.Forms.TextBox();
            this.txtReincidentesNum4 = new System.Windows.Forms.TextBox();
            this.txtReincidentesNome4 = new System.Windows.Forms.TextBox();
            this.txtReincidentesNum3 = new System.Windows.Forms.TextBox();
            this.txtReincidentesNome3 = new System.Windows.Forms.TextBox();
            this.txtReincidentesNum2 = new System.Windows.Forms.TextBox();
            this.txtReincidentesNome2 = new System.Windows.Forms.TextBox();
            this.txtReincidentesNum1 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtReincidentesNome1 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtNumProntuarioNovos10 = new System.Windows.Forms.TextBox();
            this.txtProntuariosNovo10 = new System.Windows.Forms.TextBox();
            this.txtNumProntuarioNovos9 = new System.Windows.Forms.TextBox();
            this.txtProntuariosNovo9 = new System.Windows.Forms.TextBox();
            this.txtNumProntuarioNovos8 = new System.Windows.Forms.TextBox();
            this.txtProntuariosNovo8 = new System.Windows.Forms.TextBox();
            this.txtNumProntuarioNovos7 = new System.Windows.Forms.TextBox();
            this.txtProntuariosNovo7 = new System.Windows.Forms.TextBox();
            this.txtNumProntuarioNovos6 = new System.Windows.Forms.TextBox();
            this.txtProntuariosNovo6 = new System.Windows.Forms.TextBox();
            this.txtNumProntuarioNovos5 = new System.Windows.Forms.TextBox();
            this.txtProntuariosNovo5 = new System.Windows.Forms.TextBox();
            this.txtNumProntuarioNovos4 = new System.Windows.Forms.TextBox();
            this.txtProntuariosNovo4 = new System.Windows.Forms.TextBox();
            this.txtNumProntuarioNovos3 = new System.Windows.Forms.TextBox();
            this.txtProntuariosNovo3 = new System.Windows.Forms.TextBox();
            this.txtNumProntuarioNovos2 = new System.Windows.Forms.TextBox();
            this.txtProntuariosNovo2 = new System.Windows.Forms.TextBox();
            this.txtNumProntuarioNovos1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtProntuariosNovo1 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtQtdVisitas4 = new System.Windows.Forms.TextBox();
            this.txtParaVisitas4 = new System.Windows.Forms.TextBox();
            this.txtQtdVisitas3 = new System.Windows.Forms.TextBox();
            this.txtParaVisitas3 = new System.Windows.Forms.TextBox();
            this.txtQtdVisitas2 = new System.Windows.Forms.TextBox();
            this.txtParaVisitas2 = new System.Windows.Forms.TextBox();
            this.txtQtdVisitas1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtParaVisitas1 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.txtQtdRelatorios4 = new System.Windows.Forms.TextBox();
            this.txtParaRelatorios4 = new System.Windows.Forms.TextBox();
            this.txtQtdRelatorios3 = new System.Windows.Forms.TextBox();
            this.txtParaRelatorios3 = new System.Windows.Forms.TextBox();
            this.txtQtdRelatorios2 = new System.Windows.Forms.TextBox();
            this.txtParaRelatorios2 = new System.Windows.Forms.TextBox();
            this.txtQtdRelatorios1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtParaRelatorios1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.btnNovo = new System.Windows.Forms.Button();
            this.btnSalvar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.lblOut = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cboRelatorios);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.txtData);
            this.groupBox2.Controls.Add(this.txtTecnicoResponsavel);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1222, 46);
            this.groupBox2.TabIndex = 34;
            this.groupBox2.TabStop = false;
            // 
            // cboRelatorios
            // 
            this.cboRelatorios.FormattingEnabled = true;
            this.cboRelatorios.Location = new System.Drawing.Point(841, 14);
            this.cboRelatorios.Name = "cboRelatorios";
            this.cboRelatorios.Size = new System.Drawing.Size(274, 21);
            this.cboRelatorios.TabIndex = 44;
            this.cboRelatorios.SelectedIndexChanged += new System.EventHandler(this.cboRelatorios_SelectedIndexChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(667, 17);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(155, 15);
            this.label17.TabIndex = 43;
            this.label17.Text = "Relatorios Disponiveis:";
            // 
            // txtData
            // 
            this.txtData.Location = new System.Drawing.Point(475, 13);
            this.txtData.Mask = "00/00/0000";
            this.txtData.Name = "txtData";
            this.txtData.Size = new System.Drawing.Size(75, 20);
            this.txtData.TabIndex = 42;
            // 
            // txtTecnicoResponsavel
            // 
            this.txtTecnicoResponsavel.Location = new System.Drawing.Point(161, 12);
            this.txtTecnicoResponsavel.Name = "txtTecnicoResponsavel";
            this.txtTecnicoResponsavel.Size = new System.Drawing.Size(251, 20);
            this.txtTecnicoResponsavel.TabIndex = 36;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(424, 14);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(45, 15);
            this.label18.TabIndex = 33;
            this.label18.Text = "Data: ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(8, 13);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(147, 15);
            this.label11.TabIndex = 31;
            this.label11.Text = "Técnico Responsável:";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Silver;
            this.groupBox1.Controls.Add(this.txtQtdEncaminhamentos4);
            this.groupBox1.Controls.Add(this.txtParaEncaminhamentos4);
            this.groupBox1.Controls.Add(this.txtQtdEncaminhamentos3);
            this.groupBox1.Controls.Add(this.txtParaEncaminhamentos3);
            this.groupBox1.Controls.Add(this.txtQtdEncaminhamentos2);
            this.groupBox1.Controls.Add(this.txtParaEncaminhamentos2);
            this.groupBox1.Controls.Add(this.txtQtdEncaminhamentos1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtParaEncaminhamentos1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(18, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(350, 169);
            this.groupBox1.TabIndex = 35;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "ENCAMINHAMENTOS REALIZADOS";
            // 
            // txtQtdEncaminhamentos4
            // 
            this.txtQtdEncaminhamentos4.Location = new System.Drawing.Point(254, 110);
            this.txtQtdEncaminhamentos4.Name = "txtQtdEncaminhamentos4";
            this.txtQtdEncaminhamentos4.Size = new System.Drawing.Size(90, 20);
            this.txtQtdEncaminhamentos4.TabIndex = 9;
            // 
            // txtParaEncaminhamentos4
            // 
            this.txtParaEncaminhamentos4.Location = new System.Drawing.Point(11, 110);
            this.txtParaEncaminhamentos4.Name = "txtParaEncaminhamentos4";
            this.txtParaEncaminhamentos4.Size = new System.Drawing.Size(237, 20);
            this.txtParaEncaminhamentos4.TabIndex = 8;
            // 
            // txtQtdEncaminhamentos3
            // 
            this.txtQtdEncaminhamentos3.Location = new System.Drawing.Point(254, 84);
            this.txtQtdEncaminhamentos3.Name = "txtQtdEncaminhamentos3";
            this.txtQtdEncaminhamentos3.Size = new System.Drawing.Size(90, 20);
            this.txtQtdEncaminhamentos3.TabIndex = 7;
            // 
            // txtParaEncaminhamentos3
            // 
            this.txtParaEncaminhamentos3.Location = new System.Drawing.Point(11, 84);
            this.txtParaEncaminhamentos3.Name = "txtParaEncaminhamentos3";
            this.txtParaEncaminhamentos3.Size = new System.Drawing.Size(237, 20);
            this.txtParaEncaminhamentos3.TabIndex = 6;
            // 
            // txtQtdEncaminhamentos2
            // 
            this.txtQtdEncaminhamentos2.Location = new System.Drawing.Point(254, 58);
            this.txtQtdEncaminhamentos2.Name = "txtQtdEncaminhamentos2";
            this.txtQtdEncaminhamentos2.Size = new System.Drawing.Size(90, 20);
            this.txtQtdEncaminhamentos2.TabIndex = 5;
            // 
            // txtParaEncaminhamentos2
            // 
            this.txtParaEncaminhamentos2.Location = new System.Drawing.Point(11, 58);
            this.txtParaEncaminhamentos2.Name = "txtParaEncaminhamentos2";
            this.txtParaEncaminhamentos2.Size = new System.Drawing.Size(237, 20);
            this.txtParaEncaminhamentos2.TabIndex = 4;
            // 
            // txtQtdEncaminhamentos1
            // 
            this.txtQtdEncaminhamentos1.Location = new System.Drawing.Point(254, 32);
            this.txtQtdEncaminhamentos1.Name = "txtQtdEncaminhamentos1";
            this.txtQtdEncaminhamentos1.Size = new System.Drawing.Size(90, 20);
            this.txtQtdEncaminhamentos1.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(251, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Quantidade:";
            // 
            // txtParaEncaminhamentos1
            // 
            this.txtParaEncaminhamentos1.Location = new System.Drawing.Point(11, 32);
            this.txtParaEncaminhamentos1.Name = "txtParaEncaminhamentos1";
            this.txtParaEncaminhamentos1.Size = new System.Drawing.Size(237, 20);
            this.txtParaEncaminhamentos1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Para:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 64);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1237, 559);
            this.tabControl1.TabIndex = 36;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1229, 533);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Silver;
            this.groupBox5.Controls.Add(this.groupBox8);
            this.groupBox5.Controls.Add(this.groupBox7);
            this.groupBox5.Controls.Add(this.groupBox6);
            this.groupBox5.Location = new System.Drawing.Point(7, 188);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(1211, 335);
            this.groupBox5.TabIndex = 37;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "ACOMPANHAMENTO";
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.Silver;
            this.groupBox8.Controls.Add(this.txtDesligadosNum10);
            this.groupBox8.Controls.Add(this.txtDesligadosNome10);
            this.groupBox8.Controls.Add(this.txtDesligadosNum9);
            this.groupBox8.Controls.Add(this.txtDesligadosNome9);
            this.groupBox8.Controls.Add(this.txtDesligadosNum8);
            this.groupBox8.Controls.Add(this.txtDesligadosNome8);
            this.groupBox8.Controls.Add(this.txtDesligadosNum7);
            this.groupBox8.Controls.Add(this.txtDesligadosNome7);
            this.groupBox8.Controls.Add(this.txtDesligadosNum6);
            this.groupBox8.Controls.Add(this.txtDesligadosNome6);
            this.groupBox8.Controls.Add(this.txtDesligadosNum5);
            this.groupBox8.Controls.Add(this.txtDesligadosNome5);
            this.groupBox8.Controls.Add(this.txtDesligadosNum4);
            this.groupBox8.Controls.Add(this.txtDesligadosNome4);
            this.groupBox8.Controls.Add(this.txtDesligadosNum3);
            this.groupBox8.Controls.Add(this.txtDesligadosNome3);
            this.groupBox8.Controls.Add(this.txtDesligadosNum2);
            this.groupBox8.Controls.Add(this.txtDesligadosNome2);
            this.groupBox8.Controls.Add(this.txtDesligadosNum1);
            this.groupBox8.Controls.Add(this.label12);
            this.groupBox8.Controls.Add(this.txtDesligadosNome1);
            this.groupBox8.Controls.Add(this.label13);
            this.groupBox8.Location = new System.Drawing.Point(805, 19);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(390, 307);
            this.groupBox8.TabIndex = 37;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "PRONTUÁRIOS DESLIGADOS";
            // 
            // txtDesligadosNum10
            // 
            this.txtDesligadosNum10.Location = new System.Drawing.Point(243, 266);
            this.txtDesligadosNum10.Name = "txtDesligadosNum10";
            this.txtDesligadosNum10.Size = new System.Drawing.Size(136, 20);
            this.txtDesligadosNum10.TabIndex = 21;
            // 
            // txtDesligadosNome10
            // 
            this.txtDesligadosNome10.Location = new System.Drawing.Point(11, 266);
            this.txtDesligadosNome10.Name = "txtDesligadosNome10";
            this.txtDesligadosNome10.Size = new System.Drawing.Size(226, 20);
            this.txtDesligadosNome10.TabIndex = 20;
            // 
            // txtDesligadosNum9
            // 
            this.txtDesligadosNum9.Location = new System.Drawing.Point(243, 240);
            this.txtDesligadosNum9.Name = "txtDesligadosNum9";
            this.txtDesligadosNum9.Size = new System.Drawing.Size(136, 20);
            this.txtDesligadosNum9.TabIndex = 19;
            // 
            // txtDesligadosNome9
            // 
            this.txtDesligadosNome9.Location = new System.Drawing.Point(11, 240);
            this.txtDesligadosNome9.Name = "txtDesligadosNome9";
            this.txtDesligadosNome9.Size = new System.Drawing.Size(226, 20);
            this.txtDesligadosNome9.TabIndex = 18;
            // 
            // txtDesligadosNum8
            // 
            this.txtDesligadosNum8.Location = new System.Drawing.Point(243, 214);
            this.txtDesligadosNum8.Name = "txtDesligadosNum8";
            this.txtDesligadosNum8.Size = new System.Drawing.Size(136, 20);
            this.txtDesligadosNum8.TabIndex = 17;
            // 
            // txtDesligadosNome8
            // 
            this.txtDesligadosNome8.Location = new System.Drawing.Point(11, 214);
            this.txtDesligadosNome8.Name = "txtDesligadosNome8";
            this.txtDesligadosNome8.Size = new System.Drawing.Size(226, 20);
            this.txtDesligadosNome8.TabIndex = 16;
            // 
            // txtDesligadosNum7
            // 
            this.txtDesligadosNum7.Location = new System.Drawing.Point(243, 188);
            this.txtDesligadosNum7.Name = "txtDesligadosNum7";
            this.txtDesligadosNum7.Size = new System.Drawing.Size(136, 20);
            this.txtDesligadosNum7.TabIndex = 15;
            // 
            // txtDesligadosNome7
            // 
            this.txtDesligadosNome7.Location = new System.Drawing.Point(11, 188);
            this.txtDesligadosNome7.Name = "txtDesligadosNome7";
            this.txtDesligadosNome7.Size = new System.Drawing.Size(226, 20);
            this.txtDesligadosNome7.TabIndex = 14;
            // 
            // txtDesligadosNum6
            // 
            this.txtDesligadosNum6.Location = new System.Drawing.Point(243, 162);
            this.txtDesligadosNum6.Name = "txtDesligadosNum6";
            this.txtDesligadosNum6.Size = new System.Drawing.Size(136, 20);
            this.txtDesligadosNum6.TabIndex = 13;
            // 
            // txtDesligadosNome6
            // 
            this.txtDesligadosNome6.Location = new System.Drawing.Point(11, 162);
            this.txtDesligadosNome6.Name = "txtDesligadosNome6";
            this.txtDesligadosNome6.Size = new System.Drawing.Size(226, 20);
            this.txtDesligadosNome6.TabIndex = 12;
            // 
            // txtDesligadosNum5
            // 
            this.txtDesligadosNum5.Location = new System.Drawing.Point(243, 136);
            this.txtDesligadosNum5.Name = "txtDesligadosNum5";
            this.txtDesligadosNum5.Size = new System.Drawing.Size(136, 20);
            this.txtDesligadosNum5.TabIndex = 11;
            // 
            // txtDesligadosNome5
            // 
            this.txtDesligadosNome5.Location = new System.Drawing.Point(11, 136);
            this.txtDesligadosNome5.Name = "txtDesligadosNome5";
            this.txtDesligadosNome5.Size = new System.Drawing.Size(226, 20);
            this.txtDesligadosNome5.TabIndex = 10;
            // 
            // txtDesligadosNum4
            // 
            this.txtDesligadosNum4.Location = new System.Drawing.Point(243, 110);
            this.txtDesligadosNum4.Name = "txtDesligadosNum4";
            this.txtDesligadosNum4.Size = new System.Drawing.Size(136, 20);
            this.txtDesligadosNum4.TabIndex = 9;
            // 
            // txtDesligadosNome4
            // 
            this.txtDesligadosNome4.Location = new System.Drawing.Point(11, 110);
            this.txtDesligadosNome4.Name = "txtDesligadosNome4";
            this.txtDesligadosNome4.Size = new System.Drawing.Size(226, 20);
            this.txtDesligadosNome4.TabIndex = 8;
            // 
            // txtDesligadosNum3
            // 
            this.txtDesligadosNum3.Location = new System.Drawing.Point(243, 84);
            this.txtDesligadosNum3.Name = "txtDesligadosNum3";
            this.txtDesligadosNum3.Size = new System.Drawing.Size(136, 20);
            this.txtDesligadosNum3.TabIndex = 7;
            // 
            // txtDesligadosNome3
            // 
            this.txtDesligadosNome3.Location = new System.Drawing.Point(11, 84);
            this.txtDesligadosNome3.Name = "txtDesligadosNome3";
            this.txtDesligadosNome3.Size = new System.Drawing.Size(226, 20);
            this.txtDesligadosNome3.TabIndex = 6;
            // 
            // txtDesligadosNum2
            // 
            this.txtDesligadosNum2.Location = new System.Drawing.Point(243, 58);
            this.txtDesligadosNum2.Name = "txtDesligadosNum2";
            this.txtDesligadosNum2.Size = new System.Drawing.Size(136, 20);
            this.txtDesligadosNum2.TabIndex = 5;
            // 
            // txtDesligadosNome2
            // 
            this.txtDesligadosNome2.Location = new System.Drawing.Point(11, 58);
            this.txtDesligadosNome2.Name = "txtDesligadosNome2";
            this.txtDesligadosNome2.Size = new System.Drawing.Size(226, 20);
            this.txtDesligadosNome2.TabIndex = 4;
            // 
            // txtDesligadosNum1
            // 
            this.txtDesligadosNum1.Location = new System.Drawing.Point(243, 32);
            this.txtDesligadosNum1.Name = "txtDesligadosNum1";
            this.txtDesligadosNum1.Size = new System.Drawing.Size(136, 20);
            this.txtDesligadosNum1.TabIndex = 3;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(240, 16);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 13);
            this.label12.TabIndex = 2;
            this.label12.Text = "N° Prontuário";
            // 
            // txtDesligadosNome1
            // 
            this.txtDesligadosNome1.Location = new System.Drawing.Point(11, 32);
            this.txtDesligadosNome1.Name = "txtDesligadosNome1";
            this.txtDesligadosNome1.Size = new System.Drawing.Size(226, 20);
            this.txtDesligadosNome1.TabIndex = 1;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(8, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(43, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Nome:";
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.Silver;
            this.groupBox7.Controls.Add(this.txtReincidentesNum10);
            this.groupBox7.Controls.Add(this.txtReincidentesNome10);
            this.groupBox7.Controls.Add(this.txtReincidentesNum9);
            this.groupBox7.Controls.Add(this.txtReincidentesNome9);
            this.groupBox7.Controls.Add(this.txtReincidentesNum8);
            this.groupBox7.Controls.Add(this.txtReincidentesNome8);
            this.groupBox7.Controls.Add(this.txtReincidentesNum7);
            this.groupBox7.Controls.Add(this.txtReincidentesNome7);
            this.groupBox7.Controls.Add(this.txtReincidentesNum6);
            this.groupBox7.Controls.Add(this.txtReincidentesNome6);
            this.groupBox7.Controls.Add(this.txtReincidentesNum5);
            this.groupBox7.Controls.Add(this.txtReincidentesNome5);
            this.groupBox7.Controls.Add(this.txtReincidentesNum4);
            this.groupBox7.Controls.Add(this.txtReincidentesNome4);
            this.groupBox7.Controls.Add(this.txtReincidentesNum3);
            this.groupBox7.Controls.Add(this.txtReincidentesNome3);
            this.groupBox7.Controls.Add(this.txtReincidentesNum2);
            this.groupBox7.Controls.Add(this.txtReincidentesNome2);
            this.groupBox7.Controls.Add(this.txtReincidentesNum1);
            this.groupBox7.Controls.Add(this.label9);
            this.groupBox7.Controls.Add(this.txtReincidentesNome1);
            this.groupBox7.Controls.Add(this.label10);
            this.groupBox7.Location = new System.Drawing.Point(409, 19);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(390, 307);
            this.groupBox7.TabIndex = 37;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "PRONTUÁRIOS REINCIDENTES";
            // 
            // txtReincidentesNum10
            // 
            this.txtReincidentesNum10.Location = new System.Drawing.Point(243, 266);
            this.txtReincidentesNum10.Name = "txtReincidentesNum10";
            this.txtReincidentesNum10.Size = new System.Drawing.Size(136, 20);
            this.txtReincidentesNum10.TabIndex = 21;
            // 
            // txtReincidentesNome10
            // 
            this.txtReincidentesNome10.Location = new System.Drawing.Point(11, 266);
            this.txtReincidentesNome10.Name = "txtReincidentesNome10";
            this.txtReincidentesNome10.Size = new System.Drawing.Size(226, 20);
            this.txtReincidentesNome10.TabIndex = 20;
            // 
            // txtReincidentesNum9
            // 
            this.txtReincidentesNum9.Location = new System.Drawing.Point(243, 240);
            this.txtReincidentesNum9.Name = "txtReincidentesNum9";
            this.txtReincidentesNum9.Size = new System.Drawing.Size(136, 20);
            this.txtReincidentesNum9.TabIndex = 19;
            // 
            // txtReincidentesNome9
            // 
            this.txtReincidentesNome9.Location = new System.Drawing.Point(11, 240);
            this.txtReincidentesNome9.Name = "txtReincidentesNome9";
            this.txtReincidentesNome9.Size = new System.Drawing.Size(226, 20);
            this.txtReincidentesNome9.TabIndex = 18;
            // 
            // txtReincidentesNum8
            // 
            this.txtReincidentesNum8.Location = new System.Drawing.Point(243, 214);
            this.txtReincidentesNum8.Name = "txtReincidentesNum8";
            this.txtReincidentesNum8.Size = new System.Drawing.Size(136, 20);
            this.txtReincidentesNum8.TabIndex = 17;
            // 
            // txtReincidentesNome8
            // 
            this.txtReincidentesNome8.Location = new System.Drawing.Point(11, 214);
            this.txtReincidentesNome8.Name = "txtReincidentesNome8";
            this.txtReincidentesNome8.Size = new System.Drawing.Size(226, 20);
            this.txtReincidentesNome8.TabIndex = 16;
            // 
            // txtReincidentesNum7
            // 
            this.txtReincidentesNum7.Location = new System.Drawing.Point(243, 188);
            this.txtReincidentesNum7.Name = "txtReincidentesNum7";
            this.txtReincidentesNum7.Size = new System.Drawing.Size(136, 20);
            this.txtReincidentesNum7.TabIndex = 15;
            // 
            // txtReincidentesNome7
            // 
            this.txtReincidentesNome7.Location = new System.Drawing.Point(11, 188);
            this.txtReincidentesNome7.Name = "txtReincidentesNome7";
            this.txtReincidentesNome7.Size = new System.Drawing.Size(226, 20);
            this.txtReincidentesNome7.TabIndex = 14;
            // 
            // txtReincidentesNum6
            // 
            this.txtReincidentesNum6.Location = new System.Drawing.Point(243, 162);
            this.txtReincidentesNum6.Name = "txtReincidentesNum6";
            this.txtReincidentesNum6.Size = new System.Drawing.Size(136, 20);
            this.txtReincidentesNum6.TabIndex = 13;
            // 
            // txtReincidentesNome6
            // 
            this.txtReincidentesNome6.Location = new System.Drawing.Point(11, 162);
            this.txtReincidentesNome6.Name = "txtReincidentesNome6";
            this.txtReincidentesNome6.Size = new System.Drawing.Size(226, 20);
            this.txtReincidentesNome6.TabIndex = 12;
            // 
            // txtReincidentesNum5
            // 
            this.txtReincidentesNum5.Location = new System.Drawing.Point(243, 136);
            this.txtReincidentesNum5.Name = "txtReincidentesNum5";
            this.txtReincidentesNum5.Size = new System.Drawing.Size(136, 20);
            this.txtReincidentesNum5.TabIndex = 11;
            // 
            // txtReincidentesNome5
            // 
            this.txtReincidentesNome5.Location = new System.Drawing.Point(11, 136);
            this.txtReincidentesNome5.Name = "txtReincidentesNome5";
            this.txtReincidentesNome5.Size = new System.Drawing.Size(226, 20);
            this.txtReincidentesNome5.TabIndex = 10;
            // 
            // txtReincidentesNum4
            // 
            this.txtReincidentesNum4.Location = new System.Drawing.Point(243, 110);
            this.txtReincidentesNum4.Name = "txtReincidentesNum4";
            this.txtReincidentesNum4.Size = new System.Drawing.Size(136, 20);
            this.txtReincidentesNum4.TabIndex = 9;
            // 
            // txtReincidentesNome4
            // 
            this.txtReincidentesNome4.Location = new System.Drawing.Point(11, 110);
            this.txtReincidentesNome4.Name = "txtReincidentesNome4";
            this.txtReincidentesNome4.Size = new System.Drawing.Size(226, 20);
            this.txtReincidentesNome4.TabIndex = 8;
            // 
            // txtReincidentesNum3
            // 
            this.txtReincidentesNum3.Location = new System.Drawing.Point(243, 84);
            this.txtReincidentesNum3.Name = "txtReincidentesNum3";
            this.txtReincidentesNum3.Size = new System.Drawing.Size(136, 20);
            this.txtReincidentesNum3.TabIndex = 7;
            // 
            // txtReincidentesNome3
            // 
            this.txtReincidentesNome3.Location = new System.Drawing.Point(11, 84);
            this.txtReincidentesNome3.Name = "txtReincidentesNome3";
            this.txtReincidentesNome3.Size = new System.Drawing.Size(226, 20);
            this.txtReincidentesNome3.TabIndex = 6;
            // 
            // txtReincidentesNum2
            // 
            this.txtReincidentesNum2.Location = new System.Drawing.Point(243, 58);
            this.txtReincidentesNum2.Name = "txtReincidentesNum2";
            this.txtReincidentesNum2.Size = new System.Drawing.Size(136, 20);
            this.txtReincidentesNum2.TabIndex = 5;
            // 
            // txtReincidentesNome2
            // 
            this.txtReincidentesNome2.Location = new System.Drawing.Point(11, 58);
            this.txtReincidentesNome2.Name = "txtReincidentesNome2";
            this.txtReincidentesNome2.Size = new System.Drawing.Size(226, 20);
            this.txtReincidentesNome2.TabIndex = 4;
            // 
            // txtReincidentesNum1
            // 
            this.txtReincidentesNum1.Location = new System.Drawing.Point(243, 32);
            this.txtReincidentesNum1.Name = "txtReincidentesNum1";
            this.txtReincidentesNum1.Size = new System.Drawing.Size(136, 20);
            this.txtReincidentesNum1.TabIndex = 3;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(240, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "N° Prontuário";
            // 
            // txtReincidentesNome1
            // 
            this.txtReincidentesNome1.Location = new System.Drawing.Point(11, 32);
            this.txtReincidentesNome1.Name = "txtReincidentesNome1";
            this.txtReincidentesNome1.Size = new System.Drawing.Size(226, 20);
            this.txtReincidentesNome1.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(8, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Nome:";
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Silver;
            this.groupBox6.Controls.Add(this.txtNumProntuarioNovos10);
            this.groupBox6.Controls.Add(this.txtProntuariosNovo10);
            this.groupBox6.Controls.Add(this.txtNumProntuarioNovos9);
            this.groupBox6.Controls.Add(this.txtProntuariosNovo9);
            this.groupBox6.Controls.Add(this.txtNumProntuarioNovos8);
            this.groupBox6.Controls.Add(this.txtProntuariosNovo8);
            this.groupBox6.Controls.Add(this.txtNumProntuarioNovos7);
            this.groupBox6.Controls.Add(this.txtProntuariosNovo7);
            this.groupBox6.Controls.Add(this.txtNumProntuarioNovos6);
            this.groupBox6.Controls.Add(this.txtProntuariosNovo6);
            this.groupBox6.Controls.Add(this.txtNumProntuarioNovos5);
            this.groupBox6.Controls.Add(this.txtProntuariosNovo5);
            this.groupBox6.Controls.Add(this.txtNumProntuarioNovos4);
            this.groupBox6.Controls.Add(this.txtProntuariosNovo4);
            this.groupBox6.Controls.Add(this.txtNumProntuarioNovos3);
            this.groupBox6.Controls.Add(this.txtProntuariosNovo3);
            this.groupBox6.Controls.Add(this.txtNumProntuarioNovos2);
            this.groupBox6.Controls.Add(this.txtProntuariosNovo2);
            this.groupBox6.Controls.Add(this.txtNumProntuarioNovos1);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this.txtProntuariosNovo1);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Location = new System.Drawing.Point(11, 19);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(390, 307);
            this.groupBox6.TabIndex = 36;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "PRONTUÁRIOS NOVOS";
            // 
            // txtNumProntuarioNovos10
            // 
            this.txtNumProntuarioNovos10.Location = new System.Drawing.Point(243, 266);
            this.txtNumProntuarioNovos10.Name = "txtNumProntuarioNovos10";
            this.txtNumProntuarioNovos10.Size = new System.Drawing.Size(136, 20);
            this.txtNumProntuarioNovos10.TabIndex = 21;
            // 
            // txtProntuariosNovo10
            // 
            this.txtProntuariosNovo10.Location = new System.Drawing.Point(11, 266);
            this.txtProntuariosNovo10.Name = "txtProntuariosNovo10";
            this.txtProntuariosNovo10.Size = new System.Drawing.Size(226, 20);
            this.txtProntuariosNovo10.TabIndex = 20;
            // 
            // txtNumProntuarioNovos9
            // 
            this.txtNumProntuarioNovos9.Location = new System.Drawing.Point(243, 240);
            this.txtNumProntuarioNovos9.Name = "txtNumProntuarioNovos9";
            this.txtNumProntuarioNovos9.Size = new System.Drawing.Size(136, 20);
            this.txtNumProntuarioNovos9.TabIndex = 19;
            // 
            // txtProntuariosNovo9
            // 
            this.txtProntuariosNovo9.Location = new System.Drawing.Point(11, 240);
            this.txtProntuariosNovo9.Name = "txtProntuariosNovo9";
            this.txtProntuariosNovo9.Size = new System.Drawing.Size(226, 20);
            this.txtProntuariosNovo9.TabIndex = 18;
            // 
            // txtNumProntuarioNovos8
            // 
            this.txtNumProntuarioNovos8.Location = new System.Drawing.Point(243, 214);
            this.txtNumProntuarioNovos8.Name = "txtNumProntuarioNovos8";
            this.txtNumProntuarioNovos8.Size = new System.Drawing.Size(136, 20);
            this.txtNumProntuarioNovos8.TabIndex = 17;
            // 
            // txtProntuariosNovo8
            // 
            this.txtProntuariosNovo8.Location = new System.Drawing.Point(11, 214);
            this.txtProntuariosNovo8.Name = "txtProntuariosNovo8";
            this.txtProntuariosNovo8.Size = new System.Drawing.Size(226, 20);
            this.txtProntuariosNovo8.TabIndex = 16;
            // 
            // txtNumProntuarioNovos7
            // 
            this.txtNumProntuarioNovos7.Location = new System.Drawing.Point(243, 188);
            this.txtNumProntuarioNovos7.Name = "txtNumProntuarioNovos7";
            this.txtNumProntuarioNovos7.Size = new System.Drawing.Size(136, 20);
            this.txtNumProntuarioNovos7.TabIndex = 15;
            // 
            // txtProntuariosNovo7
            // 
            this.txtProntuariosNovo7.Location = new System.Drawing.Point(11, 188);
            this.txtProntuariosNovo7.Name = "txtProntuariosNovo7";
            this.txtProntuariosNovo7.Size = new System.Drawing.Size(226, 20);
            this.txtProntuariosNovo7.TabIndex = 14;
            // 
            // txtNumProntuarioNovos6
            // 
            this.txtNumProntuarioNovos6.Location = new System.Drawing.Point(243, 162);
            this.txtNumProntuarioNovos6.Name = "txtNumProntuarioNovos6";
            this.txtNumProntuarioNovos6.Size = new System.Drawing.Size(136, 20);
            this.txtNumProntuarioNovos6.TabIndex = 13;
            // 
            // txtProntuariosNovo6
            // 
            this.txtProntuariosNovo6.Location = new System.Drawing.Point(11, 162);
            this.txtProntuariosNovo6.Name = "txtProntuariosNovo6";
            this.txtProntuariosNovo6.Size = new System.Drawing.Size(226, 20);
            this.txtProntuariosNovo6.TabIndex = 12;
            // 
            // txtNumProntuarioNovos5
            // 
            this.txtNumProntuarioNovos5.Location = new System.Drawing.Point(243, 136);
            this.txtNumProntuarioNovos5.Name = "txtNumProntuarioNovos5";
            this.txtNumProntuarioNovos5.Size = new System.Drawing.Size(136, 20);
            this.txtNumProntuarioNovos5.TabIndex = 11;
            // 
            // txtProntuariosNovo5
            // 
            this.txtProntuariosNovo5.Location = new System.Drawing.Point(11, 136);
            this.txtProntuariosNovo5.Name = "txtProntuariosNovo5";
            this.txtProntuariosNovo5.Size = new System.Drawing.Size(226, 20);
            this.txtProntuariosNovo5.TabIndex = 10;
            // 
            // txtNumProntuarioNovos4
            // 
            this.txtNumProntuarioNovos4.Location = new System.Drawing.Point(243, 110);
            this.txtNumProntuarioNovos4.Name = "txtNumProntuarioNovos4";
            this.txtNumProntuarioNovos4.Size = new System.Drawing.Size(136, 20);
            this.txtNumProntuarioNovos4.TabIndex = 9;
            // 
            // txtProntuariosNovo4
            // 
            this.txtProntuariosNovo4.Location = new System.Drawing.Point(11, 110);
            this.txtProntuariosNovo4.Name = "txtProntuariosNovo4";
            this.txtProntuariosNovo4.Size = new System.Drawing.Size(226, 20);
            this.txtProntuariosNovo4.TabIndex = 8;
            // 
            // txtNumProntuarioNovos3
            // 
            this.txtNumProntuarioNovos3.Location = new System.Drawing.Point(243, 84);
            this.txtNumProntuarioNovos3.Name = "txtNumProntuarioNovos3";
            this.txtNumProntuarioNovos3.Size = new System.Drawing.Size(136, 20);
            this.txtNumProntuarioNovos3.TabIndex = 7;
            // 
            // txtProntuariosNovo3
            // 
            this.txtProntuariosNovo3.Location = new System.Drawing.Point(11, 84);
            this.txtProntuariosNovo3.Name = "txtProntuariosNovo3";
            this.txtProntuariosNovo3.Size = new System.Drawing.Size(226, 20);
            this.txtProntuariosNovo3.TabIndex = 6;
            // 
            // txtNumProntuarioNovos2
            // 
            this.txtNumProntuarioNovos2.Location = new System.Drawing.Point(243, 58);
            this.txtNumProntuarioNovos2.Name = "txtNumProntuarioNovos2";
            this.txtNumProntuarioNovos2.Size = new System.Drawing.Size(136, 20);
            this.txtNumProntuarioNovos2.TabIndex = 5;
            // 
            // txtProntuariosNovo2
            // 
            this.txtProntuariosNovo2.Location = new System.Drawing.Point(11, 58);
            this.txtProntuariosNovo2.Name = "txtProntuariosNovo2";
            this.txtProntuariosNovo2.Size = new System.Drawing.Size(226, 20);
            this.txtProntuariosNovo2.TabIndex = 4;
            // 
            // txtNumProntuarioNovos1
            // 
            this.txtNumProntuarioNovos1.Location = new System.Drawing.Point(243, 32);
            this.txtNumProntuarioNovos1.Name = "txtNumProntuarioNovos1";
            this.txtNumProntuarioNovos1.Size = new System.Drawing.Size(136, 20);
            this.txtNumProntuarioNovos1.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(240, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "N° Prontuário";
            // 
            // txtProntuariosNovo1
            // 
            this.txtProntuariosNovo1.Location = new System.Drawing.Point(11, 32);
            this.txtProntuariosNovo1.Name = "txtProntuariosNovo1";
            this.txtProntuariosNovo1.Size = new System.Drawing.Size(226, 20);
            this.txtProntuariosNovo1.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Nome:";
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Silver;
            this.groupBox4.Controls.Add(this.txtQtdVisitas4);
            this.groupBox4.Controls.Add(this.txtParaVisitas4);
            this.groupBox4.Controls.Add(this.txtQtdVisitas3);
            this.groupBox4.Controls.Add(this.txtParaVisitas3);
            this.groupBox4.Controls.Add(this.txtQtdVisitas2);
            this.groupBox4.Controls.Add(this.txtParaVisitas2);
            this.groupBox4.Controls.Add(this.txtQtdVisitas1);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.txtParaVisitas1);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Location = new System.Drawing.Point(812, 13);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(350, 169);
            this.groupBox4.TabIndex = 36;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "VISITAS REALIZADAS";
            // 
            // txtQtdVisitas4
            // 
            this.txtQtdVisitas4.Location = new System.Drawing.Point(254, 110);
            this.txtQtdVisitas4.Name = "txtQtdVisitas4";
            this.txtQtdVisitas4.Size = new System.Drawing.Size(90, 20);
            this.txtQtdVisitas4.TabIndex = 9;
            // 
            // txtParaVisitas4
            // 
            this.txtParaVisitas4.Location = new System.Drawing.Point(11, 110);
            this.txtParaVisitas4.Name = "txtParaVisitas4";
            this.txtParaVisitas4.Size = new System.Drawing.Size(237, 20);
            this.txtParaVisitas4.TabIndex = 8;
            // 
            // txtQtdVisitas3
            // 
            this.txtQtdVisitas3.Location = new System.Drawing.Point(254, 84);
            this.txtQtdVisitas3.Name = "txtQtdVisitas3";
            this.txtQtdVisitas3.Size = new System.Drawing.Size(90, 20);
            this.txtQtdVisitas3.TabIndex = 7;
            // 
            // txtParaVisitas3
            // 
            this.txtParaVisitas3.Location = new System.Drawing.Point(11, 84);
            this.txtParaVisitas3.Name = "txtParaVisitas3";
            this.txtParaVisitas3.Size = new System.Drawing.Size(237, 20);
            this.txtParaVisitas3.TabIndex = 6;
            // 
            // txtQtdVisitas2
            // 
            this.txtQtdVisitas2.Location = new System.Drawing.Point(254, 58);
            this.txtQtdVisitas2.Name = "txtQtdVisitas2";
            this.txtQtdVisitas2.Size = new System.Drawing.Size(90, 20);
            this.txtQtdVisitas2.TabIndex = 5;
            // 
            // txtParaVisitas2
            // 
            this.txtParaVisitas2.Location = new System.Drawing.Point(11, 58);
            this.txtParaVisitas2.Name = "txtParaVisitas2";
            this.txtParaVisitas2.Size = new System.Drawing.Size(237, 20);
            this.txtParaVisitas2.TabIndex = 4;
            // 
            // txtQtdVisitas1
            // 
            this.txtQtdVisitas1.Location = new System.Drawing.Point(254, 32);
            this.txtQtdVisitas1.Name = "txtQtdVisitas1";
            this.txtQtdVisitas1.Size = new System.Drawing.Size(90, 20);
            this.txtQtdVisitas1.TabIndex = 3;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(251, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Quantidade:";
            // 
            // txtParaVisitas1
            // 
            this.txtParaVisitas1.Location = new System.Drawing.Point(11, 32);
            this.txtParaVisitas1.Name = "txtParaVisitas1";
            this.txtParaVisitas1.Size = new System.Drawing.Size(237, 20);
            this.txtParaVisitas1.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Para:";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Silver;
            this.groupBox3.Controls.Add(this.txtQtdRelatorios4);
            this.groupBox3.Controls.Add(this.txtParaRelatorios4);
            this.groupBox3.Controls.Add(this.txtQtdRelatorios3);
            this.groupBox3.Controls.Add(this.txtParaRelatorios3);
            this.groupBox3.Controls.Add(this.txtQtdRelatorios2);
            this.groupBox3.Controls.Add(this.txtParaRelatorios2);
            this.groupBox3.Controls.Add(this.txtQtdRelatorios1);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.txtParaRelatorios1);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Location = new System.Drawing.Point(416, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(350, 169);
            this.groupBox3.TabIndex = 36;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "RELATÓRIOS REALIZADOS";
            // 
            // txtQtdRelatorios4
            // 
            this.txtQtdRelatorios4.Location = new System.Drawing.Point(254, 110);
            this.txtQtdRelatorios4.Name = "txtQtdRelatorios4";
            this.txtQtdRelatorios4.Size = new System.Drawing.Size(90, 20);
            this.txtQtdRelatorios4.TabIndex = 9;
            // 
            // txtParaRelatorios4
            // 
            this.txtParaRelatorios4.Location = new System.Drawing.Point(11, 110);
            this.txtParaRelatorios4.Name = "txtParaRelatorios4";
            this.txtParaRelatorios4.Size = new System.Drawing.Size(237, 20);
            this.txtParaRelatorios4.TabIndex = 8;
            // 
            // txtQtdRelatorios3
            // 
            this.txtQtdRelatorios3.Location = new System.Drawing.Point(254, 84);
            this.txtQtdRelatorios3.Name = "txtQtdRelatorios3";
            this.txtQtdRelatorios3.Size = new System.Drawing.Size(90, 20);
            this.txtQtdRelatorios3.TabIndex = 7;
            // 
            // txtParaRelatorios3
            // 
            this.txtParaRelatorios3.Location = new System.Drawing.Point(11, 84);
            this.txtParaRelatorios3.Name = "txtParaRelatorios3";
            this.txtParaRelatorios3.Size = new System.Drawing.Size(237, 20);
            this.txtParaRelatorios3.TabIndex = 6;
            // 
            // txtQtdRelatorios2
            // 
            this.txtQtdRelatorios2.Location = new System.Drawing.Point(254, 58);
            this.txtQtdRelatorios2.Name = "txtQtdRelatorios2";
            this.txtQtdRelatorios2.Size = new System.Drawing.Size(90, 20);
            this.txtQtdRelatorios2.TabIndex = 5;
            // 
            // txtParaRelatorios2
            // 
            this.txtParaRelatorios2.Location = new System.Drawing.Point(11, 58);
            this.txtParaRelatorios2.Name = "txtParaRelatorios2";
            this.txtParaRelatorios2.Size = new System.Drawing.Size(237, 20);
            this.txtParaRelatorios2.TabIndex = 4;
            // 
            // txtQtdRelatorios1
            // 
            this.txtQtdRelatorios1.Location = new System.Drawing.Point(254, 32);
            this.txtQtdRelatorios1.Name = "txtQtdRelatorios1";
            this.txtQtdRelatorios1.Size = new System.Drawing.Size(90, 20);
            this.txtQtdRelatorios1.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(251, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Quantidade:";
            // 
            // txtParaRelatorios1
            // 
            this.txtParaRelatorios1.Location = new System.Drawing.Point(11, 32);
            this.txtParaRelatorios1.Name = "txtParaRelatorios1";
            this.txtParaRelatorios1.Size = new System.Drawing.Size(237, 20);
            this.txtParaRelatorios1.TabIndex = 1;
            this.txtParaRelatorios1.Text = " ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Para:";
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1229, 533);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // btnNovo
            // 
            this.btnNovo.Location = new System.Drawing.Point(878, 629);
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(89, 36);
            this.btnNovo.TabIndex = 54;
            this.btnNovo.Text = "Novo";
            this.btnNovo.UseVisualStyleBackColor = true;
            this.btnNovo.Click += new System.EventHandler(this.btnNovo_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Location = new System.Drawing.Point(973, 629);
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(89, 36);
            this.btnSalvar.TabIndex = 53;
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.UseVisualStyleBackColor = true;
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Location = new System.Drawing.Point(1159, 629);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(89, 36);
            this.btnCancelar.TabIndex = 52;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            // 
            // btnImprimir
            // 
            this.btnImprimir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImprimir.Location = new System.Drawing.Point(1068, 629);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(85, 36);
            this.btnImprimir.TabIndex = 51;
            this.btnImprimir.Text = "Imprimir";
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // lblOut
            // 
            this.lblOut.AutoSize = true;
            this.lblOut.BackColor = System.Drawing.Color.LightGray;
            this.lblOut.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOut.ForeColor = System.Drawing.Color.Red;
            this.lblOut.Location = new System.Drawing.Point(20, 637);
            this.lblOut.Name = "lblOut";
            this.lblOut.Size = new System.Drawing.Size(110, 18);
            this.lblOut.TabIndex = 55;
            this.lblOut.Text = "arquivo Saida";
            this.lblOut.Visible = false;
            // 
            // frmRelatorioIntervençõesDiarias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1261, 675);
            this.Controls.Add(this.lblOut);
            this.Controls.Add(this.btnNovo);
            this.Controls.Add(this.btnSalvar);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnImprimir);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox2);
            this.Name = "frmRelatorioIntervençõesDiarias";
            this.Text = "Relatório de Intervenções Diarias";
            this.Load += new System.EventHandler(this.frmRelatorioIntervençõesDiarias_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox cboRelatorios;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.MaskedTextBox txtData;
        private System.Windows.Forms.TextBox txtTecnicoResponsavel;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtQtdEncaminhamentos1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtParaEncaminhamentos1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtQtdEncaminhamentos3;
        private System.Windows.Forms.TextBox txtParaEncaminhamentos3;
        private System.Windows.Forms.TextBox txtQtdEncaminhamentos2;
        private System.Windows.Forms.TextBox txtParaEncaminhamentos2;
        private System.Windows.Forms.TextBox txtQtdEncaminhamentos4;
        private System.Windows.Forms.TextBox txtParaEncaminhamentos4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtQtdVisitas4;
        private System.Windows.Forms.TextBox txtParaVisitas4;
        private System.Windows.Forms.TextBox txtQtdVisitas3;
        private System.Windows.Forms.TextBox txtParaVisitas3;
        private System.Windows.Forms.TextBox txtQtdVisitas2;
        private System.Windows.Forms.TextBox txtParaVisitas2;
        private System.Windows.Forms.TextBox txtQtdVisitas1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtParaVisitas1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtQtdRelatorios4;
        private System.Windows.Forms.TextBox txtParaRelatorios4;
        private System.Windows.Forms.TextBox txtQtdRelatorios3;
        private System.Windows.Forms.TextBox txtParaRelatorios3;
        private System.Windows.Forms.TextBox txtQtdRelatorios2;
        private System.Windows.Forms.TextBox txtParaRelatorios2;
        private System.Windows.Forms.TextBox txtQtdRelatorios1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtParaRelatorios1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox txtNumProntuarioNovos9;
        private System.Windows.Forms.TextBox txtProntuariosNovo9;
        private System.Windows.Forms.TextBox txtNumProntuarioNovos8;
        private System.Windows.Forms.TextBox txtProntuariosNovo8;
        private System.Windows.Forms.TextBox txtNumProntuarioNovos7;
        private System.Windows.Forms.TextBox txtProntuariosNovo7;
        private System.Windows.Forms.TextBox txtNumProntuarioNovos6;
        private System.Windows.Forms.TextBox txtProntuariosNovo6;
        private System.Windows.Forms.TextBox txtNumProntuarioNovos5;
        private System.Windows.Forms.TextBox txtProntuariosNovo5;
        private System.Windows.Forms.TextBox txtNumProntuarioNovos4;
        private System.Windows.Forms.TextBox txtProntuariosNovo4;
        private System.Windows.Forms.TextBox txtNumProntuarioNovos3;
        private System.Windows.Forms.TextBox txtProntuariosNovo3;
        private System.Windows.Forms.TextBox txtNumProntuarioNovos2;
        private System.Windows.Forms.TextBox txtProntuariosNovo2;
        private System.Windows.Forms.TextBox txtNumProntuarioNovos1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtProntuariosNovo1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox txtDesligadosNum9;
        private System.Windows.Forms.TextBox txtDesligadosNome9;
        private System.Windows.Forms.TextBox txtDesligadosNum8;
        private System.Windows.Forms.TextBox txtDesligadosNome8;
        private System.Windows.Forms.TextBox txtDesligadosNum7;
        private System.Windows.Forms.TextBox txtDesligadosNome7;
        private System.Windows.Forms.TextBox txtDesligadosNum6;
        private System.Windows.Forms.TextBox txtDesligadosNome6;
        private System.Windows.Forms.TextBox txtDesligadosNum5;
        private System.Windows.Forms.TextBox txtDesligadosNome5;
        private System.Windows.Forms.TextBox txtDesligadosNum4;
        private System.Windows.Forms.TextBox txtDesligadosNome4;
        private System.Windows.Forms.TextBox txtDesligadosNum3;
        private System.Windows.Forms.TextBox txtDesligadosNome3;
        private System.Windows.Forms.TextBox txtDesligadosNum2;
        private System.Windows.Forms.TextBox txtDesligadosNome2;
        private System.Windows.Forms.TextBox txtDesligadosNum1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtDesligadosNome1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TextBox txtReincidentesNum9;
        private System.Windows.Forms.TextBox txtReincidentesNome9;
        private System.Windows.Forms.TextBox txtReincidentesNum8;
        private System.Windows.Forms.TextBox txtReincidentesNome8;
        private System.Windows.Forms.TextBox txtReincidentesNum7;
        private System.Windows.Forms.TextBox txtReincidentesNome7;
        private System.Windows.Forms.TextBox txtReincidentesNum6;
        private System.Windows.Forms.TextBox txtReincidentesNome6;
        private System.Windows.Forms.TextBox txtReincidentesNum5;
        private System.Windows.Forms.TextBox txtReincidentesNome5;
        private System.Windows.Forms.TextBox txtReincidentesNum4;
        private System.Windows.Forms.TextBox txtReincidentesNome4;
        private System.Windows.Forms.TextBox txtReincidentesNum3;
        private System.Windows.Forms.TextBox txtReincidentesNome3;
        private System.Windows.Forms.TextBox txtReincidentesNum2;
        private System.Windows.Forms.TextBox txtReincidentesNome2;
        private System.Windows.Forms.TextBox txtReincidentesNum1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtReincidentesNome1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnNovo;
        private System.Windows.Forms.Button btnSalvar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Label lblOut;
        private System.Windows.Forms.TextBox txtReincidentesNum10;
        private System.Windows.Forms.TextBox txtReincidentesNome10;
        private System.Windows.Forms.TextBox txtNumProntuarioNovos10;
        private System.Windows.Forms.TextBox txtProntuariosNovo10;
        private System.Windows.Forms.TextBox txtDesligadosNum10;
        private System.Windows.Forms.TextBox txtDesligadosNome10;
    }
}