﻿using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Office2010.Excel;
using Microsoft.Office.Interop.Word;
using SISSocial.Classes;
using SISSocial.RN;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SISSocial
{
    public partial class frmAtividadesEquipeTecnica : Form
    {
        List<RelatorioAtividadesEquipeTecnica> ListaEquipeTecnica = new List<RelatorioAtividadesEquipeTecnica>();
        bool relatoriosCarregados = false;
        bool updateRelatorio = false;
        public frmAtividadesEquipeTecnica()
        {
            InitializeComponent();
        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtNomeServico.Text == string.Empty)
                {
                    lblOut.Text ="Não há informações suficientes para gerar o arquivo!";
                    return;
                }
                string path = System.AppDomain.CurrentDomain.BaseDirectory.ToString();
                path = path.Replace("\\", "/");
                string templatePath = $"{path}Resources Base/RELATORIO DE ATIVIDADES Equipe Tecnica.docx";
                string outputPath = $"{path}Arquivos saida/RELATORIO DE ATIVIDADES {txtNomeServico.Text}.docx";
                var fields = new Dictionary<string, string>
                {
                    { "{{NomeServico}}", txtNomeServico.Text },
                    { "{{Nome}}", txtNome.Text },
                    { "{{Funcao}}", txtFuncao.Text },
                    { "{{MesRef}}", txtMesReferencia.Text },
                    { "{{DataEntrega}}", txtDataEntrega.Text },
                    { "{{Descricao}}", txtDescricaoAtividade.Text },
                    { "{{PublicoAlvo}}", txtPublicoAlvo.Text },
                    { "{{PessoasAtendidas}}", txtQtdPessoasAtendidas.Text },
                    { "{{Periodicidade}}", txtEncontrosRealizados.Text },
                    { "{{RecursosHumanos}}", txtRecursosHumanos.Text },
                    { "{{Abrangencia}}", txtAbrangenciaTerritorial.Text },
                    { "{{Resultados}}", txtResultadosObtidos.Text },
                    { "{{RecursosFinanceiros}}", txtOrigemRecursosFinanceiros.Text },
                    { "{{Destacar}}", txtAtividadesGratuitas.Text },
                    { "{{Parcerias}}", txtParcerias.Text },
                };
                lblOut.Visible = true;
                if (GerarWord.PreencherCampos(templatePath, outputPath, fields))
                {
                    lblOut.Text = $"Arquivo gerado com sucesso na pasta {path} Arquivos saida";
                }
                else
                {
                    lblOut.Text = $"Erro ao gerar o arquivo por favor entre em contato com o administrador do sistema";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                RNRelatorioAtividadesEquipeTecnica rn = new RNRelatorioAtividadesEquipeTecnica();
                RelatorioAtividadesEquipeTecnica equipeTecnica = new RelatorioAtividadesEquipeTecnica
                {
                    NomeServico = txtNomeServico.Text,
                    Nome = txtNome.Text,
                    Funcao = txtFuncao.Text,
                    MesRef = txtMesReferencia.Text,
                    DataEntrega = txtDataEntrega.Text,
                    Descricao = txtDescricaoAtividade.Text,
                    PublicoAlvo = txtPublicoAlvo.Text,
                    PessoasAtendidas = txtQtdPessoasAtendidas.Text,
                    Periodicidade = txtEncontrosRealizados.Text,
                    RecursosHumanos = txtRecursosHumanos.Text,
                    Abrangencia = txtAbrangenciaTerritorial.Text,
                    Resultados = txtResultadosObtidos.Text,
                    RecursosFinanceiros = txtOrigemRecursosFinanceiros.Text,
                    Destacar = txtAtividadesGratuitas.Text,
                    Parcerias = txtParcerias.Text,
                    Id = (int)cboRelatorios.SelectedValue
            };


                if (updateRelatorio || equipeTecnica.Id != 0)
                {
                    if (rn.UpdateRelatorioAtividadesEquipeTecnica(equipeTecnica))
                    {
                        MessageBox.Show("Registro atualizado com sucesso!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                    }
                }
                else if (rn.InsereRelatorioAtividadesEquipeTecnica(equipeTecnica))
                {
                    MessageBox.Show("Registro salvo com sucesso!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                }
                else
                {
                    MessageBox.Show("Erro ao gravar relatorio!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                }
                cboRelatorios.DataSource = null;
                ListaEquipeTecnica.Clear();
                relatoriosCarregados = false;
                CarregaRelatorios();
                btnImprimir.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private void frmAtividadesEquipeTecnica_Load(object sender, EventArgs e)
        {
            try
            {
                CarregaRelatorios();

                foreach (Control c in this.Controls)
                {
                    foreach (Control y in c.Controls)
                    {
                        foreach (Control x in y.Controls)
                        {
                            if (x.GetType().ToString() == "System.Windows.Forms.TextBox")
                            {
                                TextBox tx = (TextBox)x;
                                tx.ShortcutsEnabled = true;
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }

        }
        private void CarregaRelatorios()
        {
            try
            {
                RNRelatorioAtividadesEquipeTecnica rn = new RNRelatorioAtividadesEquipeTecnica();
                DataSet ds = new DataSet();
                ds = rn.SelectRelatorioAtividadesEquipeTecnica();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        RelatorioAtividadesEquipeTecnica relatorio = new RelatorioAtividadesEquipeTecnica();
                        relatorio.Id = (int)dr["ID"];
                        relatorio.NomeServico = dr["NomeServico"].ToString();
                        relatorio.Nome = dr["Nome"].ToString();
                        relatorio.Funcao = dr["Funcao"].ToString();
                        relatorio.MesRef = dr["MesRef"].ToString();
                        relatorio.DataEntrega = dr["DataEntrega"].ToString();
                        relatorio.Descricao = dr["Descricao"].ToString();
                        relatorio.PublicoAlvo = dr["PublicoAlvo"].ToString();
                        relatorio.PessoasAtendidas = dr["PessoasAtendidas"].ToString();
                        relatorio.Periodicidade = dr["Periodicidade"].ToString();
                        relatorio.RecursosHumanos = dr["RecursosHumanos"].ToString();
                        relatorio.Abrangencia = dr["Abrangencia"].ToString();
                        relatorio.Resultados = dr["Resultados"].ToString();
                        relatorio.RecursosFinanceiros = dr["RecursosFinanceiros"].ToString();
                        relatorio.Destacar = dr["Destacar"].ToString();
                        relatorio.Parcerias = dr["Parcerias"].ToString();
                        ListaEquipeTecnica.Add(relatorio);
                    }
                    cboRelatorios.DataSource = ds.Tables[0];
                    cboRelatorios.ValueMember = "ID";
                    cboRelatorios.DisplayMember = "NomeServico";
                    cboRelatorios.SelectedIndex = 0;
                    relatoriosCarregados = true;
                    updateRelatorio = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private void cboRelatorios_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!relatoriosCarregados)
                {
                    return;
                }
                int Id = (int)cboRelatorios.SelectedValue;
                if (Id == 0){
                    return;
                }
                foreach (RelatorioAtividadesEquipeTecnica resultado in ListaEquipeTecnica)
                {
                    if (resultado.Id == Id)
                    {
                        CarregaTelaRelatorio(resultado);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private void CarregaTelaRelatorio(RelatorioAtividadesEquipeTecnica equipeTecnica)
        {
            try
            {
                txtNomeServico.Text = equipeTecnica.NomeServico;
                txtNome.Text = equipeTecnica.Nome;
                txtFuncao.Text = equipeTecnica.Funcao;
                txtMesReferencia.Text = equipeTecnica.MesRef;
                txtDataEntrega.Text = equipeTecnica.DataEntrega;
                txtDescricaoAtividade.Text = equipeTecnica.Descricao;
                txtPublicoAlvo.Text = equipeTecnica.PublicoAlvo;
                txtQtdPessoasAtendidas.Text = equipeTecnica.PessoasAtendidas;
                txtEncontrosRealizados.Text = equipeTecnica.Periodicidade;
                txtRecursosHumanos.Text = equipeTecnica.RecursosHumanos;
                txtAbrangenciaTerritorial.Text = equipeTecnica.Abrangencia ;
                txtResultadosObtidos.Text = equipeTecnica.Resultados;
                txtOrigemRecursosFinanceiros.Text = equipeTecnica.RecursosFinanceiros;
                txtAtividadesGratuitas.Text = equipeTecnica.Destacar;
                txtParcerias.Text =equipeTecnica.Parcerias;
                updateRelatorio = true;
                btnImprimir.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            txtNomeServico.Text = string.Empty;
            txtNome.Text = string.Empty;
            txtFuncao.Text = string.Empty;
            txtMesReferencia.Text = string.Empty;
            txtDataEntrega.Text = string.Empty;
            txtDescricaoAtividade.Text = string.Empty;
            txtPublicoAlvo.Text = string.Empty;
            txtQtdPessoasAtendidas.Text = string.Empty;
            txtEncontrosRealizados.Text = string.Empty;
            txtRecursosHumanos.Text = string.Empty;
            txtAbrangenciaTerritorial.Text = string.Empty;
            txtResultadosObtidos.Text = string.Empty;
            txtOrigemRecursosFinanceiros.Text = string.Empty;
            txtAtividadesGratuitas.Text = string.Empty;
            txtParcerias.Text = string.Empty;
            updateRelatorio = false;
            cboRelatorios.SelectedIndex = 0;
            btnImprimir.Enabled = false;
        }
    }
}
