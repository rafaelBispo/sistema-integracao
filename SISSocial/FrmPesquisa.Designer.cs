﻿namespace SISSocial
{
    partial class FrmPesquisa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.lblNumRegistros = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.cboBuscarPor = new System.Windows.Forms.ComboBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtBusca = new System.Windows.Forms.TextBox();
            this.lblBuscarPor = new System.Windows.Forms.Label();
            this.dtGridResultado = new System.Windows.Forms.DataGridView();
            this.cboPesquisa = new System.Windows.Forms.ComboBox();
            this.dtPesquisa = new System.Windows.Forms.DateTimePicker();
            this.groupBox22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtGridResultado)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.dtPesquisa);
            this.groupBox22.Controls.Add(this.lblNumRegistros);
            this.groupBox22.Controls.Add(this.cboPesquisa);
            this.groupBox22.Controls.Add(this.label25);
            this.groupBox22.Controls.Add(this.cboBuscarPor);
            this.groupBox22.Controls.Add(this.btnBuscar);
            this.groupBox22.Controls.Add(this.txtBusca);
            this.groupBox22.Controls.Add(this.lblBuscarPor);
            this.groupBox22.Location = new System.Drawing.Point(12, 12);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(1149, 104);
            this.groupBox22.TabIndex = 33;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "Pesquisar";
            // 
            // lblNumRegistros
            // 
            this.lblNumRegistros.AutoSize = true;
            this.lblNumRegistros.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumRegistros.Location = new System.Drawing.Point(985, 53);
            this.lblNumRegistros.Name = "lblNumRegistros";
            this.lblNumRegistros.Size = new System.Drawing.Size(102, 16);
            this.lblNumRegistros.TabIndex = 28;
            this.lblNumRegistros.Text = "Num. Registros:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(13, 26);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(78, 16);
            this.label25.TabIndex = 26;
            this.label25.Text = "Buscar por: ";
            // 
            // cboBuscarPor
            // 
            this.cboBuscarPor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBuscarPor.FormattingEnabled = true;
            this.cboBuscarPor.Items.AddRange(new object[] {
            "Selecione...",
            "CPF",
            "Nome",
            "Número da Ficha",
            "Bairro",
            "Data de Cadastro"});
            this.cboBuscarPor.Location = new System.Drawing.Point(16, 45);
            this.cboBuscarPor.Name = "cboBuscarPor";
            this.cboBuscarPor.Size = new System.Drawing.Size(184, 21);
            this.cboBuscarPor.TabIndex = 25;
            this.cboBuscarPor.SelectedIndexChanged += new System.EventHandler(this.cboBuscarPor_SelectedIndexChanged);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(413, 46);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(69, 23);
            this.btnBuscar.TabIndex = 17;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtBusca
            // 
            this.txtBusca.Location = new System.Drawing.Point(245, 46);
            this.txtBusca.Name = "txtBusca";
            this.txtBusca.Size = new System.Drawing.Size(150, 20);
            this.txtBusca.TabIndex = 15;
            // 
            // lblBuscarPor
            // 
            this.lblBuscarPor.AutoSize = true;
            this.lblBuscarPor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBuscarPor.Location = new System.Drawing.Point(225, 25);
            this.lblBuscarPor.Name = "lblBuscarPor";
            this.lblBuscarPor.Size = new System.Drawing.Size(0, 16);
            this.lblBuscarPor.TabIndex = 16;
            // 
            // dtGridResultado
            // 
            this.dtGridResultado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtGridResultado.Location = new System.Drawing.Point(12, 133);
            this.dtGridResultado.Name = "dtGridResultado";
            this.dtGridResultado.Size = new System.Drawing.Size(1149, 461);
            this.dtGridResultado.TabIndex = 34;
            this.dtGridResultado.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dtGridResultado_CellMouseDoubleClick);
            // 
            // cboPesquisa
            // 
            this.cboPesquisa.FormattingEnabled = true;
            this.cboPesquisa.Location = new System.Drawing.Point(211, 45);
            this.cboPesquisa.Name = "cboPesquisa";
            this.cboPesquisa.Size = new System.Drawing.Size(184, 21);
            this.cboPesquisa.TabIndex = 27;
            this.cboPesquisa.Visible = false;
            // 
            // dtPesquisa
            // 
            this.dtPesquisa.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtPesquisa.Location = new System.Drawing.Point(228, 45);
            this.dtPesquisa.Name = "dtPesquisa";
            this.dtPesquisa.Size = new System.Drawing.Size(165, 20);
            this.dtPesquisa.TabIndex = 29;
            this.dtPesquisa.Value = new System.DateTime(2024, 3, 9, 0, 0, 0, 0);
            // 
            // FrmPesquisa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1176, 606);
            this.Controls.Add(this.dtGridResultado);
            this.Controls.Add(this.groupBox22);
            this.Name = "FrmPesquisa";
            this.Text = "FrmPesquisa";
            this.Load += new System.EventHandler(this.FrmPesquisa_Load);
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtGridResultado)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox22;
        public System.Windows.Forms.Label label25;
        public System.Windows.Forms.ComboBox cboBuscarPor;
        private System.Windows.Forms.Button btnBuscar;
        public System.Windows.Forms.TextBox txtBusca;
        public System.Windows.Forms.Label lblBuscarPor;
        private System.Windows.Forms.DataGridView dtGridResultado;
        public System.Windows.Forms.Label lblNumRegistros;
        public System.Windows.Forms.ComboBox cboPesquisa;
        private System.Windows.Forms.DateTimePicker dtPesquisa;
    }
}