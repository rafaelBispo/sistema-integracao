﻿namespace SISSocial
{
    partial class FrmFormulario
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNome = new System.Windows.Forms.TextBox();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.txtTelefone = new System.Windows.Forms.TextBox();
            this.txtMae = new System.Windows.Forms.TextBox();
            this.txtPai = new System.Windows.Forms.TextBox();
            this.txtCPF = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lblOut = new System.Windows.Forms.Label();
            this.btnImprimirAssistido = new System.Windows.Forms.Button();
            this.txtResponsavel = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtNis = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.cboBairro = new System.Windows.Forms.ComboBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.txtQtdPessoasCasa = new System.Windows.Forms.TextBox();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.lblLoas = new System.Windows.Forms.Label();
            this.txtLoas = new System.Windows.Forms.TextBox();
            this.rdLoasNao = new System.Windows.Forms.RadioButton();
            this.rdLoasSim = new System.Windows.Forms.RadioButton();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.rdMaeApoioPaisNao = new System.Windows.Forms.RadioButton();
            this.rdMaeApoioPaisSim = new System.Windows.Forms.RadioButton();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.rdAlugada = new System.Windows.Forms.RadioButton();
            this.rdInvadida = new System.Windows.Forms.RadioButton();
            this.lblValorAluguel = new System.Windows.Forms.Label();
            this.txtValorAluguel = new System.Windows.Forms.TextBox();
            this.rdCedida = new System.Windows.Forms.RadioButton();
            this.rdPropria = new System.Windows.Forms.RadioButton();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.lblValorBolsaFamilia = new System.Windows.Forms.Label();
            this.txtvalorBolsaFamilia = new System.Windows.Forms.TextBox();
            this.rdBolsaFamiliaNao = new System.Windows.Forms.RadioButton();
            this.rdBolsaFamiliaSim = new System.Windows.Forms.RadioButton();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.lblProfissao = new System.Windows.Forms.Label();
            this.txtProfissao = new System.Windows.Forms.TextBox();
            this.rdCarteiraAssinadaNao = new System.Windows.Forms.RadioButton();
            this.rdCarteiraAssinadaSim = new System.Windows.Forms.RadioButton();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.rdAnalfabeto = new System.Windows.Forms.RadioButton();
            this.rdPrimario = new System.Windows.Forms.RadioButton();
            this.rdEnsSuperiorIncomp = new System.Windows.Forms.RadioButton();
            this.rdEnsSuperiorComp = new System.Windows.Forms.RadioButton();
            this.rdEnsMedioIncomp = new System.Windows.Forms.RadioButton();
            this.rdEnsMedioComp = new System.Windows.Forms.RadioButton();
            this.rdEnsFundamentalincomp = new System.Windows.Forms.RadioButton();
            this.rdEnsFundamentalComp = new System.Windows.Forms.RadioButton();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.rdIdadeMaior60 = new System.Windows.Forms.RadioButton();
            this.rdIdade30a59 = new System.Windows.Forms.RadioButton();
            this.rdIdade18a29 = new System.Windows.Forms.RadioButton();
            this.rdIdade15a17 = new System.Windows.Forms.RadioButton();
            this.rdIdade12a14 = new System.Windows.Forms.RadioButton();
            this.rdIdade7a11 = new System.Windows.Forms.RadioButton();
            this.rdIdade0a6 = new System.Windows.Forms.RadioButton();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.rdIdadeMaeMaior21 = new System.Windows.Forms.RadioButton();
            this.rdIdadeMae18a21 = new System.Windows.Forms.RadioButton();
            this.rdIdadeMaeMenor18 = new System.Windows.Forms.RadioButton();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.rdMaePresenteNao = new System.Windows.Forms.RadioButton();
            this.rdMaePresenteSim = new System.Windows.Forms.RadioButton();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.rdPossuiDeficienciaNao = new System.Windows.Forms.RadioButton();
            this.rdPossuiDeficienciaSim = new System.Windows.Forms.RadioButton();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.rdAuxilioPrevNao = new System.Windows.Forms.RadioButton();
            this.rdAuxilioPrevSim = new System.Windows.Forms.RadioButton();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblQualDoenca = new System.Windows.Forms.Label();
            this.txtQualDoenca = new System.Windows.Forms.TextBox();
            this.rdProbSaudeNao = new System.Windows.Forms.RadioButton();
            this.rdProbSaudeSim = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.rdRecebePensaoNao = new System.Windows.Forms.RadioButton();
            this.rdRecebePensaoSim = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rdCrasNao = new System.Windows.Forms.RadioButton();
            this.rdCrasSim = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdFilhosNao = new System.Windows.Forms.RadioButton();
            this.rdFilhosSim = new System.Windows.Forms.RadioButton();
            this.lblQtdFilhos = new System.Windows.Forms.Label();
            this.txtQtdFilhos = new System.Windows.Forms.TextBox();
            this.txtRG = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.cboEstadoCivil = new System.Windows.Forms.ComboBox();
            this.cboSexo = new System.Windows.Forms.ComboBox();
            this.dtDataNasc = new System.Windows.Forms.DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.lblOutCF = new System.Windows.Forms.Label();
            this.btnImprimiComposicao = new System.Windows.Forms.Button();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtCadastroCrasCF = new System.Windows.Forms.TextBox();
            this.rdCadastroCrasNaoCF = new System.Windows.Forms.RadioButton();
            this.rdCadastroCrasSimCF = new System.Windows.Forms.RadioButton();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtAlgumaAtividadeCF = new System.Windows.Forms.TextBox();
            this.rdAlgumaAtividadeNaoCF = new System.Windows.Forms.RadioButton();
            this.rdAlgumaAtividadeSimCF = new System.Windows.Forms.RadioButton();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtAtividadesAlemScfvCF = new System.Windows.Forms.TextBox();
            this.rdAtividadesAlemScfvNaoCF = new System.Windows.Forms.RadioButton();
            this.rdAtividadesAlemScfvSimCF = new System.Windows.Forms.RadioButton();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txtUsoMedicamentoCF = new System.Windows.Forms.TextBox();
            this.rdUsoMedicamentoNaoCF = new System.Windows.Forms.RadioButton();
            this.rdUsoMedicamentoSimCF = new System.Windows.Forms.RadioButton();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtAlergiaAlimentoCF = new System.Windows.Forms.TextBox();
            this.rdAlergiaAlimentoNaoCF = new System.Windows.Forms.RadioButton();
            this.rdAlergiaAlimentoSimCF = new System.Windows.Forms.RadioButton();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtProblemaSaudeCF = new System.Windows.Forms.TextBox();
            this.rdProblemaSaudeNaoCF = new System.Windows.Forms.RadioButton();
            this.rdProblemaSaudeSimCF = new System.Windows.Forms.RadioButton();
            this.txtEscolaridadeCF = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtIdadeCF = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.txtLaudo = new System.Windows.Forms.TextBox();
            this.txtFiliacao = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtCertidaoNasc = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtCartaoSUS = new System.Windows.Forms.TextBox();
            this.lblCertidaoNasc = new System.Windows.Forms.Label();
            this.btnNovoDependente = new System.Windows.Forms.Button();
            this.lblComposicaoFamiliar = new System.Windows.Forms.Label();
            this.cboComposicaoFamiliar = new System.Windows.Forms.ComboBox();
            this.btnGravarDependente = new System.Windows.Forms.Button();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.lblProfissaoComposicao = new System.Windows.Forms.Label();
            this.txtProfissaoComposicao = new System.Windows.Forms.TextBox();
            this.rdCarteiraAssinadaNaoCF = new System.Windows.Forms.RadioButton();
            this.rdCarteiraAssinadaSimCF = new System.Windows.Forms.RadioButton();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.rdDeficienciaNaoComposicao = new System.Windows.Forms.RadioButton();
            this.rdDeficienciaSimComposicao = new System.Windows.Forms.RadioButton();
            this.txtRGComposicao = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.cbEstadoCivilComposicao = new System.Windows.Forms.ComboBox();
            this.cbSexoComposicao = new System.Windows.Forms.ComboBox();
            this.dtNascimentoComposicao = new System.Windows.Forms.DateTimePicker();
            this.txtNomeComposicao = new System.Windows.Forms.TextBox();
            this.txtTelefoneComposicao = new System.Windows.Forms.TextBox();
            this.txtCPFComposicao = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnLimpar = new System.Windows.Forms.Button();
            this.lblHistoricoAcompanhamento = new System.Windows.Forms.Label();
            this.cboHistoricoAcompanhamento = new System.Windows.Forms.ComboBox();
            this.btnSalvarAnotacao = new System.Windows.Forms.Button();
            this.txtObservacao = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.dtAcompanhamento = new System.Windows.Forms.DateTimePicker();
            this.label21 = new System.Windows.Forms.Label();
            this.btnGravar = new System.Windows.Forms.Button();
            this.btnNovo = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.cboOrientacao = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.lblDataCadastro = new System.Windows.Forms.Label();
            this.lblNumCadastro = new System.Windows.Forms.Label();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.cbUsuariosEncontrados = new System.Windows.Forms.ComboBox();
            this.dtBusca = new System.Windows.Forms.DateTimePicker();
            this.label25 = new System.Windows.Forms.Label();
            this.cboBuscarPor = new System.Windows.Forms.ComboBox();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtBusca = new System.Windows.Forms.TextBox();
            this.lblBuscarPor = new System.Windows.Forms.Label();
            this.btnSair = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.groupBox24.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtNome
            // 
            this.txtNome.Enabled = false;
            this.txtNome.Location = new System.Drawing.Point(67, 21);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(388, 20);
            this.txtNome.TabIndex = 0;
            // 
            // txtEndereco
            // 
            this.txtEndereco.Enabled = false;
            this.txtEndereco.Location = new System.Drawing.Point(77, 150);
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(378, 20);
            this.txtEndereco.TabIndex = 8;
            // 
            // txtTelefone
            // 
            this.txtTelefone.Enabled = false;
            this.txtTelefone.Location = new System.Drawing.Point(327, 183);
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(128, 20);
            this.txtTelefone.TabIndex = 10;
            // 
            // txtMae
            // 
            this.txtMae.Location = new System.Drawing.Point(55, 22);
            this.txtMae.Name = "txtMae";
            this.txtMae.Size = new System.Drawing.Size(378, 22);
            this.txtMae.TabIndex = 11;
            // 
            // txtPai
            // 
            this.txtPai.Location = new System.Drawing.Point(55, 50);
            this.txtPai.Name = "txtPai";
            this.txtPai.Size = new System.Drawing.Size(378, 22);
            this.txtPai.TabIndex = 12;
            // 
            // txtCPF
            // 
            this.txtCPF.Enabled = false;
            this.txtCPF.Location = new System.Drawing.Point(67, 85);
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(150, 20);
            this.txtCPF.TabIndex = 4;
            this.txtCPF.Leave += new System.EventHandler(this.txtCPF_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 16);
            this.label1.TabIndex = 10;
            this.label1.Text = "Nome:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(18, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 16);
            this.label2.TabIndex = 11;
            this.label2.Text = "Mãe:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(18, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 16);
            this.label3.TabIndex = 12;
            this.label3.Text = "Pai:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(24, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 16);
            this.label4.TabIndex = 13;
            this.label4.Text = "Data Nascimento:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(24, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 16);
            this.label5.TabIndex = 14;
            this.label5.Text = "CPF:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(260, 184);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 16);
            this.label6.TabIndex = 15;
            this.label6.Text = "Telefone:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(5, 151);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 16);
            this.label7.TabIndex = 16;
            this.label7.Text = "Endereço:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(20, 26);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 20);
            this.label11.TabIndex = 21;
            this.label11.Text = "N° Cadastro:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(8, 54);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1092, 699);
            this.tabControl1.TabIndex = 22;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.LightGray;
            this.tabPage1.Controls.Add(this.lblOut);
            this.tabPage1.Controls.Add(this.btnImprimirAssistido);
            this.tabPage1.Controls.Add(this.txtResponsavel);
            this.tabPage1.Controls.Add(this.label28);
            this.tabPage1.Controls.Add(this.txtNis);
            this.tabPage1.Controls.Add(this.label27);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.cboBairro);
            this.tabPage1.Controls.Add(this.groupBox17);
            this.tabPage1.Controls.Add(this.groupBox16);
            this.tabPage1.Controls.Add(this.groupBox15);
            this.tabPage1.Controls.Add(this.groupBox14);
            this.tabPage1.Controls.Add(this.groupBox13);
            this.tabPage1.Controls.Add(this.groupBox12);
            this.tabPage1.Controls.Add(this.groupBox11);
            this.tabPage1.Controls.Add(this.groupBox10);
            this.tabPage1.Controls.Add(this.groupBox9);
            this.tabPage1.Controls.Add(this.groupBox8);
            this.tabPage1.Controls.Add(this.groupBox7);
            this.tabPage1.Controls.Add(this.groupBox6);
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.txtRG);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.cboEstadoCivil);
            this.tabPage1.Controls.Add(this.cboSexo);
            this.tabPage1.Controls.Add(this.dtDataNasc);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.txtNome);
            this.tabPage1.Controls.Add(this.txtEndereco);
            this.tabPage1.Controls.Add(this.txtTelefone);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.txtCPF);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1084, 673);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Assistido";
            // 
            // lblOut
            // 
            this.lblOut.AutoSize = true;
            this.lblOut.BackColor = System.Drawing.Color.LightGray;
            this.lblOut.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOut.ForeColor = System.Drawing.Color.Red;
            this.lblOut.Location = new System.Drawing.Point(16, 638);
            this.lblOut.Name = "lblOut";
            this.lblOut.Size = new System.Drawing.Size(110, 18);
            this.lblOut.TabIndex = 47;
            this.lblOut.Text = "arquivo Saida";
            // 
            // btnImprimirAssistido
            // 
            this.btnImprimirAssistido.Enabled = false;
            this.btnImprimirAssistido.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImprimirAssistido.Location = new System.Drawing.Point(986, 616);
            this.btnImprimirAssistido.Name = "btnImprimirAssistido";
            this.btnImprimirAssistido.Size = new System.Drawing.Size(87, 40);
            this.btnImprimirAssistido.TabIndex = 27;
            this.btnImprimirAssistido.Text = "Imprimir";
            this.btnImprimirAssistido.UseVisualStyleBackColor = true;
            this.btnImprimirAssistido.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // txtResponsavel
            // 
            this.txtResponsavel.Enabled = false;
            this.txtResponsavel.Location = new System.Drawing.Point(563, 112);
            this.txtResponsavel.Name = "txtResponsavel";
            this.txtResponsavel.Size = new System.Drawing.Size(337, 20);
            this.txtResponsavel.TabIndex = 13;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(471, 113);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(91, 16);
            this.label28.TabIndex = 46;
            this.label28.Text = "Responsável:";
            // 
            // txtNis
            // 
            this.txtNis.Enabled = false;
            this.txtNis.Location = new System.Drawing.Point(312, 121);
            this.txtNis.Name = "txtNis";
            this.txtNis.Size = new System.Drawing.Size(143, 20);
            this.txtNis.TabIndex = 7;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(273, 121);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(32, 16);
            this.label27.TabIndex = 44;
            this.label27.Text = "NIS:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(17, 184);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 16);
            this.label8.TabIndex = 42;
            this.label8.Text = "Bairro:";
            // 
            // cboBairro
            // 
            this.cboBairro.Enabled = false;
            this.cboBairro.FormattingEnabled = true;
            this.cboBairro.Items.AddRange(new object[] {
            "Selecione..."});
            this.cboBairro.Location = new System.Drawing.Point(67, 182);
            this.cboBairro.Name = "cboBairro";
            this.cboBairro.Size = new System.Drawing.Size(181, 21);
            this.cboBairro.TabIndex = 9;
            // 
            // groupBox17
            // 
            this.groupBox17.BackColor = System.Drawing.Color.Silver;
            this.groupBox17.Controls.Add(this.txtQtdPessoasCasa);
            this.groupBox17.Enabled = false;
            this.groupBox17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox17.Location = new System.Drawing.Point(821, 235);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(252, 58);
            this.groupBox17.TabIndex = 25;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "Quantas pessoas moram na casa?";
            // 
            // txtQtdPessoasCasa
            // 
            this.txtQtdPessoasCasa.Location = new System.Drawing.Point(9, 25);
            this.txtQtdPessoasCasa.Name = "txtQtdPessoasCasa";
            this.txtQtdPessoasCasa.Size = new System.Drawing.Size(90, 22);
            this.txtQtdPessoasCasa.TabIndex = 26;
            // 
            // groupBox16
            // 
            this.groupBox16.BackColor = System.Drawing.Color.Silver;
            this.groupBox16.Controls.Add(this.lblLoas);
            this.groupBox16.Controls.Add(this.txtLoas);
            this.groupBox16.Controls.Add(this.rdLoasNao);
            this.groupBox16.Controls.Add(this.rdLoasSim);
            this.groupBox16.Enabled = false;
            this.groupBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox16.Location = new System.Drawing.Point(514, 457);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(301, 79);
            this.groupBox16.TabIndex = 33;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Recebe LOAS (BPC) ?  ";
            // 
            // lblLoas
            // 
            this.lblLoas.AutoSize = true;
            this.lblLoas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoas.Location = new System.Drawing.Point(6, 52);
            this.lblLoas.Name = "lblLoas";
            this.lblLoas.Size = new System.Drawing.Size(72, 16);
            this.lblLoas.TabIndex = 17;
            this.lblLoas.Text = "De quem ?";
            this.lblLoas.Visible = false;
            // 
            // txtLoas
            // 
            this.txtLoas.Location = new System.Drawing.Point(85, 49);
            this.txtLoas.Name = "txtLoas";
            this.txtLoas.Size = new System.Drawing.Size(169, 22);
            this.txtLoas.TabIndex = 16;
            this.txtLoas.Visible = false;
            // 
            // rdLoasNao
            // 
            this.rdLoasNao.AutoSize = true;
            this.rdLoasNao.Location = new System.Drawing.Point(85, 21);
            this.rdLoasNao.Name = "rdLoasNao";
            this.rdLoasNao.Size = new System.Drawing.Size(51, 20);
            this.rdLoasNao.TabIndex = 15;
            this.rdLoasNao.TabStop = true;
            this.rdLoasNao.Text = "Não";
            this.rdLoasNao.UseVisualStyleBackColor = true;
            // 
            // rdLoasSim
            // 
            this.rdLoasSim.AutoSize = true;
            this.rdLoasSim.Location = new System.Drawing.Point(9, 22);
            this.rdLoasSim.Name = "rdLoasSim";
            this.rdLoasSim.Size = new System.Drawing.Size(48, 20);
            this.rdLoasSim.TabIndex = 14;
            this.rdLoasSim.TabStop = true;
            this.rdLoasSim.Text = "Sim";
            this.rdLoasSim.UseVisualStyleBackColor = true;
            this.rdLoasSim.CheckedChanged += new System.EventHandler(this.rdLoasSim_CheckedChanged);
            // 
            // groupBox15
            // 
            this.groupBox15.BackColor = System.Drawing.Color.Silver;
            this.groupBox15.Controls.Add(this.rdMaeApoioPaisNao);
            this.groupBox15.Controls.Add(this.rdMaeApoioPaisSim);
            this.groupBox15.Enabled = false;
            this.groupBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox15.Location = new System.Drawing.Point(514, 151);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(301, 58);
            this.groupBox15.TabIndex = 21;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Mãe que cria seus filhos sem apoio dos pais?";
            // 
            // rdMaeApoioPaisNao
            // 
            this.rdMaeApoioPaisNao.AutoSize = true;
            this.rdMaeApoioPaisNao.Location = new System.Drawing.Point(85, 22);
            this.rdMaeApoioPaisNao.Name = "rdMaeApoioPaisNao";
            this.rdMaeApoioPaisNao.Size = new System.Drawing.Size(51, 20);
            this.rdMaeApoioPaisNao.TabIndex = 15;
            this.rdMaeApoioPaisNao.TabStop = true;
            this.rdMaeApoioPaisNao.Text = "Não";
            this.rdMaeApoioPaisNao.UseVisualStyleBackColor = true;
            // 
            // rdMaeApoioPaisSim
            // 
            this.rdMaeApoioPaisSim.AutoSize = true;
            this.rdMaeApoioPaisSim.Location = new System.Drawing.Point(9, 22);
            this.rdMaeApoioPaisSim.Name = "rdMaeApoioPaisSim";
            this.rdMaeApoioPaisSim.Size = new System.Drawing.Size(48, 20);
            this.rdMaeApoioPaisSim.TabIndex = 14;
            this.rdMaeApoioPaisSim.TabStop = true;
            this.rdMaeApoioPaisSim.Text = "Sim";
            this.rdMaeApoioPaisSim.UseVisualStyleBackColor = true;
            // 
            // groupBox14
            // 
            this.groupBox14.BackColor = System.Drawing.Color.Silver;
            this.groupBox14.Controls.Add(this.rdAlugada);
            this.groupBox14.Controls.Add(this.rdInvadida);
            this.groupBox14.Controls.Add(this.lblValorAluguel);
            this.groupBox14.Controls.Add(this.txtValorAluguel);
            this.groupBox14.Controls.Add(this.rdCedida);
            this.groupBox14.Controls.Add(this.rdPropria);
            this.groupBox14.Enabled = false;
            this.groupBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox14.Location = new System.Drawing.Point(820, 150);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(253, 79);
            this.groupBox14.TabIndex = 22;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "A casa é?";
            // 
            // rdAlugada
            // 
            this.rdAlugada.AutoSize = true;
            this.rdAlugada.Location = new System.Drawing.Point(9, 51);
            this.rdAlugada.Name = "rdAlugada";
            this.rdAlugada.Size = new System.Drawing.Size(76, 20);
            this.rdAlugada.TabIndex = 19;
            this.rdAlugada.TabStop = true;
            this.rdAlugada.Text = "Alugada";
            this.rdAlugada.UseVisualStyleBackColor = true;
            this.rdAlugada.CheckedChanged += new System.EventHandler(this.rdAlugada_CheckedChanged);
            // 
            // rdInvadida
            // 
            this.rdInvadida.AutoSize = true;
            this.rdInvadida.Location = new System.Drawing.Point(159, 22);
            this.rdInvadida.Name = "rdInvadida";
            this.rdInvadida.Size = new System.Drawing.Size(77, 20);
            this.rdInvadida.TabIndex = 18;
            this.rdInvadida.TabStop = true;
            this.rdInvadida.Text = "Invadida";
            this.rdInvadida.UseVisualStyleBackColor = true;
            // 
            // lblValorAluguel
            // 
            this.lblValorAluguel.AutoSize = true;
            this.lblValorAluguel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorAluguel.Location = new System.Drawing.Point(94, 53);
            this.lblValorAluguel.Name = "lblValorAluguel";
            this.lblValorAluguel.Size = new System.Drawing.Size(42, 16);
            this.lblValorAluguel.TabIndex = 17;
            this.lblValorAluguel.Text = "Valor:";
            this.lblValorAluguel.Visible = false;
            // 
            // txtValorAluguel
            // 
            this.txtValorAluguel.Location = new System.Drawing.Point(143, 50);
            this.txtValorAluguel.Name = "txtValorAluguel";
            this.txtValorAluguel.Size = new System.Drawing.Size(100, 22);
            this.txtValorAluguel.TabIndex = 23;
            this.txtValorAluguel.Visible = false;
            // 
            // rdCedida
            // 
            this.rdCedida.AutoSize = true;
            this.rdCedida.Location = new System.Drawing.Point(85, 21);
            this.rdCedida.Name = "rdCedida";
            this.rdCedida.Size = new System.Drawing.Size(69, 20);
            this.rdCedida.TabIndex = 15;
            this.rdCedida.TabStop = true;
            this.rdCedida.Text = "Cedida";
            this.rdCedida.UseVisualStyleBackColor = true;
            // 
            // rdPropria
            // 
            this.rdPropria.AutoSize = true;
            this.rdPropria.Location = new System.Drawing.Point(9, 22);
            this.rdPropria.Name = "rdPropria";
            this.rdPropria.Size = new System.Drawing.Size(69, 20);
            this.rdPropria.TabIndex = 14;
            this.rdPropria.TabStop = true;
            this.rdPropria.Text = "Própria";
            this.rdPropria.UseVisualStyleBackColor = true;
            // 
            // groupBox13
            // 
            this.groupBox13.BackColor = System.Drawing.Color.Silver;
            this.groupBox13.Controls.Add(this.lblValorBolsaFamilia);
            this.groupBox13.Controls.Add(this.txtvalorBolsaFamilia);
            this.groupBox13.Controls.Add(this.rdBolsaFamiliaNao);
            this.groupBox13.Controls.Add(this.rdBolsaFamiliaSim);
            this.groupBox13.Enabled = false;
            this.groupBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox13.Location = new System.Drawing.Point(514, 372);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(301, 79);
            this.groupBox13.TabIndex = 30;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Recebe bolsa família?";
            // 
            // lblValorBolsaFamilia
            // 
            this.lblValorBolsaFamilia.AutoSize = true;
            this.lblValorBolsaFamilia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValorBolsaFamilia.Location = new System.Drawing.Point(6, 52);
            this.lblValorBolsaFamilia.Name = "lblValorBolsaFamilia";
            this.lblValorBolsaFamilia.Size = new System.Drawing.Size(42, 16);
            this.lblValorBolsaFamilia.TabIndex = 17;
            this.lblValorBolsaFamilia.Text = "Valor:";
            this.lblValorBolsaFamilia.Visible = false;
            // 
            // txtvalorBolsaFamilia
            // 
            this.txtvalorBolsaFamilia.Location = new System.Drawing.Point(55, 49);
            this.txtvalorBolsaFamilia.Name = "txtvalorBolsaFamilia";
            this.txtvalorBolsaFamilia.Size = new System.Drawing.Size(123, 22);
            this.txtvalorBolsaFamilia.TabIndex = 31;
            this.txtvalorBolsaFamilia.Visible = false;
            // 
            // rdBolsaFamiliaNao
            // 
            this.rdBolsaFamiliaNao.AutoSize = true;
            this.rdBolsaFamiliaNao.Location = new System.Drawing.Point(85, 22);
            this.rdBolsaFamiliaNao.Name = "rdBolsaFamiliaNao";
            this.rdBolsaFamiliaNao.Size = new System.Drawing.Size(51, 20);
            this.rdBolsaFamiliaNao.TabIndex = 15;
            this.rdBolsaFamiliaNao.TabStop = true;
            this.rdBolsaFamiliaNao.Text = "Não";
            this.rdBolsaFamiliaNao.UseVisualStyleBackColor = true;
            // 
            // rdBolsaFamiliaSim
            // 
            this.rdBolsaFamiliaSim.AutoSize = true;
            this.rdBolsaFamiliaSim.Location = new System.Drawing.Point(9, 22);
            this.rdBolsaFamiliaSim.Name = "rdBolsaFamiliaSim";
            this.rdBolsaFamiliaSim.Size = new System.Drawing.Size(48, 20);
            this.rdBolsaFamiliaSim.TabIndex = 14;
            this.rdBolsaFamiliaSim.TabStop = true;
            this.rdBolsaFamiliaSim.Text = "Sim";
            this.rdBolsaFamiliaSim.UseVisualStyleBackColor = true;
            this.rdBolsaFamiliaSim.CheckedChanged += new System.EventHandler(this.rbBolsaFamiliaSim_CheckedChanged);
            // 
            // groupBox12
            // 
            this.groupBox12.BackColor = System.Drawing.Color.Silver;
            this.groupBox12.Controls.Add(this.lblProfissao);
            this.groupBox12.Controls.Add(this.txtProfissao);
            this.groupBox12.Controls.Add(this.rdCarteiraAssinadaNao);
            this.groupBox12.Controls.Add(this.rdCarteiraAssinadaSim);
            this.groupBox12.Enabled = false;
            this.groupBox12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox12.Location = new System.Drawing.Point(821, 300);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(252, 110);
            this.groupBox12.TabIndex = 28;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Possui Fonte de renda? ";
            // 
            // lblProfissao
            // 
            this.lblProfissao.AutoSize = true;
            this.lblProfissao.Location = new System.Drawing.Point(10, 53);
            this.lblProfissao.Name = "lblProfissao";
            this.lblProfissao.Size = new System.Drawing.Size(42, 16);
            this.lblProfissao.TabIndex = 20;
            this.lblProfissao.Text = "Qual?";
            this.lblProfissao.Visible = false;
            // 
            // txtProfissao
            // 
            this.txtProfissao.Location = new System.Drawing.Point(9, 72);
            this.txtProfissao.Name = "txtProfissao";
            this.txtProfissao.Size = new System.Drawing.Size(190, 22);
            this.txtProfissao.TabIndex = 29;
            this.txtProfissao.Visible = false;
            // 
            // rdCarteiraAssinadaNao
            // 
            this.rdCarteiraAssinadaNao.AutoSize = true;
            this.rdCarteiraAssinadaNao.Location = new System.Drawing.Point(85, 22);
            this.rdCarteiraAssinadaNao.Name = "rdCarteiraAssinadaNao";
            this.rdCarteiraAssinadaNao.Size = new System.Drawing.Size(51, 20);
            this.rdCarteiraAssinadaNao.TabIndex = 15;
            this.rdCarteiraAssinadaNao.TabStop = true;
            this.rdCarteiraAssinadaNao.Text = "Não";
            this.rdCarteiraAssinadaNao.UseVisualStyleBackColor = true;
            // 
            // rdCarteiraAssinadaSim
            // 
            this.rdCarteiraAssinadaSim.AutoSize = true;
            this.rdCarteiraAssinadaSim.Location = new System.Drawing.Point(9, 22);
            this.rdCarteiraAssinadaSim.Name = "rdCarteiraAssinadaSim";
            this.rdCarteiraAssinadaSim.Size = new System.Drawing.Size(48, 20);
            this.rdCarteiraAssinadaSim.TabIndex = 14;
            this.rdCarteiraAssinadaSim.TabStop = true;
            this.rdCarteiraAssinadaSim.Text = "Sim";
            this.rdCarteiraAssinadaSim.UseVisualStyleBackColor = true;
            this.rdCarteiraAssinadaSim.CheckedChanged += new System.EventHandler(this.rdCarteiraAssinadaSim_CheckedChanged);
            // 
            // groupBox11
            // 
            this.groupBox11.BackColor = System.Drawing.Color.Silver;
            this.groupBox11.Controls.Add(this.rdAnalfabeto);
            this.groupBox11.Controls.Add(this.rdPrimario);
            this.groupBox11.Controls.Add(this.rdEnsSuperiorIncomp);
            this.groupBox11.Controls.Add(this.rdEnsSuperiorComp);
            this.groupBox11.Controls.Add(this.rdEnsMedioIncomp);
            this.groupBox11.Controls.Add(this.rdEnsMedioComp);
            this.groupBox11.Controls.Add(this.rdEnsFundamentalincomp);
            this.groupBox11.Controls.Add(this.rdEnsFundamentalComp);
            this.groupBox11.Enabled = false;
            this.groupBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox11.Location = new System.Drawing.Point(13, 215);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(227, 244);
            this.groupBox11.TabIndex = 14;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Escolaridade:";
            // 
            // rdAnalfabeto
            // 
            this.rdAnalfabeto.AutoSize = true;
            this.rdAnalfabeto.Location = new System.Drawing.Point(9, 25);
            this.rdAnalfabeto.Name = "rdAnalfabeto";
            this.rdAnalfabeto.Size = new System.Drawing.Size(90, 20);
            this.rdAnalfabeto.TabIndex = 21;
            this.rdAnalfabeto.TabStop = true;
            this.rdAnalfabeto.Text = "Analfabeto";
            this.rdAnalfabeto.UseVisualStyleBackColor = true;
            // 
            // rdPrimario
            // 
            this.rdPrimario.AutoSize = true;
            this.rdPrimario.Location = new System.Drawing.Point(9, 49);
            this.rdPrimario.Name = "rdPrimario";
            this.rdPrimario.Size = new System.Drawing.Size(75, 20);
            this.rdPrimario.TabIndex = 20;
            this.rdPrimario.TabStop = true;
            this.rdPrimario.Text = "Primario";
            this.rdPrimario.UseVisualStyleBackColor = true;
            // 
            // rdEnsSuperiorIncomp
            // 
            this.rdEnsSuperiorIncomp.AutoSize = true;
            this.rdEnsSuperiorIncomp.Location = new System.Drawing.Point(9, 199);
            this.rdEnsSuperiorIncomp.Name = "rdEnsSuperiorIncomp";
            this.rdEnsSuperiorIncomp.Size = new System.Drawing.Size(145, 20);
            this.rdEnsSuperiorIncomp.TabIndex = 19;
            this.rdEnsSuperiorIncomp.TabStop = true;
            this.rdEnsSuperiorIncomp.Text = "Superior incompleto";
            this.rdEnsSuperiorIncomp.UseVisualStyleBackColor = true;
            // 
            // rdEnsSuperiorComp
            // 
            this.rdEnsSuperiorComp.AutoSize = true;
            this.rdEnsSuperiorComp.Location = new System.Drawing.Point(9, 174);
            this.rdEnsSuperiorComp.Name = "rdEnsSuperiorComp";
            this.rdEnsSuperiorComp.Size = new System.Drawing.Size(135, 20);
            this.rdEnsSuperiorComp.TabIndex = 18;
            this.rdEnsSuperiorComp.TabStop = true;
            this.rdEnsSuperiorComp.Text = "Superior completo";
            this.rdEnsSuperiorComp.UseVisualStyleBackColor = true;
            // 
            // rdEnsMedioIncomp
            // 
            this.rdEnsMedioIncomp.AutoSize = true;
            this.rdEnsMedioIncomp.Location = new System.Drawing.Point(9, 150);
            this.rdEnsMedioIncomp.Name = "rdEnsMedioIncomp";
            this.rdEnsMedioIncomp.Size = new System.Drawing.Size(197, 20);
            this.rdEnsMedioIncomp.TabIndex = 17;
            this.rdEnsMedioIncomp.TabStop = true;
            this.rdEnsMedioIncomp.Text = "Ensino médio  incompleto      ";
            this.rdEnsMedioIncomp.UseVisualStyleBackColor = true;
            // 
            // rdEnsMedioComp
            // 
            this.rdEnsMedioComp.AutoSize = true;
            this.rdEnsMedioComp.Location = new System.Drawing.Point(9, 124);
            this.rdEnsMedioComp.Name = "rdEnsMedioComp";
            this.rdEnsMedioComp.Size = new System.Drawing.Size(169, 20);
            this.rdEnsMedioComp.TabIndex = 16;
            this.rdEnsMedioComp.TabStop = true;
            this.rdEnsMedioComp.Text = "Ensino médio  completo";
            this.rdEnsMedioComp.UseVisualStyleBackColor = true;
            // 
            // rdEnsFundamentalincomp
            // 
            this.rdEnsFundamentalincomp.AutoSize = true;
            this.rdEnsFundamentalincomp.Location = new System.Drawing.Point(9, 98);
            this.rdEnsFundamentalincomp.Name = "rdEnsFundamentalincomp";
            this.rdEnsFundamentalincomp.Size = new System.Drawing.Size(217, 20);
            this.rdEnsFundamentalincomp.TabIndex = 15;
            this.rdEnsFundamentalincomp.TabStop = true;
            this.rdEnsFundamentalincomp.Text = "Ensino fundamental  incompleto ";
            this.rdEnsFundamentalincomp.UseVisualStyleBackColor = true;
            // 
            // rdEnsFundamentalComp
            // 
            this.rdEnsFundamentalComp.AutoSize = true;
            this.rdEnsFundamentalComp.Location = new System.Drawing.Point(9, 72);
            this.rdEnsFundamentalComp.Name = "rdEnsFundamentalComp";
            this.rdEnsFundamentalComp.Size = new System.Drawing.Size(201, 20);
            this.rdEnsFundamentalComp.TabIndex = 14;
            this.rdEnsFundamentalComp.TabStop = true;
            this.rdEnsFundamentalComp.Text = "Ensino fundamental completo";
            this.rdEnsFundamentalComp.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.BackColor = System.Drawing.Color.Silver;
            this.groupBox10.Controls.Add(this.rdIdadeMaior60);
            this.groupBox10.Controls.Add(this.rdIdade30a59);
            this.groupBox10.Controls.Add(this.rdIdade18a29);
            this.groupBox10.Controls.Add(this.rdIdade15a17);
            this.groupBox10.Controls.Add(this.rdIdade12a14);
            this.groupBox10.Controls.Add(this.rdIdade7a11);
            this.groupBox10.Controls.Add(this.rdIdade0a6);
            this.groupBox10.Enabled = false;
            this.groupBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.Location = new System.Drawing.Point(246, 215);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(262, 180);
            this.groupBox10.TabIndex = 15;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Idade:";
            // 
            // rdIdadeMaior60
            // 
            this.rdIdadeMaior60.AutoSize = true;
            this.rdIdadeMaior60.Location = new System.Drawing.Point(131, 78);
            this.rdIdadeMaior60.Name = "rdIdadeMaior60";
            this.rdIdadeMaior60.Size = new System.Drawing.Size(128, 20);
            this.rdIdadeMaior60.TabIndex = 20;
            this.rdIdadeMaior60.TabStop = true;
            this.rdIdadeMaior60.Text = "Maior de 60 anos";
            this.rdIdadeMaior60.UseVisualStyleBackColor = true;
            // 
            // rdIdade30a59
            // 
            this.rdIdade30a59.AutoSize = true;
            this.rdIdade30a59.Location = new System.Drawing.Point(131, 50);
            this.rdIdade30a59.Name = "rdIdade30a59";
            this.rdIdade30a59.Size = new System.Drawing.Size(100, 20);
            this.rdIdade30a59.TabIndex = 19;
            this.rdIdade30a59.TabStop = true;
            this.rdIdade30a59.Text = "30 a 59 anos";
            this.rdIdade30a59.UseVisualStyleBackColor = true;
            // 
            // rdIdade18a29
            // 
            this.rdIdade18a29.AutoSize = true;
            this.rdIdade18a29.Location = new System.Drawing.Point(131, 25);
            this.rdIdade18a29.Name = "rdIdade18a29";
            this.rdIdade18a29.Size = new System.Drawing.Size(100, 20);
            this.rdIdade18a29.TabIndex = 18;
            this.rdIdade18a29.TabStop = true;
            this.rdIdade18a29.Text = "18 a 29 anos";
            this.rdIdade18a29.UseVisualStyleBackColor = true;
            // 
            // rdIdade15a17
            // 
            this.rdIdade15a17.AutoSize = true;
            this.rdIdade15a17.Location = new System.Drawing.Point(9, 100);
            this.rdIdade15a17.Name = "rdIdade15a17";
            this.rdIdade15a17.Size = new System.Drawing.Size(100, 20);
            this.rdIdade15a17.TabIndex = 17;
            this.rdIdade15a17.TabStop = true;
            this.rdIdade15a17.Text = "15 a 17 anos";
            this.rdIdade15a17.UseVisualStyleBackColor = true;
            // 
            // rdIdade12a14
            // 
            this.rdIdade12a14.AutoSize = true;
            this.rdIdade12a14.Location = new System.Drawing.Point(9, 74);
            this.rdIdade12a14.Name = "rdIdade12a14";
            this.rdIdade12a14.Size = new System.Drawing.Size(100, 20);
            this.rdIdade12a14.TabIndex = 16;
            this.rdIdade12a14.TabStop = true;
            this.rdIdade12a14.Text = "12 a 14 anos";
            this.rdIdade12a14.UseVisualStyleBackColor = true;
            // 
            // rdIdade7a11
            // 
            this.rdIdade7a11.AutoSize = true;
            this.rdIdade7a11.Location = new System.Drawing.Point(9, 48);
            this.rdIdade7a11.Name = "rdIdade7a11";
            this.rdIdade7a11.Size = new System.Drawing.Size(93, 20);
            this.rdIdade7a11.TabIndex = 15;
            this.rdIdade7a11.TabStop = true;
            this.rdIdade7a11.Text = "7 a 11 anos";
            this.rdIdade7a11.UseVisualStyleBackColor = true;
            // 
            // rdIdade0a6
            // 
            this.rdIdade0a6.AutoSize = true;
            this.rdIdade0a6.Location = new System.Drawing.Point(9, 22);
            this.rdIdade0a6.Name = "rdIdade0a6";
            this.rdIdade0a6.Size = new System.Drawing.Size(86, 20);
            this.rdIdade0a6.TabIndex = 14;
            this.rdIdade0a6.TabStop = true;
            this.rdIdade0a6.Text = "0 a 6 anos";
            this.rdIdade0a6.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.BackColor = System.Drawing.Color.Silver;
            this.groupBox9.Controls.Add(this.rdIdadeMaeMaior21);
            this.groupBox9.Controls.Add(this.rdIdadeMae18a21);
            this.groupBox9.Controls.Add(this.rdIdadeMaeMenor18);
            this.groupBox9.Enabled = false;
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox9.Location = new System.Drawing.Point(514, 281);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(301, 81);
            this.groupBox9.TabIndex = 27;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Mãe é?";
            // 
            // rdIdadeMaeMaior21
            // 
            this.rdIdadeMaeMaior21.AutoSize = true;
            this.rdIdadeMaeMaior21.Location = new System.Drawing.Point(148, 22);
            this.rdIdadeMaeMaior21.Name = "rdIdadeMaeMaior21";
            this.rdIdadeMaeMaior21.Size = new System.Drawing.Size(140, 20);
            this.rdIdadeMaeMaior21.TabIndex = 16;
            this.rdIdadeMaeMaior21.TabStop = true;
            this.rdIdadeMaeMaior21.Text = "É maior de 21 anos";
            this.rdIdadeMaeMaior21.UseVisualStyleBackColor = true;
            // 
            // rdIdadeMae18a21
            // 
            this.rdIdadeMae18a21.AutoSize = true;
            this.rdIdadeMae18a21.Location = new System.Drawing.Point(9, 48);
            this.rdIdadeMae18a21.Name = "rdIdadeMae18a21";
            this.rdIdadeMae18a21.Size = new System.Drawing.Size(137, 20);
            this.rdIdadeMae18a21.TabIndex = 15;
            this.rdIdadeMae18a21.TabStop = true;
            this.rdIdadeMae18a21.Text = "Tem entre 18  a  21";
            this.rdIdadeMae18a21.UseVisualStyleBackColor = true;
            // 
            // rdIdadeMaeMenor18
            // 
            this.rdIdadeMaeMenor18.AutoSize = true;
            this.rdIdadeMaeMenor18.Location = new System.Drawing.Point(9, 22);
            this.rdIdadeMaeMenor18.Name = "rdIdadeMaeMenor18";
            this.rdIdadeMaeMenor18.Size = new System.Drawing.Size(132, 20);
            this.rdIdadeMaeMenor18.TabIndex = 14;
            this.rdIdadeMaeMenor18.TabStop = true;
            this.rdIdadeMaeMenor18.Text = "Menor de 18 anos";
            this.rdIdadeMaeMenor18.UseVisualStyleBackColor = true;
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.Silver;
            this.groupBox8.Controls.Add(this.rdMaePresenteNao);
            this.groupBox8.Controls.Add(this.rdMaePresenteSim);
            this.groupBox8.Enabled = false;
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.Location = new System.Drawing.Point(514, 217);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(301, 58);
            this.groupBox8.TabIndex = 24;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Mãe Presente na familia?";
            // 
            // rdMaePresenteNao
            // 
            this.rdMaePresenteNao.AutoSize = true;
            this.rdMaePresenteNao.Location = new System.Drawing.Point(85, 23);
            this.rdMaePresenteNao.Name = "rdMaePresenteNao";
            this.rdMaePresenteNao.Size = new System.Drawing.Size(51, 20);
            this.rdMaePresenteNao.TabIndex = 15;
            this.rdMaePresenteNao.TabStop = true;
            this.rdMaePresenteNao.Text = "Não";
            this.rdMaePresenteNao.UseVisualStyleBackColor = true;
            // 
            // rdMaePresenteSim
            // 
            this.rdMaePresenteSim.AutoSize = true;
            this.rdMaePresenteSim.Location = new System.Drawing.Point(9, 22);
            this.rdMaePresenteSim.Name = "rdMaePresenteSim";
            this.rdMaePresenteSim.Size = new System.Drawing.Size(48, 20);
            this.rdMaePresenteSim.TabIndex = 14;
            this.rdMaePresenteSim.TabStop = true;
            this.rdMaePresenteSim.Text = "Sim";
            this.rdMaePresenteSim.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.Silver;
            this.groupBox7.Controls.Add(this.rdPossuiDeficienciaNao);
            this.groupBox7.Controls.Add(this.rdPossuiDeficienciaSim);
            this.groupBox7.Enabled = false;
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(514, 545);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(301, 58);
            this.groupBox7.TabIndex = 35;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Possui deficiência?";
            // 
            // rdPossuiDeficienciaNao
            // 
            this.rdPossuiDeficienciaNao.AutoSize = true;
            this.rdPossuiDeficienciaNao.Location = new System.Drawing.Point(85, 22);
            this.rdPossuiDeficienciaNao.Name = "rdPossuiDeficienciaNao";
            this.rdPossuiDeficienciaNao.Size = new System.Drawing.Size(51, 20);
            this.rdPossuiDeficienciaNao.TabIndex = 15;
            this.rdPossuiDeficienciaNao.TabStop = true;
            this.rdPossuiDeficienciaNao.Text = "Não";
            this.rdPossuiDeficienciaNao.UseVisualStyleBackColor = true;
            // 
            // rdPossuiDeficienciaSim
            // 
            this.rdPossuiDeficienciaSim.AutoSize = true;
            this.rdPossuiDeficienciaSim.Location = new System.Drawing.Point(9, 22);
            this.rdPossuiDeficienciaSim.Name = "rdPossuiDeficienciaSim";
            this.rdPossuiDeficienciaSim.Size = new System.Drawing.Size(48, 20);
            this.rdPossuiDeficienciaSim.TabIndex = 14;
            this.rdPossuiDeficienciaSim.TabStop = true;
            this.rdPossuiDeficienciaSim.Text = "Sim";
            this.rdPossuiDeficienciaSim.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Silver;
            this.groupBox6.Controls.Add(this.rdAuxilioPrevNao);
            this.groupBox6.Controls.Add(this.rdAuxilioPrevSim);
            this.groupBox6.Enabled = false;
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(821, 416);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(252, 58);
            this.groupBox6.TabIndex = 32;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Recebe auxilio Previdênciario?";
            // 
            // rdAuxilioPrevNao
            // 
            this.rdAuxilioPrevNao.AutoSize = true;
            this.rdAuxilioPrevNao.Location = new System.Drawing.Point(85, 23);
            this.rdAuxilioPrevNao.Name = "rdAuxilioPrevNao";
            this.rdAuxilioPrevNao.Size = new System.Drawing.Size(51, 20);
            this.rdAuxilioPrevNao.TabIndex = 15;
            this.rdAuxilioPrevNao.TabStop = true;
            this.rdAuxilioPrevNao.Text = "Não";
            this.rdAuxilioPrevNao.UseVisualStyleBackColor = true;
            // 
            // rdAuxilioPrevSim
            // 
            this.rdAuxilioPrevSim.AutoSize = true;
            this.rdAuxilioPrevSim.Location = new System.Drawing.Point(9, 22);
            this.rdAuxilioPrevSim.Name = "rdAuxilioPrevSim";
            this.rdAuxilioPrevSim.Size = new System.Drawing.Size(48, 20);
            this.rdAuxilioPrevSim.TabIndex = 14;
            this.rdAuxilioPrevSim.TabStop = true;
            this.rdAuxilioPrevSim.Text = "Sim";
            this.rdAuxilioPrevSim.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Silver;
            this.groupBox5.Controls.Add(this.lblQualDoenca);
            this.groupBox5.Controls.Add(this.txtQualDoenca);
            this.groupBox5.Controls.Add(this.rdProbSaudeNao);
            this.groupBox5.Controls.Add(this.rdProbSaudeSim);
            this.groupBox5.Enabled = false;
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(13, 468);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(227, 101);
            this.groupBox5.TabIndex = 16;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Tem Problemas de Saúde?";
            // 
            // lblQualDoenca
            // 
            this.lblQualDoenca.AutoSize = true;
            this.lblQualDoenca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQualDoenca.Location = new System.Drawing.Point(6, 48);
            this.lblQualDoenca.Name = "lblQualDoenca";
            this.lblQualDoenca.Size = new System.Drawing.Size(42, 16);
            this.lblQualDoenca.TabIndex = 17;
            this.lblQualDoenca.Text = "Qual?";
            this.lblQualDoenca.Visible = false;
            // 
            // txtQualDoenca
            // 
            this.txtQualDoenca.Location = new System.Drawing.Point(6, 67);
            this.txtQualDoenca.Name = "txtQualDoenca";
            this.txtQualDoenca.Size = new System.Drawing.Size(205, 22);
            this.txtQualDoenca.TabIndex = 17;
            this.txtQualDoenca.Visible = false;
            // 
            // rdProbSaudeNao
            // 
            this.rdProbSaudeNao.AutoSize = true;
            this.rdProbSaudeNao.Location = new System.Drawing.Point(85, 22);
            this.rdProbSaudeNao.Name = "rdProbSaudeNao";
            this.rdProbSaudeNao.Size = new System.Drawing.Size(51, 20);
            this.rdProbSaudeNao.TabIndex = 15;
            this.rdProbSaudeNao.TabStop = true;
            this.rdProbSaudeNao.Text = "Não";
            this.rdProbSaudeNao.UseVisualStyleBackColor = true;
            // 
            // rdProbSaudeSim
            // 
            this.rdProbSaudeSim.AutoSize = true;
            this.rdProbSaudeSim.Location = new System.Drawing.Point(9, 22);
            this.rdProbSaudeSim.Name = "rdProbSaudeSim";
            this.rdProbSaudeSim.Size = new System.Drawing.Size(48, 20);
            this.rdProbSaudeSim.TabIndex = 14;
            this.rdProbSaudeSim.TabStop = true;
            this.rdProbSaudeSim.Text = "Sim";
            this.rdProbSaudeSim.UseVisualStyleBackColor = true;
            this.rdProbSaudeSim.CheckedChanged += new System.EventHandler(this.rdProbSaudeSim_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Silver;
            this.groupBox4.Controls.Add(this.rdRecebePensaoNao);
            this.groupBox4.Controls.Add(this.rdRecebePensaoSim);
            this.groupBox4.Enabled = false;
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(246, 512);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(260, 59);
            this.groupBox4.TabIndex = 20;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Recebe Pensão?";
            // 
            // rdRecebePensaoNao
            // 
            this.rdRecebePensaoNao.AutoSize = true;
            this.rdRecebePensaoNao.Location = new System.Drawing.Point(85, 22);
            this.rdRecebePensaoNao.Name = "rdRecebePensaoNao";
            this.rdRecebePensaoNao.Size = new System.Drawing.Size(51, 20);
            this.rdRecebePensaoNao.TabIndex = 15;
            this.rdRecebePensaoNao.TabStop = true;
            this.rdRecebePensaoNao.Text = "Não";
            this.rdRecebePensaoNao.UseVisualStyleBackColor = true;
            // 
            // rdRecebePensaoSim
            // 
            this.rdRecebePensaoSim.AutoSize = true;
            this.rdRecebePensaoSim.Location = new System.Drawing.Point(9, 22);
            this.rdRecebePensaoSim.Name = "rdRecebePensaoSim";
            this.rdRecebePensaoSim.Size = new System.Drawing.Size(48, 20);
            this.rdRecebePensaoSim.TabIndex = 14;
            this.rdRecebePensaoSim.TabStop = true;
            this.rdRecebePensaoSim.Text = "Sim";
            this.rdRecebePensaoSim.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Silver;
            this.groupBox3.Controls.Add(this.rdCrasNao);
            this.groupBox3.Controls.Add(this.rdCrasSim);
            this.groupBox3.Enabled = false;
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(821, 485);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(252, 58);
            this.groupBox3.TabIndex = 34;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Possui Cadastro no CRAS?";
            // 
            // rdCrasNao
            // 
            this.rdCrasNao.AutoSize = true;
            this.rdCrasNao.Location = new System.Drawing.Point(85, 21);
            this.rdCrasNao.Name = "rdCrasNao";
            this.rdCrasNao.Size = new System.Drawing.Size(51, 20);
            this.rdCrasNao.TabIndex = 15;
            this.rdCrasNao.TabStop = true;
            this.rdCrasNao.Text = "Não";
            this.rdCrasNao.UseVisualStyleBackColor = true;
            // 
            // rdCrasSim
            // 
            this.rdCrasSim.AutoSize = true;
            this.rdCrasSim.Location = new System.Drawing.Point(9, 22);
            this.rdCrasSim.Name = "rdCrasSim";
            this.rdCrasSim.Size = new System.Drawing.Size(48, 20);
            this.rdCrasSim.TabIndex = 14;
            this.rdCrasSim.TabStop = true;
            this.rdCrasSim.Text = "Sim";
            this.rdCrasSim.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Silver;
            this.groupBox2.Controls.Add(this.rdFilhosNao);
            this.groupBox2.Controls.Add(this.rdFilhosSim);
            this.groupBox2.Controls.Add(this.lblQtdFilhos);
            this.groupBox2.Controls.Add(this.txtQtdFilhos);
            this.groupBox2.Enabled = false;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(246, 408);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(260, 87);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Filhos:";
            // 
            // rdFilhosNao
            // 
            this.rdFilhosNao.AutoSize = true;
            this.rdFilhosNao.Location = new System.Drawing.Point(9, 48);
            this.rdFilhosNao.Name = "rdFilhosNao";
            this.rdFilhosNao.Size = new System.Drawing.Size(51, 20);
            this.rdFilhosNao.TabIndex = 15;
            this.rdFilhosNao.TabStop = true;
            this.rdFilhosNao.Text = "Não";
            this.rdFilhosNao.UseVisualStyleBackColor = true;
            // 
            // rdFilhosSim
            // 
            this.rdFilhosSim.AutoSize = true;
            this.rdFilhosSim.Location = new System.Drawing.Point(9, 22);
            this.rdFilhosSim.Name = "rdFilhosSim";
            this.rdFilhosSim.Size = new System.Drawing.Size(48, 20);
            this.rdFilhosSim.TabIndex = 14;
            this.rdFilhosSim.TabStop = true;
            this.rdFilhosSim.Text = "Sim";
            this.rdFilhosSim.UseVisualStyleBackColor = true;
            this.rdFilhosSim.CheckedChanged += new System.EventHandler(this.rdFilhosSim_CheckedChanged);
            // 
            // lblQtdFilhos
            // 
            this.lblQtdFilhos.AutoSize = true;
            this.lblQtdFilhos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQtdFilhos.Location = new System.Drawing.Point(63, 24);
            this.lblQtdFilhos.Name = "lblQtdFilhos";
            this.lblQtdFilhos.Size = new System.Drawing.Size(60, 16);
            this.lblQtdFilhos.TabIndex = 13;
            this.lblQtdFilhos.Text = "Quantos:";
            this.lblQtdFilhos.Visible = false;
            // 
            // txtQtdFilhos
            // 
            this.txtQtdFilhos.Location = new System.Drawing.Point(130, 22);
            this.txtQtdFilhos.Name = "txtQtdFilhos";
            this.txtQtdFilhos.Size = new System.Drawing.Size(53, 22);
            this.txtQtdFilhos.TabIndex = 19;
            this.txtQtdFilhos.Visible = false;
            // 
            // txtRG
            // 
            this.txtRG.Enabled = false;
            this.txtRG.Location = new System.Drawing.Point(273, 85);
            this.txtRG.Name = "txtRG";
            this.txtRG.Size = new System.Drawing.Size(182, 20);
            this.txtRG.TabIndex = 5;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(233, 85);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(30, 16);
            this.label14.TabIndex = 27;
            this.label14.Text = "RG:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(208, 55);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 16);
            this.label13.TabIndex = 25;
            this.label13.Text = "Estado Cívil:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(24, 54);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(41, 16);
            this.label12.TabIndex = 24;
            this.label12.Text = "Sexo:";
            // 
            // cboEstadoCivil
            // 
            this.cboEstadoCivil.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboEstadoCivil.Enabled = false;
            this.cboEstadoCivil.FormattingEnabled = true;
            this.cboEstadoCivil.Items.AddRange(new object[] {
            "Selecione...",
            "Casado(a)",
            "Solteiro(a)",
            "Divorciado(a)",
            "Viuvo(a)"});
            this.cboEstadoCivil.Location = new System.Drawing.Point(296, 53);
            this.cboEstadoCivil.Name = "cboEstadoCivil";
            this.cboEstadoCivil.Size = new System.Drawing.Size(122, 21);
            this.cboEstadoCivil.TabIndex = 3;
            // 
            // cboSexo
            // 
            this.cboSexo.Enabled = false;
            this.cboSexo.FormattingEnabled = true;
            this.cboSexo.Items.AddRange(new object[] {
            "Selecione...",
            "Masculino",
            "Feminino",
            "Não informado"});
            this.cboSexo.Location = new System.Drawing.Point(67, 52);
            this.cboSexo.Name = "cboSexo";
            this.cboSexo.Size = new System.Drawing.Size(121, 21);
            this.cboSexo.TabIndex = 2;
            // 
            // dtDataNasc
            // 
            this.dtDataNasc.Enabled = false;
            this.dtDataNasc.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDataNasc.Location = new System.Drawing.Point(145, 118);
            this.dtDataNasc.Name = "dtDataNasc";
            this.dtDataNasc.Size = new System.Drawing.Size(96, 20);
            this.dtDataNasc.TabIndex = 6;
            this.dtDataNasc.Value = new System.DateTime(2021, 7, 21, 18, 59, 47, 0);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Silver;
            this.groupBox1.Controls.Add(this.txtMae);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtPai);
            this.groupBox1.Enabled = false;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(467, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(462, 87);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filiação:";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Silver;
            this.tabPage2.Controls.Add(this.lblOutCF);
            this.tabPage2.Controls.Add(this.btnImprimiComposicao);
            this.tabPage2.Controls.Add(this.groupBox27);
            this.tabPage2.Controls.Add(this.groupBox26);
            this.tabPage2.Controls.Add(this.groupBox25);
            this.tabPage2.Controls.Add(this.groupBox24);
            this.tabPage2.Controls.Add(this.groupBox19);
            this.tabPage2.Controls.Add(this.groupBox18);
            this.tabPage2.Controls.Add(this.txtEscolaridadeCF);
            this.tabPage2.Controls.Add(this.label30);
            this.tabPage2.Controls.Add(this.txtIdadeCF);
            this.tabPage2.Controls.Add(this.label16);
            this.tabPage2.Controls.Add(this.groupBox23);
            this.tabPage2.Controls.Add(this.txtFiliacao);
            this.tabPage2.Controls.Add(this.label29);
            this.tabPage2.Controls.Add(this.txtCertidaoNasc);
            this.tabPage2.Controls.Add(this.label26);
            this.tabPage2.Controls.Add(this.txtCartaoSUS);
            this.tabPage2.Controls.Add(this.lblCertidaoNasc);
            this.tabPage2.Controls.Add(this.btnNovoDependente);
            this.tabPage2.Controls.Add(this.lblComposicaoFamiliar);
            this.tabPage2.Controls.Add(this.cboComposicaoFamiliar);
            this.tabPage2.Controls.Add(this.btnGravarDependente);
            this.tabPage2.Controls.Add(this.groupBox21);
            this.tabPage2.Controls.Add(this.groupBox20);
            this.tabPage2.Controls.Add(this.txtRGComposicao);
            this.tabPage2.Controls.Add(this.label9);
            this.tabPage2.Controls.Add(this.label10);
            this.tabPage2.Controls.Add(this.label15);
            this.tabPage2.Controls.Add(this.cbEstadoCivilComposicao);
            this.tabPage2.Controls.Add(this.cbSexoComposicao);
            this.tabPage2.Controls.Add(this.dtNascimentoComposicao);
            this.tabPage2.Controls.Add(this.txtNomeComposicao);
            this.tabPage2.Controls.Add(this.txtTelefoneComposicao);
            this.tabPage2.Controls.Add(this.txtCPFComposicao);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Controls.Add(this.label18);
            this.tabPage2.Controls.Add(this.label19);
            this.tabPage2.Controls.Add(this.label20);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1084, 673);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Composição Familiar";
            // 
            // lblOutCF
            // 
            this.lblOutCF.AutoSize = true;
            this.lblOutCF.BackColor = System.Drawing.Color.LightGray;
            this.lblOutCF.Cursor = System.Windows.Forms.Cursors.Default;
            this.lblOutCF.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOutCF.ForeColor = System.Drawing.Color.Red;
            this.lblOutCF.Location = new System.Drawing.Point(19, 642);
            this.lblOutCF.Name = "lblOutCF";
            this.lblOutCF.Size = new System.Drawing.Size(110, 18);
            this.lblOutCF.TabIndex = 75;
            this.lblOutCF.Text = "arquivo Saida";
            // 
            // btnImprimiComposicao
            // 
            this.btnImprimiComposicao.Enabled = false;
            this.btnImprimiComposicao.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImprimiComposicao.Location = new System.Drawing.Point(935, 448);
            this.btnImprimiComposicao.Name = "btnImprimiComposicao";
            this.btnImprimiComposicao.Size = new System.Drawing.Size(133, 52);
            this.btnImprimiComposicao.TabIndex = 74;
            this.btnImprimiComposicao.Text = "Imprimir Composição";
            this.btnImprimiComposicao.UseVisualStyleBackColor = true;
            this.btnImprimiComposicao.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox27
            // 
            this.groupBox27.BackColor = System.Drawing.Color.Silver;
            this.groupBox27.Controls.Add(this.label32);
            this.groupBox27.Controls.Add(this.txtCadastroCrasCF);
            this.groupBox27.Controls.Add(this.rdCadastroCrasNaoCF);
            this.groupBox27.Controls.Add(this.rdCadastroCrasSimCF);
            this.groupBox27.Enabled = false;
            this.groupBox27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox27.Location = new System.Drawing.Point(505, 414);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(227, 98);
            this.groupBox27.TabIndex = 53;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "Possui Cadastro no CRAS?";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(6, 45);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(42, 16);
            this.label32.TabIndex = 18;
            this.label32.Text = "Qual?";
            this.label32.Visible = false;
            // 
            // txtCadastroCrasCF
            // 
            this.txtCadastroCrasCF.Location = new System.Drawing.Point(6, 64);
            this.txtCadastroCrasCF.Name = "txtCadastroCrasCF";
            this.txtCadastroCrasCF.Size = new System.Drawing.Size(205, 22);
            this.txtCadastroCrasCF.TabIndex = 19;
            this.txtCadastroCrasCF.Visible = false;
            // 
            // rdCadastroCrasNaoCF
            // 
            this.rdCadastroCrasNaoCF.AutoSize = true;
            this.rdCadastroCrasNaoCF.Location = new System.Drawing.Point(85, 21);
            this.rdCadastroCrasNaoCF.Name = "rdCadastroCrasNaoCF";
            this.rdCadastroCrasNaoCF.Size = new System.Drawing.Size(51, 20);
            this.rdCadastroCrasNaoCF.TabIndex = 15;
            this.rdCadastroCrasNaoCF.TabStop = true;
            this.rdCadastroCrasNaoCF.Text = "Não";
            this.rdCadastroCrasNaoCF.UseVisualStyleBackColor = true;
            // 
            // rdCadastroCrasSimCF
            // 
            this.rdCadastroCrasSimCF.AutoSize = true;
            this.rdCadastroCrasSimCF.Location = new System.Drawing.Point(9, 22);
            this.rdCadastroCrasSimCF.Name = "rdCadastroCrasSimCF";
            this.rdCadastroCrasSimCF.Size = new System.Drawing.Size(48, 20);
            this.rdCadastroCrasSimCF.TabIndex = 14;
            this.rdCadastroCrasSimCF.TabStop = true;
            this.rdCadastroCrasSimCF.Text = "Sim";
            this.rdCadastroCrasSimCF.UseVisualStyleBackColor = true;
            this.rdCadastroCrasSimCF.CheckedChanged += new System.EventHandler(this.rdCadastroCrasSimCF_CheckedChanged);
            // 
            // groupBox26
            // 
            this.groupBox26.BackColor = System.Drawing.Color.Silver;
            this.groupBox26.Controls.Add(this.label37);
            this.groupBox26.Controls.Add(this.txtAlgumaAtividadeCF);
            this.groupBox26.Controls.Add(this.rdAlgumaAtividadeNaoCF);
            this.groupBox26.Controls.Add(this.rdAlgumaAtividadeSimCF);
            this.groupBox26.Enabled = false;
            this.groupBox26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox26.Location = new System.Drawing.Point(505, 292);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(227, 116);
            this.groupBox26.TabIndex = 51;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Restrição para participar de alguma atividade?";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(6, 66);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(42, 16);
            this.label37.TabIndex = 17;
            this.label37.Text = "Qual?";
            this.label37.Visible = false;
            // 
            // txtAlgumaAtividadeCF
            // 
            this.txtAlgumaAtividadeCF.Location = new System.Drawing.Point(6, 85);
            this.txtAlgumaAtividadeCF.Name = "txtAlgumaAtividadeCF";
            this.txtAlgumaAtividadeCF.Size = new System.Drawing.Size(205, 22);
            this.txtAlgumaAtividadeCF.TabIndex = 17;
            this.txtAlgumaAtividadeCF.Visible = false;
            // 
            // rdAlgumaAtividadeNaoCF
            // 
            this.rdAlgumaAtividadeNaoCF.AutoSize = true;
            this.rdAlgumaAtividadeNaoCF.Location = new System.Drawing.Point(85, 40);
            this.rdAlgumaAtividadeNaoCF.Name = "rdAlgumaAtividadeNaoCF";
            this.rdAlgumaAtividadeNaoCF.Size = new System.Drawing.Size(51, 20);
            this.rdAlgumaAtividadeNaoCF.TabIndex = 15;
            this.rdAlgumaAtividadeNaoCF.TabStop = true;
            this.rdAlgumaAtividadeNaoCF.Text = "Não";
            this.rdAlgumaAtividadeNaoCF.UseVisualStyleBackColor = true;
            // 
            // rdAlgumaAtividadeSimCF
            // 
            this.rdAlgumaAtividadeSimCF.AutoSize = true;
            this.rdAlgumaAtividadeSimCF.Location = new System.Drawing.Point(9, 40);
            this.rdAlgumaAtividadeSimCF.Name = "rdAlgumaAtividadeSimCF";
            this.rdAlgumaAtividadeSimCF.Size = new System.Drawing.Size(48, 20);
            this.rdAlgumaAtividadeSimCF.TabIndex = 14;
            this.rdAlgumaAtividadeSimCF.TabStop = true;
            this.rdAlgumaAtividadeSimCF.Text = "Sim";
            this.rdAlgumaAtividadeSimCF.UseVisualStyleBackColor = true;
            this.rdAlgumaAtividadeSimCF.CheckedChanged += new System.EventHandler(this.rdAlgumaAtividadeSimCF_CheckedChanged);
            // 
            // groupBox25
            // 
            this.groupBox25.BackColor = System.Drawing.Color.Silver;
            this.groupBox25.Controls.Add(this.label36);
            this.groupBox25.Controls.Add(this.txtAtividadesAlemScfvCF);
            this.groupBox25.Controls.Add(this.rdAtividadesAlemScfvNaoCF);
            this.groupBox25.Controls.Add(this.rdAtividadesAlemScfvSimCF);
            this.groupBox25.Enabled = false;
            this.groupBox25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox25.Location = new System.Drawing.Point(738, 182);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(227, 101);
            this.groupBox25.TabIndex = 50;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "Outras atividades além do SCFV ?";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(6, 48);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(42, 16);
            this.label36.TabIndex = 17;
            this.label36.Text = "Qual?";
            this.label36.Visible = false;
            // 
            // txtAtividadesAlemScfvCF
            // 
            this.txtAtividadesAlemScfvCF.Location = new System.Drawing.Point(6, 67);
            this.txtAtividadesAlemScfvCF.Name = "txtAtividadesAlemScfvCF";
            this.txtAtividadesAlemScfvCF.Size = new System.Drawing.Size(205, 22);
            this.txtAtividadesAlemScfvCF.TabIndex = 17;
            this.txtAtividadesAlemScfvCF.Visible = false;
            // 
            // rdAtividadesAlemScfvNaoCF
            // 
            this.rdAtividadesAlemScfvNaoCF.AutoSize = true;
            this.rdAtividadesAlemScfvNaoCF.Location = new System.Drawing.Point(85, 22);
            this.rdAtividadesAlemScfvNaoCF.Name = "rdAtividadesAlemScfvNaoCF";
            this.rdAtividadesAlemScfvNaoCF.Size = new System.Drawing.Size(51, 20);
            this.rdAtividadesAlemScfvNaoCF.TabIndex = 15;
            this.rdAtividadesAlemScfvNaoCF.TabStop = true;
            this.rdAtividadesAlemScfvNaoCF.Text = "Não";
            this.rdAtividadesAlemScfvNaoCF.UseVisualStyleBackColor = true;
            // 
            // rdAtividadesAlemScfvSimCF
            // 
            this.rdAtividadesAlemScfvSimCF.AutoSize = true;
            this.rdAtividadesAlemScfvSimCF.Location = new System.Drawing.Point(9, 22);
            this.rdAtividadesAlemScfvSimCF.Name = "rdAtividadesAlemScfvSimCF";
            this.rdAtividadesAlemScfvSimCF.Size = new System.Drawing.Size(48, 20);
            this.rdAtividadesAlemScfvSimCF.TabIndex = 14;
            this.rdAtividadesAlemScfvSimCF.TabStop = true;
            this.rdAtividadesAlemScfvSimCF.Text = "Sim";
            this.rdAtividadesAlemScfvSimCF.UseVisualStyleBackColor = true;
            this.rdAtividadesAlemScfvSimCF.CheckedChanged += new System.EventHandler(this.rdAtividadesAlemScfvSimCF_CheckedChanged);
            // 
            // groupBox24
            // 
            this.groupBox24.BackColor = System.Drawing.Color.Silver;
            this.groupBox24.Controls.Add(this.label35);
            this.groupBox24.Controls.Add(this.txtUsoMedicamentoCF);
            this.groupBox24.Controls.Add(this.rdUsoMedicamentoNaoCF);
            this.groupBox24.Controls.Add(this.rdUsoMedicamentoSimCF);
            this.groupBox24.Enabled = false;
            this.groupBox24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox24.Location = new System.Drawing.Point(505, 182);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(227, 101);
            this.groupBox24.TabIndex = 49;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "Uso de Medicamento?";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(6, 48);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(42, 16);
            this.label35.TabIndex = 17;
            this.label35.Text = "Qual?";
            this.label35.Visible = false;
            // 
            // txtUsoMedicamentoCF
            // 
            this.txtUsoMedicamentoCF.Location = new System.Drawing.Point(6, 67);
            this.txtUsoMedicamentoCF.Name = "txtUsoMedicamentoCF";
            this.txtUsoMedicamentoCF.Size = new System.Drawing.Size(205, 22);
            this.txtUsoMedicamentoCF.TabIndex = 17;
            this.txtUsoMedicamentoCF.Visible = false;
            // 
            // rdUsoMedicamentoNaoCF
            // 
            this.rdUsoMedicamentoNaoCF.AutoSize = true;
            this.rdUsoMedicamentoNaoCF.Location = new System.Drawing.Point(85, 22);
            this.rdUsoMedicamentoNaoCF.Name = "rdUsoMedicamentoNaoCF";
            this.rdUsoMedicamentoNaoCF.Size = new System.Drawing.Size(51, 20);
            this.rdUsoMedicamentoNaoCF.TabIndex = 15;
            this.rdUsoMedicamentoNaoCF.TabStop = true;
            this.rdUsoMedicamentoNaoCF.Text = "Não";
            this.rdUsoMedicamentoNaoCF.UseVisualStyleBackColor = true;
            // 
            // rdUsoMedicamentoSimCF
            // 
            this.rdUsoMedicamentoSimCF.AutoSize = true;
            this.rdUsoMedicamentoSimCF.Location = new System.Drawing.Point(9, 22);
            this.rdUsoMedicamentoSimCF.Name = "rdUsoMedicamentoSimCF";
            this.rdUsoMedicamentoSimCF.Size = new System.Drawing.Size(48, 20);
            this.rdUsoMedicamentoSimCF.TabIndex = 14;
            this.rdUsoMedicamentoSimCF.TabStop = true;
            this.rdUsoMedicamentoSimCF.Text = "Sim";
            this.rdUsoMedicamentoSimCF.UseVisualStyleBackColor = true;
            this.rdUsoMedicamentoSimCF.CheckedChanged += new System.EventHandler(this.rdUsoMedicamentoSimCF_CheckedChanged);
            // 
            // groupBox19
            // 
            this.groupBox19.BackColor = System.Drawing.Color.Silver;
            this.groupBox19.Controls.Add(this.label34);
            this.groupBox19.Controls.Add(this.txtAlergiaAlimentoCF);
            this.groupBox19.Controls.Add(this.rdAlergiaAlimentoNaoCF);
            this.groupBox19.Controls.Add(this.rdAlergiaAlimentoSimCF);
            this.groupBox19.Enabled = false;
            this.groupBox19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox19.Location = new System.Drawing.Point(738, 75);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(227, 101);
            this.groupBox19.TabIndex = 48;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Alergia a Alimento?";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(6, 48);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(42, 16);
            this.label34.TabIndex = 17;
            this.label34.Text = "Qual?";
            this.label34.Visible = false;
            // 
            // txtAlergiaAlimentoCF
            // 
            this.txtAlergiaAlimentoCF.Location = new System.Drawing.Point(6, 67);
            this.txtAlergiaAlimentoCF.Name = "txtAlergiaAlimentoCF";
            this.txtAlergiaAlimentoCF.Size = new System.Drawing.Size(205, 22);
            this.txtAlergiaAlimentoCF.TabIndex = 17;
            this.txtAlergiaAlimentoCF.Visible = false;
            // 
            // rdAlergiaAlimentoNaoCF
            // 
            this.rdAlergiaAlimentoNaoCF.AutoSize = true;
            this.rdAlergiaAlimentoNaoCF.Location = new System.Drawing.Point(85, 22);
            this.rdAlergiaAlimentoNaoCF.Name = "rdAlergiaAlimentoNaoCF";
            this.rdAlergiaAlimentoNaoCF.Size = new System.Drawing.Size(51, 20);
            this.rdAlergiaAlimentoNaoCF.TabIndex = 15;
            this.rdAlergiaAlimentoNaoCF.TabStop = true;
            this.rdAlergiaAlimentoNaoCF.Text = "Não";
            this.rdAlergiaAlimentoNaoCF.UseVisualStyleBackColor = true;
            // 
            // rdAlergiaAlimentoSimCF
            // 
            this.rdAlergiaAlimentoSimCF.AutoSize = true;
            this.rdAlergiaAlimentoSimCF.Location = new System.Drawing.Point(9, 22);
            this.rdAlergiaAlimentoSimCF.Name = "rdAlergiaAlimentoSimCF";
            this.rdAlergiaAlimentoSimCF.Size = new System.Drawing.Size(48, 20);
            this.rdAlergiaAlimentoSimCF.TabIndex = 14;
            this.rdAlergiaAlimentoSimCF.TabStop = true;
            this.rdAlergiaAlimentoSimCF.Text = "Sim";
            this.rdAlergiaAlimentoSimCF.UseVisualStyleBackColor = true;
            this.rdAlergiaAlimentoSimCF.CheckedChanged += new System.EventHandler(this.rdAlergiaAlimentoSimCF_CheckedChanged);
            // 
            // groupBox18
            // 
            this.groupBox18.BackColor = System.Drawing.Color.Silver;
            this.groupBox18.Controls.Add(this.label31);
            this.groupBox18.Controls.Add(this.txtProblemaSaudeCF);
            this.groupBox18.Controls.Add(this.rdProblemaSaudeNaoCF);
            this.groupBox18.Controls.Add(this.rdProblemaSaudeSimCF);
            this.groupBox18.Enabled = false;
            this.groupBox18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox18.Location = new System.Drawing.Point(505, 75);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(227, 101);
            this.groupBox18.TabIndex = 47;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Tem Problemas de Saúde?";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(6, 48);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(42, 16);
            this.label31.TabIndex = 17;
            this.label31.Text = "Qual?";
            this.label31.Visible = false;
            // 
            // txtProblemaSaudeCF
            // 
            this.txtProblemaSaudeCF.Location = new System.Drawing.Point(6, 67);
            this.txtProblemaSaudeCF.Name = "txtProblemaSaudeCF";
            this.txtProblemaSaudeCF.Size = new System.Drawing.Size(205, 22);
            this.txtProblemaSaudeCF.TabIndex = 17;
            this.txtProblemaSaudeCF.Visible = false;
            // 
            // rdProblemaSaudeNaoCF
            // 
            this.rdProblemaSaudeNaoCF.AutoSize = true;
            this.rdProblemaSaudeNaoCF.Location = new System.Drawing.Point(85, 22);
            this.rdProblemaSaudeNaoCF.Name = "rdProblemaSaudeNaoCF";
            this.rdProblemaSaudeNaoCF.Size = new System.Drawing.Size(51, 20);
            this.rdProblemaSaudeNaoCF.TabIndex = 15;
            this.rdProblemaSaudeNaoCF.TabStop = true;
            this.rdProblemaSaudeNaoCF.Text = "Não";
            this.rdProblemaSaudeNaoCF.UseVisualStyleBackColor = true;
            // 
            // rdProblemaSaudeSimCF
            // 
            this.rdProblemaSaudeSimCF.AutoSize = true;
            this.rdProblemaSaudeSimCF.Location = new System.Drawing.Point(9, 22);
            this.rdProblemaSaudeSimCF.Name = "rdProblemaSaudeSimCF";
            this.rdProblemaSaudeSimCF.Size = new System.Drawing.Size(48, 20);
            this.rdProblemaSaudeSimCF.TabIndex = 14;
            this.rdProblemaSaudeSimCF.TabStop = true;
            this.rdProblemaSaudeSimCF.Text = "Sim";
            this.rdProblemaSaudeSimCF.UseVisualStyleBackColor = true;
            this.rdProblemaSaudeSimCF.CheckedChanged += new System.EventHandler(this.rdProblemaSaudeSimCF_CheckedChanged);
            // 
            // txtEscolaridadeCF
            // 
            this.txtEscolaridadeCF.Enabled = false;
            this.txtEscolaridadeCF.Location = new System.Drawing.Point(108, 288);
            this.txtEscolaridadeCF.Name = "txtEscolaridadeCF";
            this.txtEscolaridadeCF.Size = new System.Drawing.Size(243, 20);
            this.txtEscolaridadeCF.TabIndex = 72;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Enabled = false;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(18, 289);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(91, 16);
            this.label30.TabIndex = 73;
            this.label30.Text = "Escolaridade:";
            // 
            // txtIdadeCF
            // 
            this.txtIdadeCF.Enabled = false;
            this.txtIdadeCF.Location = new System.Drawing.Point(108, 184);
            this.txtIdadeCF.Name = "txtIdadeCF";
            this.txtIdadeCF.Size = new System.Drawing.Size(128, 20);
            this.txtIdadeCF.TabIndex = 43;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Enabled = false;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(58, 185);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(45, 16);
            this.label16.TabIndex = 71;
            this.label16.Text = "Idade:";
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.txtLaudo);
            this.groupBox23.Location = new System.Drawing.Point(22, 314);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(467, 223);
            this.groupBox23.TabIndex = 69;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "Laudo";
            // 
            // txtLaudo
            // 
            this.txtLaudo.Location = new System.Drawing.Point(7, 19);
            this.txtLaudo.Multiline = true;
            this.txtLaudo.Name = "txtLaudo";
            this.txtLaudo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtLaudo.Size = new System.Drawing.Size(450, 188);
            this.txtLaudo.TabIndex = 46;
            // 
            // txtFiliacao
            // 
            this.txtFiliacao.Enabled = false;
            this.txtFiliacao.Location = new System.Drawing.Point(108, 217);
            this.txtFiliacao.Multiline = true;
            this.txtFiliacao.Name = "txtFiliacao";
            this.txtFiliacao.Size = new System.Drawing.Size(381, 54);
            this.txtFiliacao.TabIndex = 45;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Enabled = false;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(41, 218);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(58, 16);
            this.label29.TabIndex = 67;
            this.label29.Text = "Filiação:";
            // 
            // txtCertidaoNasc
            // 
            this.txtCertidaoNasc.Enabled = false;
            this.txtCertidaoNasc.Location = new System.Drawing.Point(108, 151);
            this.txtCertidaoNasc.Name = "txtCertidaoNasc";
            this.txtCertidaoNasc.Size = new System.Drawing.Size(381, 20);
            this.txtCertidaoNasc.TabIndex = 42;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Enabled = false;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(16, 119);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(84, 16);
            this.label26.TabIndex = 65;
            this.label26.Text = "Cartão SUS :";
            // 
            // txtCartaoSUS
            // 
            this.txtCartaoSUS.Enabled = false;
            this.txtCartaoSUS.Location = new System.Drawing.Point(107, 118);
            this.txtCartaoSUS.Name = "txtCartaoSUS";
            this.txtCartaoSUS.Size = new System.Drawing.Size(150, 20);
            this.txtCartaoSUS.TabIndex = 40;
            // 
            // lblCertidaoNasc
            // 
            this.lblCertidaoNasc.AutoSize = true;
            this.lblCertidaoNasc.Enabled = false;
            this.lblCertidaoNasc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCertidaoNasc.Location = new System.Drawing.Point(3, 152);
            this.lblCertidaoNasc.Name = "lblCertidaoNasc";
            this.lblCertidaoNasc.Size = new System.Drawing.Size(102, 16);
            this.lblCertidaoNasc.TabIndex = 63;
            this.lblCertidaoNasc.Text = "Certidão Nasc. :";
            // 
            // btnNovoDependente
            // 
            this.btnNovoDependente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNovoDependente.Location = new System.Drawing.Point(935, 517);
            this.btnNovoDependente.Name = "btnNovoDependente";
            this.btnNovoDependente.Size = new System.Drawing.Size(133, 50);
            this.btnNovoDependente.TabIndex = 61;
            this.btnNovoDependente.Text = "Novo Dependente";
            this.btnNovoDependente.UseVisualStyleBackColor = true;
            this.btnNovoDependente.Click += new System.EventHandler(this.btnNovoDependente_Click);
            // 
            // lblComposicaoFamiliar
            // 
            this.lblComposicaoFamiliar.AutoSize = true;
            this.lblComposicaoFamiliar.Enabled = false;
            this.lblComposicaoFamiliar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblComposicaoFamiliar.Location = new System.Drawing.Point(651, 21);
            this.lblComposicaoFamiliar.Name = "lblComposicaoFamiliar";
            this.lblComposicaoFamiliar.Size = new System.Drawing.Size(138, 16);
            this.lblComposicaoFamiliar.TabIndex = 60;
            this.lblComposicaoFamiliar.Text = "Composição Familiar:";
            this.lblComposicaoFamiliar.Visible = false;
            // 
            // cboComposicaoFamiliar
            // 
            this.cboComposicaoFamiliar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboComposicaoFamiliar.Enabled = false;
            this.cboComposicaoFamiliar.FormattingEnabled = true;
            this.cboComposicaoFamiliar.Location = new System.Drawing.Point(796, 19);
            this.cboComposicaoFamiliar.Name = "cboComposicaoFamiliar";
            this.cboComposicaoFamiliar.Size = new System.Drawing.Size(263, 21);
            this.cboComposicaoFamiliar.TabIndex = 59;
            this.cboComposicaoFamiliar.Visible = false;
            this.cboComposicaoFamiliar.SelectedIndexChanged += new System.EventHandler(this.cboComposicaoFamiliar_SelectedIndexChanged);
            this.cboComposicaoFamiliar.SelectedValueChanged += new System.EventHandler(this.cboComposicaoFamiliar_SelectedValueChanged);
            // 
            // btnGravarDependente
            // 
            this.btnGravarDependente.Enabled = false;
            this.btnGravarDependente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGravarDependente.Location = new System.Drawing.Point(935, 582);
            this.btnGravarDependente.Name = "btnGravarDependente";
            this.btnGravarDependente.Size = new System.Drawing.Size(133, 78);
            this.btnGravarDependente.TabIndex = 58;
            this.btnGravarDependente.Text = "Gravar Dependente";
            this.btnGravarDependente.UseVisualStyleBackColor = true;
            this.btnGravarDependente.Click += new System.EventHandler(this.btnGravarDependente_Click);
            // 
            // groupBox21
            // 
            this.groupBox21.BackColor = System.Drawing.Color.Silver;
            this.groupBox21.Controls.Add(this.lblProfissaoComposicao);
            this.groupBox21.Controls.Add(this.txtProfissaoComposicao);
            this.groupBox21.Controls.Add(this.rdCarteiraAssinadaNaoCF);
            this.groupBox21.Controls.Add(this.rdCarteiraAssinadaSimCF);
            this.groupBox21.Enabled = false;
            this.groupBox21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox21.Location = new System.Drawing.Point(738, 292);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(227, 116);
            this.groupBox21.TabIndex = 52;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Possui Fonte de renda? ";
            // 
            // lblProfissaoComposicao
            // 
            this.lblProfissaoComposicao.AutoSize = true;
            this.lblProfissaoComposicao.Location = new System.Drawing.Point(10, 53);
            this.lblProfissaoComposicao.Name = "lblProfissaoComposicao";
            this.lblProfissaoComposicao.Size = new System.Drawing.Size(38, 16);
            this.lblProfissaoComposicao.TabIndex = 20;
            this.lblProfissaoComposicao.Text = "Qual:";
            this.lblProfissaoComposicao.Visible = false;
            // 
            // txtProfissaoComposicao
            // 
            this.txtProfissaoComposicao.Location = new System.Drawing.Point(9, 72);
            this.txtProfissaoComposicao.Name = "txtProfissaoComposicao";
            this.txtProfissaoComposicao.Size = new System.Drawing.Size(190, 22);
            this.txtProfissaoComposicao.TabIndex = 19;
            this.txtProfissaoComposicao.Visible = false;
            // 
            // rdCarteiraAssinadaNaoCF
            // 
            this.rdCarteiraAssinadaNaoCF.AutoSize = true;
            this.rdCarteiraAssinadaNaoCF.Location = new System.Drawing.Point(85, 23);
            this.rdCarteiraAssinadaNaoCF.Name = "rdCarteiraAssinadaNaoCF";
            this.rdCarteiraAssinadaNaoCF.Size = new System.Drawing.Size(51, 20);
            this.rdCarteiraAssinadaNaoCF.TabIndex = 15;
            this.rdCarteiraAssinadaNaoCF.TabStop = true;
            this.rdCarteiraAssinadaNaoCF.Text = "Não";
            this.rdCarteiraAssinadaNaoCF.UseVisualStyleBackColor = true;
            // 
            // rdCarteiraAssinadaSimCF
            // 
            this.rdCarteiraAssinadaSimCF.AutoSize = true;
            this.rdCarteiraAssinadaSimCF.Location = new System.Drawing.Point(9, 22);
            this.rdCarteiraAssinadaSimCF.Name = "rdCarteiraAssinadaSimCF";
            this.rdCarteiraAssinadaSimCF.Size = new System.Drawing.Size(48, 20);
            this.rdCarteiraAssinadaSimCF.TabIndex = 14;
            this.rdCarteiraAssinadaSimCF.TabStop = true;
            this.rdCarteiraAssinadaSimCF.Text = "Sim";
            this.rdCarteiraAssinadaSimCF.UseVisualStyleBackColor = true;
            this.rdCarteiraAssinadaSimCF.CheckedChanged += new System.EventHandler(this.rdCarteiraAssinadaSimComposicao_CheckedChanged);
            // 
            // groupBox20
            // 
            this.groupBox20.BackColor = System.Drawing.Color.Silver;
            this.groupBox20.Controls.Add(this.rdDeficienciaNaoComposicao);
            this.groupBox20.Controls.Add(this.rdDeficienciaSimComposicao);
            this.groupBox20.Enabled = false;
            this.groupBox20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox20.Location = new System.Drawing.Point(505, 518);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(227, 58);
            this.groupBox20.TabIndex = 54;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Possui deficiência?";
            // 
            // rdDeficienciaNaoComposicao
            // 
            this.rdDeficienciaNaoComposicao.AutoSize = true;
            this.rdDeficienciaNaoComposicao.Location = new System.Drawing.Point(85, 22);
            this.rdDeficienciaNaoComposicao.Name = "rdDeficienciaNaoComposicao";
            this.rdDeficienciaNaoComposicao.Size = new System.Drawing.Size(51, 20);
            this.rdDeficienciaNaoComposicao.TabIndex = 15;
            this.rdDeficienciaNaoComposicao.TabStop = true;
            this.rdDeficienciaNaoComposicao.Text = "Não";
            this.rdDeficienciaNaoComposicao.UseVisualStyleBackColor = true;
            // 
            // rdDeficienciaSimComposicao
            // 
            this.rdDeficienciaSimComposicao.AutoSize = true;
            this.rdDeficienciaSimComposicao.Location = new System.Drawing.Point(9, 22);
            this.rdDeficienciaSimComposicao.Name = "rdDeficienciaSimComposicao";
            this.rdDeficienciaSimComposicao.Size = new System.Drawing.Size(48, 20);
            this.rdDeficienciaSimComposicao.TabIndex = 14;
            this.rdDeficienciaSimComposicao.TabStop = true;
            this.rdDeficienciaSimComposicao.Text = "Sim";
            this.rdDeficienciaSimComposicao.UseVisualStyleBackColor = true;
            // 
            // txtRGComposicao
            // 
            this.txtRGComposicao.Enabled = false;
            this.txtRGComposicao.Location = new System.Drawing.Point(359, 84);
            this.txtRGComposicao.Name = "txtRGComposicao";
            this.txtRGComposicao.Size = new System.Drawing.Size(130, 20);
            this.txtRGComposicao.TabIndex = 39;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Enabled = false;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(323, 84);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 16);
            this.label9.TabIndex = 54;
            this.label9.Text = "RG:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Enabled = false;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(275, 55);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 16);
            this.label10.TabIndex = 52;
            this.label10.Text = "Estado Cívil:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Enabled = false;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(64, 54);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 16);
            this.label15.TabIndex = 51;
            this.label15.Text = "Sexo:";
            // 
            // cbEstadoCivilComposicao
            // 
            this.cbEstadoCivilComposicao.Enabled = false;
            this.cbEstadoCivilComposicao.FormattingEnabled = true;
            this.cbEstadoCivilComposicao.Items.AddRange(new object[] {
            "Selecione...",
            "Casado(a)",
            "Solteiro(a)",
            "Divorciado(a)",
            "Viuvo(a)",
            "Outros"});
            this.cbEstadoCivilComposicao.Location = new System.Drawing.Point(359, 53);
            this.cbEstadoCivilComposicao.Name = "cbEstadoCivilComposicao";
            this.cbEstadoCivilComposicao.Size = new System.Drawing.Size(130, 21);
            this.cbEstadoCivilComposicao.TabIndex = 37;
            // 
            // cbSexoComposicao
            // 
            this.cbSexoComposicao.Enabled = false;
            this.cbSexoComposicao.FormattingEnabled = true;
            this.cbSexoComposicao.Items.AddRange(new object[] {
            "Selecione...",
            "Masculino",
            "Feminino",
            "Não informado"});
            this.cbSexoComposicao.Location = new System.Drawing.Point(107, 52);
            this.cbSexoComposicao.Name = "cbSexoComposicao";
            this.cbSexoComposicao.Size = new System.Drawing.Size(121, 21);
            this.cbSexoComposicao.TabIndex = 36;
            // 
            // dtNascimentoComposicao
            // 
            this.dtNascimentoComposicao.Enabled = false;
            this.dtNascimentoComposicao.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtNascimentoComposicao.Location = new System.Drawing.Point(379, 115);
            this.dtNascimentoComposicao.Name = "dtNascimentoComposicao";
            this.dtNascimentoComposicao.Size = new System.Drawing.Size(108, 20);
            this.dtNascimentoComposicao.TabIndex = 41;
            this.dtNascimentoComposicao.Value = new System.DateTime(2021, 7, 21, 18, 59, 47, 0);
            // 
            // txtNomeComposicao
            // 
            this.txtNomeComposicao.Enabled = false;
            this.txtNomeComposicao.Location = new System.Drawing.Point(107, 21);
            this.txtNomeComposicao.Name = "txtNomeComposicao";
            this.txtNomeComposicao.Size = new System.Drawing.Size(382, 20);
            this.txtNomeComposicao.TabIndex = 35;
            this.txtNomeComposicao.TextChanged += new System.EventHandler(this.txtNomeComposicao_TextChanged);
            // 
            // txtTelefoneComposicao
            // 
            this.txtTelefoneComposicao.Enabled = false;
            this.txtTelefoneComposicao.Location = new System.Drawing.Point(310, 185);
            this.txtTelefoneComposicao.Name = "txtTelefoneComposicao";
            this.txtTelefoneComposicao.Size = new System.Drawing.Size(179, 20);
            this.txtTelefoneComposicao.TabIndex = 44;
            // 
            // txtCPFComposicao
            // 
            this.txtCPFComposicao.Enabled = false;
            this.txtCPFComposicao.Location = new System.Drawing.Point(107, 85);
            this.txtCPFComposicao.Name = "txtCPFComposicao";
            this.txtCPFComposicao.Size = new System.Drawing.Size(150, 20);
            this.txtCPFComposicao.TabIndex = 38;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Enabled = false;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(247, 186);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(64, 16);
            this.label17.TabIndex = 46;
            this.label17.Text = "Telefone:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Enabled = false;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(64, 85);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(36, 16);
            this.label18.TabIndex = 45;
            this.label18.Text = "CPF:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Enabled = false;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(266, 119);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(114, 16);
            this.label19.TabIndex = 44;
            this.label19.Text = "Data Nascimento:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Enabled = false;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(56, 22);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(47, 16);
            this.label20.TabIndex = 43;
            this.label20.Text = "Nome:";
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.Gainsboro;
            this.tabPage3.Controls.Add(this.btnLimpar);
            this.tabPage3.Controls.Add(this.lblHistoricoAcompanhamento);
            this.tabPage3.Controls.Add(this.cboHistoricoAcompanhamento);
            this.tabPage3.Controls.Add(this.btnSalvarAnotacao);
            this.tabPage3.Controls.Add(this.txtObservacao);
            this.tabPage3.Controls.Add(this.label24);
            this.tabPage3.Controls.Add(this.dtAcompanhamento);
            this.tabPage3.Controls.Add(this.label21);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1084, 673);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Acompanhamento";
            // 
            // btnLimpar
            // 
            this.btnLimpar.Location = new System.Drawing.Point(725, 75);
            this.btnLimpar.Name = "btnLimpar";
            this.btnLimpar.Size = new System.Drawing.Size(87, 31);
            this.btnLimpar.TabIndex = 63;
            this.btnLimpar.Text = "Limpar";
            this.btnLimpar.UseVisualStyleBackColor = true;
            this.btnLimpar.Click += new System.EventHandler(this.btnLimpar_Click);
            // 
            // lblHistoricoAcompanhamento
            // 
            this.lblHistoricoAcompanhamento.AutoSize = true;
            this.lblHistoricoAcompanhamento.Enabled = false;
            this.lblHistoricoAcompanhamento.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHistoricoAcompanhamento.Location = new System.Drawing.Point(840, 17);
            this.lblHistoricoAcompanhamento.Name = "lblHistoricoAcompanhamento";
            this.lblHistoricoAcompanhamento.Size = new System.Drawing.Size(191, 16);
            this.lblHistoricoAcompanhamento.TabIndex = 62;
            this.lblHistoricoAcompanhamento.Text = "Histórico de acompanhamento";
            this.lblHistoricoAcompanhamento.Visible = false;
            // 
            // cboHistoricoAcompanhamento
            // 
            this.cboHistoricoAcompanhamento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboHistoricoAcompanhamento.Enabled = false;
            this.cboHistoricoAcompanhamento.FormattingEnabled = true;
            this.cboHistoricoAcompanhamento.Location = new System.Drawing.Point(768, 36);
            this.cboHistoricoAcompanhamento.Name = "cboHistoricoAcompanhamento";
            this.cboHistoricoAcompanhamento.Size = new System.Drawing.Size(263, 21);
            this.cboHistoricoAcompanhamento.TabIndex = 61;
            this.cboHistoricoAcompanhamento.Visible = false;
            this.cboHistoricoAcompanhamento.SelectedIndexChanged += new System.EventHandler(this.cboHistoricoAcompanhamento_SelectedIndexChanged);
            // 
            // btnSalvarAnotacao
            // 
            this.btnSalvarAnotacao.Location = new System.Drawing.Point(728, 523);
            this.btnSalvarAnotacao.Name = "btnSalvarAnotacao";
            this.btnSalvarAnotacao.Size = new System.Drawing.Size(87, 46);
            this.btnSalvarAnotacao.TabIndex = 53;
            this.btnSalvarAnotacao.Text = "Salvar Anotação";
            this.btnSalvarAnotacao.UseVisualStyleBackColor = true;
            this.btnSalvarAnotacao.Click += new System.EventHandler(this.btnSalvarAnotacao_Click);
            // 
            // txtObservacao
            // 
            this.txtObservacao.Location = new System.Drawing.Point(20, 75);
            this.txtObservacao.Multiline = true;
            this.txtObservacao.Name = "txtObservacao";
            this.txtObservacao.Size = new System.Drawing.Size(699, 494);
            this.txtObservacao.TabIndex = 52;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Enabled = false;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(17, 56);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(75, 16);
            this.label24.TabIndex = 51;
            this.label24.Text = "Anotações:";
            // 
            // dtAcompanhamento
            // 
            this.dtAcompanhamento.Enabled = false;
            this.dtAcompanhamento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtAcompanhamento.Location = new System.Drawing.Point(176, 17);
            this.dtAcompanhamento.Name = "dtAcompanhamento";
            this.dtAcompanhamento.Size = new System.Drawing.Size(96, 20);
            this.dtAcompanhamento.TabIndex = 50;
            this.dtAcompanhamento.Value = new System.DateTime(2021, 7, 21, 18, 59, 47, 0);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Enabled = false;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(17, 17);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(152, 16);
            this.label21.TabIndex = 49;
            this.label21.Text = "Data Acompanhamento:";
            // 
            // btnGravar
            // 
            this.btnGravar.Enabled = false;
            this.btnGravar.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGravar.Location = new System.Drawing.Point(1176, 718);
            this.btnGravar.Name = "btnGravar";
            this.btnGravar.Size = new System.Drawing.Size(68, 35);
            this.btnGravar.TabIndex = 23;
            this.btnGravar.Text = "Gravar";
            this.btnGravar.UseVisualStyleBackColor = true;
            this.btnGravar.Click += new System.EventHandler(this.btnGravar_Click);
            // 
            // btnNovo
            // 
            this.btnNovo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNovo.Location = new System.Drawing.Point(1104, 718);
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(66, 35);
            this.btnNovo.TabIndex = 24;
            this.btnNovo.Text = "Novo";
            this.btnNovo.UseVisualStyleBackColor = true;
            this.btnNovo.Click += new System.EventHandler(this.btnNovo_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(269, 29);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(85, 18);
            this.label22.TabIndex = 27;
            this.label22.Text = "Orientação:";
            // 
            // cboOrientacao
            // 
            this.cboOrientacao.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboOrientacao.Enabled = false;
            this.cboOrientacao.FormattingEnabled = true;
            this.cboOrientacao.Items.AddRange(new object[] {
            "Selecione...",
            "SCFV",
            "SCFV Idosos",
            "SCFV Adolescentes",
            "SCFV Adultos",
            "Psicologia",
            "Psicopedagogia",
            "Apadrinhamento Afetivo",
            "Casa de Atenção à Pessoa Idosa",
            "Demanda espontânea",
            "Outros"});
            this.cboOrientacao.Location = new System.Drawing.Point(357, 27);
            this.cboOrientacao.Name = "cboOrientacao";
            this.cboOrientacao.Size = new System.Drawing.Size(175, 21);
            this.cboOrientacao.TabIndex = 26;
            this.cboOrientacao.SelectedIndexChanged += new System.EventHandler(this.cboOrientacao_SelectedIndexChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(613, 30);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(122, 18);
            this.label23.TabIndex = 28;
            this.label23.Text = "Data Cadastro:";
            // 
            // lblDataCadastro
            // 
            this.lblDataCadastro.AutoSize = true;
            this.lblDataCadastro.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataCadastro.Location = new System.Drawing.Point(741, 30);
            this.lblDataCadastro.Name = "lblDataCadastro";
            this.lblDataCadastro.Size = new System.Drawing.Size(90, 18);
            this.lblDataCadastro.TabIndex = 29;
            this.lblDataCadastro.Text = "__/__/____";
            // 
            // lblNumCadastro
            // 
            this.lblNumCadastro.AutoSize = true;
            this.lblNumCadastro.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNumCadastro.Location = new System.Drawing.Point(125, 26);
            this.lblNumCadastro.Name = "lblNumCadastro";
            this.lblNumCadastro.Size = new System.Drawing.Size(59, 20);
            this.lblNumCadastro.TabIndex = 30;
            this.lblNumCadastro.Text = "00000";
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.cbUsuariosEncontrados);
            this.groupBox22.Controls.Add(this.dtBusca);
            this.groupBox22.Controls.Add(this.label25);
            this.groupBox22.Controls.Add(this.cboBuscarPor);
            this.groupBox22.Controls.Add(this.btnBuscar);
            this.groupBox22.Controls.Add(this.txtBusca);
            this.groupBox22.Controls.Add(this.lblBuscarPor);
            this.groupBox22.Location = new System.Drawing.Point(1106, 8);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(193, 493);
            this.groupBox22.TabIndex = 32;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "Pesquisar";
            // 
            // cbUsuariosEncontrados
            // 
            this.cbUsuariosEncontrados.FormattingEnabled = true;
            this.cbUsuariosEncontrados.Items.AddRange(new object[] {
            "Selecione..."});
            this.cbUsuariosEncontrados.Location = new System.Drawing.Point(6, 123);
            this.cbUsuariosEncontrados.Name = "cbUsuariosEncontrados";
            this.cbUsuariosEncontrados.Size = new System.Drawing.Size(150, 21);
            this.cbUsuariosEncontrados.TabIndex = 28;
            this.cbUsuariosEncontrados.Visible = false;
            this.cbUsuariosEncontrados.SelectedIndexChanged += new System.EventHandler(this.cbUsuariosEncontrados_SelectedIndexChanged);
            // 
            // dtBusca
            // 
            this.dtBusca.Location = new System.Drawing.Point(6, 123);
            this.dtBusca.Name = "dtBusca";
            this.dtBusca.Size = new System.Drawing.Size(149, 20);
            this.dtBusca.TabIndex = 27;
            this.dtBusca.Value = new System.DateTime(2024, 3, 9, 0, 0, 0, 0);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(3, 37);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(78, 16);
            this.label25.TabIndex = 26;
            this.label25.Text = "Buscar por: ";
            // 
            // cboBuscarPor
            // 
            this.cboBuscarPor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBuscarPor.Enabled = false;
            this.cboBuscarPor.FormattingEnabled = true;
            this.cboBuscarPor.Items.AddRange(new object[] {
            "Selecione...",
            "CPF",
            "Nome",
            "Número da Ficha",
            "Data"});
            this.cboBuscarPor.Location = new System.Drawing.Point(6, 59);
            this.cboBuscarPor.Name = "cboBuscarPor";
            this.cboBuscarPor.Size = new System.Drawing.Size(150, 21);
            this.cboBuscarPor.TabIndex = 25;
            this.cboBuscarPor.SelectedIndexChanged += new System.EventHandler(this.cboBuscarPor_SelectedIndexChanged);
            // 
            // btnBuscar
            // 
            this.btnBuscar.Location = new System.Drawing.Point(6, 146);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(69, 23);
            this.btnBuscar.TabIndex = 17;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtBusca
            // 
            this.txtBusca.Location = new System.Drawing.Point(6, 123);
            this.txtBusca.Name = "txtBusca";
            this.txtBusca.Size = new System.Drawing.Size(150, 20);
            this.txtBusca.TabIndex = 15;
            // 
            // lblBuscarPor
            // 
            this.lblBuscarPor.AutoSize = true;
            this.lblBuscarPor.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBuscarPor.Location = new System.Drawing.Point(3, 102);
            this.lblBuscarPor.Name = "lblBuscarPor";
            this.lblBuscarPor.Size = new System.Drawing.Size(64, 16);
            this.lblBuscarPor.TabIndex = 16;
            this.lblBuscarPor.Text = "A definir...";
            // 
            // btnSair
            // 
            this.btnSair.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSair.Location = new System.Drawing.Point(1250, 718);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(47, 35);
            this.btnSair.TabIndex = 33;
            this.btnSair.Text = "Sair";
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // FrmFormulario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1304, 765);
            this.Controls.Add(this.btnSair);
            this.Controls.Add(this.groupBox22);
            this.Controls.Add(this.lblNumCadastro);
            this.Controls.Add(this.lblDataCadastro);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.cboOrientacao);
            this.Controls.Add(this.btnNovo);
            this.Controls.Add(this.btnGravar);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.label11);
            this.Name = "FrmFormulario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Cadastro Assistido";
            this.Load += new System.EventHandler(this.FrmFormulario_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.RadioButton rdEnsFundamentalincomp;
        public System.Windows.Forms.RadioButton rdEnsFundamentalComp;
        public System.Windows.Forms.TextBox txtNome;
        public System.Windows.Forms.TextBox txtEndereco;
        public System.Windows.Forms.TextBox txtTelefone;
        public System.Windows.Forms.TextBox txtMae;
        public System.Windows.Forms.TextBox txtPai;
        public System.Windows.Forms.TextBox txtCPF;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.TabControl tabControl1;
        public System.Windows.Forms.TabPage tabPage1;
        public System.Windows.Forms.TabPage tabPage2;
        public System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.RadioButton rdFilhosNao;
        public System.Windows.Forms.RadioButton rdFilhosSim;
        public System.Windows.Forms.Label lblQtdFilhos;
        public System.Windows.Forms.TextBox txtQtdFilhos;
        public System.Windows.Forms.TextBox txtRG;
        public System.Windows.Forms.Label label14;
        public System.Windows.Forms.Label label13;
        public System.Windows.Forms.Label label12;
        public System.Windows.Forms.ComboBox cboEstadoCivil;
        public System.Windows.Forms.ComboBox cboSexo;
        public System.Windows.Forms.DateTimePicker dtDataNasc;
        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.GroupBox groupBox4;
        public System.Windows.Forms.RadioButton rdRecebePensaoNao;
        public System.Windows.Forms.RadioButton rdRecebePensaoSim;
        public System.Windows.Forms.GroupBox groupBox3;
        public System.Windows.Forms.RadioButton rdCrasNao;
        public System.Windows.Forms.RadioButton rdCrasSim;
        public System.Windows.Forms.GroupBox groupBox7;
        public System.Windows.Forms.RadioButton rdPossuiDeficienciaNao;
        public System.Windows.Forms.RadioButton rdPossuiDeficienciaSim;
        public System.Windows.Forms.GroupBox groupBox6;
        public System.Windows.Forms.RadioButton rdAuxilioPrevNao;
        public System.Windows.Forms.RadioButton rdAuxilioPrevSim;
        public System.Windows.Forms.GroupBox groupBox5;
        public System.Windows.Forms.Label lblQualDoenca;
        public System.Windows.Forms.TextBox txtQualDoenca;
        public System.Windows.Forms.RadioButton rdProbSaudeNao;
        public System.Windows.Forms.RadioButton rdProbSaudeSim;
        public System.Windows.Forms.GroupBox groupBox10;
        public System.Windows.Forms.RadioButton rdIdadeMaior60;
        public System.Windows.Forms.RadioButton rdIdade30a59;
        public System.Windows.Forms.RadioButton rdIdade18a29;
        public System.Windows.Forms.RadioButton rdIdade15a17;
        public System.Windows.Forms.RadioButton rdIdade12a14;
        public System.Windows.Forms.RadioButton rdIdade7a11;
        public System.Windows.Forms.RadioButton rdIdade0a6;
        public System.Windows.Forms.GroupBox groupBox9;
        public System.Windows.Forms.RadioButton rdIdadeMaeMaior21;
        public System.Windows.Forms.RadioButton rdIdadeMae18a21;
        public System.Windows.Forms.RadioButton rdIdadeMaeMenor18;
        public System.Windows.Forms.GroupBox groupBox8;
        public System.Windows.Forms.RadioButton rdMaePresenteNao;
        public System.Windows.Forms.RadioButton rdMaePresenteSim;
        public System.Windows.Forms.GroupBox groupBox13;
        public System.Windows.Forms.Label lblValorBolsaFamilia;
        public System.Windows.Forms.TextBox txtvalorBolsaFamilia;
        public System.Windows.Forms.RadioButton rdBolsaFamiliaNao;
        public System.Windows.Forms.RadioButton rdBolsaFamiliaSim;
        public System.Windows.Forms.GroupBox groupBox12;
        public System.Windows.Forms.RadioButton rdCarteiraAssinadaNao;
        public System.Windows.Forms.RadioButton rdCarteiraAssinadaSim;
        public System.Windows.Forms.GroupBox groupBox11;
        public System.Windows.Forms.RadioButton rdEnsSuperiorIncomp;
        public System.Windows.Forms.RadioButton rdEnsSuperiorComp;
        public System.Windows.Forms.RadioButton rdEnsMedioIncomp;
        public System.Windows.Forms.RadioButton rdEnsMedioComp;
        public System.Windows.Forms.GroupBox groupBox14;
        public System.Windows.Forms.RadioButton rdAlugada;
        public System.Windows.Forms.RadioButton rdInvadida;
        public System.Windows.Forms.Label lblValorAluguel;
        public System.Windows.Forms.TextBox txtValorAluguel;
        public System.Windows.Forms.RadioButton rdCedida;
        public System.Windows.Forms.RadioButton rdPropria;
        public System.Windows.Forms.GroupBox groupBox15;
        public System.Windows.Forms.RadioButton rdMaeApoioPaisNao;
        public System.Windows.Forms.RadioButton rdMaeApoioPaisSim;
        public System.Windows.Forms.GroupBox groupBox16;
        public System.Windows.Forms.Label lblLoas;
        public System.Windows.Forms.TextBox txtLoas;
        public System.Windows.Forms.RadioButton rdLoasNao;
        public System.Windows.Forms.RadioButton rdLoasSim;
        public System.Windows.Forms.GroupBox groupBox17;
        public System.Windows.Forms.TextBox txtQtdPessoasCasa;
        public System.Windows.Forms.Label lblProfissao;
        public System.Windows.Forms.TextBox txtProfissao;
        public System.Windows.Forms.GroupBox groupBox21;
        public System.Windows.Forms.Label lblProfissaoComposicao;
        public System.Windows.Forms.TextBox txtProfissaoComposicao;
        public System.Windows.Forms.RadioButton rdCarteiraAssinadaNaoCF;
        public System.Windows.Forms.RadioButton rdCarteiraAssinadaSimCF;
        public System.Windows.Forms.GroupBox groupBox20;
        public System.Windows.Forms.RadioButton rdDeficienciaNaoComposicao;
        public System.Windows.Forms.RadioButton rdDeficienciaSimComposicao;
        public System.Windows.Forms.TextBox txtRGComposicao;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label label15;
        public System.Windows.Forms.ComboBox cbEstadoCivilComposicao;
        public System.Windows.Forms.ComboBox cbSexoComposicao;
        public System.Windows.Forms.DateTimePicker dtNascimentoComposicao;
        public System.Windows.Forms.TextBox txtNomeComposicao;
        public System.Windows.Forms.TextBox txtTelefoneComposicao;
        public System.Windows.Forms.TextBox txtCPFComposicao;
        public System.Windows.Forms.Label label17;
        public System.Windows.Forms.Label label18;
        public System.Windows.Forms.Label label19;
        public System.Windows.Forms.Label label20;
        public System.Windows.Forms.Button btnGravar;
        public System.Windows.Forms.Button btnNovo;
        public System.Windows.Forms.Label label22;
        public System.Windows.Forms.ComboBox cboOrientacao;
        public System.Windows.Forms.Label label23;
        public System.Windows.Forms.Label lblDataCadastro;
        public System.Windows.Forms.Label lblNumCadastro;
        public System.Windows.Forms.Button btnGravarDependente;
        private System.Windows.Forms.GroupBox groupBox22;
        public System.Windows.Forms.TextBox txtBusca;
        public System.Windows.Forms.Label lblBuscarPor;
        public System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.Button btnBuscar;
        public System.Windows.Forms.Label lblComposicaoFamiliar;
        public System.Windows.Forms.ComboBox cboComposicaoFamiliar;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox txtObservacao;
        public System.Windows.Forms.Label label24;
        public System.Windows.Forms.DateTimePicker dtAcompanhamento;
        public System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button btnSalvarAnotacao;
        public System.Windows.Forms.Label lblHistoricoAcompanhamento;
        public System.Windows.Forms.ComboBox cboHistoricoAcompanhamento;
        private System.Windows.Forms.Button btnLimpar;
        public System.Windows.Forms.Label label25;
        public System.Windows.Forms.ComboBox cboBuscarPor;
        public System.Windows.Forms.Button btnImprimirAssistido;
        public System.Windows.Forms.Button btnNovoDependente;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.ComboBox cboBairro;
        public System.Windows.Forms.TextBox txtCertidaoNasc;
        public System.Windows.Forms.Label label26;
        public System.Windows.Forms.TextBox txtCartaoSUS;
        public System.Windows.Forms.Label lblCertidaoNasc;
        public System.Windows.Forms.RadioButton rdAnalfabeto;
        public System.Windows.Forms.RadioButton rdPrimario;
        private System.Windows.Forms.DateTimePicker dtBusca;
        public System.Windows.Forms.TextBox txtNis;
        public System.Windows.Forms.Label label27;
        public System.Windows.Forms.TextBox txtResponsavel;
        public System.Windows.Forms.Label label28;
        public System.Windows.Forms.TextBox txtFiliacao;
        public System.Windows.Forms.Label label29;
        private System.Windows.Forms.GroupBox groupBox23;
        public System.Windows.Forms.TextBox txtLaudo;
        public System.Windows.Forms.TextBox txtIdadeCF;
        public System.Windows.Forms.Label label16;
        public System.Windows.Forms.GroupBox groupBox18;
        public System.Windows.Forms.Label label31;
        public System.Windows.Forms.TextBox txtProblemaSaudeCF;
        public System.Windows.Forms.RadioButton rdProblemaSaudeNaoCF;
        public System.Windows.Forms.RadioButton rdProblemaSaudeSimCF;
        public System.Windows.Forms.TextBox txtEscolaridadeCF;
        public System.Windows.Forms.Label label30;
        public System.Windows.Forms.GroupBox groupBox26;
        public System.Windows.Forms.Label label37;
        public System.Windows.Forms.TextBox txtAlgumaAtividadeCF;
        public System.Windows.Forms.RadioButton rdAlgumaAtividadeNaoCF;
        public System.Windows.Forms.RadioButton rdAlgumaAtividadeSimCF;
        public System.Windows.Forms.GroupBox groupBox25;
        public System.Windows.Forms.Label label36;
        public System.Windows.Forms.TextBox txtAtividadesAlemScfvCF;
        public System.Windows.Forms.RadioButton rdAtividadesAlemScfvNaoCF;
        public System.Windows.Forms.RadioButton rdAtividadesAlemScfvSimCF;
        public System.Windows.Forms.GroupBox groupBox24;
        public System.Windows.Forms.Label label35;
        public System.Windows.Forms.TextBox txtUsoMedicamentoCF;
        public System.Windows.Forms.RadioButton rdUsoMedicamentoNaoCF;
        public System.Windows.Forms.RadioButton rdUsoMedicamentoSimCF;
        public System.Windows.Forms.GroupBox groupBox19;
        public System.Windows.Forms.Label label34;
        public System.Windows.Forms.TextBox txtAlergiaAlimentoCF;
        public System.Windows.Forms.RadioButton rdAlergiaAlimentoNaoCF;
        public System.Windows.Forms.RadioButton rdAlergiaAlimentoSimCF;
        public System.Windows.Forms.GroupBox groupBox27;
        public System.Windows.Forms.Label label32;
        public System.Windows.Forms.TextBox txtCadastroCrasCF;
        public System.Windows.Forms.RadioButton rdCadastroCrasNaoCF;
        public System.Windows.Forms.RadioButton rdCadastroCrasSimCF;
        private System.Windows.Forms.ComboBox cbUsuariosEncontrados;
        private System.Windows.Forms.Button btnImprimiComposicao;
        private System.Windows.Forms.Label lblOut;
        private System.Windows.Forms.Label lblOutCF;
    }
}

