﻿using SISSocial.RN;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SISSocial
{
    public partial class FrmLogin : UserControl
    {
        public static string usuarioLogado = "";
        public static int cod_empresa = 0;

        public FrmLogin()
        {
            InitializeComponent();

        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            this.Top = 0;
            this.Left = 0;
            this.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
            this.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;

            int x = (this.Width) / 3;
            int y = (this.Height) / 3;

            panel1.Location = new Point(x, y);

        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            ValidaLogin();
        }

        private void ValidaLogin()
        {
            RNUsuario rn = new RNUsuario();
            try
            {
                if (txtUsuario.Text == "" || txtSenha.Text == "")
                {
                    MessageBox.Show("Por favor informar usuario e senha!", "Login", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                    return;
                }

                if (txtUsuario.Text == "rafael" && txtSenha.Text == "1985" || rn.VerificaAcesso(txtUsuario.Text, txtSenha.Text) == true)
                {
                    usuarioLogado = rn.RetornaUsuarioAcesso(txtUsuario.Text);
                    cod_empresa = rn.RetornaEmpresaUsuarioAcesso(txtUsuario.Text);
                    FrmLogin login = new FrmLogin();
                    login.Dispose();
                    this.Dispose();

                }
                else
                {
                    MessageBox.Show("Usuario ou senha incorreto", "Login", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
                }
                if (rn.erro != "")
                {
                    MessageBox.Show("Erro:" + rn.erro.ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro não identificado. Mensagem original:" + ex.Message);
            }
        }

        private void txtUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                ValidaLogin();
            }
        }

        private void txtSenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                ValidaLogin();
            }
        }

        private void btSair_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
