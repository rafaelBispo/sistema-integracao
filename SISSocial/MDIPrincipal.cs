﻿using SISSocial.RN;
using System;
using System.Windows.Forms;

namespace SISSocial
{
    public partial class MDIPrincipal : Form
    {
        private int childFormNumber = 0;
        public string usuarioLogado = "";
        public static int cod_empresa = 0;

        public MDIPrincipal()
        {
            InitializeComponent();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new Form();
            childForm.MdiParent = this;
            childForm.Text = "Janela " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Arquivos de texto (*.txt)|*.txt|Todos os arquivos (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Arquivos de texto (*.txt)|*.txt|Todos os arquivos (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void cadastroToolStripMenuItem_Click(object sender, EventArgs e)
        {
            TipoAcessoUsuario();

        }

        private void assistidoToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void MDIPrincipal_Load(object sender, EventArgs e)
        {
            FrmLogin login = new FrmLogin();
            login.Parent = this;
            login.Show();
        }

        private void TipoAcessoUsuario()
        {
            RNUsuario rn = new RNUsuario();

            try
            {
                if (rn.RetornaTipoAcesso(FrmLogin.usuarioLogado) == 1)
                {
                    usuárioToolStripMenuItem.Visible = true;
                }
                else
                {
                    usuárioToolStripMenuItem.Visible = false;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao validar tipo de acesso do usuario", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
            }
        }

        private void assistidosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmFormulario frm = new FrmFormulario();
            frm.MdiParent = this;
            frm.usuarioLogado = FrmLogin.usuarioLogado;
            frm.Show();
        }

        private void usuárioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmCadastroUsuario frm = new FrmCadastroUsuario();
            frm.MdiParent = this;
            frm.Show();
        }

        private void MDIPrincipal_Activated(object sender, EventArgs e, bool pLogado)
        {
            if (pLogado == true)
            {
                menuStrip.Enabled = true;
                usuarioLogado = FrmLogin.usuarioLogado;
            }
        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void pesquisaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmPesquisa frm = new FrmPesquisa();
            frm.MdiParent = this;
            frm.usuarioLogado = FrmLogin.usuarioLogado;
            frm.Show();
        }

        private void rELATÓRIODEATIVIDADESEquipeTecnicaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAtividadesEquipeTecnica frm = new frmAtividadesEquipeTecnica();
            frm.MdiParent = this;
            frm.Show();
        }

        private void relatórioDeIntervençõesDiáriasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmRelatorioIntervençõesDiarias frm = new frmRelatorioIntervençõesDiarias();
            frm.MdiParent = this;
            frm.Show();
        }
    }
}
