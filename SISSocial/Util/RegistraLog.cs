﻿using SISSocial.Classes;
using System;
using System.IO;
using System.Net;
using System.Reflection;

namespace SISSocial.Util
{
    public static class RegistraLog
    {
        private static string caminhoExe = string.Empty;
        public static bool Log(string strMensagem, string strNomeArquivo = "ArquivoLog")
        {
            try
            {
                caminhoExe = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                string caminhoArquivo = Path.Combine(caminhoExe, strNomeArquivo);
                if (!File.Exists(caminhoArquivo))
                {
                    FileStream arquivo = File.Create(caminhoArquivo);
                    arquivo.Close();
                }
                using (StreamWriter w = File.AppendText(caminhoArquivo))
                {
                    AppendLog(strMensagem, w);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        private static void AppendLog(string logMensagem, TextWriter txtWriter)
        {
            try
            {
                txtWriter.Write("\r\nLog Entrada : ");
                txtWriter.WriteLine($"{DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
                txtWriter.WriteLine("  :");
                txtWriter.WriteLine($"  :{logMensagem}");
                txtWriter.WriteLine("------------------------------------");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void gravaLog(String texto)
        {
            using (StreamWriter outputFile = new StreamWriter("log.dat", true))
            {
                String data = DateTime.Now.ToShortDateString();
                String hora = DateTime.Now.ToShortTimeString();
                String computador = Dns.GetHostName();
                outputFile.WriteLine(data + " " + hora + " (" + computador + ")" + texto);
            }
        }



        #region "LOGS PERSONALIZADOS"
        public static void textoLogInsereAssistido(Assistido pAssis)
        {
            string texto = "";
            try
            {
                texto = "FrmFormulario_InsereAssistido" + pAssis.Codigo + pAssis.Nome;
                gravaLog(texto);
            }
            catch (Exception ex)
            {

            }
        }

        public static void textoLogInsereComposicaoFamiliar(ComposicaoFamiliar pComp)
        {
            string texto = "";
            try
            {
                texto = "FrmFormulario_InsereComposicaoFamiliar" + pComp.Codigo + pComp.Nome;
                gravaLog(texto);
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

    }
}
