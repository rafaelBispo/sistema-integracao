﻿Public Class RemessaCemig


#Region "Campos"


    Private mID As Integer
    Private mCod_EAN_Cliente As String
    Private mDDD As String
    Private mTelefone As String
    Private mNomeCliente As String
    Private mCPF As String
    Private mAutorizante As String
    Private mValor As String
    Private mNumeroInstalacao As String
    Private mMesEnvio As String

    Public Property MesEnvio As String
        Get
            Return mMesEnvio
        End Get
        Set(value As String)
            mMesEnvio = value
        End Set
    End Property

    Public Property NumeroInstalacao As String
        Get
            Return mNumeroInstalacao
        End Get
        Set(value As String)
            mNumeroInstalacao = value
        End Set
    End Property

    Public Property Valor As String
        Get
            Return mValor
        End Get
        Set(value As String)
            mValor = value
        End Set
    End Property

    Public Property Autorizante As String
        Get
            Return mAutorizante
        End Get
        Set(value As String)
            mAutorizante = value
        End Set
    End Property

    Public Property CPF As String
        Get
            Return mCPF
        End Get
        Set(value As String)
            mCPF = value
        End Set
    End Property

    Public Property NomeCliente As String
        Get
            Return mNomeCliente
        End Get
        Set(value As String)
            mNomeCliente = value
        End Set
    End Property

    Public Property Telefone As String
        Get
            Return mTelefone
        End Get
        Set(value As String)
            mTelefone = value
        End Set
    End Property

    Public Property DDD As String
        Get
            Return mDDD
        End Get
        Set(value As String)
            mDDD = value
        End Set
    End Property

    Public Property Cod_EAN_Cliente As String
        Get
            Return mCod_EAN_Cliente
        End Get
        Set(value As String)
            mCod_EAN_Cliente = value
        End Set
    End Property

    Public Property ID As Integer
        Get
            Return mID
        End Get
        Set(value As Integer)
            [mID] = value
        End Set
    End Property


#End Region

End Class
