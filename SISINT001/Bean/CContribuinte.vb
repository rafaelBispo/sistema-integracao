﻿Public Class CContribuinte

#Region "Campos"


    Private mCodCliente As Integer
    Private mCod_EAN_Cliente As String
    Private mNomeCliente1 As String
    Private mNomeCliente2 As String
    Private mOperador As String
    Private mCNPJ_CPF As String
    Private mIE_CI As String
    Private mEndereco As String
    Private mBairro As String
    Private mCidade As String
    Private mUF As String
    Private mCEP As String
    Private mTelefone1 As String
    Private mTelefone2 As String
    Private mEmail As String
    Private mDataCadastro As String
    Private mDataNascimento As String
    Private mCod_Categoria As Integer
    Private mObservacao As String
    Private mReferencia As String
    Private mDescontinuado As String
    Private mCod_grupo As Integer
    Private mValor As Decimal
    Private mCod_Especie As Integer
    Private mDiaLimite As Integer
    Private mDt_ligar As String
    Private mDt_inativo As String
    Private mCod_empresa As Integer
    Private mCod_Cidade As Integer
    Private mCod_Bairro As Integer
    Private mCod_Operador As Integer
    Private mTel_desligado As String
    Private mNao_pedir_extra As String
    Private mCod_Usuario As Integer
    Private mDT_NC_MP As String
    Private mNao_Pedir_Mensal As String
    Private mDT_Reajuste As String
    Private mCod_Regiao As Integer
    Private mDDD As String
    Private mClienteOi As String
    Private mCategoriaOi As String
    Private mSituacaoOi As String

    Private mRequisicao_lista_id As Integer
    Private mRequisicao_id As Integer
    Private mConfirmado As Integer
    Private mTipo_mov As String
    Private mTipo_cobranca As String
    Private mCobrador As String
    Private mCod_opera As String
    Private mData_mov As String
    Private mCod_contribuinte As String
    Private mNome_contribuinte As String
    Private mTipo_contrib As String
    Private mCod_recibo As String
    Private mDia As Integer
    Private mCod_cobrador As Integer
    Private mSituacao As Integer
    Private mDt_nc As String

    Private mData_venc As String
    Private mCod_mov_contribuicao As Long
    Private mNao_conforme As Integer

    Private mData_impressao As String
    Private mTelefone3 As String
    Private mTelefone4 As String
    Private mTelefone5 As String
    Private mTelefone6 As String
    Private mTelefone7 As String
    Private mNumeroInstalacao As String
    Private mTelefonePrincipal As String








#End Region


#Region "Propriedades"

    Public Property CodCliente() As Integer
        Get
            Return mCodCliente
        End Get
        Set(ByVal value As Integer)
            mCodCliente = value
        End Set
    End Property

    Public Property Cod_EAN_Cliente() As String
        Get
            Return mCod_EAN_Cliente
        End Get
        Set(ByVal value As String)
            mCod_EAN_Cliente = value
        End Set
    End Property

    Public Property NomeCliente1() As String
        Get
            Return mNomeCliente1
        End Get
        Set(ByVal value As String)
            mNomeCliente1 = value
        End Set
    End Property

    Public Property NomeCliente2() As String
        Get
            Return mNomeCliente2
        End Get
        Set(ByVal value As String)
            mNomeCliente2 = value
        End Set
    End Property

    Public Property Operador() As String
        Get
            Return mOperador
        End Get
        Set(ByVal value As String)
            mOperador = value
        End Set
    End Property

    Public Property CNPJ_CPF() As String
        Get
            Return mCNPJ_CPF
        End Get
        Set(ByVal value As string)
            mCNPJ_CPF = value
        End Set
    End Property

    Public Property IE_CI() As String
        Get
            Return mIE_CI
        End Get
        Set(ByVal value As String)
            mIE_CI = value
        End Set
    End Property

    Public Property Endereco() As String
        Get
            Return mEndereco
        End Get
        Set(ByVal value As String)
            mEndereco = value
        End Set
    End Property

    Public Property Bairro() As String
        Get
            Return mBairro
        End Get
        Set(ByVal value As String)
            mBairro = value
        End Set
    End Property

    Public Property Cidade() As String
        Get
            Return mCidade
        End Get
        Set(ByVal value As String)
            mCidade = value
        End Set
    End Property

    Public Property UF() As String
        Get
            Return mUF
        End Get
        Set(ByVal value As String)
            mUF = value
        End Set
    End Property

    Public Property CEP() As String
        Get
            Return mCEP
        End Get
        Set(ByVal value As String)
            mCEP = value
        End Set
    End Property

    Public Property Telefone1() As String
        Get
            Return mTelefone1
        End Get
        Set(ByVal value As String)
            mTelefone1 = value
        End Set
    End Property

    Public Property Telefone2() As String
        Get
            Return mTelefone2
        End Get
        Set(ByVal value As String)
            mTelefone2 = value
        End Set
    End Property

    Public Property Email() As String
        Get
            Return mEmail
        End Get
        Set(ByVal value As String)
            mEmail = value
        End Set
    End Property

    Public Property DataCadastro() As String
        Get
            Return mDataCadastro
        End Get
        Set(ByVal value As String)
            mDataCadastro = value
        End Set
    End Property

    Public Property DataNascimento() As String
        Get
            Return mDataNascimento
        End Get
        Set(ByVal value As String)
            mDataNascimento = value
        End Set
    End Property

    Public Property Cod_Categoria() As Integer
        Get
            Return mCod_Categoria
        End Get
        Set(ByVal value As Integer)
            mCod_Categoria = value
        End Set
    End Property

    Public Property Observacao() As String
        Get
            Return mObservacao
        End Get
        Set(ByVal value As String)
            mObservacao = value
        End Set
    End Property


    Public Property Referencia() As String
        Get
            Return mReferencia
        End Get
        Set(ByVal value As String)
            mReferencia = value
        End Set
    End Property

    Public Property Descontinuado() As String
        Get
            Return mDescontinuado
        End Get
        Set(ByVal value As String)
            mDescontinuado = value
        End Set
    End Property


    Public Property Cod_grupo() As Integer
        Get
            Return mCod_grupo
        End Get
        Set(ByVal value As Integer)
            mCod_grupo = value
        End Set
    End Property

    Public Property Valor() As Decimal
        Get
            Return mValor
        End Get
        Set(ByVal value As Decimal)
            mValor = value
        End Set
    End Property

    Public Property Cod_Especie() As Integer
        Get
            Return mCod_Especie
        End Get
        Set(ByVal value As Integer)
            mCod_Especie = value
        End Set
    End Property

    Public Property DiaLimite() As Integer
        Get
            Return mDiaLimite
        End Get
        Set(ByVal value As Integer)
            mDiaLimite = value
        End Set
    End Property

    Public Property Dt_ligar() As String
        Get
            Return mDt_ligar
        End Get
        Set(ByVal value As String)
            mDt_ligar = value
        End Set
    End Property



    Public Property Dt_inativo() As String
        Get
            Return mDt_inativo
        End Get
        Set(ByVal value As String)
            mDt_inativo = value
        End Set
    End Property


    Public Property Cod_empresa() As Integer
        Get
            Return mCod_empresa
        End Get
        Set(ByVal value As Integer)
            mCod_empresa = value
        End Set
    End Property


    Public Property Cod_Cidade() As Integer
        Get
            Return mCod_Cidade
        End Get
        Set(ByVal value As Integer)
            mCod_Cidade = value
        End Set
    End Property



    Public Property Cod_Bairro() As Integer
        Get
            Return mCod_Bairro
        End Get
        Set(ByVal value As Integer)
            mCod_Bairro = value
        End Set
    End Property


    Public Property Cod_Operador() As Integer
        Get
            Return mCod_Operador
        End Get
        Set(ByVal value As Integer)
            mCod_Operador = value
        End Set
    End Property


    Public Property Tel_desligado() As String
        Get
            Return mTel_desligado
        End Get
        Set(ByVal value As String)
            mTel_desligado = value
        End Set
    End Property


    Public Property Nao_pedir_extra() As String
        Get
            Return mNao_pedir_extra
        End Get
        Set(ByVal value As String)
            mNao_pedir_extra = value
        End Set
    End Property


    Public Property Cod_Usuario() As Integer
        Get
            Return mCod_Usuario
        End Get
        Set(ByVal value As Integer)
            mCod_Usuario = value
        End Set
    End Property


    Public Property DT_NC_MP() As String
        Get
            Return mDT_NC_MP
        End Get
        Set(ByVal value As String)
            mDT_NC_MP = value
        End Set
    End Property


    Public Property Nao_Pedir_Mensal() As String
        Get
            Return mNao_Pedir_Mensal
        End Get
        Set(ByVal value As String)
            mNao_Pedir_Mensal = value
        End Set
    End Property


    Public Property DT_Reajuste() As String
        Get
            Return mDT_Reajuste
        End Get
        Set(ByVal value As String)
            mDT_Reajuste = value
        End Set
    End Property


    Public Property Cod_Regiao() As Integer
        Get
            Return mCod_Regiao
        End Get
        Set(ByVal value As Integer)
            mCod_Regiao = value
        End Set
    End Property

    Public Property DDD() As String
        Get
            Return mDDD
        End Get
        Set(ByVal value As String)
            mDDD = value
        End Set
    End Property

    Public Property ClienteOi() As String
        Get
            Return mClienteOi
        End Get
        Set(ByVal value As String)
            mClienteOi = value
        End Set
    End Property

    Public Property CategoriaOi() As String
        Get
            Return mCategoriaOi
        End Get
        Set(ByVal value As String)
            mCategoriaOi = value
        End Set
    End Property

    Public Property SituacaoOi() As String
        Get
            Return mSituacaoOi
        End Get
        Set(ByVal value As String)
            mSituacaoOi = value
        End Set
    End Property

    Public Property Requisicao_lista_id() As Integer
        Get
            Return mRequisicao_lista_id
        End Get
        Set(ByVal value As Integer)
            mRequisicao_lista_id = value
        End Set
    End Property

    Public Property Requisicao_id() As Integer
        Get
            Return mRequisicao_id
        End Get
        Set(ByVal value As Integer)
            mRequisicao_id = value
        End Set
    End Property

    Public Property Confirmado() As Integer
        Get
            Return mConfirmado
        End Get
        Set(ByVal value As Integer)
            mConfirmado = value
        End Set
    End Property

    Public Property Tipo_mov() As String
        Get
            Return mTipo_mov
        End Get
        Set(ByVal value As String)
            mTipo_mov = value
        End Set
    End Property

    Public Property Tipo_cobranca() As String
        Get
            Return mTipo_cobranca
        End Get
        Set(ByVal value As String)
            mTipo_cobranca = value
        End Set
    End Property

    Public Property Cobrador() As String
        Get
            Return mCobrador
        End Get
        Set(ByVal value As String)
            mCobrador = value
        End Set
    End Property

    Public Property Cod_opera() As String
        Get
            Return mCod_opera
        End Get
        Set(ByVal value As String)
            mCod_opera = value
        End Set
    End Property

    Public Property Data_mov() As String
        Get
            Return mData_mov
        End Get
        Set(ByVal value As String)
            mData_mov = value
        End Set
    End Property

    Public Property Cod_contribuinte() As String
        Get
            Return mCod_contribuinte
        End Get
        Set(ByVal value As String)
            mCod_contribuinte = value
        End Set
    End Property


    Public Property Nome_contribuinte() As String
        Get
            Return mNome_contribuinte
        End Get
        Set(ByVal value As String)
            mNome_contribuinte = value
        End Set
    End Property

    Public Property Tipo_contrib() As String
        Get
            Return mTipo_contrib
        End Get
        Set(ByVal value As String)
            mTipo_contrib = value
        End Set
    End Property

    Public Property Cod_recibo() As String
        Get
            Return mCod_recibo
        End Get
        Set(ByVal value As String)
            mCod_recibo = value
        End Set
    End Property

    Public Property Dia() As Integer
        Get
            Return mDia
        End Get
        Set(ByVal value As Integer)
            mDia = value
        End Set
    End Property

    Public Property Cod_cobrador() As Integer
        Get
            Return mCod_cobrador
        End Get
        Set(ByVal value As Integer)
            mCod_cobrador = value
        End Set
    End Property

    Public Property Situacao() As Integer
        Get
            Return mSituacao
        End Get
        Set(ByVal value As Integer)
            mSituacao = value
        End Set
    End Property

    Public Property Dt_nc() As String
        Get
            Return mDt_nc
        End Get
        Set(ByVal value As String)
            mDt_nc = value
        End Set
    End Property

    Public Property Data_venc() As String
        Get
            Return mData_venc
        End Get
        Set(ByVal value As String)
            mData_venc = value
        End Set
    End Property

    Public Property Cod_mov_contribuicao() As Long
        Get
            Return mCod_mov_contribuicao
        End Get
        Set(ByVal value As Long)
            mCod_mov_contribuicao = value
        End Set
    End Property

    Public Property Nao_Conforme() As Integer
        Get
            Return mNao_conforme
        End Get
        Set(ByVal value As Integer)
            mNao_conforme = value
        End Set
    End Property

    Public Property Data_impressao() As String
        Get
            Return mData_impressao
        End Get
        Set(ByVal value As String)
            mData_impressao = value
        End Set
    End Property

    Public Property Numero_Instalacao() As String
        Get
            Return mNumeroInstalacao
        End Get
        Set(ByVal value As String)
            mNumeroInstalacao = value
        End Set
    End Property

    Public Property TelefonePrincipal() As String
        Get
            Return mTelefonePrincipal
        End Get
        Set(ByVal value As String)
            mTelefonePrincipal = value
        End Set
    End Property



    Public Property Telefone3() As String
        Get
            Return mtelefone3
        End Get
        Set(ByVal value As String)
            mTelefone3 = value
        End Set
    End Property

    Public Property Telefone4() As String
        Get
            Return mTelefone4
        End Get
        Set(ByVal value As String)
            mTelefone4 = value
        End Set
    End Property

    Public Property Telefone5() As String
        Get
            Return mTelefone5
        End Get
        Set(ByVal value As String)
            mTelefone5 = value
        End Set
    End Property

    Public Property Telefone6() As String
        Get
            Return mTelefone6
        End Get
        Set(ByVal value As String)
            mTelefone6 = value
        End Set
    End Property

    Public Property Telefone7() As String
        Get
            Return mTelefone7
        End Get
        Set(ByVal value As String)
            mTelefone7 = value
        End Set
    End Property

#End Region

End Class
