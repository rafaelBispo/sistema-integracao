﻿Public Class CListaTelefonica



#Region "Campos"



    'LISTA TELEFONICA
    Public mCod_cliente As String
    Public mDDD As String
    Public mTelefone As String
    Public mValor As Decimal
    Public mNome_cliente As String
    Public mCategoria As Integer
    Private mSituacao As String
    Public mMesSituacao As String
    Public mUltimaSituacao As String
    Public mMotivoSituacao As String
    Public mDataSituacao As String
    Public mcod_cidade As Integer
    Public mStDescReferencia As String
    Public mLista_telefonica_id As Integer
#End Region


#Region "Propriedades"



    Public Property Cod_cliente() As String
        Get
            Return mCod_cliente
        End Get
        Set(ByVal value As String)
            mCod_cliente = value
        End Set
    End Property

    Public Property DDD() As String
        Get
            Return mDDD
        End Get
        Set(ByVal value As String)
            mDDD = value
        End Set
    End Property

    Public Property Telefone() As String
        Get
            Return mTelefone
        End Get
        Set(ByVal value As String)
            mTelefone = value
        End Set
    End Property

    Public Property Nome_cliente() As String
        Get
            Return mNome_cliente
        End Get
        Set(ByVal value As String)
            mNome_cliente = value
        End Set
    End Property

    Public Property Valor() As Decimal
        Get
            Return mValor
        End Get
        Set(ByVal value As Decimal)
            mValor = value
        End Set
    End Property

    Public Property Categoria() As Integer
        Get
            Return mCategoria
        End Get
        Set(ByVal value As Integer)
            mCategoria = value
        End Set
    End Property

    Public Property Situacao() As String
        Get
            Return mSituacao
        End Get
        Set(ByVal value As String)
            mSituacao = value
        End Set
    End Property


    Public Property MesSituacao() As String
        Get
            Return mMesSituacao
        End Get
        Set(ByVal value As String)
            mMesSituacao = value
        End Set
    End Property

    Public Property UltimaSituacao() As String
        Get
            Return mUltimaSituacao
        End Get
        Set(ByVal value As String)
            mUltimaSituacao = value
        End Set
    End Property

    Public Property StDescReferencia() As String
        Get
            Return mStdescReferencia
        End Get
        Set(ByVal value As String)
            mStdescReferencia = value
        End Set
    End Property



    Public Property MotivoSituacao() As String
        Get
            Return mMotivoSituacao
        End Get
        Set(ByVal value As String)
            mMotivoSituacao = value
        End Set
    End Property

    Public Property DataSituacao() As String
        Get
            Return mDataSituacao
        End Get
        Set(ByVal value As String)
            mDataSituacao = value
        End Set
    End Property

    Public Property Cod_cidade() As Integer
        Get
            Return mcod_cidade
        End Get
        Set(ByVal value As Integer)
            mcod_cidade = value
        End Set
    End Property

    Public Property Lista_telefonica_id() As Integer
        Get
            Return mLista_telefonica_id
        End Get
        Set(ByVal value As Integer)
            mLista_telefonica_id = value
        End Set
    End Property




#End Region

End Class
