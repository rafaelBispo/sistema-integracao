﻿Public Class CRetorno

#Region "Campos"

    Private mProtocolo As String
    Private mRemessa As Integer
    Private mAnoMesRemessa As String
    Private mDdDataGeracao As String
    Private mUsTipoRegistroH As String
    Private mEmpresaTerceiro As String
    Private mEmpresaTelemar As String
    Private mUsTipoRegistroD As String
    Private mUiCodServicoArbor As String
    Private mUiCodLocalidadeCobranca As String
    Private mStDDDMeioAcessoCobranca As String
    Private mStNumMeioAcessoCobranca As String
    Private mDVlrServico As String
    Private mDdDataServico As String
    Private mUsNumParcela As String
    Private mUsQteParcela As String
    Private mUiDataInicioCobranca As String
    Private mUcSinal As String
    Private mUiAnoMesConta As String
    Private mStDescReferencia As String
    Private mDNumNotaFiscal As String
    Private mDdDataVencimento As String
    Private mDdDataSituacao As String
    Private mDVlrFaturado As String
    Private mStCodigoMovimentacao As String
    Private mUsSitFaturamento As String
    Private mUiMotivoSituacao As String
    Private mUiAnoMesContaMulta As String
    Private mStNumAssinanteMovel As String
    Private mSistemaFaturamento As String
    Private mCiclo As String
    Private mUsTipoRegistroT As String
    Private mQuantidadeDeRegistro As Long
    Private mValorArquivo As String
    Private mCod_ean_cliente As String
    Private mCod_cliente As String
    Private mNome_cliente As String
    Private mUltimoAceite As String


    Private mNome_Arquivo As String
    Private mId_arquivo As Integer
#End Region

#Region "Propriedades"


    Public Property Protocolo() As String
        Get
            Return mProtocolo
        End Get
        Set(ByVal value As String)
            mProtocolo = value
        End Set
    End Property

    Public Property Remessa() As String
        Get
            Return mRemessa
        End Get
        Set(ByVal value As String)
            mRemessa = value
        End Set
    End Property

    Public Property AnoMesRemessa() As String
        Get
            Return mAnoMesRemessa
        End Get
        Set(ByVal value As String)
            mAnoMesRemessa = value
        End Set
    End Property

    Public Property DdDataGeracao() As String
        Get
            Return mDdDataGeracao
        End Get
        Set(ByVal value As String)
            mDdDataGeracao = value
        End Set
    End Property


    Public Property UsTipoRegistroH() As String
        Get
            Return mUsTipoRegistroH
        End Get
        Set(ByVal value As String)
            mUsTipoRegistroH = value
        End Set
    End Property


    Public Property EmpresaTerceiro() As String
        Get
            Return mEmpresaTerceiro
        End Get
        Set(ByVal value As String)
            mEmpresaTerceiro = value
        End Set
    End Property

    Public Property UltimoAceite() As String
        Get
            Return mUltimoAceite
        End Get
        Set(ByVal value As String)
            mUltimoAceite = value
        End Set
    End Property

    Public Property EmpresaTelemar() As String
        Get
            Return mEmpresaTelemar
        End Get
        Set(ByVal value As String)
            mEmpresaTelemar = value
        End Set
    End Property


    Public Property UsTipoRegistroD() As String
        Get
            Return mUsTipoRegistroD
        End Get
        Set(ByVal value As String)
            mUsTipoRegistroD = value
        End Set
    End Property

    Public Property UiCodServicoArbor() As String
        Get
            Return mUiCodServicoArbor
        End Get
        Set(ByVal value As String)
            mUiCodServicoArbor = value
        End Set
    End Property

    Public Property UiCodLocalidadeCobranca() As String
        Get
            Return mUiCodLocalidadeCobranca
        End Get
        Set(ByVal value As String)
            mUiCodLocalidadeCobranca = value
        End Set
    End Property

    Public Property StDDDMeioAcessoCobranca() As String
        Get
            Return mStDDDMeioAcessoCobranca
        End Get
        Set(ByVal value As String)
            mStDDDMeioAcessoCobranca = value
        End Set
    End Property

    Public Property StNumMeioAcessoCobranca() As String
        Get
            Return mStNumMeioAcessoCobranca
        End Get
        Set(ByVal value As String)
            mStNumMeioAcessoCobranca = value
        End Set
    End Property

    Public Property DVlrServico() As String
        Get
            Return mDVlrServico
        End Get
        Set(ByVal value As String)
            mDVlrServico = value
        End Set
    End Property

    Public Property DdDataServico() As String
        Get
            Return mDdDataServico
        End Get
        Set(ByVal value As String)
            mDdDataServico = value
        End Set
    End Property

    Public Property UsNumParcela() As String
        Get
            Return mUsNumParcela
        End Get
        Set(ByVal value As String)
            mUsNumParcela = value
        End Set
    End Property

    Public Property UsQteParcela() As String
        Get
            Return mUsQteParcela
        End Get
        Set(ByVal value As String)
            mUsQteParcela = value
        End Set
    End Property

    Public Property UiDataInicioCobranca() As String
        Get
            Return mUiDataInicioCobranca
        End Get
        Set(ByVal value As String)
            mUiDataInicioCobranca = value
        End Set
    End Property

    Public Property UcSinal() As String
        Get
            Return mUcSinal
        End Get
        Set(ByVal value As String)
            mUcSinal = value
        End Set
    End Property

    Public Property UiAnoMesConta() As String
        Get
            Return mUiAnoMesConta
        End Get
        Set(ByVal value As String)
            mUiAnoMesConta = value
        End Set
    End Property

    Public Property StDescReferencia() As String
        Get
            Return mStDescReferencia
        End Get
        Set(ByVal value As String)
            mStDescReferencia = value
        End Set
    End Property

    Public Property DNumNotaFiscal() As String
        Get
            Return mDNumNotaFiscal
        End Get
        Set(ByVal value As String)
            mDNumNotaFiscal = value
        End Set
    End Property

    Public Property DdDataVencimento() As String
        Get
            Return mDdDataVencimento
        End Get
        Set(ByVal value As String)
            mDdDataVencimento = value
        End Set
    End Property

    Public Property DdDataSituacao() As String
        Get
            Return mDdDataSituacao
        End Get
        Set(ByVal value As String)
            mDdDataSituacao = value
        End Set
    End Property

    Public Property DVlrFaturado() As String
        Get
            Return mDVlrFaturado
        End Get
        Set(ByVal value As String)
            mDVlrFaturado = value
        End Set
    End Property

    Public Property StCodigoMovimentacao() As String
        Get
            Return mStCodigoMovimentacao
        End Get
        Set(ByVal value As String)
            mStCodigoMovimentacao = value
        End Set
    End Property

    Public Property UsSitFaturamento() As String
        Get
            Return mUsSitFaturamento
        End Get
        Set(ByVal value As String)
            mUsSitFaturamento = value
        End Set
    End Property

    Public Property UiMotivoSituacao() As String
        Get
            Return mUiMotivoSituacao
        End Get
        Set(ByVal value As String)
            mUiMotivoSituacao = value
        End Set
    End Property

    Public Property UiAnoMesContaMulta() As String
        Get
            Return mUiAnoMesContaMulta
        End Get
        Set(ByVal value As String)
            mUiAnoMesContaMulta = value
        End Set
    End Property

    Public Property StNumAssinanteMovel() As String
        Get
            Return mStNumAssinanteMovel
        End Get
        Set(ByVal value As String)
            mStNumAssinanteMovel = value
        End Set
    End Property

    Public Property SistemaFaturamento() As String
        Get
            Return mSistemaFaturamento
        End Get
        Set(ByVal value As String)
            mSistemaFaturamento = value
        End Set
    End Property

    Public Property Ciclo() As String
        Get
            Return mCiclo
        End Get
        Set(ByVal value As String)
            mCiclo = value
        End Set
    End Property

    Public Property UsTipoRegistroT() As String
        Get
            Return mUsTipoRegistroT
        End Get
        Set(ByVal value As String)
            mUsTipoRegistroT = value
        End Set
    End Property

    Public Property QuantidadeDeRegistro() As Long
        Get
            Return mQuantidadeDeRegistro
        End Get
        Set(ByVal value As Long)
            mQuantidadeDeRegistro = value
        End Set
    End Property

    Public Property ValorArquivo() As String
        Get
            Return mValorArquivo
        End Get
        Set(ByVal value As String)
            mValorArquivo = value
        End Set
    End Property


    Public Property Cod_ean_cliente() As String
        Get
            Return mCod_ean_cliente
        End Get
        Set(ByVal value As String)
            mCod_ean_cliente = value
        End Set
    End Property

    Public Property Cod_cliente() As String
        Get
            Return mCod_cliente
        End Get
        Set(ByVal value As String)
            mCod_cliente = value
        End Set
    End Property

    Public Property Nome_cliente() As String
        Get
            Return mNome_cliente
        End Get
        Set(ByVal value As String)
            mNome_cliente = value
        End Set
    End Property

    Public Property Nome_Arquivo() As String
        Get
            Return mNome_Arquivo
        End Get
        Set(ByVal value As String)
            mNome_Arquivo = value
        End Set
    End Property


    Public Property Id_arquivo() As String
        Get
            Return mId_arquivo
        End Get
        Set(ByVal value As String)
            mId_arquivo = value
        End Set
    End Property

#End Region
End Class
