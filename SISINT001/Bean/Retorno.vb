﻿Public Class Retorno

#Region "Campos"

    Private mUsTipoRegistroD As String
    Private mUiCodServicoArbor As String
    Private mUiCodLocalidadeCobranca As String
    Private mStDDDMeioAcessoCobranca As String
    Private mStNumMeioAcessoCobranca As String
    Private mDVlrServico As String
    Private mDdDataServico As String
    Private mUsNumParcela As String
    Private mUsQteParcela As String
    Private mUiDataInicioCobranca As String
    Private mUcSinal As String
    Private mUiAnoMesConta As String
    Private mStDescReferencia As String
    Private mDNumNotaFiscal As String
    Private mDdDataVencimento As String
    Private mDdDataSituacao As String
    Private mDVlrFaturado As String
    Private mStCodigoMovimentacao As String
    Private mUsSitFaturamento As String
    Private mUiMotivoSituacao As String
    Private mUiAnoMesContaMulta As String
    Private mStNumAssinanteMovel As String
    Private mSistemaFaturamento As String
    Private mCiclo As String

#End Region

#Region "Propriedades"

    Public Property UsTipoRegistroD() As String
        Get
            Return mUsTipoRegistroD
        End Get
        Set(ByVal value As String)
            mUsTipoRegistroD = value
        End Set
    End Property

    Public Property UiCodServicoArbor() As String
        Get
            Return mUiCodServicoArbor
        End Get
        Set(ByVal value As String)
            mUiCodServicoArbor = value
        End Set
    End Property

    Public Property UiCodLocalidadeCobranca() As String
        Get
            Return mUiCodLocalidadeCobranca
        End Get
        Set(ByVal value As String)
            mUiCodLocalidadeCobranca = value
        End Set
    End Property

    Public Property StDDDMeioAcessoCobranca() As String
        Get
            Return mStDDDMeioAcessoCobranca
        End Get
        Set(ByVal value As String)
            mStDDDMeioAcessoCobranca = value
        End Set
    End Property

    Public Property StNumMeioAcessoCobranca() As String
        Get
            Return mStNumMeioAcessoCobranca
        End Get
        Set(ByVal value As String)
            mStNumMeioAcessoCobranca = value
        End Set
    End Property

    Public Property DVlrServico() As String
        Get
            Return mDVlrServico
        End Get
        Set(ByVal value As String)
            mDVlrServico = value
        End Set
    End Property

    Public Property DdDataServico() As String
        Get
            Return mDdDataServico
        End Get
        Set(ByVal value As String)
            mDdDataServico = value
        End Set
    End Property

    Public Property UsNumParcela() As String
        Get
            Return mUsNumParcela
        End Get
        Set(ByVal value As String)
            mUsNumParcela = value
        End Set
    End Property

    Public Property UsQteParcela() As String
        Get
            Return mUsQteParcela
        End Get
        Set(ByVal value As String)
            mUsQteParcela = value
        End Set
    End Property

    Public Property UiDataInicioCobranca() As String
        Get
            Return mUiDataInicioCobranca
        End Get
        Set(ByVal value As String)
            mUiDataInicioCobranca = value
        End Set
    End Property

    Public Property UcSinal() As String
        Get
            Return mUcSinal
        End Get
        Set(ByVal value As String)
            mUcSinal = value
        End Set
    End Property

    Public Property UiAnoMesConta() As String
        Get
            Return mUiAnoMesConta
        End Get
        Set(ByVal value As String)
            mUiAnoMesConta = value
        End Set
    End Property

    Public Property StDescReferencia() As String
        Get
            Return mStDescReferencia
        End Get
        Set(ByVal value As String)
            mStDescReferencia = value
        End Set
    End Property

    Public Property DNumNotaFiscal() As String
        Get
            Return mDNumNotaFiscal
        End Get
        Set(ByVal value As String)
            mDNumNotaFiscal = value
        End Set
    End Property

    Public Property DdDataVencimento() As String
        Get
            Return mDdDataVencimento
        End Get
        Set(ByVal value As String)
            mDdDataVencimento = value
        End Set
    End Property

    Public Property DdDataSituacao() As String
        Get
            Return mDdDataSituacao
        End Get
        Set(ByVal value As String)
            mDdDataSituacao = value
        End Set
    End Property

    Public Property DVlrFaturado() As String
        Get
            Return mDVlrFaturado
        End Get
        Set(ByVal value As String)
            mDVlrFaturado = value
        End Set
    End Property

    Public Property StCodigoMovimentacao() As String
        Get
            Return mStCodigoMovimentacao
        End Get
        Set(ByVal value As String)
            mStCodigoMovimentacao = value
        End Set
    End Property

    Public Property UsSitFaturamento() As String
        Get
            Return mUsSitFaturamento
        End Get
        Set(ByVal value As String)
            mUsSitFaturamento = value
        End Set
    End Property

    Public Property UiMotivoSituacao() As String
        Get
            Return mUiMotivoSituacao
        End Get
        Set(ByVal value As String)
            mUiMotivoSituacao = value
        End Set
    End Property

    Public Property UiAnoMesContaMulta() As String
        Get
            Return mUiAnoMesContaMulta
        End Get
        Set(ByVal value As String)
            mUiAnoMesContaMulta = value
        End Set
    End Property

    Public Property StNumAssinanteMovel() As String
        Get
            Return mStNumAssinanteMovel
        End Get
        Set(ByVal value As String)
            mStNumAssinanteMovel = value
        End Set
    End Property

    Public Property SistemaFaturamento() As String
        Get
            Return mSistemaFaturamento
        End Get
        Set(ByVal value As String)
            mSistemaFaturamento = value
        End Set
    End Property

    Public Property Ciclo() As String
        Get
            Return mCiclo
        End Get
        Set(ByVal value As String)
            mCiclo = value
        End Set
    End Property

#End Region
End Class
