﻿Public Class CGenerico

#Region "Campos"

    'CIDADE
    Private mCod_Cidade As Integer
    Private mNome_Cidade As String
    Private mUf_Cidade As String

    'BAIRRO
    Private mCod_Bairro As Integer
    Private mNome_Bairro As String
    Private mCep_Bairro As String
    Private mCod_Cidade_Bairro As Integer
    Private mCod_Regiao_Bairro As Integer

    'CATEGORIA
    Private mCod_Categoria As Integer
    Private mNome_Categoria As String

    'OPERADOR
    Private mCod_Operador As Integer
    Private mNome_Operador As String

    'ESPECIE
    Private mCod_Especie As Integer
    Private mNome_Especie As String

    'GRUPO
    Private mCod_Grupo As Integer
    Private mNome_Grupo As String

    'SITUACAO
    Private mSituacao_Faturamento As Integer
    Private mSituacao As String

    'USUARIO
    Private mUsuario As String
    Private mSenha As String
    Private mAcesso As Integer

    'LISTA TELEFONICA
    Public mCod_cliente As String
    Public mDDD As String
    Public mTelefone As String
    Public mValor As Decimal
    Public mNome_cliente As String
    Public mCategoria As Integer
    Public mMesSituacao As String
    Public mUltimaSituacao As String
    Public mMotivoSituacao As String
    Public mDataSituacao As String

    'RESUMO REQUISICAO
    Public mOperador As String
    Public mOperador_id As Integer
    Public mQuantidadeConfirmado As String
    Public mQuantidadeRecusado As String
    Public mValorConfirmado As String
    Public mValorRecusado As String
    Public mCod_ean As String
    Public mAceite As String
    Public mHistAceite As String
    Public mMesRefSeguinte As String
    Public mDt_inclusao As String
    Public mCpf As String
    Public mAutorizante As String
    Public mObservacao As String
    Public mRequisicaoId As Long

    'FATURAMENTO
    Public mCod_mov_contribuicao_descr As Integer
    Public mCod_mov_contribuicao As Integer
    Public mTipo_mov As String
    Public mTipo_cobranca As String
    Public mCobrador As String
    Public mCod_opera As Integer
    Public mData_mov As String
    Public mData_venc As String
    Public mCod_ean_cliente As String
    Public mCod_contribuinte As Integer
    Public mNome_contribuinte As String
    Public mTipo_contrib As String
    Public mConfirmado As Integer
    Public mCod_recibo As String
    Public mDia As Integer
    Public mCidade As String
    Public mCod_cobrador As Integer
    Public mCod_empresa As Integer
    Public mCod_usuario As String
    Public mCod_regiao As String
    Public mValor_total As Decimal
    Public mQuantidade_total As Integer

    'CARGO
    Public mCod_Cargo As Integer
    Public mNome_Cargo As String

    'FUNCIONARIO
    Public mCod_Funcionario As Integer
    Public mCod_Ean_Funcionario As Integer
    Public mNome_Funcionario As String
    Public mSexo As String
    Public mEndereco As String
    Public mBairro As String
    Public mCep As String
    Public mCelular As String
    Public mEmail As String
    Public mRg As String
    Public mData_Nascimento As String
    Public mData_admissao As String
    Public mData_demissao As String
    Public mComissao As Decimal
    Public mSalario As Decimal
    Public mMeta_1 As Decimal
    Public mQuant_1 As Decimal
    Public mComissao_1 As Decimal
    Public mMeta_2 As Decimal
    Public mQuant_2 As Decimal
    Public mComissao_2 As Decimal
    Public mMeta_3 As Decimal
    Public mQuant_3 As Decimal
    Public mComissao_3 As Decimal

    'ID
    Public mListaId As Integer





#End Region


#Region "Propriedades"

    Public Property Cod_Cidade() As Integer
        Get
            Return mCod_Cidade
        End Get
        Set(ByVal value As Integer)
            mCod_Cidade = value
        End Set
    End Property

    Public Property Nome_Cidade() As String
        Get
            Return mNome_Cidade
        End Get
        Set(ByVal value As String)
            mNome_Cidade = value
        End Set
    End Property

    Public Property Uf_Cidade() As String
        Get
            Return mUf_Cidade
        End Get
        Set(ByVal value As String)
            mUf_Cidade = value
        End Set
    End Property

    Public Property Cod_Bairro() As Integer
        Get
            Return mCod_Bairro
        End Get
        Set(ByVal value As Integer)
            mCod_Bairro = value
        End Set
    End Property

    Public Property Nome_Bairro() As String
        Get
            Return mNome_Bairro
        End Get
        Set(ByVal value As String)
            mNome_Bairro = value
        End Set
    End Property

    Public Property Cod_Cidade_Bairro() As Integer
        Get
            Return mCod_Cidade_Bairro
        End Get
        Set(ByVal value As Integer)
            mCod_Cidade_Bairro = value
        End Set
    End Property

    Public Property Cod_Regiao_Bairro() As Integer
        Get
            Return mCod_Regiao_Bairro
        End Get
        Set(ByVal value As Integer)
            mCod_Regiao_Bairro = value
        End Set
    End Property

    Public Property Cod_Categoria() As Integer
        Get
            Return mCod_Categoria
        End Get
        Set(ByVal value As Integer)
            mCod_Categoria = value
        End Set
    End Property

    Public Property Nome_Categoria() As String
        Get
            Return mNome_Categoria
        End Get
        Set(ByVal value As String)
            mNome_Categoria = value
        End Set
    End Property

    Public Property Cod_Operador() As Integer
        Get
            Return mCod_Operador
        End Get
        Set(ByVal value As Integer)
            mCod_Operador = value
        End Set
    End Property

    Public Property Nome_Operador() As String
        Get
            Return mNome_Operador
        End Get
        Set(ByVal value As String)
            mNome_Operador = value
        End Set
    End Property


    Public Property Cod_Especie() As Integer
        Get
            Return mCod_Especie
        End Get
        Set(ByVal value As Integer)
            mCod_Especie = value
        End Set
    End Property

    Public Property Nome_Especie() As String
        Get
            Return mNome_Especie
        End Get
        Set(ByVal value As String)
            mNome_Especie = value
        End Set
    End Property

    Public Property Cod_Grupo() As Integer
        Get
            Return mCod_Grupo
        End Get
        Set(ByVal value As Integer)
            mCod_Grupo = value
        End Set
    End Property

    Public Property Nome_Grupo() As String
        Get
            Return mNome_Grupo
        End Get
        Set(ByVal value As String)
            mNome_Grupo = value
        End Set
    End Property

    Public Property Situacao_Faturamento() As Integer
        Get
            Return mSituacao_Faturamento
        End Get
        Set(ByVal value As Integer)
            mSituacao_Faturamento = value
        End Set
    End Property

    Public Property Situacao() As String
        Get
            Return mSituacao
        End Get
        Set(ByVal value As String)
            mSituacao = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return mUsuario
        End Get
        Set(ByVal value As String)
            mUsuario = value
        End Set
    End Property

    Public Property Senha() As String
        Get
            Return mSenha
        End Get
        Set(ByVal value As String)
            mSenha = value
        End Set
    End Property

    Public Property Acesso() As Integer
        Get
            Return mAcesso
        End Get
        Set(ByVal value As Integer)
            mAcesso = value
        End Set
    End Property


    Public Property Cod_cliente() As String
        Get
            Return mCod_cliente
        End Get
        Set(ByVal value As String)
            mCod_cliente = value
        End Set
    End Property

    Public Property DDD() As String
        Get
            Return mDDD
        End Get
        Set(ByVal value As String)
            mDDD = value
        End Set
    End Property

    Public Property Telefone() As String
        Get
            Return mTelefone
        End Get
        Set(ByVal value As String)
            mTelefone = value
        End Set
    End Property

    Public Property Nome_cliente() As String
        Get
            Return mNome_cliente
        End Get
        Set(ByVal value As String)
            mNome_cliente = value
        End Set
    End Property

    Public Property Valor() As Decimal
        Get
            Return mValor
        End Get
        Set(ByVal value As Decimal)
            mValor = value
        End Set
    End Property

    Public Property Categoria() As Integer
        Get
            Return mCategoria
        End Get
        Set(ByVal value As Integer)
            mCategoria = value
        End Set
    End Property


    Public Property MesSituacao() As String
        Get
            Return mMesSituacao
        End Get
        Set(ByVal value As String)
            mMesSituacao = value
        End Set
    End Property

    Public Property UltimaSituacao() As String
        Get
            Return mUltimaSituacao
        End Get
        Set(ByVal value As String)
            mUltimaSituacao = value
        End Set
    End Property

    Public Property MotivoSituacao() As String
        Get
            Return mMotivoSituacao
        End Get
        Set(ByVal value As String)
            mMotivoSituacao = value
        End Set
    End Property

    Public Property DataSituacao() As String
        Get
            Return mDataSituacao
        End Get
        Set(ByVal value As String)
            mDataSituacao = value
        End Set
    End Property

    Public Property Operador() As String
        Get
            Return mOperador
        End Get
        Set(ByVal value As String)
            mOperador = value
        End Set
    End Property

    Public Property Operador_id() As Integer
        Get
            Return mOperador_id
        End Get
        Set(ByVal value As Integer)
            mOperador_id = value
        End Set
    End Property

    Public Property QuantidadeConfirmado() As String
        Get
            Return mQuantidadeConfirmado
        End Get
        Set(ByVal value As String)
            mQuantidadeConfirmado = value
        End Set
    End Property

    Public Property QuantidadeRecusado() As String
        Get
            Return mQuantidadeRecusado
        End Get
        Set(ByVal value As String)
            mQuantidadeRecusado = value
        End Set
    End Property

    Public Property ValorConfirmado() As String
        Get
            Return mValorConfirmado
        End Get
        Set(ByVal value As String)
            mValorConfirmado = value
        End Set
    End Property

    Public Property ValorRecusado() As String
        Get
            Return mValorRecusado
        End Get
        Set(ByVal value As String)
            mValorRecusado = value
        End Set
    End Property

    Public Property Cod_ean() As String
        Get
            Return mcod_ean
        End Get
        Set(ByVal value As String)
            mcod_ean = value
        End Set
    End Property

    Public Property Aceite() As String
        Get
            Return mAceite
        End Get
        Set(ByVal value As String)
            mAceite = value
        End Set
    End Property

    Public Property Hist_Aceite() As String
        Get
            Return mHistAceite
        End Get
        Set(ByVal value As String)
            mHistAceite = value
        End Set
    End Property

    Public Property MesRefSeguinte() As String
        Get
            Return mMesRefSeguinte
        End Get
        Set(ByVal value As String)
            mMesRefSeguinte = value
        End Set
    End Property

    Public Property Dt_inclusao() As String
        Get
            Return mDt_inclusao
        End Get
        Set(ByVal value As String)
            mDt_inclusao = value
        End Set
    End Property

    Public Property Cpf() As String
        Get
            Return mCpf
        End Get
        Set(ByVal value As String)
            mCpf = value
        End Set
    End Property

    Public Property Autorizante() As String
        Get
            Return mAutorizante
        End Get
        Set(ByVal value As String)
            mAutorizante = value
        End Set
    End Property

    Public Property Observacao() As String
        Get
            Return mObservacao
        End Get
        Set(ByVal value As String)
            mObservacao = value
        End Set
    End Property

    Public Property Requisicao_Id() As Long
        Get
            Return mRequisicaoId
        End Get
        Set(ByVal value As Long)
            mRequisicaoId = value
        End Set
    End Property


    Public Property Cod_mov_contribuicao_descr() As Integer
        Get
            Return mCod_mov_contribuicao_descr
        End Get
        Set(ByVal value As Integer)
            mCod_mov_contribuicao_descr = value
        End Set
    End Property

    Public Property Cod_mov_contribuicao() As Integer
        Get
            Return mCod_mov_contribuicao
        End Get
        Set(ByVal value As Integer)
            mCod_mov_contribuicao = value
        End Set
    End Property

    Public Property Tipo_mov() As String
        Get
            Return mTipo_mov
        End Get
        Set(ByVal value As String)
            mTipo_mov = value
        End Set
    End Property

    Public Property Tipo_cobranca() As String
        Get
            Return mTipo_cobranca
        End Get
        Set(ByVal value As String)
            mTipo_cobranca = value
        End Set
    End Property

    Public Property Cobrador() As String
        Get
            Return mCobrador
        End Get
        Set(ByVal value As String)
            mCobrador = value
        End Set
    End Property

    Public Property Cod_opera() As Integer
        Get
            Return mCod_opera
        End Get
        Set(ByVal value As Integer)
            mCod_opera = value
        End Set
    End Property

    Public Property Data_mov() As String
        Get
            Return mData_mov
        End Get
        Set(ByVal value As String)
            mData_mov = value
        End Set
    End Property

    Public Property Data_venc() As String
        Get
            Return mData_venc
        End Get
        Set(ByVal value As String)
            mData_venc = value
        End Set
    End Property

    Public Property Cod_ean_cliente() As String
        Get
            Return mCod_ean_cliente
        End Get
        Set(ByVal value As String)
            mCod_ean_cliente = value
        End Set
    End Property

    Public Property Cod_contribuinte() As Integer
        Get
            Return mCod_contribuinte
        End Get
        Set(ByVal value As Integer)
            mCod_contribuinte = value
        End Set
    End Property

    Public Property Nome_contribuinte() As String
        Get
            Return mNome_contribuinte
        End Get
        Set(ByVal value As String)
            mNome_contribuinte = value
        End Set
    End Property

    Public Property Tipo_contrib() As String
        Get
            Return mTipo_contrib
        End Get
        Set(ByVal value As String)
            mTipo_contrib = value
        End Set
    End Property

    Public Property Confirmado() As Integer
        Get
            Return mConfirmado
        End Get
        Set(ByVal value As Integer)
            mConfirmado = value
        End Set
    End Property

    Public Property Cod_recibo() As String
        Get
            Return mCod_recibo
        End Get
        Set(ByVal value As String)
            mCod_recibo = value
        End Set
    End Property

    Public Property Dia() As Integer
        Get
            Return mDia
        End Get
        Set(ByVal value As Integer)
            mDia = value
        End Set
    End Property

    Public Property Cidade() As String
        Get
            Return mCidade
        End Get
        Set(ByVal value As String)
            mCidade = value
        End Set
    End Property

    Public Property Cod_cobrador() As Integer
        Get
            Return mCod_cobrador
        End Get
        Set(ByVal value As Integer)
            mCod_cobrador = value
        End Set
    End Property


    Public Property Cod_empresa() As Integer
        Get
            Return mCod_empresa
        End Get
        Set(ByVal value As Integer)
            mCod_empresa = value
        End Set
    End Property

    Public Property Cod_usuario() As String
        Get
            Return mCod_usuario
        End Get
        Set(ByVal value As String)
            mCod_usuario = value
        End Set
    End Property

    Public Property Cod_regiao() As String
        Get
            Return mCod_regiao
        End Get
        Set(ByVal value As String)
            mCod_regiao = value
        End Set
    End Property

    Public Property Valor_fichas() As Decimal
        Get
            Return mValor_total
        End Get
        Set(ByVal value As Decimal)
            mValor_total = value
        End Set
    End Property

    Public Property Total_fichas() As Integer
        Get
            Return mQuantidade_total
        End Get
        Set(ByVal value As Integer)
            mQuantidade_total = value
        End Set
    End Property

    Public Property Cod_Cargo() As Integer
        Get
            Return mCod_Cargo
        End Get
        Set(ByVal value As Integer)
            mCod_Cargo = value
        End Set
    End Property

    Public Property Nome_Cargo() As String
        Get
            Return mNome_Cargo
        End Get
        Set(ByVal value As String)
            mNome_Cargo = value
        End Set
    End Property

    Public Property Cod_Funcionario() As Integer
        Get
            Return mCod_Funcionario
        End Get
        Set(ByVal value As Integer)
            mCod_Funcionario = value
        End Set
    End Property

    Public Property Cod_Ean_Funcionario() As Integer
        Get
            Return mCod_Ean_Funcionario
        End Get
        Set(ByVal value As Integer)
            mCod_Ean_Funcionario = value
        End Set
    End Property

    Public Property Nome_Funcionario() As String
        Get
            Return mNome_Funcionario
        End Get
        Set(ByVal value As String)
            mNome_Funcionario = value
        End Set
    End Property

    Public Property Sexo() As String
        Get
            Return mSexo
        End Get
        Set(ByVal value As String)
            mSexo = value
        End Set
    End Property

    Public Property Endereco() As String
        Get
            Return mEndereco
        End Get
        Set(ByVal value As String)
            mEndereco = value
        End Set
    End Property

    Public Property Bairro() As String
        Get
            Return mBairro
        End Get
        Set(ByVal value As String)
            mBairro = value
        End Set
    End Property

    Public Property Cep() As String
        Get
            Return mCep
        End Get
        Set(ByVal value As String)
            mCep = value
        End Set
    End Property

    Public Property Celular() As String
        Get
            Return mCelular
        End Get
        Set(ByVal value As String)
            mCelular = value
        End Set
    End Property

    Public Property Email() As String
        Get
            Return mEmail
        End Get
        Set(ByVal value As String)
            mEmail = value
        End Set
    End Property

    Public Property RG() As String
        Get
            Return mRg
        End Get
        Set(ByVal value As String)
            mRg = value
        End Set
    End Property

    Public Property Data_Nascimento() As String
        Get
            Return mData_Nascimento
        End Get
        Set(ByVal value As String)
            mData_Nascimento = value
        End Set
    End Property

    Public Property Data_admissao() As String
        Get
            Return mData_admissao
        End Get
        Set(ByVal value As String)
            mData_admissao = value
        End Set
    End Property

    Public Property Data_demissao() As String
        Get
            Return mData_demissao
        End Get
        Set(ByVal value As String)
            mData_demissao = value
        End Set
    End Property

    Public Property Comissao() As Decimal
        Get
            Return mComissao
        End Get
        Set(ByVal value As Decimal)
            mComissao = value
        End Set
    End Property

    Public Property Salario() As Decimal
        Get
            Return mSalario
        End Get
        Set(ByVal value As Decimal)
            mSalario = value
        End Set
    End Property

    Public Property Meta_1() As Decimal
        Get
            Return mMeta_1
        End Get
        Set(ByVal value As Decimal)
            mMeta_1 = value
        End Set
    End Property

    Public Property Quant_1() As Decimal
        Get
            Return mQuant_1
        End Get
        Set(ByVal value As Decimal)
            mQuant_1 = value
        End Set
    End Property

    Public Property Comissao_1() As Decimal
        Get
            Return mComissao_1
        End Get
        Set(ByVal value As Decimal)
            mComissao_1 = value
        End Set
    End Property

    Public Property Meta_2() As Decimal
        Get
            Return mMeta_2
        End Get
        Set(ByVal value As Decimal)
            mMeta_2 = value
        End Set
    End Property

    Public Property Quant_2() As Decimal
        Get
            Return mQuant_2
        End Get
        Set(ByVal value As Decimal)
            mQuant_2 = value
        End Set
    End Property

    Public Property Comissao_2() As Decimal
        Get
            Return mComissao_2
        End Get
        Set(ByVal value As Decimal)
            mComissao_2 = value
        End Set
    End Property

    Public Property Meta_3() As Decimal
        Get
            Return mMeta_3
        End Get
        Set(ByVal value As Decimal)
            mMeta_3 = value
        End Set
    End Property

    Public Property Quant_3() As Decimal
        Get
            Return mQuant_2
        End Get
        Set(ByVal value As Decimal)
            mQuant_2 = value
        End Set
    End Property

    Public Property Comissao_3() As Decimal
        Get
            Return mComissao_2
        End Get
        Set(ByVal value As Decimal)
            mComissao_2 = value
        End Set
    End Property

    Public Property Lista_telefonica_id() As Integer
        Get
            Return mListaId
        End Get
        Set(ByVal value As Integer)
            mListaId = value
        End Set
    End Property



#End Region

End Class
