﻿Public Class CRequisicao

#Region "Campos"




    Private mMov_contribuicao_id As Integer
    Private mLiberacao_id As String
    Private mBordero_id As String
    Private mBaixa_id As String
    Private mTipo_mov As String
    Private mTipo_cobranca As String
    Private mCobrador As String
    Private mData_mov As String
    Private mCod_empresa As Integer
    Private mCod_cobrador As Integer
    Private mConfirmado As Integer
    Private mValor_fichas As String
    Private mTotal_fichas As String





#End Region


#Region "Propriedades"



    Public Property Mov_contribuicao_id() As Integer
        Get
            Return mMov_contribuicao_id
        End Get
        Set(ByVal value As Integer)
            mMov_contribuicao_id = value
        End Set
    End Property

    Public Property Liberacao_id() As Integer
        Get
            Return mLiberacao_id
        End Get
        Set(ByVal value As Integer)
            mLiberacao_id = value
        End Set
    End Property

    Public Property Bordero_id() As Integer
        Get
            Return mBordero_id
        End Get
        Set(ByVal value As Integer)
            mBordero_id = value
        End Set
    End Property

    Public Property Baixa_id() As Integer
        Get
            Return mBaixa_id
        End Get
        Set(ByVal value As Integer)
            mBaixa_id = value
        End Set
    End Property

    Public Property Tipo_mov() As String
        Get
            Return mTipo_mov
        End Get
        Set(ByVal value As String)
            mTipo_mov = value
        End Set
    End Property

    Public Property Tipo_cobranca() As String
        Get
            Return mTipo_cobranca
        End Get
        Set(ByVal value As String)
            mTipo_cobranca = value
        End Set
    End Property

    Public Property Cobrador() As String
        Get
            Return mCobrador
        End Get
        Set(ByVal value As String)
            mCobrador = value
        End Set
    End Property

    Public Property Data_mov() As String
        Get
            Return mData_mov
        End Get
        Set(ByVal value As String)
            mData_mov = value
        End Set
    End Property

    Public Property Cod_empresa() As Integer
        Get
            Return mCod_empresa
        End Get
        Set(ByVal value As Integer)
            mCod_empresa = value
        End Set
    End Property

    Public Property Cod_cobrador() As Integer
        Get
            Return mCod_cobrador
        End Get
        Set(ByVal value As Integer)
            mCod_cobrador = value
        End Set
    End Property

    Public Property Confirmado() As Integer
        Get
            Return mConfirmado
        End Get
        Set(ByVal value As Integer)
            mConfirmado = value
        End Set
    End Property

    Public Property Valor_fichas() As String
        Get
            Return mValor_fichas
        End Get
        Set(ByVal value As String)
            mValor_fichas = value
        End Set
    End Property

    Public Property Total_fichas() As String
        Get
            Return mTotal_fichas
        End Get
        Set(ByVal value As String)
            mTotal_fichas = value
        End Set
    End Property

#End Region

End Class
