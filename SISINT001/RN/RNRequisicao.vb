﻿Public Class RNRequisicao

#Region "Variáveis"

    Private daoRequisicao As RequisicaoDAO

#End Region

#Region "Construtores"

    Sub New()

        ' daoRetorno = New RetornoDAO()

    End Sub

#End Region



    Public Function DELETE_T_REQUISICAO_TMP() As Boolean

        Try

            If (daoRequisicao.DELETE_T_REQUISICAO_TMP()) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function DELETE_T_REQUISICAO_LISTA() As Boolean

        Try

            If (daoRequisicao.DELETE_T_REQUISICAO_LISTA()) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function DELETE_T_REQUISICAO_TMP2() As Boolean

        Try

            If (daoRequisicao.DELETE_T_REQUISICAO_TMP2()) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function DELETE_T_REQUISICAO_LISTA2() As Boolean

        Try

            If (daoRequisicao.DELETE_T_REQUISICAO_LISTA2()) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function SELECT_T_FUNCIONARIOS(ByVal TINST As String) As Integer
        Dim cod As Integer
        Try

            If Not IsDBNull(daoRequisicao.SELECT_T_FUNCIONARIOS(TINST)) Then
                Return cod
            Else
                Return 1
            End If

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function SELECT_T_CLIENTES() As DataSet
        Dim ds As DataSet
        Try

            ds = (daoRequisicao.SELECT_T_CLIENTES)

            If Not (IsDBNull(ds)) Then
                Return ds
            Else
                Return Nothing

            End If


        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function DELETE_T_LISTA_CONTRIBUINTE_LIB_TMP1(ByVal SETEMPRESA As Integer) As Boolean

        Try

            If (daoRequisicao.DELETE_T_LISTA_CONTRIBUINTE_LIB_TMP1(SETEMPRESA)) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function DELETE_T_LISTA_CONTRIBUINTE_LIB_TMP2(ByVal SETEMPRESA As Integer) As Boolean

        Try

            If (daoRequisicao.DELETE_T_LISTA_CONTRIBUINTE_LIB_TMP2(SETEMPRESA)) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function SELECT_T_MOV_CONTRIBUICAO_DESCR(ByVal TCODEAN As String) As DataSet
        Dim ds As DataSet
        Try

            ds = (daoRequisicao.SELECT_T_MOV_CONTRIBUICAO_DESCR(TCODEAN))

            If Not (IsDBNull(ds)) Then
                Return ds
            Else
                Return Nothing

            End If


        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function SELECT_T_REQUISICAO_TMP(ByVal TCODEANCLI As String) As DataSet
        Dim ds As DataSet
        Try

            ds = (daoRequisicao.SELECT_T_REQUISICAO_TMP(TCODEANCLI))

            If Not (IsDBNull(ds)) Then
                Return ds
            Else
                Return Nothing

            End If


        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function SELECT_T_ULT_MOV(ByVal TCODEANCLI As String) As DataSet
        Dim ds As DataSet
        Try

            ds = (daoRequisicao.SELECT_T_REQUISICAO_TMP(TCODEANCLI))

            If Not (IsDBNull(ds)) Then
                Return ds
            Else
                Return Nothing

            End If


        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    Public Function CarregaFichas(ByVal cidade As String, ByVal dia As Integer) As DataSet

        Return daoRequisicao.CarregaFichas(cidade, dia)


    End Function

#Region "Fichas"



    Public Function CarregaFichasOperador(ByVal categoria As Integer, ByVal cidade As String, ByVal codUsuario As Integer) As List(Of CContribuinte)
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Try

            Return ConverteParaObjetoFichas(daoRequisicao.CarregaFichasOperador(categoria, cidade, codUsuario))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregaFichasCemigOperador(ByVal categoria As Integer, ByVal cidade As String, ByVal codUsuario As Integer) As List(Of CContribuinte)
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Try

            Return ConverteParaObjetoFichas(daoRequisicao.CarregaFichasCemigOperador(categoria, cidade, codUsuario))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregaFichasSupervisao(ByVal categoria As Integer, ByVal cidade As String) As List(Of CContribuinte)
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Try

            Return ConverteParaObjetoFichas(daoRequisicao.CarregaFichasSupervisao(categoria, cidade))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function CarregaFichasNaoAtendeu(ByVal categoria As Integer, ByVal cidade As String, ByVal dia As Integer, ByVal cod_opera As Integer) As List(Of CContribuinte)
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Try

            Return ConverteParaObjetoFichas(daoRequisicao.CarregaFichasNaoAtendeu(categoria, cidade, dia, cod_opera))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function ConsultaFichas(ByVal categoria As Integer, ByVal cidade As String, ByVal dia As Integer, ByVal cod_opera As Integer) As List(Of CContribuinte)
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Try

            Return ConverteParaObjetoFichas(daoRequisicao.ConsultaFichas(categoria, cidade, dia, cod_opera))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregaFichasSupervisaoOi(ByVal categoria As String, ByVal operador As String) As List(Of CContribuinte)
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Try

            Return ConverteParaObjetoFichas(daoRequisicao.CarregaFichasSupervisaoOi(categoria, operador))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Private Function ConverteParaObjetoFichas(ByVal dsRequisicao As DataSet) As List(Of CContribuinte)
        Dim row As DataRow
        Dim fichas As List(Of CContribuinte) = New List(Of CContribuinte)

        Try

            For Each row In dsRequisicao.Tables(0).Rows

                fichas.Add(ConverteParaListFichas(row))

            Next

            Return fichas

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing

        End Try

    End Function

    Private Function ConverteParaListFichas(ByVal rwObjeto As DataRow) As CContribuinte
        Dim objRequisicao As CContribuinte = New CContribuinte()

        Try


            objRequisicao.Confirmado = IIf(IsDBNull(rwObjeto.Item("Confirmado")), 0, rwObjeto.Item("Confirmado"))
            objRequisicao.Tipo_mov = IIf(IsDBNull(rwObjeto.Item("Tipo_mov").ToString()), "", rwObjeto.Item("Tipo_mov").ToString())
            objRequisicao.Tipo_cobranca = IIf(IsDBNull(rwObjeto.Item("Tipo_cobranca").ToString()), "", rwObjeto.Item("Tipo_cobranca").ToString())
            objRequisicao.Cobrador = IIf(IsDBNull(rwObjeto.Item("Cobrador").ToString()), "", rwObjeto.Item("Cobrador").ToString())
            objRequisicao.Cod_Categoria = (IIf(IsDBNull(rwObjeto.Item("Cod_Categoria")), 0, rwObjeto.Item("Cod_Categoria")))
            objRequisicao.Cod_grupo = (IIf(IsDBNull(rwObjeto.Item("Cod_grupo")), 0, rwObjeto.Item("Cod_grupo")))
            objRequisicao.Cod_opera = IIf(IsDBNull(rwObjeto.Item("Cod_opera").ToString()), "", rwObjeto.Item("Cod_opera").ToString())
            objRequisicao.Cod_Especie = IIf(IsDBNull(rwObjeto.Item("Cod_Especie")), 0, rwObjeto.Item("Cod_Especie"))
            objRequisicao.Data_mov = IIf(IsDBNull(rwObjeto.Item("Data_mov").ToString()), "", rwObjeto.Item("Data_mov").ToString())
            objRequisicao.Cod_EAN_Cliente = IIf(IsDBNull(rwObjeto.Item("Cod_EAN_Cliente").ToString()), "", rwObjeto.Item("Cod_EAN_Cliente").ToString())
            objRequisicao.Cod_contribuinte = IIf(IsDBNull(rwObjeto.Item("Cod_contribuinte").ToString()), "", rwObjeto.Item("Cod_contribuinte").ToString())
            objRequisicao.Nome_contribuinte = IIf(IsDBNull(rwObjeto.Item("Nome_contribuinte").ToString()), "", rwObjeto.Item("Nome_contribuinte").ToString())
            objRequisicao.Valor = IIf(IsDBNull(rwObjeto.Item("Valor")), 0, rwObjeto.Item("Valor"))
            objRequisicao.Tipo_contrib = IIf(IsDBNull(rwObjeto.Item("Tipo_contrib").ToString()), "", rwObjeto.Item("Tipo_contrib").ToString())
            objRequisicao.Cod_recibo = IIf(IsDBNull(rwObjeto.Item("Cod_recibo").ToString()), "", rwObjeto.Item("Cod_recibo").ToString())
            objRequisicao.Dia = IIf(IsDBNull(rwObjeto.Item("Dia").ToString()), 0, rwObjeto.Item("Dia").ToString())
            objRequisicao.Cidade = IIf(IsDBNull(rwObjeto.Item("Cidade").ToString()), "", rwObjeto.Item("Cidade").ToString())
            objRequisicao.Cod_cobrador = IIf(IsDBNull(rwObjeto.Item("Cod_cobrador")), 0, rwObjeto.Item("Cod_cobrador"))
            objRequisicao.Cod_Cidade = IIf(IsDBNull(rwObjeto.Item("Cod_Cidade")), 0, rwObjeto.Item("Cod_Cidade"))
            objRequisicao.Cod_empresa = IIf(IsDBNull(rwObjeto.Item("Cod_empresa")), 0, rwObjeto.Item("Cod_empresa"))
            objRequisicao.Situacao = IIf(IsDBNull(rwObjeto.Item("Situacao").ToString()), "", rwObjeto.Item("Situacao").ToString())
            objRequisicao.Cod_Bairro = IIf(IsDBNull(rwObjeto.Item("Cod_Bairro")), 0, rwObjeto.Item("Cod_Bairro"))
            objRequisicao.Cod_Usuario = IIf(IsDBNull(rwObjeto.Item("Cod_Usuario")), 0, rwObjeto.Item("Cod_Usuario"))
            objRequisicao.DDD = IIf(IsDBNull(rwObjeto.Item("DDD").ToString()), "", rwObjeto.Item("DDD").ToString())
            objRequisicao.Telefone1 = IIf(IsDBNull(rwObjeto.Item("telefone1").ToString()), "", rwObjeto.Item("telefone1").ToString())
            objRequisicao.Data_impressao = IIf(IsDBNull(rwObjeto.Item("dt_impressao").ToString()), "", rwObjeto.Item("dt_impressao").ToString())
            objRequisicao.Observacao = IIf(IsDBNull(rwObjeto.Item("observacao").ToString()), "", rwObjeto.Item("observacao").ToString())



            Return objRequisicao

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function

    Public Function CarregaFichaPorCodEan(ByVal cod_ean As String) As DataSet
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Try

            Return daoRequisicao.CarregaRequisicaoPorCodEan(cod_ean)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaRequisicaoPorCodEan(ByVal cod_ean As String) As DataSet
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Try

            Return daoRequisicao.RetornaRequisicaoPorCodEan(cod_ean)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function



    Public Function CarregaContribuintePorCodEan(ByVal cod_ean As String) As DataSet
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Try

            Return daoRequisicao.CarregaContribuintePorCodEan(cod_ean)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregaFichaPorCodRecibo(ByVal cod_recibo As String) As DataSet
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Try

            Return daoRequisicao.CarregaRequisicaoPorCod_recibo(cod_recibo)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Private Function ConverteParaObjetoLiberaFicha(ByVal dsRequisicao As DataSet) As List(Of CContribuinte)
        Dim row As DataRow
        Dim fichas As List(Of CContribuinte) = New List(Of CContribuinte)

        Try

            For Each row In dsRequisicao.Tables(0).Rows

                fichas.Add(ConverteParaListLiberaFicha(row))

            Next

            Return fichas

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing

        End Try

    End Function

    Private Function ConverteParaListLiberaFicha(ByVal rwObjeto As DataRow) As CContribuinte
        Dim objRequisicao As CContribuinte = New CContribuinte()

        Try


            objRequisicao.Confirmado = IIf(IsDBNull(rwObjeto.Item("Confirmado").ToString()), "", rwObjeto.Item("Confirmado").ToString())
            objRequisicao.Tipo_mov = IIf(IsDBNull(rwObjeto.Item("Tipo_mov").ToString()), "", rwObjeto.Item("Tipo_mov").ToString())
            objRequisicao.Tipo_cobranca = IIf(IsDBNull(rwObjeto.Item("Tipo_cobranca").ToString()), "", rwObjeto.Item("Tipo_cobranca").ToString())
            objRequisicao.Cobrador = IIf(IsDBNull(rwObjeto.Item("Cobrador").ToString()), "", rwObjeto.Item("Cobrador").ToString())
            objRequisicao.Cod_Categoria = (IIf(IsDBNull(rwObjeto.Item("Cod_Categoria").ToString()), "", rwObjeto.Item("Cod_Categoria").ToString()))
            objRequisicao.Cod_grupo = (IIf(IsDBNull(rwObjeto.Item("Cod_grupo").ToString()), "", rwObjeto.Item("Cod_grupo").ToString()))
            objRequisicao.Cod_opera = IIf(IsDBNull(rwObjeto.Item("Cod_opera").ToString()), "", rwObjeto.Item("Cod_opera").ToString())
            objRequisicao.Cod_Especie = IIf(IsDBNull(rwObjeto.Item("Cod_Especie").ToString()), "", rwObjeto.Item("Cod_Especie").ToString())
            objRequisicao.Data_mov = IIf(IsDBNull(rwObjeto.Item("Data_mov").ToString()), "", rwObjeto.Item("Data_mov").ToString())
            objRequisicao.Cod_EAN_Cliente = IIf(IsDBNull(rwObjeto.Item("Cod_EAN_Cliente").ToString()), "", rwObjeto.Item("Cod_EAN_Cliente").ToString())
            objRequisicao.Cod_contribuinte = IIf(IsDBNull(rwObjeto.Item("Cod_contribuinte").ToString()), "", rwObjeto.Item("Cod_contribuinte").ToString())
            objRequisicao.Nome_contribuinte = IIf(IsDBNull(rwObjeto.Item("Nome_contribuinte").ToString()), "", rwObjeto.Item("Nome_contribuinte").ToString())
            objRequisicao.Valor = IIf(IsDBNull(rwObjeto.Item("Valor").ToString()), "", rwObjeto.Item("Valor").ToString())
            objRequisicao.Tipo_contrib = IIf(IsDBNull(rwObjeto.Item("Tipo_contrib").ToString()), "", rwObjeto.Item("Tipo_contrib").ToString())
            objRequisicao.Cod_recibo = IIf(IsDBNull(rwObjeto.Item("Cod_recibo").ToString()), "", rwObjeto.Item("Cod_recibo").ToString())
            objRequisicao.Dia = IIf(IsDBNull(rwObjeto.Item("Dia").ToString()), "", rwObjeto.Item("Dia").ToString())
            objRequisicao.Cidade = IIf(IsDBNull(rwObjeto.Item("Cidade").ToString()), "", rwObjeto.Item("Cidade").ToString())
            objRequisicao.Cod_cobrador = IIf(IsDBNull(rwObjeto.Item("Cod_cobrador").ToString()), "", rwObjeto.Item("Cod_cobrador").ToString())
            objRequisicao.Cod_Cidade = IIf(IsDBNull(rwObjeto.Item("Cod_Cidade").ToString()), "", rwObjeto.Item("Cod_Cidade").ToString())
            objRequisicao.Cod_empresa = IIf(IsDBNull(rwObjeto.Item("Cod_empresa").ToString()), "", rwObjeto.Item("Cod_empresa").ToString())
            objRequisicao.Situacao = IIf(IsDBNull(rwObjeto.Item("Situacao").ToString()), "", rwObjeto.Item("Situacao").ToString())
            objRequisicao.Cod_Bairro = IIf(IsDBNull(rwObjeto.Item("Cod_Bairro").ToString()), "", rwObjeto.Item("Cod_Bairro").ToString())
            objRequisicao.Cod_Usuario = IIf(IsDBNull(rwObjeto.Item("Cod_Usuario").ToString()), "", rwObjeto.Item("Cod_Usuario").ToString())



            Return objRequisicao

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function

    Public Function ApagaRegistroErradosRequisicao(ByVal telefone As String) As Boolean
        Dim daoRequisicao As New RequisicaoDAO
        Try



            'If daoRequisicao.ApagaRegistroErradosRequisicao(telefone) = True Then
            '    Return True
            'Else
            '    Return False
            'End If

            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function


    Public Function AtualizaDadosListaTelefonica(ByVal contribuinte As CContribuinte) As Boolean
        Dim daoRequisicao As New RequisicaoDAO
        Try



            If daoRequisicao.AtualizaDadosListaTelefonica(contribuinte) = True Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function AtualizaDadosListaTelefonicaCemig(ByVal contribuinte As CContribuinte) As Boolean
        Dim daoRequisicao As New RequisicaoDAO
        Try
            If daoRequisicao.AtualizaDadosListaTelefonicaCemig(contribuinte) = True Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function



#End Region


#Region "Faturamento"

    Public Function CarregaFichasFaturamento(ByVal cidade As String, ByVal dia As Integer, ByVal operador_id As Integer, ByVal imprimirMensal As String) As DataSet
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Dim ds As DataSet = New DataSet


        Try
            If imprimirMensal = "S" And operador_id <= 0 Then
                daoRequisicao.AtualizaCodOperaMensal(cidade, dia, operador_id, imprimirMensal)
            End If

            ds = daoRequisicao.CarregaFichasFaturamento(cidade, dia, operador_id, imprimirMensal)

            If Not ds Is Nothing Then
                If (ds.Tables(0).Rows.Count > 0) Then
                    If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                        Return ds
                    End If
                Else
                    Return Nothing
                End If
            End If

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregaFichasFaturamentoMensalEspecial(ByVal cidade As String, ByVal dia As Integer, ByVal operador_id As Integer) As DataSet
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Dim ds As DataSet = New DataSet


        Try

            ds = daoRequisicao.CarregaFichasFaturamentoMensalEspecial(cidade, dia, operador_id)

            If Not ds Is Nothing Then
                If (ds.Tables(0).Rows.Count > 0) Then
                    If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                        Return ds
                    End If
                Else
                    Return Nothing
                End If
            End If

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregaFichasFaturamentoBordero(ByVal cidade As String, ByVal dia As Integer, ByVal operador_id As Integer) As DataSet
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Dim ds As DataSet = New DataSet


        Try

            ds = daoRequisicao.CarregaFichasFaturamentoBordero(cidade, dia, operador_id)

            If Not ds Is Nothing Then
                If (ds.Tables(0).Rows.Count > 0) Then
                    If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                        Return ds
                    End If
                Else
                    Return Nothing
                End If
            End If
            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregaFichasFaturamentoBaixa(ByVal cidade As String, ByVal dia As Integer, ByVal operador_id As Integer) As DataSet
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Dim ds As DataSet = New DataSet


        Try

            ds = daoRequisicao.CarregaFichasFaturamentoBaixa(cidade, dia, operador_id)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    Return ds
                End If
            Else
                Return Nothing
            End If

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function CarregaFichasImprimirManual(ByVal cidade As String, ByVal dia As Integer, ByVal operador_id As Integer) As DataSet
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Dim ds As DataSet = New DataSet


        Try

            ds = daoRequisicao.CarregaFichasImprimirManual(cidade, dia, operador_id)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    Return ds
                End If
            Else
                Return Nothing
            End If

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function



    Public Function CarregaFichasRequisicaoPorCidadeDia(ByVal cidade As String, ByVal dia As Integer, ByVal categoria_id As Integer) As DataSet
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Dim ds As DataSet = New DataSet


        Try

            ds = daoRequisicao.CarregaFichasRequisicaoPorCidadeDia(cidade, dia, categoria_id)

            If Not ds Is Nothing Then
                If (ds.Tables(0).Rows.Count > 0) Then
                    If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                        Return ds
                    End If
                Else
                    Return Nothing
                End If

                Return ds

            End If
            Return Nothing
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregaFichasRequisicaoCemigPorCidadeDia(ByVal cidade As String, ByVal dia As Integer, ByVal categoria_id As Integer) As DataSet
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Dim ds As DataSet = New DataSet


        Try

            ds = daoRequisicao.CarregaFichasRequisicaoCemigPorCidadeDia(cidade, dia, categoria_id)

            If Not ds Is Nothing Then
                If (ds.Tables(0).Rows.Count > 0) Then
                    If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                        Return ds
                    End If
                Else
                    Return Nothing
                End If

                Return ds

            End If
            Return Nothing
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function




#End Region

#Region "Movimentacoes"

    Public Function CarregaMovimentacoes(ByVal cod_mov_contribuicao As Long, ByVal tipo_mov As String, ByVal cobrador As String, ByVal dataMov As String) As List(Of CRequisicao)
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Return ConverteParaObjetoMovimentacao(daoRequisicao.CarregaMovimentacoes(cod_mov_contribuicao, tipo_mov, cobrador, dataMov))


    End Function

    Public Function CarregaMovimentacoesDescrOpt(ByVal cod_mov_contribuicao As Long, ByVal tipo_mov As String, ByVal cobrador As String, ByVal dataMov As String) As DataSet
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Return daoRequisicao.CarregaMovimentacoesDescrOpt(cod_mov_contribuicao, tipo_mov, cobrador, dataMov)


    End Function

    Private Function ConverteParaObjetoMovimentacao(ByVal dsMovimentacoes As DataSet) As List(Of CRequisicao)
        Dim row As DataRow
        Dim movimentacoes As List(Of CRequisicao) = New List(Of CRequisicao)

        Try

            For Each row In dsMovimentacoes.Tables(0).Rows

                movimentacoes.Add(ConverteParaListMovimentacao(row))

            Next

            Return movimentacoes

        Catch ex As Exception

            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing

        End Try

    End Function

    Private Function ConverteParaListMovimentacao(ByVal rwObjeto As DataRow) As CRequisicao
        Dim objMovimentacoes As CRequisicao = New CRequisicao()

        Try

            objMovimentacoes.Mov_contribuicao_id = (IIf(IsDBNull(rwObjeto.Item("cod_mov_contribuicao")), 0, rwObjeto.Item("cod_mov_contribuicao")))
            objMovimentacoes.Bordero_id = (IIf(IsDBNull(rwObjeto.Item("Bordero_id")), 0, rwObjeto.Item("Bordero_id")))
            objMovimentacoes.Baixa_id = IIf(IsDBNull(rwObjeto.Item("Baixa_id")), 0, rwObjeto.Item("Baixa_id"))
            objMovimentacoes.Confirmado = IIf(IsDBNull(rwObjeto.Item("Confirmado")), 0, rwObjeto.Item("Confirmado"))
            objMovimentacoes.Tipo_mov = IIf(IsDBNull(rwObjeto.Item("Tipo_mov").ToString()), "", rwObjeto.Item("Tipo_mov").ToString())
            objMovimentacoes.Tipo_cobranca = IIf(IsDBNull(rwObjeto.Item("Tipo_cobranca").ToString()), "", rwObjeto.Item("Tipo_cobranca").ToString())
            objMovimentacoes.Cobrador = IIf(IsDBNull(rwObjeto.Item("Cobrador").ToString()), "", rwObjeto.Item("Cobrador").ToString())
            objMovimentacoes.Total_fichas = IIf(IsDBNull(rwObjeto.Item("Total_fichas").ToString()), "", rwObjeto.Item("Total_fichas").ToString())
            objMovimentacoes.Valor_fichas = IIf(IsDBNull(rwObjeto.Item("Valor_fichas").ToString()), "", rwObjeto.Item("Valor_fichas").ToString())
            objMovimentacoes.Data_mov = IIf(IsDBNull(rwObjeto.Item("Data_mov").ToString()), "", rwObjeto.Item("Data_mov").ToString())
            objMovimentacoes.Cod_cobrador = IIf(IsDBNull(rwObjeto.Item("Cod_cobrador")), 0, rwObjeto.Item("Cod_cobrador"))
            objMovimentacoes.Cod_empresa = IIf(IsDBNull(rwObjeto.Item("Cod_empresa")), 0, rwObjeto.Item("Cod_empresa"))



            Return objMovimentacoes

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaUltimaMovimentacao() As Long
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Dim cod_mov_contribuicao As Long

        Try


            cod_mov_contribuicao = daoRequisicao.RetornaUltimaMovimentacao()

            If (cod_mov_contribuicao > 0) Then

                Return cod_mov_contribuicao + 100000

            Else
                Return 109000
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function RetornaUltimoRecibo() As Long
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Dim cod_recibo As Long

        Try

            cod_recibo = daoRequisicao.RetornaUltimoRecibo()

            If (cod_recibo > 0) Then

                Return cod_recibo

            Else
                Return 203207
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

#End Region

#Region "Historico"


    Public Function CarregaHistoricoContribuinte(ByVal cod_ean As String) As List(Of CContribuinte)
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Return ConverteParaObjetoHistorico(daoRequisicao.CarregaHistoricoContribuinte(cod_ean))


    End Function

    Private Function ConverteParaObjetoHistorico(ByVal dsRequisicao As DataSet) As List(Of CContribuinte)
        Dim row As DataRow
        Dim fichas As List(Of CContribuinte) = New List(Of CContribuinte)

        Try

            For Each row In dsRequisicao.Tables(0).Rows

                fichas.Add(ConverteParaListHistoricoContribuinte(row))

            Next

            Return fichas

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing

        End Try

    End Function

    Private Function ConverteParaListHistoricoContribuinte(ByVal rwObjeto As DataRow) As CContribuinte
        Dim objRequisicao As CContribuinte = New CContribuinte()

        Try
            objRequisicao.Tipo_mov = IIf(IsDBNull(rwObjeto.Item("tipo_mov").ToString()), "", rwObjeto.Item("tipo_mov").ToString())
            objRequisicao.Tipo_cobranca = IIf(IsDBNull(rwObjeto.Item("tipo_cobranca").ToString()), "", rwObjeto.Item("tipo_cobranca").ToString())
            objRequisicao.Cobrador = IIf(IsDBNull(rwObjeto.Item("cobrador").ToString()), "", rwObjeto.Item("cobrador").ToString())
            objRequisicao.Cod_Categoria = (IIf(IsDBNull(rwObjeto.Item("Cod_Categoria").ToString()), "", rwObjeto.Item("Cod_Categoria").ToString()))
            objRequisicao.Cod_grupo = (IIf(IsDBNull(rwObjeto.Item("Cod_grupo").ToString()), "", rwObjeto.Item("Cod_grupo").ToString()))
            objRequisicao.Cod_opera = IIf(IsDBNull(rwObjeto.Item("Cod_opera").ToString()), "", rwObjeto.Item("Cod_opera").ToString())
            objRequisicao.Cod_Especie = IIf(IsDBNull(rwObjeto.Item("Cod_Especie").ToString()), "", rwObjeto.Item("Cod_Especie").ToString())
            objRequisicao.Data_mov = IIf(IsDBNull(rwObjeto.Item("Data_mov").ToString()), "", rwObjeto.Item("Data_mov").ToString())
            objRequisicao.Cod_EAN_Cliente = IIf(IsDBNull(rwObjeto.Item("Cod_EAN_Cliente").ToString()), "", rwObjeto.Item("Cod_EAN_Cliente").ToString())
            objRequisicao.Cod_contribuinte = IIf(IsDBNull(rwObjeto.Item("Cod_contribuinte").ToString()), "", rwObjeto.Item("Cod_contribuinte").ToString())
            objRequisicao.Nome_contribuinte = IIf(IsDBNull(rwObjeto.Item("Nome_contribuinte").ToString()), "", rwObjeto.Item("Nome_contribuinte").ToString())
            objRequisicao.Valor = IIf(IsDBNull(rwObjeto.Item("Valor").ToString()), "", rwObjeto.Item("Valor").ToString())
            objRequisicao.Tipo_contrib = IIf(IsDBNull(rwObjeto.Item("Tipo_contrib").ToString()), "", rwObjeto.Item("Tipo_contrib").ToString())
            objRequisicao.Cod_recibo = IIf(IsDBNull(rwObjeto.Item("Cod_recibo").ToString()), "", rwObjeto.Item("Cod_recibo").ToString())
            objRequisicao.Dia = IIf(IsDBNull(rwObjeto.Item("Dia").ToString()), "", rwObjeto.Item("Dia").ToString())
            objRequisicao.Cidade = IIf(IsDBNull(rwObjeto.Item("Cidade").ToString()), "", rwObjeto.Item("Cidade").ToString())
            objRequisicao.Cod_cobrador = IIf(IsDBNull(rwObjeto.Item("Cod_cobrador").ToString()), "", rwObjeto.Item("Cod_cobrador").ToString())
            objRequisicao.Cod_Cidade = IIf(IsDBNull(rwObjeto.Item("Cod_Cidade").ToString()), "", rwObjeto.Item("Cod_Cidade").ToString())
            objRequisicao.Cod_empresa = IIf(IsDBNull(rwObjeto.Item("Cod_empresa").ToString()), "", rwObjeto.Item("Cod_empresa").ToString())
            objRequisicao.Situacao = IIf(IsDBNull(rwObjeto.Item("Situacao").ToString()), "", rwObjeto.Item("Situacao").ToString())
            objRequisicao.Cod_Bairro = IIf(IsDBNull(rwObjeto.Item("Cod_Bairro").ToString()), "", rwObjeto.Item("Cod_Bairro").ToString())
            objRequisicao.Cod_Usuario = IIf(IsDBNull(rwObjeto.Item("Cod_Usuario").ToString()), "", rwObjeto.Item("Cod_Usuario").ToString())
            objRequisicao.DDD = IIf(IsDBNull(rwObjeto.Item("DDD").ToString()), "", rwObjeto.Item("DDD").ToString())
            objRequisicao.Telefone1 = IIf(IsDBNull(rwObjeto.Item("telefone1").ToString()), "", rwObjeto.Item("telefone1").ToString())
            objRequisicao.data_venc = IIf(IsDBNull(rwObjeto.Item("data_venc").ToString()), "", rwObjeto.Item("data_venc").ToString())
            objRequisicao.Confirmado = IIf(IsDBNull(rwObjeto.Item("confirmado").ToString()), "", rwObjeto.Item("confirmado").ToString())
            objRequisicao.Cod_Regiao = IIf(IsDBNull(rwObjeto.Item("cod_regiao").ToString()), "", rwObjeto.Item("cod_regiao").ToString())


            Return objRequisicao

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function ConfirmarContribuicao(ByVal filtro As CContribuinte, ByVal aceite As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.ConfirmarContribuicao(filtro, aceite)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function ConfirmarContribuicaoCemig(ByVal filtro As CContribuinte, ByVal aceite As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.ConfirmarContribuicaoCemig(filtro, aceite)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function ConfirmarContribuicaoSupervisao(ByVal filtro As CContribuinte, ByVal aceite As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.ConfirmarContribuicaoSupervisao(filtro, aceite)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function AgendaProxDia(ByVal observacao As String, ByVal cod_ean As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.AgendaProxDia(observacao, cod_ean)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function AgendaProxDiaOi(ByVal observacao As String, ByVal telefone As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.AgendaProxDiaOi(observacao, telefone)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function AgendaProxDiaCemig(ByVal observacao As String, ByVal telefone As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.AgendaProxDiaCemig(observacao, telefone)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function TelefoneDesligado(ByVal telefone As String, ByVal cod_ean As String, ByVal ddd As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.TelefoneDesligado(telefone, cod_ean, ddd)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function TelefoneDesligadoCemig(ByVal telefone As String, ByVal cod_ean As String, ByVal ddd As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.TelefoneDesligadoCemig(telefone, cod_ean, ddd)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function RecusaEspecialParaMensal(ByVal categoria As Integer, ByVal cod_ean As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.RecusaEspecialParaMensal(categoria, cod_ean)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function RecusaEspecialParaMensalCemig(ByVal categoria As Integer, ByVal cod_ean As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.RecusaEspecialParaMensalCemig(categoria, cod_ean)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function CemigRecusaEspecialParaMensal(ByVal categoria As Integer, ByVal cod_ean As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.RecusaEspecialParaMensal(categoria, cod_ean)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function LiberaFichaParaFaturamento(ByVal categoria As Integer, ByVal cod_ean As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.LiberaFichaParaFaturamento(categoria, cod_ean)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function Atualiza_Movimentacao(ByVal cod_mov_contribuicao As Long, ByVal cod_bordero As Long, ByVal cod_recibo As String, ByVal cod_cobrador As Integer, ByVal cobrador As String, ByVal cod_cliente As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.Atualiza_Movimentacao(cod_mov_contribuicao, cod_bordero, cod_recibo, cod_cobrador, cobrador, cod_cliente)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function Atualiza_Movimentacao_baixa(ByVal cod_mov_contribuicao As Long, ByVal cod_recibo As String, ByVal cod_baixa As Long, ByVal cod_cobrador As Integer, ByVal cobrador As String, ByVal tipo_mov As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.Atualiza_Movimentacao_baixa(cod_mov_contribuicao, cod_recibo, cod_baixa, cod_cobrador, cobrador, tipo_mov)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function GeraRegistroBordero(ByVal cod_mov_contribuicao As Long, ByVal cod_recibo As String, ByVal cod_cliente As String, ByVal bordero_id As Long) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try


            Return daoRequisicao.CriaRegistroBordero(cod_mov_contribuicao, cod_recibo, cod_cliente, bordero_id)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function GeraRegistroBaixa(ByVal cod_mov_contribuicao As Long, ByVal cod_recibo As String, ByVal cod_cliente As String, ByVal baixa_id As Long, ByVal tipo_mov As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try


            Return daoRequisicao.CriaRegistroBaixa(cod_mov_contribuicao, cod_recibo, cod_cliente, baixa_id, tipo_mov)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function RetornaIdBordero() As Integer
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Dim bordero_id As Integer = 0
        Try

            bordero_id = daoRequisicao.RetornaIdBordero()

            Return bordero_id


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return 0
        End Try
    End Function

    Public Function RetornaIdBaixa() As Integer
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Dim bordero_id As Integer = 0
        Try

            bordero_id = daoRequisicao.RetornaIdBaixa()

            Return bordero_id


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return 0
        End Try
    End Function

    Public Function GeraMovimentacao(ByVal cod_mov_contribuicao As Long, ByVal cod_recibo As String, ByVal cod_cliente As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.GeraMovimentacao(cod_mov_contribuicao, cod_recibo, cod_cliente)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return 0
        End Try
    End Function

    Public Function Atualiza_Movimentacao_Liberacao(ByVal cod_mov_contribuicao As Long, ByVal cod_recibo As String, ByVal cod_cobrador As Integer, ByVal cobrador As String, ByVal cod_cliente As String, ByVal valor As Decimal) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try


            Return daoRequisicao.Atualiza_Movimentacao_Liberacao(cod_mov_contribuicao, cod_recibo, cod_cobrador, cobrador, cod_cliente, valor)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return 0
        End Try
    End Function

    Public Function VerficaFichaImpressa(ByVal cod_cliente As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Dim ds As New DataSet
        Try


            ds = daoRequisicao.VerficaFichaImpressa(cod_cliente)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    Return True
                End If
            Else
                Return False
            End If
            Return Nothing
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function GravaTabelaLiberacao(ByVal cod_mov_contribuicao As Long, ByVal cod_recibo As String, ByVal cod_cliente As String, ByVal valor As Decimal) As Boolean

        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.GravaTabelaLiberacao(cod_mov_contribuicao, cod_recibo, cod_cliente, valor)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function DeletaDadosTabelaLiberacao() As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.DeletaDadosTabelaLiberacao()


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function GravaNumerosMovimentacao(ByVal cod_mov_contribuicao As Long, ByVal cobrador As String, ByVal cod_cobrador As Integer, ByVal cod_empresa As Integer, ByVal totalFichas As Integer, ByVal ValorFichas As Decimal, ByVal tipo_mov As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.GravaNumerosMovimentacao(cod_mov_contribuicao, cobrador, cod_cobrador, cod_empresa, totalFichas, ValorFichas, tipo_mov)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function CarregaMovimentacaoDescr(ByVal cod_mov_contribuicao As Long) As DataSet
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Return daoRequisicao.CarregaMovimentacoesDescr(cod_mov_contribuicao)


    End Function

    Public Function CarregaMovimentacoesDescrBaixa(ByVal cod_mov_contribuicao As Long) As DataSet
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Return daoRequisicao.CarregaMovimentacoesDescrBaixa(cod_mov_contribuicao)


    End Function

    Public Function CarregaMovimentacaoDescrBorderoDataMov(ByVal data_mov As String, ByVal operador As String) As DataSet
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Return daoRequisicao.CarregaMovimentacaoDescrBorderoDataMov(data_mov, operador)


    End Function

    Public Function CarregaMovimentacaoDescrFichasAbertasDataMov(ByVal data_mov As String, ByVal cidade As String) As DataSet
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Return daoRequisicao.CarregaMovimentacaoDescrFichasAbertasDataMov(data_mov, cidade)


    End Function

    Public Function CarregaMovimentacaoDescrFichasAbertasDataMovCobrador(ByVal data_mov As String, ByVal cobrador As String, ByVal cidade As String) As DataSet
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Return daoRequisicao.CarregaMovimentacaoDescrFichasAbertasDataMovCobrador(data_mov, cobrador, cidade)


    End Function

    Public Function CarregaMovimentacaoDescrBordero(ByVal cod_mov_contribuicao As Long) As DataSet
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Return daoRequisicao.CarregaMovimentacaoDescrBordero(cod_mov_contribuicao)


    End Function

    Public Function CarregaMovimentacaoDescrFichasAbertas(ByVal cod_mov_contribuicao As Long) As DataSet
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Return daoRequisicao.CarregaMovimentacaoDescrFichasAbertas(cod_mov_contribuicao)


    End Function

    Private Function ConverteParaObjetoMovimentacaoDescr(ByVal dsRequisicao As DataSet) As List(Of CContribuinte)
        Dim row As DataRow
        Dim fichas As List(Of CContribuinte) = New List(Of CContribuinte)

        Try

            For Each row In dsRequisicao.Tables(0).Rows

                fichas.Add(ConverteParaListMovimentacaoDescr(row))

            Next

            Return fichas

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing

        End Try

    End Function

    Private Function ConverteParaListMovimentacaoDescr(ByVal rwObjeto As DataRow) As CContribuinte
        Dim objRequisicao As CContribuinte = New CContribuinte()

        Try

            objRequisicao.Cod_mov_contribuicao = IIf(IsDBNull(rwObjeto.Item("Cod_mov_contribuicao")), 0, rwObjeto.Item("Cod_mov_contribuicao"))
            objRequisicao.Tipo_mov = IIf(IsDBNull(rwObjeto.Item("tipo_mov").ToString()), "", rwObjeto.Item("tipo_mov").ToString())
            objRequisicao.Tipo_cobranca = IIf(IsDBNull(rwObjeto.Item("tipo_cobranca").ToString()), "", rwObjeto.Item("tipo_cobranca").ToString())
            objRequisicao.Cobrador = IIf(IsDBNull(rwObjeto.Item("cobrador").ToString()), "", rwObjeto.Item("cobrador").ToString())
            objRequisicao.Cod_Categoria = (IIf(IsDBNull(rwObjeto.Item("Cod_Categoria").ToString()), "", rwObjeto.Item("Cod_Categoria").ToString()))
            objRequisicao.Cod_grupo = (IIf(IsDBNull(rwObjeto.Item("Cod_grupo").ToString()), "", rwObjeto.Item("Cod_grupo").ToString()))
            objRequisicao.Cod_opera = IIf(IsDBNull(rwObjeto.Item("Cod_opera").ToString()), "", rwObjeto.Item("Cod_opera").ToString())
            objRequisicao.Cod_Especie = IIf(IsDBNull(rwObjeto.Item("Cod_Especie").ToString()), "", rwObjeto.Item("Cod_Especie").ToString())
            objRequisicao.Data_mov = IIf(IsDBNull(rwObjeto.Item("Data_mov").ToString()), "", rwObjeto.Item("Data_mov").ToString())
            objRequisicao.Cod_EAN_Cliente = IIf(IsDBNull(rwObjeto.Item("Cod_EAN_Cliente").ToString()), "", rwObjeto.Item("Cod_EAN_Cliente").ToString())
            objRequisicao.Cod_contribuinte = IIf(IsDBNull(rwObjeto.Item("Cod_contribuinte").ToString()), "", rwObjeto.Item("Cod_contribuinte").ToString())
            objRequisicao.Nome_contribuinte = IIf(IsDBNull(rwObjeto.Item("Nome_contribuinte").ToString()), "", rwObjeto.Item("Nome_contribuinte").ToString())
            objRequisicao.Valor = IIf(IsDBNull(rwObjeto.Item("Valor").ToString()), "", rwObjeto.Item("Valor").ToString())
            objRequisicao.Tipo_contrib = IIf(IsDBNull(rwObjeto.Item("Tipo_contrib").ToString()), "", rwObjeto.Item("Tipo_contrib").ToString())
            objRequisicao.Cod_recibo = IIf(IsDBNull(rwObjeto.Item("Cod_recibo").ToString()), "", rwObjeto.Item("Cod_recibo").ToString())
            objRequisicao.Dia = IIf(IsDBNull(rwObjeto.Item("Dia").ToString()), "", rwObjeto.Item("Dia").ToString())
            objRequisicao.Cidade = IIf(IsDBNull(rwObjeto.Item("Cidade").ToString()), "", rwObjeto.Item("Cidade").ToString())
            objRequisicao.Cod_cobrador = IIf(IsDBNull(rwObjeto.Item("Cod_cobrador").ToString()), "", rwObjeto.Item("Cod_cobrador").ToString())
            objRequisicao.Cod_Cidade = IIf(IsDBNull(rwObjeto.Item("Cod_Cidade").ToString()), "", rwObjeto.Item("Cod_Cidade").ToString())
            objRequisicao.Cod_empresa = IIf(IsDBNull(rwObjeto.Item("Cod_empresa").ToString()), "", rwObjeto.Item("Cod_empresa").ToString())
            objRequisicao.Situacao = IIf(IsDBNull(rwObjeto.Item("Situacao").ToString()), "", rwObjeto.Item("Situacao").ToString())
            objRequisicao.Cod_Bairro = IIf(IsDBNull(rwObjeto.Item("Cod_Bairro").ToString()), "", rwObjeto.Item("Cod_Bairro").ToString())
            objRequisicao.Cod_Usuario = IIf(IsDBNull(rwObjeto.Item("Cod_Usuario").ToString()), "", rwObjeto.Item("Cod_Usuario").ToString())
            objRequisicao.DDD = IIf(IsDBNull(rwObjeto.Item("DDD").ToString()), "", rwObjeto.Item("DDD").ToString())
            objRequisicao.Telefone1 = IIf(IsDBNull(rwObjeto.Item("telefone1").ToString()), "", rwObjeto.Item("telefone1").ToString())
            objRequisicao.Data_venc = IIf(IsDBNull(rwObjeto.Item("data_venc").ToString()), "", rwObjeto.Item("data_venc").ToString())
            objRequisicao.Confirmado = IIf(IsDBNull(rwObjeto.Item("confirmado").ToString()), "", rwObjeto.Item("confirmado").ToString())
            objRequisicao.Cod_Regiao = IIf(IsDBNull(rwObjeto.Item("cod_regiao").ToString()), "", rwObjeto.Item("cod_regiao").ToString())


            Return objRequisicao

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function ConfirmarContribuicaoSupervisaoOi(ByVal filtro As CContribuinte, ByVal aceite As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.ConfirmarContribuicaoSupervisaoOi(filtro, aceite)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

#End Region

#Region "Lista Nova"

    Public Function CarregaFichasListaNova(ByVal cidade As String, ByVal cod As Integer) As List(Of CContribuinte)
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Try



            Return ConverteParaObjetoListaNova(daoRequisicao.CarregaListaNova(cidade, cod))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Private Function ConverteParaObjetoListaNova(ByVal dsRequisicao As DataSet) As List(Of CContribuinte)
        Dim row As DataRow
        Dim fichas As List(Of CContribuinte) = New List(Of CContribuinte)

        Try

            For Each row In dsRequisicao.Tables(0).Rows

                fichas.Add(ConverteParaListListaNova(row))

            Next

            Return fichas

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing

        End Try

    End Function

    Private Function ConverteParaObjetoListaNovaOi(ByVal dsRequisicao As DataSet) As List(Of CContribuinte)
        Dim row As DataRow
        Dim fichas As List(Of CContribuinte) = New List(Of CContribuinte)

        Try

            For Each row In dsRequisicao.Tables(0).Rows

                fichas.Add(ConverteParaListListaNovaOi(row))

            Next

            Return fichas

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing

        End Try

    End Function

    Private Function ConverteParaListListaNova(ByVal rwObjeto As DataRow) As CContribuinte
        Dim objRequisicao As CContribuinte = New CContribuinte()

        Try

            objRequisicao.Cod_contribuinte = IIf(IsDBNull(rwObjeto.Item("Cod").ToString()), "", rwObjeto.Item("Cod").ToString())
            objRequisicao.Nome_contribuinte = IIf(IsDBNull(rwObjeto.Item("Nome_cliente").ToString()), "", rwObjeto.Item("Nome_cliente").ToString())
            objRequisicao.NomeCliente1 = IIf(IsDBNull(rwObjeto.Item("Nome_cliente").ToString()), "", rwObjeto.Item("Nome_cliente").ToString())
            objRequisicao.NomeCliente2 = IIf(IsDBNull(rwObjeto.Item("Nome_cliente").ToString()), "", rwObjeto.Item("Nome_cliente").ToString())
            objRequisicao.Endereco = IIf(IsDBNull(rwObjeto.Item("endereco")), 0, rwObjeto.Item("endereco"))
            objRequisicao.Bairro = IIf(IsDBNull(rwObjeto.Item("bairro").ToString()), "", rwObjeto.Item("bairro").ToString())
            objRequisicao.Cidade = IIf(IsDBNull(rwObjeto.Item("Cidade").ToString()), "", rwObjeto.Item("Cidade").ToString())
            objRequisicao.UF = IIf(IsDBNull(rwObjeto.Item("uf").ToString()), "", rwObjeto.Item("uf").ToString())
            objRequisicao.CEP = IIf(IsDBNull(rwObjeto.Item("cep").ToString()), "", rwObjeto.Item("cep").ToString())
            objRequisicao.Telefone1 = IIf(IsDBNull(rwObjeto.Item("telefone1").ToString()), "", rwObjeto.Item("telefone1").ToString())
            objRequisicao.Cod_empresa = IIf(IsDBNull(rwObjeto.Item("Cod_empresa")), 0, rwObjeto.Item("Cod_empresa"))
            objRequisicao.Cod_Cidade = IIf(IsDBNull(rwObjeto.Item("Cod_Cidade")), 0, rwObjeto.Item("Cod_Cidade"))
            objRequisicao.Cod_Bairro = IIf(IsDBNull(rwObjeto.Item("Cod_Bairro")), 0, rwObjeto.Item("Cod_Bairro"))

            Return objRequisicao

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function

    Private Function ConverteParaListListaNovaOi(ByVal rwObjeto As DataRow) As CContribuinte
        Dim objRequisicao As CContribuinte = New CContribuinte()

        Try

            ' objRequisicao.Cod_contribuinte = IIf(IsDBNull(rwObjeto.Item("Cod").ToString()), "", rwObjeto.Item("Cod").ToString())
            objRequisicao.Nome_contribuinte = IIf(IsDBNull(rwObjeto.Item("Nome_cliente").ToString()), "", rwObjeto.Item("Nome_cliente").ToString())
            objRequisicao.NomeCliente1 = IIf(IsDBNull(rwObjeto.Item("Nome_cliente").ToString()), "", rwObjeto.Item("Nome_cliente").ToString())
            objRequisicao.NomeCliente2 = IIf(IsDBNull(rwObjeto.Item("Nome_cliente").ToString()), "", rwObjeto.Item("Nome_cliente").ToString())
            objRequisicao.Endereco = IIf(IsDBNull(rwObjeto.Item("endereco")), 0, rwObjeto.Item("endereco"))
            objRequisicao.Bairro = IIf(IsDBNull(rwObjeto.Item("bairro").ToString()), "", rwObjeto.Item("bairro").ToString())
            objRequisicao.Cidade = IIf(IsDBNull(rwObjeto.Item("Cidade").ToString()), "", rwObjeto.Item("Cidade").ToString())
            objRequisicao.UF = IIf(IsDBNull(rwObjeto.Item("uf").ToString()), "", rwObjeto.Item("uf").ToString())
            objRequisicao.CEP = IIf(IsDBNull(rwObjeto.Item("cep").ToString()), "", rwObjeto.Item("cep").ToString())
            objRequisicao.DDD = IIf(IsDBNull(rwObjeto.Item("DDD").ToString()), "", rwObjeto.Item("DDD").ToString())
            objRequisicao.Telefone1 = IIf(IsDBNull(rwObjeto.Item("telefone1").ToString()), "", rwObjeto.Item("telefone1").ToString())
            objRequisicao.Telefone2 = IIf(IsDBNull(rwObjeto.Item("telefone2").ToString()), "", rwObjeto.Item("telefone2").ToString())
            objRequisicao.Cod_empresa = IIf(IsDBNull(rwObjeto.Item("Cod_empresa")), 0, rwObjeto.Item("Cod_empresa"))
            objRequisicao.Cod_Cidade = IIf(IsDBNull(rwObjeto.Item("Cod_Cidade")), 0, rwObjeto.Item("Cod_Cidade"))
            objRequisicao.Cod_Bairro = IIf(IsDBNull(rwObjeto.Item("Cod_Bairro")), 0, rwObjeto.Item("Cod_Bairro"))
            objRequisicao.ClienteOi = IIf(IsDBNull(rwObjeto.Item("ClienteOi")), 0, rwObjeto.Item("ClienteOi"))
            objRequisicao.Cobrador = IIf(IsDBNull(rwObjeto.Item("Identificacao")), "", rwObjeto.Item("Identificacao"))
            objRequisicao.CNPJ_CPF = IIf(IsDBNull(rwObjeto.Item("cpf")), "", rwObjeto.Item("cpf"))

            Return objRequisicao

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function

    Public Function IncluiRegistroListaNovaRequisicaoLista(ByVal contribuinte As CContribuinte, ByVal cidade As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.IncluiRegistroListaNovaRequisicaoLista(contribuinte, cidade)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function CarregaFichasListaNovaOi(uf As String) As List(Of CContribuinte)
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Try

            Return ConverteParaObjetoListaNovaOi(daoRequisicao.CarregaListaNovaOi(uf))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function IncluiRegistroListaNovaRequisicaoOi(ByVal contribuinte As CContribuinte) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.IncluiRegistroListaNovaRequisicaoOi(contribuinte)


        Catch ex As Exception
            'MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function IncluiRegistroListaNovaRequisicaoCemig(contribuinte As CContribuinte,
                                                            numeroInstalacao As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.IncluiRegistroListaNovaRequisicaoCemig(contribuinte, numeroInstalacao)


        Catch ex As Exception
            'MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    'Public Function EnriquecimentoBase(ByVal ddd As String, ByVal telefone As String) As Boolean
    '    Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
    '    Try

    '        Return daoRequisicao.IncluiRegistroListaNovaRequisicaoOi(ddd, telefone)


    '    Catch ex As Exception
    '        MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
    '        Return False
    '    End Try
    'End Function

    Public Function VerificaInclusaoCliente(DDD As String,
                                            telefone As String,
                                            ByRef cod_ean_existente As String,
                                            ByRef cod_cliente_existente As String,
                                            isOi As Boolean) As Boolean
        Try
            Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
            Return daoRequisicao.VerificaInclusaoCliente(DDD, telefone, cod_ean_existente, cod_cliente_existente, isOi)

        Catch ex As Exception
            'MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function VerificaInclusaoRequisicaoLista(cod_ean_cliente As String, integracao As String) As Boolean
        Try
            Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
            Return daoRequisicao.VerificaInclusaoRequisicaLista(cod_ean_cliente, integracao)
        Catch ex As Exception
            'MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function


    Public Function RetornaCodCidade(ByVal cidade As String) As Integer
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.RetornaCodCidade(cidade)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function ExclusaoOutraOperadora(ByVal DDD As String, ByVal telefone As String, ByVal cod_ean As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.ExclusaoOutraOperadora(DDD, telefone, cod_ean)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function


    Public Function UpdateOutraOperadora(ByVal DDD As String, ByVal telefone As String, ByVal cod_ean As String) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
        Try

            Return daoRequisicao.UpdateOutraOperadora(DDD, telefone, cod_ean)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function CarregaFichasExportacaoOi() As DataSet
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Return daoRequisicao.CarregaFichasExportacaoOi()


    End Function

    Public Function AtualizaFichasExportacaoOi() As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Return daoRequisicao.AtualizaFichasExportacaoOi()


    End Function

    Public Function ImportaParaRequisicaoOi(ByVal dt As DataTable) As Boolean
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Return daoRequisicao.ImportaParaRequisicaoOi(dt)


    End Function

#End Region

#Region "cemig"

    Public Function CarregaHistoricoContribuinteCemig(ByVal cod_ean As String) As List(Of CContribuinte)
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Return ConverteParaObjetoHistorico(daoRequisicao.CarregaHistoricoContribuinteCemig(cod_ean))


    End Function

    Public Function RetornaMovimentos(movimento As String, novaInclusao As String) As List(Of RemessaCemig)
        Try
            Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO
            Return ConverteParaRemessaCemig(daoRequisicao.RetornaMovimento(movimento, novaInclusao))
        Catch ex As Exception

        End Try

    End Function


    Private Function ConverteParaRemessaCemig(dsRequisicao As DataSet) As List(Of RemessaCemig)
        Try
            Dim row As DataRow
            Dim fichas As List(Of RemessaCemig) = New List(Of RemessaCemig)
            For Each row In dsRequisicao.Tables(0).Rows
                fichas.Add(ConverteParaListRemessaCemig(row))
            Next
            Return fichas
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Private Function ConverteParaListRemessaCemig(rwObjeto As DataRow) As RemessaCemig
        Try
            Dim cemig As New RemessaCemig With {
                .ID = IIf(IsDBNull(rwObjeto.Item("ID").ToString()), "", rwObjeto.Item("ID").ToString()),
                .Cod_EAN_Cliente = IIf(IsDBNull(rwObjeto.Item("Cod Ean Cliente").ToString()), "", rwObjeto.Item("Cod Ean Cliente").ToString()),
                .DDD = IIf(IsDBNull(rwObjeto.Item("DDD").ToString()), "", rwObjeto.Item("DDD").ToString()),
                .Telefone = (IIf(IsDBNull(rwObjeto.Item("Telefone").ToString()), "", rwObjeto.Item("Telefone").ToString())),
                .NomeCliente = (IIf(IsDBNull(rwObjeto.Item("Nome").ToString()), "", rwObjeto.Item("Nome").ToString())),
                .CPF = IIf(IsDBNull(rwObjeto.Item("CPF").ToString()), "", rwObjeto.Item("CPF").ToString()),
                .Autorizante = IIf(IsDBNull(rwObjeto.Item("Autorizante").ToString()), "", rwObjeto.Item("Autorizante").ToString()),
                .Valor = IIf(IsDBNull(rwObjeto.Item("Valor").ToString()), "", rwObjeto.Item("Valor").ToString()),
                .NumeroInstalacao = IIf(IsDBNull(rwObjeto.Item("Número de Instalação").ToString()), "", rwObjeto.Item("Número de Instalação").ToString()),
                .MesEnvio = IIf(IsDBNull(rwObjeto.Item("Mês de envio").ToString()), "", rwObjeto.Item("Mês de envio").ToString())
            }
            Return cemig

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaNumeroRemessa() As Integer
        Try
            Dim dao As New RequisicaoDAO()
            Return dao.RetornaNumeroRemessa()
        Catch ex As Exception

        End Try
    End Function

    Public Function GeraHeader(numeroRemessa As Integer) As String
        Try
            '03462' +  -- CÓDIGO DA EMPRESA                                                                           
            'REPLICATE(' ', 15 ) + -- AUTORIZAÇÃO DE DÉBITO
            '00' + --MOVIMENTO
            '00' + --SUBTIPO
            'Trim('PR/AA - 3462/') + REPLICATE(' ', 13 - LEN('PR/AA - 3462/')) + --NRO. DO CONTRATO
            'Replace(Convert(VARCHAR(10), getdate(), 103),'/','') +  -- DATA DE TRANSMISSÃO
            'REPLICATE('0', 6 - LEN	('2')) + '2' + --NUMERO SEQUENCIAL                                   
            'REPLICATE(' ', 4)+ -- VERSÃO                                  
            'REPLICATE(' ', 20 )--BRANCOS      
            Dim codEmpresa As String = "03462"
            Dim autorizacaoDebito As String = String.Empty
            Dim movimento As String = "00"
            Dim subtipo As String = "00"
            Dim numContrato As String = "PR/AA - 3462/"
            Dim dataTransmissao As String = Format(Now(), "ddMMyyyy")
            Dim versao As String = String.Empty
            Dim espacoBranco As String = String.Empty
            Return $"{codEmpresa}{autorizacaoDebito.PadRight(15, " ")}{movimento}{subtipo}{numContrato}{dataTransmissao}{ _
                    numeroRemessa.ToString.PadLeft(6, "0")}{versao.PadRight(4, " ")}{espacoBranco.PadRight(20, " ")}"
        Catch ex As Exception

        End Try
    End Function

    Public Function GeraAutorizacaoDebito(id As String, movimento As String) As String
        Try
            Dim dao As New RequisicaoDAO
            Return dao.RetornaAutorizacaoDebito(id, movimento)
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return String.Empty
        End Try
    End Function

    Public Function GeraItemDebito(id As String, datacobranca As String, movimento As String) As String
        Try
            Dim dao As New RequisicaoDAO
            Return dao.RetornaItemDebito(id, datacobranca, movimento)
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return String.Empty
        End Try
    End Function


    Public Function GeraGrupoParcelas(id As String, movimento As String) As String
        Try
            Dim dao As New RequisicaoDAO
            Return dao.RetornaGrupoParcelas(id, movimento)
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return String.Empty
        End Try
    End Function

    Public Function GeraTrailler(numeroLinhas As String) As String
        Try

            '   '03462' +  -- CÓDIGO DA EMPRESA                                                                           
            '   REPLICATE('9', 15 ) + -- AUTORIZAÇÃO DE DÉBITO
            ''99' + --MOVIMENTO
            ''99' + --SUBTIPO 
            'REPLICATE('0', 15 - LEN(2)) + RTrim(COUNT(*)) +   --NUMERO DE REGISTROS
            '   REPLICATE(' ', 33 )--BRANCOS     
            Dim codEmpresa As String = "03462"
            Dim autorizacaoDebito As String = String.Empty
            Dim movimento As String = "99"
            Dim subtipo As String = "99"
            Dim espacoBranco As String = String.Empty
            Return $"{codEmpresa}{autorizacaoDebito.PadRight(15, "9")}{movimento}{subtipo}{ _
                numeroLinhas.PadLeft(15, "0")}{espacoBranco.PadRight(33, " ")}"
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        Return String.Empty
        End Try
    End Function

    Public Function AtualizaIDProcessado(ID As Integer) As Boolean
        Try
            Dim dao As New RequisicaoDAO
            Return dao.AtualizaRegistroProcessado(ID)
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Function

    Public Function InsereRemessaProcessada(movimento As Integer,
                                            versao As Integer,
                                            qtdRegistros As Integer,
                                            qtdLinhas As Integer) As String
        Try
            Dim dao As New RequisicaoDAO
            Return dao.InsereControleProcessamento(movimento, versao, qtdRegistros, qtdLinhas)
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function


#End Region





End Class
