﻿Imports System.Text.RegularExpressions


Public Class RNRetorno

#Region "Variáveis"

    Private daoRetorno As RetornoDAO

#End Region

#Region "Construtores"

    Sub New()

        daoRetorno = New RetornoDAO()

    End Sub

#End Region


    Public Function LeituraArquivo(ByVal strLinha As String) As CRetorno

        'Declara as variáveis dos campos
        Dim retorno As CRetorno = New CRetorno
        Dim infRetorno As CRetorno = New CRetorno

        'variaveis para tratar o retorno
        Dim dia As String
        Dim mes As String
        Dim ano As String

        Try

            'Recupera os valores de acordo com o posicionamento


            If strLinha.Substring(0, 1) = "1" Then
                '******HEADER
                retorno.UsTipoRegistroH = strLinha.Substring(0, 1)

                retorno.DdDataGeracao = strLinha.Substring(1, 8)
                dia = retorno.DdDataGeracao.Substring(6, 2)
                mes = retorno.DdDataGeracao.Substring(4, 2)
                ano = retorno.DdDataGeracao.Substring(0, 4)
                retorno.DdDataGeracao = CStr(dia + "/" + mes + "/" + ano)

                retorno.EmpresaTerceiro = strLinha.Substring(9, 5)
                retorno.EmpresaTelemar = strLinha.Substring(14, 5)

            ElseIf strLinha.Substring(0, 1) = "2" Then
                '******DETALHE
                retorno.UsTipoRegistroD = strLinha.Substring(0, 1)
                retorno.UiCodServicoArbor = strLinha.Substring(4, 2)
                retorno.UiCodLocalidadeCobranca = strLinha.Substring(6, 5)
                retorno.StDDDMeioAcessoCobranca = strLinha.Substring(11, 2)
                retorno.StNumMeioAcessoCobranca = strLinha.Substring(13, 10)
                retorno.DVlrServico = CDec(strLinha.Substring(23, 9)).ToString("R$ #,###.00") ' tamanho real 11 ajustado para 9 para tirar os dois ultimos zeros


                retorno.DdDataServico = strLinha.Substring(34, 8)
                dia = retorno.DdDataServico.Substring(6, 2)
                mes = retorno.DdDataServico.Substring(4, 2)
                ano = retorno.DdDataServico.Substring(0, 4)
                retorno.DdDataServico = CStr(dia + "/" + mes + "/" + ano)

                retorno.UsNumParcela = strLinha.Substring(42, 3)
                retorno.UsQteParcela = strLinha.Substring(45, 3)

                retorno.UiDataInicioCobranca = strLinha.Substring(48, 6)
                mes = retorno.UiDataInicioCobranca.Substring(0, 2)
                ano = retorno.UiDataInicioCobranca.Substring(2, 4)
                retorno.UiDataInicioCobranca = CStr(mes + "/" + ano)


                retorno.UcSinal = strLinha.Substring(54, 1)

                retorno.UiAnoMesConta = strLinha.Substring(55, 6)
                mes = retorno.UiAnoMesConta.Substring(0, 2)
                ano = retorno.UiAnoMesConta.Substring(2, 4)
                retorno.UiAnoMesConta = CStr(mes + "/" + ano)

                retorno.StDescReferencia = strLinha.Substring(61, 10)
                If RTrim(LTrim(retorno.StDescReferencia)) = "" Then
                    retorno.StDescReferencia = daoRetorno.RetornaCodEAN(CStr(CInt(retorno.StNumMeioAcessoCobranca)))
                End If


                retorno.DNumNotaFiscal = strLinha.Substring(71, 13)

                retorno.DdDataVencimento = strLinha.Substring(84, 8)
                dia = retorno.DdDataVencimento.Substring(6, 2)
                mes = retorno.DdDataVencimento.Substring(4, 2)
                ano = retorno.DdDataVencimento.Substring(0, 4)
                retorno.DdDataVencimento = CStr(dia + "/" + mes + "/" + ano)

                retorno.DdDataSituacao = strLinha.Substring(92, 8)
                dia = retorno.DdDataSituacao.Substring(6, 2)
                mes = retorno.DdDataSituacao.Substring(4, 2)
                ano = retorno.DdDataSituacao.Substring(0, 4)
                retorno.DdDataSituacao = CStr(dia + "/" + mes + "/" + ano)

                retorno.DVlrFaturado = CDec(strLinha.Substring(100, 9)).ToString("R$ #,###.00") ' tamanho real 11 ajustado para 9 para tirar os dois ultimos zeros
                retorno.StCodigoMovimentacao = strLinha.Substring(111, 2)



                retorno.UsSitFaturamento = strLinha.Substring(113, 2)

                If retorno.UsSitFaturamento = "PG" Then
                    retorno.UiMotivoSituacao = retorno.UsSitFaturamento
                Else
                    retorno.UsSitFaturamento = daoRetorno.RetornaSituacao(CInt(retorno.UsSitFaturamento))
                End If
                If retorno.UsSitFaturamento = "criticado" Then
                    retorno.UiMotivoSituacao = daoRetorno.RetornaNomeCritica(strLinha.Substring(115, 6))
                Else
                    If retorno.UiMotivoSituacao = "" Then
                        retorno.UiMotivoSituacao = "Sem crítica"
                    End If
                End If

                retorno.UiAnoMesContaMulta = strLinha.Substring(121, 6)
                mes = retorno.UiAnoMesContaMulta.Substring(4, 2)
                ano = retorno.UiAnoMesContaMulta.Substring(0, 4)
                retorno.UiAnoMesContaMulta = CStr(mes + "/" + ano)

                retorno.StNumAssinanteMovel = strLinha.Substring(127, 11)
                retorno.SistemaFaturamento = strLinha.Substring(138, 6)
                retorno.Ciclo = strLinha.Substring(144, 3)

            ElseIf strLinha.Substring(0, 1) = "3" Then
                '******TRAILLER
                retorno.UsTipoRegistroT = strLinha.Substring(0, 1)
                retorno.QuantidadeDeRegistro = strLinha.Substring(1, 9)
                retorno.ValorArquivo = CDec(strLinha.Substring(10, 13)).ToString("R$ #,###.00")

            End If


            Return retorno

        Catch ex As Exception

            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing

        End Try

    End Function

    Public Function GravaDadosArquivo(ByVal Cretorno As CRetorno, ByVal nomeArq As String, ByVal usuario As String, ByVal id_arq As Integer) As Boolean

        Try

            If daoRetorno.Salvar(Cretorno, nomeArq, usuario, id_arq) = True Then
                If Not Cretorno.UsTipoRegistroH = 1 Then
                    If Not Cretorno.UsTipoRegistroH = 3 Then
                        daoRetorno.SalvarMovimentacao(Cretorno, nomeArq, usuario)
                        Return True
                    End If
                    Return True
                End If
                Return True

            Else
                Return False
            End If



        Catch ex As Exception

            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function IsArquivoProcessado(ByVal nomeArq As String) As Boolean

        Try

            If daoRetorno.IsArquivoProcessado(nomeArq) = True Then
                Return True
            Else
                Return False
            End If



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function ApagaArquivoProcessado(ByVal nomeArq As String) As Boolean

        Try

            If daoRetorno.ApagaArquivoProcessado(nomeArq) = True Then
                Return True
            Else
                Return False
            End If



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function RetornaIdArquivo() As Integer
        Dim id_arq As Integer
        Try

            id_arq = daoRetorno.RetornaIDArquivo()

            Return id_arq


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function RetornaUltimaDataSituacao(ByVal telefone As String) As String
        Dim Data As String = ""
        Try

            Data = daoRetorno.RetornaDataUltimaSituacao(telefone)

            Return Data


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function RetornaUltimaSituacao(ByVal telefone As String) As String
        Dim Situacao As String = ""
        Try

            Situacao = daoRetorno.RetornaUltimaSituacao(telefone)

            Return Situacao


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function


    Public Function RetornaAFaturar(ByVal MesAnoRef As String) As String
        Dim Situacao As String = ""
        Try

            Situacao = daoRetorno.RetornaAFaturar(MesAnoRef)

            Return Situacao


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function RetornaFaturado(ByVal MesAnoRef As String) As String
        Dim Situacao As String = ""
        Try

            Situacao = daoRetorno.RetornaFaturado(MesAnoRef)

            Return Situacao


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function RetornaCriticado(ByVal MesAnoRef As String) As String
        Dim Situacao As String = ""
        Try

            Situacao = daoRetorno.RetornaCriticado(MesAnoRef)

            Return Situacao


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function RetornaArrecadado(ByVal MesAnoRef As String) As String
        Dim Situacao As String = ""
        Try

            Situacao = daoRetorno.RetornaArrecadado(MesAnoRef)

            Return Situacao


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function RetornaAltConta(ByVal MesAnoRef As String) As String
        Dim Situacao As String = ""
        Try

            Situacao = daoRetorno.RetornaAltConta(MesAnoRef)

            Return Situacao


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function RetornaContestado(ByVal MesAnoRef As String) As String
        Dim Situacao As String = ""
        Try

            Situacao = daoRetorno.RetornaContestado(MesAnoRef)

            Return Situacao


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function DataRemessa() As String
        Dim data As String = ""
        Try

            data = daoRetorno.DataRemessa()

            Return data


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function CarregarContribuintesPelaSituacao(ByVal situacao As String, ByVal mesAno As String, ByVal form As String) As List(Of CRetorno)

        Return ConverteParaObjetoContribuintesSituacaoOi(daoRetorno.CarregarGridContribuintesOiPorSituacao(situacao, mesAno), form)


    End Function

    Public Function CarregarFeedContribuintesPelaSituacao(ByVal situacao As String, ByVal form As String) As List(Of CRetorno)

        Return ConverteParaObjetoContribuintesSituacaoOi(daoRetorno.CarregarFeedGridContribuintesOiPorSituacao(situacao), form)


    End Function

    Public Function CarregarFeedContribuintesPelaCategoria(ByVal situacao As String, ByVal mesAno As String, ByVal codUsuario As Integer, ByVal form As String) As List(Of CRetorno)

        Return ConverteParaObjetoContribuintesCategoriaOi(daoRetorno.CarregarFeedGridContribuintesOiPorCategoria(situacao, mesAno), form)


    End Function

    Public Function CarregarFeedContribuintesPelaCategoriaNovo(ByVal situacao As String, ByVal operador_id As Integer, ByVal codUsuario As Integer, ByVal form As String) As List(Of CRetorno)

        Return ConverteParaObjetoContribuintesCategoriaOi(daoRetorno.CarregarFeedGridContribuintesOiPorCategoriaNovo(situacao, operador_id, codUsuario), form)


    End Function

    Public Function CarregarFeedRetornaRequisicao(ByVal situacao As String, ByVal mesAno As String, ByVal codUsuario As Integer, ByVal form As String, ByVal tipoPesquisa As Integer) As List(Of CRetorno) '0 =  categoria, 1 =  operador

        Return ConverteParaObjetoContribuintesCategoriaOi(daoRetorno.CarregarFeedGridRetornaRequisicao(situacao, mesAno, codUsuario, tipoPesquisa), form)


    End Function

    Public Function CarregarFeedRetornaRequisicaoCemig(ByVal situacao As String, ByVal mesAno As String, ByVal codUsuario As Integer, ByVal form As String, ByVal tipoPesquisa As Integer) As List(Of CRetorno) '0 =  categoria, 1 =  operador

        Return ConverteParaObjetoContribuintesCategoriaOi(daoRetorno.CarregarFeedGridRetornaRequisicaoCemig(situacao, mesAno, codUsuario, tipoPesquisa), form)


    End Function

    Public Function RetornaDadosPorCategoria(ByVal categoria As Integer, ByVal mesAno As String) As DataSet

        Return daoRetorno.RetornaDadosPorCategoria(categoria, mesAno)

    End Function

    Public Function RetornaDadosPorCategoriaListaNovaOi(ByVal categoria As Integer, ByVal mesAno As String, ByVal cidade As String) As DataSet

        Return daoRetorno.RetornaDadosPorCategoriaListaNovaOi(categoria, mesAno, cidade)

    End Function

    Public Function RetornaDadosPorCategoriaManual(ByVal categoria As Integer, ByVal cidade As String, ByVal dia As String) As DataSet

        Return daoRetorno.RetornaDadosPorCategoriaManual(categoria, cidade, dia)

    End Function

    Public Function RetornaDadosPorCategoriaCemig(ByVal categoria As Integer, ByVal cidade As String, ByVal dia As String) As DataSet

        Return daoRetorno.RetornaDadosPorCategoriaCemig(categoria, cidade, dia)

    End Function

    Public Function RetornaFichasDisponiveisManual(ByVal categoria As Integer, ByVal cidade As String, ByVal dia As String) As Integer

        Return daoRetorno.RetornaFichasDisponiveisManual(categoria, cidade, dia)

    End Function

    Public Function RetornaFichasDisponiveisCemig(ByVal categoria As Integer, ByVal cidade As String, ByVal dia As String) As Integer

        Return daoRetorno.RetornaFichasDisponiveisCemig(categoria, cidade, dia)

    End Function

    Public Function RetornaFichasDisponiveis(ByVal categoria As Integer) As Integer

        Return daoRetorno.RetornaFichasDisponiveis(categoria)

    End Function

    Public Function RetornaFichasBloqueadas(ByVal categoria As Integer) As Integer

        Return daoRetorno.RetornaFichasBloqueadas(categoria)

    End Function

    Public Function AtualizaImpressaoFichas() As Integer

        Return daoRetorno.AtualizaImpressaoFichas()

    End Function

    Public Function RetornaFichasDisponiveisListaNovaOi(ByVal categoria As Integer, ByVal cidade As String) As Integer

        Return daoRetorno.RetornaFichasDisponiveisListaNova(categoria, cidade)

    End Function


    Public Function CarregarTotaisContribuintesPelaCategoria(ByVal situacao As String, ByVal mesAno As String, ByVal codUsuario As Integer) As DataSet

        Return daoRetorno.CarregarFeedGridContribuintesOiPorCategoria(situacao, mesAno)


    End Function

    Public Function CarregarTotaisContribuintesPelaCategoriaNovo(ByVal situacao As String, ByVal mesAno As String, ByVal codUsuario As Integer) As DataSet

        Return daoRetorno.CarregarFeedGridContribuintesOiPorCategoriaNovo(situacao, mesAno, codUsuario)


    End Function
    Public Function CarregarTotaisRetornoRequisicao(ByVal situacao As String, ByVal mesAno As String, ByVal codUsuario As Integer, ByVal tipoPesquisa As Integer) As DataSet '0 =  categoria, 1 =  operador


        Return daoRetorno.CarregarFeedGridRetornaRequisicao(situacao, mesAno, codUsuario, tipoPesquisa)


    End Function

    Public Function RetornaDadosCSVOi(categoria As Integer) As DataSet

        Return daoRetorno.RetornaDadosCSVOi(categoria)

    End Function

    Public Function RetornaDadosCSVManual(categoria As Integer) As DataSet

        Return daoRetorno.RetornaDadosCSVManual(categoria)

    End Function


    Private Function ConverteParaObjetoContribuintesSituacaoOi(ByVal dsContribuinte As DataSet, ByVal form As String) As List(Of CRetorno)

        Try

            Dim row As DataRow
            Dim contribuintesOi As List(Of CRetorno) = New List(Of CRetorno)

            If Not (dsContribuinte Is Nothing) Then

                For Each row In dsContribuinte.Tables(0).Rows

                    contribuintesOi.Add(ConverteParaListContribuinteSituacaoOI(row))

                Next

            End If

            Return contribuintesOi

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Private Function ConverteParaObjetoContribuintesPorSituacaoOi(ByVal dsContribuinte As DataSet, ByVal form As String) As List(Of CRetorno)

        Try

            Dim row As DataRow
            Dim contribuintesOi As List(Of CRetorno) = New List(Of CRetorno)

            If Not (dsContribuinte Is Nothing) Then

                For Each row In dsContribuinte.Tables(0).Rows

                    contribuintesOi.Add(ConverteParaListContribuintePorSituacaoOI(row))

                Next
            End If

            Return contribuintesOi

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Private Function ConverteParaObjetoContribuintesCategoriaOi(ByVal dsContribuinte As DataSet, ByVal form As String) As List(Of CRetorno)

        Try

            Dim row As DataRow
            Dim contribuintesOi As List(Of CRetorno) = New List(Of CRetorno)

            If Not (dsContribuinte Is Nothing) Then

                For Each row In dsContribuinte.Tables(0).Rows

                    contribuintesOi.Add(ConverteParaListContribuinteCategoriaOI(row))

                Next

            End If

            Return contribuintesOi

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Private Function ConverteParaObjetoArquivo(ByVal dsArquivo As DataSet, ByVal form As String) As List(Of CRetorno)

        Try

            Dim row As DataRow
            Dim arquivos As List(Of CRetorno) = New List(Of CRetorno)

            arquivos.Add(ConvertePrimeiraLinhaArquivo())

            If Not (dsArquivo Is Nothing) Then

                For Each row In dsArquivo.Tables(0).Rows

                    arquivos.Add(ConverteParaListArquivos(row))

                Next

            End If

            Return arquivos

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Private Function ConvertePrimeiraLinhaRetorno() As CRetorno


        Dim objRetorno As CRetorno = New CRetorno()


        objRetorno.Cod_cliente = "-1"
        objRetorno.Nome_cliente = "Selecione..."


        Return objRetorno

    End Function

    Private Function ConvertePrimeiraLinhaArquivo() As CRetorno


        Dim objRetorno As CRetorno = New CRetorno()


        objRetorno.Id_arquivo = "-1"
        objRetorno.Nome_Arquivo = "Selecione..."


        Return objRetorno

    End Function

    Private Function ConverteParaListContribuinteSituacaoOI(ByVal rwObjeto As DataRow) As CRetorno

        Try


            Dim objRetorno As CRetorno = New CRetorno()


            objRetorno.StDDDMeioAcessoCobranca = rwObjeto.Item("StDDDMeioAcessoCobranca").ToString()
            objRetorno.StNumMeioAcessoCobranca = rwObjeto.Item("StNumMeioAcessoCobranca").ToString()
            objRetorno.DVlrServico = rwObjeto.Item("DVlrServico").ToString()
            objRetorno.StDescReferencia = rwObjeto.Item("cod_ean_cliente").ToString()
            objRetorno.Cod_cliente = IIf(IsDBNull(rwObjeto.Item("cod_cliente").ToString()), "", rwObjeto.Item("cod_cliente").ToString())
            objRetorno.Cod_ean_cliente = IIf(IsDBNull(rwObjeto.Item("cod_ean_cliente").ToString()), "", rwObjeto.Item("cod_ean_cliente").ToString())
            objRetorno.Nome_cliente = IIf(IsDBNull(rwObjeto.Item("nome_cliente").ToString()), "", rwObjeto.Item("nome_cliente").ToString())
            objRetorno.UsSitFaturamento = IIf(IsDBNull(rwObjeto.Item("UsSitFaturamento").ToString()), "", rwObjeto.Item("UsSitFaturamento").ToString())
            objRetorno.UiMotivoSituacao = IIf(IsDBNull(rwObjeto.Item("UiMotivoSituacao").ToString()), "", rwObjeto.Item("UiMotivoSituacao").ToString())


            Return objRetorno

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function

    Private Function ConverteParaListContribuinteCategoriaOI(ByVal rwObjeto As DataRow) As CRetorno

        Try


            Dim objRetorno As CRetorno = New CRetorno()


            objRetorno.StDDDMeioAcessoCobranca = rwObjeto.Item("DDD").ToString()
            objRetorno.StNumMeioAcessoCobranca = rwObjeto.Item("telefone").ToString()
            objRetorno.DVlrServico = rwObjeto.Item("valor").ToString()
            objRetorno.Cod_ean_cliente = IIf(IsDBNull(rwObjeto.Item("cod_ean").ToString()), "", rwObjeto.Item("cod_ean").ToString())
            objRetorno.Nome_cliente = IIf(IsDBNull(rwObjeto.Item("nome").ToString()), "", rwObjeto.Item("nome").ToString())
            objRetorno.UsSitFaturamento = IIf(IsDBNull(rwObjeto.Item("observacao").ToString()), "", rwObjeto.Item("observacao").ToString())
            objRetorno.EmpresaTerceiro = IIf(IsDBNull(rwObjeto.Item("cod_empresa").ToString()), "", rwObjeto.Item("cod_empresa").ToString())
            objRetorno.UltimoAceite = IIf(IsDBNull(rwObjeto.Item("ultimo_aceite").ToString()), "", rwObjeto.Item("ultimo_aceite").ToString())


            Return objRetorno

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function

    Private Function ConverteParaListContribuintePorSituacaoOI(ByVal rwObjeto As DataRow) As CRetorno

        Try


            Dim objRetorno As CRetorno = New CRetorno()


            objRetorno.StDDDMeioAcessoCobranca = rwObjeto.Item("DDD").ToString()
            objRetorno.StNumMeioAcessoCobranca = rwObjeto.Item("telefone").ToString()
            objRetorno.DVlrServico = rwObjeto.Item("valor").ToString()
            objRetorno.Cod_ean_cliente = IIf(IsDBNull(rwObjeto.Item("Cod_ean_cliente").ToString()), "", rwObjeto.Item("Cod_ean_cliente").ToString())
            objRetorno.Nome_cliente = IIf(IsDBNull(rwObjeto.Item("nome_cliente").ToString()), "", rwObjeto.Item("nome_cliente").ToString())
            objRetorno.UsSitFaturamento = IIf(IsDBNull(rwObjeto.Item("UsSitFaturamento").ToString()), "", rwObjeto.Item("UsSitFaturamento").ToString())


            Return objRetorno

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function


    Private Function ConverteParaListArquivos(ByVal rwObjeto As DataRow) As CRetorno

        Try


            Dim objRetorno As CRetorno = New CRetorno()


            objRetorno.Nome_Arquivo = rwObjeto.Item("nome_Arquivo").ToString()
            objRetorno.Id_arquivo = rwObjeto.Item("id_arq").ToString()


            Return objRetorno

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function

    Public Function ConsultaArquivo(ByVal arquivo As String) As List(Of CRetorno)
        Dim daoRetorno As RetornoDAO = New RetornoDAO
        Dim retorno As CRetorno = New CRetorno
        Dim list As List(Of CRetorno) = New List(Of CRetorno)
        Dim ds As DataSet
        Try

            ds = daoRetorno.ConsultaArquivo(arquivo)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then

                    For Each linhas As DataRow In ds.Tables(0).Rows


                        '******DETALHE
                        retorno.UsTipoRegistroD = linhas.Item(6)
                        retorno.UiCodServicoArbor = linhas.Item(7)
                        retorno.UiCodLocalidadeCobranca = linhas.Item(8)
                        retorno.StDDDMeioAcessoCobranca = linhas.Item(9)
                        retorno.StNumMeioAcessoCobranca = linhas.Item(10)
                        retorno.DVlrServico = CDec(linhas.Item(11))


                        retorno.DdDataServico = linhas.Item(12)

                        retorno.UsNumParcela = linhas.Item(13)
                        retorno.UsQteParcela = linhas.Item(14)

                        retorno.UiDataInicioCobranca = linhas.Item(15)


                        retorno.UcSinal = linhas.Item(16)

                        retorno.UiAnoMesConta = linhas.Item(17)

                        retorno.StDescReferencia = linhas.Item(18)



                        retorno.DNumNotaFiscal = linhas.Item(19)

                        retorno.DdDataVencimento = linhas.Item(20)


                        retorno.DdDataSituacao = linhas.Item(21)


                        retorno.DVlrFaturado = CDec(linhas.Item(22))
                        retorno.StCodigoMovimentacao = linhas.Item(23)

                        retorno.UsSitFaturamento = linhas.Item(24)

                        retorno.UiMotivoSituacao = linhas.Item(25)


                        retorno.UiAnoMesContaMulta = linhas.Item(26)


                        retorno.StNumAssinanteMovel = linhas.Item(27)
                        retorno.SistemaFaturamento = linhas.Item(28)
                        retorno.Ciclo = linhas.Item(29)

                        list.Add(retorno)

                    Next



                    Return list
                Else
                    Return Nothing
                End If
            End If

            Return Nothing
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function BuscarArquivos() As List(Of CRetorno)
        Dim daoRetorno As RetornoDAO = New RetornoDAO

        Return ConverteParaObjetoArquivo(daoRetorno.RetornaArquivos(), "ConsultaArquivo")

    End Function

    Public Function retorna_valores_atuais(ByVal mesAno As String) As DataSet
        Dim retornoDao As RetornoDAO = New RetornoDAO
        Dim ds As DataSet = New DataSet

        Try

            If Not IsDBNull(retornoDao.retorna_valores_atuais(mesAno)) Then
                ds = retornoDao.retorna_valores_atuais(mesAno)
            End If


            If ((ds.Tables(0).Rows.Count > 0) Or (ds.Tables(1).Rows.Count > 0) Or
                (ds.Tables(2).Rows.Count > 0) Or (ds.Tables(3).Rows.Count > 0) Or
                (ds.Tables(4).Rows.Count > 0) Or (ds.Tables(5).Rows.Count > 0) Or
                (ds.Tables(6).Rows.Count > 0) Or (ds.Tables(7).Rows.Count > 0) Or
                (ds.Tables(8).Rows.Count > 0)) Then

                Return ds
            Else
                Return Nothing
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function retorna_Arrecadado(ByVal mesAno As String) As DataSet
        Dim retornoDao As RetornoDAO = New RetornoDAO
        Dim ds As DataSet = New DataSet

        Try
            ds = retornoDao.retorna_Arrecadado(mesAno)
            Return ds

        Catch ex As Exception
            Return Nothing
        End Try

    End Function



    Public Function RetornaUltimaSituacaoOi(ByVal telefone As String, ByVal ddd As String) As DataSet

        Try

            Return daoRetorno.RetornaUltimaSituacaoOi(telefone, ddd)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function IsArquivoRemessaProcessado(ByVal nome_arquivo As String) As Boolean

        Try

            If daoRetorno.IsArquivoRemessaProcessado(nome_arquivo) = True Then
                Return True
            Else
                Return False
            End If



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function ApagaArquivoRemessaProcessado(ByVal nomeArq As String) As Boolean

        Try

            If daoRetorno.ApagaArquivoRemessaProcessado(nomeArq) = True Then
                Return True
            Else
                Return False
            End If



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function LeituraArquivoRemessa(ByVal strLinha As String) As CRetorno

        'Declara as variáveis dos campos
        Dim retorno As CRetorno = New CRetorno
        Dim infRetorno As CRetorno = New CRetorno

        'variaveis para tratar o retorno
        Dim dia As String
        Dim mes As String
        Dim ano As String

        Try

            'Recupera os valores de acordo com o posicionamento


            If strLinha.Substring(0, 1) = "1" Then
                '******HEADER

                retorno.UsTipoRegistroH = "1"

                retorno.DdDataGeracao = strLinha.Substring(23, 8)
                dia = retorno.DdDataGeracao.Substring(0, 2)
                mes = retorno.DdDataGeracao.Substring(2, 2)
                ano = retorno.DdDataGeracao.Substring(4, 4)
                retorno.DdDataGeracao = CStr(dia + "/" + mes + "/" + ano)
                retorno.DdDataServico = CStr(ano + mes + dia)

            ElseIf strLinha.Substring(0, 1) = "2" Then
                '******DETALHE
                retorno.UsTipoRegistroH = "2"

                retorno.StDDDMeioAcessoCobranca = strLinha.Substring(9, 2)
                retorno.StNumMeioAcessoCobranca = strLinha.Substring(11, 10)
                retorno.DVlrServico = CDec(strLinha.Substring(22, 8)) ' tamanho real 11 ajustado para 9 para tirar os dois ultimos zeros

                retorno.DdDataServico = strLinha.Substring(32, 8)
                dia = retorno.DdDataServico.Substring(0, 2)
                mes = retorno.DdDataServico.Substring(2, 2)
                ano = retorno.DdDataServico.Substring(4, 4)
                retorno.DdDataGeracao = CStr(dia + "/" + mes + "/" + ano)
                retorno.DdDataServico = CStr(ano + mes + dia)

                retorno.StDescReferencia = strLinha.Substring(55, 8)

            ElseIf strLinha.Substring(0, 1) = "3" Then
                '******TRAILLER

                retorno.UsTipoRegistroH = "3"

                retorno.QuantidadeDeRegistro = strLinha.Substring(2, 6)
                retorno.ValorArquivo = CDec(strLinha.Substring(9, 10))

            End If


            Return retorno

        Catch ex As Exception

            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing

        End Try

    End Function

    Public Function GravaDadosArquivoRemessa(ByVal Cretorno As CRetorno, ByVal nomeArq As String, ByVal id_arq As Integer, ByVal UF As String) As Boolean

        Try

            If Cretorno.UsTipoRegistroH = 1 Then
                daoRetorno.SalvarDadosArquivoRemessa(Cretorno, nomeArq, 1, id_arq, UF)
            ElseIf Cretorno.UsTipoRegistroH = 2 Then
                daoRetorno.SalvarDadosArquivoRemessa(Cretorno, nomeArq, 2, id_arq, UF)
            ElseIf Cretorno.UsTipoRegistroH = 3 Then
                daoRetorno.SalvarDadosArquivoRemessa(Cretorno, nomeArq, 3, id_arq, UF)
            End If

            Return True



        Catch ex As Exception

            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function RetornaIdArquivoRemessa(ByVal nomeArq As String) As Integer
        Dim id_remessa As Integer

        Try

            id_remessa = daoRetorno.RetornaIdArquivoRemessa(nomeArq)

            Return id_remessa

        Catch ex As Exception

            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function ConsultaTelefoneEnviadoRemessa(ByVal ddd As String, ByVal telefone As String, ByVal dataRemessa As String) As Boolean
        Dim enviado As Boolean

        Try

            enviado = daoRetorno.ConsultaTelefoneEnviadoRemessa(ddd, telefone, dataRemessa)

            Return enviado

        Catch ex As Exception

            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function


#Region "Recuperacao"

    Public Function BuscaTelefonesRecuperacao() As List(Of CRetorno)

        Return ConverteParaObjetoLista(daoRetorno.Busca_Telefones_Recuperacao())

    End Function


    Private Function ConverteParaObjetoLista(ByVal dsGrupo As DataSet) As List(Of CRetorno)

        Dim row As DataRow
        Dim grupo As List(Of CRetorno) = New List(Of CRetorno)

        If Not (dsGrupo Is Nothing) Then

            For Each row In dsGrupo.Tables(0).Rows

                grupo.Add(ConverteParaListLista(row))

            Next

        End If

        Return grupo

    End Function

    Private Function ConverteParaListLista(ByVal rwObjeto As DataRow) As CRetorno

        Dim objLista As CRetorno = New CRetorno()

        objLista.StDDDMeioAcessoCobranca = IIf(IsDBNull(rwObjeto.Item("StDDDMeioAcessoCobranca").ToString()), "", rwObjeto.Item("StDDDMeioAcessoCobranca").ToString())
        objLista.StNumMeioAcessoCobranca = IIf(IsDBNull(rwObjeto.Item("StNumMeioAcessoCobranca").ToString()), "", rwObjeto.Item("StNumMeioAcessoCobranca").ToString())
        objLista.DVlrServico = IIf(IsDBNull(rwObjeto.Item("DVlrServico").ToString()), "", rwObjeto.Item("DVlrServico").ToString())
        objLista.DdDataServico = IIf(IsDBNull(rwObjeto.Item("DdDataServico").ToString()), "", rwObjeto.Item("DdDataServico").ToString())
        objLista.DdDataSituacao = IIf(IsDBNull(rwObjeto.Item("DdDataSituacao").ToString()), "", rwObjeto.Item("DdDataSituacao").ToString())
        objLista.StDescReferencia = IIf(IsDBNull(rwObjeto.Item("StDescReferencia").ToString()), "", rwObjeto.Item("StDescReferencia").ToString())
        objLista.UsSitFaturamento = IIf(IsDBNull(rwObjeto.Item("UsSitFaturamento").ToString()), "", rwObjeto.Item("UsSitFaturamento").ToString())
        objLista.UiMotivoSituacao = IIf(IsDBNull(rwObjeto.Item("UiMotivoSituacao").ToString()), "", rwObjeto.Item("UiMotivoSituacao").ToString())


        Return objLista

    End Function


    Public Function BuscaTotaisTelefonesRecuperacao() As DataSet
        Dim ds

        Try

            ds = daoRetorno.Busca_Telefones_Recuperacao()

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function CarregarFeedContribuintesRecuperacao(ByVal operador_id As Integer, ByVal codUsuario As Integer, ByVal form As String) As List(Of CRetorno)

        Return ConverteParaObjetoContribuintesCategoriaOi(daoRetorno.CarregarFeedGridRecuperacao(codUsuario), form)


    End Function


#End Region

#Region "Exportação/ Importação"

    Public Function LeituraArquivoImportacao(ByVal strLinha As String, ByVal tabela As String) As Boolean

        'Declara as variáveis dos campos
        Dim daoGenerico As GenericoDAO = New GenericoDAO

        Try

            'Recupera os valores de acordo com o posicionamento


            If strLinha.Substring(0, 1) = "|" Then

                Dim linha As String() = SepararPalavras(strLinha)

                If tabela = "Requisicao_oi_tb" Then
                    daoGenerico.SalvaImportacaoRequisicaoOi(linha)
                ElseIf tabela = "Requisicao_tb" Then
                    daoGenerico.SalvaImportacaoRequisicao(linha)
                ElseIf tabela = "lista_telefonica_tb" Then
                    daoGenerico.SalvaImportacaoListaTelefonica(linha)
                ElseIf tabela = "telefone_exclusao" Then
                    daoGenerico.SalvaImportacaoTelefoneExclusao(linha)
                End If


            End If

            Return True

        Catch ex As Exception

            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False

        End Try

    End Function

    Private Function SepararPalavras(ByRef texto As String) As String()
        'retorna a string de palavras separadas com base na expressão regular
        Dim expr_regex As String = ";"
        Return Regex.Split(texto, expr_regex)
    End Function


#End Region

#Region "CEMIG"

    Public Function RetornaDadosPorCategoriaCemig(ByVal categoria As Integer, ByVal mesAno As String) As DataSet

        Return daoRetorno.RetornaDadosPorCategoriaCemig(categoria, mesAno)

    End Function

    Public Function RetornaFichasDisponiveisCemig(ByVal categoria As Integer) As Integer

        Return daoRetorno.RetornaFichasDisponiveisCemig(categoria)

    End Function

    Public Function RetornaFichasBloqueadasCemig(ByVal categoria As Integer) As Integer

        Return daoRetorno.RetornaFichasBloqueadasCemig(categoria)

    End Function

    Public Function CarregarFeedContribuintesCemigPelaCategoriaNovo(ByVal situacao As String, ByVal codUsuario As Integer, ByVal form As String) As List(Of CRetorno)

        Return ConverteParaObjetoContribuintesCategoriaOi(daoRetorno.CarregarFeedGridContribuintesCemigPorCategoriaNovo(situacao, codUsuario), form)


    End Function

    Public Function CarregarTotaisContribuintesPelaCategoriaCemig(ByVal situacao As String, ByVal mesAno As String, ByVal codUsuario As Integer) As DataSet

        Return daoRetorno.CarregarFeedGridContribuintesCemigPorCategoria(situacao, mesAno)


    End Function

    Public Function RetornaDadosCSVCemig(categoria As Integer) As DataSet

        Return daoRetorno.RetornaDadosCSVCemig(categoria)

    End Function




#End Region

End Class
