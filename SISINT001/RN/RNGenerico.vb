﻿Public Class RNGenerico

#Region "Variáveis"

    Private daoGenerico As GenericoDAO

#End Region

#Region "Construtores"

    Sub New()

        daoGenerico = New GenericoDAO()

    End Sub

#End Region

#Region "Procedimentos"

#Region "Cidade"

    Public Function BuscarCidades(ByVal cod_cidade As Integer) As List(Of CGenerico)

        Return ConverteParaObjetoCidades(daoGenerico.BuscarCidades(cod_cidade))

    End Function

    Public Function BuscarCidadesCemig(ByVal cod_cidade As Integer) As List(Of CGenerico)

        Return ConverteParaObjetoCidades(daoGenerico.BuscarCidadesCemig(cod_cidade))

    End Function

    Private Function ConvertePrimeiraLinhaCidade() As CGenerico


        Dim objCidade As CGenerico = New CGenerico()


        objCidade.Cod_Cidade = "-1"
        objCidade.Nome_Cidade = "Selecione..."


        Return objCidade

    End Function

    Private Function ConverteParaObjetoCidades(ByVal dsCidades As DataSet) As List(Of CGenerico)

        Dim row As DataRow
        Dim cidades As List(Of CGenerico) = New List(Of CGenerico)

        cidades.Add(ConvertePrimeiraLinhaCidade())
        If dsCidades Is Nothing Then Return Nothing
        For Each row In dsCidades.Tables(0).Rows

            cidades.Add(ConverteParaListCidades(row))

        Next

        Return cidades

    End Function

    Private Function ConverteParaListCidades(ByVal rwObjeto As DataRow) As CGenerico

        Dim objCidades As CGenerico = New CGenerico()

        objCidades.Cod_Cidade = rwObjeto.Item("cod_cidade").ToString()
        objCidades.Nome_Cidade = rwObjeto.Item("nome_cidade").ToString()
        objCidades.Uf_Cidade = rwObjeto.Item("uf").ToString()


        Return objCidades

    End Function


#End Region

#Region "Bairro"

    Public Function BuscarBairros(ByVal _cod As Integer) As List(Of CGenerico)

        Return ConverteParaObjetoBairros(daoGenerico.BuscarBairros(_cod))

    End Function

    Private Function ConvertePrimeiraLinhaBairro() As CGenerico


        Dim objBairro As CGenerico = New CGenerico()


        objBairro.Cod_Bairro = "-1"
        objBairro.Nome_Bairro = "Selecione..."


        Return objBairro

    End Function

    Private Function ConverteParaObjetoBairros(ByVal dsBairros As DataSet) As List(Of CGenerico)

        Dim row As DataRow
        Dim bairros As List(Of CGenerico) = New List(Of CGenerico)

        bairros.Add(ConvertePrimeiraLinhaBairro())

        For Each row In dsBairros.Tables(0).Rows

            bairros.Add(ConverteParaListBairros(row))

        Next

        Return bairros

    End Function

    Private Function ConverteParaListBairros(ByVal rwObjeto As DataRow) As CGenerico

        Dim objBairros As CGenerico = New CGenerico()

        objBairros.Nome_Bairro = rwObjeto.Item("nome_bairro").ToString()
        objBairros.Cod_Bairro = rwObjeto.Item("cod_bairro").ToString()
        objBairros.Cod_Cidade_Bairro = rwObjeto.Item("cod_cidade").ToString()
        objBairros.Cod_Regiao_Bairro = rwObjeto.Item("cod_regiao").ToString()

        Return objBairros

    End Function
#End Region

#Region "Categoria"

    Public Function BuscarCategoria(ByVal cod_categoria As Integer) As List(Of CGenerico)

        Return ConverteParaObjetoCategorias(daoGenerico.BuscarCategorias(cod_categoria))

    End Function

    Public Function BuscarCategoriaPorOperador(ByVal operador_id As Integer) As List(Of CGenerico)

        Return ConverteParaObjetoCategorias(daoGenerico.BuscarCategoriasPorOperador(operador_id))

    End Function

    Public Function BuscarCategoriasPorRequisicaoOperador(ByVal operador_id As Integer) As List(Of CGenerico)

        Return ConverteParaObjetoCategorias(daoGenerico.BuscarCategoriasPorRequisicaoOperador(operador_id))

    End Function

    Public Function BuscarCategoriasPorRequisicaoCemigOperador(ByVal operador_id As Integer) As List(Of CGenerico)

        Return ConverteParaObjetoCategorias(daoGenerico.BuscarCategoriasPorRequisicaoCemigOperador(operador_id))

    End Function




    Private Function ConvertePrimeiraLinhaCategorias() As CGenerico


        Dim objCategorias As CGenerico = New CGenerico()


        objCategorias.Cod_Categoria = "-1"
        objCategorias.Nome_Categoria = "Selecione..."


        Return objCategorias

    End Function

    Private Function ConverteParaObjetoCategorias(ByVal dsCategorias As DataSet) As List(Of CGenerico)

        Dim row As DataRow
        Dim categorias As List(Of CGenerico) = New List(Of CGenerico)

        categorias.Add(ConvertePrimeiraLinhaCategorias())

        For Each row In dsCategorias.Tables(0).Rows

            categorias.Add(ConverteParaListCategorias(row))

        Next

        Return categorias

    End Function

    Private Function ConverteParaListCategorias(ByVal rwObjeto As DataRow) As CGenerico

        Dim objCategorias As CGenerico = New CGenerico()

        objCategorias.Nome_Categoria = rwObjeto.Item("nome_categoria").ToString()
        objCategorias.Cod_Categoria = rwObjeto.Item("cod_categoria").ToString()


        Return objCategorias

    End Function
#End Region

#Region "Operador"

    Public Function BuscarOperador(ByVal cod_operador As Integer) As List(Of CGenerico)

        Return ConverteParaObjetoOperador(daoGenerico.BuscarOperador(cod_operador))

    End Function

    'Public Function BuscarOperadorNome(ByVal operador As String) As List(Of CGenerico)

    '    Return ConverteParaObjetoOperador(daoGenerico.BuscarOperadorNome(operador))

    'End Function



    Public Function BuscarOperadorPeloNome(ByVal operador As String) As List(Of CGenerico)

        Return ConverteParaObjetoOperadorSemPrimeiraLinha(daoGenerico.BuscarOperadorPeloNome(operador))

    End Function

    Public Function BuscarOperadorDoMes() As List(Of CGenerico)

        Return ConverteParaObjetoOperador(daoGenerico.BuscarOperadorDoMes())

    End Function
    Public Function BuscarOperadorDoMesCemig() As List(Of CGenerico)

        Return ConverteParaObjetoOperador(daoGenerico.BuscarOperadorDoMesCemig())

    End Function

    Private Function ConvertePrimeiraLinhaOperador() As CGenerico


        Dim objOperador As CGenerico = New CGenerico()


        objOperador.Cod_Operador = "-1"
        objOperador.Nome_Operador = "Selecione..."


        Return objOperador

    End Function

    Private Function ConverteParaObjetoOperador(ByVal dsOperador As DataSet) As List(Of CGenerico)

        Dim row As DataRow
        Dim operador As List(Of CGenerico) = New List(Of CGenerico)

        operador.Add(ConvertePrimeiraLinhaOperador())

        For Each row In dsOperador.Tables(0).Rows

            operador.Add(ConverteParaListOperador(row))

        Next

        Return operador

    End Function

    Private Function ConverteParaObjetoOperadorSemPrimeiraLinha(ByVal dsOperador As DataSet) As List(Of CGenerico)

        Dim row As DataRow
        Dim operador As List(Of CGenerico) = New List(Of CGenerico)

        For Each row In dsOperador.Tables(0).Rows

            operador.Add(ConverteParaListOperador(row))

        Next

        Return operador

    End Function

    Private Function ConverteParaListOperador(ByVal rwObjeto As DataRow) As CGenerico

        Dim objOperador As CGenerico = New CGenerico()

        objOperador.Nome_Operador = rwObjeto.Item("nome_funcionario").ToString()
        objOperador.Cod_Operador = rwObjeto.Item("cod_ean_funcionario").ToString()


        Return objOperador

    End Function

    Public Function BuscarOperadorDigital(ByVal cod_operador As Integer) As List(Of CGenerico)

        Return ConverteParaObjetoOperador(daoGenerico.BuscarOperadorDigital(cod_operador))

    End Function

#End Region

#Region "Usuario"


    Public Function BuscarUsuario(ByVal cod_operador As Integer) As List(Of CGenerico)

        Return ConverteParaObjetoUsuario(daoGenerico.BuscarUsuarios(cod_operador))

    End Function

    Public Function BuscarOperadorSemana(ByVal dt_inicial As String, ByVal dt_final As String) As List(Of String)
        Dim ds As New DataSet
        Dim dt As DataTable
        Dim listaOperador As New List(Of String)

        Try

            ds = daoGenerico.CarregaOperadorSemana(dt_inicial, dt_final)
            dt = ds.Tables(0)


            For Each linha As DataRow In dt.Rows

                listaOperador.Add(linha.Item(0))

            Next


            Return listaOperador

        Catch ex As Exception
            MsgBox(ex)
            Return Nothing
        End Try


    End Function

    Public Function BuscarOperador() As List(Of String)
        Dim ds As New DataSet
        Dim dt As New DataTable
        Dim listaOperador As New List(Of String)

        Try

            ds = daoGenerico.CarregaOperador()
            dt = ds.Tables(0)


            For Each linha As DataRow In dt.Rows

                listaOperador.Add(linha.Item(0))

            Next


            Return listaOperador

        Catch ex As Exception
            MsgBox(ex)
            Return Nothing
        End Try


    End Function

#End Region

#Region "Cargo"

    Public Function BuscarCargo(ByVal cod_cargo As Integer) As List(Of CGenerico)

        Return ConverteParaObjetoCargo(daoGenerico.BuscarCargo(cod_cargo))

    End Function

    Public Function BuscarCargoPeloNome(ByVal cargo As String) As List(Of CGenerico)

        Return ConverteParaObjetoCargoSemPrimeiraLinha(daoGenerico.BuscarCargoPeloNome(cargo))

    End Function

    Private Function ConvertePrimeiraLinhaCargo() As CGenerico


        Dim objCargo As CGenerico = New CGenerico()


        objCargo.Cod_Cargo = "-1"
        objCargo.Nome_Cargo = "Selecione..."


        Return objCargo

    End Function

    Private Function ConverteParaObjetoCargo(ByVal dsCargo As DataSet) As List(Of CGenerico)

        Dim row As DataRow
        Dim cargo As List(Of CGenerico) = New List(Of CGenerico)

        cargo.Add(ConvertePrimeiraLinhaCargo())

        For Each row In dsCargo.Tables(0).Rows

            cargo.Add(ConverteParaListCargo(row))

        Next

        Return cargo

    End Function

    Private Function ConverteParaObjetoCargoSemPrimeiraLinha(ByVal dsCargo As DataSet) As List(Of CGenerico)

        Dim row As DataRow
        Dim cargo As List(Of CGenerico) = New List(Of CGenerico)

        For Each row In dsCargo.Tables(0).Rows

            cargo.Add(ConverteParaListCargo(row))

        Next

        Return cargo

    End Function

    Private Function ConverteParaListCargo(ByVal rwObjeto As DataRow) As CGenerico

        Dim objCargo As CGenerico = New CGenerico()

        objCargo.Nome_Cargo = rwObjeto.Item("nome_cargo").ToString()
        objCargo.Cod_Cargo = rwObjeto.Item("cod_cargo").ToString()


        Return objCargo

    End Function
#End Region

#Region "Especie"

    Public Function BuscarEspecie(ByVal cod_especie As Integer) As List(Of CGenerico)

        Return ConverteParaObjetoEspecie(daoGenerico.BuscarEspecie(cod_especie))

    End Function

    Private Function ConvertePrimeiraLinhaEspecie() As CGenerico


        Dim objEspecie As CGenerico = New CGenerico()


        objEspecie.Cod_Especie = "-1"
        objEspecie.Nome_Especie = "Selecione..."


        Return objEspecie

    End Function

    Private Function ConverteParaObjetoEspecie(ByVal dsEspecie As DataSet) As List(Of CGenerico)

        Dim row As DataRow
        Dim especie As List(Of CGenerico) = New List(Of CGenerico)

        especie.Add(ConvertePrimeiraLinhaEspecie())

        For Each row In dsEspecie.Tables(0).Rows

            especie.Add(ConverteParaListEspecie(row))

        Next

        Return especie

    End Function

    Private Function ConverteParaListEspecie(ByVal rwObjeto As DataRow) As CGenerico

        Dim objEspecie As CGenerico = New CGenerico()

        objEspecie.Nome_Especie = rwObjeto.Item("nome_especie").ToString()
        objEspecie.Cod_Especie = rwObjeto.Item("cod_especie").ToString()


        Return objEspecie

    End Function
#End Region

#Region "Grupo"

    Public Function BuscarGrupo(ByVal cod_grupo As Integer) As List(Of CGenerico)

        Return ConverteParaObjetoGrupo(daoGenerico.BuscarGrupo(cod_grupo))

    End Function

    Private Function ConvertePrimeiraLinhaGrupo() As CGenerico


        Dim objGrupo As CGenerico = New CGenerico()


        objGrupo.Cod_Grupo = "-1"
        objGrupo.Nome_Grupo = "Selecione..."


        Return objGrupo

    End Function

    Private Function ConverteParaObjetoGrupo(ByVal dsGrupo As DataSet) As List(Of CGenerico)

        Dim row As DataRow
        Dim grupo As List(Of CGenerico) = New List(Of CGenerico)

        grupo.Add(ConvertePrimeiraLinhaGrupo())

        For Each row In dsGrupo.Tables(0).Rows

            grupo.Add(ConverteParaListGrupo(row))

        Next

        Return grupo

    End Function

    Private Function ConverteParaListGrupo(ByVal rwObjeto As DataRow) As CGenerico

        Dim objGrupo As CGenerico = New CGenerico()

        objGrupo.Nome_Grupo = rwObjeto.Item("nome_grupo").ToString()
        objGrupo.Cod_Grupo = rwObjeto.Item("cod_grupo").ToString()


        Return objGrupo

    End Function
#End Region

#Region "Situacao"

    Public Function BuscarSituacao(ByVal cod_Situacao As Integer) As List(Of CGenerico)

        Return ConverteParaObjetoSituacao(daoGenerico.BuscarSituacao(cod_Situacao))

    End Function

    Private Function ConvertePrimeiraLinhaSituacao() As CGenerico


        Dim objSituacao As CGenerico = New CGenerico()


        objSituacao.Situacao_Faturamento = "-1"
        objSituacao.Situacao = "Selecione..."


        Return objSituacao

    End Function

    Private Function ConverteParaObjetoSituacao(ByVal dsSituacao As DataSet) As List(Of CGenerico)

        Dim row As DataRow
        Dim situacao As List(Of CGenerico) = New List(Of CGenerico)

        situacao.Add(ConvertePrimeiraLinhaSituacao())

        For Each row In dsSituacao.Tables(0).Rows

            situacao.Add(ConverteParaListSituacao(row))

        Next

        Return situacao

    End Function

    Private Function ConverteParaListSituacao(ByVal rwObjeto As DataRow) As CGenerico

        Dim objSituacao As CGenerico = New CGenerico()

        objSituacao.Situacao = rwObjeto.Item("situacao").ToString()
        objSituacao.Situacao_Faturamento = rwObjeto.Item("situacao_faturamento_id").ToString()


        Return objSituacao

    End Function
#End Region

#Region "Usuario"
    Public Function ValidaLogin(ByVal usuario As String, ByVal senha As String) As DataSet

        Return (daoGenerico.ValidaLogin(usuario, senha))

    End Function

    Public Function BuscaDadosAcesso(ByVal cod_funcionario As Integer) As List(Of CGenerico)

        Return ConverteParaObjetoUsuario(daoGenerico.BuscaAcesso(cod_funcionario))

    End Function

    Private Function ConverteParaObjetoUsuario(ByVal dsLista As DataSet) As List(Of CGenerico)

        Dim row As DataRow
        Dim grupo As List(Of CGenerico) = New List(Of CGenerico)

        If Not IsNothing(dsLista) Then
            If dsLista.Tables(0).Rows.Count > 0 Then

                For Each row In dsLista.Tables(0).Rows

                    grupo.Add(ConverteParaListUsuario(row))

                Next
            End If
        End If
        Return grupo

    End Function

    Private Function ConverteParaListUsuario(ByVal rwObjeto As DataRow) As CGenerico

        Dim objGrupo As CGenerico = New CGenerico()

        objGrupo.Usuario = rwObjeto.Item("usuario").ToString()
        objGrupo.Senha = rwObjeto.Item("senha").ToString()
        objGrupo.Acesso = rwObjeto.Item("cod_acessos").ToString()
        objGrupo.Cod_Funcionario = rwObjeto.Item("cod_funcionario").ToString()

        Return objGrupo

    End Function

    Public Function CadastroUsuario(ByVal usuario As CGenerico, ByVal operacao As String) As Boolean

        Return (daoGenerico.CadastroUsuario(usuario, operacao))

    End Function

    Public Function RetornaIDFuncionario() As Integer
        Dim ID As Integer
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable
        Try
            ds = (daoGenerico.RetornaIDFuncionario())
            If (ds.Tables(0).Rows.Count > 0) Then
                ID = ds.Tables(0).Rows(0).Item(0)
            End If

            Return ID

        Catch ex As Exception
            Return 1
        End Try

    End Function



#End Region

#Region "Funcionario"

    Public Function BuscarFuncionarioPeloNome(ByVal funcionario As String) As List(Of CGenerico)

        Return ConverteParaObjetoFuncionario(daoGenerico.BuscarOperadorPeloNome(funcionario))

    End Function

    Public Function BuscarFuncionario(ByVal cod_funcionario As Integer) As List(Of CGenerico)

        Return ConverteParaObjetoFuncionario(daoGenerico.BuscarCargo(cod_funcionario))

    End Function

    Private Function ConverteParaObjetoFuncionario(ByVal dsLista As DataSet) As List(Of CGenerico)

        Dim row As DataRow
        Dim grupo As List(Of CGenerico) = New List(Of CGenerico)

        For Each row In dsLista.Tables(0).Rows

            grupo.Add(ConverteParaListFuncionario(row))

        Next

        Return grupo

    End Function

    Private Function ConverteParaListFuncionario(ByVal rwObjeto As DataRow) As CGenerico

        Dim objGrupo As CGenerico = New CGenerico()

        objGrupo.Cod_Funcionario = rwObjeto.Item("Cod_Funcionario").ToString()
        objGrupo.Cod_Ean_Funcionario = rwObjeto.Item("Cod_Ean_Funcionario").ToString()
        objGrupo.Nome_Funcionario = rwObjeto.Item("Nome_Funcionario").ToString()
        objGrupo.Nome_Cargo = rwObjeto.Item("Cargo").ToString()
        objGrupo.Cpf = rwObjeto.Item("Cpf").ToString()
        objGrupo.Sexo = rwObjeto.Item("Sexo").ToString()
        objGrupo.Endereco = rwObjeto.Item("Endereco").ToString()
        objGrupo.Bairro = rwObjeto.Item("Bairro").ToString()
        objGrupo.Cidade = rwObjeto.Item("Cidade").ToString()
        objGrupo.Uf_Cidade = rwObjeto.Item("Uf").ToString()
        objGrupo.Cep = rwObjeto.Item("Cep").ToString()
        objGrupo.Telefone = rwObjeto.Item("Telefone").ToString()
        objGrupo.Celular = rwObjeto.Item("Celular").ToString()
        objGrupo.Email = rwObjeto.Item("Email").ToString()
        objGrupo.RG = rwObjeto.Item("ci").ToString()
        objGrupo.Data_Nascimento = rwObjeto.Item("Data_Nasc").ToString()
        objGrupo.Data_admissao = rwObjeto.Item("Data_admissao").ToString()
        objGrupo.Data_demissao = rwObjeto.Item("Data_demissao").ToString()
        objGrupo.Comissao = rwObjeto.Item("Comissao").ToString()
        objGrupo.Salario = rwObjeto.Item("Salario").ToString()
        objGrupo.Observacao = rwObjeto.Item("Observacao").ToString()
        objGrupo.Meta_1 = rwObjeto.Item("Meta_1").ToString()
        objGrupo.Quant_1 = rwObjeto.Item("Quant_1").ToString()
        objGrupo.Comissao_1 = rwObjeto.Item("Com_1").ToString()
        objGrupo.Meta_2 = rwObjeto.Item("Meta_2").ToString()
        objGrupo.Quant_2 = rwObjeto.Item("Quant_2").ToString()
        objGrupo.Comissao_2 = rwObjeto.Item("Com_2").ToString()
        objGrupo.Meta_3 = rwObjeto.Item("Meta_3").ToString()
        objGrupo.Quant_3 = rwObjeto.Item("Quant_3").ToString()
        objGrupo.Comissao_3 = rwObjeto.Item("Com_3").ToString()
        objGrupo.Cod_Cargo = rwObjeto.Item("Cod_Cargo").ToString()
        objGrupo.Cod_Cidade = rwObjeto.Item("Cod_Cidade").ToString()
        objGrupo.Cod_empresa = rwObjeto.Item("Cod_empresa").ToString()
        objGrupo.Cod_Bairro = rwObjeto.Item("Cod_Bairro").ToString()



        Return objGrupo

    End Function

    Public Function CadastroFuncionario(ByVal funcionario As CGenerico, ByVal operacao As String) As Boolean

        Return (daoGenerico.CadastroUsuario(funcionario, operacao))

    End Function

#End Region

#Region "Lista Telefonica"

    Public Function BuscaQuantidadeListaTelefonica(ByVal categoria As Integer) As Integer

        Return daoGenerico.Busca_Quantidade_Lista_Telefonica(categoria)

    End Function

    Public Function BuscaValorListaTelefonica(ByVal categoria As Integer) As Integer

        Return daoGenerico.Busca_Valor_Lista_Telefonica(categoria)

    End Function

    Public Function BuscaListaTelefonica(ByVal categoria As Integer) As List(Of CGenerico)
        Try


            Return ConverteParaObjetoLista(daoGenerico.Busca_Lista_Telefonica(categoria))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function


    Public Function ConverteParaObjetoLista(ByVal dsGrupo As DataSet) As List(Of CGenerico)

        Dim row As DataRow
        Dim grupo As List(Of CGenerico) = New List(Of CGenerico)

        For Each row In dsGrupo.Tables(0).Rows

            grupo.Add(ConverteParaListLista(row))

        Next

        Return grupo

    End Function

    Private Function ConverteParaListLista(ByVal rwObjeto As DataRow) As CGenerico

        Dim objLista As CGenerico = New CGenerico()
        Try


            objLista.mCod_cliente = IIf(IsDBNull(rwObjeto.Item("cod_ean").ToString()), "", rwObjeto.Item("cod_ean").ToString())
            objLista.mDDD = IIf(IsDBNull(rwObjeto.Item("DDD").ToString()), "", rwObjeto.Item("DDD").ToString())
            objLista.mTelefone = IIf(IsDBNull(rwObjeto.Item("telefone").ToString()), "", rwObjeto.Item("telefone").ToString())
            objLista.mValor = IIf(IsDBNull(rwObjeto.Item("valor").ToString()), 0, rwObjeto.Item("valor").ToString())
            objLista.mNome_cliente = IIf(IsDBNull(rwObjeto.Item("nome_cliente").ToString()), "", rwObjeto.Item("nome_cliente").ToString())
            objLista.mCategoria = IIf(IsDBNull(rwObjeto.Item("categoria").ToString()), "", rwObjeto.Item("categoria").ToString())
            objLista.mMesSituacao = IIf(IsDBNull(rwObjeto.Item("mesSituacao").ToString()), "", rwObjeto.Item("mesSituacao").ToString())
            objLista.mUltimaSituacao = IIf(IsDBNull(rwObjeto.Item("situacao").ToString()), "", rwObjeto.Item("situacao").ToString())
            objLista.mListaId = IIf(IsDBNull(rwObjeto.Item("lista_telefonica_id")), 0, rwObjeto.Item("lista_telefonica_id"))


            Return objLista


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function
    Public Function Retorna_Historico(ByVal telefone As String, ByVal ddd As String, ByVal cod_ean As String) As List(Of CListaTelefonica)
        Dim daogenerico As New GenericoDAO

        Try

            Return ConverteParaObjetoListaHistorico(daogenerico.Retorna_Historico(telefone, ddd, cod_ean))

        Catch ex As Exception
            Return Nothing
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Function

    Public Function RetornaHistoricoOi(ByVal telefone As String, ByVal ddd As String) As DataSet
        Try
            Return daoGenerico.RetornaHistoricoOi(telefone, ddd)
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaHistoricoOiOperadora() As DataSet
        Try
            ' Return daoGenerico.RetornaHistoricoOi(telefone, ddd, isCritica)
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function Retorna_HistoricoCemig(ByVal telefone As String, ByVal ddd As String, ByVal cod_ean As String) As List(Of CListaTelefonica)
        Dim daogenerico As New GenericoDAO

        Try

            Return ConverteParaObjetoListaHistorico(daogenerico.Retorna_HistoricoCemig(telefone, ddd, cod_ean))

        Catch ex As Exception
            Return Nothing
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Function

    Public Function Retorna_HistoricoSemRecusas(ByVal telefone As String, ByVal ddd As String, ByVal cod_ean As String) As List(Of CListaTelefonica)
        Dim daogenerico As New GenericoDAO

        Try

            Return ConverteParaObjetoListaHistorico(daogenerico.Retorna_HistoricoSemRecusas(telefone, ddd, cod_ean))

        Catch ex As Exception
            Return Nothing
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Function

    Public Function Retorna_HistoricoSimplificado(ByVal telefone As String, ByVal ddd As String) As List(Of CListaTelefonica)
        Dim daogenerico As New GenericoDAO

        Try

            Return ConverteParaObjetoListaHistorico(daogenerico.Retorna_HistoricoSimplificado(telefone, ddd))

        Catch ex As Exception
            Return Nothing
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Function

    Public Function RetornaRequisicaoSelecionada(ByVal requisicaoId As Long) As List(Of CGenerico)
        Dim daogenerico As New GenericoDAO

        Try

            Return ConverteParaObjetoListaDetalhes(daogenerico.RetornaRequisicaoSelecionada(requisicaoId))

        Catch ex As Exception
            Return Nothing
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Function

    Public Function RetornaRequisicaoCemigSelecionada(ByVal requisicaoId As Long) As List(Of CGenerico)
        Dim daogenerico As New GenericoDAO

        Try

            Return ConverteParaObjetoListaDetalhes(daogenerico.RetornaRequisicaoCemigSelecionada(requisicaoId))

        Catch ex As Exception
            Return Nothing
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Function

    Private Function ConverteParaObjetoListaHistorico(ByVal dsGrupo As DataSet) As List(Of CListaTelefonica)

        Dim row As DataRow
        Dim grupo As List(Of CListaTelefonica) = New List(Of CListaTelefonica)
        If dsGrupo Is Nothing Then Return Nothing
        For Each row In dsGrupo.Tables(0).Rows

            grupo.Add(ConverteParaListListaHistorico(row))

        Next

        Return grupo

    End Function

    Private Function ConverteParaObjetoListaDetalhes(ByVal dsGrupo As DataSet) As List(Of CGenerico)

        Dim row As DataRow
        Dim grupo As List(Of CGenerico) = New List(Of CGenerico)

        For Each row In dsGrupo.Tables(0).Rows

            grupo.Add(ConverteParaListListaResumoRequisicao(row))

        Next

        Return grupo

    End Function


    Private Function ConverteParaListListaHistorico(ByVal rwObjeto As DataRow) As CListaTelefonica

        Dim objLista As CListaTelefonica = New CListaTelefonica()

        objLista.DDD = IIf(IsDBNull(rwObjeto.Item("StDDDMeioAcessoCobranca").ToString()), "", rwObjeto.Item("StDDDMeioAcessoCobranca").ToString())
        objLista.Telefone = IIf(IsDBNull(rwObjeto.Item("StNumMeioAcessoCobranca").ToString()), "", rwObjeto.Item("StNumMeioAcessoCobranca").ToString())
        'objLista.Telefone = objLista.Telefone.Remove(0, 2)
        objLista.Valor = IIf(IsDBNull(rwObjeto.Item("DVlrServico")), 0, rwObjeto.Item("DVlrServico").ToString())
        objLista.Categoria = IIf(IsDBNull(rwObjeto.Item("categoria")), 0, rwObjeto.Item("categoria").ToString())
        objLista.MesSituacao = IIf(IsDBNull(rwObjeto.Item("DdDataServico").ToString()), "", rwObjeto.Item("DdDataServico").ToString())
        objLista.Situacao = IIf(IsDBNull(rwObjeto.Item("UsSitFaturamento").ToString()), "", rwObjeto.Item("UsSitFaturamento").ToString())
        objLista.MotivoSituacao = IIf(IsDBNull(rwObjeto.Item("UiMotivoSituacao").ToString()), "", rwObjeto.Item("UiMotivoSituacao").ToString())
        objLista.DataSituacao = IIf(IsDBNull(rwObjeto.Item("DdDataSituacao").ToString()), "", rwObjeto.Item("DdDataSituacao").ToString())
        objLista.Nome_cliente = IIf(IsDBNull(rwObjeto.Item("nome_cliente").ToString()), "", rwObjeto.Item("nome_cliente").ToString())
        objLista.UltimaSituacao = IIf(IsDBNull(rwObjeto.Item("DdDataSituacao").ToString()), "", rwObjeto.Item("DdDataSituacao").ToString())
        objLista.StdescReferencia = IIf(IsDBNull(rwObjeto.Item("StdescReferencia").ToString()), "", rwObjeto.Item("StdescReferencia").ToString())





        Return objLista

    End Function

    Private Function ConverteParaListListaResumoRequisicao(ByVal rwObjeto As DataRow) As CGenerico

        Dim objLista As CGenerico = New CGenerico()

        objLista.DDD = IIf(IsDBNull(rwObjeto.Item("DDD").ToString()), "", rwObjeto.Item("DDD").ToString())
        objLista.Telefone = IIf(IsDBNull(rwObjeto.Item("Telefone").ToString()), "", rwObjeto.Item("Telefone").ToString())
        objLista.Valor = IIf(IsDBNull(rwObjeto.Item("valor")), 0, rwObjeto.Item("valor").ToString())
        objLista.Cod_Categoria = IIf(IsDBNull(rwObjeto.Item("categoria")), 0, rwObjeto.Item("categoria").ToString())
        objLista.MesSituacao = IIf(IsDBNull(rwObjeto.Item("mesRef").ToString()), "", rwObjeto.Item("mesRef").ToString())
        objLista.Situacao = IIf(IsDBNull(rwObjeto.Item("aceite").ToString()), "", rwObjeto.Item("aceite").ToString())
        objLista.Dt_inclusao = IIf(IsDBNull(rwObjeto.Item("dt_inclusao").ToString()), "", rwObjeto.Item("dt_inclusao").ToString())
        objLista.Nome_cliente = IIf(IsDBNull(rwObjeto.Item("nome_cliente").ToString()), "", rwObjeto.Item("nome_cliente").ToString())
        objLista.Cod_cliente = IIf(IsDBNull(rwObjeto.Item("cod_ean").ToString()), "", rwObjeto.Item("cod_ean").ToString())
        objLista.Operador = IIf(IsDBNull(rwObjeto.Item("operador").ToString()), "", rwObjeto.Item("operador").ToString())
        objLista.Operador_id = IIf(IsDBNull(rwObjeto.Item("operador_id")), 0, rwObjeto.Item("operador_id"))
        objLista.Cpf = IIf(IsDBNull(rwObjeto.Item("Cpf").ToString()), "", rwObjeto.Item("Cpf").ToString())
        objLista.Autorizante = IIf(IsDBNull(rwObjeto.Item("Autorizante").ToString()), "", rwObjeto.Item("Autorizante").ToString())
        objLista.Observacao = IIf(IsDBNull(rwObjeto.Item("Observacao").ToString()), "", rwObjeto.Item("Observacao").ToString())






        Return objLista

    End Function


    Public Function Atualiza_Lista_Telefonica(ByVal lista As CListaTelefonica) As Boolean
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.Atualiza_Lista_telefonica(lista))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function


    Public Function Busca_Informações_Lista_Telefonica_list(ByVal telefone As String, ByVal DDD As String) As CContribuinte
        Dim daogenerico As New GenericoDAO

        Try
            Return ConverteParaObjetoContribuinte(daogenerico.Busca_Informações_Lista_Telefonica(telefone, DDD))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function Busca_Informações_Lista_Telefonica(ByVal telefone As String, ByVal DDD As String) As DataSet
        Dim daogenerico As New GenericoDAO

        Try
            Return daogenerico.Busca_Informações_Lista_Telefonica(telefone, DDD)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Private Function ConverteParaObjetoContribuinte(ByVal dsContribuinte As DataSet) As CContribuinte

        Try


            Dim row As DataRow
            Dim objContribuinte As CContribuinte = New CContribuinte()

            For Each row In dsContribuinte.Tables(0).Rows

                objContribuinte.CodCliente = row.Item("cod_cliente").ToString()
                objContribuinte.Cod_EAN_Cliente = row.Item("cod_ean_cliente").ToString()
                objContribuinte.NomeCliente1 = row.Item("nome_cliente").ToString()
                objContribuinte.NomeCliente2 = row.Item("nome_cliente2").ToString()
                objContribuinte.Operador = row.Item("operador").ToString()
                objContribuinte.CNPJ_CPF = row.Item("cnpj_cpf").ToString()
                objContribuinte.IE_CI = row.Item("ie_ci").ToString()
                objContribuinte.Endereco = row.Item("endereco").ToString()
                objContribuinte.Bairro = row.Item("bairro").ToString()
                objContribuinte.Cidade = row.Item("cidade").ToString()
                objContribuinte.UF = row.Item("uf").ToString()
                objContribuinte.CEP = row.Item("cep").ToString()
                objContribuinte.Telefone1 = row.Item("telefone1").ToString()
                objContribuinte.Telefone2 = row.Item("telefone2").ToString()
                objContribuinte.Email = row.Item("email").ToString()
                objContribuinte.DataCadastro = row.Item("data_cadastro").ToString()
                objContribuinte.DataNascimento = row.Item("data_nascimento").ToString()
                objContribuinte.Cod_Categoria = row.Item("cod_categoria").ToString()
                objContribuinte.Observacao = row.Item("observacao").ToString()
                objContribuinte.Referencia = row.Item("referencia").ToString()
                objContribuinte.Descontinuado = row.Item("descontinuado").ToString()
                objContribuinte.Cod_grupo = row.Item("cod_grupo").ToString()
                objContribuinte.Valor = row.Item("valor").ToString()
                objContribuinte.Cod_Especie = row.Item("cod_especie").ToString()
                objContribuinte.DiaLimite = row.Item("dia_limite").ToString()
                objContribuinte.Dt_ligar = row.Item("data_ligar").ToString()
                objContribuinte.Dt_inativo = row.Item("data_inativo").ToString()
                objContribuinte.Cod_empresa = row.Item("cod_empresa").ToString()
                objContribuinte.Cod_Cidade = row.Item("cod_cidade").ToString()
                objContribuinte.Cod_Bairro = row.Item("cod_bairro").ToString()
                objContribuinte.Cod_Operador = row.Item("cod_operador").ToString()
                objContribuinte.Tel_desligado = row.Item("telefone_deslig").ToString()
                objContribuinte.Nao_pedir_extra = row.Item("nao_pedir_extra").ToString()
                objContribuinte.Cod_Usuario = row.Item("cod_usuario").ToString()
                objContribuinte.DT_NC_MP = row.Item("data_nc_mp").ToString()
                objContribuinte.Nao_Pedir_Mensal = row.Item("nao_pedir_mensal").ToString()
                objContribuinte.DT_Reajuste = row.Item("data_reajuste").ToString()
                objContribuinte.Cod_Regiao = row.Item("cod_regiao").ToString()
                objContribuinte.DDD = row.Item("DDD").ToString()
                objContribuinte.ClienteOi = row.Item("ClienteOi").ToString()


            Next

            Return objContribuinte

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function


#End Region

    Public Function SalvaPermissoes(ByVal operador_id As Integer, ByVal operador As String, ByVal cod_categoria As Integer, ByVal categoria As String) As Boolean
        Dim dao As GenericoDAO = New GenericoDAO
        Try
            If dao.SalvaPermissoes(operador_id, operador, cod_categoria, categoria) = True Then
                Return True
            Else
                Return False
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try



    End Function

    Public Function DeletaPermissoes(ByVal operador_id As Integer) As Boolean
        Dim dao As GenericoDAO = New GenericoDAO
        Try
            If dao.DeletaPermissoes(operador_id) = True Then
                Return True
            Else
                Return False
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try



    End Function

    Public Function CarregaPermissoes(ByVal operador_id As Integer) As DataSet
        Dim dao As GenericoDAO = New GenericoDAO
        Dim ds As DataSet
        Try
            ds = dao.CarregaPermissoes(operador_id)

            If Not IsNothing(ds) Then
                If (ds.Tables(0).Rows.Count > 0) Then
                    Return ds
                Else
                    Return Nothing
                End If
            End If

            Return Nothing

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try



    End Function


    Public Function SalvaRegras(ByVal operador_id As Integer, ByVal operador As String, ByVal cod_categoria As Integer, ByVal categoria As String, ByVal contribuiu As Integer, ByVal nao_contribuiu As Integer) As Boolean
        Dim dao As GenericoDAO = New GenericoDAO
        Try
            If dao.SalvaRegras(operador_id, operador, cod_categoria, categoria, contribuiu, nao_contribuiu) = True Then
                Return True
            Else
                Return False
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try



    End Function

    Public Function DeletaRegras(ByVal operador_id As Integer) As Boolean
        Dim dao As GenericoDAO = New GenericoDAO
        Try
            If dao.DeletaRegras(operador_id) = True Then
                Return True
            Else
                Return False
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try



    End Function

    Public Function SalvaRegraEspecial(ByVal diasAceite As Integer, ByVal diasRecusa As Integer) As Boolean
        Dim dao As GenericoDAO = New GenericoDAO
        Try
            If dao.SalvaRegraEspecial(diasAceite, diasRecusa) = True Then
                Return True
            Else
                Return False
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try



    End Function


    Public Function ValidaRemessa(ByVal mesRef As String) As Boolean
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.ValidaRemessa(mesRef))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function ValidaRemessaManual(ByVal mesRef As String) As Boolean
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.ValidaRemessaManual(mesRef))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function GerarRequisicao(ByVal mesRef As String, ByVal proxMesRef As String, ByVal usuario As String) As Boolean
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.GerarRequisicao(mesRef, proxMesRef, usuario))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function


    Public Function GerarRequisicaoManual(ByVal mesRef As String, ByVal proxMesRef As String, ByVal usuario As String) As Boolean
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.GerarRequisicaoManual(mesRef, proxMesRef, usuario))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function SeparaFichas(ByVal operador_id As Integer, ByVal categoria As Integer, quantidade As Integer) As Boolean
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.SeparaFichas(operador_id, categoria, quantidade))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function SeparaFichasManual(ByVal operador_id As Integer, ByVal categoria As Integer, ByVal quantidade As Integer, ByVal cidade As String, ByVal dia As Integer) As Boolean
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.SeparaFichasManual(operador_id, categoria, quantidade, cidade, dia))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function SeparaFichasCemig(ByVal operador_id As Integer, ByVal categoria As Integer, ByVal quantidade As Integer, ByVal cidade As String) As Boolean
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.SeparaFichasCemig(operador_id, categoria, quantidade, cidade))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function RetirarOperadora(ByVal cidade As String, ByVal operador_id As Integer) As Boolean
        Dim daogenerico As New GenericoDAO

        Try
            Return (daogenerico.RetirarOperadora(operador_id, cidade))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function RetirarOperadoraCemig(ByVal cidade As String, ByVal operador_id As Integer) As Boolean
        Dim daogenerico As New GenericoDAO

        Try
            Return (daogenerico.RetirarOperadoraCemig(operador_id, cidade))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function


    Public Function SeparaFichasListaNovaOi(ByVal operador_id As Integer, ByVal categoria As Integer, ByVal quantidade As Integer, ByVal cidade As String) As Boolean
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.SeparaFichasListaNovaOi(operador_id, categoria, quantidade, cidade))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function ResetaFichasPersonalizadoManual(ByVal operador_id As String, ByVal categoria As String, ByVal quantidade As String, ByVal cidade As String) As Boolean
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.ResetaFichasPersonalizadoManual(operador_id, categoria, quantidade, cidade))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function ResetaFichasPersonalizadoOi(ByVal operador_id As String, ByVal operador As String, ByVal categoria As String, ByVal quantidade As String) As Boolean
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.ResetaFichasPersonalizadoOi(operador_id, operador, categoria, quantidade))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function TrocarFichasPersonalizadoOi(ByVal de_operador_id As Integer, ByVal de_operador As String, ByVal para_operador_id As Integer, ByVal para_operador As String, ByVal cod_categoria As Integer, ByVal quantidade As Integer) As Boolean
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.TrocarFichasPersonalizadoOi(de_operador_id, de_operador, para_operador_id, para_operador, cod_categoria, quantidade))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function




    Public Function ResetaFichasManual(ByVal categoria As Integer, ByVal dia As Integer, ByVal cidade As String) As Boolean
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.ResetaFichasManual(categoria, dia, cidade))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function ResetaFichasCemig(ByVal categoria As Integer) As Boolean
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.ResetaFichasCemig(categoria))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function ResetaFichasOi(ByVal categoria As Integer) As Boolean
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.ResetaFichasOi(categoria))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function


    Public Function OperadorPorCategoria(ByVal categoria As Integer) As DataSet
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.Retorna_Operador_por_Categoria(categoria))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function NomeOperadorPorCategoria(ByVal categoria As Integer) As DataSet
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.Retorna_Nome_Operador_por_Categoria(categoria))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaRequisicao() As DataSet
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.RetornaDadosRequisicao())

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaRequisicaoManual() As DataSet
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.RetornaDadosRequisicaoManual())

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaRequisicaoCemig() As DataSet
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.RetornaDadosRequisicaoCemig())

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaRequisicaoPorCategoria() As List(Of CGenerico)
        Dim daogenerico As New GenericoDAO

        Try

            Return ConverteParaObjetoListaResumoRequisicaoCategoria(daogenerico.RetornaRequisicaoPorCategoria())

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaRequisicaoPorCategoriaManual() As List(Of CGenerico)
        Dim daogenerico As New GenericoDAO

        Try

            Return ConverteParaObjetoListaResumoRequisicaoCategoria(daogenerico.RetornaRequisicaoPorCategoriaManual())

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaResumoRequisicao(ByVal mesRef As Date) As List(Of CGenerico)
        Dim daogenerico As New GenericoDAO

        Try

            Return ConverteParaObjetoListaResumoRequisicao(daogenerico.RetornaResumoRequisicao(mesRef))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaResumoRequisicaoManual(ByVal mesRef As Date) As List(Of CGenerico)
        Dim daogenerico As New GenericoDAO

        Try

            Return ConverteParaObjetoListaResumoRequisicao(daogenerico.RetornaResumoRequisicaoManual(mesRef))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Private Function ConverteParaObjetoListaResumoRequisicao(ByVal dsResumo As DataSet) As List(Of CGenerico)

        Dim row As DataRow
        Dim resumo As List(Of CGenerico) = New List(Of CGenerico)

        For Each row In dsResumo.Tables(0).Rows

            resumo.Add(ConverteParaListResumoRequisicao(row))

        Next

        Return resumo

    End Function

    Private Function ConverteParaListResumoRequisicao(ByVal rwObjeto As DataRow) As CGenerico

        Dim objLista As CGenerico = New CGenerico()

        objLista.Operador = IIf(IsDBNull(rwObjeto.Item("operador").ToString()), "", rwObjeto.Item("operador").ToString())
        objLista.ValorConfirmado = IIf(IsDBNull(rwObjeto.Item("valor_confirmado")), "", rwObjeto.Item("valor_confirmado"))
        objLista.ValorRecusado = IIf(IsDBNull(rwObjeto.Item("valor_recusado")), "", rwObjeto.Item("valor_recusado"))
        objLista.QuantidadeConfirmado = IIf(IsDBNull(rwObjeto.Item("quantidade_confirmado")), 0, rwObjeto.Item("quantidade_confirmado"))
        objLista.QuantidadeRecusado = IIf(IsDBNull(rwObjeto.Item("quantidade_recusado")), 0, rwObjeto.Item("quantidade_recusado"))

        Return objLista

    End Function


    Private Function ConverteParaObjetoListaResumoRequisicaoCategoria(ByVal dsResumo As DataSet) As List(Of CGenerico)

        Dim row As DataRow
        Dim resumo As List(Of CGenerico) = New List(Of CGenerico)

        For Each row In dsResumo.Tables(0).Rows

            resumo.Add(ConverteParaListResumoRequisicaoCategoria(row))

        Next

        Return resumo

    End Function

    Private Function ConverteParaListResumoRequisicaoCategoria(ByVal rwObjeto As DataRow) As CGenerico

        Dim objLista As CGenerico = New CGenerico()

        objLista.Nome_Categoria = IIf(IsDBNull(rwObjeto.Item("categoria").ToString()), "", rwObjeto.Item("categoria").ToString())
        objLista.QuantidadeConfirmado = IIf(IsDBNull(rwObjeto.Item("quantidade")), "", rwObjeto.Item("quantidade"))
        objLista.ValorConfirmado = IIf(IsDBNull(rwObjeto.Item("valor")), "", rwObjeto.Item("valor"))

        Return objLista

    End Function

    Public Function RetornaTelefonesAnaliseExclusao(ByVal form As String) As List(Of CGenerico)

        Return ConverteParaObjetoAnaliseExclusao(daoGenerico.RetornaTelefonesAnaliseExclusao(), form)


    End Function

    Public Function AtualizaAnaliseExclusao(ByVal contribuinte As CContribuinte) As Boolean

        If daoGenerico.AtualizaTelefonesAnaliseExclusao(contribuinte) = True Then

            Return True
        Else

            Return False

        End If

    End Function


    Public Function CarregarRequisicaoPorTelefone(ByVal telefone As String, ByVal DDD As String, ByVal form As String) As List(Of CGenerico)

        Return ConverteParaObjetoRequisicao(daoGenerico.RetornaRequisicaoPorTelefone(telefone, DDD), form)


    End Function

    Public Function CarregarRequisicaoCemigPorTelefone(ByVal telefone As String, ByVal DDD As String, ByVal form As String) As List(Of CGenerico)

        Return ConverteParaObjetoRequisicao(daoGenerico.RetornaRequisicaoCemigPorTelefone(telefone, DDD), form)


    End Function

    Public Function CarregarRequisicaoPorPeriodoData(ByVal dtInicial As Date, ByVal dtFinal As Date, ByVal tipoAceite As Integer, ByVal naoConforme As Boolean, ByVal form As String) As List(Of CGenerico)

        Return ConverteParaObjetoRequisicao(daoGenerico.RetornaRequisicaoPorPeriodoData(dtInicial, dtFinal, tipoAceite, naoConforme), form)


    End Function

    Public Function CarregarRequisicaoCemigPorPeriodoData(ByVal dtInicial As Date, ByVal dtFinal As Date, ByVal tipoAceite As Integer, ByVal naoConforme As Boolean, ByVal form As String) As List(Of CGenerico)

        Return ConverteParaObjetoRequisicao(daoGenerico.RetornaRequisicaoCemigPorPeriodoData(dtInicial, dtFinal, tipoAceite, naoConforme), form)


    End Function

    Public Function CarregarRequisicaoPorPeriodoDataComplementar(ByVal dtInicial As Date, ByVal dtFinal As Date, ByVal tipoAceite As Integer, ByVal naoConforme As Boolean, ByVal Pesquisa As Integer, ByVal TipoPesquisa As Integer, ByVal verificaGrau As Boolean, ByVal form As String) As List(Of CGenerico)

        Return ConverteParaObjetoRequisicao(daoGenerico.RetornaRequisicaoPorPeriodoDataComplementar(dtInicial, dtFinal, tipoAceite, naoConforme, Pesquisa, TipoPesquisa, verificaGrau), form)


    End Function

    Public Function RetornaRequisicaoComCriticasComissao(dtInicial As Date, dtFinal As Date, Operador As Integer) As Integer

        Return daoGenerico.RetornaRequisicaoComCriticasComissao(dtInicial, dtFinal, Operador)


    End Function



    Public Function CarregarRequisicaoCemigPorPeriodoDataComplementar(ByVal dtInicial As Date, ByVal dtFinal As Date, ByVal tipoAceite As Integer, ByVal naoConforme As Boolean, ByVal Pesquisa As Integer, ByVal TipoPesquisa As Integer, ByVal verificaGrau As Boolean, ByVal form As String) As List(Of CGenerico)

        Return ConverteParaObjetoRequisicao(daoGenerico.RetornaRequisicaoCemigPorPeriodoDataComplementar(dtInicial, dtFinal, tipoAceite, naoConforme, Pesquisa, TipoPesquisa, verificaGrau), form)


    End Function

    Public Function CarregarRequisicaoPorPeriodoDataCategoria(ByVal dtInicial As Date, ByVal dtFinal As Date, ByVal tipoAceite As Integer, ByVal categoria As Integer, ByVal operador As Integer) As DataSet

        Return daoGenerico.RetornaRequisicaoPorPeriodoDataCategoria(dtInicial, dtFinal, tipoAceite, categoria, operador)


    End Function

    Public Function CarregarRequisicaoManualPorPeriodoDataCategoria(ByVal dtInicial As Date, ByVal dtFinal As Date, ByVal tipoAceite As Integer, ByVal categoria As Integer, ByVal operador As Integer) As DataSet

        Return daoGenerico.RetornaRequisicaoManualPorPeriodoDataCategoria(dtInicial, dtFinal, tipoAceite, categoria, operador)


    End Function

    Public Function CarregarValoresAtuaisMes(ByVal operador As Integer) As DataSet

        Return daoGenerico.CarregarValoresAtuaisMes(operador)


    End Function

    Public Function CarregarValoresAtuaisDia(ByVal operador As Integer) As DataSet

        Return daoGenerico.CarregarValoresAtuaisDia(operador)


    End Function

    Public Function CarregarManualValoresAtuaisDia(ByVal operador As Integer) As DataSet

        Return daoGenerico.CarregarManualValoresAtuaisDia(operador)


    End Function

    Public Function CarregarManualValoresAtuaisMes(ByVal operador As Integer) As DataSet

        Return daoGenerico.CarregarManualValoresAtuaisMes(operador)


    End Function

    Public Function CarregarCriticadosContestados(ByVal dtConsulta As Date, ByVal tipoCritica As String) As DataSet

        Return daoGenerico.RetornaCarregarCriticadosContestados(dtConsulta, tipoCritica)


    End Function

    Public Function CarregarProducaoMensal(dataInicial As Date, dataFinal As Date) As DataSet

        Return daoGenerico.CarregaProducaoMensal(dataInicial, dataFinal)


    End Function

    Private Function ConverteParaObjetoRequisicao(ByVal dsContribuinte As DataSet, ByVal form As String) As List(Of CGenerico)

        Try

            Dim row As DataRow
            Dim contribuintesOi As List(Of CGenerico) = New List(Of CGenerico)


            For Each row In dsContribuinte.Tables(0).Rows

                contribuintesOi.Add(ConverteParaListRequisicaoTelefone(row))

            Next

            Return contribuintesOi

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Private Function ConverteParaListRequisicaoTelefone(ByVal rwObjeto As DataRow) As CGenerico

        Try


            Dim objRetorno As CGenerico = New CGenerico()


            objRetorno.DDD = IIf(IsDBNull(rwObjeto.Item("DDD").ToString()), "", rwObjeto.Item("DDD").ToString())
            objRetorno.Telefone = IIf(IsDBNull(rwObjeto.Item("Telefone").ToString()), "", rwObjeto.Item("Telefone").ToString())
            objRetorno.Valor = IIf(IsDBNull(rwObjeto.Item("valor")), "", rwObjeto.Item("valor"))
            objRetorno.Cod_ean = IIf(IsDBNull(rwObjeto.Item("cod_ean").ToString()), "", rwObjeto.Item("cod_ean").ToString())
            objRetorno.Nome_cliente = IIf(IsDBNull(rwObjeto.Item("nome_cliente").ToString()), "", rwObjeto.Item("nome_cliente").ToString())
            objRetorno.Operador = IIf(IsDBNull(rwObjeto.Item("operador").ToString()), "", rwObjeto.Item("operador").ToString())
            objRetorno.Aceite = IIf(IsDBNull(rwObjeto.Item("aceite").ToString()), "", rwObjeto.Item("aceite").ToString())
            objRetorno.Hist_Aceite = IIf(IsDBNull(rwObjeto.Item("hist_aceite").ToString()), "", rwObjeto.Item("hist_aceite").ToString())
            objRetorno.Categoria = IIf(IsDBNull(rwObjeto.Item("categoria").ToString()), "", rwObjeto.Item("categoria").ToString())
            objRetorno.MesRefSeguinte = IIf(IsDBNull(rwObjeto.Item("mesRefSeguinte").ToString()), "", rwObjeto.Item("mesRefSeguinte").ToString())
            objRetorno.Dt_inclusao = IIf(IsDBNull(rwObjeto.Item("dt_inclusao").ToString()), "", rwObjeto.Item("dt_inclusao").ToString())
            objRetorno.Cpf = IIf(IsDBNull(rwObjeto.Item("cpf").ToString()), "", rwObjeto.Item("cpf").ToString())
            objRetorno.Autorizante = IIf(IsDBNull(rwObjeto.Item("autorizante").ToString()), "", rwObjeto.Item("autorizante").ToString())
            objRetorno.Observacao = IIf(IsDBNull(rwObjeto.Item("observacao").ToString()), "", rwObjeto.Item("observacao").ToString())
            objRetorno.Requisicao_Id = IIf(IsDBNull(rwObjeto.Item("requisicao_id")), 0, rwObjeto.Item("requisicao_id"))

            If objRetorno.Aceite = "C" Then
                objRetorno.Aceite = "CANCELADO"
            End If

            Return objRetorno

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function

    Public Function ExcluirRequisicaoselecionada(ByVal requisicaoId As Long) As Boolean
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.ExcluirRequisicaoselecionada(requisicaoId))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function ExcluirRequisicaoCemigSelecionada(ByVal requisicaoId As Long) As Boolean
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.ExcluirRequisicaoCemigselecionada(requisicaoId))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Private Function ConverteParaObjetoAnaliseExclusao(ByVal dsContribuinte As DataSet, ByVal form As String) As List(Of CGenerico)

        Try

            Dim row As DataRow
            Dim contribuintesOi As List(Of CGenerico) = New List(Of CGenerico)


            For Each row In dsContribuinte.Tables(0).Rows

                contribuintesOi.Add(ConverteParaListAnaliseExclusao(row))

            Next

            Return contribuintesOi

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Private Function ConverteParaListAnaliseExclusao(ByVal rwObjeto As DataRow) As CGenerico

        Try


            Dim objRetorno As CGenerico = New CGenerico()

            objRetorno.Cod_ean_cliente = IIf(IsDBNull(rwObjeto.Item("cod_ean_cliente").ToString()), "", rwObjeto.Item("cod_ean_cliente").ToString())
            objRetorno.Telefone = IIf(IsDBNull(rwObjeto.Item("Telefone").ToString()), "", rwObjeto.Item("Telefone").ToString())
            objRetorno.Cod_opera = IIf(IsDBNull(rwObjeto.Item("Cod_opera").ToString()), "", rwObjeto.Item("Cod_opera").ToString())
            objRetorno.Operador = IIf(IsDBNull(rwObjeto.Item("operador").ToString()), "", rwObjeto.Item("operador").ToString())
            objRetorno.Dt_inclusao = IIf(IsDBNull(rwObjeto.Item("dia_solicitacao").ToString()), "", rwObjeto.Item("dia_solicitacao").ToString())


            Return objRetorno

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function

    Public Function VerificaTelefoneNuncaCriticado(ByVal telefone As String) As Boolean

        Return daoGenerico.VerificaTelefoneNuncaCriticado(telefone)


    End Function




#End Region

#Region "Acerto Caixa"

    Public Function CarregarGridFaturamento(ByVal tipo_mov As String, ByVal funcionario As String, ByVal data_movimentacao As String) As List(Of CGenerico)

        Return ConverteParaObjetoFaturamento(daoGenerico.CarregarFaturamento(tipo_mov, funcionario, data_movimentacao))


    End Function

    Public Function CarregarGridFaturamentoCod(ByVal cod_movimentacao As Integer) As List(Of CGenerico)

        Return ConverteParaObjetoFaturamento(daoGenerico.CarregarFaturamentoCod(cod_movimentacao))


    End Function
    Private Function ConverteParaObjetoFaturamento(ByVal dsContribuinte As DataSet) As List(Of CGenerico)

        Try

            Dim row As DataRow
            Dim faturamento As List(Of CGenerico) = New List(Of CGenerico)


            For Each row In dsContribuinte.Tables(0).Rows

                faturamento.Add(ConverteParaListFaturamento(row))

            Next

            Return faturamento

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Private Function ConverteParaListFaturamento(ByVal rwObjeto As DataRow) As CGenerico

        Try


            Dim objFaturamento As CGenerico = New CGenerico()


            objFaturamento.Cod_mov_contribuicao_descr = IIf(IsDBNull(rwObjeto.Item("Cod_mov_contribuicao_descr")), 0, rwObjeto.Item("Cod_mov_contribuicao_descr"))
            objFaturamento.Cod_mov_contribuicao = IIf(IsDBNull(rwObjeto.Item("Cod_mov_contribuicao")), 0, rwObjeto.Item("Cod_mov_contribuicao"))
            objFaturamento.Tipo_mov = IIf(IsDBNull(rwObjeto.Item("Tipo_mov").ToString()), "", rwObjeto.Item("Tipo_mov").ToString())
            objFaturamento.Tipo_cobranca = IIf(IsDBNull(rwObjeto.Item("Tipo_cobranca").ToString()), "", rwObjeto.Item("Tipo_cobranca").ToString())
            objFaturamento.Cobrador = IIf(IsDBNull(rwObjeto.Item("Cobrador").ToString()), "", rwObjeto.Item("Cobrador").ToString())
            objFaturamento.Cod_Categoria = IIf(IsDBNull(rwObjeto.Item("Cod_Categoria")), 0, rwObjeto.Item("Cod_Categoria"))
            objFaturamento.Cod_Grupo = IIf(IsDBNull(rwObjeto.Item("Cod_Grupo")), 0, rwObjeto.Item("Cod_Grupo"))
            objFaturamento.Cod_opera = IIf(IsDBNull(rwObjeto.Item("Cod_opera")), 0, rwObjeto.Item("Cod_opera"))
            objFaturamento.Cod_Especie = IIf(IsDBNull(rwObjeto.Item("Cod_Especie")), 0, rwObjeto.Item("Cod_Especie"))
            objFaturamento.Data_mov = IIf(IsDBNull(rwObjeto.Item("Data_mov").ToString()), "", rwObjeto.Item("Data_mov").ToString())
            objFaturamento.Data_venc = IIf(IsDBNull(rwObjeto.Item("Data_venc").ToString()), "", rwObjeto.Item("Data_venc").ToString())
            objFaturamento.Cod_ean_cliente = IIf(IsDBNull(rwObjeto.Item("Cod_ean_cliente").ToString()), "", rwObjeto.Item("Cod_ean_cliente").ToString())
            objFaturamento.Cod_contribuinte = IIf(IsDBNull(rwObjeto.Item("Cod_contribuinte")), 0, rwObjeto.Item("Cod_contribuinte"))
            objFaturamento.Nome_contribuinte = IIf(IsDBNull(rwObjeto.Item("Nome_contribuinte").ToString()), "", rwObjeto.Item("Nome_contribuinte").ToString())
            objFaturamento.Valor = IIf(IsDBNull(rwObjeto.Item("Valor")), 0, rwObjeto.Item("Valor"))
            objFaturamento.Tipo_contrib = IIf(IsDBNull(rwObjeto.Item("Tipo_contrib").ToString()), "", rwObjeto.Item("Tipo_contrib").ToString())
            objFaturamento.Confirmado = IIf(IsDBNull(rwObjeto.Item("Confirmado")), 0, rwObjeto.Item("Confirmado"))
            objFaturamento.Cod_recibo = IIf(IsDBNull(rwObjeto.Item("Cod_recibo").ToString()), "", rwObjeto.Item("Cod_recibo").ToString())
            objFaturamento.Dia = IIf(IsDBNull(rwObjeto.Item("Dia")), 0, rwObjeto.Item("Dia"))
            objFaturamento.Cidade = IIf(IsDBNull(rwObjeto.Item("Cidade").ToString()), "", rwObjeto.Item("Cidade").ToString())
            objFaturamento.Cod_cobrador = IIf(IsDBNull(rwObjeto.Item("Cod_cobrador")), 0, rwObjeto.Item("Cod_cobrador"))
            objFaturamento.Cod_Cidade = IIf(IsDBNull(rwObjeto.Item("Cod_Cidade")), 0, rwObjeto.Item("Cod_Cidade"))
            objFaturamento.Cod_empresa = IIf(IsDBNull(rwObjeto.Item("Cod_empresa")), 0, rwObjeto.Item("Cod_empresa"))
            objFaturamento.Situacao = IIf(IsDBNull(rwObjeto.Item("Situacao").ToString()), "", rwObjeto.Item("Situacao").ToString())
            objFaturamento.Cod_Bairro = IIf(IsDBNull(rwObjeto.Item("Cod_Bairro")), 0, rwObjeto.Item("Cod_Bairro"))
            objFaturamento.Cod_usuario = IIf(IsDBNull(rwObjeto.Item("Cod_usuario").ToString()), "", rwObjeto.Item("Cod_usuario").ToString())
            objFaturamento.Cod_regiao = IIf(IsDBNull(rwObjeto.Item("Cod_regiao").ToString()), "", rwObjeto.Item("Cod_regiao").ToString())


            Return objFaturamento

        Catch ex As Exception

            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing

        End Try


    End Function



    Public Function CarregarGridMovimentacao(ByVal data_Inicio As String, ByVal data_Fim As String) As List(Of CGenerico)

        Return ConverteParaObjetoMovimentacao(daoGenerico.CarregarMovimentacao(data_Inicio, data_Fim))


    End Function

    Private Function ConverteParaObjetoMovimentacao(ByVal dsContribuinte As DataSet) As List(Of CGenerico)

        Try

            Dim row As DataRow
            Dim movimentacao As List(Of CGenerico) = New List(Of CGenerico)


            For Each row In dsContribuinte.Tables(0).Rows

                movimentacao.Add(ConverteParaListMovimentacao(row))

            Next

            Return movimentacao

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Private Function ConverteParaListMovimentacao(ByVal rwObjeto As DataRow) As CGenerico

        Try


            Dim objFaturamento As CGenerico = New CGenerico()

            objFaturamento.Cod_mov_contribuicao = IIf(IsDBNull(rwObjeto.Item("Cod_mov_contribuicao")), 0, rwObjeto.Item("Cod_mov_contribuicao"))
            objFaturamento.Tipo_mov = IIf(IsDBNull(rwObjeto.Item("Tipo_mov").ToString()), "", rwObjeto.Item("Tipo_mov").ToString())
            objFaturamento.Tipo_cobranca = IIf(IsDBNull(rwObjeto.Item("Tipo_cobranca").ToString()), "", rwObjeto.Item("Tipo_cobranca").ToString())
            objFaturamento.Cobrador = IIf(IsDBNull(rwObjeto.Item("Cobrador").ToString()), "", rwObjeto.Item("Cobrador").ToString())
            objFaturamento.Data_mov = IIf(IsDBNull(rwObjeto.Item("Data_mov").ToString()), "", rwObjeto.Item("Data_mov").ToString())
            objFaturamento.Confirmado = IIf(IsDBNull(rwObjeto.Item("Confirmado")), 0, rwObjeto.Item("Confirmado"))
            objFaturamento.Cod_cobrador = IIf(IsDBNull(rwObjeto.Item("Cod_cobrador")), 0, rwObjeto.Item("Cod_cobrador"))
            objFaturamento.Cod_empresa = IIf(IsDBNull(rwObjeto.Item("Cod_empresa")), 0, rwObjeto.Item("Cod_empresa"))
            objFaturamento.Valor_fichas = IIf(IsDBNull(rwObjeto.Item("Valor_fichas")), 0, rwObjeto.Item("Valor_fichas"))
            objFaturamento.Total_fichas = IIf(IsDBNull(rwObjeto.Item("Total_fichas")), 0, rwObjeto.Item("Total_fichas"))



            Return objFaturamento

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing

        End Try


    End Function

    Public Function Atualiza_mov_contribuicao_descr(ByVal cod_mov_descr As Long) As Boolean
        Dim dao As GenericoDAO = New GenericoDAO

        Try
            If dao.Atualiza_mov_contribuicao_descr(cod_mov_descr) = True Then

                Return True

            Else

                Return False

            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function AtualizaFaturamento(ByVal tipo_mov As String, ByVal funcionario As String, ByVal data_movimentacao As String) As Boolean
        Dim dao As GenericoDAO = New GenericoDAO

        Try
            If dao.AtualizaFaturamento(tipo_mov, funcionario, data_movimentacao) = True Then

                Return True

            Else

                Return False

            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


#End Region


#Region "Auditoria Oi"

    Public Function CriaPlanilhaAuditoriaOi(ByVal dataInicial As String, ByVal dataFinal As String, ByVal operadoras As String, ByVal Condicao1 As List(Of String), ByVal Condicao2 As List(Of String), ByVal planilhaInterna As Boolean) As DataSet

        Return daoGenerico.CriaPlanilhaAuditoriaOi(dataInicial, dataFinal, operadoras, Condicao1, Condicao2, planilhaInterna)


    End Function

    Public Function RetornaLigacoesPlanilhaOi(ByVal telefones As List(Of String), ByVal dtInicio As String, ByVal dtFim As String) As DataSet

        Return daoGenerico.RetornaLigacoesPlanilhaOi(telefones, dtInicio, dtFim)

    End Function

    Public Function RetornaPlanilhaImprimir(ByVal operador As String, ByVal categoria As String, ByVal sistema As String, ByVal Simplicado As Boolean) As DataSet

        Return daoGenerico.RetornaPlanilhaImprimir(operador, categoria, sistema, Simplicado)

    End Function

    Public Function CriaArquivoRequisicaoOiExportar(ByVal dataInicial As String, ByVal dataFinal As String) As DataSet

        Return daoGenerico.CriaArquivoRequisicaoOiExportar(dataInicial, dataFinal)

    End Function

    Public Function CriaArquivoRequisicaoExportar() As DataSet

        Return daoGenerico.CriaArquivoRequisicaoExportar()

    End Function

    Public Function CriaArquivoListaTelefonicaExportar() As DataSet

        Return daoGenerico.CriaArquivoListaTelefonicaExportar()

    End Function

    Public Function CriaArquivoTelefoneExclusaoExportar() As DataSet

        Return daoGenerico.CriaArquivoTelefoneExclusaoExportar()

    End Function

#End Region

#Region "Relatorio Operador"

    Public Function RetornaDadosRelatorio(ByVal dataInicial As Date, ByVal dataFinal As Date, ByVal FiltraData As String) As DataSet

        Return daoGenerico.RetornaDadosRelatorio(dataInicial, dataFinal, FiltraData)


    End Function

    Public Function RetornaDadosRelatorioOperador(ByVal dataInicial As Date, ByVal dataFinal As Date, ByVal operador As String, ByVal FiltraData As String) As DataSet

        Return daoGenerico.RetornaDadosRelatorioOperador(dataInicial, dataFinal, operador, FiltraData)


    End Function

    Public Function RetornaRequisicoesRepetidas(ByVal dataInicial As Date, ByVal dataFinal As Date, ByVal FiltraData As String) As DataSet

        Return daoGenerico.RetornaRequisicoesRepetidas(dataInicial, dataFinal, FiltraData)


    End Function


    Public Function RetornaResumoRemessa() As DataSet

        Return daoGenerico.RetornaResumoRemessa()

    End Function




#End Region

#Region "CEMIG"
    Public Function RetornaRequisicaoPorCategoriaCemig() As List(Of CGenerico)
        Dim daogenerico As New GenericoDAO

        Try

            Return ConverteParaObjetoListaResumoRequisicaoCategoria(daogenerico.RetornaRequisicaoPorCategoriaCemig())

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function Retorna_HistoricoSemRecusasCemig(ByVal telefone As String, ByVal ddd As String, ByVal cod_ean As String) As List(Of CListaTelefonica)
        Dim daogenerico As New GenericoDAO

        Try

            Return ConverteParaObjetoListaHistorico(daogenerico.Retorna_HistoricoSemRecusasCemig(telefone, ddd, cod_ean))

        Catch ex As Exception
            Return Nothing
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Function

    Public Function Atualiza_Lista_TelefonicaCemig(lista As CListaTelefonica) As Boolean
        Dim daogenerico As New GenericoDAO

        Try

            Return (daogenerico.Atualiza_Lista_telefonicaCemig(lista))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function CarregarManualValoresAtuaisMesCemig(ByVal operador As Integer) As DataSet

        Return daoGenerico.CarregarManualValoresAtuaisMesCemig(operador)


    End Function

    Public Function CarregarManualValoresAtuaisDiaCemig(ByVal operador As Integer) As DataSet

        Return daoGenerico.CarregarManualValoresAtuaisDiaCemig(operador)


    End Function
#End Region

End Class
