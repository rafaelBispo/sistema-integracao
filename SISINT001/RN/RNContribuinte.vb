﻿Imports System.IO


Public Class RNContribuinte

#Region "Variáveis"

    Private daoContribuinte As ContribuinteDAO


#End Region

#Region "Construtores"

    Sub New()

        daoContribuinte = New ContribuinteDAO()

    End Sub

#End Region

#Region "Procedimentos"


    Public Function BuscarContribuinte(ByVal cod_cliente As Integer) As CContribuinte

        Return ConverteParaObjetoContribuinte(daoContribuinte.Consultar(cod_cliente), "Contribuinte")

    End Function

    Public Function PesquisarContribuintePorTelefone(ByVal telefone As String, ByVal ddd As String, ByVal cod_EAN As String, ByVal opcao As Integer) As CContribuinte

        If opcao = 1 Then
            Return ConverteParaObjetoContribuinte(daoContribuinte.PesquisarPorTelefone(telefone, ddd, cod_EAN, 1), "Contribuinte")
        Else
            Return ConverteParaObjetoContribuinte(daoContribuinte.PesquisarPorTelefone(telefone, ddd, cod_EAN, 2), "Contribuinte")
        End If

    End Function

    Public Function PesquisarContribuintePorTelefoneSimplificado(ByVal telefone As String, ByVal ddd As String, ByVal opcao As Integer) As CContribuinte

        If opcao = 1 Then
            Return ConverteParaObjetoContribuinte(daoContribuinte.PesquisarPorTelefoneSimplicado(telefone, ddd, 1), "Contribuinte")
        Else
            Return ConverteParaObjetoContribuinte(daoContribuinte.PesquisarPorTelefoneSimplicado(telefone, ddd, 2), "Contribuinte")
        End If

    End Function

    Public Function PesquisarPorCodBarra(ByVal cod_EAN As String) As CContribuinte

        Return ConverteParaObjetoContribuinte(daoContribuinte.PesquisarPorCodEAN(cod_EAN), "Contribuinte")

    End Function

    Public Function PesquisarTabelaRetidoPorTelefone(ByVal telefone As String, ByVal ddd As String, ByVal cod_EAN As String) As CContribuinte

        Return ConverteParaObjetoContribuinte(daoContribuinte.PesquisarTabelaRetidoPorTelefone(telefone, ddd, cod_EAN), "Contribuinte")

    End Function

    Public Function PesquisarTabelaRetidoPorTelefoneSimplificado(ByVal telefone As String, ByVal ddd As String) As CContribuinte

        Return ConverteParaObjetoContribuinte(daoContribuinte.PesquisarTabelaRetidoPorTelefoneSimplificado(telefone, ddd), "Contribuinte")

    End Function

    Public Function PesquisaClienteTabelaRetido(ByVal cod_EAN As String) As CContribuinte

        Return ConverteParaObjetoContribuinte(daoContribuinte.PesquisarTabelaRetido(cod_EAN), "Contribuinte")

    End Function

    Public Function ConsultaContribuinteOI(ByVal cod_EAN As String) As CContribuinte
        Dim dr As DataRow
        Dim contr As CContribuinte

        If daoContribuinte.PesquisarPorCodEAN(cod_EAN).Tables(0).Rows.Count > 0 Then
            dr = daoContribuinte.PesquisarPorCodEAN(cod_EAN).Tables(0).Rows(0)
            contr = ConverteParaListContribuinte(dr)
        Else
            Return Nothing
        End If




        Return contr

    End Function

    Public Function Busca_Informações_Lista_Telefonica(ByVal telefone As String, ByVal DDD As String) As DataSet
        Dim daogenerico As New GenericoDAO

        Try

            Return daogenerico.Busca_Informações_Lista_Telefonica(telefone, DDD)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function BuscaListaTelefonica(ByVal telefone As String, ByVal ddd As String, ByVal cod_ean As String) As CContribuinte

        Return ConverteParaListLista(daoContribuinte.Busca_Lista_Telefonica(telefone, ddd, cod_ean))

    End Function

    Private Function ConverteParaListLista(ByVal dsContribuinte As DataSet) As CContribuinte

        Dim objLista As CContribuinte = New CContribuinte()
        Dim row As DataRow

        Try

            For Each row In dsContribuinte.Tables(0).Rows

                objLista.Cod_EAN_Cliente = IIf(IsDBNull(row.Item("cod_ean").ToString()), "", row.Item("cod_ean").ToString())
                objLista.DDD = IIf(IsDBNull(row.Item("DDD").ToString()), "", row.Item("DDD").ToString())
                objLista.Telefone1 = IIf(IsDBNull(row.Item("telefone").ToString()), "", row.Item("telefone").ToString())
                objLista.Valor = IIf(IsDBNull(row.Item("valor").ToString()), "", row.Item("valor").ToString())
                objLista.NomeCliente1 = IIf(IsDBNull(row.Item("nome_cliente").ToString()), "", row.Item("nome_cliente").ToString())
                objLista.Cod_Categoria = IIf(IsDBNull(row.Item("categoria").ToString()), "", row.Item("categoria").ToString())

            Next

            Return objLista

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function CarregarGrid(ByVal filtro As CContribuinte, ByVal form As String) As List(Of CContribuinte)

        Return ConverteParaObjetoContribuintes(daoContribuinte.CarregarGridContribuintes(filtro), form)


    End Function

    Public Function CarregarGridOi(ByVal filtro As CContribuinte, ByVal form As String) As List(Of CContribuinte)

        Return ConverteParaObjetoContribuintes(daoContribuinte.CarregarGridContribuintesOi(filtro), form)


    End Function

    Public Function CarregarDadosRequisicaoOi(ByVal telefone As String, ByVal mesRef As String, ByVal form As String) As List(Of CContribuinte)

        Return ConverteParaObjetoRequisicaoOi(daoContribuinte.ConsultaRegistroRequisicaoOi(telefone, mesRef), form)


    End Function

    Public Function CarregarInclusaoManual(ByVal cod As String, ByVal form As String) As List(Of CContribuinte)

        Return ConverteParaObjetoContribuintes(daoContribuinte.ConsultaCodCliente(cod), form)


    End Function

    Public Function CarregarInclusaoManualRetido(ByVal cod As String, ByVal form As String) As List(Of CContribuinte)

        Return ConverteParaObjetoContribuintesRetido(daoContribuinte.ConsultaCodClienteTabelaRetido(cod), form)


    End Function

    Public Function CarregarInclusaoManualPorTelefone(ByVal telefone As String, ByVal ddd As String, ByVal cod_ean As String, ByVal form As String) As List(Of CContribuinte)

        Return ConverteParaObjetoContribuintes(daoContribuinte.PesquisarPorTelefone(telefone, ddd, cod_ean, 1), form)

    End Function

    Public Function CarregarInclusaoManualPorTelefoneSimplificado(ByVal telefone As String, ByVal ddd As String, ByVal form As String) As List(Of CContribuinte)

        Return ConverteParaObjetoContribuintes(daoContribuinte.PesquisarPorTelefoneSimplicado(telefone, ddd, 1), form)

    End Function

    Public Function CarregarContribuintePorTelefone(ByVal telefone As String, ByVal DDD As String, ByVal cod_ean As String, ByVal form As String) As CContribuinte

        Return ConverteParaObjetoContribuinte(daoContribuinte.PesquisarPorTelefone(telefone, DDD, cod_ean, 1), form)

    End Function

    Public Function CarregarDadosRequisicaoPorTelefone(ByVal telefone As String, ByVal form As String) As List(Of CContribuinte)

        Return ConverteParaObjetoRequisicaoOi(daoContribuinte.ConsultaRegistroDadosRequisicaoOi(telefone, 1), form)

    End Function


    Private Function ConverteParaObjetoContribuintes(ByVal dsContribuinte As DataSet, ByVal form As String) As List(Of CContribuinte)

        Try

            Dim row As DataRow
            Dim contribuintes As List(Of CContribuinte) = New List(Of CContribuinte)

            If Not form = "Remessa" Then
                contribuintes.Add(ConvertePrimeiraLinhaContribuinte())
            End If

            If IsNothing(dsContribuinte) Then
                Return contribuintes
            End If

            If (dsContribuinte.Tables(0).Rows.Count > 0) Then
                For Each row In dsContribuinte.Tables(0).Rows

                    contribuintes.Add(ConverteParaListContribuinte(row))

                Next

                Return contribuintes
            Else
                Return contribuintes
            End If
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Private Function ConverteParaObjetoContribuintesRetido(ByVal dsContribuinte As DataSet, ByVal form As String) As List(Of CContribuinte)

        Try

            Dim row As DataRow
            Dim contribuintes As List(Of CContribuinte) = New List(Of CContribuinte)

            If Not form = "Remessa" Then
                contribuintes.Add(ConvertePrimeiraLinhaContribuinte())
            End If

            If IsNothing(dsContribuinte) Then
                Return contribuintes
            End If

            If (dsContribuinte.Tables(0).Rows.Count > 0) Then
                For Each row In dsContribuinte.Tables(0).Rows

                    contribuintes.Add(ConverteParaListContribuinteRetido(row))

                Next

                Return contribuintes
            Else
                Return contribuintes
            End If
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Private Function ConverteParaObjetoRequisicaoOi(ByVal dsContribuinte As DataSet, ByVal form As String) As List(Of CContribuinte)

        Try

            Dim row As DataRow
            Dim contribuintes As List(Of CContribuinte) = New List(Of CContribuinte)


            If IsNothing(dsContribuinte) Then
                Return Nothing
            End If

            If (dsContribuinte.Tables(0).Rows.Count > 0) Then
                For Each row In dsContribuinte.Tables(0).Rows

                    contribuintes.Add(ConverteParaListRequisicaoOi(row))

                Next

                Return contribuintes
            Else
                Return contribuintes
            End If
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function Salvar(ByVal contribuinte As CContribuinte) As Boolean
        Try

            If daoContribuinte.Salvar(contribuinte) = True Then

                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            'MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function SalvarCemig(ByVal contribuinte As CContribuinte) As Boolean
        Try

            If daoContribuinte.SalvarCemig(contribuinte) = True Then

                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            'MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function Excluir(ByVal cod_ean As String, ByVal DDD As String, ByVal Telefone As String, ByVal motivo As String, ByVal solicitante As String, ByVal observacao As String) As Boolean
        Try

            daoContribuinte.Excluir(cod_ean, DDD, Telefone, motivo, solicitante, observacao)

            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function VerificaExistenciaContribuinte(ByVal contribuinte As CContribuinte) As Boolean
        Try

            If (daoContribuinte.VerificaContribuinte(contribuinte)) = True Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function AtualizaContribuinte(ByVal contribuinte As CContribuinte) As Boolean
        Try

            If daoContribuinte.Atualiza(contribuinte) = True Then
                Return True
            Else
                Return False
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function EnviaContribuinteParaCancelamento(ByVal contribuinte As CContribuinte, ByVal operadora As Integer) As Boolean
        Try

            daoContribuinte.EnviaContribuinteParaCancelamento(contribuinte, operadora)

            Return True
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function EnviaContribuinteParaCancelamentoMensal(ByVal contribuinte As CContribuinte) As Boolean
        Try

            daoContribuinte.EnviaContribuinteParaCancelamentoMensal(contribuinte)

            Return True
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function EnviaContribuinteCemigParaCancelamentoMensal(ByVal contribuinte As CContribuinte) As Boolean
        Try

            daoContribuinte.EnviaContribuinteCemigParaCancelamentoMensal(contribuinte)

            Return True
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function RetornaNovoCodigo(isOi As Boolean) As Integer
        Dim cod As Integer
        Try

            cod = daoContribuinte.RetornaNovoCodigoCliente(isOi)

            Return cod

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function AtualizaTabelaContribuinte() As Boolean
        Try

            If daoContribuinte.AtualizaTabelaCliente() = True Then
                Return True
            Else
                Return False
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Private Function ConvertePrimeiraLinhaContribuinte() As CContribuinte


        Dim objContribuinte As CContribuinte = New CContribuinte()


        objContribuinte.CodCliente = "-1"
        objContribuinte.NomeCliente1 = "Selecione..."


        Return objContribuinte

    End Function


    Private Function ConverteParaObjetoContribuinte(ByVal dsContribuinte As DataSet, ByVal form As String) As CContribuinte

        Try


            Dim row As DataRow
            Dim objContribuinte As CContribuinte = New CContribuinte()

            If Not form = "Consulta" Then
                ConvertePrimeiraLinhaContribuinte()
            End If

            If (dsContribuinte.Tables.Count > 0) Then

                For Each row In dsContribuinte.Tables(0).Rows

                    objContribuinte.CodCliente = row.Item("cod_cliente").ToString()
                    objContribuinte.Cod_EAN_Cliente = row.Item("cod_ean_cliente").ToString()
                    objContribuinte.NomeCliente1 = row.Item("nome_cliente").ToString()
                    objContribuinte.NomeCliente2 = row.Item("nome_cliente2").ToString()
                    objContribuinte.Operador = row.Item("operador").ToString()
                    objContribuinte.CNPJ_CPF = row.Item("cnpj_cpf").ToString()
                    objContribuinte.IE_CI = row.Item("ie_ci").ToString()
                    objContribuinte.Endereco = row.Item("endereco").ToString()
                    objContribuinte.Bairro = row.Item("bairro").ToString()
                    objContribuinte.Cidade = row.Item("cidade").ToString()
                    objContribuinte.UF = row.Item("uf").ToString()
                    objContribuinte.CEP = row.Item("cep").ToString()
                    objContribuinte.Telefone1 = row.Item("telefone1").ToString()
                    objContribuinte.Telefone2 = row.Item("telefone2").ToString()
                    objContribuinte.Email = row.Item("email").ToString()
                    objContribuinte.DataCadastro = row.Item("data_cadastro").ToString()
                    objContribuinte.DataNascimento = row.Item("data_nascimento").ToString()
                    objContribuinte.Cod_Categoria = row.Item("cod_categoria").ToString()
                    objContribuinte.Observacao = row.Item("observacao").ToString()
                    objContribuinte.Referencia = row.Item("referencia").ToString()
                    objContribuinte.Descontinuado = row.Item("descontinuado").ToString()
                    objContribuinte.Cod_grupo = row.Item("cod_grupo").ToString()
                    objContribuinte.Valor = row.Item("valor").ToString()
                    objContribuinte.Cod_Especie = row.Item("cod_especie").ToString()
                    objContribuinte.DiaLimite = row.Item("dia_limite").ToString()
                    objContribuinte.Dt_ligar = row.Item("data_ligar").ToString()
                    objContribuinte.Dt_inativo = row.Item("data_inativo").ToString()
                    objContribuinte.Cod_empresa = row.Item("cod_empresa").ToString()
                    objContribuinte.Cod_Cidade = row.Item("cod_cidade").ToString()
                    objContribuinte.Cod_Bairro = row.Item("cod_bairro").ToString()
                    objContribuinte.Cod_Operador = row.Item("cod_operador").ToString()
                    objContribuinte.Tel_desligado = row.Item("telefone_deslig").ToString()
                    objContribuinte.Nao_pedir_extra = row.Item("nao_pedir_extra").ToString()
                    objContribuinte.Cod_Usuario = row.Item("cod_usuario").ToString()
                    objContribuinte.DT_NC_MP = row.Item("data_nc_mp").ToString()
                    objContribuinte.Nao_Pedir_Mensal = row.Item("nao_pedir_mensal").ToString()
                    objContribuinte.DT_Reajuste = row.Item("data_reajuste").ToString()
                    objContribuinte.Cod_Regiao = row.Item("cod_regiao").ToString()
                    objContribuinte.DDD = row.Item("DDD").ToString()
                    objContribuinte.ClienteOi = row.Item("ClienteOi").ToString()


                Next

            End If

            Return objContribuinte

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function




    Private Function ConverteParaObjetoContribuinte2(ByVal dsContribuinte As DataSet, ByVal cod_Cliente As Integer) As List(Of CContribuinte)

        Dim row As DataRow
        Dim contribuintes As List(Of CContribuinte) = New List(Of CContribuinte)



        For Each row In dsContribuinte.Tables(0).Rows

            contribuintes.Add(ConverteParaListContribuinte(row))

        Next

        Return contribuintes

    End Function


    Public Function RetornaTelefonesListaExclusao(ByVal form As String) As List(Of CContribuinte)

        Return ConverteParaObjetoContribuinteExclusao(daoContribuinte.CarregaListaTelefoneExclusao())


    End Function




    Private Function ConverteParaObjetoContribuinteExclusao(ByVal dsContribuinte As DataSet) As List(Of CContribuinte)

        Dim row As DataRow
        Dim contribuintes As List(Of CContribuinte) = New List(Of CContribuinte)



        For Each row In dsContribuinte.Tables(0).Rows

            contribuintes.Add(ConverteParaListContribuinteExclusao(row))

        Next

        Return contribuintes

    End Function

    Private Function ConverteParaListContribuinteExclusao(ByVal rwObjeto As DataRow) As CContribuinte
        Dim objContribuinte As CContribuinte = New CContribuinte()

        Try

            objContribuinte.Telefone1 = IIf(IsDBNull(rwObjeto.Item("Telefone").ToString()), "", rwObjeto.Item("Telefone").ToString())

            Return objContribuinte

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function


    Private Function ConverteParaListContribuinte(ByVal rwObjeto As DataRow) As CContribuinte

        Try


            Dim objContribuinte As CContribuinte = New CContribuinte()

            objContribuinte.CodCliente = rwObjeto.Item("cod_cliente").ToString()
            objContribuinte.Cod_EAN_Cliente = rwObjeto.Item("cod_ean_cliente").ToString()
            objContribuinte.NomeCliente1 = rwObjeto.Item("nome_cliente").ToString()
            objContribuinte.NomeCliente2 = rwObjeto.Item("nome_cliente2").ToString()
            objContribuinte.Operador = rwObjeto.Item("operador").ToString()
            objContribuinte.CNPJ_CPF = rwObjeto.Item("cnpj_cpf").ToString()
            objContribuinte.IE_CI = rwObjeto.Item("ie_ci").ToString()
            objContribuinte.Endereco = rwObjeto.Item("endereco").ToString()
            objContribuinte.Bairro = rwObjeto.Item("bairro").ToString()
            objContribuinte.Cidade = rwObjeto.Item("cidade").ToString()
            objContribuinte.UF = rwObjeto.Item("uf").ToString()
            objContribuinte.CEP = rwObjeto.Item("cep").ToString()
            objContribuinte.Telefone1 = rwObjeto.Item("telefone1").ToString()
            objContribuinte.Telefone2 = rwObjeto.Item("telefone2").ToString()
            objContribuinte.Email = rwObjeto.Item("email").ToString()
            objContribuinte.DataCadastro = rwObjeto.Item("data_cadastro").ToString()
            objContribuinte.DataNascimento = rwObjeto.Item("data_nascimento").ToString()
            objContribuinte.Cod_Categoria = rwObjeto.Item("cod_categoria").ToString()
            objContribuinte.Observacao = rwObjeto.Item("observacao").ToString()
            objContribuinte.Referencia = rwObjeto.Item("referencia").ToString()
            objContribuinte.Descontinuado = rwObjeto.Item("descontinuado").ToString()
            objContribuinte.Cod_grupo = rwObjeto.Item("cod_grupo").ToString()
            objContribuinte.Valor = rwObjeto.Item("valor").ToString()
            objContribuinte.Cod_Especie = rwObjeto.Item("cod_especie").ToString()
            objContribuinte.DiaLimite = rwObjeto.Item("dia_limite").ToString()
            objContribuinte.Dt_ligar = rwObjeto.Item("data_ligar").ToString()
            objContribuinte.Dt_inativo = rwObjeto.Item("data_inativo").ToString()
            objContribuinte.Cod_empresa = rwObjeto.Item("cod_empresa").ToString()
            objContribuinte.Cod_Cidade = rwObjeto.Item("cod_cidade").ToString()
            objContribuinte.Cod_Bairro = rwObjeto.Item("cod_bairro").ToString()
            objContribuinte.Cod_Operador = rwObjeto.Item("cod_operador").ToString()
            objContribuinte.Tel_desligado = rwObjeto.Item("telefone_deslig").ToString()
            objContribuinte.Nao_pedir_extra = rwObjeto.Item("nao_pedir_extra").ToString()
            objContribuinte.Cod_Usuario = rwObjeto.Item("cod_usuario").ToString()
            objContribuinte.DT_NC_MP = rwObjeto.Item("data_nc_mp").ToString()
            objContribuinte.Nao_Pedir_Mensal = rwObjeto.Item("nao_pedir_mensal").ToString()
            objContribuinte.DT_Reajuste = rwObjeto.Item("data_reajuste").ToString()
            objContribuinte.Cod_Regiao = rwObjeto.Item("cod_regiao").ToString()
            objContribuinte.DDD = rwObjeto.Item("DDD").ToString()
            objContribuinte.ClienteOi = rwObjeto.Item("ClienteOi").ToString()


            Return objContribuinte

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function

    Private Function ConverteParaListContribuinteRetido(ByVal rwObjeto As DataRow) As CContribuinte

        Try


            Dim objContribuinte As CContribuinte = New CContribuinte()

            objContribuinte.CodCliente = rwObjeto.Item("cod_cliente").ToString()
            objContribuinte.Cod_EAN_Cliente = rwObjeto.Item("cod_ean_cliente").ToString()
            objContribuinte.NomeCliente1 = rwObjeto.Item("nome_cliente").ToString()
            objContribuinte.NomeCliente2 = rwObjeto.Item("nome_cliente2").ToString()
            objContribuinte.Operador = rwObjeto.Item("operador").ToString()
            objContribuinte.CNPJ_CPF = rwObjeto.Item("cnpj_cpf").ToString()
            objContribuinte.IE_CI = rwObjeto.Item("ie_ci").ToString()
            objContribuinte.Endereco = rwObjeto.Item("endereco").ToString()
            objContribuinte.Bairro = rwObjeto.Item("bairro").ToString()
            objContribuinte.Cidade = rwObjeto.Item("cidade").ToString()
            objContribuinte.UF = rwObjeto.Item("uf").ToString()
            objContribuinte.CEP = rwObjeto.Item("cep").ToString()
            objContribuinte.Telefone1 = rwObjeto.Item("telefone1").ToString()
            objContribuinte.Telefone2 = rwObjeto.Item("telefone2").ToString()
            objContribuinte.Email = rwObjeto.Item("email").ToString()
            objContribuinte.DataCadastro = rwObjeto.Item("data_cad").ToString()
            objContribuinte.DataNascimento = rwObjeto.Item("data_nasc").ToString()
            objContribuinte.Cod_Categoria = rwObjeto.Item("cod_categoria").ToString()
            objContribuinte.Cod_grupo = rwObjeto.Item("cod_grupo").ToString()
            objContribuinte.Valor = rwObjeto.Item("valor").ToString()
            objContribuinte.Cod_Operador = rwObjeto.Item("cod_opera").ToString()
            objContribuinte.Cod_Regiao = rwObjeto.Item("cod_regiao").ToString()



            Return objContribuinte

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function

    Private Function ConverteParaListRequisicaoOi(ByVal rwObjeto As DataRow) As CContribuinte

        Try


            Dim objContribuinte As CContribuinte = New CContribuinte()


            objContribuinte.Cod_EAN_Cliente = IIf(IsDBNull(rwObjeto.Item("cod_ean").ToString()), "", rwObjeto.Item("cod_ean").ToString())
            objContribuinte.DDD = IIf(IsDBNull(rwObjeto.Item("DDD").ToString()), "", rwObjeto.Item("DDD").ToString())
            objContribuinte.Telefone1 = IIf(IsDBNull(rwObjeto.Item("telefone").ToString()), "", rwObjeto.Item("telefone").ToString())
            objContribuinte.Valor = IIf(IsDBNull(rwObjeto.Item("valor").ToString()), "", rwObjeto.Item("valor").ToString())
            objContribuinte.Cod_Operador = IIf(IsDBNull(rwObjeto.Item("operador_id")), 0, rwObjeto.Item("operador_id"))
            objContribuinte.Operador = IIf(IsDBNull(rwObjeto.Item("operador").ToString()), "", rwObjeto.Item("operador").ToString())
            objContribuinte.Cod_Categoria = IIf(IsDBNull(rwObjeto.Item("categoria")), 0, rwObjeto.Item("categoria"))
            objContribuinte.DT_Reajuste = IIf(IsDBNull(rwObjeto.Item("mesRef").ToString()), "", rwObjeto.Item("mesRef").ToString())
            objContribuinte.NomeCliente1 = IIf(IsDBNull(rwObjeto.Item("nome_cliente").ToString()), "", rwObjeto.Item("nome_cliente").ToString())

            Return objContribuinte

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function




    Public Function cargaContribuinte(ByVal contribuinte As CContribuinte, ByVal mesAno As String) As Boolean

        Try

            If (daoContribuinte.CargaRemessa(contribuinte, mesAno) = True) Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False


        End Try

    End Function

    Public Function CargaArquivo(ByVal usuario As String,
                                 ByVal layout_id As Integer,
                                 Optional previsaoRemessa As Boolean = False) As Boolean
        Dim dsArquivo As DataSet
        Dim dsRemessa As DataSet
        Dim nomeArquivo As String = ""
        Dim numeroRemessa As String = ""
        Dim UF As String = ""
        Dim dataEnvio As String = ""
        Dim codigoEmpresa As String


        Try

            'daoContribuinte.BeginTransaction()
            dsArquivo = daoContribuinte.ProcessaRemessaSemCriticados(usuario, layout_id)

            If previsaoRemessa Then Return True

            If dsArquivo.Tables(0).Rows.Count > 0 Then

                dsRemessa = daoContribuinte.VerificaRemessa()

                If dsRemessa.Tables(0).Rows.Count > 0 Then
                    numeroRemessa = dsRemessa.Tables(0).Rows(0).Item(0)
                End If

                If Date.Now.Month = "1" Then
                    numeroRemessa = "00001"
                End If

                codigoEmpresa = "27178"
                If layout_id = 1 Then
                    UF = "MG"
                ElseIf layout_id = 2 Then
                    UF = "RJ"
                ElseIf layout_id = 3 Then
                    UF = "BA"
                End If
                dataEnvio = CDate(Now).ToString("yyyyMMdd")

                nomeArquivo = "ARB6.ACC" + codigoEmpresa + "." + UF + ".REM" + numeroRemessa + "." + dataEnvio + ""


                Dim SW As New StreamWriter("C:\Digital\" + nomeArquivo + "") ' Cria o arquivo de texto

                For Each row In dsArquivo.Tables(1).Rows 'select do header

                    SW.WriteLine(row.ItemArray(0)) ' Grava o Header

                Next

                For Each row In dsArquivo.Tables(2).Rows 'select do detalhe

                    SW.WriteLine(row.ItemArray(0)) ' Grava o detalhe

                Next

                For Each row In dsArquivo.Tables(3).Rows 'select do trailler

                    SW.WriteLine(row.ItemArray(0)) ' Grava o trailler

                Next

                SW.Close() 'Fecha o arquivo de texto

                SW.Dispose() 'Libera a memória utilizada

            End If

            'REALIZA EXPURGO DOS DADOS
            If daoContribuinte.ExpurgoDados() = True Then

                'daoContribuinte.CommitTransaction()
                Return True

            End If

            Return True

        Catch ex As Exception

            'daoContribuinte.RollBackTransaction()
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False

        End Try

    End Function

    Public Function ValorTotalRemessa() As Single
        Try
            Dim dsRemessa As DataSet = daoContribuinte.valorTotalRemessa()

            If dsRemessa.Tables(0).Rows.Count > 0 Then
                Return dsRemessa.Tables(0).Rows(0).Item(0)
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Sub ExpurgoDados()
        Try
            daoContribuinte.ExpurgoDados()
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Public Function CarregarGridHistoricoOi(ByVal filtro As CContribuinte, ByVal form As String) As List(Of CContribuinte)

        Return ConverteParaObjetoContribuintes(daoContribuinte.CarregarGridContribuintesOi(filtro), form)


    End Function

    Public Function Atualiza_Requisicao_Recuperacao(ByVal filtro As CContribuinte, ByVal aceite As String) As Boolean

        Try

            Return daoContribuinte.Atualiza_Requisicao_Recuperacao(filtro, aceite)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function Inclui_Numeros_Oi(ByVal filtro As CContribuinte, ByVal aceite As String, ByVal mesAtuacao As String, ByVal mesRef As String, ByVal nome As String) As Boolean
        Try
            Return daoContribuinte.Inclui_Numeros_Oi(filtro, aceite, mesAtuacao, mesRef, nome)
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function Inclui_Comercial_Nao_Contribuiu(filtro As CContribuinte, aceite As String, mesAtuacao As String, mesRef As String) As Boolean
        Try
            Return daoContribuinte.Inclui_Numeros_Requisicao_Comercial_Nao_Contribuiu(filtro, aceite, mesAtuacao, mesRef)
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function Inclui_Numeros_Cemig(filtro As CContribuinte, aceite As String, mesAtuacao As String, mesRef As String, nome As String, numeroInstalacao As String) As Boolean
        Try
            Return daoContribuinte.Inclui_Numeros_Cemig(filtro, aceite, mesAtuacao, mesRef, numeroInstalacao)
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function AnalisaInclusaoRegistroOiRepetido(ByVal filtro As CContribuinte, ByVal form As String) As CContribuinte

        Try
            Dim AnaliseRequisicao As New CContribuinte
            Dim ds As DataSet = New DataSet


            ds = daoContribuinte.AnalisaInclusaoRegistroOiRepetido(filtro)


            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then

                    For Each dr As DataRow In ds.Tables(0).Rows
                        AnaliseRequisicao.Telefone1 = IIf(IsDBNull(dr.Item("telefone")), 0, dr.Item("telefone"))
                        AnaliseRequisicao.Operador = IIf(IsDBNull(dr.Item("operador")), 0, dr.Item("operador"))
                        AnaliseRequisicao.CategoriaOi = IIf(IsDBNull(dr.Item("categoria")), 0, dr.Item("categoria"))
                        AnaliseRequisicao.Valor = IIf(IsDBNull(dr.Item("valor")), 0, dr.Item("valor"))
                        AnaliseRequisicao.Data_mov = IIf(IsDBNull(dr.Item("dt_inclusao")), 0, dr.Item("dt_inclusao"))
                        AnaliseRequisicao.Tipo_mov = IIf(IsDBNull(dr.Item("aceite")), 0, dr.Item("aceite"))

                        If AnaliseRequisicao.Tipo_mov = "S" Then
                            AnaliseRequisicao.Tipo_mov = " um Aceite"
                        Else
                            AnaliseRequisicao.Tipo_mov = " uma Recusa"
                        End If

                        If AnaliseRequisicao.CategoriaOi = "1" Then
                            AnaliseRequisicao.CategoriaOi = "Doação"
                        ElseIf AnaliseRequisicao.CategoriaOi = "2" Then
                            AnaliseRequisicao.CategoriaOi = "Lista"
                        ElseIf AnaliseRequisicao.CategoriaOi = "3" Then
                            AnaliseRequisicao.CategoriaOi = "Mensal"
                        ElseIf AnaliseRequisicao.CategoriaOi = "4" Then
                            AnaliseRequisicao.CategoriaOi = "Especial"
                        ElseIf AnaliseRequisicao.CategoriaOi = "5" Then
                            AnaliseRequisicao.CategoriaOi = "Avulso"
                        ElseIf AnaliseRequisicao.CategoriaOi = "6" Then
                            AnaliseRequisicao.CategoriaOi = "Mensal Primeira"
                        ElseIf AnaliseRequisicao.CategoriaOi = "7" Then
                            AnaliseRequisicao.CategoriaOi = "Avulso B"
                        ElseIf AnaliseRequisicao.CategoriaOi = "8" Then
                            AnaliseRequisicao.CategoriaOi = "Inativo"
                        ElseIf AnaliseRequisicao.CategoriaOi = "9" Then
                            AnaliseRequisicao.CategoriaOi = "Recuperação"
                        ElseIf AnaliseRequisicao.CategoriaOi = "10" Then
                            AnaliseRequisicao.CategoriaOi = "Lista Nova"
                        ElseIf AnaliseRequisicao.CategoriaOi = "11" Then
                            AnaliseRequisicao.CategoriaOi = "Lista B"
                        ElseIf AnaliseRequisicao.CategoriaOi = "12" Then
                            AnaliseRequisicao.CategoriaOi = "Pedir Mensal"
                        ElseIf AnaliseRequisicao.CategoriaOi = "13" Then
                            AnaliseRequisicao.CategoriaOi = "Mensal Retido"
                        ElseIf AnaliseRequisicao.CategoriaOi = "14" Then
                            AnaliseRequisicao.CategoriaOi = "Avulso Retido"
                        ElseIf AnaliseRequisicao.CategoriaOi = "15" Then
                            AnaliseRequisicao.CategoriaOi = "Recupera Retido"
                        End If


                    Next

                Else
                    Return Nothing
                End If
            End If

            Return AnaliseRequisicao


        Catch ex As Exception
            'retorna nothing no dataset
            Return Nothing
        End Try
    End Function

    Public Function AnalisaInclusaoRegistroOi(ByVal filtro As CContribuinte, ByVal form As String) As CContribuinte

        Try
            Dim AnaliseRequisicao As New CContribuinte
            Dim ds As DataSet = New DataSet


            ds = daoContribuinte.AnalisaInclusaoRegistroOi(filtro)


            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then

                    For Each dr As DataRow In ds.Tables(0).Rows
                        AnaliseRequisicao.Telefone1 = IIf(IsDBNull(dr.Item("telefone")), 0, dr.Item("telefone"))
                        AnaliseRequisicao.Operador = IIf(IsDBNull(dr.Item("operador")), 0, dr.Item("operador"))
                        AnaliseRequisicao.CategoriaOi = IIf(IsDBNull(dr.Item("categoria")), 0, dr.Item("categoria"))
                        AnaliseRequisicao.Valor = IIf(IsDBNull(dr.Item("valor")), 0, dr.Item("valor"))
                        AnaliseRequisicao.Data_mov = IIf(IsDBNull(dr.Item("dt_inclusao")), 0, dr.Item("dt_inclusao"))
                        AnaliseRequisicao.Tipo_mov = IIf(IsDBNull(dr.Item("aceite")), 0, dr.Item("aceite"))

                        If AnaliseRequisicao.Tipo_mov = "S" Then
                            AnaliseRequisicao.Tipo_mov = " um Aceite"
                        Else
                            AnaliseRequisicao.Tipo_mov = " uma Recusa"
                        End If

                        If AnaliseRequisicao.CategoriaOi = "1" Then
                            AnaliseRequisicao.CategoriaOi = "Doação"
                        ElseIf AnaliseRequisicao.CategoriaOi = "2" Then
                            AnaliseRequisicao.CategoriaOi = "Lista"
                        ElseIf AnaliseRequisicao.CategoriaOi = "3" Then
                            AnaliseRequisicao.CategoriaOi = "Mensal"
                        ElseIf AnaliseRequisicao.CategoriaOi = "4" Then
                            AnaliseRequisicao.CategoriaOi = "Especial"
                        ElseIf AnaliseRequisicao.CategoriaOi = "5" Then
                            AnaliseRequisicao.CategoriaOi = "Avulso"
                        ElseIf AnaliseRequisicao.CategoriaOi = "6" Then
                            AnaliseRequisicao.CategoriaOi = "Mensal Primeira"
                        ElseIf AnaliseRequisicao.CategoriaOi = "7" Then
                            AnaliseRequisicao.CategoriaOi = "Avulso B"
                        ElseIf AnaliseRequisicao.CategoriaOi = "8" Then
                            AnaliseRequisicao.CategoriaOi = "Inativo"
                        ElseIf AnaliseRequisicao.CategoriaOi = "9" Then
                            AnaliseRequisicao.CategoriaOi = "Recuperação"
                        ElseIf AnaliseRequisicao.CategoriaOi = "10" Then
                            AnaliseRequisicao.CategoriaOi = "Lista Nova"
                        ElseIf AnaliseRequisicao.CategoriaOi = "11" Then
                            AnaliseRequisicao.CategoriaOi = "Lista B"
                        ElseIf AnaliseRequisicao.CategoriaOi = "12" Then
                            AnaliseRequisicao.CategoriaOi = "Pedir Mensal"
                        ElseIf AnaliseRequisicao.CategoriaOi = "13" Then
                            AnaliseRequisicao.CategoriaOi = "Mensal Retido"
                        ElseIf AnaliseRequisicao.CategoriaOi = "14" Then
                            AnaliseRequisicao.CategoriaOi = "Avulso Retido"
                        ElseIf AnaliseRequisicao.CategoriaOi = "15" Then
                            AnaliseRequisicao.CategoriaOi = "Recupera Retido"
                        End If


                    Next

                Else
                    Return Nothing
                End If
            End If

            Return AnaliseRequisicao


        Catch ex As Exception
            'retorna nothing no dataset
            Return Nothing
        End Try
    End Function

    Public Function Atualiza_contribuicao_Oi(ByVal filtro As CContribuinte, ByVal aceite As String, ByVal mesAtuacao As String, ByVal mesRef As String, ByVal nome As String, ByVal RequisicaoIdSelecionado As Integer) As Boolean

        Try

            Return daoContribuinte.AtualizaRequisicaoOi(filtro, aceite, mesAtuacao, mesRef, nome, RequisicaoIdSelecionado)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function Atualiza_contribuicao_Cemig(ByVal filtro As CContribuinte, ByVal aceite As String, ByVal mesAtuacao As String, ByVal mesRef As String, ByVal nome As String, ByVal RequisicaoIdSelecionado As Integer) As Boolean

        Try

            Return daoContribuinte.AtualizaRequisicaoCemig(filtro, aceite, mesAtuacao, mesRef, nome, RequisicaoIdSelecionado)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function Analisar_Aceite_Oi(ByVal filtro As CContribuinte, ByVal valor As Decimal) As String()
        Dim ds As DataSet
        Dim retorno(0 To 1) As String
        Dim media As Decimal
        Dim diferenca As Decimal
        Try

            ds = daoContribuinte.Analisar_Aceite_Oi(filtro, valor)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    media = ds.Tables(0).Rows(0).Item(0)
                    diferenca = ds.Tables(1).Rows(0).Item(0)
                End If
            End If

            retorno(0) = media
            retorno(1) = diferenca

            Return retorno


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function Analisar_Aceite_Cemig(ByVal filtro As CContribuinte, ByVal valor As Decimal) As String()
        Dim ds As DataSet
        Dim retorno(0 To 1) As String
        Dim media As Decimal
        Dim diferenca As Decimal
        Try

            ds = daoContribuinte.Analisar_Aceite_Cemig(filtro, valor)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    media = ds.Tables(0).Rows(0).Item(0)
                    diferenca = ds.Tables(1).Rows(0).Item(0)
                End If
            End If

            retorno(0) = media
            retorno(1) = diferenca

            Return retorno


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function Atualiza_Informacoes_Oi(ByVal filtro As CContribuinte, ByVal mesAtuacao As String, ByVal mesRef As String, ByVal cpf As String, ByVal autorizante As String, ByVal observacao As String, ByVal valorTotal As String) As Boolean

        Try

            Return daoContribuinte.Atualiza_Informacoes_Oi(filtro, mesAtuacao, mesRef, cpf, autorizante, observacao, valorTotal)


        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function Atualiza_Informacoes_Cemig(ByVal filtro As CContribuinte, ByVal mesAtuacao As String, ByVal mesRef As String, ByVal cpf As String, ByVal autorizante As String, ByVal observacao As String, ByVal valorTotal As String) As Boolean
        Try
            Return daoContribuinte.Atualiza_Informacoes_Cemig(filtro, mesAtuacao, mesRef, cpf, autorizante, observacao, valorTotal)
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function Atualiza_UsuarioRegistro(ByVal filtro As CContribuinte, ByVal mesAtuacao As String, ByVal mesRef As String, ByVal cpf As String, ByVal autorizante As String, ByVal observacao As String) As Boolean

        Try

            Return daoContribuinte.Atualiza_UsuarioRegistro(filtro, mesAtuacao, mesRef, cpf, autorizante, observacao)


        Catch ex As Exception
            Return False
        End Try
    End Function


    Public Function Excluir_Numeros_Oi(ByVal filtro As CContribuinte) As Boolean

        Try

            Return daoContribuinte.Excluir_Numeros_Oi(filtro)


        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function Excluir_Requisicao_Oi(ByVal filtro As CContribuinte, ByVal mesRef As String) As Boolean

        Try

            Return daoContribuinte.Excluir_Requisicao_Oi(filtro, mesRef)


        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function AtualizaTabelaCliente() As Boolean

        Try

            Return daoContribuinte.AtualizaTabelaCliente()


        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function AtualizaRequisicaoOi(ByVal filtro As CContribuinte, ByVal aceite As String, ByVal mesAtuacao As String, ByVal mesRef As String, ByVal nome As String, ByVal requisicao_id As Integer) As Boolean

        Try

            Return daoContribuinte.AtualizaRequisicaoOi(filtro, aceite, mesAtuacao, mesRef, nome, requisicao_id)


        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function ConsultaContribuintePorCategoria(ByVal cod_categoria As Integer) As DataSet
        Dim daoContribuinte As ContribuinteDAO = New ContribuinteDAO

        Try

            Return daoContribuinte.ConsultaContribuintePorCategoria(cod_categoria)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function ConsultaContribuintePorCidade(ByVal cidade As String) As DataSet
        Dim daoContribuinte As ContribuinteDAO = New ContribuinteDAO

        Try

            Return daoContribuinte.ConsultaContribuintePorCidade(cidade)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregaListaTelefoneExclusao() As List(Of CContribuinte)

        Return ConverteParaObjetoContribuinteExclusao(daoContribuinte.CarregaListaTelefoneExclusao())

    End Function


    Public Function RodarExclusao(ByVal contribuinte As CContribuinte) As Boolean
        Try

            daoContribuinte.RodarExclusao(contribuinte)

            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function RetornaNomeContribuinte(ByVal contribuinte As CContribuinte) As String
        Try

            Dim nome As String = ""

            nome = daoContribuinte.RetornaNomeContribuinte(contribuinte)

            Return nome

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function Busca_TelefoneExclusao(ByVal telefone As String, ByVal DDD As String) As DataSet
        Dim daogenerico As New GenericoDAO

        Try

            Return daogenerico.Busca_Informações_Exclusao(telefone, DDD)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


#End Region




#Region "Lista Nova"

    Public Function CarregaFichasListaNova(ByVal cidade As String, ByVal cod As Integer) As List(Of CContribuinte)
        Dim daocontribuicao As ContribuinteDAO = New ContribuinteDAO

        Try

            'If cidade = "ANTONIO CARLOS" Then
            '    cidade = "clientes_lista_acok_tb"

            'ElseIf cidade = "BARBACENA" Then
            '    cidade = "clientes_lista_barbok_tb"

            'ElseIf cidade = "BICAS" Then
            '    cidade = "clientes_lista_biok_tb"

            'ElseIf cidade = "CATAGUASES" Then
            '    cidade = "clientes_lista_caok_tb"

            'ElseIf cidade = "CARANDAI" Then
            '    cidade = "clientes_lista_ciok_tb"

            'ElseIf cidade = "DONA EUSEBIA" Then
            '    cidade = "clientes_lista_deok_tb"

            'ElseIf cidade = "DIVINESIA" Then
            '    cidade = "clientes_lista_diok_tb"

            'ElseIf cidade = "ITAMARATY" Then
            '    cidade = "clientes_lista_itok_tb"

            'ElseIf cidade = "JUIZ DE FORA" Then
            '    cidade = "clientes_lista_jfok_tb"

            'ElseIf cidade = "RIO NOVO" Then
            '    cidade = "clientes_lista_rnok_tb"

            'ElseIf cidade = "SANTOS DUMONT" Then
            '    cidade = "clientes_lista_sdok_tb"

            'ElseIf cidade = "UBA" Then
            '    cidade = "clientes_lista_ubaok_tb"

            'End If

            'Return ConverteParaObjetoListaNova(daocontribuicao.CarregaListaNova(cidade, cod))
            Return Nothing
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregaFichaListaNova(ByVal cidade As String, ByVal cod As Integer) As List(Of CContribuinte)
        Dim daocontribuicao As ContribuinteDAO = New ContribuinteDAO

        Try

            If cidade = "ANTONIO CARLOS" Then
                cidade = "clientes_lista_acok_tb"

            ElseIf cidade = "BARBACENA" Then
                cidade = "clientes_lista_barbok_tb"

            ElseIf cidade = "BICAS" Then
                cidade = "clientes_lista_biok_tb"

            ElseIf cidade = "CATAGUASES" Then
                cidade = "clientes_lista_caok_tb"

            ElseIf cidade = "CARANDAI" Then
                cidade = "clientes_lista_ciok_tb"

            ElseIf cidade = "DONA EUSEBIA" Then
                cidade = "clientes_lista_deok_tb"

            ElseIf cidade = "DIVINESIA" Then
                cidade = "clientes_lista_diok_tb"

            ElseIf cidade = "ITAMARATY" Then
                cidade = "clientes_lista_itok_tb"

            ElseIf cidade = "JUIZ DE FORA" Then
                cidade = "clientes_lista_jfok_tb"

            ElseIf cidade = "RIO NOVO" Then
                cidade = "clientes_lista_rnok_tb"

            ElseIf cidade = "SANTOS DUMONT" Then
                cidade = "clientes_lista_sdok_tb"

            ElseIf cidade = "UBA" Then
                cidade = "clientes_lista_ubaok_tb"

            End If

            Return ConverteParaObjetoListaNova(daocontribuicao.CarregaListaNova(cidade, cod))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Private Function ConverteParaObjetoListaNova(ByVal dsRequisicao As DataSet) As List(Of CContribuinte)
        Dim row As DataRow
        Dim fichas As List(Of CContribuinte) = New List(Of CContribuinte)

        Try

            For Each row In dsRequisicao.Tables(0).Rows

                fichas.Add(ConverteParaListListaNova(row))

            Next

            Return fichas

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing

        End Try

    End Function

    Private Function ConverteParaListListaNova(ByVal rwObjeto As DataRow) As CContribuinte
        Dim objRequisicao As CContribuinte = New CContribuinte()

        Try

            objRequisicao.Cod_contribuinte = IIf(IsDBNull(rwObjeto.Item("Cod").ToString()), "", rwObjeto.Item("Cod").ToString())
            objRequisicao.Nome_contribuinte = IIf(IsDBNull(rwObjeto.Item("Nome_cliente").ToString()), "", rwObjeto.Item("Nome_cliente").ToString())
            objRequisicao.NomeCliente1 = IIf(IsDBNull(rwObjeto.Item("Nome_cliente").ToString()), "", rwObjeto.Item("Nome_cliente").ToString())
            objRequisicao.NomeCliente2 = IIf(IsDBNull(rwObjeto.Item("Nome_cliente").ToString()), "", rwObjeto.Item("Nome_cliente").ToString())
            objRequisicao.Endereco = IIf(IsDBNull(rwObjeto.Item("endereco")), 0, rwObjeto.Item("endereco"))
            objRequisicao.Bairro = IIf(IsDBNull(rwObjeto.Item("bairro").ToString()), "", rwObjeto.Item("bairro").ToString())
            objRequisicao.Cidade = IIf(IsDBNull(rwObjeto.Item("Cidade").ToString()), "", rwObjeto.Item("Cidade").ToString())
            objRequisicao.UF = IIf(IsDBNull(rwObjeto.Item("uf").ToString()), "", rwObjeto.Item("uf").ToString())
            objRequisicao.CEP = IIf(IsDBNull(rwObjeto.Item("cep").ToString()), "", rwObjeto.Item("cep").ToString())
            objRequisicao.Telefone1 = IIf(IsDBNull(rwObjeto.Item("telefone1").ToString()), "", rwObjeto.Item("telefone1").ToString())
            objRequisicao.Cod_empresa = IIf(IsDBNull(rwObjeto.Item("Cod_empresa")), 0, rwObjeto.Item("Cod_empresa"))
            objRequisicao.Cod_Cidade = IIf(IsDBNull(rwObjeto.Item("Cod_Cidade")), 0, rwObjeto.Item("Cod_Cidade"))
            objRequisicao.Cod_Bairro = IIf(IsDBNull(rwObjeto.Item("Cod_Bairro")), 0, rwObjeto.Item("Cod_Bairro"))

            Return objRequisicao

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function

    Public Function AtualizaFichaListaNova(ByVal cidade As String, ByVal cod As Integer) As Boolean
        Dim daocontribuicao As ContribuinteDAO = New ContribuinteDAO

        Try

            If cidade = "ANTONIO CARLOS" Then
                cidade = "clientes_lista_acok_tb"

            ElseIf cidade = "BARBACENA" Then
                cidade = "clientes_lista_barbok_tb"

            ElseIf cidade = "BICAS" Then
                cidade = "clientes_lista_biok_tb"

            ElseIf cidade = "CATAGUASES" Then
                cidade = "clientes_lista_caok_tb"

            ElseIf cidade = "CARANDAI" Then
                cidade = "clientes_lista_ciok_tb"

            ElseIf cidade = "DONA EUSEBIA" Then
                cidade = "clientes_lista_deok_tb"

            ElseIf cidade = "DIVINESIA" Then
                cidade = "clientes_lista_diok_tb"

            ElseIf cidade = "ITAMARATY" Then
                cidade = "clientes_lista_itok_tb"

            ElseIf cidade = "JUIZ DE FORA" Then
                cidade = "clientes_lista_jfok_tb"

            ElseIf cidade = "RIO NOVO" Then
                cidade = "clientes_lista_rnok_tb"

            ElseIf cidade = "SANTOS DUMONT" Then
                cidade = "clientes_lista_sdok_tb"

            ElseIf cidade = "UBA" Then
                cidade = "clientes_lista_ubaok_tb"

            End If

            If daocontribuicao.AtualizaFichaListaNova(cidade, cod) = True Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregaFichasListaNovaOi(uf As String) As List(Of CContribuinte)
        Dim daoRequisicao As RequisicaoDAO = New RequisicaoDAO

        Try

            Return ConverteParaObjetoListaNova(daoRequisicao.CarregaListaNovaOi(uf))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

#End Region

#Region "Contatos Marketing"

    Public Sub Inclui_Contatos(ByVal filtro As CContribuinte, ByVal mesRef As String, ByVal email As String, ByVal WhatsApp As String)

        Try

            daoContribuinte.Inclui_Contatos(filtro, mesRef, email, WhatsApp)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Public Function RetornaEmailsEnviar() As DataSet

        Try

            Return daoContribuinte.RetornaEmailsEnviar()


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function


    Public Function RetornaDadosMarketing(ByVal ddd As String, ByVal telefone As String) As DataSet
        Try

            Return daoContribuinte.RetornaDadosMarketing(ddd, telefone)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


#End Region

#Region "cemig"
    Public Function PesquisarPorCodBarraCemig(ByVal cod_EAN As String) As CContribuinte

        Return ConverteParaObjetoContribuinte(daoContribuinte.PesquisarPorCodEANCemig(cod_EAN), "Contribuinte")

    End Function

    Public Function CarregarContribuintePorTelefoneCemig(ByVal telefone As String, ByVal DDD As String, ByVal cod_ean As String, ByVal form As String) As CContribuinte

        Return ConverteParaObjetoContribuinteCemig(daoContribuinte.PesquisarPorTelefoneCemig(telefone, DDD, cod_ean, 1), form)

    End Function

    Public Function BuscaListaTelefonicaCemig(ByVal telefone As String, ByVal ddd As String, ByVal cod_ean As String) As CContribuinte

        Return ConverteParaListLista(daoContribuinte.Busca_Lista_TelefonicaCemig(telefone, ddd, cod_ean))

    End Function

    Public Function RetornaNomeContribuinteCemig(ByVal contribuinte As CContribuinte) As String
        Try

            Dim nome As String = ""

            nome = daoContribuinte.RetornaNomeContribuinteCemig(contribuinte)

            Return nome

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function AnalisaInclusaoRegistroCemig(ByVal filtro As CContribuinte, ByVal form As String) As CContribuinte

        Try
            Dim AnaliseRequisicao As New CContribuinte
            Dim ds As DataSet = New DataSet


            ds = daoContribuinte.AnalisaInclusaoRegistroCemig(filtro)


            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then

                    For Each dr As DataRow In ds.Tables(0).Rows
                        AnaliseRequisicao.Telefone1 = IIf(IsDBNull(dr.Item("telefone")), 0, dr.Item("telefone"))
                        AnaliseRequisicao.Operador = IIf(IsDBNull(dr.Item("operador")), 0, dr.Item("operador"))
                        AnaliseRequisicao.CategoriaOi = IIf(IsDBNull(dr.Item("categoria")), 0, dr.Item("categoria"))
                        AnaliseRequisicao.Valor = IIf(IsDBNull(dr.Item("valor")), 0, dr.Item("valor"))
                        AnaliseRequisicao.Data_mov = IIf(IsDBNull(dr.Item("dt_inclusao")), 0, dr.Item("dt_inclusao"))
                        AnaliseRequisicao.Tipo_mov = IIf(IsDBNull(dr.Item("aceite")), 0, dr.Item("aceite"))

                        If AnaliseRequisicao.Tipo_mov = "S" Then
                            AnaliseRequisicao.Tipo_mov = " um Aceite"
                        Else
                            AnaliseRequisicao.Tipo_mov = " uma Recusa"
                        End If

                        If AnaliseRequisicao.CategoriaOi = "1" Then
                            AnaliseRequisicao.CategoriaOi = "Doação"
                        ElseIf AnaliseRequisicao.CategoriaOi = "2" Then
                            AnaliseRequisicao.CategoriaOi = "Lista"
                        ElseIf AnaliseRequisicao.CategoriaOi = "3" Then
                            AnaliseRequisicao.CategoriaOi = "Mensal"
                        ElseIf AnaliseRequisicao.CategoriaOi = "4" Then
                            AnaliseRequisicao.CategoriaOi = "Especial"
                        ElseIf AnaliseRequisicao.CategoriaOi = "5" Then
                            AnaliseRequisicao.CategoriaOi = "Avulso"
                        ElseIf AnaliseRequisicao.CategoriaOi = "6" Then
                            AnaliseRequisicao.CategoriaOi = "Mensal Primeira"
                        ElseIf AnaliseRequisicao.CategoriaOi = "7" Then
                            AnaliseRequisicao.CategoriaOi = "Avulso B"
                        ElseIf AnaliseRequisicao.CategoriaOi = "8" Then
                            AnaliseRequisicao.CategoriaOi = "Inativo"
                        ElseIf AnaliseRequisicao.CategoriaOi = "9" Then
                            AnaliseRequisicao.CategoriaOi = "Recuperação"
                        ElseIf AnaliseRequisicao.CategoriaOi = "10" Then
                            AnaliseRequisicao.CategoriaOi = "Lista Nova"
                        ElseIf AnaliseRequisicao.CategoriaOi = "11" Then
                            AnaliseRequisicao.CategoriaOi = "Lista B"
                        ElseIf AnaliseRequisicao.CategoriaOi = "12" Then
                            AnaliseRequisicao.CategoriaOi = "Pedir Mensal"
                        ElseIf AnaliseRequisicao.CategoriaOi = "13" Then
                            AnaliseRequisicao.CategoriaOi = "Mensal Retido"
                        ElseIf AnaliseRequisicao.CategoriaOi = "14" Then
                            AnaliseRequisicao.CategoriaOi = "Avulso Retido"
                        ElseIf AnaliseRequisicao.CategoriaOi = "15" Then
                            AnaliseRequisicao.CategoriaOi = "Recupera Retido"
                        End If


                    Next

                Else
                    Return Nothing
                End If
            End If

            Return AnaliseRequisicao


        Catch ex As Exception
            'retorna nothing no dataset
            Return Nothing
        End Try
    End Function

    Private Function ConverteParaObjetoContribuinteCemig(ByVal dsContribuinte As DataSet, ByVal form As String) As CContribuinte

        Try


            Dim row As DataRow
            Dim objContribuinte As CContribuinte = New CContribuinte()

            If Not form = "Consulta" Then
                ConvertePrimeiraLinhaContribuinte()
            End If

            If (dsContribuinte.Tables.Count > 0) Then

                For Each row In dsContribuinte.Tables(0).Rows

                    objContribuinte.CodCliente = row.Item("cod_cliente").ToString()
                    objContribuinte.Cod_EAN_Cliente = row.Item("cod_ean_cliente").ToString()
                    objContribuinte.NomeCliente1 = row.Item("nome_cliente").ToString()
                    objContribuinte.NomeCliente2 = row.Item("nome_cliente2").ToString()
                    objContribuinte.Operador = row.Item("operador").ToString()
                    objContribuinte.CNPJ_CPF = row.Item("cnpj_cpf").ToString()
                    objContribuinte.IE_CI = row.Item("ie_ci").ToString()
                    objContribuinte.Endereco = row.Item("endereco").ToString()
                    objContribuinte.Bairro = row.Item("bairro").ToString()
                    objContribuinte.Cidade = row.Item("cidade").ToString()
                    objContribuinte.UF = row.Item("uf").ToString()
                    objContribuinte.CEP = row.Item("cep").ToString()
                    objContribuinte.Telefone1 = row.Item("telefone1").ToString()
                    objContribuinte.Telefone2 = row.Item("telefone2").ToString()
                    objContribuinte.Email = row.Item("email").ToString()
                    objContribuinte.DataCadastro = row.Item("data_cadastro").ToString()
                    objContribuinte.DataNascimento = row.Item("data_nascimento").ToString()
                    objContribuinte.Cod_Categoria = row.Item("cod_categoria").ToString()
                    objContribuinte.Observacao = row.Item("observacao").ToString()
                    objContribuinte.Referencia = row.Item("referencia").ToString()
                    objContribuinte.Descontinuado = row.Item("descontinuado").ToString()
                    objContribuinte.Cod_grupo = row.Item("cod_grupo").ToString()
                    objContribuinte.Valor = row.Item("valor").ToString()
                    objContribuinte.Cod_Especie = row.Item("cod_especie").ToString()
                    objContribuinte.DiaLimite = row.Item("dia_limite").ToString()
                    objContribuinte.Dt_ligar = row.Item("data_ligar").ToString()
                    objContribuinte.Dt_inativo = row.Item("data_inativo").ToString()
                    objContribuinte.Cod_empresa = row.Item("cod_empresa").ToString()
                    objContribuinte.Cod_Cidade = row.Item("cod_cidade").ToString()
                    objContribuinte.Cod_Bairro = row.Item("cod_bairro").ToString()
                    objContribuinte.Cod_Operador = row.Item("cod_operador").ToString()
                    objContribuinte.Tel_desligado = row.Item("telefone_deslig").ToString()
                    objContribuinte.Nao_pedir_extra = row.Item("nao_pedir_extra").ToString()
                    objContribuinte.Cod_Usuario = row.Item("cod_usuario").ToString()
                    objContribuinte.DT_NC_MP = row.Item("data_nc_mp").ToString()
                    objContribuinte.Nao_Pedir_Mensal = row.Item("nao_pedir_mensal").ToString()
                    objContribuinte.DT_Reajuste = row.Item("data_reajuste").ToString()
                    objContribuinte.Cod_Regiao = row.Item("cod_regiao").ToString()
                    objContribuinte.DDD = row.Item("DDD").ToString()
                    objContribuinte.Numero_Instalacao = row.Item("Numero_instalacao").ToString()
                    objContribuinte.Telefone3 = row.Item("Telefone3").ToString()
                    objContribuinte.Telefone4 = row.Item("Telefone4").ToString()
                    objContribuinte.Telefone5 = row.Item("Telefone5").ToString()
                    objContribuinte.Telefone6 = row.Item("Telefone6").ToString()
                    objContribuinte.Telefone7 = row.Item("Telefone7").ToString()
                    objContribuinte.TelefonePrincipal = row.Item("Telefone_principal").ToString()



                Next

            End If

            Return objContribuinte

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function

    Public Function AtualizaContribuinteCemig(contribuinte As CContribuinte) As Boolean
        Try
            If daoContribuinte.AtualizaCemig(contribuinte) = True Then
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function VerificaExistenciaContribuinteCemig(contribuinte As CContribuinte) As Boolean
        Try

            If (daoContribuinte.VerificaContribuinteCemig(contribuinte)) = True Then
                Return True
            Else
                Return False
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function CarregarInclusaoManualCemig(ByVal cod As String, ByVal form As String) As List(Of CContribuinte)

        Return ConverteParaObjetoContribuintesCemig(daoContribuinte.ConsultaCodClienteCemig(cod), form)


    End Function

    Private Function ConverteParaObjetoContribuintesCemig(ByVal dsContribuinte As DataSet, ByVal form As String) As List(Of CContribuinte)

        Try

            Dim row As DataRow
            Dim contribuintes As List(Of CContribuinte) = New List(Of CContribuinte)

            If Not form = "Remessa" Then
                contribuintes.Add(ConvertePrimeiraLinhaContribuinte())
            End If

            If IsNothing(dsContribuinte) Then
                Return contribuintes
            End If

            If (dsContribuinte.Tables(0).Rows.Count > 0) Then
                For Each row In dsContribuinte.Tables(0).Rows

                    contribuintes.Add(ConverteParaListContribuinteCemig(row))

                Next

                Return contribuintes
            Else
                Return contribuintes
            End If
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Private Function ConverteParaListContribuinteCemig(ByVal rwObjeto As DataRow) As CContribuinte

        Try


            Dim objContribuinte As CContribuinte = New CContribuinte()

            objContribuinte.CodCliente = rwObjeto.Item("cod_cliente").ToString()
            objContribuinte.Cod_EAN_Cliente = rwObjeto.Item("cod_ean_cliente").ToString()
            objContribuinte.NomeCliente1 = rwObjeto.Item("nome_cliente").ToString()
            objContribuinte.NomeCliente2 = rwObjeto.Item("nome_cliente2").ToString()
            objContribuinte.Operador = rwObjeto.Item("operador").ToString()
            objContribuinte.CNPJ_CPF = rwObjeto.Item("cnpj_cpf").ToString()
            objContribuinte.IE_CI = rwObjeto.Item("ie_ci").ToString()
            objContribuinte.Endereco = rwObjeto.Item("endereco").ToString()
            objContribuinte.Bairro = rwObjeto.Item("bairro").ToString()
            objContribuinte.Cidade = rwObjeto.Item("cidade").ToString()
            objContribuinte.UF = rwObjeto.Item("uf").ToString()
            objContribuinte.CEP = rwObjeto.Item("cep").ToString()
            objContribuinte.Telefone1 = rwObjeto.Item("telefone1").ToString()
            objContribuinte.Telefone2 = rwObjeto.Item("telefone2").ToString()
            objContribuinte.Email = rwObjeto.Item("email").ToString()
            objContribuinte.DataCadastro = rwObjeto.Item("data_cadastro").ToString()
            objContribuinte.DataNascimento = rwObjeto.Item("data_nascimento").ToString()
            objContribuinte.Cod_Categoria = rwObjeto.Item("cod_categoria").ToString()
            objContribuinte.Observacao = rwObjeto.Item("observacao").ToString()
            objContribuinte.Referencia = rwObjeto.Item("referencia").ToString()
            objContribuinte.Descontinuado = rwObjeto.Item("descontinuado").ToString()
            objContribuinte.Cod_grupo = rwObjeto.Item("cod_grupo").ToString()
            objContribuinte.Valor = rwObjeto.Item("valor").ToString()
            objContribuinte.Cod_Especie = rwObjeto.Item("cod_especie").ToString()
            objContribuinte.DiaLimite = rwObjeto.Item("dia_limite").ToString()
            objContribuinte.Dt_ligar = rwObjeto.Item("data_ligar").ToString()
            objContribuinte.Dt_inativo = rwObjeto.Item("data_inativo").ToString()
            objContribuinte.Cod_empresa = rwObjeto.Item("cod_empresa").ToString()
            objContribuinte.Cod_Cidade = rwObjeto.Item("cod_cidade").ToString()
            objContribuinte.Cod_Bairro = rwObjeto.Item("cod_bairro").ToString()
            objContribuinte.Cod_Operador = rwObjeto.Item("cod_operador").ToString()
            objContribuinte.Tel_desligado = rwObjeto.Item("telefone_deslig").ToString()
            objContribuinte.Nao_pedir_extra = rwObjeto.Item("nao_pedir_extra").ToString()
            objContribuinte.Cod_Usuario = rwObjeto.Item("cod_usuario").ToString()
            objContribuinte.DT_NC_MP = rwObjeto.Item("data_nc_mp").ToString()
            objContribuinte.Nao_Pedir_Mensal = rwObjeto.Item("nao_pedir_mensal").ToString()
            objContribuinte.DT_Reajuste = rwObjeto.Item("data_reajuste").ToString()
            objContribuinte.Cod_Regiao = rwObjeto.Item("cod_regiao").ToString()
            objContribuinte.DDD = rwObjeto.Item("DDD").ToString()
            objContribuinte.Numero_Instalacao = rwObjeto.Item("Numero_instalacao").ToString()
            objContribuinte.Telefone3 = rwObjeto.Item("Telefone3").ToString()
            objContribuinte.Telefone4 = rwObjeto.Item("Telefone4").ToString()
            objContribuinte.Telefone5 = rwObjeto.Item("Telefone5").ToString()
            objContribuinte.Telefone6 = rwObjeto.Item("Telefone6").ToString()
            objContribuinte.Telefone7 = rwObjeto.Item("Telefone7").ToString()
            objContribuinte.TelefonePrincipal = rwObjeto.Item("Telefone_principal").ToString()


            Return objContribuinte

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function

    Public Function Inclui_Comercial_Nao_ContribuiuCemig(filtro As CContribuinte, aceite As String, mesAtuacao As String, mesRef As String) As Boolean
        Try
            Return daoContribuinte.Inclui_Numeros_Requisicao_Comercial_Nao_ContribuiuCemig(filtro, aceite, mesAtuacao, mesRef)
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function Excluir_Requisicao_Cemig(ByVal filtro As CContribuinte, ByVal mesRef As String) As Boolean

        Try

            Return daoContribuinte.Excluir_Requisicao_Cemig(filtro, mesRef)


        Catch ex As Exception
            Return False
        End Try
    End Function


#End Region

End Class
