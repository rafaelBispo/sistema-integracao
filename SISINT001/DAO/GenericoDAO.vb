﻿Public Class GenericoDAO

#Region "Variáveis"


    Private Cod_Cidade As Integer
    Private Nome_Cidade As String
    Private Uf_Cidade As String

    Private Cod_Bairro As Integer
    Private Nome_Bairro As String
    Private Cep_Bairro As String
    Private Cod_Cidade_Bairro As Integer
    Private Cod_Regiao_Bairro As Integer
    Public cUser As String

    Private ConnectionHelper As Util.ConnectionHelper


#End Region


#Region "Construtores"

    Sub New(Optional ByVal p_Cod_Cidade As Decimal = 0, _
        Optional ByVal p_Nome_Cidade As String = "", _
        Optional ByVal p_Uf_Cidade As String = "", _
        Optional ByVal p_Cod_Bairro As Integer = 0, _
        Optional ByVal p_Nome_Bairro As String = "", _
        Optional ByVal p_Cep_Bairro As Integer = 0, _
        Optional ByVal p_Cod_Cidade_Bairro As Integer = 0, _
        Optional ByVal p_Cod_Regiao_Bairro As Integer = 0)

        Cod_Cidade = p_Cod_Cidade
        Nome_Cidade = p_Nome_Cidade
        Uf_Cidade = p_Uf_Cidade
        Cod_Bairro = p_Cod_Bairro
        Nome_Bairro = p_Nome_Bairro
        Cep_Bairro = p_Cep_Bairro
        Cod_Cidade_Bairro = p_Cod_Cidade_Bairro
        Cod_Regiao_Bairro = p_Cod_Regiao_Bairro


        ConnectionHelper = New Util.ConnectionHelper(System.Reflection.Assembly.GetExecutingAssembly)

    End Sub

    Sub New(ByRef p_connectionHelper As Util.ConnectionHelper, _
           Optional ByVal p_Cod_Cidade As Decimal = 0, _
           Optional ByVal p_Nome_Cidade As String = "", _
           Optional ByVal p_Uf_Cidade As String = "", _
           Optional ByVal p_Cod_Bairro As Integer = 0, _
           Optional ByVal p_Nome_Bairro As String = "", _
           Optional ByVal p_Cep_Bairro As Integer = 0, _
           Optional ByVal p_Cod_Cidade_Bairro As Integer = 0, _
           Optional ByVal p_Cod_Regiao_Bairro As Integer = 0)

        Cod_Cidade = p_Cod_Cidade
        Nome_Cidade = p_Nome_Cidade
        Uf_Cidade = p_Uf_Cidade
        Cod_Bairro = p_Cod_Bairro
        Nome_Bairro = p_Nome_Bairro
        Cep_Bairro = p_Cep_Bairro
        Cod_Cidade_Bairro = p_Cod_Cidade_Bairro
        Cod_Regiao_Bairro = p_Cod_Regiao_Bairro

        ConnectionHelper = p_connectionHelper

    End Sub


#End Region



#Region "metodos"

    Public Function BuscarCidades(ByVal cod_cidade As Integer) As DataSet

        Try
            Dim sSql As String = ""
            sSql = sSql + "select cod_cidade, nome_cidade, UF "
            sSql = sSql + "from controle_db..cidade_tb with (nolock) "
            If cod_cidade <> 0 Then
                sSql = sSql + "where cod_cidade =" & cod_cidade
            End If
            sSql = sSql + " order by nome_cidade"


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function BuscarCidadesCemig(ByVal cod_cidade As Integer) As DataSet

        Try
            Dim sSql As String = ""
            sSql = sSql + "select distinct(a.cod_cidade), a.nome_cidade, a.UF  "
            sSql = sSql + "from integracao_cemig_db..requisicao_lista_tb b (nolock) "
            sSql = sSql + "inner join controle_db..cidade_tb a (nolock) "
            sSql = sSql + "on a.nome_cidade = b.cidade "
            If cod_cidade <> 0 Then
                sSql = sSql + "where b.cod_cidade =" & cod_cidade
            End If
            sSql = sSql + " order by nome_cidade"


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function BuscarBairros(ByVal _cod As Integer) As DataSet

        Try
            Dim sSql As String = ""
            sSql = sSql + "select cod_bairro, nome_bairro, cep, cod_cidade, cod_regiao "
            sSql = sSql + "from controle_db..bairro_tb with (nolock) "
            If _cod <> 0 Then
                sSql = sSql + " where cod_bairro = " & _cod & " order by nome_bairro"
            End If

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function



    Public Function BuscarCategorias(ByVal cod_categoria As Integer) As DataSet

        Try
            Dim sSql As String = ""
            sSql = sSql + "select cod_categoria, nome_categoria "
            sSql = sSql + " from controle_db..categoria_tb with (nolock) "
            If cod_categoria <> 0 Then
                sSql = sSql + "where cod_categoria = " & cod_categoria
            End If
            sSql = sSql + "order by nome_categoria "

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function BuscarCategoriasPorOperador(ByVal operador_id As Integer) As DataSet

        Try
            Dim sSql As String = ""
            sSql = sSql + "select categoria_tb.cod_categoria, categoria_tb.nome_categoria    "
            sSql = sSql + "from controle_db..categoria_tb with (nolock)  "
            sSql = sSql + "inner join integracao_cemig_db..requisicao_tb with (nolock)  "
            sSql = sSql + "on categoria_tb.cod_categoria = requisicao_tb.categoria   "
            sSql = sSql + "where requisicao_tb.atual = 'S' "
            sSql = sSql + "and vinculado_operador = 'S' "
            If operador_id <> 0 Then
                sSql = sSql + " and  requisicao_tb.operador_id = " & operador_id
            End If
            sSql = sSql + "group by categoria_tb.cod_categoria, categoria_tb.nome_categoria   "


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function BuscarCategoriasPorRequisicaoOperador(ByVal operador_id As Integer) As DataSet

        Try
            Dim sSql As String = ""
            sSql = sSql + "select distinct (requisicao_lista_tb.cod_categoria), categoria_tb.nome_categoria     "
            sSql = sSql + " from controle_db..categoria_tb with (nolock)  "
            sSql = sSql + "inner join controle_db..requisicao_lista_tb with (nolock)  "
            sSql = sSql + " on categoria_tb.cod_categoria = requisicao_lista_tb.cod_categoria    "
            If operador_id <> 0 Then
                sSql = sSql + " where requisicao_lista_tb.cod_opera = " & operador_id
            End If

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function BuscarCategoriasPorRequisicaoCemigOperador(ByVal operador_id As Integer) As DataSet

        Try
            Dim sSql As String = ""
            sSql = sSql + "select distinct (requisicao_lista_tb.cod_categoria), categoria_tb.nome_categoria     "
            sSql = sSql + " from controle_db..categoria_tb with (nolock)  "
            sSql = sSql + "inner join integracao_cemig_db..requisicao_lista_tb with (nolock)  "
            sSql = sSql + " on categoria_tb.cod_categoria = requisicao_lista_tb.cod_categoria    "
            If operador_id <> 0 Then
                sSql = sSql + " where requisicao_lista_tb.cod_opera = " & operador_id
            End If

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    Public Function BuscarOperador(ByVal cod_operador As Integer) As DataSet

        Try
            Dim sSql As String = ""
            sSql = sSql + "select cod_ean_funcionario, nome_funcionario " & vbNewLine
            sSql = sSql + " from controle_db..funcionarios_tb with (nolock) " & vbNewLine
            If cod_operador <> 0 Then
                sSql = sSql + "where cod_ean_funcionario = " & cod_operador & vbNewLine
                ' sSql = sSql + "and cargo = 'OPERADORA'"
            End If

            sSql = sSql + " ORDER BY nome_funcionario" & vbNewLine

            'sSql = sSql + " union " & vbNewLine
            'sSql = sSql + "select cod_funcionario, usuario "
            'sSql = sSql + "from controle_db..usuario_tb with (nolock) "
            'sSql = sSql + "where cod_funcionario = 90"
            'sSql = sSql + " order by nome_funcionario " & vbNewLine



            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    'Public Function BuscarOperadorNome(ByVal operador As String) As DataSet

    '    Try
    '        Dim sSql As String = ""
    '        sSql = sSql + "select cod_funcionario, usuario " & vbNewLine
    '        sSql = sSql + " from controle_db..usuario_tb with (nolock) " & vbNewLine
    '        sSql = sSql + "where usuaio = " & operador & vbNewLine
    '        sSql = sSql + " ORDER BY nome_funcionario" & vbNewLine

    '        Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

    '    Catch ex As Exception
    '        Return Nothing
    '    End Try

    'End Function


    Public Function BuscarOperadorDoMes() As DataSet

        Try
            Dim sSql As String = ""
            sSql = sSql + "DECLARE @MesAtual datetime, @primeirodiaMesAtual date, @UltimodiaMesAtual date  " & vbNewLine
            sSql = sSql + "set @MesAtual = getdate()   " & vbNewLine
            sSql = sSql + "SELECT @primeirodiaMesAtual = CONVERT(VARCHAR, getdate()- DAY(@MesAtual) + 1, 103)  " & vbNewLine
            sSql = sSql + "SELECT @UltimodiaMesAtual = DATEADD(d, -DAY(@MesAtual),DATEADD(m,1,@MesAtual)) " & vbNewLine

            sSql = sSql + "select distinct operador as nome_funcionario, operador_id as cod_ean_funcionario  " & vbNewLine
            sSql = sSql + "from controle_db..requisicao_oi_tb  " & vbNewLine
            sSql = sSql + "WHERE CONVERT(DATE, DT_INCLUSAO) BETWEEN @primeirodiaMesAtual AND @UltimodiaMesAtual " & vbNewLine
            sSql = sSql + "and operador <> '' " & vbNewLine
            sSql = sSql + "order by operador " & vbNewLine

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function BuscarOperadorDoMesCemig() As DataSet

        Try
            Dim sSql As String = ""
            sSql = sSql + "DECLARE @MesAtual datetime, @primeirodiaMesAtual date, @UltimodiaMesAtual date  " & vbNewLine
            sSql = sSql + "set @MesAtual = getdate()   " & vbNewLine
            sSql = sSql + "SELECT @primeirodiaMesAtual = CONVERT(VARCHAR, getdate()- DAY(@MesAtual) + 1, 103)  " & vbNewLine
            sSql = sSql + "SELECT @UltimodiaMesAtual = DATEADD(d, -DAY(@MesAtual),DATEADD(m,1,@MesAtual)) " & vbNewLine

            sSql = sSql + "select distinct operador as nome_funcionario, operador_id as cod_ean_funcionario  " & vbNewLine
            sSql = sSql + "from integracao_cemig_db..requisicao_cemig_tb  " & vbNewLine
            sSql = sSql + "WHERE CONVERT(DATE, DT_INCLUSAO) BETWEEN @primeirodiaMesAtual AND @UltimodiaMesAtual " & vbNewLine
            sSql = sSql + "and operador <> '' " & vbNewLine
            sSql = sSql + "order by operador " & vbNewLine

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function




    Public Function BuscarUsuarios(ByVal cod_operador As Integer) As DataSet

        Try
            Dim sSql As String = ""
            sSql = sSql + "select * " & vbNewLine
            sSql = sSql + " from controle_db..usuario_tb with (nolock) " & vbNewLine
            If cod_operador <> 0 Then
                sSql = sSql + "where cod_funcionario = " & cod_operador & vbNewLine
                sSql = sSql + "and cod_acessos = 6" & vbNewLine
            Else
                sSql = sSql + "where cod_acessos = 6" & vbNewLine
            End If
            ' sSql = sSql + " where cargo = 'OPERADORA'"
            sSql = sSql + " order by usuario " & vbNewLine
            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function BuscarCargo(ByVal cod_cargo As Integer) As DataSet

        Try
            Dim sSql As String = ""
            sSql = sSql + "select cod_cargo, nome_cargo  "
            sSql = sSql + " from controle_db..cargo_tb with (nolock) "
            If cod_cargo <> 0 Then
                sSql = sSql + "where cod_cargo = " & cod_cargo
            End If
            sSql = sSql + " order by nome_cargo "

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function BuscarCargoPeloNome(ByVal cargo As String) As DataSet

        Try
            Dim sSql As String = ""
            sSql = sSql + "select cod_cargo, nome_cargo  "
            sSql = sSql + " from controle_db..cargo_tb with (nolock) "
            If cargo <> "" Then
                sSql = sSql + "where nome_cargo = '" & cargo & "'"
            End If
            sSql = sSql + " order by nome_cargo "
            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    Public Function BuscarOperadorPeloNome(ByVal operador As String) As DataSet

        Try
            Dim sSql As String = ""
            sSql = sSql + "select * "
            sSql = sSql + " from controle_db..funcionarios_tb with (nolock) "
            If operador <> "" Then
                sSql = sSql + "where nome_funcionario = '" & operador & "'"
            End If
            sSql = sSql + " order by nome_funcionario "
            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function BuscarOperadorDigital(ByVal cod_operador As Integer) As DataSet

        Try
            Dim sSql As String = ""
            sSql = sSql + "exec controle_db..BuscaFuncionarioBDDigital "

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function BuscarEspecie(ByVal cod_especie As Integer) As DataSet

        Try
            Dim sSql As String = ""
            sSql = sSql + "select cod_especie, nome_especie "
            sSql = sSql + " from controle_db..especie_tb with (nolock) "
            If cod_especie <> 0 Then
                sSql = sSql + "where cod_especie = " & cod_especie
            End If

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function BuscarGrupo(ByVal cod_grupo As Integer) As DataSet

        Try
            Dim sSql As String = ""
            sSql = sSql + "select cod_grupo, nome_grupo "
            sSql = sSql + " from controle_db..grupo_tb with (nolock) "
            If cod_grupo <> 0 Then
                sSql = sSql + "where cod_grupo = " & cod_grupo
            End If

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function BuscarSituacao(ByVal cod_grupo As Integer) As DataSet

        Try
            Dim sSql As String = ""
            sSql = sSql + "select situacao_faturamento_id, situacao "
            sSql = sSql + " from controle_db..SituacaoFaturamento_tb with (nolock) "
            If cod_grupo <> 0 Then
                sSql = sSql + "where situacao_faturamento_id = " & cod_grupo
            End If

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function



    Public Function ValidaLogin(ByVal usuario As String, ByVal senha As String) As DataSet
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "SELECT * FROM controle_db..usuario_tb  " & vbNewLine
            sSql &= "WHERE usuario = '" & usuario & "' and senha = '" & senha & "'"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                Return ds
            Else
                Return Nothing
            End If



        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Function BuscaAcesso(ByVal cod_funcionario As Integer) As DataSet
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "SELECT * FROM controle_db..usuario_tb  " & vbNewLine
            sSql &= "WHERE cod_funcionario = '" & cod_funcionario & "' "


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                Return ds
            Else
                Return Nothing
            End If



        Catch ex As Exception
            Return Nothing
        End Try


    End Function

    Public Function SalvaPermissoes(ByVal operador_id As Integer, ByVal operador As String, ByVal cod_categoria As Integer, ByVal categoria As String) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "Exec controle_db..Salva_permissoes_spi" & vbNewLine
            sSql &= "@OPERADOR_ID = " & operador_id & "," & vbNewLine
            sSql &= "@OPERADOR = '" & operador & "'," & vbNewLine
            sSql &= "@CATEGORIA = '" & categoria & "'," & vbNewLine
            sSql &= "@COD_CATEGORIA = " & cod_categoria & "," & vbNewLine
            sSql &= "@DT_INCLUSAO = '" & Format(Now.Date, "yyyyMMdd") & "'," & vbNewLine
            sSql &= "@USUARIO = '" & cUser & "'" & vbNewLine


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function CarregaPermissoes(ByVal operador_id As Integer) As DataSet
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "select categoria, cod_categoria from controle_db..perfil_operador_tb" & vbNewLine
            sSql &= " where operador_id = " & operador_id

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            If (ds.Tables(0).Rows.Count > 0) Then
                Return ds
            Else
                Return Nothing
            End If

        Catch ex As Exception
            Return Nothing
        End Try


    End Function

    Public Function DeletaPermissoes(ByVal operador_id As Integer) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "Exec controle_db..Deleta_permissoes_spi" & vbNewLine
            sSql &= "@OPERADOR_ID = " & operador_id

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function SalvaRegras(ByVal operador_id As Integer, ByVal operador As String, ByVal cod_categoria As Integer, ByVal categoria As String, ByVal contribuiu As Integer, ByVal nao_contribuiu As Integer) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "Exec controle_db..Salva_regras_spi" & vbNewLine
            sSql &= "@OPERADOR_ID = " & operador_id & "," & vbNewLine
            sSql &= "@OPERADOR = '" & operador & "'," & vbNewLine
            sSql &= "@CATEGORIA = '" & categoria & "'," & vbNewLine
            sSql &= "@COD_CATEGORIA = " & cod_categoria & "," & vbNewLine
            sSql &= "@CONTRIBUIU = " & contribuiu & "," & vbNewLine
            sSql &= "@NAO_CONTRIBUIU = " & nao_contribuiu & "," & vbNewLine
            sSql &= "@DT_INCLUSAO = '" & Format(Now.Date, "yyyyMMdd") & "'," & vbNewLine
            sSql &= "@USUARIO = '" & cUser & "'" & vbNewLine


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function DeletaRegras(ByVal operador_id As Integer) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "Exec controle_db..deleta_regra_requisicao_spd" & vbNewLine
            sSql &= "@OPERADOR_ID = " & operador_id

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function SalvaRegraEspecial(ByVal diasAceite As Integer, ByVal diasRecusa As Integer) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "Exec controle_db..Salva_regras_especial_spi" & vbNewLine
            sSql &= "@DIAS_ESPECIAL_CONTRIBUIU = " & diasAceite & "," & vbNewLine
            sSql &= "@DIAS_ESPECIAL_NAO_CONTRIBUIU = " & diasRecusa & "," & vbNewLine
            sSql &= "@DT_INCLUSAO = '" & Format(Now.Date, "yyyyMMdd") & "'," & vbNewLine
            sSql &= "@USUARIO = '" & cUser & "'" & vbNewLine


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function ValidaRemessa(ByVal mesRef As String) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "SELECT * FROM controle_db..controle_requisicao_tb  " & vbNewLine
            sSql &= "WHERE mesRef = '" & mesRef & "' "


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                Return True
            Else
                Return False
            End If



        Catch ex As Exception
            Return Nothing
        End Try


    End Function

    Public Function ValidaRemessaManual(ByVal mesRef As String) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "SELECT * FROM controle_db.dbo.controle_requisicao_manual_tb  " & vbNewLine
            sSql &= "WHERE mesRef = '" & mesRef & "' "


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                Return True
            Else
                Return False
            End If



        Catch ex As Exception
            Return Nothing
        End Try


    End Function

    Public Function GerarRequisicao(ByVal mesRef As String, ByVal proxMesRef As String, ByVal usuario As String) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            'sSql &= "Exec controle_db..GERA_REQUISICAO_SPI" & vbNewLine
            sSql &= "Exec controle_db..Atualiza_requisicao_novo" & vbNewLine
            sSql &= "@mesRef = '" & mesRef & "' "
            'sSql &= ", @prox_mes_Ref = '" & proxMesRef & "' "
            'sSql &= ", @usuario = '" & usuario & "' "

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                Return True
            Else
                Return False
            End If



        Catch ex As Exception
            Return Nothing
        End Try


    End Function

    Public Function GerarRequisicaoManual(ByVal mesRef As String, ByVal proxMesRef As String, ByVal usuario As String) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "Exec controle_db..GERA_REQUISICAO_MANUAL_SPI" & vbNewLine
            sSql &= "@mesRef = '" & mesRef & "' "
            sSql &= ", @prox_mes_Ref = '" & proxMesRef & "' "
            sSql &= ", @usuario = '" & usuario & "' "
            sSql &= ", @cod_usuario = 99  "

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                Return True
            Else
                Return False
            End If



        Catch ex As Exception
            Return Nothing
        End Try


    End Function


    Public Function SeparaFichas(ByVal operador_id As Integer, ByVal cod_categoria As Integer, ByVal quantidade As Integer) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "Exec controle_db..Separa_requisicao_SPI" & vbNewLine
            sSql &= " @OPERADOR_ID = " & operador_id & " "
            sSql &= ", @COD_CATEGORIA = " & cod_categoria & " "
            sSql &= ", @QUANTIDADE = " & quantidade & " "

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True


        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function SeparaFichasManual(ByVal operador_id As Integer, ByVal cod_categoria As Integer, ByVal quantidade As Integer, ByVal cidade As String, ByVal dia As Integer) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "Exec controle_db..Separa_requisicao_Manual_SPU" & vbNewLine
            sSql &= " @OPERADOR_ID = " & operador_id & " "
            sSql &= ", @COD_CATEGORIA = " & cod_categoria & " "
            sSql &= ", @QUANTIDADE = " & quantidade & " "
            sSql &= ", @cidade = '" & cidade & "' "
            sSql &= ", @dia = " & Dia

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True


        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function SeparaFichasCemig(ByVal operador_id As Integer, ByVal cod_categoria As Integer, ByVal quantidade As Integer, ByVal cidade As String) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet
            sSql = ""
            sSql &= "Exec integracao_cemig_db..Separa_requisicao_SPI" & vbNewLine
            sSql &= " @OPERADOR_ID = " & operador_id & " "
            sSql &= ", @COD_CATEGORIA = " & cod_categoria & " "
            sSql &= ", @QUANTIDADE = " & quantidade & " "
            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function RetirarOperadora(ByVal operador_id As Integer, ByVal cidade As String) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= " update a " & vbNewLine
            sSql &= " set COD_OPERA = 1, dt_alteracao = null, " & vbNewLine
            sSql &= " tipo_mov = 'REQUISICAO', data_mov = GETDATE(), " & vbNewLine
            sSql &= " COBRADOR = 'INSTITUICAO', dt_impressao = null " & vbNewLine
            sSql &= " From controle_db..requisicao_lista_tb a " & vbNewLine
            sSql &= " WHERE cod_recibo = 1 AND situacao = 1 AND confirmado = 1" & vbNewLine
            sSql &= " AND CIDADE = '" & cidade & "' and COD_OPERA = " & operador_id

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True


        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function RetirarOperadoraCemig(ByVal operador_id As Integer, ByVal cidade As String) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= " update a " & vbNewLine
            sSql &= " set COD_OPERA = 1, dt_alteracao = null, " & vbNewLine
            sSql &= " tipo_mov = 'REQUISICAO', data_mov = GETDATE(), " & vbNewLine
            sSql &= " COBRADOR = 'INSTITUICAO', dt_impressao = null " & vbNewLine
            sSql &= " From integracao_cemig_db..requisicao_lista_tb a " & vbNewLine
            sSql &= " WHERE cod_recibo = 1 AND situacao = 1 AND confirmado = 1" & vbNewLine
            sSql &= " AND CIDADE = '" & cidade & "' and COD_OPERA = " & operador_id

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True


        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function ResetaFichasPersonalizadoManual(ByVal operador_id As String, ByVal cod_categoria As String, ByVal quantidade As String, ByVal cidade As String) As Boolean
        Try
            Dim sSql As String = ""
            Dim sSqlAux As String = ""
            Dim numCondicoes As Integer = 0
            Dim ds As DataSet

            sSql = ""
            sSql &= " select * " & vbNewLine
            sSql &= " INTO #TD " & vbNewLine
            sSql &= " From controle_db..requisicao_lista_tb  " & vbNewLine
            sSql &= " where tipo_mov in ('TELEFONE DESLIGADO', 'OUTRA OPERADORA','TD') " & vbNewLine
            'sSql &= " and dt_nc <= DATEADD(month, 2,getdate()) " & vbNewLine
            sSql &= " " & vbNewLine
            sSql &= " select *  " & vbNewLine
            sSql &= " into #recusa " & vbNewLine
            sSql &= " from controle_db..requisicao_lista_tb " & vbNewLine
            sSql &= " where situacao = 9 " & vbNewLine
            sSql &= " and confirmado = 2 " & vbNewLine
            'sSql &= " and dt_alteracao <= DATEADD(month, 1,getdate()) " & vbNewLine
            sSql &= " " & vbNewLine


            sSql &= "Select * " & vbNewLine
            sSql &= "INTO #REQ " & vbNewLine
            sSql &= "from controle_db..requisicao_lista_tb a " & vbNewLine

            If operador_id > 0 Then
                sSqlAux &= " where cod_opera = " + operador_id.ToString() & vbNewLine
                numCondicoes += 1
            End If

            If cod_categoria > 0 And numCondicoes = 0 Then
                sSql &= " where cod_categoria = " + cod_categoria.ToString() & vbNewLine
                numCondicoes += 1
            ElseIf cod_categoria > 0 And numCondicoes = 1 Then
                sSql &= " and cod_categoria = " + cod_categoria.ToString() & vbNewLine
            End If

            If cidade <> "Selecione..." And numCondicoes = 0 Then
                sSql &= " where cidade = '" + cidade + "'" & vbNewLine
            ElseIf cidade <> "Selecione..." And numCondicoes = 1 Then
                sSql &= " and  cidade = '" + cidade + "'" & vbNewLine
            End If


            If quantidade > 0 Then
                sSql &= " update top(" + quantidade.ToString() + ") a " & vbNewLine
            Else
                sSql &= " update a " & vbNewLine
            End If
            sSql &= " set cod_recibo = 1, situacao = 1, confirmado = 1, " & vbNewLine
            sSql &= " tipo_mov = 'REQUISICAO', data_mov = GETDATE(), " & vbNewLine
            sSql &= " dt_alteracao = NULL, DT_INCLUSAO = GETDATE(), " & vbNewLine
            sSql &= " COD_OPERA = 1 " & vbNewLine
            sSql &= " ,COBRADOR = 'INSTITUICAO', dt_impressao = null " & vbNewLine
            sSql &= " From controle_db..requisicao_lista_tb a " & vbNewLine
            sSql &= " INNER JOIN  #REQ B " & vbNewLine
            sSql &= " ON A.COD_EAN_CLIENTE = B.COD_EAN_CLIENTE " & vbNewLine

            sSql &= " " & vbNewLine


            sSql &= " update a " & vbNewLine
            sSql &= "set cod_recibo = 1, situacao = 9, confirmado = 99,  " & vbNewLine
            sSql &= "tipo_mov = 'TD', data_mov = GETDATE(),  " & vbNewLine
            sSql &= "dt_alteracao = getdate(),COD_OPERA = 1 " & vbNewLine
            sSql &= ",COBRADOR = 'INSTITUICAO', dt_impressao = null " & vbNewLine
            sSql &= "From controle_db..requisicao_lista_tb a " & vbNewLine
            sSql &= "inner Join #td b " & vbNewLine
            sSql &= "On a.cod_ean_cliente= b.cod_ean_cliente			 " & vbNewLine

            sSql &= " " & vbNewLine

            sSql &= "update a" & vbNewLine
            sSql &= "set cod_recibo = 1, situacao = 9, confirmado = 2,  " & vbNewLine
            sSql &= "tipo_mov = 'TD', data_mov = GETDATE(),  " & vbNewLine
            sSql &= "dt_alteracao = getdate(),COD_OPERA = 1 " & vbNewLine
            sSql &= ",COBRADOR = 'INSTITUICAO', dt_impressao = null " & vbNewLine
            sSql &= "From controle_db..requisicao_lista_tb a " & vbNewLine
            sSql &= "inner Join #recusa b " & vbNewLine
            sSql &= "On a.cod_ean_cliente= b.cod_ean_cliente	" & vbNewLine

            sSql &= " " & vbNewLine


            sSql &= " update a " & vbNewLine
            sSql &= "set a.observacao = null " & vbNewLine
            sSql &= "From controle_Db..cliente_tb a " & vbNewLine
            sSql &= "inner Join controle_db..requisicao_lista_tb b  " & vbNewLine
            sSql &= "On a.cod_ean_cliente = b.cod_ean_cliente      " & vbNewLine
            sSql &= "inner Join  #req c " & vbNewLine
            sSql &= "On b.cod_ean_cliente = c.cod_ean_cliente  " & vbNewLine

            sSql &= " " & vbNewLine

            sSql &= "update b " & vbNewLine
            sSql &= "set b.observacao = null " & vbNewLine
            sSql &= "From controle_Db..cliente_tb a " & vbNewLine
            sSql &= "inner Join controle_db..requisicao_lista_tb b " & vbNewLine
            sSql &= "On a.cod_ean_cliente = b.cod_ean_cliente " & vbNewLine
            sSql &= "inner Join  #req c  " & vbNewLine
            sSql &= "On b.cod_ean_cliente = c.cod_ean_cliente  " & vbNewLine

            sSql &= " " & vbNewLine

            sSql &= "update a " & vbNewLine
            sSql &= "set a.observacao = '' " & vbNewLine
            sSql &= "From controle_Db..lista_retida_tmp_tb  a" & vbNewLine
            sSql &= "inner Join controle_db..requisicao_lista_tb b " & vbNewLine
            sSql &= "On a.cod_ean_cliente = b.cod_ean_cliente " & vbNewLine
            sSql &= "inner Join  #req c  " & vbNewLine
            sSql &= "On b.cod_ean_cliente = c.cod_ean_cliente " & vbNewLine

            sSql &= " " & vbNewLine

            sSql &= "update b " & vbNewLine
            sSql &= "set b.observacao = null " & vbNewLine
            sSql &= "From controle_Db..lista_retida_tmp_tb a " & vbNewLine
            sSql &= "inner Join controle_db..requisicao_lista_tb b " & vbNewLine
            sSql &= "On a.cod_ean_cliente = b.cod_ean_cliente " & vbNewLine
            sSql &= "inner Join  #req c  " & vbNewLine
            sSql &= "On b.cod_ean_cliente = c.cod_ean_cliente  " & vbNewLine



            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True


        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function ResetaFichasPersonalizadoOi(ByVal operador_id As String, ByVal operador As String, ByVal cod_categoria As String, ByVal pQuantidade As String) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            If pQuantidade > 0 Then
                sSql &= " update top (" & pQuantidade.ToString() & ") requisicao_tb   " & vbNewLine
            Else
                sSql &= " update requisicao_tb   " & vbNewLine
            End If
            sSql &= " set requisicao_tb.operador_id = null, requisicao_tb.vinculado_operador = null, requisicao_tb.operador = null, dt_alteracao = null, observacao = null " & vbNewLine
            sSql &= " From controle_db..requisicao_tb requisicao_tb        " & vbNewLine
            sSql &= " where categoria = " & cod_categoria.ToString() & vbNewLine
            sSql &= " and operador_id = " & operador_id.ToString() & " and vinculado_operador =  'S' and operador = '" & operador & "'" & vbNewLine
            sSql &= " and atual = 'S' and confirmado is null" & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True


        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function TrocarFichasPersonalizadoOi(ByVal de_operador_id As Integer, ByVal de_operador As String, ByVal para_operador_id As Integer, ByVal para_operador As String, ByVal cod_categoria As Integer, ByVal quantidade As Integer) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            If quantidade > 0 Then
                sSql &= " update top (" + quantidade.ToString() + ") requisicao_tb   " & vbNewLine
            Else
                sSql &= " update requisicao_tb   " & vbNewLine
            End If
            sSql &= " set requisicao_tb.operador_id = " + para_operador_id.ToString() + ", requisicao_tb.operador = '" + para_operador + "' , dt_alteracao = getdate() , observacao = null" & vbNewLine
            sSql &= " From controle_db..requisicao_tb requisicao_tb        " & vbNewLine
            sSql &= " where operador_id = " + de_operador_id.ToString() + " and vinculado_operador =  'S' and operador = '" + de_operador + "'" & vbNewLine
            sSql &= " and atual = 'S' and confirmado is null " & vbNewLine
            If cod_categoria > 0 Then
                sSql &= " and categoria = " + cod_categoria.ToString() & vbNewLine
            End If


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True


        Catch ex As Exception
            Return False
        End Try


    End Function



    Public Function SeparaFichasListaNovaOi(ByVal operador_id As Integer, ByVal cod_categoria As Integer, ByVal quantidade As Integer, ByVal cidade As String) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "Exec controle_db..Separa_requisicao_lista_nova_oi_SPI" & vbNewLine
            sSql &= " @OPERADOR_ID = " & operador_id & " "
            sSql &= ", @COD_CATEGORIA = " & cod_categoria & " "
            sSql &= ", @QUANTIDADE = " & quantidade & " "
            sSql &= ", @cidade = '" & cidade & "' "

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True


        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function ResetaFichasManual(ByVal cod_categoria As Integer, ByVal dia As Integer, ByVal cidade As String) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "update controle_db.dbo.requisicao_lista_tb" & vbNewLine
            sSql &= " set dt_alteracao = null, cod_opera = 1"
            sSql &= " where COD_CATEGORIA = " & cod_categoria & " "
            sSql &= " and situacao = 1"
            sSql &= " and dia = " & dia
            sSql &= " and cidade = '" & cidade & "'"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True


        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function ResetaFichasCemig(ByVal cod_categoria As Integer) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "update integracao_cemig_db.dbo.requisicao_tb" & vbNewLine
            sSql &= " set Vinculado_Operador = null"
            sSql &= " where categoria = " & cod_categoria & " "


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True


        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function ResetaFichasOi(ByVal cod_categoria As Integer) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "update controle_db.dbo.requisicao_tb" & vbNewLine
            sSql &= " set Vinculado_Operador = null"
            sSql &= " where categoria = " & cod_categoria & " "

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True


        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function Retorna_Operador_por_Categoria(ByVal cod_categoria As Integer) As DataSet
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "Exec controle_db..retorna_Operador_Categoria_SPS" & vbNewLine
            sSql &= " @COD_CATEGORIA = " & cod_categoria & " "

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                Return ds
            Else
                Return Nothing
            End If



        Catch ex As Exception
            Return Nothing
        End Try


    End Function

    Public Function Retorna_Nome_Operador_por_Categoria(ByVal cod_categoria As Integer) As DataSet
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "select operador from controle_db..PERFIL_OPERADOR_TB  " & vbNewLine
            sSql &= " where cod_categoria = " & cod_categoria & " "


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                Return ds
            Else
                Return Nothing
            End If



        Catch ex As Exception
            Return Nothing
        End Try


    End Function

    Public Function Retorna_Historico(ByVal telefone As String, ByVal DDD As String, ByVal cod_ean As String) As DataSet
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "SELECT ddd as StDDDMeioAcessoCobranca" & vbNewLine
            sSql &= ", telefone StNumMeioAcessoCobranca" & vbNewLine
            sSql &= ", valor as DVlrServico" & vbNewLine
            sSql &= ", categoria as categoria" & vbNewLine
            sSql &= ", mesRef  as DdDataServico" & vbNewLine
            sSql &= ", dt_inclusao as DdDataSituacao" & vbNewLine
            sSql &= ", cod_ean as  StDescReferencia" & vbNewLine
            sSql &= ", case when aceite = 'S' then 'Aceite'" & vbNewLine
            sSql &= "when aceite = 'N' then 'Recusa'" & vbNewLine
            sSql &= "else 'Cancelado' end as UsSitFaturamento" & vbNewLine
            sSql &= ", observacao as UiMotivoSituacao " & vbNewLine
            sSql &= ", nome_cliente as nome_cliente " & vbNewLine
            sSql &= "from controle_db.dbo.requisicao_oi_tb (nolock)" & vbNewLine
            sSql &= "where telefone = '" & telefone & "'" & vbNewLine
            If DDD <> "" Then
                sSql &= " and DDD = '" & DDD & "'" & vbNewLine
            End If
            If cod_ean <> "" Then
                sSql &= " and cod_ean = '" & cod_ean & "'" & vbNewLine
            End If
            sSql &= " union " & vbNewLine
            sSql &= "SELECT ddd as StDDDMeioAcessoCobranca" & vbNewLine
            sSql &= ", telefone StNumMeioAcessoCobranca" & vbNewLine
            sSql &= ", valor as DVlrServico" & vbNewLine
            sSql &= ", categoria as categoria" & vbNewLine
            sSql &= ", mesRef  as DdDataServico" & vbNewLine
            sSql &= ", dt_inclusao as DdDataSituacao" & vbNewLine
            sSql &= ", cod_ean as  StDescReferencia" & vbNewLine
            sSql &= ", case when aceite = 'S' then 'Aceite'" & vbNewLine
            sSql &= "when aceite = 'N' then 'Recusa'" & vbNewLine
            sSql &= "else 'Cancelado' end as UsSitFaturamento" & vbNewLine
            sSql &= ", observacao as UiMotivoSituacao " & vbNewLine
            sSql &= ", nome_cliente as nome_cliente " & vbNewLine
            sSql &= "from controle_db.dbo.requisicao_oi_tb (nolock)" & vbNewLine
            sSql &= "where telefone = '" & telefone & "'" & vbNewLine
            If DDD <> "" Then
                sSql &= " and DDD = '" & DDD & "'" & vbNewLine
            End If
            sSql &= "order by dt_inclusao desc" & vbNewLine


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return ds


        Catch ex As Exception
            Return Nothing
        End Try


    End Function

    Public Function RetornaHistoricoOi(telefone As String,
                                       ddd As String) As DataSet
        Try
            Dim ds As DataSet
            Dim valor As String = ""
            Dim sSql As String = ""

            sSql &= "select StDDDMeioAcessoCobranca" & vbNewLine
            sSql &= "	 , StNumMeioAcessoCobranca " & vbNewLine
            sSql &= ", DVlrServico" & vbNewLine
            sSql &= "	 , DdDataServico" & vbNewLine
            sSql &= "	 , DdDataSituacao" & vbNewLine
            sSql &= "	 , UsSitFaturamento" & vbNewLine
            sSql &= "	 , UiMotivoSituacao" & vbNewLine
            sSql &= "	 , StDescReferencia" & vbNewLine
            sSql &= "from interface_dados_db..arquivo_retorno_detalhe_tb (nolock)" & vbNewLine
            sSql &= "where substring(StNumMeioAcessoCobranca,3,12) = '" & RTrim(LTrim(telefone)) & "'" & vbNewLine
            sSql &= " and StDDDMeioAcessoCobranca = '" & RTrim(LTrim(ddd)) & "'" & vbNewLine
            sSql &= "order by convert (DATE, DdDataSituacao) asc  "
            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
            If (ds.Tables(0).Rows.Count > 0) Then
                Return ds
            Else
                Return Nothing
            End If



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function Retorna_HistoricoCemig(ByVal telefone As String, ByVal DDD As String, ByVal cod_ean As String) As DataSet
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "SELECT ddd as StDDDMeioAcessoCobranca" & vbNewLine
            sSql &= ", telefone StNumMeioAcessoCobranca" & vbNewLine
            sSql &= ", valor as DVlrServico" & vbNewLine
            sSql &= ", categoria as categoria" & vbNewLine
            sSql &= ", mesRef  as DdDataServico" & vbNewLine
            sSql &= ", dt_inclusao as DdDataSituacao" & vbNewLine
            sSql &= ", cod_ean as  StDescReferencia" & vbNewLine
            sSql &= ", case when aceite = 'S' then 'Aceite'" & vbNewLine
            sSql &= "when aceite = 'N' then 'Recusa'" & vbNewLine
            sSql &= "else 'Cancelado' end as UsSitFaturamento" & vbNewLine
            sSql &= ", observacao as UiMotivoSituacao " & vbNewLine
            sSql &= ", nome_cliente as nome_cliente " & vbNewLine
            sSql &= "from integracao_cemig_db.dbo.requisicao_cemig_tb " & vbNewLine
            sSql &= "where telefone = '" & telefone & "'" & vbNewLine
            If DDD <> "" Then
                sSql &= " and DDD = '" & DDD & "'" & vbNewLine
            End If
            If cod_ean <> "" Then
                sSql &= " and cod_ean = '" & cod_ean & "'" & vbNewLine
            End If
            sSql &= " union " & vbNewLine
            sSql &= "SELECT ddd as StDDDMeioAcessoCobranca" & vbNewLine
            sSql &= ", telefone StNumMeioAcessoCobranca" & vbNewLine
            sSql &= ", valor as DVlrServico" & vbNewLine
            sSql &= ", categoria as categoria" & vbNewLine
            sSql &= ", mesRef  as DdDataServico" & vbNewLine
            sSql &= ", dt_inclusao as DdDataSituacao" & vbNewLine
            sSql &= ", cod_ean as  StDescReferencia" & vbNewLine
            sSql &= ", case when aceite = 'S' then 'Aceite'" & vbNewLine
            sSql &= "when aceite = 'N' then 'Recusa'" & vbNewLine
            sSql &= "else 'Cancelado' end as UsSitFaturamento" & vbNewLine
            sSql &= ", observacao as UiMotivoSituacao " & vbNewLine
            sSql &= ", nome_cliente as nome_cliente " & vbNewLine
            sSql &= "from integracao_cemig_db.dbo.requisicao_cemig_tb " & vbNewLine
            sSql &= "where telefone = '" & telefone & "'" & vbNewLine
            If DDD <> "" Then
                sSql &= " and DDD = '" & DDD & "'" & vbNewLine
            End If
            sSql &= "order by dt_inclusao desc" & vbNewLine


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return ds


        Catch ex As Exception
            Return Nothing
        End Try


    End Function

    Public Function Retorna_HistoricoSemRecusas(ByVal telefone As String, ByVal DDD As String, ByVal cod_ean As String) As DataSet
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "SELECT ddd as StDDDMeioAcessoCobranca" & vbNewLine
            sSql &= ", telefone StNumMeioAcessoCobranca" & vbNewLine
            sSql &= ", valor as DVlrServico" & vbNewLine
            sSql &= ", categoria as categoria" & vbNewLine
            sSql &= ", mesRef  as DdDataServico" & vbNewLine
            sSql &= ", dt_inclusao as DdDataSituacao" & vbNewLine
            sSql &= ", cod_ean as  StDescReferencia" & vbNewLine
            sSql &= ", case when aceite = 'S' then 'Aceite'" & vbNewLine
            sSql &= "when aceite = 'N' then 'Recusa'" & vbNewLine
            sSql &= "else 'Cancelado' end as UsSitFaturamento" & vbNewLine
            sSql &= ", observacao as UiMotivoSituacao " & vbNewLine
            sSql &= ", nome_cliente as nome_cliente " & vbNewLine
            sSql &= "from controle_db.dbo.requisicao_oi_tb " & vbNewLine
            sSql &= "where aceite = 'S' " & vbNewLine
            sSql &= "and telefone = '" & telefone & "'" & vbNewLine
            If DDD <> "" Then
                sSql &= " and DDD = '" & DDD & "'" & vbNewLine
            End If
            If cod_ean <> "" Then
                sSql &= " and cod_ean = '" & cod_ean & "'" & vbNewLine
            End If
            sSql &= " union " & vbNewLine
            sSql &= "SELECT ddd as StDDDMeioAcessoCobranca" & vbNewLine
            sSql &= ", telefone StNumMeioAcessoCobranca" & vbNewLine
            sSql &= ", valor as DVlrServico" & vbNewLine
            sSql &= ", categoria as categoria" & vbNewLine
            sSql &= ", mesRef  as DdDataServico" & vbNewLine
            sSql &= ", dt_inclusao as DdDataSituacao" & vbNewLine
            sSql &= ", cod_ean as  StDescReferencia" & vbNewLine
            sSql &= ", case when aceite = 'S' then 'Aceite'" & vbNewLine
            sSql &= "when aceite = 'N' then 'Recusa'" & vbNewLine
            sSql &= "else 'Cancelado' end as UsSitFaturamento" & vbNewLine
            sSql &= ", observacao as UiMotivoSituacao " & vbNewLine
            sSql &= ", nome_cliente as nome_cliente " & vbNewLine
            sSql &= "from controle_db.dbo.requisicao_oi_tb " & vbNewLine
            sSql &= "where aceite = 'S' " & vbNewLine
            sSql &= "and telefone = '" & telefone & "'" & vbNewLine
            If DDD <> "" Then
                sSql &= " and DDD = '" & DDD & "'" & vbNewLine
            End If
            sSql &= "order by dt_inclusao desc" & vbNewLine


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return ds


        Catch ex As Exception
            Return Nothing
        End Try


    End Function

    Public Function Retorna_HistoricoSimplificado(ByVal telefone As String, ByVal DDD As String) As DataSet
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "SELECT ddd as StDDDMeioAcessoCobranca" & vbNewLine
            sSql &= ", telefone StNumMeioAcessoCobranca" & vbNewLine
            sSql &= ", valor as DVlrServico" & vbNewLine
            sSql &= ", categoria as categoria" & vbNewLine
            sSql &= ", mesRef  as DdDataServico" & vbNewLine
            sSql &= ", dt_inclusao as DdDataSituacao" & vbNewLine
            sSql &= ", cod_ean as  StDescReferencia" & vbNewLine
            sSql &= ", case when aceite = 'S' then 'Aceite'" & vbNewLine
            sSql &= "when aceite = 'N' then 'Recusa'" & vbNewLine
            sSql &= "else 'Cancelado' end as UsSitFaturamento" & vbNewLine
            sSql &= ", observacao as UiMotivoSituacao " & vbNewLine
            sSql &= ", nome_cliente as nome_cliente " & vbNewLine
            sSql &= "from controle_db.dbo.requisicao_oi_tb " & vbNewLine
            sSql &= "where telefone = '" & telefone & "'" & vbNewLine
            If DDD <> "" Then
                sSql &= " and DDD = '" & DDD & "'" & vbNewLine
            End If
            sSql &= "order by dt_inclusao desc" & vbNewLine


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return ds


        Catch ex As Exception
            Return Nothing
        End Try


    End Function

    Public Function RetornaRequisicaoSelecionada(ByVal requisicaoId As Long) As DataSet
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "SELECT ddd " & vbNewLine
            sSql &= ", telefone " & vbNewLine
            sSql &= ", valor " & vbNewLine
            sSql &= ", categoria " & vbNewLine
            sSql &= ", mesRef  " & vbNewLine
            sSql &= ", dt_inclusao " & vbNewLine
            sSql &= ", cod_ean " & vbNewLine
            sSql &= ", case when aceite = 'S' then 'Aceite'" & vbNewLine
            sSql &= "when aceite = 'N' then 'Recusa'" & vbNewLine
            sSql &= "else 'Cancelado' end as aceite" & vbNewLine
            sSql &= ", observacao " & vbNewLine
            sSql &= ", nome_cliente " & vbNewLine
            sSql &= ", operador "
            sSql &= ", operador_id "
            sSql &= ", cpf "
            sSql &= ",autorizante "
            sSql &= "from controle_db.dbo.requisicao_oi_tb " & vbNewLine
            sSql &= "where requisicao_id = '" & requisicaoId & "'" & vbNewLine
            sSql &= "order by dt_inclusao desc" & vbNewLine


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return ds


        Catch ex As Exception
            Return Nothing
        End Try


    End Function

    Public Function RetornaRequisicaoCemigSelecionada(ByVal requisicaoId As Long) As DataSet
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "SELECT ddd " & vbNewLine
            sSql &= ", telefone " & vbNewLine
            sSql &= ", valor " & vbNewLine
            sSql &= ", categoria " & vbNewLine
            sSql &= ", mesRef  " & vbNewLine
            sSql &= ", dt_inclusao " & vbNewLine
            sSql &= ", cod_ean " & vbNewLine
            sSql &= ", case when aceite = 'S' then 'Aceite'" & vbNewLine
            sSql &= "when aceite = 'N' then 'Recusa'" & vbNewLine
            sSql &= "else 'Cancelado' end as aceite" & vbNewLine
            sSql &= ", observacao " & vbNewLine
            sSql &= ", nome_cliente " & vbNewLine
            sSql &= ", operador "
            sSql &= ", operador_id "
            sSql &= ", cpf "
            sSql &= ",autorizante "
            sSql &= "from integracao_cemig_db.dbo.requisicao_cemig_tb " & vbNewLine
            sSql &= "where requisicao_id = '" & requisicaoId & "'" & vbNewLine
            sSql &= "order by dt_inclusao desc" & vbNewLine
            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
            Return ds
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

#End Region

    Public Function Busca_Lista_Telefonica(ByVal categoria As Integer) As DataSet
        Try
            Dim sSql As String

            sSql = ""
            sSql &= "SELECT cod_ean,DDD,telefone,valor,nome_cliente,categoria,mesSituacao,Situacao,dt_inclusao,dt_alteracao,usuario,cod_cidade, lista_telefonica_id" & vbNewLine
            sSql &= "FROM controle_db..lista_telefonica_tb" & vbNewLine
            If categoria <> 0 Then
                sSql &= " where categoria = '" & categoria & "'"
            End If

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function Busca_Quantidade_Lista_Telefonica(ByVal categoria As Integer) As Integer
        Try
            Dim sSql As String
            Dim ds As DataSet
            Dim quantidade As Integer

            sSql = ""
            sSql &= "SELECT COUNT(*)" & vbNewLine
            sSql &= "FROM controle_db..lista_telefonica_tb" & vbNewLine
            If categoria <> 0 Then
                sSql &= " where categoria = '" & categoria & "'"
            End If

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                quantidade = ds.Tables(0).Rows.Item(0).Item(0)
                Return quantidade
            Else
                quantidade = 0
                Return quantidade
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function Busca_Valor_Lista_Telefonica(ByVal categoria As Integer) As Decimal
        Try
            Dim sSql As String
            Dim ds As DataSet
            Dim valor As Decimal


            sSql = ""
            sSql &= "SELECT SUM(VALOR)" & vbNewLine
            sSql &= "FROM controle_db..lista_telefonica_tb" & vbNewLine
            If categoria <> 0 Then
                sSql &= " where categoria = '" & categoria & "'"
            End If

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                valor = ds.Tables(0).Rows.Item(0).Item(0)
                Return valor
            Else
                valor = 0
                Return valor
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function Atualiza_Lista_telefonica(ByVal lista As CListaTelefonica) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "Exec controle_db..Atualiza_Lista_Telelfonica_SPU" & vbNewLine
            sSql &= " @cod_ean = '" & lista.Cod_cliente & "', "
            sSql &= " @nome = '" & lista.Nome_cliente & "', "
            sSql &= " @DDD = '" & lista.DDD & "', "
            sSql &= " @Telefone = '" & lista.Telefone & "', "
            sSql &= " @valor = " & Replace(lista.Valor, ",", ".") & ", "
            sSql &= " @categoria = " & lista.Categoria & ", "
            sSql &= " @cod_cidade = " & lista.Cod_cidade & ", "
            sSql &= " @Lista_telefonica_id  = " & lista.Lista_telefonica_id & " "



            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try


    End Function

    Public Function Busca_Informações_Lista_Telefonica(ByVal telefone As String, ByVal DDD As String) As DataSet
        Try
            Dim sSql As String

            sSql = ""
            sSql &= "SELECT cod_ean,DDD,telefone,valor,nome_cliente,categoria,mesSituacao,Situacao,dt_inclusao,dt_alteracao,usuario,cod_cidade, lista_telefonica_id" & vbNewLine
            sSql &= "FROM controle_db..lista_telefonica_tb" & vbNewLine
            If telefone <> 0 Then
                sSql &= " where telefone = '" & telefone & "'"
            End If
            If DDD <> "" Then
                sSql &= " and DDD = '" & DDD & "'"
            End If

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function Busca_Informações_Exclusao(ByVal telefone As String, ByVal DDD As String) As DataSet
        Try
            Dim sSql As String

            sSql = ""
            sSql &= "SELECT top 1 * " & vbNewLine
            sSql &= "FROM controle_db..telefone_exclusao" & vbNewLine
            sSql &= " where telefone = '" & telefone & "'"
            sSql &= " and DDD = '" & DDD & "'"
            sSql &= " order by dt_inclusao desc " & vbNewLine

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function Busca_Telefones_Recuperacao() As DataSet
        Try
            Dim sSql As String

            sSql = ""
            sSql &= "EXEC controle_db..RECUPERA_TELEFONES_SPS"


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaResumoRequisicao(ByVal mesRef As Date) As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try

            sSql = ""
            sSql &= "exec controle_db.dbo.RetornaDadosAtuaisRequisicao_SPS " & vbNewLine
            sSql &= "@mes_ref = '" & mesRef & "'"

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    Public Function RetornaResumoRequisicaoManual(ByVal mesRef As Date) As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try

            sSql = ""
            sSql &= "exec controle_db.dbo.RetornaDadosAtuaisRequisicao_manual_SPS " & vbNewLine
            sSql &= "@mes_ref = '" & mesRef & "'"

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function RetornaDadosRequisicao() As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try

            sSql = ""
            sSql &= " select COUNT(*) as quantidade_requisicao, SUM(valor) as valor_requisicao from controle_db..requisicao_tb " & vbNewLine
            sSql &= " where Atual = 'S'"

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function RetornaDadosRequisicaoManual() As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try

            sSql = ""
            sSql &= " select COUNT(*) as quantidade_requisicao, SUM(valor) as valor_requisicao from controle_db..requisicao_lista_tb " & vbNewLine


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function RetornaDadosRequisicaoCemig() As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try

            sSql = ""
            sSql &= " select COUNT(*) as quantidade_requisicao, SUM(valor) as valor_requisicao from integracao_cemig_db..requisicao_tb " & vbNewLine


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function RetornaRequisicaoPorCategoria() As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try

            sSql = ""
            sSql &= " select b.nome_categoria as categoria" & vbNewLine
            sSql &= " ,COUNT(*) as quantidade" & vbNewLine
            sSql &= " ,'R$ ' + convert(varchar(10),SUM(convert(money,valor))) as valor" & vbNewLine
            sSql &= " from controle_db..requisicao_tb a" & vbNewLine
            sSql &= " inner join controle_db..categoria_tb b" & vbNewLine
            sSql &= " on a.categoria = b.cod_categoria" & vbNewLine
            sSql &= " where Atual = 'S'" & vbNewLine
            sSql &= " group by b.nome_categoria " & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function RetornaRequisicaoPorCategoriaManual() As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try

            sSql = ""
            sSql &= " select b.nome_categoria as categoria" & vbNewLine
            sSql &= " ,COUNT(*) as quantidade" & vbNewLine
            sSql &= " ,'R$ ' + convert(varchar(10),SUM(convert(money,valor))) as valor" & vbNewLine
            sSql &= " from controle_db..requisicao_lista_tb a" & vbNewLine
            sSql &= " inner join controle_db..categoria_tb b" & vbNewLine
            sSql &= " on a.cod_categoria = b.cod_categoria" & vbNewLine
            sSql &= " group by b.nome_categoria " & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function RetornaRequisicaoPorTelefone(ByVal Telefone As String, ByVal ddd As String) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "select requisicao_id,cod_ean,DDD,telefone,valor,operador,aceite, hist_aceite, categoria,mesRefSeguinte,dt_inclusao,nome_cliente,cpf,autorizante,observacao " & vbNewLine
            sSql &= " from controle_db.dbo.requisicao_oi_tb" & vbNewLine
            sSql &= "where telefone ='" & Telefone & "'"
            If ddd <> "" Then
                sSql &= "and DDD ='" & ddd & "'"
            End If


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function RetornaRequisicaoCemigPorTelefone(ByVal Telefone As String, ByVal ddd As String) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "select requisicao_id,cod_ean,DDD,telefone,valor,operador,aceite, hist_aceite, categoria,mesRefSeguinte,dt_inclusao,nome_cliente,cpf,autorizante,observacao,num_instalacao " & vbNewLine
            sSql &= " from integracao_cemig_db.dbo.requisicao_cemig_tb" & vbNewLine
            sSql &= "where telefone ='" & Telefone & "'"
            If ddd <> "" Then
                sSql &= "and DDD ='" & ddd & "'"
            End If


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function RetornaRequisicaoPorPeriodoData(ByVal dtInicial As Date, ByVal dtFinal As Date, ByVal tipoAceite As Integer, ByVal naoConforme As Boolean) As DataSet
        Dim sSql As String
        Dim aceite As String = ""

        Try
            If tipoAceite = 0 Then
                aceite = "S"
            ElseIf tipoAceite = 1 Then
                aceite = "N"
            ElseIf tipoAceite = 2 Then
                aceite = "C"
            End If


            sSql = ""
            sSql &= "select requisicao_id,cod_ean,DDD,telefone,valor,operador,aceite, hist_aceite, categoria,mesRefSeguinte,dt_inclusao,nome_cliente,cpf,autorizante,observacao " & vbNewLine
            sSql &= " from controle_db.dbo.requisicao_oi_tb" & vbNewLine
            sSql &= "where convert(date,dt_inclusao) between '" & dtInicial & "' and '" & dtFinal & "'"
            If tipoAceite <> 3 Then
                sSql &= "and aceite = '" & aceite & "'"
            End If
            If naoConforme = True Then
                ' sSql &= "and nao_conforme = 1"
            ElseIf naoConforme = False Then
                ' sSql &= "and nao_conforme = 0"
            End If

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function RetornaRequisicaoCemigPorPeriodoData(ByVal dtInicial As Date, ByVal dtFinal As Date, ByVal tipoAceite As Integer, ByVal naoConforme As Boolean) As DataSet
        Dim sSql As String
        Dim aceite As String = ""

        Try
            If tipoAceite = 0 Then
                aceite = "S"
            ElseIf tipoAceite = 1 Then
                aceite = "N"
            ElseIf tipoAceite = 2 Then
                aceite = "C"
            End If


            sSql = ""
            sSql &= "select requisicao_id,cod_ean,DDD,telefone,valor,operador,aceite, hist_aceite, categoria,mesRefSeguinte,dt_inclusao,nome_cliente,cpf,autorizante,observacao " & vbNewLine
            sSql &= " from integracao_cemig_db.dbo.requisicao_cemig_tb" & vbNewLine
            sSql &= "where convert(date,dt_inclusao) between '" & dtInicial & "' and '" & dtFinal & "'"
            If tipoAceite <> 3 Then
                sSql &= "and aceite = '" & aceite & "'"
            End If
            If naoConforme = True Then
                sSql &= "and nao_conforme = 1"
            ElseIf naoConforme = False Then
                sSql &= "and nao_conforme = 0"
            End If

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    Public Function RetornaRequisicaoPorPeriodoDataComplementar(ByVal dtInicial As Date, ByVal dtFinal As Date, ByVal tipoAceite As Integer, ByVal naoConforme As Boolean, ByVal Pesquisa As Integer, ByVal TipoPesquisa As Integer, ByVal verificaGrau As Boolean) As DataSet
        Dim sSql As String
        Dim aceite As String = ""
        Dim ds As DataSet
        '0 =  categoria, 1 =  operador

        Try
            If tipoAceite = 0 Then
                aceite = "S"
            ElseIf tipoAceite = 1 Then
                aceite = "N"
            ElseIf tipoAceite = 2 Then
                aceite = "C"
            End If


            sSql = ""
            sSql &= "select requisicao_id,cod_ean,DDD,telefone,valor,operador,aceite, hist_aceite, categoria,mesRefSeguinte,dt_inclusao,nome_cliente,cpf,autorizante,observacao " & vbNewLine

            sSql &= " from controle_db.dbo.requisicao_oi_tb" & vbNewLine
            sSql &= "where convert(date,dt_inclusao) between '" & dtInicial & "' and '" & dtFinal & "'"
            If tipoAceite <> 3 Then
                sSql &= "and aceite = '" & aceite & "'"
            End If
            'If verificaGrau = True Then
            '    If naoConforme = True Then
            '        sSql &= "and nao_conforme = 1"
            '    ElseIf naoConforme = False Then
            '        sSql &= "and nao_conforme = 0"
            '    End If
            'End If
            If TipoPesquisa = 0 Then
                sSql &= "and categoria = " & Pesquisa
            ElseIf TipoPesquisa = 1 Then
                Dim operador As String = ""
                Dim sSqlAux As String = ""
                sSqlAux = ""
                sSqlAux = "Select nome_funcionario from controle_db..funcionarios_tb where cod_ean_funcionario = " & Pesquisa

                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSqlAux)

                If (ds.Tables(0).Rows.Count > 0) Then
                    operador = ds.Tables(0).Rows.Item(0).Item(0)
                Else
                    operador = ""
                End If

                sSql &= "and operador = '" & operador & "'"
            End If


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function



    Public Function RetornaRequisicaoComCriticasComissao(dtInicial As Date, dtFinal As Date, operadorId As Integer) As Integer
        Try
            Dim sSql As String
            Dim ds As New DataSet
            Dim dsResultado As New DataSet
            Dim totalValorCriticados As Integer = 0
            Dim operador As String = ""
            Dim sSqlAux As String = ""
            sSqlAux = ""
            sSqlAux = "Select nome_funcionario from controle_db..funcionarios_tb (nolock) where cod_ean_funcionario = " & operadorId
            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSqlAux)
            If (ds.Tables(0).Rows.Count > 0) Then
                operador = ds.Tables(0).Rows.Item(0).Item(0)
            Else
                operador = ""
            End If
            sSql = ""
            sSql &= "select requisicao_id,cod_ean,DDD,telefone,valor,operador,aceite, hist_aceite, categoria,mesRefSeguinte,dt_inclusao,nome_cliente,cpf,autorizante,observacao " & vbNewLine
            sSql &= " into #requisicao" & vbNewLine
            sSql &= " from controle_db.dbo.requisicao_oi_tb (nolock)" & vbNewLine
            sSql &= "where convert(date,dt_inclusao) between '" & dtInicial & "' and '" & dtFinal & "'" & vbNewLine
            sSql &= "and aceite = 'S'" & vbNewLine
            sSql &= "and operador = '" & operador & "'" & vbNewLine
            sSql &= "Declare @MesAnterior datetime, @primeirodiaMesAnterior date, @ultimodiamesMesAnterior DATE
                    Set @MesAnterior = DATEADD(month, -1,getdate())                                                                   
                    Select @primeirodiaMesAnterior = CONVERT(VARCHAR, @MesAnterior - DAY(@MesAnterior) + 1, 103)                                                      
                    Select @ultimodiamesMesAnterior = DATEADD(d, -DAY(@MesAnterior),DATEADD(m,1,@MesAnterior))" & vbNewLine
            sSql &= "select DVlrServico
                    into #valorCriticados
                    from interface_dados_db..arquivo_retorno_detalhe_tb a (nolock)
                    inner join #requisicao b
                    on substring(StNumMeioAcessoCobranca,3,12) = b.telefone
                    and StDDDMeioAcessoCobranca = b.DDD
                    where  UsSitFaturamento in ('contestado','criticado','cancelado')
                    and CONVERT(DATE, DdDataSituacao) BETWEEN @primeirodiaMesAnterior AND @ultimodiamesMesAnterior     " & vbNewLine
            sSql &= "select sum(convert(decimal, replace(replace(DVlrServico,'R$',''),',','.'))) from #valorCriticados" & vbNewLine
            dsResultado = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
            If (dsResultado.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(dsResultado.Tables(0).Rows.Item(0).Item(0)) Then
                    totalValorCriticados = dsResultado.Tables(0).Rows.Item(0).Item(0)
                Else
                    totalValorCriticados = 0
                End If
            End If
            Return totalValorCriticados
        Catch ex As Exception
            Return Nothing
        End Try
    End Function



    Public Function RetornaRequisicaoCemigPorPeriodoDataComplementar(ByVal dtInicial As Date, ByVal dtFinal As Date, ByVal tipoAceite As Integer, ByVal naoConforme As Boolean, ByVal Pesquisa As Integer, ByVal TipoPesquisa As Integer, ByVal verificaGrau As Boolean) As DataSet
        Dim sSql As String
        Dim aceite As String = ""
        Dim ds As DataSet
        '0 =  categoria, 1 =  operador

        Try
            If tipoAceite = 0 Then
                aceite = "S"
            ElseIf tipoAceite = 1 Then
                aceite = "N"
            ElseIf tipoAceite = 2 Then
                aceite = "C"
            End If


            sSql = ""
            sSql &= "select requisicao_id,cod_ean,DDD,telefone,valor,operador,aceite, hist_aceite, categoria,mesRefSeguinte,dt_inclusao,nome_cliente,cpf,autorizante,observacao " & vbNewLine
            sSql &= " from integracao_cemig_db.dbo.requisicao_cemig_tb" & vbNewLine
            sSql &= "where convert(date,dt_inclusao) between '" & dtInicial & "' and '" & dtFinal & "'"
            If tipoAceite <> 3 Then
                sSql &= "and aceite = '" & aceite & "'"
            End If
            If verificaGrau = True Then
                If naoConforme = True Then
                    sSql &= "and nao_conforme = 1"
                ElseIf naoConforme = False Then
                    sSql &= "and nao_conforme = 0"
                End If
            End If
            If TipoPesquisa = 0 Then
                sSql &= "and categoria = " & Pesquisa
            ElseIf TipoPesquisa = 1 Then
                Dim operador As String = ""
                Dim sSqlAux As String = ""
                sSqlAux = ""
                sSqlAux = "Select nome_funcionario from controle_db..funcionarios_tb where cod_ean_funcionario = " & Pesquisa

                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSqlAux)

                If (ds.Tables(0).Rows.Count > 0) Then
                    operador = ds.Tables(0).Rows.Item(0).Item(0)
                Else
                    operador = ""
                End If

                sSql &= "and operador = '" & operador & "'"
            End If


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function RetornaRequisicaoPorPeriodoDataCategoria(ByVal dtInicial As Date, ByVal dtFinal As Date, ByVal tipoAceite As Integer, ByVal categoria As Integer, ByVal operador As Integer) As DataSet
        Dim sSql As String
        Dim aceite As String = ""

        Try
            If tipoAceite = 0 Then
                aceite = "S"
            ElseIf tipoAceite = 1 Then
                aceite = "N"
            ElseIf tipoAceite = 2 Then
                aceite = "C"
            End If

            sSql = ""
            sSql &= "create table #resumo" & vbNewLine
            sSql &= "(valor_aceite  decimal null" & vbNewLine
            sSql &= ",total_fichas_aceitas int null" & vbNewLine
            sSql &= ",total_fichas int null)" & vbNewLine

            sSql &= "insert into #resumo (valor_aceite)" & vbNewLine
            sSql &= "select sum(valor) as valor_aceite" & vbNewLine
            sSql &= "from controle_db..requisicao_oi_tb" & vbNewLine
            sSql &= "where operador_id = " & operador & vbNewLine
            If tipoAceite <> 3 Then
                sSql &= "and aceite = '" & aceite & "'"
            End If
            sSql &= "and convert(date,dt_inclusao) between '" & dtInicial & "' and '" & dtFinal & "'" & vbNewLine


            sSql &= "update #resumo " & vbNewLine
            sSql &= "set total_fichas_aceitas = (select COUNT(*) as total_fichas_aceitas" & vbNewLine
            sSql &= "from controle_db..requisicao_oi_tb" & vbNewLine
            sSql &= "where operador_id = " & operador & vbNewLine
            If tipoAceite <> 3 Then
                sSql &= "and aceite = '" & aceite & "'"
            End If
            sSql &= "and convert(date,dt_inclusao) between '" & dtInicial & "' and '" & dtFinal & "')" & vbNewLine


            sSql &= "update #resumo " & vbNewLine
            sSql &= "set total_fichas = (select COUNT(*) as total_fichas" & vbNewLine
            sSql &= "from controle_db..requisicao_tb" & vbNewLine
            sSql &= "where atual = 'S'and Vinculado_Operador = 'S' and operador_id = " & operador & ")" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "select valor_aceite, total_fichas_aceitas, total_fichas  from #resumo" & vbNewLine


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    Public Function CarregarValoresAtuaisMes(ByVal operador As Integer) As DataSet
        Dim sSql As String
        Dim aceite As String = ""

        Try


            sSql = ""
            sSql &= "DECLARE @DATA DATETIME, @primeirodia smalldatetime, @ultimodiames DATETIME, @dataHabilitada datetime " & vbNewLine
            sSql &= "SET @DATA = getdate() " & vbNewLine
            sSql &= "SELECT @primeirodia = CONVERT(VARCHAR, @DATA - DAY(@DATA) + 1, 103) " & vbNewLine
            sSql &= "SELECT @ultimodiames = DATEADD(d, -DAY(GETDATE()),DATEADD(m,1,GETDATE())) " & vbNewLine
            sSql &= "set @ultimodiames = cast(convert(char(8),@ultimodiames,112) as smalldatetime) " & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "create table #resumo" & vbNewLine
            sSql &= "(valor_aceite  decimal null" & vbNewLine
            sSql &= ",total_fichas_aceitas int null" & vbNewLine
            sSql &= ",total_fichas int null)" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "insert into #resumo (valor_aceite)" & vbNewLine
            sSql &= "select isnull(sum(valor),0)  as valor_aceite" & vbNewLine
            sSql &= "from controle_db..requisicao_oi_tb" & vbNewLine
            sSql &= "where operador_id = " & operador & vbNewLine
            sSql &= "and aceite = 'S' " & vbNewLine
            sSql &= "and convert(date,dt_inclusao) between @primeirodia and @ultimodiames" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "update #resumo " & vbNewLine
            sSql &= "set total_fichas_aceitas = (select COUNT(*) as total_fichas_aceitas" & vbNewLine
            sSql &= "from controle_db..requisicao_oi_tb" & vbNewLine
            sSql &= "where operador_id = " & operador & vbNewLine
            sSql &= "and aceite = 'S' " & vbNewLine
            sSql &= "and  convert(date,dt_inclusao) between @primeirodia and @ultimodiames )" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "update #resumo " & vbNewLine
            sSql &= "set total_fichas = (select COUNT(*) as total_fichas" & vbNewLine
            sSql &= "from controle_db..requisicao_tb" & vbNewLine
            sSql &= "where atual = 'S'and Vinculado_Operador = 'S'  and confirmado is null and operador_id = " & operador & ")" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "select valor_aceite, total_fichas_aceitas, total_fichas  from #resumo" & vbNewLine


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function CarregarValoresAtuaisDia(ByVal operador As Integer) As DataSet
        Dim sSql As String
        Dim aceite As String = ""

        Try

            sSql = ""
            sSql &= "DECLARE @DATA DATETIME, @primeirodia smalldatetime, @ultimodiames DATETIME, @dataHabilitada datetime " & vbNewLine
            sSql &= "SET @DATA = getdate() " & vbNewLine
            sSql &= "SELECT @primeirodia = CONVERT(VARCHAR, @DATA - DAY(@DATA) + 1, 103) " & vbNewLine
            sSql &= "SELECT @ultimodiames = DATEADD(d, -DAY(GETDATE()),DATEADD(m,1,GETDATE())) " & vbNewLine
            sSql &= "set @ultimodiames = cast(convert(char(8),@ultimodiames,112) as smalldatetime) " & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "create table #resumo" & vbNewLine
            sSql &= "(valor_aceite  decimal null" & vbNewLine
            sSql &= ",total_fichas_aceitas int null" & vbNewLine
            sSql &= ",total_fichas int null)" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "insert into #resumo (valor_aceite)" & vbNewLine
            sSql &= "select isnull(sum(valor),0) as valor_aceite" & vbNewLine
            sSql &= "from controle_db..requisicao_oi_tb" & vbNewLine
            sSql &= "where operador_id = " & operador & vbNewLine
            sSql &= "and aceite = 'S' " & vbNewLine
            sSql &= "and convert(date, dt_inclusao) = convert (date,getdate()) " & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "update #resumo " & vbNewLine
            sSql &= "set total_fichas_aceitas = (select COUNT(*) as total_fichas_aceitas" & vbNewLine
            sSql &= "from controle_db..requisicao_oi_tb" & vbNewLine
            sSql &= "where operador_id = " & operador & vbNewLine
            sSql &= "and aceite = 'S' " & vbNewLine
            sSql &= "and convert(date, dt_inclusao) = convert (date,getdate()) )" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "update #resumo " & vbNewLine
            sSql &= "set total_fichas = (select COUNT(*) as total_fichas" & vbNewLine
            sSql &= "from controle_db..requisicao_tb" & vbNewLine
            sSql &= "where atual = 'S'and Vinculado_Operador = 'S'  and confirmado is null and operador_id = " & operador & ")" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "select valor_aceite, total_fichas_aceitas, total_fichas  from #resumo" & vbNewLine


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function RetornaRequisicaoManualPorPeriodoDataCategoria(ByVal dtInicial As Date, ByVal dtFinal As Date, ByVal tipoAceite As Integer, ByVal categoria As Integer, ByVal operador As Integer) As DataSet
        Dim sSql As String
        Dim aceite As String = ""

        Try
            sSql = ""
            sSql &= "create table #resumo" & vbNewLine
            sSql &= "(valor_aceite  decimal null" & vbNewLine
            sSql &= ",total_fichas_aceitas int null" & vbNewLine
            sSql &= ",total_fichas int null)" & vbNewLine

            sSql &= "insert into #resumo (valor_aceite)" & vbNewLine
            sSql &= "select isnull(sum(valor),0)  as valor_aceite" & vbNewLine
            sSql &= "from controle_db..requisicao_lista_tb" & vbNewLine
            sSql &= "where cod_opera = " & operador & vbNewLine
            sSql &= "and situacao = 2 "
            If categoria > 0 Then
                sSql &= "and cod_categoria = " & categoria
            End If
            sSql &= "and convert(date,dt_alteracao) between '" & dtInicial & "' and '" & dtFinal & "'" & vbNewLine



            sSql &= "update #resumo " & vbNewLine
            sSql &= "set total_fichas_aceitas = (select COUNT(*) as total_fichas_aceitas" & vbNewLine
            sSql &= "from controle_db..requisicao_lista_tb" & vbNewLine
            sSql &= "where cod_opera = " & operador & vbNewLine
            sSql &= "and situacao = 2 "
            If categoria > 0 Then
                sSql &= "and cod_categoria = " & categoria
            End If
            sSql &= "and convert(date,dt_alteracao) between '" & dtInicial & "' and '" & dtFinal & "')" & vbNewLine



            sSql &= "update #resumo " & vbNewLine
            sSql &= "set total_fichas = (select COUNT(*) as total_fichas" & vbNewLine
            sSql &= "from controle_db..requisicao_lista_tb" & vbNewLine
            sSql &= " where situacao = 1 and cod_opera = " & operador & ")" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "select valor_aceite, total_fichas_aceitas, total_fichas  from #resumo" & vbNewLine


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function CarregarManualValoresAtuaisMes(ByVal operador As Integer) As DataSet
        Dim sSql As String
        Dim aceite As String = ""

        Try


            sSql = ""
            sSql &= "DECLARE @DATA DATETIME, @primeirodia smalldatetime, @ultimodiames DATETIME, @dataHabilitada datetime " & vbNewLine
            sSql &= "SET @DATA = getdate() " & vbNewLine
            sSql &= "SELECT @primeirodia = CONVERT(VARCHAR, @DATA - DAY(@DATA) + 1, 103) " & vbNewLine
            sSql &= "SELECT @ultimodiames = DATEADD(d, -DAY(GETDATE()),DATEADD(m,1,GETDATE())) " & vbNewLine
            sSql &= "set @ultimodiames = cast(convert(char(8),@ultimodiames,112) as smalldatetime) " & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "create table #resumo" & vbNewLine
            sSql &= "(valor_aceite  decimal null" & vbNewLine
            sSql &= ",total_fichas_aceitas int null" & vbNewLine
            sSql &= ",total_fichas int null)" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "insert into #resumo (valor_aceite)" & vbNewLine
            sSql &= "select isnull(sum(valor),0) as valor_aceite" & vbNewLine
            sSql &= "from controle_db..requisicao_lista_tb" & vbNewLine
            sSql &= "where cod_opera = " & operador & vbNewLine
            sSql &= "and situacao = 2 " & vbNewLine
            sSql &= "and  convert(date,dt_alteracao) between @primeirodia and @ultimodiames" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "update #resumo " & vbNewLine
            sSql &= "set total_fichas_aceitas = (select COUNT(*) as total_fichas_aceitas" & vbNewLine
            sSql &= "from controle_db..requisicao_lista_tb" & vbNewLine
            sSql &= "where cod_opera = " & operador & vbNewLine
            sSql &= "and situacao = 2  " & vbNewLine
            sSql &= "and convert(date,dt_alteracao) between @primeirodia and @ultimodiames )" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "update #resumo " & vbNewLine
            sSql &= "set total_fichas = (select COUNT(*) as total_fichas" & vbNewLine
            sSql &= "from controle_db..requisicao_lista_tb" & vbNewLine
            sSql &= "where situacao = 1 and cod_opera = " & operador & ")" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "select valor_aceite, total_fichas_aceitas, total_fichas  from #resumo" & vbNewLine


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function CarregarManualValoresAtuaisDia(ByVal operador As Integer) As DataSet
        Dim sSql As String
        Dim aceite As String = ""

        Try

            sSql = ""
            sSql &= "DECLARE @DATA DATETIME, @primeirodia smalldatetime, @ultimodiames DATETIME, @dataHabilitada datetime " & vbNewLine
            sSql &= "SET @DATA = getdate() " & vbNewLine
            sSql &= "SELECT @primeirodia = CONVERT(VARCHAR, @DATA - DAY(@DATA) + 1, 103) " & vbNewLine
            sSql &= "SELECT @ultimodiames = DATEADD(d, -DAY(GETDATE()),DATEADD(m,1,GETDATE())) " & vbNewLine
            sSql &= "set @ultimodiames = cast(convert(char(8),@ultimodiames,112) as smalldatetime) " & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "create table #resumo" & vbNewLine
            sSql &= "(valor_aceite  decimal null" & vbNewLine
            sSql &= ",total_fichas_aceitas int null" & vbNewLine
            sSql &= ",total_fichas int null)" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "insert into #resumo (valor_aceite)" & vbNewLine
            sSql &= "select isnull(sum(valor),0) as valor_aceite" & vbNewLine
            sSql &= "from controle_db..requisicao_lista_tb" & vbNewLine
            sSql &= "where cod_opera = " & operador & vbNewLine
            sSql &= "and situacao = 2  " & vbNewLine
            sSql &= "and convert(date, dt_alteracao) = convert (date,getdate()) " & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "update #resumo " & vbNewLine
            sSql &= "set total_fichas_aceitas = (select COUNT(*) as total_fichas_aceitas" & vbNewLine
            sSql &= "from controle_db..requisicao_lista_tb" & vbNewLine
            sSql &= "where cod_opera = " & operador & vbNewLine
            sSql &= "and situacao = 2  " & vbNewLine
            sSql &= "and convert(date, dt_alteracao) = convert (date,getdate()) )" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "update #resumo " & vbNewLine
            sSql &= "set total_fichas = (select COUNT(*) as total_fichas" & vbNewLine
            sSql &= "from controle_db..requisicao_lista_tb" & vbNewLine
            sSql &= "where situacao = 1 and cod_opera = " & operador & ")" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "select valor_aceite, total_fichas_aceitas, total_fichas  from #resumo" & vbNewLine


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function CarregarFaturamento(ByVal tipo_mov As String, ByVal funcionario As String, ByVal data_movimentacao As String) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec controle_db.dbo.carregaFaturamento_SPS " & vbNewLine
            sSql &= " @tipo_mov = '" & tipo_mov & "'" & vbNewLine
            sSql &= ", @operador = '" & funcionario & "'" & vbNewLine
            sSql &= ", @data_mov = '" & data_movimentacao & "'"


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function CarregarFaturamentoCod(ByVal COD_MOV_CONTRIBUICAO As Integer) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec controle_db.dbo.carregaFaturamento_Cod_SPS " & vbNewLine
            sSql &= " @COD_MOV_CONTRIBUICAO = " & COD_MOV_CONTRIBUICAO


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function CarregarMovimentacao(ByVal data_inicio As String, ByVal data_fim As String) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec controle_db.dbo.Retorna_movimentacao_SPS " & vbNewLine
            sSql &= " @dt_inicio = '" & data_inicio & "'" & vbNewLine
            sSql &= ", @dt_fim = '" & data_fim & "'"


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function Atualiza_mov_contribuicao_descr(ByVal cod_mov_descr As Long) As Boolean
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec controle_db.dbo.Atualiza_mov_contribuicao_descr_SPU " & vbNewLine
            sSql &= " @COD_MOV_CONTRIBUICAO_DESCR = " & cod_mov_descr


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function AtualizaFaturamento(ByVal tipo_mov As String, ByVal funcionario As String, ByVal data_movimentacao As String) As Boolean
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec controle_db.dbo.AtualizaFaturamento_SPU " & vbNewLine
            sSql &= " @tipo_mov = '" & tipo_mov & "'" & vbNewLine
            sSql &= ", @operador = '" & funcionario & "'" & vbNewLine
            sSql &= ", @data_mov = '" & data_movimentacao & "'"


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function CadastroUsuario(ByVal usuario As CGenerico, ByVal operacao As String) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= "Exec controle_db..Cadastra_Usuario_SPI" & vbNewLine
            sSql &= "@usuario = '" & usuario.Usuario & "', " & vbNewLine
            sSql &= "@senha = '" & usuario.Senha & "', " & vbNewLine
            sSql &= "@cod_acesso = " & usuario.Acesso & ", " & vbNewLine
            sSql &= "@cod_empresa = " & usuario.Cod_empresa & ", " & vbNewLine
            sSql &= "@operacao = '" & operacao & "'" & vbNewLine
            sSql &= ",@Cod_Ean_Funcionario = '" & IIf(IsDBNull(usuario.Cod_Ean_Funcionario), "01", usuario.Cod_Ean_Funcionario) & "'" & vbNewLine
            sSql &= ",@Nome_Funcionario = '" & usuario.Nome_Funcionario & "'" & vbNewLine
            sSql &= ",@Cpf = '" & usuario.Cpf & "'" & vbNewLine
            sSql &= ",@RG = '" & usuario.RG & "'" & vbNewLine
            sSql &= ",@Email = '" & usuario.Email & "'" & vbNewLine
            sSql &= ",@DDD = '" & usuario.DDD & "'" & vbNewLine
            sSql &= ",@Telefone = '" & usuario.Telefone & "'" & vbNewLine
            sSql &= ",@Celular = '" & usuario.Celular & "'" & vbNewLine
            sSql &= ",@Endereco = '" & usuario.Endereco & "'" & vbNewLine
            sSql &= ",@Uf_Cidade = '" & usuario.Uf_Cidade & "'" & vbNewLine
            sSql &= ",@Cep = '" & usuario.Cep & "'" & vbNewLine
            sSql &= ",@Cod_regiao = null " & vbNewLine
            sSql &= ",@Data_Nascimento = '" & usuario.Data_Nascimento & "'" & vbNewLine
            sSql &= ",@Data_admissao = '" & usuario.Data_admissao & "'" & vbNewLine
            sSql &= ",@Data_demissao = '" & usuario.Data_demissao & "'" & vbNewLine
            sSql &= ",@Observacao = '" & usuario.Observacao & "'" & vbNewLine
            sSql &= ",@Quant_1 = " & usuario.Quant_1 & "" & vbNewLine
            sSql &= ",@Meta_1 = " & usuario.Meta_1 & "" & vbNewLine
            sSql &= ",@Comissao_1 = " & usuario.Comissao_1 & "" & vbNewLine
            sSql &= ",@Quant_2 = " & usuario.Quant_2 & "" & vbNewLine
            sSql &= ",@Meta_2 = " & usuario.Meta_2 & "" & vbNewLine
            sSql &= ",@Comissao_2 = " & usuario.Comissao_2 & "" & vbNewLine
            sSql &= ",@Quant_3 = " & usuario.Quant_3 & "" & vbNewLine
            sSql &= ",@Meta_3 = " & usuario.Meta_3 & "" & vbNewLine
            sSql &= ",@Comissao_3 = " & usuario.Comissao_3 & "" & vbNewLine
            sSql &= ",@Cod_Bairro = " & usuario.Cod_Bairro & "" & vbNewLine
            sSql &= ",@Cod_Cargo = " & usuario.Cod_Cargo & "" & vbNewLine
            sSql &= ",@Cod_Cidade = " & usuario.Cod_Cidade & "" & vbNewLine
            sSql &= ",@Sexo = '" & usuario.Sexo & "'" & vbNewLine
            sSql &= ",@cargo = " & usuario.Cod_Cargo


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)



            Return True

        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function ExcluirRequisicaoselecionada(ByVal requisicaoId As Long) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= " update controle_db.dbo.requisicao_oi_tb" & vbNewLine
            sSql &= " set aceite = 'C'"
            sSql &= " where requisicao_id = " & requisicaoId

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function ExcluirRequisicaoCemigSelecionada(ByVal requisicaoId As Long) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= " update integracao_cemig_db.dbo.requisicao_cemig_tb" & vbNewLine
            sSql &= " set aceite = 'C'"
            sSql &= " where requisicao_id = " & requisicaoId

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function RetornaTelefonesAnaliseExclusao() As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "select cod_ean_cliente,telefone,cod_opera,operador,dia_solicitacao,operador_exclusao,dia_exclusao " & vbNewLine
            sSql &= " from controle_db.dbo.solicitacao_cancelamento_tb " & vbNewLine
            sSql &= " where dia_exclusao is null " & vbNewLine


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function AtualizaTelefonesAnaliseExclusao(ByVal contribuinte As CContribuinte) As Boolean
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "update controle_db.dbo.solicitacao_cancelamento_tb " & vbNewLine
            sSql &= " set operador_exclusao = '" & contribuinte.Operador & "'" & vbNewLine
            sSql &= " , dia_exclusao = GETDATE() " & vbNewLine
            sSql &= " where telefone = '" & contribuinte.Telefone1 & "'"

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function RetornaIDFuncionario() As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "select max (cod_funcionario)+ 1 " & vbNewLine
            sSql &= " from controle_db.dbo.funcionarios_tb" & vbNewLine


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Function VerificaTelefoneNuncaCriticado(ByVal telefone As String) As Boolean
        Try
            Dim IsNuncaCriticado As String
            Dim ds As New DataSet
            Dim sSql As String = ""
            sSql &= "Exec controle_db.DBO.VerificaTelefoneNuncaCriticado" & vbNewLine
            sSql &= "@telefone = '" & telefone & "'"
            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
            If (ds.Tables(0).Rows.Count > 0) Then
                IsNuncaCriticado = ds.Tables(0).Rows.Item(0).Item(0)
                If IsNuncaCriticado <> "" Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If

        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    Public Function RetornaCarregarCriticadosContestados(dtConsulta As Date, tipoCritica As String) As DataSet
        Try
            Dim sSql As String = ""
            sSql &= $"exec controle_db..GERA_RELATORIO_EXCLUSAO 'S','{dtConsulta}', '{tipoCritica.Substring(0, 1).ToUpper}'"
            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function CarregaProducaoMensal(dataInicial As Date, dataFinal As Date) As DataSet
        Try
            Dim sSql As String = ""
            sSql &= $"exec controle_db..RETORNA_PRODUCAO_MENSAL '{dataInicial}', '{dataFinal}'"
            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


#Region "Auditoria Oi"

    Public Function CriaPlanilhaAuditoriaOi(ByVal dataInicial As String, ByVal dataFinal As String, ByVal operadoras As String, ByVal Condicao1 As List(Of String), ByVal Condicao2 As List(Of String), ByVal planilhaInterna As Boolean) As DataSet
        Try
            Dim aceite As String = ""
            Dim sSql As String = ""
            sSql &= "create table #t " & vbNewLine
            sSql &= "(cont int identity (1,1) ," & vbNewLine
            sSql &= " id int null," & vbNewLine
            sSql &= " nome_cliente varchar(200) null," & vbNewLine
            sSql &= " titular varchar(200) null," & vbNewLine
            sSql &= " ddd char(4) null," & vbNewLine
            sSql &= " telefone varchar(22) null," & vbNewLine
            sSql &= " valor decimal null," & vbNewLine
            sSql &= " dt_inicial varchar(30) null," & vbNewLine
            sSql &= " dt_final varchar(30) null," & vbNewLine
            sSql &= " endereco varchar(200) null," & vbNewLine
            sSql &= " cidade varchar(50) null," & vbNewLine
            sSql &= " bairro varchar(50) null," & vbNewLine
            sSql &= " uf varchar(20) null," & vbNewLine
            sSql &= " rg varchar(20) null," & vbNewLine
            sSql &= " cpf varchar(20) null," & vbNewLine
            sSql &= " periodo varchar(50) null," & vbNewLine
            sSql &= " dt_inclusao date null," & vbNewLine
            sSql &= " hora_inclusao smalldatetime null," & vbNewLine
            sSql &= " operador varchar(30) null," & vbNewLine
            sSql &= " cod_ean varchar(20) null," & vbNewLine
            sSql &= " aceite char(1) null," & vbNewLine
            sSql &= " requisicao_id int null," & vbNewLine
            sSql &= " mesRef varchar(10)" & vbNewLine
            sSql &= ")" & vbNewLine

            sSql &= " declare @dataInicial varchar(14), @dataFinal varchar(14), @dtInicial date, @dtFinal date" & vbNewLine
            sSql &= "set @dataInicial = '" & CDate(dataInicial).ToString("dd/MM/yyyy") & "' " & vbNewLine
            sSql &= "set @dataFinal = '" & CDate(dataFinal).ToString("dd/MM/yyyy") & "' " & vbNewLine

            sSql &= " insert into #t ( id , nome_cliente , titular , ddd , telefone , valor , dt_inicial , dt_final , endereco, bairro,cidade, uf , rg , cpf, periodo, dt_inclusao, hora_inclusao, operador,cod_ean, aceite, requisicao_id,mesRef )" & vbNewLine
            sSql &= " select 27178 as id, c.nome_cliente,c.nome_cliente as titular, a.DDD, a.telefone, a.valor, @dataInicial as dt_inicial, @dataFinal as dt_final, b.endereco, b.bairro, b.cidade, b.uf, null as rg,isnull(a.cpf,'') as cpf,@dataInicial +' À ' + @dataFinal as periodo, a.dt_inclusao , a.dt_inclusao, a.operador, a.cod_ean, a.aceite, a.requisicao_id,a.mesRef   " & vbNewLine
            sSql &= " from controle_db..requisicao_oi_tb a " & vbNewLine
            sSql &= " left join controle_db..cliente_tb b" & vbNewLine
            sSql &= " on a.telefone = b.telefone1" & vbNewLine
            sSql &= " and a.ddd = b.ddd" & vbNewLine
            sSql &= " left join controle_db..Lista_Telefonica_tb c" & vbNewLine
            sSql &= " on a.telefone = c.telefone" & vbNewLine
            sSql &= " and a.ddd = b.ddd" & vbNewLine
            sSql &= " where convert(date,a.dt_inclusao) between '" & CDate(dataInicial).ToString("yyyyMMdd") & "' and '" & CDate(dataFinal).ToString("yyyyMMdd") & "' and aceite = 'S'" & vbNewLine
            sSql &= " and a.operador in  ( " & operadoras & ")" & vbNewLine
            sSql &= " group by   c.nome_cliente,c.nome_cliente, a.DDD, a.telefone, a.valor, b.endereco, b.bairro, b.cidade, b.uf, a.cpf, a.dt_inclusao,a.operador, a.cod_ean, a.aceite, requisicao_id,a.mesRef       " & vbNewLine

            sSql &= " update t" & vbNewLine
            sSql &= " set nome_cliente  = b.nome_cliente" & vbNewLine
            sSql &= " from #t t" & vbNewLine
            sSql &= " inner join controle_db..requisicao_oi_tb b" & vbNewLine
            sSql &= " on t.telefone = b.telefone" & vbNewLine
            sSql &= " and t.ddd = b.ddd" & vbNewLine
            sSql &= " where t.nome_cliente is null" & vbNewLine
            sSql &= " and (b.nome_cliente is not null or b.nome_cliente <> '')" & vbNewLine

            sSql &= " update t" & vbNewLine
            sSql &= " set titular  = b.autorizante" & vbNewLine
            sSql &= " from #t t" & vbNewLine
            sSql &= " inner join controle_db..requisicao_oi_tb b" & vbNewLine
            sSql &= " on t.telefone = b.telefone" & vbNewLine
            sSql &= " and t.ddd = b.ddd" & vbNewLine
            sSql &= " where t.titular is null" & vbNewLine
            sSql &= " and (b.autorizante is not null or b.autorizante <> '')" & vbNewLine

            sSql &= " update t" & vbNewLine
            sSql &= " set endereco  = b.endereco" & vbNewLine
            sSql &= " from #t t" & vbNewLine
            sSql &= " inner join controle_db..cliente_tb b" & vbNewLine
            sSql &= " on t.cod_ean = b.cod_ean_cliente" & vbNewLine
            sSql &= " where t.endereco is null" & vbNewLine
            sSql &= " and b.endereco is not null" & vbNewLine

            sSql &= " update t" & vbNewLine
            sSql &= " set endereco  = b.endereco" & vbNewLine
            sSql &= " from #t t" & vbNewLine
            sSql &= " inner join controle_db..lista_retida_tmp_tb b" & vbNewLine
            sSql &= " on t.cod_ean = b.cod_ean_cliente" & vbNewLine
            sSql &= " where t.endereco is null" & vbNewLine
            sSql &= " and b.endereco is not null" & vbNewLine

            sSql &= " update t" & vbNewLine
            sSql &= " set cidade  = b.cidade" & vbNewLine
            sSql &= " from #t t" & vbNewLine
            sSql &= " inner join controle_db..cliente_tb b" & vbNewLine
            sSql &= " on t.cod_ean = b.cod_ean_cliente" & vbNewLine
            sSql &= " where t.cidade is null" & vbNewLine
            sSql &= " and b.cidade is not null" & vbNewLine

            sSql &= " update t" & vbNewLine
            sSql &= " set cidade  = b.cidade" & vbNewLine
            sSql &= " from #t t" & vbNewLine
            sSql &= " inner join controle_db..lista_retida_tmp_tb b" & vbNewLine
            sSql &= " on t.cod_ean = b.cod_ean_cliente" & vbNewLine
            sSql &= " where t.cidade is null" & vbNewLine
            sSql &= " and b.cidade is not null" & vbNewLine

            sSql &= " update t" & vbNewLine
            sSql &= " set bairro  = b.bairro" & vbNewLine
            sSql &= " from #t t" & vbNewLine
            sSql &= " inner join controle_db..cliente_tb b" & vbNewLine
            sSql &= " on t.cod_ean = b.cod_ean_cliente" & vbNewLine
            sSql &= " where t.bairro is null" & vbNewLine
            sSql &= " and b.bairro is not null" & vbNewLine

            sSql &= " update t" & vbNewLine
            sSql &= " set bairro  = b.bairro" & vbNewLine
            sSql &= " from #t t" & vbNewLine
            sSql &= " inner join controle_db..lista_retida_tmp_tb b" & vbNewLine
            sSql &= " on t.cod_ean = b.cod_ean_cliente" & vbNewLine
            sSql &= " where t.bairro is null" & vbNewLine
            sSql &= " and b.bairro is not null" & vbNewLine

            sSql &= " update t" & vbNewLine
            sSql &= " set nome_cliente  = b.nome_cliente" & vbNewLine
            sSql &= " from #t t" & vbNewLine
            sSql &= " inner join digital..t_clientes b" & vbNewLine
            sSql &= " on t.cod_ean = b.cod_ean_cliente" & vbNewLine
            sSql &= " where t.nome_cliente is null" & vbNewLine
            sSql &= " and (b.nome_cliente is not null or b.nome_cliente <> '')" & vbNewLine

            sSql &= " update t" & vbNewLine
            sSql &= " set nome_cliente  = b.nome_cliente" & vbNewLine
            sSql &= " from #t t" & vbNewLine
            sSql &= " inner join digital..t_lista_retida_tmp b" & vbNewLine
            sSql &= " on t.cod_ean = b.cod_ean_cliente" & vbNewLine
            sSql &= " where t.nome_cliente is null" & vbNewLine
            sSql &= " and (b.nome_cliente is not null or b.nome_cliente <> '')" & vbNewLine

            sSql &= " update t" & vbNewLine
            sSql &= " set endereco  = b.endereco" & vbNewLine
            sSql &= " from #t t" & vbNewLine
            sSql &= " inner join digital..t_clientes b" & vbNewLine
            sSql &= " on t.cod_ean = b.cod_ean_cliente" & vbNewLine
            sSql &= " where t.endereco is null" & vbNewLine
            sSql &= " and b.endereco is not null" & vbNewLine

            sSql &= " update t" & vbNewLine
            sSql &= " set endereco  = b.endereco" & vbNewLine
            sSql &= " from #t t" & vbNewLine
            sSql &= " inner join digital..t_lista_retida_tmp b" & vbNewLine
            sSql &= " on t.cod_ean = b.cod_ean_cliente" & vbNewLine
            sSql &= " where t.endereco is null" & vbNewLine
            sSql &= " and b.endereco is not null" & vbNewLine

            sSql &= " update t" & vbNewLine
            sSql &= " set cidade  = b.cidade" & vbNewLine
            sSql &= " from #t t" & vbNewLine
            sSql &= " inner join digital..t_clientes b" & vbNewLine
            sSql &= " on t.cod_ean = b.cod_ean_cliente" & vbNewLine
            sSql &= " where t.cidade is null" & vbNewLine
            sSql &= " and b.cidade is not null" & vbNewLine

            sSql &= " update t" & vbNewLine
            sSql &= " set cidade  = b.cidade" & vbNewLine
            sSql &= " from #t t" & vbNewLine
            sSql &= " inner join digital..t_lista_retida_tmp b" & vbNewLine
            sSql &= " on t.cod_ean = b.cod_ean_cliente" & vbNewLine
            sSql &= " where t.cidade is null" & vbNewLine
            sSql &= " and b.cidade is not null" & vbNewLine

            sSql &= " update t" & vbNewLine
            sSql &= " set bairro  = b.bairro" & vbNewLine
            sSql &= " from #t t" & vbNewLine
            sSql &= " inner join digital..t_clientes b" & vbNewLine
            sSql &= " on t.cod_ean = b.cod_ean_cliente" & vbNewLine
            sSql &= " where t.bairro is null" & vbNewLine
            sSql &= " and b.bairro is not null" & vbNewLine

            sSql &= " update t" & vbNewLine
            sSql &= " set bairro  = b.bairro" & vbNewLine
            sSql &= " from #t t" & vbNewLine
            sSql &= " inner join digital..t_lista_retida_tmp b" & vbNewLine
            sSql &= " on t.cod_ean = b.cod_ean_cliente" & vbNewLine
            sSql &= " where t.bairro is null" & vbNewLine
            sSql &= " and b.bairro is not null" & vbNewLine

            sSql &= " delete from  #t " & vbNewLine
            sSql &= " where telefone in " & vbNewLine
            sSql &= " (select telefone from  #t " & vbNewLine
            sSql &= " group by ddd,telefone " & vbNewLine
            sSql &= " having Count(telefone)>1) " & vbNewLine
            sSql &= " and not cont in " & vbNewLine
            sSql &= " (select MAX(cont) from  #t " & vbNewLine
            sSql &= " group by ddd,telefone " & vbNewLine
            sSql &= " having Count(telefone)>1) " & vbNewLine

            sSql &= " delete from #t" & vbNewLine
            sSql &= " where nome_cliente is null " & vbNewLine

            sSql &= " delete from #t" & vbNewLine
            sSql &= " where nome_cliente = ''" & vbNewLine

            sSql &= "  delete from #t" & vbNewLine
            sSql &= "  where valor = 0 " & vbNewLine

            If Condicao1.Count > 0 Then

                For Each linha As String In Condicao1
                    sSql += linha
                Next

            End If

            sSql &= "  create table #temp (ddd char(4), telefone varchar(20))" & vbNewLine

            If Condicao2.Count > 0 Then

                For Each linha As String In Condicao2
                    sSql += linha
                Next

            End If

            sSql &= " Delete a " & vbNewLine
            sSql &= " from #t a " & vbNewLine
            sSql &= " inner join #temp b " & vbNewLine
            sSql &= " on a.ddd = b.ddd " & vbNewLine
            sSql &= " and a.telefone = b.telefone " & vbNewLine


            If planilhaInterna = False Then
                sSql &= "select id, nome_cliente,titular, DDD, telefone, valor, dt_inicial, dt_final, endereco, bairro, cidade, uf, rg, cpf, periodo " & vbNewLine
                sSql &= "from #t" & vbNewLine
            Else
                sSql &= "select a.DDD, a.telefone,  a.nome_cliente,a.operador,a.hora_inclusao " & vbNewLine
                sSql &= "from #t a order by dt_inclusao" & vbNewLine
            End If


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    Public Function CriaArquivoRequisicaoOiExportar(ByVal dataInicial As String, ByVal dataFinal As String) As DataSet
        Try
            Dim aceite As String = ""
            Dim sSql As String = ""
            sSql &= " declare @dataInicial varchar(14), @dataFinal varchar(14), @dtInicial date, @dtFinal date" & vbNewLine
            sSql &= "set @dataInicial = '" & CDate(dataInicial).ToString("dd/MM/yyyy") & "' " & vbNewLine
            sSql &= "set @dataFinal = '" & CDate(dataFinal).ToString("dd/MM/yyyy") & "' " & vbNewLine

            sSql &= " select requisicao_id,cod_ean,DDD,telefone,valor,operador_id,operador,aceite,categoria," & vbNewLine
            sSql &= " mesRef, mesRefSeguinte, dt_inclusao, dt_alteracao, usuario, nome_cliente, cpf, autorizante, " & vbNewLine
            sSql &= " observacao,nao_conforme,hist_aceite,exportado,valor_total  " & vbNewLine
            sSql &= " from controle_db.dbo.requisicao_oi_tb a " & vbNewLine
            sSql &= " where convert(date,a.dt_inclusao) between '" & CDate(dataInicial).ToString("yyyyMMdd") & "' and '" & CDate(dataFinal).ToString("yyyyMMdd") & "'"
            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
        Catch ex As Exception
            MsgBox(ex)
            Return Nothing
        End Try
    End Function

    Public Function CriaArquivoRequisicaoExportar() As DataSet
        Try
            Dim aceite As String = ""
            Dim sSql As String = ""
            sSql &= " select requisicao_id,numero_requisicao,cod_ean,DDD,telefone,valor,operador_id,operador,aceite,categoria,mesRef," & vbNewLine
            sSql &= " dt_inclusao,dt_alteracao,usuario,nome_cliente,Atual,Vinculado_Operador,confirmado,observacao,cod_empresa,ultimo_aceite " & vbNewLine
            sSql &= " from controle_db..requisicao_tb " & vbNewLine
            sSql &= " where atual =  'S'"
            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Function CriaArquivoListaTelefonicaExportar() As DataSet
        Try
            Dim aceite As String = ""
            Dim sSql As String = ""
            sSql &= " select lista_telefonica_id,cod_ean,DDD,telefone,valor,nome_cliente,categoria,mesSituacao," & vbNewLine
            sSql &= " Situacao,dt_inclusao,dt_alteracao,usuario,cod_cidade " & vbNewLine
            sSql &= " from controle_db..lista_telefonica_tb " & vbNewLine
            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Function CriaArquivoTelefoneExclusaoExportar() As DataSet
        Try
            Dim aceite As String = ""
            Dim sSql As String = ""
            sSql &= " select ID,DDD,Telefone,dt_inclusao,motivo,solicitante,observacao" & vbNewLine
            sSql &= " from controle_db..telefone_Exclusao " & vbNewLine
            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
        Catch ex As Exception
            Return Nothing
        End Try



    End Function


    Public Function RetornaLigacoesPlanilhaOi(ByVal telefones As List(Of String), ByVal dtInicio As String, dtFim As String) As DataSet
        Dim sSql As String
        Dim aceite As String = ""

        Try

            sSql = ""

            sSql &= " create table #T " & vbNewLine
            sSql &= "(telefone varchar(10) null " & vbNewLine
            sSql &= ", ddd varchar(2) null " & vbNewLine
            sSql &= ", nome_cliente varchar(200) null " & vbNewLine
            sSql &= ", operador varchar(30) null " & vbNewLine
            sSql &= ", dt_inclusao datetime null)  " & vbNewLine

            sSql &= " declare @dataInicial varchar(14), @dataFinal varchar(14), @dtInicial date, @dtFinal date" & vbNewLine
            sSql &= "set @dataInicial = '" & CDate(dtInicio).ToString("dd/MM/yyyy") & "' " & vbNewLine
            sSql &= "set @dataFinal = '" & CDate(dtFim).ToString("dd/MM/yyyy") & "' " & vbNewLine


            If telefones.Count > 0 Then

                For Each linha As String In telefones
                    sSql += linha & vbNewLine
                Next

            End If

            sSql &= "update b " & vbNewLine
            sSql &= "set b.nome_cliente = a.nome_cliente, b.operador = a.operador, b.dt_inclusao  = a.dt_inclusao  " & vbNewLine
            sSql &= "FROM #T B " & vbNewLine
            sSql &= "INNER Join CONTROLE_DB..REQUISICAO_OI_TB  A " & vbNewLine
            sSql &= "On A.TELEFONE = B.TELEFONE " & vbNewLine
            sSql &= "And A.DDD = B.DDD " & vbNewLine
            sSql &= "where Convert(Date, a.dt_inclusao) between @dataInicial AND @dataFinal and a.aceite IN ('S','C')  " & vbNewLine

            sSql &= "Select  ddd, telefone, nome_cliente, operador, dt_inclusao " & vbNewLine
            sSql &= "FROM #T  " & vbNewLine
            sSql &= "ORDER BY dt_inclusao " & vbNewLine




            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    Public Function RetornaDadosRelatorio(ByVal dataInicial As Date, ByVal dataFinal As Date, ByVal FiltraData As String) As DataSet
        Dim sSql As String
        Dim aceite As String = ""

        Try

            sSql = ""
            sSql &= "Exec controle_db.DBO.Relatorio_previsto_operador_SPS" & vbNewLine
            sSql &= "@dataInicial = '" & Format(dataInicial, "yyyyMMdd") & "', " & vbNewLine
            sSql &= "@dataFinal = '" & Format(dataFinal, "yyyyMMdd") & "', " & vbNewLine
            sSql &= "@FiltraData = '" & FiltraData & "'"

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function RetornaDadosRelatorioOperador(ByVal dataInicial As Date, ByVal dataFinal As Date, ByVal operador As String, ByVal FiltraData As String) As DataSet
        Dim sSql As String
        Dim aceite As String = ""

        Try

            sSql = ""
            sSql &= "exec controle_db.DBO.Relatorio_previsto_operador_Detalhado_SPS" & vbNewLine
            sSql &= "@dataInicial = '" & Format(dataInicial, "yyyyMMdd") & "', " & vbNewLine
            sSql &= "@dataFinal = '" & Format(dataFinal, "yyyyMMdd") & "', " & vbNewLine
            sSql &= "@Operador = '" & operador & "', " & vbNewLine
            sSql &= "@FiltraData = '" & FiltraData & "'"

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function RetornaRequisicoesRepetidas(ByVal dataInicial As Date, ByVal dataFinal As Date, ByVal FiltraData As String) As DataSet
        Dim sSql As String
        Dim aceite As String = ""
        Dim ds As DataSet

        Try

            sSql = ""
            sSql &= "select cod_ean,ddd,telefone,nome_cliente,valor,operador,aceite,categoria,dt_inclusao from controle_db..requisicao_oi_tb "
            sSql &= " where usuario = 'ManualOiRepetido'" & vbNewLine
            If FiltraData = "S" Then
                sSql &= "and dt_inclusao between = '" & Format(dataInicial, "yyyyMMdd") & "' and '" & Format(dataFinal, "yyyyMMdd") & "'"
            End If


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                Return ds
            Else
                Return Nothing
            End If

        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    Public Function RetornaResumoRemessa() As DataSet
        Dim sSql As String
        Dim aceite As String = ""

        Try

            sSql = ""
            sSql &= "exec controle_db.DBO.Previsao_remessa_sps" & vbNewLine

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function RetornaPlanilhaImprimir(ByVal operador As String, ByVal categoria As String, ByVal sistema As String, ByVal Simplicado As Boolean) As DataSet
        Dim sSql As String = ""

        Try

            If sistema = "manual" Then

                If Simplicado = True Then

                    sSql = ""
                    sSql &= " select a.cod_ean_cliente, isnull(b.ddd , substring(c.telefone1,2,2)) as DDD , isnull(b.telefone1, c.telefone1) as Telefone, a.nome_contribuinte," & vbNewLine
                    sSql &= "  '' as titular, a.valor, '' as Aceite, '' as 'Valor Total', b.cod_empresa" & vbNewLine
                    sSql &= " from controle_db..requisicao_lista_tb a" & vbNewLine
                    sSql &= " left join controle_db..cliente_tb b" & vbNewLine
                    sSql &= " on a.cod_ean_cliente = b.cod_ean_cliente" & vbNewLine
                    sSql &= " left join controle_db..lista_retida_tmp_tb c" & vbNewLine
                    sSql &= " on a.cod_ean_cliente = c.cod_ean_cliente" & vbNewLine
                    sSql &= " inner join controle_db.. usuario_tb d" & vbNewLine
                    sSql &= " on a.cod_opera = d.cod_funcionario " & vbNewLine
                    sSql &= " where d.usuario = '" + operador + "'" & vbNewLine
                    sSql &= " and confirmado = 1" & vbNewLine
                    If categoria > 0 Then
                        sSql &= " and a.cod_categoria = " + categoria & vbNewLine
                    End If
                    sSql &= " order by cod_empresa desc" & vbNewLine

                Else

                    sSql = ""
                    sSql &= " select b.cod_empresa, a.cod_ean_cliente, isnull(b.ddd , substring(c.telefone1,2,2)) as DDD , isnull(b.telefone1, c.telefone1) as Telefone, a.nome_contribuinte," & vbNewLine
                    sSql &= "  '' as titular, a.valor, '' as Aceite, '' as 'Valor Total'" & vbNewLine
                    sSql &= " from controle_db..requisicao_lista_tb a" & vbNewLine
                    sSql &= " left join controle_db..cliente_tb b" & vbNewLine
                    sSql &= " on a.cod_ean_cliente = b.cod_ean_cliente" & vbNewLine
                    sSql &= " left join controle_db..lista_retida_tmp_tb c" & vbNewLine
                    sSql &= " on a.cod_ean_cliente = c.cod_ean_cliente" & vbNewLine
                    sSql &= " inner join controle_db.. usuario_tb d" & vbNewLine
                    sSql &= " on a.cod_opera = d.cod_funcionario " & vbNewLine
                    sSql &= " where d.usuario = '" + operador + "'" & vbNewLine
                    sSql &= " and confirmado = 1" & vbNewLine
                    If categoria > 0 Then
                        sSql &= " and a.cod_categoria = " + categoria & vbNewLine
                    End If
                    sSql &= " order by cod_empresa desc" & vbNewLine

                End If



            End If

            If sistema = "Oi" And (categoria <> 3 Or categoria <> 4) Then
                sSql = ""
                sSql &= " select cod_ean, ddd, telefone, nome_cliente, " & vbNewLine
                sSql &= "  '' as titular, valor, '' as Aceite, '' as 'Valor Total'" & vbNewLine
                sSql &= " FROM CONTROLE_DB..REQUISICAO_TB" & vbNewLine
                sSql &= " WHERE ATUAL = 'S'" & vbNewLine
                sSql &= " AND ACEITE IS NULL" & vbNewLine
                sSql &= " AND OPERADOR = '" + operador + "'" & vbNewLine
                If categoria > 0 Then
                    sSql &= " and categoria = " + categoria & vbNewLine
                End If
            End If

            If sistema = "Oi" And (categoria = 3 Or categoria = 4) Then
                sSql = ""
                sSql &= " select cod_ean, ddd, telefone, nome_cliente," & vbNewLine
                sSql &= "  '' as titular, valor, '' as Aceite, '' as 'Valor Total'" & vbNewLine
                sSql &= "  into #mensal" & vbNewLine
                sSql &= " FROM CONTROLE_DB..REQUISICAO_TB b" & vbNewLine
                sSql &= " WHERE b.ATUAL = 'S'" & vbNewLine
                sSql &= " AND b.ACEITE IS NULL" & vbNewLine
                sSql &= " AND b.OPERADOR = 'franciele'" & vbNewLine
                sSql &= " AND B.CATEGORIA IN (3,4)" & vbNewLine

                sSql &= " alter table  #mensal add ultima_ligacao varchar(100)" & vbNewLine

                sSql &= " update a" & vbNewLine
                sSql &= " set ultima_ligacao= (select top 1 'Data = ' + convert(varchar(30),dt_inclusao,3) + ' ultima situacao = ' + aceite " & vbNewLine
                sSql &= " 					  from controle_db..requisicao_oi_tb b " & vbNewLine
                sSql &= " 					 where a.telefone = b.telefone" & vbNewLine
                sSql &= " 					 and a.ddd = b.ddd" & vbNewLine
                sSql &= " 					 order by dt_inclusao desc)" & vbNewLine
                sSql &= " from #mensal a " & vbNewLine

                sSql &= " select cod_ean, ddd, telefone, nome_cliente," & vbNewLine
                sSql &= "  '' as titular, valor, '' as Aceite, '' as 'Valor Total', ultima_ligacao" & vbNewLine
                sSql &= "  from #mensal" & vbNewLine

            End If

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function SalvaImportacaoListaTelefonica(ByVal linha As String()) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= " SET IDENTITY_INSERT dbo.Lista_Telefonica_tb ON; " & vbNewLine
            sSql &= " INSERT INTO dbo.Lista_Telefonica_tb(lista_telefonica_id, cod_ean, DDD, telefone, valor, nome_cliente, categoria, mesSituacao, Situacao, dt_inclusao, dt_alteracao, usuario, cod_cidade) " & vbNewLine
            sSql &= " values (" + linha(0) + "," + linha(1) + "," + linha(2) + "," + linha(3) + "," + linha(4) + "," + linha(5) + "," + linha(6) + "," + linha(7) + "," + linha(8) + "," + linha(9) + "," + linha(10) + ")" & vbNewLine
            sSql &= " SET IDENTITY_INSERT dbo.Lista_Telefonica_tb OFF; "

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            Return False
        End Try


    End Function

    Public Function SalvaImportacaoRequisicaoOi(ByVal linha As String()) As Boolean
        Try
            Dim sSql As String
            Dim len As Integer = 0
            Dim requisicao_id As Integer = 0
            Dim isGravado As Integer = 0
            Dim ds As DataSet = New DataSet
            Dim dt_Inclusao As DateTime

            len = linha(0).Length - 1

            requisicao_id = linha(0).Substring(1, len)

            dt_Inclusao = Convert.ToDateTime(linha(11))
            'dt_Inclusao = Format(dt_Inclusao, "yyyyMMdd")

            sSql = ""
            sSql &= " select count(*) from controle_db.dbo.requisicao_oi_tb " & vbNewLine
            sSql &= " where ddd = '" + linha(2) + "' and telefone = '" + linha(3) + "' and convert(date, dt_inclusao) = '" + Format(dt_Inclusao, "yyyyMMdd") + "'" & vbNewLine


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                isGravado = ds.Tables(0).Rows.Item(0).Item(0)

            Else
                isGravado = 0
            End If

            If isGravado = 0 Then

                sSql = ""
                sSql &= " insert into controle_db.dbo.requisicao_oi_tb " & vbNewLine
                sSql &= "( cod_ean, DDD, telefone, valor, operador_id, operador, aceite, categoria, mesRef, mesRefSeguinte, dt_inclusao, dt_alteracao, usuario, nome_cliente, cpf, autorizante, observacao, nao_conforme, hist_aceite, exportado, valor_total) " & vbNewLine
                sSql &= " values " & vbNewLine
                If linha.Length = 18 Then
                    sSql &= "('" + linha(1) + "','" + linha(2) + "','" + linha(3) + "','" + linha(4) + "','" + linha(5) + "','" + linha(6) + "','" + linha(7) + "','" + linha(8) + "','" + linha(9) + "','" + linha(10) + "','" + linha(11) + "','" + linha(12) + "','" + linha(13) + "','" + linha(14) + "','" + linha(15) + "','" + linha(16) + "','" + linha(17) + "','','','S','')" & vbNewLine
                Else

                    sSql &= "('" + linha(1) + "','" + linha(2) + "','" + linha(3) + "','" + linha(4) + "','" + linha(5) + "','" + linha(6) + "','" + linha(7) + "','" + linha(8) + "','" + linha(9) + "','" + linha(10) + "','" + linha(11) + "','" + linha(12) + "','" + linha(13) + "','" + linha(14) + "','" + linha(15) + "','" + linha(16) + "','" + linha(17) + "','" + linha(18) + "','" + linha(19) + "','S','" + linha(21) + "')" & vbNewLine
                End If

                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

                Return True

            End If

            Return False

        Catch ex As Exception
            MsgBox(ex)
            Return False
        End Try

    End Function

    Public Function SalvaImportacaoRequisicao(ByVal linha As String()) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= " SET IDENTITY_INSERT dbo.requisicao_tb ON; " & vbNewLine
            sSql &= " INSERT INTO dbo.requisicao_tb(requisicao_id, numero_requisicao, cod_ean, DDD, telefone, valor, operador_id, operador, aceite, categoria, mesRef, dt_inclusao, dt_alteracao, usuario, nome_cliente, Atual, Vinculado_Operador, confirmado, observacao, cod_empresa)" & vbNewLine
            sSql &= " values " & vbNewLine
            sSql &= "(" + linha(0) + "," + linha(1) + "," + linha(2) + "," + linha(3) + "," + linha(4) + "," + linha(5) + "," + linha(6) + "," + linha(7) + "," + linha(8) + "," + linha(9) + "," + linha(10) + "," + linha(11) + "," + linha(12) + "," + linha(13) + "," + linha(14) + "," + linha(15) + "," + linha(16) + "," + linha(17) + "," + linha(18) + "," + linha(19) + "," + linha(20) + "," + linha(21) + "," + linha(22) + ")" & vbNewLine
            sSql &= " SET IDENTITY_INSERT dbo.requisicao_tb OFF; "


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function SalvaImportacaoTelefoneExclusao(ByVal linha As String()) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= " SET IDENTITY_INSERT dbo.telefone_exclusao ON; " & vbNewLine
            sSql &= " INSERT INTO dbo.telefone_exclusao(ID, DDD, Telefone, dt_inclusao, motivo, solicitante, observacao)" & vbNewLine
            sSql &= " values " & vbNewLine
            sSql &= "(" + linha(0) + "," + linha(1) + "," + linha(2) + "," + linha(3) + "," + linha(4) + "," + linha(5) + "," + linha(6) + "," + linha(7) + ")" & vbNewLine
            sSql &= " SET IDENTITY_INSERT dbo.telefone_exclusao  OFF; "


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function


    Public Function CarregaOperadorSemana(ByVal dt_inicial As String, ByVal dt_final As String) As DataSet

        Try
            Dim sSql As String = ""

            sSql &= " declare @dataInicial varchar(14), @dataFinal varchar(14), @dtInicial date, @dtFinal date" & vbNewLine
            sSql &= "set @dataInicial = '" & CDate(dt_inicial).ToString("dd/MM/yyyy") & "' " & vbNewLine
            sSql &= "set @dataFinal = '" & CDate(dt_final).ToString("dd/MM/yyyy") & "' " & vbNewLine

            sSql &= "Select distinct(operador) from controle_db..requisicao_oi_tb " & vbNewLine
            sSql &= " where CONVERT(Date, dt_inclusao) between  @dataInicial and  @dataFinal " & vbNewLine
            sSql &= " order by operador "


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function CarregaOperador() As DataSet

        Try
            Dim sSql As String = ""

            sSql &= "Select distinct(usuario) from controle_db..usuario_tb " & vbNewLine
            sSql &= " order by usuario "


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function


#End Region

#Region "CEMIG"

    Public Function RetornaRequisicaoPorCategoriaCemig() As DataSet
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= " select b.nome_categoria as categoria" & vbNewLine
            sSql &= " ,COUNT(*) as quantidade" & vbNewLine
            sSql &= " ,'R$ ' + convert(varchar(10),SUM(convert(money,valor))) as valor" & vbNewLine
            sSql &= " from integracao_cemig_db..requisicao_lista_tb a" & vbNewLine
            sSql &= " inner join controle_db..categoria_tb b" & vbNewLine
            sSql &= " on a.cod_categoria = b.cod_categoria" & vbNewLine
            sSql &= " group by b.nome_categoria " & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function Retorna_HistoricoSemRecusasCemig(ByVal telefone As String, ByVal DDD As String, ByVal cod_ean As String) As DataSet
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "SELECT ddd as StDDDMeioAcessoCobranca" & vbNewLine
            sSql &= ", telefone StNumMeioAcessoCobranca" & vbNewLine
            sSql &= ", valor as DVlrServico" & vbNewLine
            sSql &= ", categoria as categoria" & vbNewLine
            sSql &= ", mesRef  as DdDataServico" & vbNewLine
            sSql &= ", dt_inclusao as DdDataSituacao" & vbNewLine
            sSql &= ", cod_ean as  StDescReferencia" & vbNewLine
            sSql &= ", case when aceite = 'S' then 'Aceite'" & vbNewLine
            sSql &= "when aceite = 'N' then 'Recusa'" & vbNewLine
            sSql &= "else 'Cancelado' end as UsSitFaturamento" & vbNewLine
            sSql &= ", observacao as UiMotivoSituacao " & vbNewLine
            sSql &= ", nome_cliente as nome_cliente " & vbNewLine
            sSql &= "from integracao_cemig_db.dbo.requisicao_cemig_tb " & vbNewLine
            sSql &= "where aceite = 'S' " & vbNewLine
            sSql &= "and telefone = '" & telefone & "'" & vbNewLine
            If DDD <> "" Then
                sSql &= " and DDD = '" & DDD & "'" & vbNewLine
            End If
            If cod_ean <> "" Then
                sSql &= " and cod_ean = '" & cod_ean & "'" & vbNewLine
            End If
            sSql &= " union " & vbNewLine
            sSql &= "SELECT ddd as StDDDMeioAcessoCobranca" & vbNewLine
            sSql &= ", telefone StNumMeioAcessoCobranca" & vbNewLine
            sSql &= ", valor as DVlrServico" & vbNewLine
            sSql &= ", categoria as categoria" & vbNewLine
            sSql &= ", mesRef  as DdDataServico" & vbNewLine
            sSql &= ", dt_inclusao as DdDataSituacao" & vbNewLine
            sSql &= ", cod_ean as  StDescReferencia" & vbNewLine
            sSql &= ", case when aceite = 'S' then 'Aceite'" & vbNewLine
            sSql &= "when aceite = 'N' then 'Recusa'" & vbNewLine
            sSql &= "else 'Cancelado' end as UsSitFaturamento" & vbNewLine
            sSql &= ", observacao as UiMotivoSituacao " & vbNewLine
            sSql &= ", nome_cliente as nome_cliente " & vbNewLine
            sSql &= "from integracao_cemig_db.dbo.requisicao_cemig_tb  " & vbNewLine
            sSql &= "where aceite = 'S' " & vbNewLine
            sSql &= "and telefone = '" & telefone & "'" & vbNewLine
            If DDD <> "" Then
                sSql &= " and DDD = '" & DDD & "'" & vbNewLine
            End If
            sSql &= "order by dt_inclusao desc" & vbNewLine


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return ds


        Catch ex As Exception
            Return Nothing
        End Try


    End Function

    Public Function Atualiza_Lista_telefonicaCemig(ByVal lista As CListaTelefonica) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "Exec integracao_cemig_db..Atualiza_Lista_Telelfonica_SPU" & vbNewLine
            sSql &= " @cod_ean = '" & lista.Cod_cliente & "', "
            sSql &= " @nome = '" & lista.Nome_cliente & "', "
            sSql &= " @DDD = '" & lista.DDD & "', "
            sSql &= " @Telefone = '" & lista.Telefone & "', "
            sSql &= " @valor = " & Replace(lista.Valor, ",", ".") & ", "
            sSql &= " @categoria = " & lista.Categoria & ", "
            sSql &= " @cod_cidade = " & lista.Cod_cidade & ", "
            sSql &= " @Lista_telefonica_id  = " & lista.Lista_telefonica_id & " "



            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function


    Public Function CarregarManualValoresAtuaisMesCemig(ByVal operador As Integer) As DataSet
        Try
            Dim sSql As String
            Dim aceite As String = ""
            sSql = ""
            sSql &= "DECLARE @DATA DATETIME, @primeirodia smalldatetime, @ultimodiames DATETIME, @dataHabilitada datetime " & vbNewLine
            sSql &= "SET @DATA = getdate() " & vbNewLine
            sSql &= "SELECT @primeirodia = CONVERT(VARCHAR, @DATA - DAY(@DATA) + 1, 103) " & vbNewLine
            sSql &= "SELECT @ultimodiames = DATEADD(d, -DAY(GETDATE()),DATEADD(m,1,GETDATE())) " & vbNewLine
            sSql &= "set @ultimodiames = cast(convert(char(8),@ultimodiames,112) as smalldatetime) " & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "create table #resumo" & vbNewLine
            sSql &= "(valor_aceite  decimal null" & vbNewLine
            sSql &= ",total_fichas_aceitas int null" & vbNewLine
            sSql &= ",total_fichas int null)" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "insert into #resumo (valor_aceite)" & vbNewLine
            sSql &= "select isnull(sum(valor),0) as valor_aceite" & vbNewLine
            sSql &= "from integracao_cemig_db..requisicao_cemig_tb" & vbNewLine
            sSql &= "where operador_id = " & operador & vbNewLine
            sSql &= "and aceite = 'S'" & vbNewLine
            sSql &= "and  convert(date,dt_alteracao) between @primeirodia and @ultimodiames" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "update #resumo " & vbNewLine
            sSql &= "set total_fichas_aceitas = (select COUNT(*) as total_fichas_aceitas" & vbNewLine
            sSql &= "from integracao_cemig_db..requisicao_cemig_tb" & vbNewLine
            sSql &= "where operador_id = " & operador & vbNewLine
            sSql &= "and aceite = 'S'  " & vbNewLine
            sSql &= "and convert(date,dt_alteracao) between @primeirodia and @ultimodiames )" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "update #resumo " & vbNewLine
            sSql &= "set total_fichas = (select COUNT(*) as total_fichas" & vbNewLine
            sSql &= "from integracao_cemig_db..requisicao_cemig_tb" & vbNewLine
            sSql &= "where aceite is null and operador_id = " & operador & ")" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "select valor_aceite, total_fichas_aceitas, total_fichas  from #resumo" & vbNewLine


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function CarregarManualValoresAtuaisDiaCemig(ByVal operador As Integer) As DataSet
        Dim sSql As String
        Dim aceite As String = ""

        Try

            sSql = ""
            sSql &= "DECLARE @DATA DATETIME, @primeirodia smalldatetime, @ultimodiames DATETIME, @dataHabilitada datetime " & vbNewLine
            sSql &= "SET @DATA = getdate() " & vbNewLine
            sSql &= "SELECT @primeirodia = CONVERT(VARCHAR, @DATA - DAY(@DATA) + 1, 103) " & vbNewLine
            sSql &= "SELECT @ultimodiames = DATEADD(d, -DAY(GETDATE()),DATEADD(m,1,GETDATE())) " & vbNewLine
            sSql &= "set @ultimodiames = cast(convert(char(8),@ultimodiames,112) as smalldatetime) " & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "create table #resumo" & vbNewLine
            sSql &= "(valor_aceite  decimal null" & vbNewLine
            sSql &= ",total_fichas_aceitas int null" & vbNewLine
            sSql &= ",total_fichas int null)" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "insert into #resumo (valor_aceite)" & vbNewLine
            sSql &= "select isnull(sum(valor),0) as valor_aceite" & vbNewLine
            sSql &= "from integracao_cemig_db..requisicao_cemig_tb" & vbNewLine
            sSql &= "where operador_id = " & operador & vbNewLine
            sSql &= "and aceite = 'S' " & vbNewLine
            sSql &= "and convert(date, dt_alteracao) = convert (date,getdate()) " & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "update #resumo " & vbNewLine
            sSql &= "set total_fichas_aceitas = (select COUNT(*) as total_fichas_aceitas" & vbNewLine
            sSql &= "from integracao_cemig_db..requisicao_cemig_tb" & vbNewLine
            sSql &= "where operador_id = " & operador & vbNewLine
            sSql &= "and aceite = 'S'  " & vbNewLine
            sSql &= "and convert(date, dt_alteracao) = convert (date,getdate()) )" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "update #resumo " & vbNewLine
            sSql &= "set total_fichas = (select COUNT(*) as total_fichas" & vbNewLine
            sSql &= "from integracao_cemig_db..requisicao_cemig_tb" & vbNewLine
            sSql &= "where aceite is null and operador_id = " & operador & ")" & vbNewLine
            sSql &= "" & vbNewLine

            sSql &= "select valor_aceite, total_fichas_aceitas, total_fichas  from #resumo" & vbNewLine


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


        Catch ex As Exception
            Return Nothing
        End Try

    End Function

#End Region

End Class

