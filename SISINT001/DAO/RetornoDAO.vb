﻿Imports System.Globalization

Public Class RetornoDAO

#Region "Variáveis"

    Private Protocolo As String
    Private Remessa As Integer
    Private AnoMesRemessa As String
    Private DdDataGeracao As String
    Private UsTipoRegistroH As String
    Private EmpresaTerceiro As String
    Private EmpresaTelemar As String
    Private UsTipoRegistroD As String
    Private UiCodServicoArbor As String
    Private UiCodLocalidadeCobranca As String
    Private StDDDMeioAcessoCobranca As String
    Private StNumMeioAcessoCobranca As String
    Private DVlrServico As String
    Private DdDataServico As String
    Private UsNumParcela As String
    Private UsQteParcela As String
    Private UiDataInicioCobranca As String
    Private UcSinal As String
    Private UiAnoMesConta As String
    Private StDescReferencia As String
    Private DNumNotaFiscal As String
    Private DdDataVencimento As String
    Private DdDataSituacao As String
    Private DVlrFaturado As String
    Private StCodigoMovimentacao As String
    Private UsSitFaturamento As String
    Private UiMotivoSituacao As String
    Private UiAnoMesContaMulta As String
    Private StNumAssinanteMovel As String
    Private SistemaFaturamento As String
    Private Ciclo As String
    Private UsTipoRegistroT As String
    Private QuantidadeDeRegistro As Long
    Private ValorArquivo As String


    Private ConnectionHelper As Util.ConnectionHelper

#End Region

#Region "Construtores"

    Sub New(Optional ByVal p_Protocolo As String = "",
         Optional ByVal p_Remessa As Integer = 0,
   Optional ByVal p_AnoMesRemessa As String = "",
   Optional ByVal p_DdDataGeracao As String = "",
   Optional ByVal p_UsTipoRegistroH As String = "",
   Optional ByVal p_EmpresaTerceiro As String = "",
   Optional ByVal p_EmpresaTelemar As String = "",
   Optional ByVal p_UsTipoRegistroD As String = "",
   Optional ByVal p_UiCodServicoArbor As String = "",
   Optional ByVal p_UiCodLocalidadeCobranca As String = "",
   Optional ByVal p_StDDDMeioAcessoCobranca As String = "",
   Optional ByVal p_StNumMeioAcessoCobranca As String = "",
   Optional ByVal p_DVlrServico As String = "",
   Optional ByVal p_DdDataServico As String = "",
   Optional ByVal p_UsNumParcela As String = "",
   Optional ByVal p_UsQteParcela As String = "",
   Optional ByVal p_UiDataInicioCobranca As String = "",
   Optional ByVal p_UcSinal As String = "",
   Optional ByVal p_UiAnoMesConta As String = "",
   Optional ByVal p_StDescReferencia As String = "",
   Optional ByVal p_DNumNotaFiscal As String = "",
   Optional ByVal p_DdDataVencimento As String = "",
   Optional ByVal p_DdDataSituacao As String = "",
   Optional ByVal p_DVlrFaturado As String = "",
   Optional ByVal p_StCodigoMovimentacao As String = "",
   Optional ByVal p_UsSitFaturamento As String = "",
   Optional ByVal p_UiMotivoSituacao As String = "",
   Optional ByVal p_UiAnoMesContaMulta As String = "",
   Optional ByVal p_StNumAssinanteMovel As String = "",
   Optional ByVal p_SistemaFaturamento As String = "",
   Optional ByVal p_Ciclo As String = "",
   Optional ByVal p_UsTipoRegistroT As String = "",
   Optional ByVal p_QuantidadeDeRegistro As Long = 0,
   Optional ByVal p_ValorArquivo As Decimal = 0)

        Protocolo = p_Protocolo
        Remessa = p_Remessa
        AnoMesRemessa = p_AnoMesRemessa
        DdDataGeracao = p_DdDataGeracao
        UsTipoRegistroH = p_UsTipoRegistroH
        EmpresaTerceiro = p_EmpresaTerceiro
        EmpresaTelemar = p_EmpresaTelemar
        UsTipoRegistroD = p_UsTipoRegistroD
        UiCodServicoArbor = p_UiCodServicoArbor
        UiCodLocalidadeCobranca = p_UiCodLocalidadeCobranca
        StDDDMeioAcessoCobranca = p_StDDDMeioAcessoCobranca
        StNumMeioAcessoCobranca = p_StNumMeioAcessoCobranca
        DVlrServico = p_DVlrServico
        DdDataServico = p_DdDataServico
        UsNumParcela = p_UsNumParcela
        UsQteParcela = p_UsQteParcela
        UiDataInicioCobranca = p_UiDataInicioCobranca
        UcSinal = p_UcSinal
        UiAnoMesConta = p_UiAnoMesConta
        StDescReferencia = p_StDescReferencia
        DNumNotaFiscal = p_DNumNotaFiscal
        DdDataVencimento = p_DdDataVencimento
        DdDataSituacao = p_DdDataSituacao
        DVlrFaturado = p_DVlrFaturado
        StCodigoMovimentacao = p_StCodigoMovimentacao
        UsSitFaturamento = p_UsSitFaturamento
        UiMotivoSituacao = p_UiMotivoSituacao
        UiAnoMesContaMulta = p_UiAnoMesContaMulta
        StNumAssinanteMovel = p_StNumAssinanteMovel
        SistemaFaturamento = p_SistemaFaturamento
        Ciclo = p_Ciclo
        UsTipoRegistroT = p_UsTipoRegistroT
        QuantidadeDeRegistro = p_QuantidadeDeRegistro
        ValorArquivo = p_ValorArquivo


        ConnectionHelper = New Util.ConnectionHelper(System.Reflection.Assembly.GetExecutingAssembly)

    End Sub


    Sub New(ByRef p_connectionHelper As Util.ConnectionHelper,
            Optional ByVal p_Protocolo As String = "",
         Optional ByVal p_Remessa As Integer = 0,
   Optional ByVal p_AnoMesRemessa As String = "",
   Optional ByVal p_DdDataGeracao As String = "",
   Optional ByVal p_UsTipoRegistroH As String = "",
   Optional ByVal p_EmpresaTerceiro As String = "",
   Optional ByVal p_EmpresaTelemar As String = "",
   Optional ByVal p_UsTipoRegistroD As String = "",
   Optional ByVal p_UiCodServicoArbor As String = "",
   Optional ByVal p_UiCodLocalidadeCobranca As String = "",
   Optional ByVal p_StDDDMeioAcessoCobranca As String = "",
   Optional ByVal p_StNumMeioAcessoCobranca As String = "",
   Optional ByVal p_DVlrServico As String = "",
   Optional ByVal p_DdDataServico As String = "",
   Optional ByVal p_UsNumParcela As String = "",
   Optional ByVal p_UsQteParcela As String = "",
   Optional ByVal p_UiDataInicioCobranca As String = "",
   Optional ByVal p_UcSinal As String = "",
   Optional ByVal p_UiAnoMesConta As String = "",
   Optional ByVal p_StDescReferencia As String = "",
   Optional ByVal p_DNumNotaFiscal As String = "",
   Optional ByVal p_DdDataVencimento As String = "",
   Optional ByVal p_DdDataSituacao As String = "",
   Optional ByVal p_DVlrFaturado As String = "",
   Optional ByVal p_StCodigoMovimentacao As String = "",
   Optional ByVal p_UsSitFaturamento As String = "",
   Optional ByVal p_UiMotivoSituacao As String = "",
   Optional ByVal p_UiAnoMesContaMulta As String = "",
   Optional ByVal p_StNumAssinanteMovel As String = "",
   Optional ByVal p_SistemaFaturamento As String = "",
   Optional ByVal p_Ciclo As String = "",
   Optional ByVal p_UsTipoRegistroT As String = "",
   Optional ByVal p_QuantidadeDeRegistro As Long = 0,
   Optional ByVal p_ValorArquivo As String = "")

        Protocolo = p_Protocolo
        Remessa = p_Remessa
        AnoMesRemessa = p_AnoMesRemessa
        DdDataGeracao = p_DdDataGeracao
        UsTipoRegistroH = p_UsTipoRegistroH
        EmpresaTerceiro = p_EmpresaTerceiro
        EmpresaTelemar = p_EmpresaTelemar
        UsTipoRegistroD = p_UsTipoRegistroD
        UiCodServicoArbor = p_UiCodServicoArbor
        UiCodLocalidadeCobranca = p_UiCodLocalidadeCobranca
        StDDDMeioAcessoCobranca = p_StDDDMeioAcessoCobranca
        StNumMeioAcessoCobranca = p_StNumMeioAcessoCobranca
        DVlrServico = p_DVlrServico
        DdDataServico = p_DdDataServico
        UsNumParcela = p_UsNumParcela
        UsQteParcela = p_UsQteParcela
        UiDataInicioCobranca = p_UiDataInicioCobranca
        UcSinal = p_UcSinal
        UiAnoMesConta = p_UiAnoMesConta
        StDescReferencia = p_StDescReferencia
        DNumNotaFiscal = p_DNumNotaFiscal
        DdDataVencimento = p_DdDataVencimento
        DdDataSituacao = p_DdDataSituacao
        DVlrFaturado = p_DVlrFaturado
        StCodigoMovimentacao = p_StCodigoMovimentacao
        UsSitFaturamento = p_UsSitFaturamento
        UiMotivoSituacao = p_UiMotivoSituacao
        UiAnoMesContaMulta = p_UiAnoMesContaMulta
        StNumAssinanteMovel = p_StNumAssinanteMovel
        SistemaFaturamento = p_SistemaFaturamento
        Ciclo = p_Ciclo
        UsTipoRegistroT = p_UsTipoRegistroT
        QuantidadeDeRegistro = p_QuantidadeDeRegistro
        ValorArquivo = p_ValorArquivo
        ConnectionHelper = p_connectionHelper
    End Sub

#End Region

#Region "metodos"

    Public Function Consultar(ByVal filtro As Integer) As DataSet

        Try
            Dim sSql As String = "select * " _
                                 + "from controle_db..cliente_tb with (nolock)" _
                                 + "where cod_cliente = " & filtro



            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function



    Public Function RetornaCodEAN(ByVal telefone As String) As String
        Dim ds As DataSet
        Dim cod_ean As String = ""
        Try

            Dim sSql As String = "SELECT cod_ean_cliente " _
                             + "FROM controle_db..cliente_tb with (nolock)" _
                             + "WHERE " _
                             + "telefone1 = '" + telefone + "'"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    cod_ean = ds.Tables(0).Rows(0).Item(0)
                End If
            End If

            Return cod_ean


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaSituacao(ByVal situacaoId As Integer) As String
        Dim ds As DataSet
        Dim situacao As String = ""
        Try

            Dim sSql As String = "SELECT situacao " _
                             + "FROM controle_db..SituacaoFaturamento_tb with (nolock)" _
                             + "WHERE " _
                             + "situacao_faturamento_id = " & situacaoId


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                situacao = ds.Tables(0).Rows(0).Item(0)
            End If

            Return situacao

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function RetornaIDArquivo() As Integer
        Dim ds As DataSet
        Dim id_arq As Integer = 0
        Try

            Dim sSql As String = "SELECT max(isnull(id_arq,0))+ 1 " _
                             + "FROM interface_dados_db..Arquivo_retorno_tb with (nolock)"



            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                If IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    id_arq = 1
                Else
                    id_arq = ds.Tables(0).Rows(0).Item(0)
                End If
            End If

            Return id_arq

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaNomeCritica(ByVal MotivoSituacao As Integer) As String
        Dim ds As DataSet
        Dim motivo As String = ""
        Try

            motivo = MotivoSituacao

            Dim sSql As String = "SELECT nome " _
                             + "FROM controle_db..critica_tb with (nolock)" _
                             + "WHERE " _
                             + "codigo_externo = '" + motivo + "'"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If Not IsNothing(ds.Tables(0).Rows) = 0 Then

                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then

                    If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                        motivo = ds.Tables(0).Rows(0).Item(0)
                    End If

                Else

                    sSql = "SELECT nome " _
                        + "FROM controle_db..critica_inicial_tb with (nolock)" _
                        + "WHERE " _
                        + "codigo_externo = '" + motivo + "'"

                    ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

                    If Not IsNothing(ds.Tables(0).Rows) = 0 Then
                        If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                            motivo = ds.Tables(0).Rows(0).Item(0)
                        End If
                    End If
                End If

            End If
            Return motivo

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function Salvar(ByVal dadosRetorno As CRetorno, ByVal nomeArq As String, ByVal usuario As String, ByVal arq_id As Integer) As Boolean

        Try
            Dim sSql As String

            Dim data_arq As String = ""
            data_arq = nomeArq.Substring(10, 8)
            usuario = "AACI"

            sSql = ""
            sSql &= "exec interface_dados_db.dbo.SalvaDadosArquivo_SPI " & vbNewLine
            sSql &= "  @AnoMesRemessa = '" & dadosRetorno.AnoMesRemessa & "'" & vbNewLine
            sSql &= ", @Ciclo = '" & dadosRetorno.Ciclo & "'" & vbNewLine
            sSql &= ", @DdDataGeracao = '" & dadosRetorno.DdDataGeracao & "'" & vbNewLine
            sSql &= ", @DdDataServico = '" & dadosRetorno.DdDataServico & "'" & vbNewLine
            sSql &= ", @DdDataSituacao = '" & dadosRetorno.DdDataSituacao & "'" & vbNewLine
            sSql &= ", @DdDataVencimento = '" & dadosRetorno.DdDataVencimento & "'" & vbNewLine
            sSql &= ", @DNumNotaFiscal = '" & dadosRetorno.DNumNotaFiscal & "'" & vbNewLine
            sSql &= ", @DVlrFaturado = '" & dadosRetorno.DVlrFaturado & "'" & vbNewLine
            sSql &= ", @DVlrServico = '" & dadosRetorno.DVlrServico & "'" & vbNewLine
            sSql &= ", @EmpresaTelemar = '" & dadosRetorno.EmpresaTelemar & "'" & vbNewLine
            sSql &= ", @EmpresaTerceiro = '" & dadosRetorno.EmpresaTerceiro & "'" & vbNewLine
            sSql &= ", @Protocolo = '" & dadosRetorno.Protocolo & "'" & vbNewLine
            sSql &= ", @QuantidadeDeRegistro = '" & dadosRetorno.QuantidadeDeRegistro & "'" & vbNewLine
            sSql &= ", @Remessa = '" & dadosRetorno.Remessa & "'" & vbNewLine
            sSql &= ", @SistemaFaturamento = '" & dadosRetorno.SistemaFaturamento & "'" & vbNewLine
            sSql &= ", @StCodigoMovimentacao = '" & dadosRetorno.StCodigoMovimentacao & "'" & vbNewLine
            sSql &= ", @StDDDMeioAcessoCobranca = '" & dadosRetorno.StDDDMeioAcessoCobranca & "'" & vbNewLine
            sSql &= ", @StDescReferencia = '" & dadosRetorno.StDescReferencia & "'" & vbNewLine
            sSql &= ", @StNumAssinanteMovel = '" & dadosRetorno.StNumAssinanteMovel & "'" & vbNewLine
            sSql &= ", @StNumMeioAcessoCobranca = '" & dadosRetorno.StNumMeioAcessoCobranca & "'" & vbNewLine
            sSql &= ", @UcSinal = '" & dadosRetorno.UcSinal & "'" & vbNewLine
            sSql &= ", @UiAnoMesConta = '" & dadosRetorno.UiAnoMesConta & "'" & vbNewLine
            sSql &= ", @UiAnoMesContaMulta = '" & dadosRetorno.UiAnoMesContaMulta & "'" & vbNewLine
            sSql &= ", @UiCodLocalidadeCobranca = '" & dadosRetorno.UiCodLocalidadeCobranca & "'" & vbNewLine
            sSql &= ", @UiCodServicoArbor = '" & dadosRetorno.UiCodServicoArbor & "'" & vbNewLine
            sSql &= ", @UiDataInicioCobranca = '" & dadosRetorno.UiDataInicioCobranca & "'" & vbNewLine
            sSql &= ", @UiMotivoSituacao = '" & dadosRetorno.UiMotivoSituacao & "'" & vbNewLine
            sSql &= ", @UsNumParcela = '" & dadosRetorno.UsNumParcela & "'" & vbNewLine
            sSql &= ", @UsQteParcela = '" & dadosRetorno.UsQteParcela & "'" & vbNewLine
            sSql &= ", @UsSitFaturamento = '" & dadosRetorno.UsSitFaturamento & "'" & vbNewLine
            sSql &= ", @UsTipoRegistroD = '" & dadosRetorno.UsTipoRegistroD & "'" & vbNewLine
            sSql &= ", @UsTipoRegistroH = '" & dadosRetorno.UsTipoRegistroH & "'" & vbNewLine
            sSql &= ", @UsTipoRegistroT = '" & dadosRetorno.UsTipoRegistroT & "'" & vbNewLine
            sSql &= ", @ValorArquivo = '" & dadosRetorno.ValorArquivo & "'" & vbNewLine
            sSql &= ", @id_arq = " & arq_id & "" & vbNewLine
            sSql &= ", @nome_arquivo = '" & nomeArq & "'" & vbNewLine
            sSql &= ", @mes_Ano_Ref = '" & IIf(IsDBNull(dadosRetorno.AnoMesRemessa), "", dadosRetorno.AnoMesRemessa) & "'" & vbNewLine
            sSql &= ", @data_arquivo = '" & IIf(IsDBNull(dadosRetorno.DdDataGeracao), "", dadosRetorno.DdDataGeracao) & "'" & vbNewLine
            sSql &= ", @usuario  = '" & usuario & "'" & vbNewLine


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function SalvarMovimentacao(ByVal dadosRetorno As CRetorno, ByVal nomeArq As String, ByVal usuario As String) As Boolean

        Try
            Dim sSql As String

            Dim data_arq As String = ""
            data_arq = nomeArq.Substring(10, 8)
            usuario = "AACI"

            sSql = ""
            sSql &= "exec controle_db.dbo.Salva_Ultima_movimentacao_SPI " & vbNewLine
            sSql &= "  @AnoMesRemessa = '" & dadosRetorno.AnoMesRemessa & "'" & vbNewLine
            sSql &= ", @Ciclo = '" & dadosRetorno.Ciclo & "'" & vbNewLine
            sSql &= ", @DdDataGeracao = '" & dadosRetorno.DdDataGeracao & "'" & vbNewLine
            sSql &= ", @DdDataServico = '" & dadosRetorno.DdDataServico & "'" & vbNewLine
            sSql &= ", @DdDataSituacao = '" & dadosRetorno.DdDataSituacao & "'" & vbNewLine
            sSql &= ", @DdDataVencimento = '" & dadosRetorno.DdDataVencimento & "'" & vbNewLine
            sSql &= ", @DNumNotaFiscal = '" & dadosRetorno.DNumNotaFiscal & "'" & vbNewLine
            sSql &= ", @DVlrFaturado = '" & dadosRetorno.DVlrFaturado & "'" & vbNewLine
            sSql &= ", @DVlrServico = '" & dadosRetorno.DVlrServico & "'" & vbNewLine
            sSql &= ", @EmpresaTelemar = '" & dadosRetorno.EmpresaTelemar & "'" & vbNewLine
            sSql &= ", @EmpresaTerceiro = '" & dadosRetorno.EmpresaTerceiro & "'" & vbNewLine
            sSql &= ", @Protocolo = '" & dadosRetorno.Protocolo & "'" & vbNewLine
            sSql &= ", @QuantidadeDeRegistro = '" & dadosRetorno.QuantidadeDeRegistro & "'" & vbNewLine
            sSql &= ", @Remessa = '" & dadosRetorno.Remessa & "'" & vbNewLine
            sSql &= ", @SistemaFaturamento = '" & dadosRetorno.SistemaFaturamento & "'" & vbNewLine
            sSql &= ", @StCodigoMovimentacao = '" & dadosRetorno.StCodigoMovimentacao & "'" & vbNewLine
            sSql &= ", @StDDDMeioAcessoCobranca = '" & dadosRetorno.StDDDMeioAcessoCobranca & "'" & vbNewLine
            sSql &= ", @StDescReferencia = '" & dadosRetorno.StDescReferencia & "'" & vbNewLine
            sSql &= ", @StNumAssinanteMovel = '" & dadosRetorno.StNumAssinanteMovel & "'" & vbNewLine
            sSql &= ", @StNumMeioAcessoCobranca = '" & dadosRetorno.StNumMeioAcessoCobranca & "'" & vbNewLine
            sSql &= ", @UcSinal = '" & dadosRetorno.UcSinal & "'" & vbNewLine
            sSql &= ", @UiAnoMesConta = '" & dadosRetorno.UiAnoMesConta & "'" & vbNewLine
            sSql &= ", @UiAnoMesContaMulta = '" & dadosRetorno.UiAnoMesContaMulta & "'" & vbNewLine
            sSql &= ", @UiCodLocalidadeCobranca = '" & dadosRetorno.UiCodLocalidadeCobranca & "'" & vbNewLine
            sSql &= ", @UiCodServicoArbor = '" & dadosRetorno.UiCodServicoArbor & "'" & vbNewLine
            sSql &= ", @UiDataInicioCobranca = '" & dadosRetorno.UiDataInicioCobranca & "'" & vbNewLine
            sSql &= ", @UiMotivoSituacao = '" & dadosRetorno.UiMotivoSituacao & "'" & vbNewLine
            sSql &= ", @UsNumParcela = '" & dadosRetorno.UsNumParcela & "'" & vbNewLine
            sSql &= ", @UsQteParcela = '" & dadosRetorno.UsQteParcela & "'" & vbNewLine
            sSql &= ", @UsSitFaturamento = '" & dadosRetorno.UsSitFaturamento & "'" & vbNewLine
            sSql &= ", @mes_Ano_Ref = '" & IIf(IsDBNull(dadosRetorno.AnoMesRemessa), "", dadosRetorno.AnoMesRemessa) & "'" & vbNewLine
            sSql &= ", @data_arquivo = '" & IIf(IsDBNull(dadosRetorno.DdDataGeracao), "", dadosRetorno.DdDataGeracao) & "'" & vbNewLine
            sSql &= ", @usuario  = '" & usuario & "'" & vbNewLine

            If dadosRetorno.StNumMeioAcessoCobranca <> "" Then
                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

                Return True
            End If
            Return False
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function IsArquivoProcessado(ByVal nome_arquivo As String) As Boolean
        Dim ds As DataSet
        Try

            Dim sSql As String = "SELECT nome_arquivo " _
                             + "FROM interface_dados_db..Arquivo_retorno_tb with (nolock)" _
                             + " where nome_arquivo = '" + nome_arquivo + "'"

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then

                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    nome_arquivo = ds.Tables(0).Rows(0).Item(0)
                    Return True
                Else
                    Return False
                End If
            End If

            Return False
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function ApagaArquivoProcessado(ByVal nome_arquivo As String) As Boolean
        Dim ds As DataSet
        Dim id As Integer
        Try

            Dim sSql As String = "select id_arq " _
                            + "FROM interface_dados_db..Arquivo_retorno_tb with (nolock)" _
                            + " where nome_arquivo = '" + nome_arquivo + "'"

            ds = (ConnectionHelper.ExecuteSQL(CommandType.Text, sSql))

            If (ds.Tables(0).Rows.Count > 0) Then

                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    id = ds.Tables(0).Rows(0).Item(0)
                Else
                    Return False
                End If
            Else
                Return False
            End If

            sSql = ""
            sSql = "delete " _
                             + "FROM interface_dados_db..Arquivo_retorno_tb" _
                             + " where nome_arquivo = '" + nome_arquivo + "'"

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            sSql = ""
            sSql = "delete " _
             + "FROM interface_dados_db..Arquivo_retorno_detalhe_tb " _
             + " where id_arq = " & id

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)



            Return True


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function RetornaDataUltimaSituacao(ByVal telefone As String) As String
        Dim ds As DataSet
        Dim data As String = ""
        Try

            Dim sSql As String = " SELECT max(isnull(DdDataSituacao,0))" _
                             + "FROM interface_dados_db..Arquivo_retorno_detalhe_tb with (nolock)" _
                             + "WHERE substring(StNumMeioAcessoCobranca,3,9) = '" + telefone + "'"



            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    data = ds.Tables(0).Rows(0).Item(0)

                    Return data
                Else
                    Return Nothing
                End If
            End If

            Return False

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaUltimaSituacao(ByVal telefone As String) As String
        Dim ds As DataSet
        Dim situacao As String = ""
        Try

            Dim sSql As String = " SELECT max(isnull(UsSitFaturamento,0))" _
                             + "FROM interface_dados_db..Arquivo_retorno_detalhe_tb with (nolock)" _
                             + "WHERE substring(StNumMeioAcessoCobranca,3,9) = '" + telefone + "'"



            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    situacao = ds.Tables(0).Rows(0).Item(0)
                    Return situacao
                Else
                    Return Nothing
                End If
            End If

            Return False

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaAFaturar(ByVal mesAno As String) As String
        Dim ds As DataSet
        Dim valor As String = ""
        Try

            Dim sSql As String = " select convert(decimal,replace(substring(DVlrFaturado,4,5),',','.')) as val_afaturar  " _
                                 + " into #afaturar " _
                                + " from interface_dados_db..Arquivo_retorno_detalhe_tb ar " _
                                + "where UsSitFaturamento = 'afaturar' " _
                                + "and substring(DdDataServico,4,8) = '" + mesAno + "'" _
                                + "group by substring(StNumMeioAcessoCobranca,3,9) " _
                                + ", DVlrFaturado " _
                                + ", UsSitFaturamento " _
                                + ", DdDataSituacao " _
                                + " order by substring(StNumMeioAcessoCobranca,3,9)" _
                                + " select sum (convert(numeric(15,2),(val_afaturar))) " _
                                + " from #afaturar "



            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    valor = ds.Tables(0).Rows(0).Item(0)
                    Return valor
                Else
                    Return Nothing
                End If
            End If

            Return False

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaFaturado(ByVal mesAno As String) As String
        Dim ds As DataSet
        Dim valor As String = ""
        Try

            Dim sSql As String = " select convert(decimal,replace(substring(DVlrFaturado,4,5),',','.')) as val_faturado  " _
                               + " into #faturado " _
                              + " from interface_dados_db..Arquivo_retorno_detalhe_tb ar " _
                              + "where UsSitFaturamento = 'faturado' " _
                              + "and substring(DdDataServico,4,8) = '" + mesAno + "'" _
                              + "group by substring(StNumMeioAcessoCobranca,3,9) " _
                              + ", DVlrFaturado " _
                              + ", UsSitFaturamento " _
                              + ", DdDataSituacao " _
                              + " order by substring(StNumMeioAcessoCobranca,3,9)" _
                              + " select sum (convert(numeric(15,2),(val_faturado))) " _
                              + " from #faturado "



            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    valor = ds.Tables(0).Rows(0).Item(0)
                    Return valor
                Else
                    Return Nothing
                End If
            End If

            Return False

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaCriticado(ByVal mesAno As String) As String
        Dim ds As DataSet
        Dim valor As String = ""
        Try

            Dim sSql As String = " select convert(decimal,replace(substring(DVlrServico,4,5),',','.')) as val_criticado  " _
                             + " into #criticado " _
                            + " from interface_dados_db..Arquivo_retorno_detalhe_tb ar " _
                            + "where UsSitFaturamento = 'criticado' " _
                            + "and substring(DdDataServico,4,8) = '" + mesAno + "'" _
                            + "group by substring(StNumMeioAcessoCobranca,3,9) " _
                            + ", DVlrServico " _
                            + ", UsSitFaturamento " _
                            + ", DdDataSituacao " _
                            + " order by substring(StNumMeioAcessoCobranca,3,9)" _
                            + " select sum (convert(numeric(15,2),(val_criticado))) " _
                            + " from #criticado "


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    valor = ds.Tables(0).Rows(0).Item(0)
                    Return valor
                Else
                    Return Nothing
                End If
            End If

            Return False

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaArrecadado(ByVal mesAno As String) As String
        Dim ds As DataSet
        Dim valor As String = ""
        Try

            Dim sSql As String = " select convert(decimal,replace(substring(DVlrServico,4,5),',','.')) as val_arrecadado  " _
                           + " into #arrecadado " _
                          + " from interface_dados_db..Arquivo_retorno_detalhe_tb ar " _
                          + "where UsSitFaturamento = 'arrecadado' " _
                          + "and substring(DdDataServico,4,8) = '" + mesAno + "'" _
                          + "group by substring(StNumMeioAcessoCobranca,3,9) " _
                          + ", DVlrServico " _
                          + ", UsSitFaturamento " _
                          + ", DdDataSituacao " _
                          + " order by substring(StNumMeioAcessoCobranca,3,9)" _
                          + " select sum (convert(numeric(15,2),(val_arrecadado))) " _
                          + " from #arrecadado "


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    valor = ds.Tables(0).Rows(0).Item(0)
                    Return valor
                Else
                    Return Nothing
                End If
            End If

            Return False

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaAltConta(ByVal mesAno As String) As String
        Dim ds As DataSet
        Dim valor As String = ""
        Try

            Dim sSql As String = " select convert(decimal,replace(substring(DVlrServico,4,5),',','.')) as val_alteração_vencimento  " _
                           + " into #altVencimento " _
                          + " from interface_dados_db..Arquivo_retorno_detalhe_tb ar " _
                          + "where UsSitFaturamento = 'alt.conta' " _
                          + "and substring(DdDataServico,4,8) = '" + mesAno + "'" _
                          + "group by substring(StNumMeioAcessoCobranca,3,9) " _
                          + ", DVlrServico " _
                          + ", UsSitFaturamento " _
                          + ", DdDataSituacao " _
                          + " order by substring(StNumMeioAcessoCobranca,3,9)" _
                          + " select sum (convert(numeric(15,2),(val_alteração_vencimento))) " _
                          + " from #altVencimento "


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    valor = ds.Tables(0).Rows(0).Item(0)
                    Return valor
                Else
                    Return Nothing
                End If
            End If
            Return Nothing


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaContestado(ByVal mesAno As String) As String
        Dim ds As DataSet
        Dim valor As String = ""
        Try

            Dim sSql As String = " select convert(decimal,replace(substring(DVlrServico,4,5),',','.')) as val_contestado  " _
                           + " into #contestado " _
                          + " from interface_dados_db..Arquivo_retorno_detalhe_tb ar " _
                          + " where UsSitFaturamento = 'contestado' " _
                          + " and substring(DdDataServico,4,8) = '" + mesAno + "'" _
                          + " group by substring(StNumMeioAcessoCobranca,3,9) " _
                          + ", DVlrServico " _
                          + ", UsSitFaturamento " _
                          + ", DdDataSituacao " _
                          + " order by substring(StNumMeioAcessoCobranca,3,9)" _
                          + " select sum (convert(numeric(15,2),(val_contestado))) " _
                          + " from #contestado "


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    valor = ds.Tables(0).Rows(0).Item(0)
                    Return valor
                Else
                    Return Nothing
                End If
            End If

            Return Nothing

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function



    Public Function retorna_valores_atuais(ByVal mesAno As String) As DataSet
        Dim ds As DataSet
        Try

            Dim sSql As String = " EXEC interface_dados_db..retorna_valores_atuais_SPS @DdDataServico = '" & mesAno & "'"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If ((ds.Tables(0).Rows.Count > 0) Or (ds.Tables(1).Rows.Count > 0) Or
                (ds.Tables(2).Rows.Count > 0) Or (ds.Tables(3).Rows.Count > 0) Or
                (ds.Tables(4).Rows.Count > 0) Or (ds.Tables(5).Rows.Count > 0) Or
                (ds.Tables(6).Rows.Count > 0) Or (ds.Tables(7).Rows.Count > 0) Or
                (ds.Tables(8).Rows.Count > 0)) Then
                Return ds
            Else

                Return Nothing

            End If



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function retorna_Arrecadado(ByVal mesAno As DateTime) As DataSet
        Dim ds As DataSet
        Try

            mesAno = mesAno.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)


            Dim sSql As String = " exec interface_dados_db..Retorno_valor_mensal_SPS @mesRef = '" & mesAno & "'"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                Return ds
            Else

                Return Nothing

            End If



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function DataRemessa() As String
        Dim ds As DataSet
        Dim valor As String = ""
        Try

            Dim sSql As String = " select MAX(data_arquivo) from interface_dados_db..Arquivo_retorno_tb"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    valor = ds.Tables(0).Rows(0).Item(0)
                    Return valor
                Else
                    Return Nothing
                End If
            End If

            Return Nothing

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function CarregarGridContribuintesOiPorSituacao(ByVal situacao As String, ByVal mesAno As String) As DataSet
        Dim sSql As String

        Try


            sSql = ""
            sSql &= "exec controle_db.dbo.CarregaContribuintesOiPorSituacao_SPS " & vbNewLine
            If situacao = Nothing Then
                sSql &= "@situacao = null" & vbNewLine
            Else
                sSql &= "@situacao = " & situacao
            End If
            sSql &= ", @MesAno = '" & mesAno & "'"




            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function CarregarFeedGridContribuintesOiPorSituacao(ByVal situacao As String) As DataSet
        Dim sSql As String

        Try


            sSql = ""
            sSql &= "exec controle_db.dbo.FeedContribuintesOiPorSituacao_SPS " & vbNewLine
            If situacao = Nothing Then
                sSql &= "@situacao = null" & vbNewLine
            Else
                sSql &= "@situacao = '" & situacao & "'"
            End If



            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function CarregarFeedGridContribuintesOiPorCategoria(ByVal situacao As String, ByVal mesAno As String) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec controle_db.dbo.FeedContribuintesOiPorCategoria_SPS " & vbNewLine
            If situacao = Nothing Then
                sSql &= "@cod_categoria = null" & vbNewLine
            Else
                sSql &= "@cod_categoria = " & situacao
            End If

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregarFeedGridContribuintesOiPorCategoriaNovo(ByVal situacao As String, ByVal mesRef As String, ByVal codUsuario As Integer) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            ' sSql &= "exec controle_db.dbo.FeedContribuintesOiPorCategoria_NOVO_SPS " & vbNewLine
            sSql &= "exec controle_db.dbo.Lista_para_operadores_SPS " & vbNewLine
            If situacao = Nothing Then
                sSql &= "@cod_categoria = null" & vbNewLine
            Else
                sSql &= "@cod_categoria = " & situacao
            End If
            sSql &= ",@operador_id = '" & codUsuario & "'"

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregarFeedGridRetornaRequisicao(ByVal Filtro As String, ByVal mesAno As String, ByVal codUsuario As Integer, ByVal tipoPesquisa As Integer) As DataSet ' tipo pesquisa 0 =  categoria, 1 =  operador
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec controle_db.dbo.RetornaRequisicao " & vbNewLine
            If tipoPesquisa = 0 Then
                sSql &= "@cod_categoria = " & Filtro
                sSql &= ", @mesRef = '" & mesAno & "'"
                sSql &= ", @Operador_id = null" & vbNewLine
            End If
            If tipoPesquisa = 1 Then
                sSql &= "@cod_categoria = null" & vbNewLine
                sSql &= ", @mesRef = '" & mesAno & "'"
                sSql &= ", @Operador_id  = " & Filtro
            End If


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregarFeedGridRetornaRequisicaoCemig(ByVal Filtro As String, ByVal mesAno As String, ByVal codUsuario As Integer, ByVal tipoPesquisa As Integer) As DataSet ' tipo pesquisa 0 =  categoria, 1 =  operador
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec integracao_cemig_db.dbo.RetornaRequisicao " & vbNewLine
            If tipoPesquisa = 0 Then
                sSql &= "@cod_categoria = " & Filtro
                sSql &= ", @mesRef = '" & mesAno & "'"
                sSql &= ", @Operador_id = null" & vbNewLine
            End If
            If tipoPesquisa = 1 Then
                sSql &= "@cod_categoria = null" & vbNewLine
                sSql &= ", @mesRef = '" & mesAno & "'"
                sSql &= ", @Operador_id  = " & Filtro
            End If


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaDadosPorCategoria(ByVal categoria As Integer, ByVal mesAno As String) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec controle_db.dbo.RetornaDadosGerenciais_SPS " & vbNewLine
            sSql &= "@cod_categoria = " & categoria & ", " & vbNewLine
            sSql &= "@mes_ref = '" & mesAno & "'"

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaDadosPorCategoriaListaNovaOi(ByVal categoria As Integer, ByVal mesAno As String, ByVal cidade As String) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec controle_db.dbo.RetornaDadosGerenciaisListaNova_SPS " & vbNewLine
            sSql &= "@cod_categoria = " & categoria & ", " & vbNewLine
            sSql &= "@mes_ref = '" & mesAno & "', "
            sSql &= "@cidade = '" & cidade & "'"

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaDadosPorCategoriaManual(ByVal categoria As Integer, ByVal cidade As String, ByVal dia As String) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec controle_db.dbo.RetornaDadosGerenciais_Manual_SPS " & vbNewLine
            sSql &= "@cod_categoria = " & categoria & ", " & vbNewLine
            sSql &= "@cidade = '" & cidade & "', " & vbNewLine
            sSql &= "@dia = " & dia


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaDadosPorCategoriaCemig(ByVal categoria As Integer, ByVal cidade As String, ByVal dia As String) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec integracao_cemig_db.dbo.RetornaDadosGerenciais_Cemig_SPS " & vbNewLine
            sSql &= "@cod_categoria = " & categoria & ", " & vbNewLine
            sSql &= "@cidade = '" & cidade & "', " & vbNewLine
            sSql &= "@dia = " & dia


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaFichasDisponiveisManual(ByVal categoria As Integer, ByVal cidade As String, ByVal dia As String) As Integer
        Dim sSql As String
        Dim ds As DataSet
        Dim quantidade As Integer

        Try

            sSql = ""
            sSql &= "select COUNT(*) " & vbNewLine
            sSql &= "from controle_db..requisicao_lista_tb requisicao_tb " & vbNewLine
            sSql &= "where cod_categoria = " & categoria & vbNewLine
            sSql &= " and dt_alteracao is null " & vbNewLine
            sSql &= " and cidade = '" & cidade & "'" & vbNewLine
            sSql &= " and dia = " & dia
            sSql &= " and cod_opera = 1 "


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    quantidade = ds.Tables(0).Rows(0).Item(0)
                    Return quantidade
                Else
                    Return 0
                End If
            End If

            Return Nothing

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaFichasDisponiveisCemig(ByVal categoria As Integer, ByVal cidade As String, ByVal dia As String) As Integer
        Dim sSql As String
        Dim ds As DataSet
        Dim quantidade As Integer

        Try

            sSql = ""
            sSql &= "select COUNT(*) " & vbNewLine
            sSql &= "from integracao_cemig_db..requisicao_tb requisicao_tb " & vbNewLine
            sSql &= "where cod_categoria = " & categoria & vbNewLine
            sSql &= " and dt_alteracao is null " & vbNewLine
            sSql &= " and cidade = '" & cidade & "'" & vbNewLine
            sSql &= " and dia = " & dia


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    quantidade = ds.Tables(0).Rows(0).Item(0)
                    Return quantidade
                Else
                    Return 0
                End If
            End If
            Return Nothing

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function RetornaFichasDisponiveis(ByVal categoria As Integer) As Integer
        Try
            Dim ds As DataSet
            Dim quantidade As Integer
            Dim sSql As String = ""
            sSql &= "select COUNT(*) " & vbNewLine
            sSql &= "from controle_db..requisicao_tb requisicao_tb " & vbNewLine
            sSql &= "where categoria = " & categoria & vbNewLine
            sSql &= " and atual = 'S' " & vbNewLine
            sSql &= " and vinculado_operador is null " & vbNewLine
            sSql &= " and bloqueado_requisicao = 0 " & vbNewLine
            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    quantidade = ds.Tables(0).Rows(0).Item(0)
                    Return quantidade
                Else
                    Return 0
                End If
            End If
            Return Nothing
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function
    Public Function RetornaFichasBloqueadas(ByVal categoria As Integer) As Integer
        Try
            Dim ds As DataSet
            Dim quantidade As Integer
            Dim sSql As String = ""
            sSql &= "select COUNT(*) " & vbNewLine
            sSql &= "from controle_db..requisicao_tb requisicao_tb " & vbNewLine
            sSql &= "where categoria = " & categoria & vbNewLine
            sSql &= " and atual = 'S' " & vbNewLine
            sSql &= " and vinculado_operador is null " & vbNewLine
            sSql &= " and bloqueado_requisicao = 1 " & vbNewLine
            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    quantidade = ds.Tables(0).Rows(0).Item(0)
                    Return quantidade
                Else
                    Return 0
                End If
            End If
            Return Nothing
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function AtualizaImpressaoFichas() As Boolean
        Try
            Dim ds As DataSet
            Dim sSql As String = "update controle_db..requisicao_tb
                                   set impressao_pendente = 0
                                    where atual = 'S'
                                    and impressao_pendente = 1"

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
            Return True
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function RetornaFichasDisponiveisListaNova(ByVal categoria As Integer, ByVal cidade As String) As Integer
        Dim sSql As String
        Dim ds As DataSet
        Dim quantidade As Integer

        Try

            sSql = ""
            sSql &= "select COUNT(*) " & vbNewLine
            sSql &= "from controle_db..requisicao_tb requisicao_tb " & vbNewLine
            sSql &= "inner join controle_db..cliente_tb cliente_tb " & vbNewLine
            sSql &= "on requisicao_tb.cod_ean = cliente_tb.cod_ean_cliente " & vbNewLine
            sSql &= "where categoria = " & categoria & vbNewLine
            sSql &= " and atual = 'S' " & vbNewLine
            sSql &= " and cliente_tb.cidade = '" & cidade & "'" & vbNewLine
            sSql &= " and vinculado_operador is null " & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    quantidade = ds.Tables(0).Rows(0).Item(0)
                    Return quantidade
                Else
                    Return 0
                End If
            End If
            Return Nothing

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaArquivos() As DataSet
        Dim ds As DataSet
        Dim valor As String = ""
        Try

            Dim sSql As String = " SELECT id_arq, nome_arquivo from interface_dados_db..Arquivo_retorno_tb"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    Return ds
                Else
                    Return Nothing
                End If
            End If

            Return Nothing

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function ConsultaArquivo(ByVal arq As String) As DataSet
        Dim ds As DataSet
        Dim id_arq As Integer = 0
        Try

            Dim sSql As String

            sSql = "SELECT id_arq from interface_dados_db..Arquivo_retorno_tb where nome_arquivo = '" & arq & "'"

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    id_arq = ds.Tables(0).Rows(0).Item(0)
                Else
                    id_arq = 0
                End If
            End If


            sSql = ""
            sSql = " Exec interface_dados_db..CarregaDadosArquivo @id_arq = " & id_arq


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    Return ds
                Else
                    Return Nothing
                End If
            End If
            Return Nothing


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function Busca_Telefones_Recuperacao() As DataSet
        Try
            Dim sSql As String

            sSql = ""
            sSql &= "EXEC interface_dados_db.dbo.RECUPERA_TELEFONES_SPS"


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function IsArquivoRemessaProcessado(ByVal nome_arquivo As String) As Boolean
        Dim ds As DataSet
        Dim count As Integer

        Try

            Dim sSql As String = "SELECT count(*) " _
                             + "FROM controle_db..arquivo_remessa_header_tb with (nolock)" _
                             + " where nome_arquivo = '" + nome_arquivo + "' "

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then

                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    count = ds.Tables(0).Rows(0).Item(0)

                    If count > 0 Then
                        Return True
                    Else
                        Return False
                    End If
                End If
            End If
            Return Nothing

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function ApagaArquivoRemessaProcessado(ByVal nome_arquivo As String) As Boolean
        Dim ds As DataSet
        Dim id As Integer
        Try

            Dim sSql As String = "select remessa_header_id " _
                            + "FROM controle_db..arquivo_remessa_header_tb with (nolock)" _
                            + " where nome_arquivo = '" + nome_arquivo + "'"

            ds = (ConnectionHelper.ExecuteSQL(CommandType.Text, sSql))

            If (ds.Tables(0).Rows.Count > 0) Then

                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    id = ds.Tables(0).Rows(0).Item(0)
                Else
                    Return False
                End If
            Else
                Return False
            End If

            sSql = ""
            sSql = "delete " _
                             + "FROM controle_db..arquivo_remessa_header_tb" _
                             + " where nome_arquivo = '" + nome_arquivo + "'"

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            sSql = ""
            sSql = "delete " _
             + "FROM controle_db..arquivo_remessa_tb " _
             + " where remessa_header_id = " & id

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)



            Return True


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function SalvarDadosArquivoRemessa(ByVal dadosRetorno As CRetorno, ByVal nomeArq As String, ByVal tipoRegistro As Integer, ByVal arq_id As Integer, ByVal UF As String) As Boolean

        Try
            Dim sSql As String = String.Empty

            If tipoRegistro = 1 Then
                sSql = ""
                sSql &= "insert into controle_db.dbo.arquivo_remessa_header_tb (nome_arquivo, data_servico, mes_ref, dt_inclusao, usuario, UF )" & vbNewLine
                sSql &= "values ('" & nomeArq & "','" & dadosRetorno.DdDataGeracao & "','" & dadosRetorno.DdDataServico & "', getdate(), 'AACI', '" & UF & "')" & vbNewLine

            ElseIf tipoRegistro = 2 Then
                sSql = ""
                sSql &= "insert into controle_db.dbo.arquivo_remessa_tb (remessa_header_id,requisicao_id,data_servico,mes_ano_ref,valor,Telefone,DDD,dt_inclusao,usuario)" & vbNewLine
                sSql &= "values (" & arq_id & "," & dadosRetorno.StDescReferencia & ", '" & dadosRetorno.DdDataServico & "','" & dadosRetorno.DdDataGeracao & "', " & dadosRetorno.DVlrServico & ", '" & dadosRetorno.StNumMeioAcessoCobranca & "', '" & dadosRetorno.StDDDMeioAcessoCobranca & "', getdate(),'AACI')"

            ElseIf tipoRegistro = 3 Then
                sSql = ""
                sSql &= "update controle_db.dbo.arquivo_remessa_header_tb " & vbNewLine
                sSql &= "set quantidade_registros = " & dadosRetorno.QuantidadeDeRegistro & ", valor_remessa = " & dadosRetorno.ValorArquivo & vbNewLine
                sSql &= " where remessa_header_id = " & arq_id
            End If



            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function


    Public Function RetornaIdArquivoRemessa(ByVal nomeArq As String) As Integer
        Dim ds As DataSet
        Dim id_remessa As Integer

        Try
            Dim sSql As String


            sSql = ""
            sSql &= "select remessa_header_id from controle_db.dbo.arquivo_remessa_header_tb " & vbNewLine
            sSql &= "where nome_arquivo = '" & nomeArq & "'"

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then

                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    id_remessa = ds.Tables(0).Rows(0).Item(0)
                    Return id_remessa
                Else
                    Return 0
                End If
            Else
                Return 0
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function ConsultaTelefoneEnviadoRemessa(ByVal ddd As String, ByVal telefone As String, ByVal dataRemessa As String) As Boolean
        Dim ds As DataSet
        Dim count As Integer

        Try

            Dim sSql As String = "SELECT count(*) " _
                             + "FROM controle_db..arquivo_remessa_tb with (nolock)" _
                             + " where ddd = '" + ddd + "' and telefone =  '" + telefone + "'" _
                             + " and CAST(YEAR(data_servico) AS CHAR(4)) + RIGHT('0' + CAST(MONTH(data_servico) AS VARCHAR(2)),2) = '" + dataRemessa + "'"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then

                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    count = ds.Tables(0).Rows(0).Item(0)

                    If count > 0 Then
                        Return True
                    Else
                        Return False
                    End If
                End If
            End If
            Return Nothing

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaUltimaSituacaoOi(ByVal telefone As String, ByVal ddd As String) As DataSet
        Dim ds As DataSet
        Dim valor As String = ""
        Try

            Dim sSql As String = ""


            sSql &= "DECLARE @MesAnterior datetime, @primeirodiaMesAnterior smalldatetime, @ultimodiamesMesAnterior DATETIME  " & vbNewLine
            sSql &= "DECLARE @MesAtual datetime, @primeirodiaMesAtual smalldatetime, @ultimodiamesMesAtual DATETIME " & vbNewLine
            sSql &= "set @MesAnterior = DATEADD(month, -1,getdate()) " & vbNewLine
            sSql &= "set @MesAtual = getdate()" & vbNewLine
            sSql &= "SELECT @primeirodiaMesAnterior = CONVERT(VARCHAR, @MesAnterior - DAY(@MesAnterior) + 1, 103) " & vbNewLine
            sSql &= "SELECT @ultimodiamesMesAnterior = DATEADD(d, -DAY(@MesAnterior),DATEADD(m,1,@MesAnterior))  " & vbNewLine
            sSql &= "SELECT @primeirodiaMesAtual = CONVERT(VARCHAR, @MesAtual - DAY(@MesAtual) + 1, 103)  " & vbNewLine
            sSql &= "SELECT @ultimodiamesMesAtual = DATEADD(d, -DAY(@MesAtual),DATEADD(m,1,@MesAtual))  " & vbNewLine

            sSql &= "select StDDDMeioAcessoCobranca" & vbNewLine
            sSql &= "	 , StNumMeioAcessoCobranca " & vbNewLine
            sSql &= ", DVlrServico" & vbNewLine
            sSql &= "	 , DdDataServico" & vbNewLine
            sSql &= "	 , DdDataSituacao" & vbNewLine
            sSql &= "	 , UsSitFaturamento" & vbNewLine
            sSql &= "	 , UiMotivoSituacao" & vbNewLine
            sSql &= "	 , StDescReferencia" & vbNewLine
            sSql &= "	 into #arquivo" & vbNewLine
            sSql &= "from interface_dados_db..arquivo_retorno_detalhe_tb" & vbNewLine
            sSql &= "where substring(StNumMeioAcessoCobranca,3,12) = '" & RTrim(LTrim(telefone)) & "'" & vbNewLine
            sSql &= " and StDDDMeioAcessoCobranca = '" & RTrim(LTrim(ddd)) & "'" & vbNewLine
            sSql &= "order by convert (DATE, DdDataSituacao) asc  " & vbNewLine

            sSql &= "select UsSitFaturamento from #arquivo "
            sSql &= " where convert (DATE,ddDataSituacao) between @primeirodiaMesAtual and @ultimodiamesMesAtual"
            sSql &= " and usSitFaturamento in ('afaturar','faturado')"

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                Return ds
            Else
                Return Nothing
            End If

            Return Nothing

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregarFeedGridRecuperacao(ByVal codUsuario As Integer) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec controle_db.dbo.Lista_recuperacao_SPS " & vbNewLine
            sSql &= " @operador_id = '" & codUsuario & "'"

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function RetornaDadosCSVOi(categoria As Integer) As DataSet
        Try
            Dim sSql As String = $"exec controle_db.dbo.GeraArquivoCSVOi 
                                    @categoria = {categoria}"
            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaDadosCSVManual(categoria As Integer) As DataSet
        Try
            Dim sSql As String = $"exec controle_db.dbo.GeraArquivoCSVManual 
                                    @categoria = {categoria}"
            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function



#End Region

#Region "CEMIG"
    Public Function RetornaDadosPorCategoriaCemig(ByVal categoria As Integer, ByVal mesAno As String) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec integracao_cemig_db.dbo.RetornaDadosGerenciais_SPS " & vbNewLine
            sSql &= "@cod_categoria = " & categoria & ", " & vbNewLine
            sSql &= "@mes_ref = '" & mesAno & "'"

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaFichasDisponiveisCemig(ByVal categoria As Integer) As Integer
        Try
            Dim ds As DataSet
            Dim quantidade As Integer
            Dim sSql As String = ""
            sSql &= "select COUNT(*) " & vbNewLine
            sSql &= "from integracao_cemig_db..requisicao_tb requisicao_tb " & vbNewLine
            sSql &= "where categoria = " & categoria & vbNewLine
            sSql &= " and atual = 'S' " & vbNewLine
            sSql &= " and vinculado_operador is null " & vbNewLine
            sSql &= " and bloqueado_requisicao = 0 " & vbNewLine
            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    quantidade = ds.Tables(0).Rows(0).Item(0)
                    Return quantidade
                Else
                    Return 0
                End If
            End If
            Return Nothing
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function
    Public Function RetornaFichasBloqueadasCemig(ByVal categoria As Integer) As Integer
        Try
            Dim ds As DataSet
            Dim quantidade As Integer
            Dim sSql As String = ""
            sSql &= "select COUNT(*) " & vbNewLine
            sSql &= "from integracao_cemig_db..requisicao_tb requisicao_tb " & vbNewLine
            sSql &= "where categoria = " & categoria & vbNewLine
            sSql &= " and atual = 'S' " & vbNewLine
            sSql &= " and vinculado_operador is null " & vbNewLine
            sSql &= " and bloqueado_requisicao = 1 " & vbNewLine
            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    quantidade = ds.Tables(0).Rows(0).Item(0)
                    Return quantidade
                Else
                    Return 0
                End If
            End If
            Return Nothing
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function CarregarFeedGridContribuintesCemigPorCategoriaNovo(situacao As String, codUsuario As Integer) As DataSet
        Try
            Dim sSql As String
            sSql = ""
            ' sSql &= "exec controle_db.dbo.FeedContribuintesOiPorCategoria_NOVO_SPS " & vbNewLine
            sSql &= "exec integracao_cemig_db.dbo.Lista_para_operadores_SPS " & vbNewLine
            If situacao = Nothing Then
                sSql &= "@cod_categoria = null" & vbNewLine
            Else
                sSql &= "@cod_categoria = " & situacao
            End If
            sSql &= ",@operador_id = '" & codUsuario & "'"

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregarFeedGridContribuintesCemigPorCategoria(ByVal situacao As String, ByVal mesAno As String) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec integracao_cemig_db.dbo.FeedContribuintesCemigPorCategoria_SPS " & vbNewLine
            If situacao = Nothing Then
                sSql &= "@cod_categoria = null" & vbNewLine
            Else
                sSql &= "@cod_categoria = " & situacao
            End If

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function RetornaDadosCSVCemig(categoria As Integer) As DataSet
        Try
            Dim sSql As String = $"exec controle_db.dbo.GeraArquivoCSVCemig 
                                    @categoria = {categoria}"
            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

#End Region

End Class
