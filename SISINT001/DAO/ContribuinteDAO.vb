﻿Public Class ContribuinteDAO

#Region "Variáveis"

    Private ClienteId As Decimal
    Private Cod_EAN_Cliente As String
    Private NomeCliente1 As String
    Private NomeCliente2 As String
    Private Operador As String
    Private CNPJ_CPF As String
    Private IE_CI As String
    Private Endereco As String
    Private Bairro As String
    Private Cidade As String
    Private UF As String
    Private CEP As String
    Private Telefone1 As String
    Private Telefone2 As String
    Private Email As String
    Private DataCadastro As String
    Private DataNascimento As String
    Private Cod_Categoria As Integer
    Private Observacao As String
    Private Referencia As String
    Private Descontinuado As String
    Private Cod_grupo As Integer
    Private Valor As Decimal
    Private Cod_Especie As Integer
    Private DiaLimite As Integer
    Private Dt_ligar As String
    Private Dt_inativo As String
    Private Cod_empresa As Integer
    Private Cod_Cidade As Integer
    Private Cod_Bairro As Integer
    Private Cod_Operadora As Integer
    Private Tel_desligado As String
    Private Nao_pedir_extra As String
    Private Cod_Usuario As Integer
    Private DT_NC_MP As String
    Private Nao_Pedir_Mensal As String
    Private DT_Reajuste As String
    Private Cod_Regiao As Integer


    Private ConnectionHelper As Util.ConnectionHelper

#End Region

#Region "Construtores"

    Sub New(Optional ByVal p_ClienteId As Decimal = 0,
         Optional ByVal p_Cod_EAN_Cliente As String = "",
   Optional ByVal p_NomeCliente1 As String = "",
   Optional ByVal p_NomeCliente2 As String = "",
   Optional ByVal p_Operador As String = "",
   Optional ByVal p_CNPJ_CPF As String = "",
   Optional ByVal p_IE_CI As String = "",
   Optional ByVal p_Endereco As String = "",
   Optional ByVal p_Bairro As String = "",
   Optional ByVal p_Cidade As String = "",
   Optional ByVal p_UF As String = "",
   Optional ByVal p_CEP As String = "",
   Optional ByVal p_Telefone1 As String = "",
   Optional ByVal p_Telefone2 As String = "",
   Optional ByVal p_Email As String = "",
   Optional ByVal p_DataCadastro As String = "",
   Optional ByVal p_DataNascimento As String = "",
   Optional ByVal p_Cod_Categoria As Integer = 0,
   Optional ByVal p_Observacao As String = "",
   Optional ByVal p_Referencia As String = "",
   Optional ByVal p_Descontinuado As String = "",
   Optional ByVal p_Cod_grupo As Integer = 0,
   Optional ByVal p_Valor As Decimal = 0,
   Optional ByVal p_Cod_Especie As Integer = 0,
   Optional ByVal p_DiaLimite As Integer = 0,
   Optional ByVal p_Dt_ligar As String = "",
   Optional ByVal p_Dt_inativo As String = "",
   Optional ByVal p_Cod_empresa As Integer = 0,
   Optional ByVal p_Cod_Cidade As Integer = 0,
   Optional ByVal p_Cod_Bairro As Integer = 0,
   Optional ByVal p_Cod_Operadora As Integer = 0,
   Optional ByVal p_Tel_desligado As String = "",
   Optional ByVal p_Nao_pedir_extra As String = "",
   Optional ByVal p_Cod_Usuario As Integer = 0,
   Optional ByVal p_DT_NC_MP As String = "",
   Optional ByVal p_Nao_Pedir_Mensal As String = "",
   Optional ByVal p_DT_Reajuste As String = "",
   Optional ByVal p_Cod_Regiao As Integer = 0)

        ClienteId = p_ClienteId
        Cod_EAN_Cliente = p_Cod_EAN_Cliente
        NomeCliente1 = p_NomeCliente1
        NomeCliente2 = p_NomeCliente2
        Operador = p_Operador
        CNPJ_CPF = p_CNPJ_CPF
        IE_CI = p_IE_CI
        Endereco = p_Endereco
        Bairro = p_Bairro
        Cidade = p_Cidade
        UF = p_UF
        CEP = p_CEP
        Telefone1 = p_Telefone1
        Telefone2 = p_Telefone2
        Email = p_Email
        DataCadastro = p_DataCadastro
        DataNascimento = p_DataNascimento
        Cod_Categoria = p_Cod_Categoria
        Observacao = p_Observacao
        Referencia = p_Referencia
        Descontinuado = p_Descontinuado
        Cod_grupo = p_Cod_grupo
        Valor = p_Valor
        Cod_Especie = p_Cod_Especie
        DiaLimite = p_DiaLimite
        Dt_ligar = p_Dt_ligar
        Dt_inativo = p_Dt_inativo
        Cod_empresa = p_Cod_empresa
        Cod_Cidade = p_Cod_Cidade
        Cod_Bairro = p_Cod_Bairro
        Cod_Operadora = p_Cod_Operadora
        Tel_desligado = p_Tel_desligado
        Nao_pedir_extra = p_Nao_pedir_extra
        Cod_Usuario = p_Cod_Usuario
        DT_NC_MP = p_DT_NC_MP
        Nao_Pedir_Mensal = p_Nao_Pedir_Mensal
        DT_Reajuste = p_DT_Reajuste
        Cod_Regiao = p_Cod_Regiao


        ConnectionHelper = New Util.ConnectionHelper(System.Reflection.Assembly.GetExecutingAssembly)

    End Sub


    Sub New(ByRef p_connectionHelper As Util.ConnectionHelper,
            Optional ByVal p_ClienteId As Decimal = 0,
         Optional ByVal p_Cod_EAN_Cliente As String = "",
   Optional ByVal p_NomeCliente1 As String = "",
   Optional ByVal p_NomeCliente2 As String = "",
   Optional ByVal p_Operador As String = "",
   Optional ByVal p_CNPJ_CPF As String = "",
   Optional ByVal p_IE_CI As String = "",
   Optional ByVal p_Endereco As String = "",
   Optional ByVal p_Bairro As String = "",
   Optional ByVal p_Cidade As String = "",
   Optional ByVal p_UF As String = "",
   Optional ByVal p_CEP As String = "",
   Optional ByVal p_Telefone1 As String = "",
   Optional ByVal p_Telefone2 As String = "",
   Optional ByVal p_Email As String = "",
   Optional ByVal p_DataCadastro As String = "",
   Optional ByVal p_DataNascimento As String = "",
   Optional ByVal p_Cod_Categoria As Integer = 0,
   Optional ByVal p_Observacao As String = "",
   Optional ByVal p_Referencia As String = "",
   Optional ByVal p_Descontinuado As String = "",
   Optional ByVal p_Cod_grupo As Integer = 0,
   Optional ByVal p_Valor As Decimal = 0,
   Optional ByVal p_Cod_Especie As Integer = 0,
   Optional ByVal p_DiaLimite As Integer = 0,
   Optional ByVal p_Dt_ligar As String = "",
   Optional ByVal p_Dt_inativo As String = "",
   Optional ByVal p_Cod_empresa As Integer = 0,
   Optional ByVal p_Cod_Cidade As Integer = 0,
   Optional ByVal p_Cod_Bairro As Integer = 0,
   Optional ByVal p_Cod_Operadora As Integer = 0,
   Optional ByVal p_Tel_desligado As String = "",
   Optional ByVal p_Nao_pedir_extra As String = "",
   Optional ByVal p_Cod_Usuario As Integer = 0,
   Optional ByVal p_DT_NC_MP As String = "",
   Optional ByVal p_Nao_Pedir_Mensal As String = "",
   Optional ByVal p_DT_Reajuste As String = "",
   Optional ByVal p_Cod_Regiao As Integer = 0)

        ClienteId = p_ClienteId
        Cod_EAN_Cliente = p_Cod_EAN_Cliente
        NomeCliente1 = p_NomeCliente1
        NomeCliente2 = p_NomeCliente2
        Operador = p_Operador
        CNPJ_CPF = p_CNPJ_CPF
        IE_CI = p_IE_CI
        Endereco = p_Endereco
        Bairro = p_Bairro
        Cidade = p_Cidade
        UF = p_UF
        CEP = p_CEP
        Telefone1 = p_Telefone1
        Telefone2 = p_Telefone2
        Email = p_Email
        DataCadastro = p_DataCadastro
        DataNascimento = p_DataNascimento
        Cod_Categoria = p_Cod_Categoria
        Observacao = p_Observacao
        Referencia = p_Referencia
        Descontinuado = p_Descontinuado
        Cod_grupo = p_Cod_grupo
        Valor = p_Valor
        Cod_Especie = p_Cod_Especie
        DiaLimite = p_DiaLimite
        Dt_ligar = p_Dt_ligar
        Dt_inativo = p_Dt_inativo
        Cod_empresa = p_Cod_empresa
        Cod_Cidade = p_Cod_Cidade
        Cod_Bairro = p_Cod_Bairro
        Cod_Operadora = p_Cod_Operadora
        Tel_desligado = p_Tel_desligado
        Nao_pedir_extra = p_Nao_pedir_extra
        Cod_Usuario = p_Cod_Usuario
        DT_NC_MP = p_DT_NC_MP
        Nao_Pedir_Mensal = p_Nao_Pedir_Mensal
        DT_Reajuste = p_DT_Reajuste
        Cod_Regiao = p_Cod_Regiao
        ConnectionHelper = p_connectionHelper
    End Sub

#End Region

#Region "metodos"

    Public Function Consultar(ByVal filtro As Integer) As DataSet

        Try
            Dim sSql As String = "select * " _
                                 + "from controle_db..cliente_tb with (nolock)" _
                                 + "where cod_cliente = " & filtro



            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function PesquisarPorTelefone(ByVal telefone As String, ByVal ddd As String, ByVal cod_ean As String, ByVal opcao As Integer) As DataSet

        Dim sSql As String
        Try

            If telefone.Substring(0, 1) = "(" Then

                sSql = " SELECT " _
                            + "cliente_id, cod_cliente, Cod_EAN_Cliente, nome_cliente, nome_cliente2, Operador, CNPJ_CPF, IE_CI, Endereco, Bairro, Cidade, UF, CEP" _
                            + ",substring(replace(telefone1,'-',''),5,12) as telefone1" _
                            + ",telefone2,email,data_cadastro,data_nascimento,cod_categoria,observacao,referencia,descontinuado,cod_grupo,valor,cod_especie" _
                            + ",dia_limite,data_ligar,data_inativo,cod_empresa,cod_cidade,cod_bairro,cod_operador,telefone_deslig,nao_pedir_extra,cod_usuario" _
                            + ",data_nc_mp,nao_pedir_mensal,data_reajuste,cod_regiao,dt_inclusao,dt_alteracao,dt_contratacao" _
                            + ",substring(telefone1,2,2) as DDD" _
                            + ",clienteOi,SituacaoOi,CategoriaOi" _
                           + " FROM controle_db..cliente_tb with (nolock) " _
                           + " WHERE "
                If opcao = 1 Then
                    sSql = sSql + "telefone1 = '" + telefone + "'"
                    If cod_ean <> "" Then
                        sSql = sSql + "and cod_ean_cliente = '" + cod_ean + "'"
                    End If
                    If ddd <> "" Then
                        sSql = sSql + "and DDD = '" + ddd + "'"
                    End If
                Else
                    sSql = sSql + "telefone2 = '" + telefone + "'"
                    If cod_ean <> "" Then
                        sSql = sSql + "and cod_ean_cliente = '" + cod_ean + "'"
                    End If
                    If ddd <> "" Then
                        sSql = sSql + "and DDD = '" + ddd + "'"
                    End If
                End If

                '    sSql = sSql + "UNION " & vbNewLine

                'sSql = sSql + "select " _
                '            + "cliente_id, cod_cliente, Cod_EAN_Cliente, nome_cliente, nome_cliente2, Operador, CNPJ_CPF, IE_CI, Endereco, Bairro, Cidade, UF, CEP" _
                '            + ",substring(replace(telefone1,'-',''),5,12) as telefone1" _
                '            + ",telefone2,email,data_cadastro,data_nascimento,cod_categoria,observacao,referencia,descontinuado,cod_grupo,valor,cod_especie" _
                '            + ",dia_limite,data_ligar,data_inativo,cod_empresa,cod_cidade,cod_bairro,cod_operador,telefone_deslig,nao_pedir_extra,cod_usuario" _
                '            + ",data_nc_mp,nao_pedir_mensal,data_reajuste,cod_regiao,dt_inclusao,dt_alteracao,dt_contratacao" _
                '            + ",substring(telefone1,2,2) as DDD" _
                '            + ",clienteOi,SituacaoOi,CategoriaOi" _
                '            + " FROM controle_db..cliente_tb with (nolock)  " _
                '            + " WHERE substring(replace(telefone1,'-',''),5,12) = '" + telefone + "'"
                'If cod_ean <> "" Then
                '    sSql = sSql + " and cod_ean_cliente = '" + cod_ean + "'"
                'End If
                'If ddd <> "" Then
                '    sSql = sSql + " AND  substring(telefone1,2,2) = '" + ddd + "'"
                'End If

            Else

                sSql = " SELECT * " _
                                 + " FROM controle_db..cliente_tb with (nolock) " _
                                 + " WHERE "
                If opcao = 1 Then
                    sSql = sSql + "telefone1 = '" + telefone + "'"
                    If cod_ean <> "" Then
                        sSql = sSql + "and cod_ean_cliente = '" + cod_ean + "'"
                    End If
                    If ddd <> "" Then
                        sSql = sSql + "and DDD = '" + ddd + "'"
                    End If
                Else
                    sSql = sSql + "telefone2 = '" + telefone + "'"
                    If cod_ean <> "" Then
                        sSql = sSql + "and cod_ean_cliente = '" + cod_ean + "'"
                    End If
                    If ddd <> "" Then
                        sSql = sSql + "and DDD = '" + ddd + "'"
                    End If

                End If

                '        sSql = sSql + "UNION " & vbNewLine

                '    sSql = sSql + "select " _
                '                + "cliente_id, cod_cliente, Cod_EAN_Cliente, nome_cliente, nome_cliente2, Operador, CNPJ_CPF, IE_CI, Endereco, Bairro, Cidade, UF, CEP" _
                '                + ",substring(replace(telefone1,'-',''),5,12) as telefone1" _
                '                + ",telefone2,email,data_cadastro,data_nascimento,cod_categoria,observacao,referencia,descontinuado,cod_grupo,valor,cod_especie" _
                '                + ",dia_limite,data_ligar,data_inativo,cod_empresa,cod_cidade,cod_bairro,cod_operador,telefone_deslig,nao_pedir_extra,cod_usuario" _
                '                + ",data_nc_mp,nao_pedir_mensal,data_reajuste,cod_regiao,dt_inclusao,dt_alteracao,dt_contratacao" _
                '                + ",substring(telefone1,2,2) as DDD" _
                '                + ",clienteOi,SituacaoOi,CategoriaOi" _
                '                + " FROM controle_db..cliente_tb with (nolock)  " _
                '                + "WHERE substring(replace(telefone1,'-',''),5,12) = '" + telefone + "'"
                '    If cod_ean <> "" Then
                '        sSql = sSql + "and cod_ean_cliente = '" + cod_ean + "'"
                '    End If
                'If ddd <> "" Then
                '    sSql = sSql + " AND  substring(telefone1,2,2) = '" + ddd + "'"
                '    End If


                sSql = sSql + "UNION " & vbNewLine

                sSql = sSql + "select " _
                            + "cliente_id, cod_cliente, Cod_EAN_Cliente, nome_cliente, nome_cliente2, Operador, CNPJ_CPF, IE_CI, Endereco, Bairro, Cidade, UF, CEP" _
                            + ",telefone1 as telefone1" _
                            + ",telefone2,email,data_cadastro,data_nascimento,cod_categoria,observacao,referencia,descontinuado,cod_grupo,valor,cod_especie" _
                            + ",dia_limite,data_ligar,data_inativo,cod_empresa,cod_cidade,cod_bairro,cod_operador,telefone_deslig,nao_pedir_extra,cod_usuario" _
                            + ",data_nc_mp,nao_pedir_mensal,data_reajuste,cod_regiao,dt_inclusao,dt_alteracao,dt_contratacao" _
                            + ",ddd as DDD" _
                            + ",clienteOi,SituacaoOi,CategoriaOi" _
                            + " FROM controle_db..cliente_tb with (nolock)  " _
                            + "WHERE telefone1 = '" + telefone + "'"
                If ddd <> "" Then
                    sSql = sSql + " AND  DDD = '" + ddd + "'"
                End If

            End If

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function PesquisarPorTelefoneSimplicado(ByVal telefone As String, ByVal ddd As String, ByVal opcao As Integer) As DataSet

        Dim sSql As String
        Try

            If telefone.Substring(0, 1) = "(" Then

                sSql = " SELECT " _
                            + "cliente_id, cod_cliente, Cod_EAN_Cliente, nome_cliente, nome_cliente2, Operador, CNPJ_CPF, IE_CI, Endereco, Bairro, Cidade, UF, CEP" _
                            + ",substring(replace(telefone1,'-',''),5,12) as telefone1" _
                            + ",telefone2,email,data_cadastro,data_nascimento,cod_categoria,observacao,referencia,descontinuado,cod_grupo,valor,cod_especie" _
                            + ",dia_limite,data_ligar,data_inativo,cod_empresa,cod_cidade,cod_bairro,cod_operador,telefone_deslig,nao_pedir_extra,cod_usuario" _
                            + ",data_nc_mp,nao_pedir_mensal,data_reajuste,cod_regiao,dt_inclusao,dt_alteracao,dt_contratacao" _
                            + ",substring(telefone1,2,2) as DDD" _
                            + ",clienteOi,SituacaoOi,CategoriaOi" _
                           + " FROM controle_db..cliente_tb with (nolock) " _
                           + " WHERE "
                If opcao = 1 Then
                    sSql = sSql + "telefone1 = '" + telefone + "'"
                    If ddd <> "" Then
                        sSql = sSql + "and DDD = '" + ddd + "'"
                    End If
                Else
                    sSql = sSql + "telefone2 = '" + telefone + "'"
                    If ddd <> "" Then
                        sSql = sSql + "and DDD = '" + ddd + "'"
                    End If
                End If

                sSql = sSql + "UNION " & vbNewLine

                sSql = sSql + "select " _
                            + "cliente_id, cod_cliente, Cod_EAN_Cliente, nome_cliente, nome_cliente2, Operador, CNPJ_CPF, IE_CI, Endereco, Bairro, Cidade, UF, CEP" _
                            + ",substring(replace(telefone1,'-',''),5,12) as telefone1" _
                            + ",telefone2,email,data_cadastro,data_nascimento,cod_categoria,observacao,referencia,descontinuado,cod_grupo,valor,cod_especie" _
                            + ",dia_limite,data_ligar,data_inativo,cod_empresa,cod_cidade,cod_bairro,cod_operador,telefone_deslig,nao_pedir_extra,cod_usuario" _
                            + ",data_nc_mp,nao_pedir_mensal,data_reajuste,cod_regiao,dt_inclusao,dt_alteracao,dt_contratacao" _
                            + ",substring(telefone1,2,2) as DDD" _
                            + ",clienteOi,SituacaoOi,CategoriaOi" _
                            + " FROM controle_db..cliente_tb with (nolock)  " _
                            + " WHERE substring(replace(telefone1,'-',''),5,12) = '" + telefone + "'"

                If ddd <> "" Then
                    sSql = sSql + " AND  substring(telefone1,2,2) = '" + ddd + "'"
                End If

            Else

                sSql = " SELECT * " _
                                 + " FROM controle_db..cliente_tb with (nolock) " _
                                 + " WHERE "
                If opcao = 1 Then
                    sSql = sSql + "telefone1 = '" + telefone + "'"
                    If ddd <> "" Then
                        sSql = sSql + "and DDD = '" + ddd + "'"
                    End If
                Else
                    sSql = sSql + "telefone2 = '" + telefone + "'"
                    If ddd <> "" Then
                        sSql = sSql + "and DDD = '" + ddd + "'"
                    End If

                End If

                sSql = sSql + "UNION " & vbNewLine

                sSql = sSql + "select " _
                            + "cliente_id, cod_cliente, Cod_EAN_Cliente, nome_cliente, nome_cliente2, Operador, CNPJ_CPF, IE_CI, Endereco, Bairro, Cidade, UF, CEP" _
                            + ",substring(replace(telefone1,'-',''),5,12) as telefone1" _
                            + ",telefone2,email,data_cadastro,data_nascimento,cod_categoria,observacao,referencia,descontinuado,cod_grupo,valor,cod_especie" _
                            + ",dia_limite,data_ligar,data_inativo,cod_empresa,cod_cidade,cod_bairro,cod_operador,telefone_deslig,nao_pedir_extra,cod_usuario" _
                            + ",data_nc_mp,nao_pedir_mensal,data_reajuste,cod_regiao,dt_inclusao,dt_alteracao,dt_contratacao" _
                            + ",substring(telefone1,2,2) as DDD" _
                            + ",clienteOi,SituacaoOi,CategoriaOi" _
                            + " FROM controle_db..cliente_tb with (nolock)  " _
                            + "WHERE substring(replace(telefone1,'-',''),5,12) = '" + telefone + "'"
                If ddd <> "" Then
                    sSql = sSql + " AND  substring(telefone1,2,2) = '" + ddd + "'"
                End If

            End If

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function PesquisarPorTelefone_old(ByVal telefone As String, ByVal opcao As Integer) As DataSet

        Try

            Dim sSql As String = " SELECT * " _
                             + " FROM DIGITAL..t_clientes with (nolock) " _
                             + " WHERE "
            If opcao = 1 Then
                sSql = sSql + "telefone 1 = '" + telefone + "'"
            Else
                sSql = sSql + "telefone 2 = '" + telefone + "'"
            End If



            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function PesquisarPorCodEAN_old(ByVal cod_ean As String) As DataSet

        Try

            Dim sSql As String = "SELECT * " _
                             + " FROM DIGITAL..t_clientes with (nolock)" _
                             + " WHERE " _
                             + " COD_EAN_CLIENTE = '" + cod_ean + "'"


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function
    Public Function PesquisarPorCodEAN(ByVal cod_ean As String) As DataSet
        Dim ds As DataSet

        Try

            Dim sSql As String = "SELECT * " _
                             + " FROM controle_db..cliente_tb with (nolock)" _
                             + " WHERE " _
                             + " COD_EAN_CLIENTE = '" + cod_ean + "'"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If ds.Tables(0).Rows.Count = 0 Then


                Dim iSql As String = "SELECT " & vbNewLine
                iSql &= "cod_cliente as cliente_id,cod_cliente,cod_ean_cliente,nome_cliente,nome_cliente2,operador,cnpj_cpf,ie_ci,endereco,bairro,cidade,uf,cep, SUBSTRING(replace(telefone1,'-',''),5,10) as telefone1, telefone2,email" & vbNewLine
                iSql &= ",DATA_CAD as data_cadastro, DATA_NASC as data_nascimento,cod_categoria,observacao,referencia,descontinuado,cod_grupo,valor,cod_especie,dia_limite,DT_LIGAR as data_ligar,DT_INATIVO as data_inativo" & vbNewLine
                iSql &= ",cod_empresa,cod_cidade,cod_bairro,cod_opera as cod_operador,TEL_DESLIG as telefone_deslig,N_EXTRA as nao_pedir_extra,cod_usuario,DTNCMP as  data_nc_mp,NPEDIRM as nao_pedir_mensal" & vbNewLine
                iSql &= ",DTREAJUSTE as data_reajuste,cod_regiao,DATA_CAD as dt_contratacao,substring(telefone1,2,2) as DDD, 'S' as clienteOi,null as SituacaoOi,COD_CATEGORIA as  CategoriaOi" & vbNewLine
                iSql &= " FROM controle_db..lista_retida_tmp_tb with (nolock)" & vbNewLine
                iSql &= " WHERE "
                iSql &= " COD_EAN_CLIENTE = '" + cod_ean + "'"


                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, iSql)

            End If

            If ds.Tables(0).Rows.Count = 0 Then


                Dim iSql As String = "SELECT " & vbNewLine
                iSql &= "cod_cliente as cliente_id,cod_cliente,cod_ean_cliente,nome_cliente,nome_cliente2,operador,cnpj_cpf,ie_ci,endereco,bairro,cidade,uf,cep, SUBSTRING(replace(telefone1,'-',''),5,10) as telefone1, telefone2,email" & vbNewLine
                iSql &= ",DATA_CAD as data_cadastro, DATA_NASC as data_nascimento,cod_categoria,observacao,referencia,descontinuado,cod_grupo,valor,cod_especie,dia_limite,DT_LIGAR as data_ligar,DT_INATIVO as data_inativo" & vbNewLine
                iSql &= ",cod_empresa,cod_cidade,cod_bairro,cod_opera as cod_operador,TEL_DESLIG as telefone_deslig,N_EXTRA as nao_pedir_extra,cod_usuario,DTNCMP as  data_nc_mp,NPEDIRM as nao_pedir_mensal" & vbNewLine
                iSql &= ",DTREAJUSTE as data_reajuste,cod_regiao,DATA_CAD as dt_contratacao,substring(telefone1,2,2) as DDD, 'S' as clienteOi,null as SituacaoOi,COD_CATEGORIA as  CategoriaOi" & vbNewLine
                iSql &= " FROM controle_db..lista_retida_tmp_tb with (nolock)" & vbNewLine
                iSql &= " WHERE "
                iSql &= " COD_EAN_CLIENTE = '" + cod_ean + "'" & vbNewLine

                iSql &= " union" & vbNewLine

                iSql &= "select " & vbNewLine
                iSql &= "cod_cliente as cliente_id,cod_cliente,cod_ean_cliente,nome_cliente,nome_cliente2,operador,cnpj_cpf,ie_ci,endereco,bairro,cidade,uf,cep, SUBSTRING(replace(telefone1,'-',''),5,10) as telefone1, telefone2,email" & vbNewLine
                iSql &= ",DATA_CAD as data_cadastro, DATA_NASC as data_nascimento,cod_categoria,observacao,referencia,descontinuado,cod_grupo,valor,cod_especie,dia_limite,DT_LIGAR as data_ligar,DT_INATIVO as data_inativo" & vbNewLine
                iSql &= ",cod_empresa,cod_cidade,cod_bairro,cod_opera as cod_operador,TEL_DESLIG as telefone_deslig,N_EXTRA as nao_pedir_extra,cod_usuario,DTNCMP as  data_nc_mp,NPEDIRM as nao_pedir_mensal" & vbNewLine
                iSql &= ",DTREAJUSTE as data_reajuste,cod_regiao,DATA_CAD as dt_contratacao,substring(telefone1,2,2) as DDD, 'S' as clienteOi,null as SituacaoOi,COD_CATEGORIA as  CategoriaOi" & vbNewLine
                iSql &= "from digital..t_clientes" & vbNewLine
                iSql &= " WHERE COD_EAN_CLIENTE = '" + cod_ean + "'" & vbNewLine

                iSql &= " union" & vbNewLine
                iSql &= "select " & vbNewLine
                iSql &= "cod_cliente as cliente_id,cod_cliente,cod_ean_cliente,nome_cliente,nome_cliente2,operador,cnpj_cpf,ie_ci,endereco,bairro,cidade,uf,cep, SUBSTRING(replace(telefone1,'-',''),5,10) as telefone1, telefone2,email" & vbNewLine
                iSql &= ",DATA_CAD as data_cadastro, DATA_NASC as data_nascimento,cod_categoria,observacao,referencia,descontinuado,cod_grupo,valor,cod_especie,dia_limite,DT_LIGAR as data_ligar,DT_INATIVO as data_inativo" & vbNewLine
                iSql &= ",cod_empresa,cod_cidade,cod_bairro,cod_opera as cod_operador,TEL_DESLIG as telefone_deslig,N_EXTRA as nao_pedir_extra,cod_usuario,DTNCMP as  data_nc_mp,NPEDIRM as nao_pedir_mensal" & vbNewLine
                iSql &= ",DTREAJUSTE as data_reajuste,cod_regiao,DATA_CAD as dt_contratacao,substring(telefone1,2,2) as DDD, 'S' as clienteOi,null as SituacaoOi,COD_CATEGORIA as  CategoriaOi" & vbNewLine
                iSql &= "from digital..t_lista_retida_tmp" & vbNewLine
                iSql &= " WHERE COD_EAN_CLIENTE = '" + cod_ean + "'" & vbNewLine


                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, iSql)

            End If

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function PesquisarTabelaRetido(ByVal cod_ean As String) As DataSet

        Try

            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.PesquisaClienteTabelaRetido " & vbNewLine
            sSql &= "@cod_cliente = '" & cod_ean & "' "


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function PesquisarTabelaRetidoPorTelefone(ByVal telefone As String, ByVal ddd As String, ByVal cod_EAN As String) As DataSet

        Try

            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.PesquisaTelefoneTabelaRetido " & vbNewLine
            sSql &= "@telefone = '" & telefone & "' "
            sSql &= ", @cod_ean = '" & cod_EAN & "' "
            If ddd <> "" Then
                sSql &= ", @ddd = '" & ddd & "' "
            End If


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function PesquisarTabelaRetidoPorTelefoneSimplificado(ByVal telefone As String, ByVal ddd As String) As DataSet

        Try

            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.PesquisaTelefoneTabelaRetido " & vbNewLine
            sSql &= "@telefone = '" & telefone & "', "
            sSql &= "@cod_ean = null"
            If ddd <> "" Then
                sSql &= ", @ddd = '" & ddd & "' "
            End If


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function Busca_Lista_Telefonica(ByVal telefone As String, ByVal ddd As String, ByVal cod_ean As String) As DataSet
        Try
            Dim sSql As String

            sSql = ""
            sSql &= "SELECT cod_ean,DDD,telefone,valor,nome_cliente,categoria,mesSituacao,Situacao,dt_inclusao,dt_alteracao,usuario,cod_cidade" & vbNewLine
            sSql &= "FROM controle_db..lista_telefonica_tb" & vbNewLine
            sSql &= " where telefone = '" & telefone & "'"
            If cod_ean <> "" Then
                sSql &= " and cod_ean = '" & cod_ean & "' " & vbNewLine
            End If
            If ddd <> "" Then
                sSql &= " and  ddd = '" & ddd & "'" & vbNewLine
            End If

            sSql &= "UNION " & vbNewLine
            sSql &= "SELECT cod_ean,DDD,telefone,valor,nome_cliente,categoria,mesSituacao,Situacao,dt_inclusao,dt_alteracao,usuario,cod_cidade" & vbNewLine
            sSql &= "FROM controle_db..lista_telefonica_tb" & vbNewLine
            sSql &= " where telefone = '" & telefone & "'"
            If ddd <> "" Then
                sSql &= " and  ddd = '" & ddd & "'"
            End If

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregarGridContribuintes(ByVal filtro As CContribuinte) As DataSet

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.CarregaContribuintes_SPS " & vbNewLine
            If filtro.CodCliente = 0 Then
                sSql &= "@cod_cliente = 0, " & vbNewLine
            Else
                sSql &= "@cod_cliente = " & filtro.CodCliente & ", " & vbNewLine
            End If
            If filtro.Operador = Nothing Then
                sSql &= "@operadora_telefonica = 0" & vbNewLine
            Else
                sSql &= "@operadora_telefonica = " & filtro.Operador
            End If



            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregarGridContribuintesOi(ByVal filtro As CContribuinte) As DataSet

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.CarregaContribuintesOi_SPS " & vbNewLine
            If filtro.CodCliente = 0 Then
                sSql &= "@cod_cliente = 0, " & vbNewLine
            Else
                sSql &= "@cod_cliente = " & filtro.CodCliente & ", " & vbNewLine
            End If
            If filtro.Operador = Nothing Then
                sSql &= "@operadora_telefonica = 0" & vbNewLine
            Else
                sSql &= "@operadora_telefonica = " & filtro.Operador
            End If



            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function




    Public Function Salvar(ByVal contribuinte As CContribuinte) As Boolean

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.SalvarContribuinte_SPI " & vbNewLine
            sSql &= "  @CodCliente = " & IIf(contribuinte.CodCliente = 0, 0, contribuinte.CodCliente) & vbNewLine
            sSql &= ", @Cod_EAN_Cliente = '" & IIf(contribuinte.Cod_EAN_Cliente = "", "", contribuinte.Cod_EAN_Cliente) & "'" & vbNewLine
            sSql &= ", @NomeCliente1 = '" & IIf(contribuinte.NomeCliente1 = "", "", contribuinte.NomeCliente1) & "'" & vbNewLine
            sSql &= ", @NomeCliente2 = '" & IIf(contribuinte.NomeCliente2 = "", "", contribuinte.NomeCliente2) & "'" & vbNewLine
            sSql &= ", @Operador = '" & IIf(contribuinte.Operador = "", "", contribuinte.Operador) & "'" & vbNewLine
            sSql &= ", @cnpj_cpf = '" & IIf(contribuinte.CNPJ_CPF = "", "", contribuinte.CNPJ_CPF) & "'" & vbNewLine
            sSql &= ", @ie_ci = '" & IIf(contribuinte.IE_CI = "", "", contribuinte.IE_CI) & "'" & vbNewLine
            sSql &= ", @endereco = '" & IIf(contribuinte.Endereco = "", "", contribuinte.Endereco) & "'" & vbNewLine
            sSql &= ", @bairro = '" & IIf(contribuinte.Bairro = "", "", contribuinte.Bairro) & "'" & vbNewLine
            sSql &= ", @cidade = '" & IIf(contribuinte.Cidade = "", "", contribuinte.Cidade) & "'" & vbNewLine
            sSql &= ", @uf = '" & IIf(contribuinte.UF = "", "", contribuinte.UF) & "'" & vbNewLine
            sSql &= ", @cep = '" & IIf(contribuinte.CEP = "", "", contribuinte.CEP) & "'" & vbNewLine
            sSql &= ", @telefone1 = '" & IIf(contribuinte.Telefone1 = "", "", contribuinte.Telefone1) & "'" & vbNewLine
            sSql &= ", @telefone2 = '" & IIf(contribuinte.Telefone2 = "", "", contribuinte.Telefone2) & "'" & vbNewLine
            sSql &= ", @email = '" & IIf(contribuinte.Email = "", "", contribuinte.Email) & "'" & vbNewLine
            sSql &= ", @datacadastro = '" & IIf(contribuinte.DataCadastro = "", "", contribuinte.DataCadastro) & "'" & vbNewLine
            sSql &= ", @datanascimento = '" & IIf(contribuinte.DataNascimento = "", "", contribuinte.DataNascimento) & "'" & vbNewLine
            sSql &= ", @cod_categoria = " & IIf(contribuinte.Cod_Categoria = 0, 0, contribuinte.Cod_Categoria) & vbNewLine
            sSql &= ", @observacao = '" & IIf(contribuinte.Observacao = "", "", contribuinte.Observacao) & "'" & vbNewLine
            sSql &= ", @referencia = '" & IIf(contribuinte.Referencia = "", "", contribuinte.Referencia) & "'" & vbNewLine
            sSql &= ", @descontinuado = '" & IIf(contribuinte.Descontinuado = "", 0, contribuinte.Descontinuado) & "'" & vbNewLine
            sSql &= ", @cod_grupo = " & IIf(contribuinte.Cod_grupo = 0, 0, contribuinte.Cod_grupo) & vbNewLine
            sSql &= ", @valor = " & IIf(contribuinte.Valor = 0, 0, Replace(contribuinte.Valor, ",", ".")) & vbNewLine
            sSql &= ", @cod_especie = " & IIf(contribuinte.Cod_Especie = 0, 0, contribuinte.Cod_Especie) & vbNewLine
            sSql &= ", @dialimite = '" & IIf(contribuinte.DiaLimite = 0, "", contribuinte.DiaLimite) & "'" & vbNewLine
            sSql &= ", @dt_ligar = '" & IIf(contribuinte.Dt_ligar = "", "", contribuinte.Dt_ligar) & "'" & vbNewLine
            sSql &= ", @dt_inativo = '" & IIf(contribuinte.Dt_inativo = "", "", contribuinte.Dt_inativo) & "'" & vbNewLine
            sSql &= ", @cod_empresa = " & IIf(contribuinte.Cod_empresa = 0, 0, contribuinte.Cod_empresa) & vbNewLine
            sSql &= ", @cod_cidade = " & IIf(contribuinte.Cod_Cidade = 0, 0, contribuinte.Cod_Cidade) & vbNewLine
            sSql &= ", @cod_bairro = " & IIf(contribuinte.Cod_Bairro = 0, 0, contribuinte.Cod_Bairro) & vbNewLine
            sSql &= ", @cod_operador = " & IIf(contribuinte.Cod_Operador = 0, 0, contribuinte.Cod_Operador) & vbNewLine
            sSql &= ", @tel_desligado = '" & IIf(contribuinte.Tel_desligado = "", "", contribuinte.Tel_desligado) & "'" & vbNewLine
            sSql &= ", @nao_pedir_extra = '" & IIf(contribuinte.Nao_pedir_extra = "", "", contribuinte.Nao_pedir_extra) & "'" & vbNewLine
            sSql &= ", @cod_usuario = " & IIf(contribuinte.Cod_Usuario = 0, 0, contribuinte.Cod_Usuario) & vbNewLine
            sSql &= ", @dt_nc_mp = '" & IIf(contribuinte.DT_NC_MP = "", "", contribuinte.DT_NC_MP) & "'" & vbNewLine
            sSql &= ", @nao_pedir_mensal = '" & IIf(contribuinte.Nao_Pedir_Mensal = "", "", contribuinte.Nao_Pedir_Mensal) & "'" & vbNewLine
            sSql &= ", @dt_reajuste = '" & IIf(contribuinte.DT_Reajuste = "", "", contribuinte.DT_Reajuste) & "'" & vbNewLine
            sSql &= ", @cod_regiao = " & IIf(contribuinte.Cod_Regiao = 0, 0, contribuinte.Cod_Regiao) & "" & vbNewLine
            sSql &= ", @DDD = '" & IIf(contribuinte.DDD = "", "", contribuinte.DDD) & "'" & vbNewLine
            sSql &= ", @ClienteOi = '" & IIf(contribuinte.ClienteOi = "", "", contribuinte.ClienteOi) & "'"


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            'MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function SalvarCemig(ByVal contribuinte As CContribuinte) As Boolean

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec integracao_cemig_db.dbo.SalvarContribuinte_SPI " & vbNewLine
            sSql &= "  @CodCliente = " & IIf(contribuinte.CodCliente = 0, 0, contribuinte.CodCliente) & vbNewLine
            sSql &= ", @Cod_EAN_Cliente = '" & IIf(contribuinte.Cod_EAN_Cliente = "", "", contribuinte.Cod_EAN_Cliente) & "'" & vbNewLine
            sSql &= ", @NomeCliente1 = '" & IIf(contribuinte.NomeCliente1 = "", "", contribuinte.NomeCliente1) & "'" & vbNewLine
            sSql &= ", @NomeCliente2 = '" & IIf(contribuinte.NomeCliente2 = "", "", contribuinte.NomeCliente2) & "'" & vbNewLine
            sSql &= ", @Operador = '" & IIf(contribuinte.Operador = "", "", contribuinte.Operador) & "'" & vbNewLine
            sSql &= ", @cnpj_cpf = '" & IIf(contribuinte.CNPJ_CPF = "", "", contribuinte.CNPJ_CPF) & "'" & vbNewLine
            sSql &= ", @ie_ci = '" & IIf(contribuinte.IE_CI = "", "", contribuinte.IE_CI) & "'" & vbNewLine
            sSql &= ", @endereco = '" & IIf(contribuinte.Endereco = "", "", contribuinte.Endereco) & "'" & vbNewLine
            sSql &= ", @bairro = '" & IIf(contribuinte.Bairro = "", "", contribuinte.Bairro) & "'" & vbNewLine
            sSql &= ", @cidade = '" & IIf(contribuinte.Cidade = "", "", contribuinte.Cidade) & "'" & vbNewLine
            sSql &= ", @uf = '" & IIf(contribuinte.UF = "", "", contribuinte.UF) & "'" & vbNewLine
            sSql &= ", @cep = '" & IIf(contribuinte.CEP = "", "", contribuinte.CEP) & "'" & vbNewLine
            sSql &= ", @telefone1 = '" & IIf(contribuinte.Telefone1 = "", "", contribuinte.Telefone1) & "'" & vbNewLine
            sSql &= ", @telefone2 = '" & IIf(contribuinte.Telefone2 = "", "", contribuinte.Telefone2) & "'" & vbNewLine
            sSql &= ", @email = '" & IIf(contribuinte.Email = "", "", contribuinte.Email) & "'" & vbNewLine
            sSql &= ", @datacadastro = '" & IIf(contribuinte.DataCadastro = "", "", contribuinte.DataCadastro) & "'" & vbNewLine
            sSql &= ", @datanascimento = '" & IIf(contribuinte.DataNascimento = "", "", contribuinte.DataNascimento) & "'" & vbNewLine
            sSql &= ", @cod_categoria = " & IIf(contribuinte.Cod_Categoria = 0, 0, contribuinte.Cod_Categoria) & vbNewLine
            sSql &= ", @observacao = '" & IIf(contribuinte.Observacao = "", "", contribuinte.Observacao) & "'" & vbNewLine
            sSql &= ", @referencia = '" & IIf(contribuinte.Referencia = "", "", contribuinte.Referencia) & "'" & vbNewLine
            sSql &= ", @descontinuado = '" & IIf(contribuinte.Descontinuado = "", 0, contribuinte.Descontinuado) & "'" & vbNewLine
            sSql &= ", @cod_grupo = " & IIf(contribuinte.Cod_grupo = 0, 0, contribuinte.Cod_grupo) & vbNewLine
            sSql &= ", @valor = " & IIf(contribuinte.Valor = 0, 0, Replace(contribuinte.Valor, ",", ".")) & vbNewLine
            sSql &= ", @cod_especie = " & IIf(contribuinte.Cod_Especie = 0, 0, contribuinte.Cod_Especie) & vbNewLine
            sSql &= ", @dialimite = '" & IIf(contribuinte.DiaLimite = 0, "", contribuinte.DiaLimite) & "'" & vbNewLine
            sSql &= ", @dt_ligar = '" & IIf(contribuinte.Dt_ligar = "", "", contribuinte.Dt_ligar) & "'" & vbNewLine
            sSql &= ", @dt_inativo = '" & IIf(contribuinte.Dt_inativo = "", "", contribuinte.Dt_inativo) & "'" & vbNewLine
            sSql &= ", @cod_empresa = " & IIf(contribuinte.Cod_empresa = 0, 0, contribuinte.Cod_empresa) & vbNewLine
            sSql &= ", @cod_cidade = " & IIf(contribuinte.Cod_Cidade = 0, 0, contribuinte.Cod_Cidade) & vbNewLine
            sSql &= ", @cod_bairro = " & IIf(contribuinte.Cod_Bairro = 0, 0, contribuinte.Cod_Bairro) & vbNewLine
            sSql &= ", @cod_operador = " & IIf(contribuinte.Cod_Operador = 0, 0, contribuinte.Cod_Operador) & vbNewLine
            sSql &= ", @tel_desligado = '" & IIf(contribuinte.Tel_desligado = "", "", contribuinte.Tel_desligado) & "'" & vbNewLine
            sSql &= ", @nao_pedir_extra = '" & IIf(contribuinte.Nao_pedir_extra = "", "", contribuinte.Nao_pedir_extra) & "'" & vbNewLine
            sSql &= ", @cod_usuario = " & IIf(contribuinte.Cod_Usuario = 0, 0, contribuinte.Cod_Usuario) & vbNewLine
            sSql &= ", @dt_nc_mp = '" & IIf(contribuinte.DT_NC_MP = "", "", contribuinte.DT_NC_MP) & "'" & vbNewLine
            sSql &= ", @nao_pedir_mensal = '" & IIf(contribuinte.Nao_Pedir_Mensal = "", "", contribuinte.Nao_Pedir_Mensal) & "'" & vbNewLine
            sSql &= ", @dt_reajuste = '" & IIf(contribuinte.DT_Reajuste = "", "", contribuinte.DT_Reajuste) & "'" & vbNewLine
            sSql &= ", @cod_regiao = " & IIf(contribuinte.Cod_Regiao = 0, 0, contribuinte.Cod_Regiao) & "" & vbNewLine
            sSql &= ", @DDD = '" & IIf(contribuinte.DDD = "", "", contribuinte.DDD) & "'" & vbNewLine
            sSql &= ", @ClienteOi = '" & IIf(contribuinte.ClienteOi = "", "", contribuinte.ClienteOi) & "'"


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            'MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function


    Public Function Excluir(ByVal CodCliente As String, ByVal DDD As String, ByVal Telefone As String, ByVal motivo As String, ByVal solicitante As String, ByVal observacao As String) As DataSet

        Try
            Dim sSql As String

            If (motivo = "") Then
                motivo = "DESEJA PARAR COM A DOAÇÃO"
            End If
            If (solicitante = "") Then
                solicitante = "VIA TELEFONE"
            End If
            If (observacao = "") Then
                observacao = "--"
            End If


            sSql = ""
            sSql &= "exec controle_db.dbo.EXCLUIR_CLIENTE " & vbNewLine
            sSql &= "  @Cod_Cliente = '" & CodCliente & "'" & vbNewLine
            sSql &= ", @telefone = '" & Telefone & "'"
            sSql &= ", @DDD = '" & DDD & "'"
            sSql &= ", @motivo = '" & motivo & "'"
            sSql &= ", @solicitante = '" & solicitante & "'"
            sSql &= ", @observacao = '" & observacao & "'"

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function Atualiza(ByVal contribuinte As CContribuinte) As Boolean

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.AtualizaContribuinte_SPU " & vbNewLine
            sSql &= "  @CodCliente = " & contribuinte.CodCliente & vbNewLine
            sSql &= ", @Cod_EAN_Cliente = '" & contribuinte.Cod_EAN_Cliente & "'" & vbNewLine
            sSql &= ", @NomeCliente1 = '" & contribuinte.NomeCliente1 & "'" & vbNewLine
            sSql &= ", @NomeCliente2 = '" & contribuinte.NomeCliente2 & "'" & vbNewLine
            sSql &= ", @Operador = '" & contribuinte.Operador & "'" & vbNewLine
            sSql &= ", @cnpj_cpf = '" & contribuinte.CNPJ_CPF & "'" & vbNewLine
            sSql &= ", @ie_ci = '" & contribuinte.IE_CI & "'" & vbNewLine
            sSql &= ", @endereco = '" & contribuinte.Endereco & "'" & vbNewLine
            sSql &= ", @bairro = '" & contribuinte.Bairro & "'" & vbNewLine
            sSql &= ", @cidade = '" & contribuinte.Cidade & "'" & vbNewLine
            sSql &= ", @uf = '" & contribuinte.UF & "'" & vbNewLine
            sSql &= ", @cep = '" & contribuinte.CEP & "'" & vbNewLine
            sSql &= ", @telefone1 = '" & contribuinte.Telefone1 & "'" & vbNewLine
            sSql &= ", @telefone2 = '" & contribuinte.Telefone2 & "'" & vbNewLine
            sSql &= ", @email = '" & contribuinte.Email & "'" & vbNewLine
            sSql &= ", @datacadastro = '" & contribuinte.DataCadastro & "'" & vbNewLine
            sSql &= ", @datanascimento = '" & IIf(contribuinte.DataNascimento = "", "", contribuinte.DataNascimento) & "'" & vbNewLine
            sSql &= ", @cod_categoria = " & contribuinte.Cod_Categoria & vbNewLine
            sSql &= ", @observacao = '" & contribuinte.Observacao & "'" & vbNewLine
            sSql &= ", @referencia = '" & contribuinte.Referencia & "'" & vbNewLine
            sSql &= ", @descontinuado = '" & contribuinte.Descontinuado & "'" & vbNewLine
            sSql &= ", @cod_grupo = " & contribuinte.Cod_grupo & vbNewLine
            sSql &= ", @valor = " & FormatNumber(contribuinte.Valor, 0) & vbNewLine
            sSql &= ", @cod_especie = " & contribuinte.Cod_Especie & vbNewLine
            sSql &= ", @dialimite = '" & contribuinte.DiaLimite & "'" & vbNewLine
            sSql &= ", @dt_ligar = '" & contribuinte.Dt_ligar & "'" & vbNewLine
            sSql &= ", @dt_inativo = '" & contribuinte.Dt_inativo & "'" & vbNewLine
            sSql &= ", @cod_empresa = " & contribuinte.Cod_empresa & vbNewLine
            sSql &= ", @cod_cidade = " & contribuinte.Cod_Cidade & vbNewLine
            sSql &= ", @cod_bairro = " & contribuinte.Cod_Bairro & vbNewLine
            sSql &= ", @cod_operador = " & contribuinte.Cod_Operador & vbNewLine
            sSql &= ", @tel_desligado = '" & contribuinte.Tel_desligado & "'" & vbNewLine
            sSql &= ", @nao_pedir_extra = '" & contribuinte.Nao_pedir_extra & "'" & vbNewLine
            sSql &= ", @cod_usuario = " & contribuinte.Cod_Usuario & vbNewLine
            sSql &= ", @dt_nc_mp = '" & contribuinte.DT_NC_MP & "'" & vbNewLine
            sSql &= ", @nao_pedir_mensal = '" & contribuinte.Nao_Pedir_Mensal & "'" & vbNewLine
            sSql &= ", @dt_reajuste = '" & contribuinte.DT_Reajuste & "'" & vbNewLine
            sSql &= ", @cod_regiao = " & contribuinte.Cod_Regiao & "" & vbNewLine
            sSql &= ", @DDD = '" & Trim(contribuinte.DDD) & "'" & vbNewLine
            sSql &= ", @ClienteOi = '" & contribuinte.ClienteOi & "'"


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function VerificaContribuinte(ByVal contribuinte As CContribuinte) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "SELECT count(*) FROM controle_db..cliente_tb  " & vbNewLine
            sSql &= "WHERE telefone1 = '" & contribuinte.Telefone1 & "' " & vbNewLine
            If contribuinte.DDD <> "" Then
                sSql &= "and DDD = '" & contribuinte.DDD & "' "
            End If


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                If ds.Tables(0).Rows(0).Item(0) >= 1 Then
                    Return True
                Else
                    sSql = ""
                    sSql &= "SELECT count(*) FROM controle_db..LISTA_RETIDA_TMP_tb " & vbNewLine
                    sSql &= "WHERE replace(substring(telefone1,5,10),'-','') = '" & contribuinte.Telefone1 & "' " & vbNewLine
                    If contribuinte.DDD <> "" Then
                        sSql &= "and substring(TELEFONE1,2,2) = '" & contribuinte.DDD & "' "
                    End If

                    ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

                    If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                        If ds.Tables(0).Rows(0).Item(0) >= 1 Then
                            Return True
                        Else
                            Return False
                        End If
                    Else
                        Return False
                    End If
                    Return False
                End If
            Else
                Return False
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function

    Public Function EnviaContribuinteParaCancelamento(ByVal contribuinte As CContribuinte, ByVal operadora As Integer) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "insert into Solicitacao_cancelamento_tb  " & vbNewLine
            sSql &= "(cod_ean_cliente ,telefone  ,cod_opera ,operador  ,dia_solicitacao ,operador_exclusao ,dia_exclusao )  " & vbNewLine
            sSql &= "values('" & contribuinte.Cod_EAN_Cliente & "','" & contribuinte.Telefone1 & "'," & IIf(contribuinte.Cod_opera = "", contribuinte.Cod_Operador, contribuinte.Cod_opera) & ",'" & contribuinte.Operador & "', getdate(), null, null)" & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try


    End Function

    Public Function EnviaContribuinteParaCancelamentoMensal(ByVal contribuinte As CContribuinte) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= "update controle_db..lista_telefonica_tb" & vbNewLine
            sSql &= "set categoria = 5, situacao = 'SUSPENÇÃO', mesSituacao ='" & Format(Date.Now, "yyyyMMdd") & "' " & vbNewLine
            sSql &= "where telefone = '" & contribuinte.Telefone1 & "'"

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            sSql = ""
            sSql &= "update controle_db..cliente_tb" & vbNewLine
            sSql &= "set cod_categoria = 5" & vbNewLine
            sSql &= "where telefone1 ='" & contribuinte.Telefone1 & "'"

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            sSql = ""
            sSql &= "update controle_db.dbo.requisicao_tb" & vbNewLine
            sSql &= "set aceite =  'N', confirmado = 'S'" & vbNewLine
            sSql &= "where atual = 'S' and telefone ='" & contribuinte.Telefone1 & "'"

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)



            Return True



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try


    End Function

    Public Function EnviaContribuinteCemigParaCancelamentoMensal(ByVal contribuinte As CContribuinte) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= "update integracao_cemig_db..lista_telefonica_tb" & vbNewLine
            sSql &= "set categoria = 5, situacao = 'SUSPENÇÃO', mesSituacao ='" & Format(Date.Now, "yyyyMMdd") & "' " & vbNewLine
            sSql &= "where telefone = '" & contribuinte.Telefone1 & "'"

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            sSql = ""
            sSql &= "update integracao_cemig_db..cliente_tb" & vbNewLine
            sSql &= "set cod_categoria = 5" & vbNewLine
            sSql &= "where telefone1 ='" & contribuinte.Telefone1 & "'"

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            sSql = ""
            sSql &= "update integracao_cemig_db.dbo.requisicao_tb" & vbNewLine
            sSql &= "set aceite =  'N', confirmado = 'S'" & vbNewLine
            sSql &= "where atual = 'S' and telefone ='" & contribuinte.Telefone1 & "'"

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
            Return True
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try


    End Function



    Public Function RetornaNovoCodigoCliente(isOi As Boolean) As Integer
        Try
            Dim sSql As String
            Dim ds As DataSet
            Dim cod As Integer

            sSql = ""
            If isOi Then
                sSql &= "SELECT MAX(Cod_cliente) + 1 FROM controle_db..cliente_tb  " & vbNewLine
            Else
                sSql &= "SELECT MAX(Cod_cliente) + 1 FROM integracao_cemig_db..cliente_tb  " & vbNewLine
            End If


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                cod = ds.Tables(0).Rows(0).Item(0)
            Else
                cod = 0
            End If

            Return cod


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function


    Public Function VerificaRemessa() As DataSet
        Try
            Dim sSql As String
            Dim remessa As String = ""

            sSql = ""
            sSql &= "SELECT REPLICATE('0', 5 - LEN(remessa)) + RTrim(remessa) FROM interface_dados_db..SEGA0003_10_processar_tb" & vbNewLine

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function

    Public Function CargaRemessa(ByVal contribuinte As CContribuinte, ByVal mes_ano As String) As Boolean

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec interface_dados_db.dbo.Clientes_processar " & vbNewLine
            sSql &= "  @Cod_Cliente = " & contribuinte.CodCliente & vbNewLine
            sSql &= ", @Mes_ano_ref = '" & mes_ano & "'" & vbNewLine
            sSql &= ", @usuario = '" & contribuinte.Operador & "'" & vbNewLine
            sSql &= ", @valor = '" & contribuinte.Valor & "'"
            sSql &= ", @DDD = '" & contribuinte.DDD & "'"
            sSql &= ", @telefone = '" & contribuinte.Telefone1 & "'"


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function ProcessaRemessa(ByVal usuario As String, ByVal IncluirCriticados As Boolean, ByVal IncluirContestados As Boolean) As DataSet

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "EXEC interface_dados_db.dbo.SEGS00003_SPS " & vbNewLine
            sSql &= " @Layout_id = 3 " & vbNewLine
            sSql &= ", @usuario = '" & usuario & "'"

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function ProcessaRemessaSemCriticados(ByVal usuario As String,
                                                 ByVal layout_id As Integer) As DataSet

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "EXEC interface_dados_db.dbo.Gera_remessa_SPS " & vbNewLine
            sSql &= " @Layout_id =" & layout_id & vbNewLine
            sSql &= ", @usuario = '" & usuario & "'"


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function ExpurgoDados() As Boolean

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "EXEC interface_dados_db.dbo.EXPURGO_DADOS " & vbNewLine

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function valorTotalRemessa() As DataSet

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "select sum (valor_total_registros) from interface_dados_db..SEGA0003_30_processar_tb " & vbNewLine

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregarGridHistoricoOi(ByVal filtro As CContribuinte) As DataSet

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.CarregaHistoricoOi_SPS " & vbNewLine
            If filtro.CodCliente = 0 Then
                sSql &= "@cod_cliente = 0, " & vbNewLine
            Else
                sSql &= "@cod_cliente = " & filtro.CodCliente & ", " & vbNewLine
            End If
            If filtro.Operador = Nothing Then
                sSql &= "@operadora_telefonica = 0" & vbNewLine
            Else
                sSql &= "@operadora_telefonica = " & filtro.Operador
            End If



            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function ConsultaCodCliente(ByVal cod As String) As DataSet
        Dim ds As DataSet = New DataSet
        Dim Nome As String = ""
        Try

            Dim sSql As String = " SELECT * from controle_db.dbo.cliente_tb" _
                               + " where cod_ean_cliente = '" & cod & "'"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    Return ds
                Else
                    Return Nothing
                End If
            Else

                Return Nothing
            End If



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function ConsultaCodClienteTabelaRetido(ByVal cod As String) As DataSet
        Dim ds As DataSet = New DataSet
        Dim Nome As String = ""
        Try

            Dim sSql As String = " SELECT * from controle_db.dbo.LISTA_RETIDA_TMP_TB" _
                               + " where cod_ean_cliente = '" & cod & "'"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    Return ds
                Else
                    Return Nothing
                End If
            Else

                Return Nothing
            End If



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function ConsultaContribuintePorCategoria(ByVal cod_categoria As Integer) As DataSet
        Dim ds As DataSet = New DataSet
        Dim Nome As String = ""
        Try

            Dim sSql As String = " SELECT * from controle_db..cliente_tb" _
                               + " where cod_categoria = " & cod_categoria


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    Return ds
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function ConsultaContribuintePorCidade(ByVal cidade As String) As DataSet
        Dim ds As DataSet = New DataSet
        Dim Nome As String = ""
        Try

            Dim sSql As String = " SELECT * from controle_db..cliente_tb" _
                               + " where cidade = '" & cidade & "'"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    Return ds
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function ConsultaRegistroRequisicaoOi(ByVal telefone As String, ByVal mesRef As String) As DataSet
        Dim ds As DataSet = New DataSet
        Dim Nome As String = ""
        Try

            Dim sSql As String = " SELECT * from controle_db..requisicao_oi_tb" _
                               + " where telefone = '" & telefone & "'" _
                               + " and mesRef = '" & mesRef & "'"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    Return ds
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function ConsultaRegistroDadosRequisicaoOi(ByVal telefone As String, ByVal mesRef As String) As DataSet
        Dim ds As DataSet = New DataSet
        Dim Nome As String = ""
        Try

            Dim sSql As String = " SELECT top 1 * from controle_db..requisicao_oi_tb" _
                               + " where telefone = '" & telefone & "'" _
                               + " order by dt_inclusao desc "



            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    Return ds
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function Inclui_Numeros_Oi(ByVal filtro As CContribuinte, ByVal aceite As String, ByVal mesAtuacao As String, ByVal mesRef As String, ByVal nome As String) As Boolean

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.Inclui_Numeros_Oi " & vbNewLine
            sSql &= "@DDD = '" & filtro.DDD & "'," & vbNewLine
            sSql &= "@numero = '" & filtro.Telefone1 & "', " & vbNewLine
            sSql &= "@valor = " & Replace(CDec(filtro.Valor), ",", ".") & ", " & vbNewLine
            sSql &= "@cod_cliente = '" & filtro.Cod_EAN_Cliente & "'," & vbNewLine
            sSql &= "@operador_id = '" & filtro.Cod_Operador & "'," & vbNewLine
            sSql &= "@operador = '" & filtro.Operador & "'," & vbNewLine
            sSql &= "@aceite = '" & aceite & "'," & vbNewLine
            sSql &= "@categoria = '" & filtro.Cod_Categoria & "'," & vbNewLine
            sSql &= "@mesAtuacao = '" & mesAtuacao & "'," & vbNewLine
            sSql &= "@mesRef = '" & mesRef & "'," & vbNewLine
            sSql &= "@nome = '" & filtro.NomeCliente1 & "'"



            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function Inclui_Numeros_Requisicao_Comercial_Nao_Contribuiu(filtro As CContribuinte,
                                                                       aceite As String,
                                                                       mesAtuacao As String,
                                                                       mesRef As String) As Boolean

        Try
            Dim sSql As String
            sSql = ""
            sSql &= "exec controle_db.dbo.Inclui_Comercial_Nao_Contribuinte " & vbNewLine
            sSql &= "@DDD = '" & filtro.DDD & "'," & vbNewLine
            sSql &= "@numero = '" & filtro.Telefone1 & "', " & vbNewLine
            sSql &= "@valor = " & Replace(CDec(filtro.Valor), ",", ".") & ", " & vbNewLine
            sSql &= "@cod_cliente = '" & filtro.Cod_EAN_Cliente & "'," & vbNewLine
            sSql &= "@operador_id = '" & filtro.Cod_Operador & "'," & vbNewLine
            sSql &= "@operador = '" & filtro.Operador & "'," & vbNewLine
            sSql &= "@aceite = '" & aceite & "'," & vbNewLine
            sSql &= "@categoria = '" & filtro.Cod_Categoria & "'," & vbNewLine
            sSql &= "@mesAtuacao = '" & mesAtuacao & "'," & vbNewLine
            sSql &= "@mesRef = '" & mesRef & "'," & vbNewLine
            sSql &= "@nome = '" & filtro.NomeCliente1 & "'"

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function Inclui_Numeros_Cemig(filtro As CContribuinte, aceite As String, mesAtuacao As String, mesRef As String, numeroInstalacao As String) As Boolean

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec integracao_cemig_db.dbo.Inclui_Numeros_Cemig " & vbNewLine
            sSql &= "@DDD = '" & filtro.DDD & "'," & vbNewLine
            sSql &= "@numero = '" & filtro.Telefone1 & "', " & vbNewLine
            sSql &= "@valor = " & Replace(CDec(filtro.Valor), ",", ".") & ", " & vbNewLine
            sSql &= "@cod_cliente = '" & filtro.Cod_EAN_Cliente & "'," & vbNewLine
            sSql &= "@operador_id = '" & filtro.Cod_Operador & "'," & vbNewLine
            sSql &= "@operador = '" & filtro.Operador & "'," & vbNewLine
            sSql &= "@aceite = '" & aceite & "'," & vbNewLine
            sSql &= "@categoria = '" & filtro.Cod_Categoria & "'," & vbNewLine
            sSql &= "@mesAtuacao = '" & mesAtuacao & "'," & vbNewLine
            sSql &= "@mesRef = '" & mesRef & "'," & vbNewLine
            sSql &= "@nome = '" & filtro.NomeCliente1 & "',"
            sSql &= "@numeroInstalacao = '" & numeroInstalacao & "'"
            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
            Return True
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function AnalisaInclusaoRegistroOiRepetido(ByVal filtro As CContribuinte) As DataSet

        Try
            Dim ds As DataSet = New DataSet
            Dim sSql As String

            sSql = ""

            sSql &= "select top 1 * from controle_db.dbo.requisicao_oi_tb" & vbNewLine
            sSql &= " where telefone = '" & filtro.Telefone1 & "'" & vbNewLine
            sSql &= "order by dt_inclusao desc"

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    Return ds
                Else
                    Return ds
                End If
            Else
                Return ds
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function AnalisaInclusaoRegistroOi(ByVal filtro As CContribuinte) As DataSet

        Try
            Dim ds As DataSet = New DataSet
            Dim sSql As String

            sSql = ""

            sSql &= "DECLARE @DATA DATETIME, @primeirodia smalldatetime, @ultimodiames DATETIME " & vbNewLine
            sSql &= "SET @DATA = GETDATE()   " & vbNewLine
            sSql &= "SELECT @primeirodia = CONVERT(VARCHAR, @DATA - DAY(@DATA) + 1, 103) " & vbNewLine
            sSql &= "SELECT @primeirodia  = cast(convert(char(8),@primeirodia ,112) as date) " & vbNewLine
            sSql &= "SELECT @ultimodiames = DATEADD(d, -DAY(GETDATE()),DATEADD(m,1,GETDATE())) " & vbNewLine
            sSql &= "select @ultimodiames = cast(convert(char(8),@ultimodiames ,112) as date)" & vbNewLine

            sSql &= "select top 1 * from controle_db..requisicao_oi_tb" & vbNewLine
            sSql &= " where telefone = '" & filtro.Telefone1 & "'" & vbNewLine
            sSql &= " and ddd = '" & filtro.DDD & "'" & vbNewLine
            sSql &= "and convert(date,dt_inclusao) between @primeirodia + 6 and @ultimodiames + 6" & vbNewLine
            sSql &= "order by dt_inclusao desc"

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    Return ds
                Else
                    Return ds
                End If
            Else
                Return ds
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function



    Public Function Analisar_Aceite_Oi(ByVal filtro As CContribuinte, ByVal valor As Decimal) As DataSet

        Try
            Dim ds As DataSet = New DataSet
            Dim sSql As String


            sSql = ""
            sSql &= "declare @valor decimal(15,2) " & vbNewLine
            sSql &= "declare @media decimal(15,2) " & vbNewLine
            sSql &= "" & vbNewLine
            sSql &= "set @valor =" & Convert.ToInt64(valor) & vbNewLine
            sSql &= " select @media = AVG(valor ) from controle_db.dbo.requisicao_oi_tb " & vbNewLine
            sSql &= " where telefone = '" & filtro.Telefone1 & "' " & vbNewLine
            sSql &= "select @media as media" & vbNewLine
            sSql &= "select ABS(cast(((@media /@valor)*100)-100 as decimal(15,2))) as diferenca" & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                Return ds
            Else
                Return Nothing
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function Analisar_Aceite_Cemig(filtro As CContribuinte, valor As Decimal) As DataSet

        Try
            Dim ds As DataSet = New DataSet
            Dim sSql As String


            sSql = ""
            sSql &= "declare @valor decimal(15,2) " & vbNewLine
            sSql &= "declare @media decimal(15,2) " & vbNewLine
            sSql &= "" & vbNewLine
            sSql &= "set @valor =" & Convert.ToInt64(valor) & vbNewLine
            sSql &= " select @media = AVG(valor ) from integracao_cemig_db.dbo.requisicao_cemig_tb " & vbNewLine
            sSql &= " where telefone = '" & filtro.Telefone1 & "' " & vbNewLine
            sSql &= "select @media as media" & vbNewLine
            sSql &= "select ABS(cast(((@media /@valor)*100)-100 as decimal(15,2))) as diferenca" & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                Return ds
            Else
                Return Nothing
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function Atualiza_Informacoes_Oi(ByVal filtro As CContribuinte, ByVal mesAtuacao As String, ByVal mesRef As String, ByVal cpf As String, ByVal autorizante As String, ByVal observacao As String, ByVal valorTotal As String) As Boolean

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.Atualiza_Numeros_Oi " & vbNewLine
            sSql &= "@DDD = '" & filtro.DDD & "'," & vbNewLine
            sSql &= "@numero = '" & filtro.Telefone1 & "', " & vbNewLine
            sSql &= "@cod_cliente = '" & filtro.Cod_EAN_Cliente & "'," & vbNewLine
            sSql &= "@operador_id = " & filtro.Cod_Operador & "," & vbNewLine
            sSql &= "@mesAtuacao = '" & mesAtuacao & "'," & vbNewLine
            sSql &= "@mesRef = '" & mesRef & "'," & vbNewLine
            sSql &= "@cpf = '" & cpf & "'," & vbNewLine
            sSql &= "@autorizante = '" & autorizante & "'," & vbNewLine
            sSql &= "@obs = '" & observacao & "',"
            sSql &= "@nao_conforme = '" & filtro.Nao_Conforme & "',"
            sSql &= "@valor_total = '" & valorTotal & "'"


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function Atualiza_Informacoes_Cemig(ByVal filtro As CContribuinte, ByVal mesAtuacao As String, ByVal mesRef As String, ByVal cpf As String, ByVal autorizante As String, ByVal observacao As String, ByVal valorTotal As String) As Boolean

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec integracao_cemig_db.dbo.Atualiza_Numeros_Cemig " & vbNewLine
            sSql &= "@DDD = '" & filtro.DDD & "'," & vbNewLine
            sSql &= "@numero = '" & filtro.Telefone1 & "', " & vbNewLine
            sSql &= "@cod_cliente = '" & filtro.Cod_EAN_Cliente & "'," & vbNewLine
            sSql &= "@operador_id = " & filtro.Cod_Operador & "," & vbNewLine
            sSql &= "@mesAtuacao = '" & mesAtuacao & "'," & vbNewLine
            sSql &= "@mesRef = '" & mesRef & "'," & vbNewLine
            sSql &= "@cpf = '" & cpf & "'," & vbNewLine
            sSql &= "@autorizante = '" & autorizante & "'," & vbNewLine
            sSql &= "@obs = '" & observacao & "',"
            sSql &= "@nao_conforme = '" & filtro.Nao_Conforme & "',"
            sSql &= "@valor_total = '" & valorTotal & "'"


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function Atualiza_Requisicao_Recuperacao(ByVal filtro As CContribuinte, ByVal aceite As String) As Boolean

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "update controle_db.dbo.requisicao_tb " & vbNewLine
            sSql &= "set confirmado = 'S', aceite = '" & aceite & "'" & vbNewLine
            sSql &= "where telefone = '" & filtro.Telefone1 & "'" & vbNewLine
            sSql &= "and DDD = '" & filtro.DDD & "'" & vbNewLine
            sSql &= "and Atual = 'D' "

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function Atualiza_UsuarioRegistro(ByVal filtro As CContribuinte, ByVal mesAtuacao As String, ByVal mesRef As String, ByVal cpf As String, ByVal autorizante As String, ByVal observacao As String) As Boolean

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.Atualiza_Usuario_Oi " & vbNewLine
            sSql &= "@DDD = '" & filtro.DDD & "'," & vbNewLine
            sSql &= "@numero = '" & filtro.Telefone1 & "', " & vbNewLine
            sSql &= "@cod_cliente = '" & filtro.Cod_EAN_Cliente & "'," & vbNewLine
            sSql &= "@operador_id = " & filtro.Cod_Operador & "," & vbNewLine
            sSql &= "@mesAtuacao = '" & mesAtuacao & "'," & vbNewLine
            sSql &= "@mesRef = '" & mesRef & "'," & vbNewLine
            sSql &= "@cpf = '" & cpf & "'," & vbNewLine
            sSql &= "@autorizante = '" & autorizante & "'," & vbNewLine
            sSql &= "@obs = '" & observacao & "',"
            sSql &= "@nao_conforme = '" & filtro.Nao_Conforme & "'"


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function



    Public Function Excluir_Numeros_Oi(ByVal filtro As CContribuinte) As Boolean

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.Excluir_Numeros_Oi " & vbNewLine
            sSql &= "@DDD = '" & filtro.DDD & "'," & vbNewLine
            sSql &= "@numero = '" & filtro.Telefone1 & "', " & vbNewLine
            sSql &= "@valor = " & filtro.Valor & ", " & vbNewLine
            sSql &= "@cod_cliente = '" & filtro.CodCliente & "'"


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try




    End Function

    Public Function Excluir_Requisicao_Oi(ByVal filtro As CContribuinte, ByVal mesRef As String) As Boolean

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.Excluir_requisicao_Oi " & vbNewLine
            sSql &= "@DDD = '" & filtro.DDD & "'," & vbNewLine
            sSql &= "@numero = '" & filtro.Telefone1 & "', " & vbNewLine
            sSql &= "@valor = " & Replace(filtro.Valor, ",", ".") & ", " & vbNewLine
            sSql &= "@cod_cliente = '" & filtro.CodCliente & "', "
            sSql &= "@mesRef = '" & mesRef & "'"


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            Return False
        End Try




    End Function

    Public Function AtualizaTabelaCliente() As Boolean

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.ATUALIZA_TABELA_CLIENTE " & vbNewLine

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function AtualizaRequisicaoOi(ByVal filtro As CContribuinte, ByVal aceite As String, ByVal mesAtuacao As String, ByVal mesRef As String, ByVal nome As String, ByVal requisicao_id As Integer) As Boolean

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.Atualiza_Requisicao " & vbNewLine
            sSql &= "@DDD = '" & filtro.DDD & "'," & vbNewLine
            sSql &= "@numero = '" & filtro.Telefone1 & "', " & vbNewLine
            sSql &= "@valor = " & Replace(CDec(filtro.Valor), ",", ".") & ", " & vbNewLine
            sSql &= "@cod_cliente = '" & filtro.Cod_EAN_Cliente & "'," & vbNewLine
            sSql &= "@operador_id = '" & filtro.Cod_Operador & "'," & vbNewLine
            sSql &= "@operador = '" & filtro.Operador & "'," & vbNewLine
            sSql &= "@aceite = '" & aceite & "'," & vbNewLine
            sSql &= "@categoria = '" & filtro.Cod_Categoria & "'," & vbNewLine
            sSql &= "@mesAtuacao = '" & mesAtuacao & "'," & vbNewLine
            sSql &= "@mesRef = '" & mesRef & "'," & vbNewLine
            sSql &= "@nome = '" & nome & "',"
            sSql &= "@requisicao_id = " & requisicao_id


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function IncluiTelefoneNaoContribuiuComercial(ByVal filtro As CContribuinte, ByVal aceite As String, ByVal mesAtuacao As String, ByVal mesRef As String, ByVal nome As String, ByVal requisicao_id As Integer) As Boolean

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.Atualiza_Requisicao " & vbNewLine
            sSql &= "@DDD = '" & filtro.DDD & "'," & vbNewLine
            sSql &= "@numero = '" & filtro.Telefone1 & "', " & vbNewLine
            sSql &= "@valor = " & Replace(CDec(filtro.Valor), ",", ".") & ", " & vbNewLine
            sSql &= "@cod_cliente = '" & filtro.Cod_EAN_Cliente & "'," & vbNewLine
            sSql &= "@operador_id = '" & filtro.Cod_Operador & "'," & vbNewLine
            sSql &= "@operador = '" & filtro.Operador & "'," & vbNewLine
            sSql &= "@aceite = '" & aceite & "'," & vbNewLine
            sSql &= "@categoria = '" & filtro.Cod_Categoria & "'," & vbNewLine
            sSql &= "@mesAtuacao = '" & mesAtuacao & "'," & vbNewLine
            sSql &= "@mesRef = '" & mesRef & "'," & vbNewLine
            sSql &= "@nome = '" & nome & "',"
            sSql &= "@requisicao_id = " & requisicao_id


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function AtualizaRequisicaoCemig(ByVal filtro As CContribuinte, ByVal aceite As String, ByVal mesAtuacao As String, ByVal mesRef As String, ByVal nome As String, ByVal requisicao_id As Integer) As Boolean

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec integracao_cemig_db.dbo.Atualiza_Requisicao " & vbNewLine
            sSql &= "@DDD = '" & filtro.DDD & "'," & vbNewLine
            sSql &= "@numero = '" & filtro.Telefone1 & "', " & vbNewLine
            sSql &= "@valor = " & Replace(CDec(filtro.Valor), ",", ".") & ", " & vbNewLine
            sSql &= "@cod_cliente = '" & filtro.Cod_EAN_Cliente & "'," & vbNewLine
            sSql &= "@operador_id = '" & filtro.Cod_Operador & "'," & vbNewLine
            sSql &= "@operador = '" & filtro.Operador & "'," & vbNewLine
            sSql &= "@aceite = '" & aceite & "'," & vbNewLine
            sSql &= "@categoria = '" & filtro.Cod_Categoria & "'," & vbNewLine
            sSql &= "@mesAtuacao = '" & mesAtuacao & "'," & vbNewLine
            sSql &= "@mesRef = '" & mesRef & "'," & vbNewLine
            sSql &= "@nome = '" & nome & "',"
            sSql &= "@requisicao_id = " & requisicao_id


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function CarregaListaTelefoneExclusao() As DataSet

        Try

            Dim sSql As String = "SELECT DDD, Telefone " _
                             + " FROM controle_db..telefone_exclusao with (nolock) "


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RodarExclusao(ByVal contribuinte As CContribuinte) As DataSet

        Try
            Dim sSql As String

            sSql = ""

            sSql &= "declare" & vbNewLine
            sSql &= "@COD_CLIENTE VARCHAR(20),        " & vbNewLine
            sSql &= "@TELEFONE VARCHAR(20)        " & vbNewLine
            sSql &= "" & vbNewLine
            sSql &= "set @TELEFONE = '" & contribuinte.Telefone1 & "'" & vbNewLine
            sSql &= "select @COD_CLIENTE = cod_ean_cliente " & vbNewLine
            sSql &= "from controle_db..cliente_tb " & vbNewLine
            sSql &= "where telefone1 = @TELEFONE " & vbNewLine
            sSql &= " " & vbNewLine
            sSql &= "delete from controle_db..cliente_tb  " & vbNewLine
            sSql &= "WHERE cod_ean_cliente = @COD_CLIENTE " & vbNewLine
            sSql &= " " & vbNewLine
            sSql &= "delete from controle_db..cliente_tb " & vbNewLine
            sSql &= "WHERE telefone1 = @TELEFONE " & vbNewLine
            sSql &= " " & vbNewLine

            sSql &= "update controle_db..requisicao_oi_tb " & vbNewLine
            sSql &= "set hist_aceite = aceite " & vbNewLine
            sSql &= "WHERE telefone = @TELEFONE " & vbNewLine
            sSql &= "" & vbNewLine
            sSql &= "update controle_db..requisicao_oi_tb " & vbNewLine
            sSql &= "set hist_aceite = aceite  " & vbNewLine
            sSql &= "WHERE cod_ean = @COD_CLIENTE  " & vbNewLine

            sSql &= "" & vbNewLine
            sSql &= "update controle_db..requisicao_oi_tb " & vbNewLine
            sSql &= "set aceite = 'C' " & vbNewLine
            sSql &= "WHERE cod_ean = @COD_CLIENTE " & vbNewLine
            sSql &= "and aceite <> 'C' " & vbNewLine

            sSql &= "" & vbNewLine
            sSql &= "update controle_db..requisicao_oi_tb " & vbNewLine
            sSql &= "set aceite = 'C' " & vbNewLine
            sSql &= "WHERE telefone = @TELEFONE " & vbNewLine
            sSql &= "and aceite <> 'C' " & vbNewLine
            sSql &= " " & vbNewLine
            sSql &= "DELETE FROM TEMP_DB..TelefoneOI " & vbNewLine
            sSql &= "WHERE COD_CLIENTE = @COD_CLIENTE " & vbNewLine
            sSql &= "AND NUMERO = @TELEFONE " & vbNewLine
            sSql &= " " & vbNewLine
            sSql &= "DELETE FROM controle_db..Lista_Telefonica_tb " & vbNewLine
            sSql &= "WHERE telefone = @TELEFONE " & vbNewLine
            sSql &= " " & vbNewLine
            sSql &= "DELETE FROM controle_db..requisicao_tb " & vbNewLine
            sSql &= "WHERE telefone = @TELEFONE " & vbNewLine
            sSql &= " " & vbNewLine
            sSql &= "DELETE FROM controle_db..requisicao_lista_tb " & vbNewLine
            sSql &= "WHERE cod_ean_cliente = @COD_CLIENTE " & vbNewLine
            sSql &= " " & vbNewLine
            sSql &= "delete from controle_db..LISTA_RETIDA_TMP_TB " & vbNewLine
            sSql &= "WHERE cod_ean_cliente = @COD_CLIENTE " & vbNewLine
            sSql &= " " & vbNewLine
            sSql &= "delete from controle_db..LISTA_RETIDA_TMP_TB " & vbNewLine
            sSql &= "WHERE SUBSTRING(replace(telefone1,'-',''),5,12) = @TELEFONE     " & vbNewLine
            sSql &= " " & vbNewLine


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


#End Region

#Region "Lista Nova"

    Public Function CarregaListaNova(ByVal cidade As String, ByVal cod As Integer) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "select cod, nome_cliente, endereco, bairro, cidade, " & vbNewLine
            sSql &= "uf, cep, telefone1, cod_empresa, cod_cidade, cod_bairro,cod_regiao " & vbNewLine
            sSql &= "from controle_db.dbo." & cidade & vbNewLine
            sSql &= " where dt_inclusao_cadastro is null " & vbNewLine
            If cod > 0 Then
                sSql &= " and cod =" & cod
            End If

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function AtualizaFichaListaNova(ByVal cidade As String, ByVal cod As Integer) As Boolean
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "update controle_db.dbo." & cidade & vbNewLine
            sSql &= " set dt_inclusao_cadastro =  getdate()" & vbNewLine
            sSql &= " where cod =" & cod

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function RetornaNomeContribuinte(ByVal filtro As CContribuinte) As String

        Try
            Dim ds As DataSet = New DataSet
            Dim nome As String = ""
            Dim sSql As String

            sSql = ""

            sSql &= "select nome_cliente from controle_db.dbo.requisicao_tb" & vbNewLine
            sSql &= " where telefone = '" & filtro.Telefone1 & "'" & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    nome = ds.Tables(0).Rows.Item(0).Item(0).ToString()
                Else
                    nome = ""
                End If
            Else
                Return nome
            End If

            Return nome

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


#End Region

#Region "Contato Marketing"

    Public Function Inclui_Contatos(ByVal filtro As CContribuinte, ByVal mesRef As String, ByVal email As String, ByVal WhatsApp As String) As DataSet

        Try
            Dim sSql As String

            sSql = ""

            sSql &= "INSERT INTO CONTROLE_dB.DBO.contato_marketing_tb (Cod_EAN_Cliente, ddd, telefone, Valor, mes_ref, email, WhatsApp)" & vbNewLine
            sSql &= "Values ('" & filtro.Cod_EAN_Cliente & "','" & filtro.DDD & "','" & filtro.Telefone1 & "','" & filtro.Valor & "','" & mesRef & "','" & email & "','" & WhatsApp & "')"

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaEmailsEnviar() As DataSet

        Try
            Dim sSql As String

            sSql = ""

            sSql &= "Select email from controle_db.dbo.contato_marketing_tb where processado is null " & vbNewLine

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaDadosMarketing(ByVal ddd As String, ByVal telefone As String) As DataSet

        Try
            Dim sSql As String

            sSql = ""

            sSql &= "Select top 1 email, whatsapp from controle_db.dbo.contato_marketing_tb where ddd = '" & ddd & "' and telefone = '" & telefone & "' order by contato_marketing_id desc" & vbNewLine

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

#End Region

#Region "Cemig"
    Public Function PesquisarPorCodEANCemig(ByVal cod_ean As String) As DataSet
        Try
            Dim ds As DataSet
            Dim sSql As String = "SELECT * " _
                             + " FROM integracao_cemig_db..cliente_tb with (nolock)" _
                             + " WHERE " _
                             + " COD_EAN_CLIENTE = '" + cod_ean + "'"
            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
            Return ds
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function PesquisarPorTelefoneCemig(ByVal telefone As String, ByVal ddd As String, ByVal cod_ean As String, ByVal opcao As Integer) As DataSet
        Try
            Dim sSql As String
            If telefone.Substring(0, 1) = "(" Then

                sSql = " SELECT " _
                            + "cliente_id, cod_cliente, Cod_EAN_Cliente, nome_cliente, nome_cliente2, Operador, CNPJ_CPF, IE_CI, Endereco, Bairro, Cidade, UF, CEP" _
                            + ",substring(replace(telefone1,'-',''),5,12) as telefone1" _
                            + ",telefone2,email,data_cadastro,data_nascimento,cod_categoria,observacao,referencia,descontinuado,cod_grupo,valor,cod_especie" _
                            + ",dia_limite,data_ligar,data_inativo,cod_empresa,cod_cidade,cod_bairro,cod_operador,telefone_deslig,nao_pedir_extra,cod_usuario" _
                            + ",data_nc_mp,nao_pedir_mensal,data_reajuste,cod_regiao,dt_inclusao,dt_alteracao,dt_contratacao" _
                            + ",substring(telefone1,2,2) as DDD" _
                            + ",clienteOi,SituacaoOi,CategoriaOi" _
                           + " FROM integracao_cemig_db..cliente_tb with (nolock) " _
                           + " WHERE "
                If opcao = 1 Then
                    sSql = sSql + "telefone1 = '" + telefone + "'"
                    If cod_ean <> "" Then
                        sSql = sSql + "and cod_ean_cliente = '" + cod_ean + "'"
                    End If
                    If ddd <> "" Then
                        sSql = sSql + "and DDD = '" + ddd + "'"
                    End If
                Else
                    sSql = sSql + "telefone2 = '" + telefone + "'"
                    If cod_ean <> "" Then
                        sSql = sSql + "and cod_ean_cliente = '" + cod_ean + "'"
                    End If
                    If ddd <> "" Then
                        sSql = sSql + "and DDD = '" + ddd + "'"
                    End If
                End If

            Else

                sSql = " SELECT * " _
                                 + " FROM integracao_cemig_db..cliente_tb with (nolock) " _
                                 + " WHERE "
                If opcao = 1 Then
                    sSql = sSql + "telefone1 = '" + telefone + "'"
                    If cod_ean <> "" Then
                        sSql = sSql + "and cod_ean_cliente = '" + cod_ean + "'"
                    End If
                    If ddd <> "" Then
                        sSql = sSql + "and DDD = '" + ddd + "'"
                    End If
                Else
                    sSql = sSql + "telefone2 = '" + telefone + "'"
                    If cod_ean <> "" Then
                        sSql = sSql + "and cod_ean_cliente = '" + cod_ean + "'"
                    End If
                    If ddd <> "" Then
                        sSql = sSql + "and DDD = '" + ddd + "'"
                    End If

                End If
            End If

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function Busca_Lista_TelefonicaCemig(ByVal telefone As String, ByVal ddd As String, ByVal cod_ean As String) As DataSet
        Try
            Dim sSql As String

            sSql = ""
            sSql &= "SELECT cod_ean,DDD,telefone,valor,nome_cliente,categoria,mesSituacao,Situacao,dt_inclusao,dt_alteracao,usuario,cod_cidade" & vbNewLine
            sSql &= "FROM integracao_cemig_db..lista_telefonica_tb" & vbNewLine
            sSql &= " where telefone = '" & telefone & "'"
            If cod_ean <> "" Then
                sSql &= " and cod_ean = '" & cod_ean & "' " & vbNewLine
            End If
            If ddd <> "" Then
                sSql &= " and  ddd = '" & ddd & "'" & vbNewLine
            End If

            sSql &= "UNION " & vbNewLine
            sSql &= "SELECT cod_ean,DDD,telefone,valor,nome_cliente,categoria,mesSituacao,Situacao,dt_inclusao,dt_alteracao,usuario,cod_cidade" & vbNewLine
            sSql &= "FROM integracao_cemig_db..lista_telefonica_tb" & vbNewLine
            sSql &= " where telefone = '" & telefone & "'"
            If ddd <> "" Then
                sSql &= " and  ddd = '" & ddd & "'"
            End If

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaNomeContribuinteCemig(filtro As CContribuinte) As String

        Try
            Dim ds As DataSet = New DataSet
            Dim nome As String = ""
            Dim sSql As String

            sSql = ""

            sSql &= "select nome_cliente from integracao_cemig_db.dbo.requisicao_tb" & vbNewLine
            sSql &= " where telefone = '" & filtro.Telefone1 & "'" & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    nome = ds.Tables(0).Rows.Item(0).Item(0).ToString()
                Else
                    nome = ""
                End If
            Else
                Return nome
            End If

            Return nome

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function AnalisaInclusaoRegistroCemig(filtro As CContribuinte) As DataSet

        Try
            Dim ds As DataSet = New DataSet
            Dim sSql As String

            sSql = ""

            sSql &= "DECLARE @DATA DATETIME, @primeirodia smalldatetime, @ultimodiames DATETIME " & vbNewLine
            sSql &= "SET @DATA = GETDATE()   " & vbNewLine
            sSql &= "SELECT @primeirodia = CONVERT(VARCHAR, @DATA - DAY(@DATA) + 1, 103) " & vbNewLine
            sSql &= "SELECT @primeirodia  = cast(convert(char(8),@primeirodia ,112) as date) " & vbNewLine
            sSql &= "SELECT @ultimodiames = DATEADD(d, -DAY(GETDATE()),DATEADD(m,1,GETDATE())) " & vbNewLine
            sSql &= "select @ultimodiames = cast(convert(char(8),@ultimodiames ,112) as date)" & vbNewLine

            sSql &= "select top 1 * from integracao_cemig_db..requisicao_cemig_tb" & vbNewLine
            sSql &= " where telefone = '" & filtro.Telefone1 & "'" & vbNewLine
            sSql &= " and ddd = '" & filtro.DDD & "'" & vbNewLine
            sSql &= "and convert(date,dt_inclusao) between @primeirodia + 6 and @ultimodiames + 6" & vbNewLine
            sSql &= "order by dt_inclusao desc"

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    Return ds
                Else
                    Return ds
                End If
            Else
                Return ds
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function AtualizaCemig(ByVal contribuinte As CContribuinte) As Boolean

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec integracao_cemig_db.dbo.AtualizaContribuinte_SPU " & vbNewLine
            sSql &= "  @CodCliente = " & contribuinte.CodCliente & vbNewLine
            sSql &= ", @Cod_EAN_Cliente = '" & contribuinte.Cod_EAN_Cliente & "'" & vbNewLine
            sSql &= ", @NomeCliente1 = '" & contribuinte.NomeCliente1 & "'" & vbNewLine
            sSql &= ", @NomeCliente2 = '" & contribuinte.NomeCliente2 & "'" & vbNewLine
            sSql &= ", @Operador = '" & contribuinte.Operador & "'" & vbNewLine
            sSql &= ", @cnpj_cpf = '" & contribuinte.CNPJ_CPF & "'" & vbNewLine
            sSql &= ", @ie_ci = '" & contribuinte.IE_CI & "'" & vbNewLine
            sSql &= ", @endereco = '" & contribuinte.Endereco & "'" & vbNewLine
            sSql &= ", @bairro = '" & contribuinte.Bairro & "'" & vbNewLine
            sSql &= ", @cidade = '" & contribuinte.Cidade & "'" & vbNewLine
            sSql &= ", @uf = '" & contribuinte.UF & "'" & vbNewLine
            sSql &= ", @cep = '" & contribuinte.CEP & "'" & vbNewLine
            sSql &= ", @telefone1 = '" & contribuinte.Telefone1 & "'" & vbNewLine
            sSql &= ", @telefone2 = '" & contribuinte.Telefone2 & "'" & vbNewLine
            sSql &= ", @email = '" & contribuinte.Email & "'" & vbNewLine
            sSql &= ", @datacadastro = '" & contribuinte.DataCadastro & "'" & vbNewLine
            sSql &= ", @datanascimento = '" & IIf(contribuinte.DataNascimento = "", "", contribuinte.DataNascimento) & "'" & vbNewLine
            sSql &= ", @cod_categoria = " & contribuinte.Cod_Categoria & vbNewLine
            sSql &= ", @observacao = '" & contribuinte.Observacao & "'" & vbNewLine
            sSql &= ", @referencia = '" & contribuinte.Referencia & "'" & vbNewLine
            sSql &= ", @descontinuado = '" & contribuinte.Descontinuado & "'" & vbNewLine
            sSql &= ", @cod_grupo = " & contribuinte.Cod_grupo & vbNewLine
            sSql &= ", @valor = " & FormatNumber(contribuinte.Valor, 0) & vbNewLine
            sSql &= ", @cod_especie = " & contribuinte.Cod_Especie & vbNewLine
            sSql &= ", @dialimite = '" & contribuinte.DiaLimite & "'" & vbNewLine
            sSql &= ", @dt_ligar = '" & contribuinte.Dt_ligar & "'" & vbNewLine
            sSql &= ", @dt_inativo = '" & contribuinte.Dt_inativo & "'" & vbNewLine
            sSql &= ", @cod_empresa = " & contribuinte.Cod_empresa & vbNewLine
            sSql &= ", @cod_cidade = " & contribuinte.Cod_Cidade & vbNewLine
            sSql &= ", @cod_bairro = " & contribuinte.Cod_Bairro & vbNewLine
            sSql &= ", @cod_operador = " & contribuinte.Cod_Operador & vbNewLine
            sSql &= ", @tel_desligado = '" & contribuinte.Tel_desligado & "'" & vbNewLine
            sSql &= ", @nao_pedir_extra = '" & contribuinte.Nao_pedir_extra & "'" & vbNewLine
            sSql &= ", @cod_usuario = " & contribuinte.Cod_Usuario & vbNewLine
            sSql &= ", @dt_nc_mp = '" & contribuinte.DT_NC_MP & "'" & vbNewLine
            sSql &= ", @nao_pedir_mensal = '" & contribuinte.Nao_Pedir_Mensal & "'" & vbNewLine
            sSql &= ", @dt_reajuste = '" & contribuinte.DT_Reajuste & "'" & vbNewLine
            sSql &= ", @cod_regiao = " & contribuinte.Cod_Regiao & "" & vbNewLine
            sSql &= ", @DDD = '" & Trim(contribuinte.DDD) & "'" & vbNewLine
            sSql &= ", @ClienteOi = '" & contribuinte.ClienteOi & "'"
            sSql &= ", @num_instalacao = '" & contribuinte.Numero_Instalacao & "'"

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function VerificaContribuinteCemig(contribuinte As CContribuinte) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            sSql &= "SELECT count(*) FROM integracao_cemig_db..cliente_tb  " & vbNewLine
            sSql &= "WHERE telefone1 = '" & contribuinte.Telefone1 & "' " & vbNewLine
            If contribuinte.DDD <> "" Then
                sSql &= "and DDD = '" & contribuinte.DDD & "' "
            End If
            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                If ds.Tables(0).Rows(0).Item(0) >= 1 Then
                    Return True
                Else
                    Return False
                End If
            End If
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
        Return False

    End Function

    Public Function ConsultaCodClienteCemig(ByVal cod As String) As DataSet
        Dim ds As DataSet = New DataSet
        Dim Nome As String = ""
        Try

            Dim sSql As String = " SELECT * from integracao_cemig_db.dbo.cliente_tb" _
                               + " where cod_ean_cliente = '" & cod & "'"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    Return ds
                Else
                    Return Nothing
                End If
            Else

                Return Nothing
            End If



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function Inclui_Numeros_Requisicao_Comercial_Nao_ContribuiuCemig(filtro As CContribuinte,
                                                                            aceite As String,
                                                                            mesAtuacao As String,
                                                                            mesRef As String) As Boolean

        Try
            Dim sSql As String
            sSql = ""
            sSql &= "exec integracao_cemig_db.dbo.Inclui_Comercial_Nao_Contribuinte " & vbNewLine
            sSql &= "@DDD = '" & filtro.DDD & "'," & vbNewLine
            sSql &= "@numero = '" & filtro.Telefone1 & "', " & vbNewLine
            sSql &= "@valor = " & Replace(CDec(filtro.Valor), ",", ".") & ", " & vbNewLine
            sSql &= "@cod_cliente = '" & filtro.Cod_EAN_Cliente & "'," & vbNewLine
            sSql &= "@operador_id = '" & filtro.Cod_Operador & "'," & vbNewLine
            sSql &= "@operador = '" & filtro.Operador & "'," & vbNewLine
            sSql &= "@aceite = '" & aceite & "'," & vbNewLine
            sSql &= "@categoria = '" & filtro.Cod_Categoria & "'," & vbNewLine
            sSql &= "@mesAtuacao = '" & mesAtuacao & "'," & vbNewLine
            sSql &= "@mesRef = '" & mesRef & "'," & vbNewLine
            sSql &= "@nome = '" & filtro.NomeCliente1 & "'"

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function Excluir_Requisicao_Cemig(filtro As CContribuinte, mesRef As String) As Boolean

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec integracao_cemig_db.dbo.Excluir_requisicao_Cemig " & vbNewLine
            sSql &= "@DDD = '" & filtro.DDD & "'," & vbNewLine
            sSql &= "@numero = '" & filtro.Telefone1 & "', " & vbNewLine
            sSql &= "@valor = " & Replace(filtro.Valor, ",", ".") & ", " & vbNewLine
            sSql &= "@cod_cliente = '" & filtro.CodCliente & "', "
            sSql &= "@mesRef = '" & mesRef & "'"


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            Return False
        End Try




    End Function

#End Region
End Class
