﻿Public Class RequisicaoDAO

#Region "Variáveis"


    Private Cod_Cidade As Integer
    Private Nome_Cidade As String
    Private Uf_Cidade As String

    Private Cod_Bairro As Integer
    Private Nome_Bairro As String
    Private Cep_Bairro As String
    Private Cod_Cidade_Bairro As Integer
    Private Cod_Regiao_Bairro As Integer

    Private ConnectionHelper As Util.ConnectionHelper


#End Region


#Region "Construtores"

    Sub New(Optional ByVal p_Cod_Cidade As Decimal = 0,
        Optional ByVal p_Nome_Cidade As String = "",
        Optional ByVal p_Uf_Cidade As String = "",
        Optional ByVal p_Cod_Bairro As Integer = 0,
        Optional ByVal p_Nome_Bairro As String = "",
        Optional ByVal p_Cep_Bairro As Integer = 0,
        Optional ByVal p_Cod_Cidade_Bairro As Integer = 0,
        Optional ByVal p_Cod_Regiao_Bairro As Integer = 0)

        Cod_Cidade = p_Cod_Cidade
        Nome_Cidade = p_Nome_Cidade
        Uf_Cidade = p_Uf_Cidade
        Cod_Bairro = p_Cod_Bairro
        Nome_Bairro = p_Nome_Bairro
        Cep_Bairro = p_Cep_Bairro
        Cod_Cidade_Bairro = p_Cod_Cidade_Bairro
        Cod_Regiao_Bairro = p_Cod_Regiao_Bairro


        ConnectionHelper = New Util.ConnectionHelper(System.Reflection.Assembly.GetExecutingAssembly)

    End Sub

    Sub New(ByRef p_connectionHelper As Util.ConnectionHelper,
           Optional ByVal p_Cod_Cidade As Decimal = 0,
           Optional ByVal p_Nome_Cidade As String = "",
           Optional ByVal p_Uf_Cidade As String = "",
           Optional ByVal p_Cod_Bairro As Integer = 0,
           Optional ByVal p_Nome_Bairro As String = "",
           Optional ByVal p_Cep_Bairro As Integer = 0,
           Optional ByVal p_Cod_Cidade_Bairro As Integer = 0,
           Optional ByVal p_Cod_Regiao_Bairro As Integer = 0)

        Cod_Cidade = p_Cod_Cidade
        Nome_Cidade = p_Nome_Cidade
        Uf_Cidade = p_Uf_Cidade
        Cod_Bairro = p_Cod_Bairro
        Nome_Bairro = p_Nome_Bairro
        Cep_Bairro = p_Cep_Bairro
        Cod_Cidade_Bairro = p_Cod_Cidade_Bairro
        Cod_Regiao_Bairro = p_Cod_Regiao_Bairro

        ConnectionHelper = p_connectionHelper

    End Sub


#End Region



#Region "metodos"

    Public Function DELETE_T_REQUISICAO_TMP() As Boolean

        Try
            Dim sSql As String = ""
            sSql = sSql + "DELETE FROM T_REQUISICAO_TMP WHERE SITUACAO = '" & 1 & "'"


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function DELETE_T_REQUISICAO_LISTA() As Boolean

        Try
            Dim sSql As String = ""
            sSql = sSql + "DELETE FROM T_REQUISICAO_LISTA WHERE SITUACAO = '" & 1 & "' AND COD_CATEGORIA <> '" & 10 & "'"


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function DELETE_T_REQUISICAO_TMP2() As Boolean

        Try
            Dim sSql As String = ""
            sSql = sSql + "DELETE FROM T_REQUISICAO_TMP2 WHERE SITUACAO = '" & 1 & "'"


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function DELETE_T_REQUISICAO_LISTA2() As Boolean

        Try
            Dim sSql As String = ""
            sSql = sSql + "DELETE FROM T_REQUISICAO_LISTA2 WHERE SITUACAO = '" & 1 & "'"


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function SELECT_T_FUNCIONARIOS(ByVal TINST As String) As Integer
        Dim cod As Integer
        Dim ds As DataSet
        Try
            Dim sSql As String = ""
            sSql = sSql + "Select COD_FUNCIONARIO from T_FUNCIONARIOS WHERE NOME_FUNCIONARIO = '" & TINST & "'"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    cod = ds.Tables(0).Rows(0).Item(0)
                    Return cod
                Else
                    Return Nothing
                End If
            End If
            Return Nothing
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function SELECT_T_CLIENTES() As DataSet
        Dim ds As DataSet
        Try
            Dim sSql As String = ""
            sSql = sSql + "Select * from DIGITAL..T_CLIENTES ORDER BY COD_EAN_CLIENTE"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                Return ds
            Else
                Return Nothing
            End If

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function SELECT_T_MOV_CONTRIBUICAO_DESCR(ByVal TCODEAN As String) As DataSet
        Dim ds As DataSet
        Try
            Dim sSql As String = ""
            sSql = sSql + "Select * from T_MOV_CONTRIBUICAO_DESCR where COD_EAN_CLIENTE = '" & TCODEAN & "' ORDER BY DATA_MOV DESC"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                Return ds
            Else
                Return Nothing
            End If

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function SELECT_T_REQUISICAO_TMP(ByVal TCODEANCLI As String) As DataSet
        Dim ds As DataSet
        Try
            Dim sSql As String = ""
            sSql = sSql + "Select * from T_REQUISICAO_TMP where COD_EAN_CLIENTE= '" & TCODEANCLI & "' ORDER BY DATA_MOV DESC"""


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                Return ds
            Else
                Return Nothing
            End If

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function SELECT_T_ULT_MOV(ByVal TCODEANCLI As String) As DataSet
        Dim ds As DataSet
        Try
            Dim sSql As String = ""
            sSql = sSql + "Select * from T_ULT_MOV where COD_EAN_CLIENTE = '" & TCODEANCLI & "'"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                Return ds
            Else
                Return Nothing
            End If

        Catch ex As Exception
            Return Nothing
        End Try

    End Function






    Public Function DELETE_T_LISTA_CONTRIBUINTE_LIB_TMP1(ByVal SETEMPRESA As Integer) As Boolean
        Dim ds As DataSet
        Try
            Dim sSql As String = ""
            sSql = sSql + "DELETE FROM T_LISTA_CONTRIBUINTE_LIB_TMP1 WHERE COD_EMPRESA = '" & SETEMPRESA & "'"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True


        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function DELETE_T_LISTA_CONTRIBUINTE_LIB_TMP2(ByVal SETEMPRESA As Integer) As Boolean
        Dim ds As DataSet
        Try
            Dim sSql As String = ""
            sSql = sSql + "DELETE FROM T_LISTA_CONTRIBUINTE_LIB_TMP2 WHERE COD_EMPRESA = '" & SETEMPRESA & "'"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True


        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function CarregaFichas(ByVal cidade As String, ByVal dia As Integer) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec controle_db.dbo.RetornaRequisicao " & vbNewLine
            sSql &= "@cidade = " & cidade
            sSql &= ", @dia = '" & dia & "'"



            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function CarregaFichasOperador(ByVal categoria As Integer, ByVal cidade As String, ByVal codUsuario As Integer) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec controle_db.dbo.RetornaRequisicaoOperador_SPS " & vbNewLine
            sSql &= "@categoria = " & categoria
            If cidade = "Selecione..." Then
                sSql &= ", @cidade = ''"
            Else
                sSql &= ", @cidade = '" & cidade & "'"
            End If
            sSql &= ", @cod_opera = " & codUsuario


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregaFichasCemigOperador(ByVal categoria As Integer, ByVal cidade As String, ByVal codUsuario As Integer) As DataSet
        Try
            Dim sSql As String = ""
            sSql &= "exec integracao_cemig_db.dbo.RetornaRequisicaoCemigOperador_SPS " & vbNewLine
            sSql &= "@categoria = " & categoria
            If cidade = "Selecione..." Then
                sSql &= ", @cidade = ''"
            Else
                sSql &= ", @cidade = '" & cidade & "'"
            End If
            sSql &= ", @cod_opera = " & codUsuario
            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function CarregaFichasSupervisao(ByVal categoria As Integer, ByVal cidade As String) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "select a.requisicao_id, a.confirmado, a.tipo_mov, a.tipo_cobranca, a.cobrador, a.cod_categoria, a.cod_grupo, a.cod_opera, a.cod_especie " & vbNewLine
            sSql &= ", a.data_mov, a.cod_ean_cliente, a.cod_contribuinte, a.nome_contribuinte, a.valor, a.tipo_contrib, a.cod_recibo, a.dia,a.cidade, a.dt_impressao " & vbNewLine
            sSql &= ", a.cod_cobrador, a.cod_cidade, a.cod_empresa, a.situacao, a.cod_bairro, a.cod_usuario, a.dt_nc, a.observacao, isnull(b.telefone1, c.telefone1) as telefone1, isnull(b.DDD ,substring(c.telefone1,2,2)) as DDD " & vbNewLine
            sSql &= "from controle_db.dbo.requisicao_lista_tb a" & vbNewLine
            sSql &= "left join controle_db.dbo.cliente_tb b" & vbNewLine
            sSql &= "on a.cod_ean_cliente = b.cod_ean_cliente" & vbNewLine
            sSql &= "left join controle_db.dbo.LISTA_RETIDA_TMP_TB c" & vbNewLine
            sSql &= "on a.cod_ean_cliente = c.cod_ean_cliente" & vbNewLine
            sSql &= "where a.confirmado = 2  and tipo_mov <> 'RECUSA'"
            sSql &= "and a.cidade = '" & cidade & "'"
            If categoria > 0 Then
                sSql &= "and a.cod_categoria = " & categoria & ""
            End If

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregaFichasNaoAtendeu(ByVal categoria As Integer, ByVal cidade As String, ByVal dia As String, ByVal cod_opera As Integer) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "select a.requisicao_id, a.confirmado, a.tipo_mov, a.tipo_cobranca, a.cobrador, a.cod_categoria, a.cod_grupo, a.cod_opera, a.cod_especie " & vbNewLine
            sSql &= ", a.data_mov, a.cod_ean_cliente, a.cod_contribuinte, a.nome_contribuinte, a.valor, a.tipo_contrib, a.cod_recibo, a.dia,a.cidade " & vbNewLine
            sSql &= ", a.cod_cobrador, a.cod_cidade, a.cod_empresa, a.situacao, a.cod_bairro, a.cod_usuario, a.dt_nc, a.observacao, isnull(b.telefone1, c.telefone1) as telefone1, isnull(b.DDD, substring(c.telefone1,2,2)) as DDD, a.dt_impressao " & vbNewLine
            sSql &= "from controle_db.dbo.requisicao_lista_tb a" & vbNewLine
            sSql &= "left join controle_db.dbo.cliente_tb b" & vbNewLine
            sSql &= "on a.cod_ean_cliente = b.cod_ean_cliente" & vbNewLine
            sSql &= "left join controle_db.dbo.LISTA_RETIDA_TMP_TB c" & vbNewLine
            sSql &= "on a.cod_ean_cliente = c.cod_ean_cliente" & vbNewLine
            If categoria = 4 Then
                sSql &= "where a.situacao = 1"
                sSql &= "and a.cidade = '" & cidade & "'"
                If dia > 0 Then
                    sSql &= "and a.dia = '" & dia & "'"
                End If
                sSql &= "and a.cod_categoria = " & categoria & ""
                If cod_opera > 0 Then
                    sSql &= "and a.cod_opera = " & cod_opera
                End If
            Else
                sSql &= "where a.confirmado = 1  and tipo_mov = 'NAO ATENDEU'"
                sSql &= "and a.cidade = '" & cidade & "'"
                If dia > 0 Then
                    sSql &= "and a.dia = '" & dia & "'"
                End If
                If cod_opera > 0 Then
                    sSql &= "and a.cod_opera = " & cod_opera
                End If
                If categoria > 0 Then
                    sSql &= "and a.cod_categoria = " & categoria & ""
                End If
            End If

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function ConsultaFichas(ByVal categoria As Integer, ByVal cidade As String, ByVal dia As String, ByVal cod_opera As Integer) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "select a.requisicao_id, a.confirmado, a.tipo_mov, a.tipo_cobranca, a.cobrador, a.cod_categoria, a.cod_grupo, a.cod_opera, a.cod_especie " & vbNewLine
            sSql &= ", a.data_mov, a.cod_ean_cliente, a.cod_contribuinte, a.nome_contribuinte, a.valor, a.tipo_contrib, a.cod_recibo, a.dia,a.cidade, a.dt_impressao " & vbNewLine
            sSql &= ", a.cod_cobrador, a.cod_cidade, a.cod_empresa, a.situacao, a.cod_bairro, a.cod_usuario, a.dt_nc, a.observacao, isnull(b.telefone1, c.telefone1) as telefone1, isnull(b.DDD, substring(c.telefone1,2,2)) as DDD " & vbNewLine
            sSql &= "from controle_db.dbo.requisicao_lista_tb a" & vbNewLine
            sSql &= "left join controle_db.dbo.cliente_tb b" & vbNewLine
            sSql &= "on a.cod_ean_cliente = b.cod_ean_cliente" & vbNewLine
            sSql &= "left join controle_db.dbo.LISTA_RETIDA_TMP_TB c" & vbNewLine
            sSql &= "on a.cod_ean_cliente = c.cod_ean_cliente" & vbNewLine
            sSql &= "where a.cidade = '" & cidade & "'" & vbNewLine
            If dia > 0 Then
                sSql &= "and a.dia = '" & dia & "'" & vbNewLine
            End If
            If cod_opera > 0 Then
                sSql &= "and a.cod_opera = " & cod_opera & "" & vbNewLine
            End If
            If categoria > 0 Then
                sSql &= "and a.cod_categoria = " & categoria & "" & vbNewLine
            End If

            sSql &= "order by a.situacao asc, confirmado asc"

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregaRequisicaoPorCodEan(ByVal cod_ean As String) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "select requisicao_id,confirmado,tipo_mov,tipo_cobranca,cobrador,cod_categoria,cod_grupo,cod_opera,cod_especie " & vbNewLine
            sSql &= ",data_mov,cod_ean_cliente,cod_contribuinte,nome_contribuinte,valor,tipo_contrib,cod_recibo,dia,cidade " & vbNewLine
            sSql &= ",cod_cobrador,cod_cidade,cod_empresa,situacao,cod_bairro,cod_usuario,dt_nc,observacao " & vbNewLine
            sSql &= "from controle_db.dbo.requisicao_lista_tb " & vbNewLine
            sSql &= "where cod_ean_cliente = '" & cod_ean & "'"


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function RetornaRequisicaoPorCodEan(ByVal cod_ean As String) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "select a.requisicao_id,a.confirmado,a.tipo_mov,a.tipo_cobranca,a.cobrador,a.cod_categoria,a.cod_grupo,a.cod_opera,a.cod_especie " & vbNewLine
            sSql &= ",a.data_mov,a.cod_ean_cliente,a.cod_contribuinte,a.nome_contribuinte,a.valor,a.tipo_contrib,a.cod_recibo,a.dia,a.cidade " & vbNewLine
            sSql &= ",a.cod_cobrador,a.cod_cidade,a.cod_empresa,a.situacao,a.cod_bairro,a.cod_usuario,a.dt_nc,a.observacao, isnull(b.telefone1, c.telefone1) as telefone1, isnull(b.ddd, SUBSTRING(c.telefone1,2,2)) as ddd " & vbNewLine
            sSql &= "from controle_db.dbo.requisicao_lista_tb a " & vbNewLine
            sSql &= "left join controle_db.dbo.cliente_tb b" & vbNewLine
            sSql &= "on a.cod_ean_cliente = b.cod_ean_cliente" & vbNewLine
            sSql &= "left join controle_db.dbo.LISTA_RETIDA_TMP_TB c" & vbNewLine
            sSql &= "on a.cod_ean_cliente = c.cod_ean_cliente" & vbNewLine
            sSql &= "where a.cod_ean_cliente = '" & cod_ean & "'"


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function


    Public Function CarregaContribuintePorCodEan(ByVal cod_ean As String) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "select 1 AS confirmado" & vbNewLine
            sSql &= ",'LIBERACAO' as tipo_mov" & vbNewLine
            sSql &= ",'MENSAGEIRO' as tipo_cobranca" & vbNewLine
            sSql &= ",'INSTITUICAO' AS cobrador      " & vbNewLine
            sSql &= ",cliente_tb.cod_categoria      " & vbNewLine
            sSql &= ",cliente_tb.cod_grupo      " & vbNewLine
            sSql &= ",cliente_tb.cod_operador as cod_opera      " & vbNewLine
            sSql &= ",cliente_tb.cod_especie      " & vbNewLine
            sSql &= ",getdate() as data_mov      " & vbNewLine
            sSql &= ",cliente_tb.cod_ean_cliente      " & vbNewLine
            sSql &= ",cliente_tb.cod_cliente as cod_contribuinte     " & vbNewLine
            sSql &= ",cliente_tb.nome_cliente as nome_contribuinte" & vbNewLine
            sSql &= ",cliente_tb.valor      " & vbNewLine
            sSql &= ",controle_db.dbo.NExtenso(cliente_tb.valor)as valor_ext    " & vbNewLine
            sSql &= ",1 as tipo_contrib     " & vbNewLine
            sSql &= ",'1' as cod_recibo     " & vbNewLine
            sSql &= ",cliente_tb.dia_limite as dia      " & vbNewLine
            sSql &= ",cliente_tb.cidade      " & vbNewLine
            sSql &= ",1 as cod_cobrador      " & vbNewLine
            sSql &= ",cliente_tb.cod_cidade      " & vbNewLine
            sSql &= ",cliente_tb.cod_empresa      " & vbNewLine
            sSql &= ",3 as situacao     " & vbNewLine
            sSql &= ",cliente_tb.cod_bairro      " & vbNewLine
            sSql &= ",cliente_tb.cod_usuario      " & vbNewLine
            sSql &= ",cliente_tb.cod_regiao  " & vbNewLine
            sSql &= ", null as dt_nc" & vbNewLine
            sSql &= ", null as observacao    " & vbNewLine
            sSql &= "from controle_db.dbo.cliente_tb cliente_tb  " & vbNewLine
            sSql &= "where cod_ean_cliente = '" & cod_ean & "'"


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function CarregaRequisicaoPorCod_recibo(ByVal cod_recibo As String) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "select cod_mov_contribuicao,confirmado,tipo_mov,tipo_cobranca,cobrador,cod_categoria,cod_grupo,cod_opera,cod_especie " & vbNewLine
            sSql &= ",data_mov,cod_ean_cliente,cod_contribuinte,nome_contribuinte,valor,tipo_contrib,cod_recibo,dia,cidade " & vbNewLine
            sSql &= ",cod_cobrador,cod_cidade,cod_empresa,situacao,cod_bairro,cod_usuario" & vbNewLine
            sSql &= "from controle_db.dbo.mov_contribuicao_descr_tb " & vbNewLine
            sSql &= "where cod_recibo = '" & cod_recibo & "'"


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function CarregaFichasFaturamento(ByVal cidade As String, ByVal dia As Integer, ByVal operador_id As Integer, ByVal imprimirMensal As String) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec controle_db.dbo.RetornaFichasFaturamento_SPS " & vbNewLine
            sSql &= "@cidade = '" & cidade & "'"
            sSql &= ", @dia = '" & dia & "'"
            sSql &= ", @operador_id = " & operador_id & ""
            sSql &= ", @imprimirMensal = '" & imprimirMensal & "'"


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function CarregaFichasFaturamentoMensalEspecial(ByVal cidade As String, ByVal dia As Integer, ByVal operador_id As Integer) As DataSet
        Try
            Dim sSql As String = String.Empty
            If cidade = "Barbacena" Then
                sSql = ""
                sSql &= "exec controle_db.dbo.MensalEspecialBarbacena_SPS " & vbNewLine
            ElseIf cidade = "Santos Dumont" Then
                sSql = ""
                sSql &= "exec controle_db.dbo.MensalEspecialSantosDumont_SPS " & vbNewLine
            ElseIf cidade = "Juiz de fora" Then
                sSql = ""
                sSql &= "exec controle_db.dbo.MensalEspecialJuizdeFora_SPS " & vbNewLine
            End If


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function AtualizaCodOperaMensal(ByVal cidade As String, ByVal dia As Integer, ByVal operador_id As Integer, ByVal imprimirMensal As String) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "update controle_db.dbo.requisicao_lista_tb" & vbNewLine
            sSql &= "set cod_opera  = 1, cobrador = 'INSTITUICAO'" & vbNewLine
            sSql &= " where cidade = '" & cidade & "'"
            sSql &= " and dia = '" & dia & "'"
            sSql &= " and cod_categoria = 3 "


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function CarregaFichasFaturamentoBordero(ByVal cidade As String, ByVal dia As Integer, ByVal operador_id As Integer) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec controle_db.dbo.RetornaFichasFaturamento_Bordero_SPS " & vbNewLine
            sSql &= "@cidade = '" & cidade & "'"
            sSql &= ", @dia = '" & dia & "'"
            sSql &= ", @operador_id = " & operador_id & ""


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function CarregaFichasFaturamentoBaixa(ByVal cidade As String, ByVal dia As Integer, ByVal operador_id As Integer) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec controle_db.dbo.RetornaFichasFaturamento_SPS " & vbNewLine
            sSql &= "@cidade = '" & cidade & "'"
            sSql &= ", @dia = '" & dia & "'"
            sSql &= ", @operador_id = " & operador_id & ""


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function CarregaFichasImprimirManual(ByVal cidade As String, ByVal dia As Integer, ByVal operador_id As Integer) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "Select requisicao_lista_tb.requisicao_id" & vbNewLine
            sSql &= ",requisicao_lista_tb.confirmado    " & vbNewLine
            sSql &= ",requisicao_lista_tb.tipo_mov    " & vbNewLine
            sSql &= ",requisicao_lista_tb.tipo_cobranca    " & vbNewLine
            sSql &= ",requisicao_lista_tb.cobrador    " & vbNewLine
            sSql &= ",requisicao_lista_tb.cod_categoria    " & vbNewLine
            sSql &= ",requisicao_lista_tb.cod_grupo    " & vbNewLine
            sSql &= ",requisicao_lista_tb.cod_opera    " & vbNewLine
            sSql &= ",requisicao_lista_tb.cod_especie    " & vbNewLine
            sSql &= ",requisicao_lista_tb.data_mov    " & vbNewLine
            sSql &= ",requisicao_lista_tb.cod_ean_cliente    " & vbNewLine
            sSql &= ",requisicao_lista_tb.cod_contribuinte    " & vbNewLine
            sSql &= ",requisicao_lista_tb.nome_contribuinte    " & vbNewLine
            sSql &= ",requisicao_lista_tb.valor    " & vbNewLine
            sSql &= ",requisicao_lista_tb.tipo_contrib    " & vbNewLine
            sSql &= ",requisicao_lista_tb.cod_recibo    " & vbNewLine
            sSql &= ",requisicao_lista_tb.dia    " & vbNewLine
            sSql &= ",requisicao_lista_tb.cidade    " & vbNewLine
            sSql &= ",requisicao_lista_tb.cod_cobrador    " & vbNewLine
            sSql &= ",requisicao_lista_tb.cod_cidade    " & vbNewLine
            sSql &= ",requisicao_lista_tb.cod_empresa    " & vbNewLine
            sSql &= ",requisicao_lista_tb.situacao    " & vbNewLine
            sSql &= ",requisicao_lista_tb.cod_bairro    " & vbNewLine
            sSql &= ",requisicao_lista_tb.cod_usuario    " & vbNewLine
            sSql &= ",requisicao_lista_tb.dt_nc    " & vbNewLine
            sSql &= ",requisicao_lista_tb.observacao    " & vbNewLine
            sSql &= ",cliente_tb.DDD    " & vbNewLine
            sSql &= ",cliente_tb.telefone1     " & vbNewLine
            sSql &= "from controle_db.dbo.requisicao_lista_tb requisicao_lista_tb    " & vbNewLine
            sSql &= "left join controle_db.dbo.cliente_tb cliente_tb    " & vbNewLine
            sSql &= "on cliente_tb.cod_ean_cliente = requisicao_lista_tb.cod_ean_cliente    " & vbNewLine
            sSql &= "where requisicao_lista_tb.cidade = '" & cidade & "'" & vbNewLine
            sSql &= " and requisicao_lista_tb.dia = '" & dia & "'" & vbNewLine
            sSql &= " and requisicao_lista_tb.cod_opera = " & operador_id


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    Public Function CarregaFichasRequisicaoPorCidadeDia(ByVal cidade As String, ByVal dia As Integer, ByVal categoria_id As Integer) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec controle_db.dbo.CarregaFichasRequisicaoPorCidadeDia " & vbNewLine
            sSql &= " @cidade = '" & cidade & "'" & vbNewLine
            sSql &= " ,@categoria = " & categoria_id & vbNewLine
            sSql &= " ,@dia = " & dia





            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function CarregaFichasRequisicaoCemigPorCidadeDia(ByVal cidade As String, ByVal dia As Integer, ByVal categoria_id As Integer) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec integracao_cemig_db.dbo.CarregaFichasRequisicaoPorCidadeDia " & vbNewLine
            sSql &= " @cidade = '" & cidade & "'" & vbNewLine
            sSql &= " ,@categoria = " & categoria_id & vbNewLine
            sSql &= " ,@dia = " & dia

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function CarregaHistoricoContribuinte(ByVal cod_ean As String) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec controle_db.dbo.HistoricoContribuinte_SPS " & vbNewLine
            sSql &= "@cod_ean = '" & cod_ean & "'"


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function ConfirmarContribuicao(ByVal filtro As CContribuinte, ByVal aceite As String) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.ConfirmaContribuicao_SPU " & vbNewLine
            sSql &= "@DDD = '" & filtro.DDD & "'," & vbNewLine
            sSql &= "@numero = '" & filtro.Telefone1 & "', " & vbNewLine
            sSql &= "@valor = " & Replace(CDec(filtro.Valor), ",", ".") & ", " & vbNewLine
            sSql &= "@cod_cliente = '" & filtro.Cod_EAN_Cliente & "'," & vbNewLine
            sSql &= "@operador_id = '" & filtro.Cod_opera & "'," & vbNewLine
            sSql &= "@operador = '" & filtro.Operador & "'," & vbNewLine
            sSql &= "@aceite = '" & aceite & "'," & vbNewLine
            sSql &= "@categoria = " & filtro.Cod_Categoria



            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function ConfirmarContribuicaoCemig(ByVal filtro As CContribuinte, ByVal aceite As String) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec integracao_cemig_db.dbo.ConfirmaContribuicao_SPU " & vbNewLine
            sSql &= "@DDD = '" & filtro.DDD & "'," & vbNewLine
            sSql &= "@numero = '" & filtro.Telefone1 & "', " & vbNewLine
            sSql &= "@valor = " & Replace(CDec(filtro.Valor), ",", ".") & ", " & vbNewLine
            sSql &= "@cod_cliente = '" & filtro.Cod_EAN_Cliente & "'," & vbNewLine
            sSql &= "@operador_id = '" & filtro.Cod_opera & "'," & vbNewLine
            sSql &= "@operador = '" & filtro.Operador & "'," & vbNewLine
            sSql &= "@aceite = '" & aceite & "'," & vbNewLine
            sSql &= "@categoria = " & filtro.Cod_Categoria



            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function ConfirmarContribuicaoSupervisao(ByVal filtro As CContribuinte, ByVal aceite As String) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= " UPDATE controle_db.dbo.requisicao_lista_tb  " & vbNewLine
            sSql &= " SET confirmado = 1 " & vbNewLine
            sSql &= " where cod_ean_cliente = '" & filtro.Cod_EAN_Cliente & "'"

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function AgendaProxDia(ByVal observacao As String, ByVal cod_ean As String) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= " UPDATE controle_db.dbo.requisicao_lista_tb  " & vbNewLine
            sSql &= " SET observacao = '" & observacao & "'" & vbNewLine
            'sSql &= " SET observacao = '" & observacao & "' ,TIPO_MOV = 'PROX.DIA' " & vbNewLine
            sSql &= " where cod_ean_cliente = '" & cod_ean & "'"

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function AgendaProxDiaCemig(ByVal observacao As String, ByVal cod_ean As String) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= " UPDATE integracao_cemig_db.dbo.requisicao_tb  " & vbNewLine
            sSql &= " SET observacao = '" & observacao & "'" & vbNewLine
            'sSql &= " SET observacao = '" & observacao & "' ,TIPO_MOV = 'PROX.DIA' " & vbNewLine
            sSql &= " where cod_ean_cliente = '" & cod_ean & "'"

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function AgendaProxDiaOi(ByVal observacao As String, ByVal telefone As String) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= " UPDATE controle_db.dbo.requisicao_tb  " & vbNewLine
            sSql &= " SET observacao = '" & observacao & "'" & vbNewLine
            sSql &= " where atual = 'S' and telefone = '" & telefone & "'"

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function TelefoneDesligado(ByVal telefone As String, ByVal cod_ean As String, ByVal ddd As String) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= " UPDATE controle_db.dbo.requisicao_tb  " & vbNewLine
            sSql &= " SET aceite = 'T', confirmado = 'T' " & vbNewLine
            sSql &= " where atual = 'S' and telefone = '" & telefone & "' and ddd = '" & ddd & "'"
            sSql &= "" & vbNewLine
            sSql &= " update controle_db..lista_telefonica_tb    " & vbNewLine
            sSql &= " set situacao = 'T', dt_alteracao = getdate()    " & vbNewLine
            sSql &= " where telefone = '" & telefone & "' and ddd = '" & ddd & "'"
            sSql &= "" & vbNewLine
            sSql &= " update controle_db..requisicao_lista_tb " & vbNewLine
            sSql &= " set confirmado = 9, tipo_mov = 'TELEFONE DESLIGADO', DT_NC = GETDATE()   " & vbNewLine
            sSql &= " where cod_ean_cliente = '" & cod_ean & "'"

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function


    Public Function TelefoneDesligadoCemig(ByVal telefone As String, ByVal cod_ean As String, ByVal ddd As String) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= " UPDATE integracao_cemig_db.dbo.requisicao_tb  " & vbNewLine
            sSql &= " SET aceite = 'T', confirmado = 'T' " & vbNewLine
            sSql &= " where atual = 'S' and telefone = '" & telefone & "' and ddd = '" & ddd & "'"
            sSql &= "" & vbNewLine
            sSql &= " update controle_db..lista_telefonica_tb    " & vbNewLine
            sSql &= " set situacao = 'T', dt_alteracao = getdate()    " & vbNewLine
            sSql &= " where telefone = '" & telefone & "' and ddd = '" & ddd & "'"
            sSql &= "" & vbNewLine
            sSql &= " update controle_db..requisicao_lista_tb " & vbNewLine
            sSql &= " set confirmado = 9, tipo_mov = 'TELEFONE DESLIGADO', DT_NC = GETDATE()   " & vbNewLine
            sSql &= " where cod_ean_cliente = '" & cod_ean & "'"

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function RecusaEspecialParaMensal(ByVal categoria As Integer, ByVal cod_ean As String) As Boolean
        Try
            Dim sSql As String

            If categoria = 4 Then

                sSql = ""
                sSql &= " UPDATE controle_db.dbo.requisicao_lista_tb  " & vbNewLine
                sSql &= " SET cod_categoria = 3 ,TIPO_MOV = 'LIBERACAO', SITUACAO = 2, CONFIRMADO = 1, COD_OPERA = 1 " & vbNewLine
                sSql &= " where cod_ean_cliente = '" & cod_ean & "'"

                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Else

                sSql = ""
                sSql &= " UPDATE controle_db.dbo.requisicao_lista_tb  " & vbNewLine
                sSql &= " SET TIPO_MOV = 'NAO ATENDEU', SITUACAO = 1, CONFIRMADO = 1 " & vbNewLine
                sSql &= " where cod_ean_cliente = '" & cod_ean & "'"

                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            End If

            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function RecusaEspecialParaMensalCemig(ByVal categoria As Integer, ByVal cod_ean As String) As Boolean
        Try
            Dim sSql As String

            If categoria = 4 Then

                sSql = ""
                sSql &= " UPDATE integracao_cemig_db.dbo.requisicao_lista_tb  " & vbNewLine
                sSql &= " SET cod_categoria = 3 ,TIPO_MOV = 'LIBERACAO', SITUACAO = 2, CONFIRMADO = 1, COD_OPERA = 1 " & vbNewLine
                sSql &= " where cod_ean_cliente = '" & cod_ean & "'"

                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Else

                sSql = ""
                sSql &= " UPDATE integracao_cemig_db.dbo.requisicao_lista_tb  " & vbNewLine
                sSql &= " SET TIPO_MOV = 'NAO ATENDEU', SITUACAO = 1, CONFIRMADO = 1 " & vbNewLine
                sSql &= " where cod_ean_cliente = '" & cod_ean & "'"

                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            End If

            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function CemigRecusaEspecialParaMensal(ByVal categoria As Integer, ByVal cod_ean As String) As Boolean
        Try
            Dim sSql As String

            If categoria = 4 Then

                sSql = ""
                sSql &= " UPDATE integracao_cemig_db.dbo.requisicao_lista_tb  " & vbNewLine
                sSql &= " SET cod_categoria = 3 ,TIPO_MOV = 'LIBERACAO', SITUACAO = 2, CONFIRMADO = 1, COD_OPERA = 1 " & vbNewLine
                sSql &= " where cod_ean_cliente = '" & cod_ean & "'"

                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Else

                sSql = ""
                sSql &= " UPDATE integracao_cemig_db.dbo.requisicao_lista_tb  " & vbNewLine
                sSql &= " SET TIPO_MOV = 'NAO ATENDEU', SITUACAO = 1, CONFIRMADO = 1 " & vbNewLine
                sSql &= " where cod_ean_cliente = '" & cod_ean & "'"

                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            End If

            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function LiberaFichaParaFaturamento(ByVal categoria As Integer, ByVal cod_ean As String) As Boolean
        Try
            Dim sSql As String

            If categoria = 4 Then

                sSql = ""
                sSql &= " UPDATE controle_db.dbo.requisicao_lista_tb  " & vbNewLine
                sSql &= " SET TIPO_MOV = 'LIBERACAO', SITUACAO = 2, CONFIRMADO = 1, COD_OPERA = 1 " & vbNewLine
                sSql &= " where cod_ean_cliente = '" & cod_ean & "'"
                sSql &= " and cod_recibo = '1'"

                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            ElseIf categoria = 3 Then

                sSql = ""
                sSql &= " UPDATE controle_db.dbo.requisicao_lista_tb  " & vbNewLine
                sSql &= " SET cod_categoria = 3 ,TIPO_MOV = 'LIBERACAO', SITUACAO = 1, CONFIRMADO = 1, COD_OPERA = 1 " & vbNewLine
                sSql &= " where cod_ean_cliente = '" & cod_ean & "'"
                sSql &= " and cod_recibo = '1'"

                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Else

                sSql = ""
                sSql &= " UPDATE controle_db.dbo.requisicao_lista_tb  " & vbNewLine
                sSql &= " SET TIPO_MOV = 'LIBERACAO', SITUACAO = 2, CONFIRMADO = 1, COD_OPERA = 1 " & vbNewLine
                sSql &= " where cod_ean_cliente = '" & cod_ean & "'"
                sSql &= " and cod_recibo = '1'"

                ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            End If

            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function
    Public Function RetornaIdBordero() As Integer
        Try
            Dim sSql As String
            Dim ds As DataSet
            Dim bordero_id As Integer

            sSql = ""
            sSql &= "SELECT MAX(isnull(bordero,1)) FROM controle_db.dbo.bordero_tb"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    bordero_id = ds.Tables(0).Rows(0).Item(0)
                Else
                    bordero_id = 1
                End If
                Return bordero_id
            Else
                Return 0
            End If


            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function RetornaIdBaixa() As Integer
        Try
            Dim sSql As String
            Dim ds As DataSet
            Dim baixa_id As Integer

            sSql = ""
            sSql &= "SELECT MAX(isnull(baixa,1)) FROM controle_db.dbo.baixa_tb"


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    baixa_id = ds.Tables(0).Rows(0).Item(0)
                Else
                    baixa_id = 1
                End If
                Return baixa_id
            Else
                Return 0
            End If


            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function CriaRegistroBordero(ByVal cod_mov_contribuicao As Long, ByVal cod_recibo As String, ByVal cod_cliente As String, ByVal bordero_id As Long) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.GeraRegistroBordero_SPI " & vbNewLine
            sSql &= "@cod_mov_contribuicao = " & cod_mov_contribuicao & "," & vbNewLine
            sSql &= "@cod_recibo = '" & cod_recibo & "', " & vbNewLine
            sSql &= "@cod_cliente = '" & cod_cliente & "', " & vbNewLine
            sSql &= "@bordero_id = " & bordero_id



            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function CriaRegistroBaixa(ByVal cod_mov_contribuicao As Long, ByVal cod_recibo As String, ByVal cod_cliente As String, ByVal baixa_id As Long, ByVal tipo_mov As String) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.GeraRegistroBaixa_SPI " & vbNewLine
            sSql &= "@cod_mov_contribuicao = " & cod_mov_contribuicao & "," & vbNewLine
            sSql &= "@cod_recibo = '" & cod_recibo & "', " & vbNewLine
            sSql &= "@cod_cliente = '" & cod_cliente & "', " & vbNewLine
            sSql &= "@baixa_id = " & baixa_id & ", " & vbNewLine
            sSql &= "@tipo_mov = '" & tipo_mov & "' "



            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function GeraMovimentacao(ByVal cod_mov_contribuicao As Long, ByVal cod_recibo As String, ByVal cod_cliente As String) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.GeraMovimentacao_SPI " & vbNewLine
            sSql &= "@cod_mov_contribuicao = " & cod_mov_contribuicao & "," & vbNewLine
            sSql &= "@cod_recibo = '" & cod_recibo & "', " & vbNewLine
            sSql &= "@cod_cliente = '" & cod_cliente & "' " & vbNewLine


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function





    Public Function VerficaFichaImpressa(ByVal cod_cliente As String) As DataSet
        Try
            Dim sSql As String
            Dim ds As New DataSet

            sSql = ""
            sSql &= "select * from controle_db.dbo.requisicao_lista_tb " & vbNewLine
            sSql &= "where dt_impressao is not null and cod_ean_cliente = '" & cod_cliente & "' " & vbNewLine


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)



            Return ds
        Catch ex As Exception
            Return Nothing
        End Try

    End Function


    Public Function Atualiza_Movimentacao_Liberacao(ByVal cod_mov_contribuicao As Long, ByVal cod_recibo As String, ByVal cod_cobrador As Integer, ByVal cobrador As String, ByVal cod_cliente As String, ByVal valor As Decimal) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.Atualiza_Movimentacao_Liberacao_SPU " & vbNewLine
            sSql &= "@cod_mov_contribuicao = " & cod_mov_contribuicao & "," & vbNewLine
            sSql &= "@cod_recibo = '" & cod_recibo & "', " & vbNewLine
            sSql &= "@cod_cobrador = " & cod_cobrador & ", " & vbNewLine
            sSql &= "@cobrador = '" & cobrador & "', " & vbNewLine
            sSql &= "@cod_ean_cliente = '" & cod_cliente & "', " & vbNewLine
            sSql &= "@valorNovo = " & Convert.ToDouble(valor)


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)



            Return True
        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function GravaTabelaLiberacao(ByVal cod_mov_contribuicao As Long, ByVal cod_recibo As String, ByVal cod_cliente As String, ByVal valor As Decimal) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.GravaTabelaLiberacao_SPI " & vbNewLine
            sSql &= "@cod_mov_contribuicao = " & cod_mov_contribuicao & "," & vbNewLine
            sSql &= "@cod_recibo = '" & cod_recibo & "', " & vbNewLine
            sSql &= "@cod_cliente = '" & cod_cliente & "', " & vbNewLine
            sSql &= "@valorNovo = " & Convert.ToDouble(valor)


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function DeletaDadosTabelaLiberacao() As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= "delete from controle_db.dbo.lista_contribuicao_liberacao_tmp_tb"


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function ApagaRegistroErradosRequisicao(ByVal telefone As String) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= "delete from controle_db.dbo.requisicao_tb" & vbNewLine
            sSql &= "where atual = 'S'" & vbNewLine
            sSql &= "and telefone = '" & telefone & "'"

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function


    Public Function AtualizaDadosListaTelefonica(ByVal contribuinte As CContribuinte) As Boolean

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "update controle_db.dbo.lista_telefonica_tb" & vbNewLine
            sSql &= "set valor = " & contribuinte.Valor & " " & vbNewLine
            sSql &= "where telefone = '" & contribuinte.Telefone1 & "'" & vbNewLine
            sSql &= "and DDD = '" & contribuinte.DDD & "'" & vbNewLine

            '  ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function AtualizaDadosListaTelefonicaCemig(ByVal contribuinte As CContribuinte) As Boolean

        Try
            Dim sSql As String

            sSql = ""
            sSql &= "update integracao_cemig_db.dbo.lista_telefonica_tb" & vbNewLine
            sSql &= "set valor = " & contribuinte.Valor & " " & vbNewLine
            sSql &= "where telefone = '" & contribuinte.Telefone1 & "'" & vbNewLine
            sSql &= "and DDD = '" & contribuinte.DDD & "'" & vbNewLine

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function


    Public Function Atualiza_Movimentacao(ByVal cod_mov_contribuicao As Long, ByVal cod_bordero As Long, ByVal cod_recibo As String, ByVal cod_cobrador As Integer, ByVal cobrador As String, ByVal cod_cliente As String) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.Atualiza_Movimentacao_SPU " & vbNewLine
            sSql &= "@cod_mov_contribuicao = " & cod_mov_contribuicao & "," & vbNewLine
            sSql &= "@cod_cliente = '" & cod_cliente & "'," & vbNewLine
            sSql &= "@cod_bordero = " & cod_bordero & ", " & vbNewLine
            sSql &= "@cod_recibo = '" & cod_recibo & "', " & vbNewLine
            sSql &= "@cod_cobrador = " & cod_cobrador & ", " & vbNewLine
            sSql &= "@cobrador = '" & cobrador & "' " & vbNewLine


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function Atualiza_Movimentacao_baixa(ByVal cod_mov_contribuicao As Long, ByVal cod_recibo As String, ByVal cod_baixa As Long, ByVal cod_cobrador As Integer, ByVal cobrador As String, ByVal tipo_mov As String) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.Atualiza_Movimentacao_baixa_spu " & vbNewLine
            sSql &= "@cod_mov_contribuicao = " & cod_mov_contribuicao & "," & vbNewLine
            sSql &= "@cod_baixa = " & cod_baixa & ", " & vbNewLine
            sSql &= "@cod_recibo = '" & cod_recibo & "', " & vbNewLine
            sSql &= "@cod_cobrador = " & cod_cobrador & ", " & vbNewLine
            sSql &= "@cobrador = '" & cobrador & "', " & vbNewLine
            sSql &= "@tipo_mov = '" & tipo_mov & "' " & vbNewLine


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function



    Public Function GravaNumerosMovimentacao(ByVal cod_mov_contribuicao As Long, ByVal cobrador As String, ByVal cod_cobrador As Integer, ByVal cod_empresa As Integer, ByVal totalFichas As Integer, ByVal ValorFichas As Decimal, ByVal tipo_mov As String) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= "exec controle_db.dbo.Grava_Numeros_Movimentacao_SPI " & vbNewLine
            sSql &= "@cod_mov_contribuicao = " & cod_mov_contribuicao & "," & vbNewLine
            sSql &= "@tipo_mov = '" & tipo_mov & "', " & vbNewLine
            sSql &= "@cobrador = '" & cobrador & "', " & vbNewLine
            sSql &= "@cod_cobrador = " & cod_cobrador & ", " & vbNewLine
            sSql &= "@cod_empresa = " & cod_empresa & ", " & vbNewLine
            sSql &= "@totalFichas = " & totalFichas & ", " & vbNewLine
            sSql &= "@ValorFichas = " & CDbl(ValorFichas)


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function CarregaMovimentacoes(ByVal cod_mov_contribuicao As Long, ByVal tipo_mov As String, ByVal cobrador As String, ByVal dataMov As String) As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try
            sSql = ""
            sSql = sSql + "Select mov_contribuicao_id " & vbNewLine
            sSql = sSql + ",cod_mov_contribuicao" & vbNewLine
            sSql = sSql + ",liberacao_id" & vbNewLine
            sSql = sSql + ",bordero_id" & vbNewLine
            sSql = sSql + ",baixa_id" & vbNewLine
            sSql = sSql + ",tipo_mov" & vbNewLine
            sSql = sSql + ",tipo_cobranca" & vbNewLine
            sSql = sSql + ",cobrador" & vbNewLine
            sSql = sSql + ",data_mov" & vbNewLine
            sSql = sSql + ",confirmado" & vbNewLine
            sSql = sSql + ",cod_cobrador" & vbNewLine
            sSql = sSql + ",cod_empresa" & vbNewLine
            sSql = sSql + ",total_fichas" & vbNewLine
            sSql = sSql + ",valor_fichas " & vbNewLine
            sSql = sSql + "from controle_db.dbo.mov_contribuicao_tb" & vbNewLine
            If tipo_mov <> "" Then
                sSql = sSql + " where tipo_mov ='" & tipo_mov & "'" & vbNewLine
                If cod_mov_contribuicao <> 0 Then
                    sSql = sSql + " and cod_mov_contribuicao =" & cod_mov_contribuicao & vbNewLine
                End If
            End If
            If cod_mov_contribuicao <> 0 Then
                sSql = sSql + " where cod_mov_contribuicao =" & cod_mov_contribuicao & vbNewLine
            End If
            If cobrador <> "" Then
                sSql = sSql + " and cobrador ='" & cobrador & "'" & vbNewLine
            End If

            If IsDate(dataMov) Then
                sSql = sSql + " and convert(date,data_mov) ='" & dataMov & "'" & vbNewLine
            End If
            sSql = sSql + " order by data_mov desc"

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function CarregaMovimentacoesDescrOpt(ByVal cod_mov_contribuicao As Long, ByVal tipo_mov As String, ByVal cobrador As String, ByVal dataMov As String) As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try
            sSql = ""
            sSql = sSql + "Select " & vbNewLine
            sSql = sSql + " cod_mov_contribuicao" & vbNewLine
            sSql = sSql + ",tipo_mov" & vbNewLine
            sSql = sSql + ",tipo_cobranca" & vbNewLine
            sSql = sSql + ",cobrador" & vbNewLine
            sSql = sSql + ",cod_categoria" & vbNewLine
            sSql = sSql + ",cod_grupo" & vbNewLine
            sSql = sSql + ",cod_opera" & vbNewLine
            sSql = sSql + ",cod_especie" & vbNewLine
            sSql = sSql + ",data_mov" & vbNewLine
            sSql = sSql + ",data_venc" & vbNewLine
            sSql = sSql + ",cod_ean_cliente" & vbNewLine
            sSql = sSql + ",cod_contribuinte" & vbNewLine
            sSql = sSql + ",nome_contribuinte " & vbNewLine
            sSql = sSql + ",valor " & vbNewLine
            sSql = sSql + ",tipo_contrib " & vbNewLine
            sSql = sSql + ",confirmado " & vbNewLine
            sSql = sSql + ",cod_recibo " & vbNewLine
            sSql = sSql + ",dia " & vbNewLine
            sSql = sSql + ",cidade " & vbNewLine
            sSql = sSql + ",cod_cobrador " & vbNewLine
            sSql = sSql + ",cod_cidade " & vbNewLine
            sSql = sSql + ",cod_empresa " & vbNewLine
            sSql = sSql + ",situacao " & vbNewLine
            sSql = sSql + ",cod_bairro " & vbNewLine
            sSql = sSql + ",cod_usuario " & vbNewLine
            sSql = sSql + ",cod_regiao " & vbNewLine
            sSql = sSql + "from controle_db.dbo.mov_contribuicao_descr_tb" & vbNewLine
            If tipo_mov <> "" Then
                sSql = sSql + " where tipo_mov ='" & tipo_mov & "'" & vbNewLine
                If cod_mov_contribuicao <> 0 Then
                    sSql = sSql + " and cod_mov_contribuicao =" & cod_mov_contribuicao & vbNewLine
                End If
            End If
            If cod_mov_contribuicao <> 0 Then
                sSql = sSql + " where cod_mov_contribuicao =" & cod_mov_contribuicao & vbNewLine
            End If
            If cobrador <> "" Then
                sSql = sSql + " and cobrador ='" & cobrador & "'" & vbNewLine
            End If

            If IsDate(dataMov) Then
                sSql = sSql + " and convert(date,data_mov) ='" & dataMov & "'" & vbNewLine
            End If
            sSql = sSql + " order by data_mov desc"

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function CarregaMovimentacoesDescr(ByVal cod_mov_contribuicao As Long) As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try
            sSql = ""
            sSql = sSql + "Select " & vbNewLine
            sSql = sSql + " a.cod_mov_contribuicao" & vbNewLine
            sSql = sSql + ",a.tipo_mov" & vbNewLine
            sSql = sSql + ",a.tipo_cobranca" & vbNewLine
            sSql = sSql + ",a.cobrador" & vbNewLine
            sSql = sSql + ",a.cod_categoria" & vbNewLine
            sSql = sSql + ",a.cod_grupo" & vbNewLine
            sSql = sSql + ",a.cod_opera" & vbNewLine
            sSql = sSql + ",a.cod_especie" & vbNewLine
            sSql = sSql + ",a.data_mov" & vbNewLine
            sSql = sSql + ",a.data_venc" & vbNewLine
            sSql = sSql + ",a.cod_ean_cliente" & vbNewLine
            sSql = sSql + ",a.cod_contribuinte" & vbNewLine
            sSql = sSql + ",a.nome_contribuinte " & vbNewLine
            sSql = sSql + ",a.valor " & vbNewLine
            sSql = sSql + ",a.tipo_contrib " & vbNewLine
            sSql = sSql + ",a.confirmado " & vbNewLine
            sSql = sSql + ",a.cod_recibo " & vbNewLine
            sSql = sSql + ",a.dia " & vbNewLine
            sSql = sSql + ",a.cidade " & vbNewLine
            sSql = sSql + ",a.cod_cobrador " & vbNewLine
            sSql = sSql + ",a.cod_cidade " & vbNewLine
            sSql = sSql + ",a.cod_empresa " & vbNewLine
            sSql = sSql + ",a.situacao " & vbNewLine
            sSql = sSql + ",a.cod_bairro " & vbNewLine
            sSql = sSql + ",a.cod_usuario " & vbNewLine
            sSql = sSql + ",a.cod_regiao " & vbNewLine
            sSql = sSql + ",a.dt_impressao " & vbNewLine
            sSql = sSql + "from controle_db.dbo.mov_contribuicao_descr_tb a" & vbNewLine
            If cod_mov_contribuicao <> 0 Then
                sSql = sSql + " where a.cod_mov_contribuicao =" & cod_mov_contribuicao & vbNewLine
                sSql = sSql + " and a.dt_inclusao is not null"
            End If

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function CarregaMovimentacoesDescrBaixa(ByVal cod_mov_contribuicao As Long) As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try
            sSql = ""
            sSql = sSql + "Select " & vbNewLine
            sSql = sSql + " cod_mov_contribuicao" & vbNewLine
            sSql = sSql + ",tipo_mov" & vbNewLine
            sSql = sSql + ",tipo_cobranca" & vbNewLine
            sSql = sSql + ",cobrador" & vbNewLine
            sSql = sSql + ",cod_categoria" & vbNewLine
            sSql = sSql + ",cod_grupo" & vbNewLine
            sSql = sSql + ",cod_opera" & vbNewLine
            sSql = sSql + ",cod_especie" & vbNewLine
            sSql = sSql + ",data_mov" & vbNewLine
            sSql = sSql + ",data_venc" & vbNewLine
            sSql = sSql + ",cod_ean_cliente" & vbNewLine
            sSql = sSql + ",cod_contribuinte" & vbNewLine
            sSql = sSql + ",nome_contribuinte " & vbNewLine
            sSql = sSql + ",valor " & vbNewLine
            sSql = sSql + ",tipo_contrib " & vbNewLine
            sSql = sSql + ",confirmado " & vbNewLine
            sSql = sSql + ",cod_recibo " & vbNewLine
            sSql = sSql + ",dia " & vbNewLine
            sSql = sSql + ",cidade " & vbNewLine
            sSql = sSql + ",cod_cobrador " & vbNewLine
            sSql = sSql + ",cod_cidade " & vbNewLine
            sSql = sSql + ",cod_empresa " & vbNewLine
            sSql = sSql + ",situacao " & vbNewLine
            sSql = sSql + ",cod_bairro " & vbNewLine
            sSql = sSql + ",cod_usuario " & vbNewLine
            sSql = sSql + ",cod_regiao " & vbNewLine
            sSql = sSql + "from controle_db.dbo.mov_contribuicao_descr_tb" & vbNewLine
            If cod_mov_contribuicao <> 0 Then
                sSql = sSql + " where cod_mov_contribuicao =" & cod_mov_contribuicao & vbNewLine
                sSql = sSql + " and dt_inclusao is not null"
                sSql = sSql + " and tipo_mov not in ('CANCELADO', 'BAIXADO', 'BORDERO') "
                sSql = sSql + " AND DT_INCLUSAO IS NOT NULL"
            Else
                sSql = sSql + " WHERE DT_INCLUSAO IS NOT NULL"
            End If


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function GeraRecibo() As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try

            sSql = ""
            sSql = sSql + "insert into controle_db.dbo.ult_mov_tb" & vbNewLine
            sSql = sSql + "(cod_ean_cliente,cod_recibo,dt_mov,valor,tipo_mov,cobrador,cod_empresa)" & vbNewLine
            sSql = sSql + "select a.cod_ean_cliente, a.cod_recibo, a.data_mov, a.valor, a.tipo_mov, a.cobrador, a.cod_empresa " & vbNewLine
            sSql = sSql + "from controle_db.dbo.lista_contribuicao_liberacao_tmp_tb a" & vbNewLine
            sSql = sSql + "left join controle_db.dbo.ult_mov_tb b" & vbNewLine
            sSql = sSql + "on a.cod_ean_cliente = b.cod_ean_cliente" & vbNewLine
            sSql = sSql + "where(b.cod_ean_cliente Is null)" & vbNewLine

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            sSql = ""
            sSql = sSql + "SELECT T_CLIENTES.nome_cliente, T_CLIENTES.endereco, T_CLIENTES.bairro, T_CLIENTES.cod_ean_cliente, T_CLIENTES.data_cad as data_cadastro, T_ESPECIE.nome_especie, T_ULT_MOV.dt_mov, T_ULT_MOV.valor, T_CLIENTES.cidade, T_CLIENTES.telefone1, T_CLIENTES.cod_grupo, T_CLIENTES.cod_categoria, T_CLIENTES.dia_limite, T_CLIENTES.observacao, T_CLIENTES.telefone2, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.data_mov, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.valor, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_categoria, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.confirmado, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_recibo, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_opera, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_regiao, T_CLIENTES.cod_regiao, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.valor_ext   " & vbNewLine
            sSql = sSql + "FROM   ((controle_db.dbo.lista_contribuicao_liberacao_tmp_tb T_LISTA_CONTRIBUICAO_LIB_TMP1_1 INNER JOIN digital.dbo.t_clientes T_CLIENTES ON T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_ean_cliente=T_CLIENTES.cod_ean_cliente) INNER JOIN controle_db.dbo.especie_tb T_ESPECIE ON (T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_especie=T_ESPECIE.cod_especie) AND (T_CLIENTES.cod_especie=T_ESPECIE.cod_especie)) INNER JOIN controle_db.dbo.ult_mov_tb T_ULT_MOV ON T_CLIENTES.cod_ean_cliente=T_ULT_MOV.cod_ean_cliente " & vbNewLine

            sSql = sSql + "UNION" & vbNewLine

            sSql = sSql + "SELECT T_CLIENTES.nome_cliente, T_CLIENTES.endereco, T_CLIENTES.bairro, T_CLIENTES.cod_ean_cliente, T_CLIENTES.DATA_CAD, T_ESPECIE.nome_especie, T_ULT_MOV.dt_mov, T_ULT_MOV.valor, T_CLIENTES.cidade, T_CLIENTES.telefone1, T_CLIENTES.cod_grupo, T_CLIENTES.cod_categoria, T_CLIENTES.dia_limite, T_CLIENTES.observacao, T_CLIENTES.telefone2, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.data_mov, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.valor, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_categoria, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.confirmado, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_recibo, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_opera, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_regiao, T_CLIENTES.cod_regiao, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.valor_ext " & vbNewLine
            sSql = sSql + "FROM   ((controle_db.dbo.lista_contribuicao_liberacao_tmp_tb T_LISTA_CONTRIBUICAO_LIB_TMP1_1 INNER JOIN digital.dbo.t_LISTA_RETIDA_TMP T_CLIENTES ON T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_ean_cliente=T_CLIENTES.cod_ean_cliente) INNER JOIN controle_db.dbo.especie_tb T_ESPECIE ON (T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_especie=T_ESPECIE.cod_especie) AND (T_CLIENTES.cod_especie=T_ESPECIE.cod_especie)) INNER JOIN controle_db.dbo.ult_mov_tb T_ULT_MOV ON T_CLIENTES.cod_ean_cliente=T_ULT_MOV.cod_ean_cliente " & vbNewLine


            sSql = sSql + "UNION" & vbNewLine

            sSql = sSql + "SELECT T_CLIENTES.nome_cliente, T_CLIENTES.endereco, T_CLIENTES.bairro, T_CLIENTES.cod_ean_cliente, T_CLIENTES.data_cadastro as data_cadastro, T_ESPECIE.nome_especie, T_ULT_MOV.dt_mov, T_ULT_MOV.valor, T_CLIENTES.cidade, T_CLIENTES.telefone1, T_CLIENTES.cod_grupo, T_CLIENTES.cod_categoria, T_CLIENTES.dia_limite, T_CLIENTES.observacao, T_CLIENTES.telefone2, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.data_mov, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.valor, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_categoria, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.confirmado, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_recibo, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_opera, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_regiao, T_CLIENTES.cod_regiao, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.valor_ext   " & vbNewLine
            sSql = sSql + "FROM   ((controle_db.dbo.lista_contribuicao_liberacao_tmp_tb T_LISTA_CONTRIBUICAO_LIB_TMP1_1 INNER JOIN controle_db.dbo.cliente_tb T_CLIENTES ON T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_ean_cliente=T_CLIENTES.cod_ean_cliente) INNER JOIN controle_db.dbo.especie_tb T_ESPECIE ON (T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_especie=T_ESPECIE.cod_especie) AND (T_CLIENTES.cod_especie=T_ESPECIE.cod_especie)) INNER JOIN controle_db.dbo.ult_mov_tb T_ULT_MOV ON T_CLIENTES.cod_ean_cliente=T_ULT_MOV.cod_ean_cliente " & vbNewLine
            sSql = sSql + "ORDER BY T_CLIENTES.cod_regiao, T_CLIENTES.nome_cliente " & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function GeraReciboRetido() As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try
            sSql = ""
            sSql = sSql + "SELECT T_CLIENTES.nome_cliente, T_CLIENTES.endereco, T_CLIENTES.bairro, T_CLIENTES.cod_ean_cliente, T_CLIENTES.data_cadastro, T_ESPECIE.nome_especie, T_ULT_MOV.dt_mov, T_ULT_MOV.valor, T_CLIENTES.cidade, T_CLIENTES.telefone1, T_CLIENTES.cod_grupo, T_CLIENTES.cod_categoria, T_CLIENTES.dia_limite, T_CLIENTES.observacao, T_CLIENTES.telefone2, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.data_mov, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.valor, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_categoria, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.confirmado, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_recibo, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_opera, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_regiao, T_CLIENTES.cod_regiao, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.valor_ext " & vbNewLine
            sSql = sSql + "FROM   ((controle_db.dbo.lista_contribuicao_liberacao_tmp_tb T_LISTA_CONTRIBUICAO_LIB_TMP1_1 INNER JOIN controle_db.dbo.cliente_tb T_CLIENTES ON T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_ean_cliente=T_CLIENTES.cod_ean_cliente) INNER JOIN controle_db.dbo.especie_tb T_ESPECIE ON (T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_especie=T_ESPECIE.cod_especie) AND (T_CLIENTES.cod_especie=T_ESPECIE.cod_especie)) INNER JOIN controle_db.dbo.ult_mov_tb T_ULT_MOV ON T_CLIENTES.cod_ean_cliente=T_ULT_MOV.cod_ean_cliente " & vbNewLine

            sSql = sSql + "UNION" & vbNewLine

            sSql = sSql + "SELECT T_CLIENTES.nome_cliente, T_CLIENTES.endereco, T_CLIENTES.bairro, T_CLIENTES.cod_ean_cliente, T_CLIENTES.DATA_CAD, T_ESPECIE.nome_especie, T_ULT_MOV.dt_mov, T_ULT_MOV.valor, T_CLIENTES.cidade, T_CLIENTES.telefone1, T_CLIENTES.cod_grupo, T_CLIENTES.cod_categoria, T_CLIENTES.dia_limite, T_CLIENTES.observacao, T_CLIENTES.telefone2, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.data_mov, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.valor, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_categoria, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.confirmado, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_recibo, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_opera, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_regiao, T_CLIENTES.cod_regiao, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.valor_ext " & vbNewLine
            sSql = sSql + "FROM   ((controle_db.dbo.lista_contribuicao_liberacao_tmp_tb T_LISTA_CONTRIBUICAO_LIB_TMP1_1 INNER JOIN controle_db.dbo.LISTA_RETIDA_TMP_TB T_CLIENTES ON T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_ean_cliente=T_CLIENTES.cod_ean_cliente) INNER JOIN controle_db.dbo.especie_tb T_ESPECIE ON (T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_especie=T_ESPECIE.cod_especie) AND (T_CLIENTES.cod_especie=T_ESPECIE.cod_especie)) INNER JOIN controle_db.dbo.ult_mov_tb T_ULT_MOV ON T_CLIENTES.cod_ean_cliente=T_ULT_MOV.cod_ean_cliente " & vbNewLine
            sSql = sSql + "ORDER BY T_CLIENTES.cod_regiao, T_CLIENTES.nome_cliente " & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function GeraRelatorioHistOperadorSintetico() As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try
            sSql = ""
            sSql = sSql + "SELECT T_MOV_CONTRIBUICAO_DESCR.cod_cobrador, T_MOV_CONTRIBUICAO_DESCR.cod_cidade, T_MOV_CONTRIBUICAO_DESCR.cidade, T_MOV_CONTRIBUICAO_DESCR.cod_recibo, T_MOV_CONTRIBUICAO_DESCR.valor, T_MOV_CONTRIBUICAO_DESCR.data_mov, T_MOV_CONTRIBUICAO_DESCR.tipo_cobranca, T_MOV_CONTRIBUICAO_DESCR.confirmado, T_MOV_CONTRIBUICAO_DESCR.tipo_mov, T_MOV_CONTRIBUICAO_DESCR.cod_opera, T_FUNCIONARIOS.nome_funcionario, T_CLIENTES.valor, T_MOV_CONTRIBUICAO_DESCR.cod_categoria" & vbNewLine
            sSql = sSql + " FROM   (controle_db.dbo.mov_contribuicao_descr_tb T_MOV_CONTRIBUICAO_DESCR INNER JOIN controle_db.dbo.funcionarios_tb T_FUNCIONARIOS ON T_MOV_CONTRIBUICAO_DESCR.cod_opera=T_FUNCIONARIOS.cod_funcionario) INNER JOIN controle_db.dbo.cliente_tb T_CLIENTES ON T_MOV_CONTRIBUICAO_DESCR.cod_contribuinte=T_CLIENTES.cod_cliente" & vbNewLine
            sSql = sSql + " WHERE  (T_MOV_CONTRIBUICAO_DESCR.tipo_cobranca=N'MENSAGEIRO' AND T_MOV_CONTRIBUICAO_DESCR.tipo_mov=N'BAIXADO' AND T_MOV_CONTRIBUICAO_DESCR.confirmado=6 OR T_MOV_CONTRIBUICAO_DESCR.confirmado=7 AND T_MOV_CONTRIBUICAO_DESCR.cod_categoria<>1)" & vbNewLine
            sSql = sSql + " ORDER BY T_MOV_CONTRIBUICAO_DESCR.cod_opera" & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function GeraRelatorioSitRecOpera() As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try
            sSql = ""
            sSql = sSql + " SELECT top 10 T_MOV_CONTRIBUICAO_DESCR.cod_cobrador, T_MOV_CONTRIBUICAO_DESCR.cidade, T_MOV_CONTRIBUICAO_DESCR.cod_recibo, T_MOV_CONTRIBUICAO_DESCR.data_mov, T_MOV_CONTRIBUICAO_DESCR.tipo_mov, T_MOV_CONTRIBUICAO_DESCR.cod_opera, T_FUNCIONARIOS.nome_funcionario, T_MOV_CONTRIBUICAO_DESCR.cod_categoria, T_MOV_CONTRIBUICAO_DESCR.data_venc, T_MOV_CONTRIBUICAO_DESCR.valor" & vbNewLine
            sSql = sSql + "FROM   controle_db.dbo.mov_contribuicao_descr_tb T_MOV_CONTRIBUICAO_DESCR INNER JOIN controle_db.dbo.funcionarios_tb T_FUNCIONARIOS ON T_MOV_CONTRIBUICAO_DESCR.cod_opera=T_FUNCIONARIOS.cod_funcionario" & vbNewLine
            sSql = sSql + "ORDER BY T_MOV_CONTRIBUICAO_DESCR.cod_opera, T_MOV_CONTRIBUICAO_DESCR.tipo_mov, T_MOV_CONTRIBUICAO_DESCR.cidade" & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function GeraRelatorioHistOperadoraAnalitico() As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try
            sSql = ""
            sSql = sSql + "  SELECT top 10 T_MOV_CONTRIBUICAO_DESCR.cod_cobrador, T_MOV_CONTRIBUICAO_DESCR.cod_cidade, T_MOV_CONTRIBUICAO_DESCR.cidade, T_MOV_CONTRIBUICAO_DESCR.cod_recibo, T_MOV_CONTRIBUICAO_DESCR.valor, T_MOV_CONTRIBUICAO_DESCR.data_mov, T_MOV_CONTRIBUICAO_DESCR.tipo_cobranca, T_MOV_CONTRIBUICAO_DESCR.confirmado, T_MOV_CONTRIBUICAO_DESCR.tipo_mov, T_MOV_CONTRIBUICAO_DESCR.cod_opera, T_FUNCIONARIOS.nome_funcionario, T_CLIENTES.valor, T_MOV_CONTRIBUICAO_DESCR.cod_categoria" & vbNewLine
            sSql = sSql + " FROM   (controle_db.dbo.mov_contribuicao_descr_tb T_MOV_CONTRIBUICAO_DESCR INNER JOIN controle_db.dbo.funcionarios_tb T_FUNCIONARIOS ON T_MOV_CONTRIBUICAO_DESCR.cod_opera=T_FUNCIONARIOS.cod_funcionario) INNER JOIN controle_db.dbo.cliente_tb T_CLIENTES ON T_MOV_CONTRIBUICAO_DESCR.cod_contribuinte=T_CLIENTES.cod_cliente" & vbNewLine
            sSql = sSql + " WHERE  (T_MOV_CONTRIBUICAO_DESCR.tipo_cobranca=N'MENSAGEIRO' AND T_MOV_CONTRIBUICAO_DESCR.tipo_mov=N'BAIXADO' AND T_MOV_CONTRIBUICAO_DESCR.confirmado=6 OR T_MOV_CONTRIBUICAO_DESCR.confirmado=7)" & vbNewLine
            sSql = sSql + "ORDER BY T_MOV_CONTRIBUICAO_DESCR.cod_opera, T_MOV_CONTRIBUICAO_DESCR.cidade" & vbNewLine


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function


    Public Function CarregaMovimentacaoDescrBordero(ByVal cod_mov_contribuicao As Long) As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try
            sSql = ""
            sSql = sSql + "Select " & vbNewLine
            sSql = sSql + " cod_mov_contribuicao" & vbNewLine
            sSql = sSql + ",tipo_mov" & vbNewLine
            sSql = sSql + ",tipo_cobranca" & vbNewLine
            sSql = sSql + ",cobrador" & vbNewLine
            sSql = sSql + ",cod_categoria" & vbNewLine
            sSql = sSql + ",cod_grupo" & vbNewLine
            sSql = sSql + ",cod_opera" & vbNewLine
            sSql = sSql + ",cod_especie" & vbNewLine
            sSql = sSql + ",data_mov" & vbNewLine
            sSql = sSql + ",data_venc" & vbNewLine
            sSql = sSql + ",cod_ean_cliente" & vbNewLine
            sSql = sSql + ",cod_contribuinte" & vbNewLine
            sSql = sSql + ",nome_contribuinte " & vbNewLine
            sSql = sSql + ",valor " & vbNewLine
            sSql = sSql + ",tipo_contrib " & vbNewLine
            sSql = sSql + ",confirmado " & vbNewLine
            sSql = sSql + ",cod_recibo " & vbNewLine
            sSql = sSql + ",dia " & vbNewLine
            sSql = sSql + ",cidade " & vbNewLine
            sSql = sSql + ",cod_cobrador " & vbNewLine
            sSql = sSql + ",cod_cidade " & vbNewLine
            sSql = sSql + ",cod_empresa " & vbNewLine
            sSql = sSql + ",situacao " & vbNewLine
            sSql = sSql + ",cod_bairro " & vbNewLine
            sSql = sSql + ",cod_usuario " & vbNewLine
            sSql = sSql + ",cod_regiao " & vbNewLine
            sSql = sSql + "from controle_db.dbo.mov_contribuicao_descr_tb" & vbNewLine
            If cod_mov_contribuicao = 0 Then
                sSql = sSql + "WHERE  situacao = 3 and tipo_mov = 'BORDERO' " & vbNewLine
            Else
                sSql = sSql + " where cod_mov_contribuicao =" & cod_mov_contribuicao & vbNewLine
                sSql = sSql + " and situacao = 3 and tipo_mov = 'BORDERO' " & vbNewLine
            End If

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function CarregaMovimentacaoDescrFichasAbertas(ByVal cod_mov_contribuicao As Long) As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try
            sSql = ""
            sSql = sSql + "Select " & vbNewLine
            sSql = sSql + " cod_mov_contribuicao" & vbNewLine
            sSql = sSql + ",tipo_mov" & vbNewLine
            sSql = sSql + ",tipo_cobranca" & vbNewLine
            sSql = sSql + ",cobrador" & vbNewLine
            sSql = sSql + ",cod_categoria" & vbNewLine
            sSql = sSql + ",cod_grupo" & vbNewLine
            sSql = sSql + ",cod_opera" & vbNewLine
            sSql = sSql + ",cod_especie" & vbNewLine
            sSql = sSql + ",data_mov" & vbNewLine
            sSql = sSql + ",data_venc" & vbNewLine
            sSql = sSql + ",cod_ean_cliente" & vbNewLine
            sSql = sSql + ",cod_contribuinte" & vbNewLine
            sSql = sSql + ",nome_contribuinte " & vbNewLine
            sSql = sSql + ",valor " & vbNewLine
            sSql = sSql + ",tipo_contrib " & vbNewLine
            sSql = sSql + ",confirmado " & vbNewLine
            sSql = sSql + ",cod_recibo " & vbNewLine
            sSql = sSql + ",dia " & vbNewLine
            sSql = sSql + ",cidade " & vbNewLine
            sSql = sSql + ",cod_cobrador " & vbNewLine
            sSql = sSql + ",cod_cidade " & vbNewLine
            sSql = sSql + ",cod_empresa " & vbNewLine
            sSql = sSql + ",situacao " & vbNewLine
            sSql = sSql + ",cod_bairro " & vbNewLine
            sSql = sSql + ",cod_usuario " & vbNewLine
            sSql = sSql + ",cod_regiao " & vbNewLine
            sSql = sSql + "from controle_db.dbo.mov_contribuicao_descr_tb" & vbNewLine
            If cod_mov_contribuicao = 0 Then
                sSql = sSql + "WHERE  situacao = 4 and tipo_mov = 'ABERTO' " & vbNewLine
            Else
                sSql = sSql + " where cod_mov_contribuicao =" & cod_mov_contribuicao & vbNewLine
                sSql = sSql + " and situacao = 4 and tipo_mov = 'ABERTO' " & vbNewLine
            End If

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function CarregaMovimentacaoDescrFichasAbertasDataMov(ByVal data_mov As String, ByVal cidade As String) As DataSet
        Dim sSql As String
        Dim ds As DataSet
        Dim data As String = ""

        data = Format(CDate(data_mov), "yyyyMMdd")

        Try
            sSql = ""
            sSql = sSql + "Select " & vbNewLine
            sSql = sSql + " cod_mov_contribuicao" & vbNewLine
            sSql = sSql + ",tipo_mov" & vbNewLine
            sSql = sSql + ",tipo_cobranca" & vbNewLine
            sSql = sSql + ",cobrador" & vbNewLine
            sSql = sSql + ",cod_categoria" & vbNewLine
            sSql = sSql + ",cod_grupo" & vbNewLine
            sSql = sSql + ",cod_opera" & vbNewLine
            sSql = sSql + ",cod_especie" & vbNewLine
            sSql = sSql + ",data_mov" & vbNewLine
            sSql = sSql + ",data_venc" & vbNewLine
            sSql = sSql + ",cod_ean_cliente" & vbNewLine
            sSql = sSql + ",cod_contribuinte" & vbNewLine
            sSql = sSql + ",nome_contribuinte " & vbNewLine
            sSql = sSql + ",valor " & vbNewLine
            sSql = sSql + ",tipo_contrib " & vbNewLine
            sSql = sSql + ",confirmado " & vbNewLine
            sSql = sSql + ",cod_recibo " & vbNewLine
            sSql = sSql + ",dia " & vbNewLine
            sSql = sSql + ",cidade " & vbNewLine
            sSql = sSql + ",cod_cobrador " & vbNewLine
            sSql = sSql + ",cod_cidade " & vbNewLine
            sSql = sSql + ",cod_empresa " & vbNewLine
            sSql = sSql + ",situacao " & vbNewLine
            sSql = sSql + ",cod_bairro " & vbNewLine
            sSql = sSql + ",cod_usuario " & vbNewLine
            sSql = sSql + ",cod_regiao " & vbNewLine
            sSql = sSql + "from controle_db.dbo.mov_contribuicao_descr_tb" & vbNewLine
            sSql = sSql + " WHERE convert(date,data_mov) = '" & data & "'" & vbNewLine
            sSql = sSql + " and tipo_mov not in ('CANCELADO', 'BAIXADO') "
            sSql = sSql + " and dt_inclusao is not null"
            If cidade <> "" Then
                sSql = sSql + " and cidade = '" & cidade & "' "
            End If


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function CarregaMovimentacaoDescrFichasAbertasDataMovCobrador(ByVal data_mov As String, ByVal cobrador As String, ByVal cidade As String) As DataSet
        Dim sSql As String
        Dim ds As DataSet
        Dim data As String = ""

        data = Format(CDate(data_mov), "yyyyMMdd")

        Try
            sSql = ""
            sSql = sSql + "Select " & vbNewLine
            sSql = sSql + " cod_mov_contribuicao" & vbNewLine
            sSql = sSql + ",tipo_mov" & vbNewLine
            sSql = sSql + ",tipo_cobranca" & vbNewLine
            sSql = sSql + ",cobrador" & vbNewLine
            sSql = sSql + ",cod_categoria" & vbNewLine
            sSql = sSql + ",cod_grupo" & vbNewLine
            sSql = sSql + ",cod_opera" & vbNewLine
            sSql = sSql + ",cod_especie" & vbNewLine
            sSql = sSql + ",data_mov" & vbNewLine
            sSql = sSql + ",data_venc" & vbNewLine
            sSql = sSql + ",cod_ean_cliente" & vbNewLine
            sSql = sSql + ",cod_contribuinte" & vbNewLine
            sSql = sSql + ",nome_contribuinte " & vbNewLine
            sSql = sSql + ",valor " & vbNewLine
            sSql = sSql + ",tipo_contrib " & vbNewLine
            sSql = sSql + ",confirmado " & vbNewLine
            sSql = sSql + ",cod_recibo " & vbNewLine
            sSql = sSql + ",dia " & vbNewLine
            sSql = sSql + ",cidade " & vbNewLine
            sSql = sSql + ",cod_cobrador " & vbNewLine
            sSql = sSql + ",cod_cidade " & vbNewLine
            sSql = sSql + ",cod_empresa " & vbNewLine
            sSql = sSql + ",situacao " & vbNewLine
            sSql = sSql + ",cod_bairro " & vbNewLine
            sSql = sSql + ",cod_usuario " & vbNewLine
            sSql = sSql + ",cod_regiao " & vbNewLine
            sSql = sSql + "from controle_db.dbo.mov_contribuicao_descr_tb" & vbNewLine
            sSql = sSql + " WHERE convert(date,data_mov) = '" & data & "'" & vbNewLine
            If cobrador <> "" Then
                sSql = sSql + " and cobrador = '" & cobrador & "' " & vbNewLine
            End If
            sSql = sSql + " and tipo_mov not in ('CANCELADO', 'BAIXADO', 'BORDERO') " & vbNewLine
            If cidade <> "" Then
                sSql = sSql + " and cidade = '" & cidade & "' "
            End If
            sSql = sSql + " AND DT_INCLUSAO IS NOT NULL"

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function


    Public Function CarregaMovimentacaoDescrBorderoDataMov(ByVal data_mov As String, ByVal operador As String) As DataSet
        Dim sSql As String
        Dim ds As DataSet
        Dim data As String = ""

        data = Format(CDate(data_mov), "yyyyMMdd")

        Try
            sSql = ""
            sSql = sSql + "Select " & vbNewLine
            sSql = sSql + " cod_mov_contribuicao" & vbNewLine
            sSql = sSql + ",tipo_mov" & vbNewLine
            sSql = sSql + ",tipo_cobranca" & vbNewLine
            sSql = sSql + ",cobrador" & vbNewLine
            sSql = sSql + ",cod_categoria" & vbNewLine
            sSql = sSql + ",cod_grupo" & vbNewLine
            sSql = sSql + ",cod_opera" & vbNewLine
            sSql = sSql + ",cod_especie" & vbNewLine
            sSql = sSql + ",data_mov" & vbNewLine
            sSql = sSql + ",data_venc" & vbNewLine
            sSql = sSql + ",cod_ean_cliente" & vbNewLine
            sSql = sSql + ",cod_contribuinte" & vbNewLine
            sSql = sSql + ",nome_contribuinte " & vbNewLine
            sSql = sSql + ",valor " & vbNewLine
            sSql = sSql + ",tipo_contrib " & vbNewLine
            sSql = sSql + ",confirmado " & vbNewLine
            sSql = sSql + ",cod_recibo " & vbNewLine
            sSql = sSql + ",dia " & vbNewLine
            sSql = sSql + ",cidade " & vbNewLine
            sSql = sSql + ",cod_cobrador " & vbNewLine
            sSql = sSql + ",cod_cidade " & vbNewLine
            sSql = sSql + ",cod_empresa " & vbNewLine
            sSql = sSql + ",situacao " & vbNewLine
            sSql = sSql + ",cod_bairro " & vbNewLine
            sSql = sSql + ",cod_usuario " & vbNewLine
            sSql = sSql + ",cod_regiao " & vbNewLine
            sSql = sSql + "from controle_db.dbo.mov_contribuicao_descr_tb" & vbNewLine
            sSql = sSql + " WHERE convert(date,data_mov) = '" & data & "'" & vbNewLine
            If operador <> "" Then
                sSql = sSql + " and cobrador = '" & operador & "'" & vbNewLine
            End If
            sSql = sSql + " and tipo_mov = 'BORDERO' "


            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function CarregaMovimentacoesBordero(ByVal cod_mov_contribuicao As Long) As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try
            sSql = ""
            sSql = sSql + "Select " & vbNewLine
            sSql = sSql + " cod_mov_contribuicao" & vbNewLine
            sSql = sSql + ",tipo_mov" & vbNewLine
            sSql = sSql + ",tipo_cobranca" & vbNewLine
            sSql = sSql + ",cobrador" & vbNewLine
            sSql = sSql + ",cod_categoria" & vbNewLine
            sSql = sSql + ",cod_grupo" & vbNewLine
            sSql = sSql + ",cod_opera" & vbNewLine
            sSql = sSql + ",cod_especie" & vbNewLine
            sSql = sSql + ",data_mov" & vbNewLine
            sSql = sSql + ",data_venc" & vbNewLine
            sSql = sSql + ",cod_ean_cliente" & vbNewLine
            sSql = sSql + ",cod_contribuinte" & vbNewLine
            sSql = sSql + ",nome_contribuinte " & vbNewLine
            sSql = sSql + ",valor " & vbNewLine
            sSql = sSql + ",tipo_contrib " & vbNewLine
            sSql = sSql + ",confirmado " & vbNewLine
            sSql = sSql + ",cod_recibo " & vbNewLine
            sSql = sSql + ",dia " & vbNewLine
            sSql = sSql + ",cidade " & vbNewLine
            sSql = sSql + ",cod_cobrador " & vbNewLine
            sSql = sSql + ",cod_cidade " & vbNewLine
            sSql = sSql + ",cod_empresa " & vbNewLine
            sSql = sSql + ",situacao " & vbNewLine
            sSql = sSql + ",cod_bairro " & vbNewLine
            sSql = sSql + ",cod_usuario " & vbNewLine
            sSql = sSql + ",cod_regiao " & vbNewLine
            sSql = sSql + "from controle_db.dbo.mov_contribuicao_descr_tb" & vbNewLine
            sSql = sSql + " where situacao = 3 and tipo_mov = 'BORDERO'" & vbNewLine
            If cod_mov_contribuicao <> 0 Then
                sSql = sSql + " AND cod_mov_contribuicao =" & cod_mov_contribuicao & vbNewLine
            End If

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function


    Public Function RetornaUltimaMovimentacao() As Long
        Dim sSql As String
        Dim ds As DataSet
        Dim cod_mov_contribuicao As Long

        Try
            sSql = ""
            sSql = sSql + "select MAX(substring(convert(varchar(10),cod_mov_contribuicao),2,10)) + 1 " & vbNewLine
            sSql = sSql + "from controle_db.dbo.mov_contribuicao_tb"

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    cod_mov_contribuicao = ds.Tables(0).Rows(0).Item(0)
                End If
            End If

            Return cod_mov_contribuicao

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return 0
        End Try
    End Function




    Public Function RetornaUltimoRecibo() As Long
        Dim sSql As String
        Dim ds As DataSet
        Dim cod_recibo As Long

        Try
            sSql = ""
            sSql = sSql + "select MAX(replace(cod_recibo,'*','')) " & vbNewLine
            sSql = sSql + "from controle_db..mov_contribuicao_descr_tb"

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    cod_recibo = ds.Tables(0).Rows(0).Item(0)
                End If
            End If

            Return cod_recibo

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return 0
        End Try
    End Function


    Public Function CarregaFichasSupervisaoOi(ByVal categoria As String, ByVal operador As String) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "select requisicao_id, cod_ean, DDD, telefone, valor, operador_id, operador, aceite" & vbNewLine
            sSql &= ",categoria, mesRef, mesRefSeguinte, dt_inclusao, nome_cliente, cpf, autorizante" & vbNewLine
            sSql &= ",observacao, liberado " & vbNewLine
            sSql &= "from controle_db.dbo.requisicao_oi_tb" & vbNewLine
            sSql &= "where liberado = 'N'" & vbNewLine
            sSql &= "and operador = '" & operador & "' and categoria = " & categoria


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function ConfirmarContribuicaoSupervisaoOi(ByVal filtro As CContribuinte, ByVal aceite As String) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= " UPDATE controle_db.dbo.requisicao_oi_tb  " & vbNewLine
            sSql &= " SET Liberado = '" & aceite & "' & vbNewLine"
            sSql &= " where telefone = '" & filtro.Telefone1 & "'"

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Sub verificaFichasComProblemas_SPU()
        Dim sSql As String

        Try

            sSql = ""
            sSql &= " exec controle_db.dbo.verificaFichasComProblemas_SPU  " & vbNewLine

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


        Catch ex As Exception

        End Try

    End Sub

    Public Sub Atualiza_data_ImpressaoFicha()
        Dim sSql As String

        Try

            sSql = ""
            sSql &= " update req" & vbNewLine
            sSql &= "set req.dt_impressao = GETDATE()" & vbNewLine
            sSql &= "from controle_db..requisicao_lista_tb req " & vbNewLine
            sSql &= "inner join controle_db..lista_contribuicao_liberacao_tmp_tb lib " & vbNewLine
            sSql &= "on req.cod_ean_cliente = lib.cod_ean_cliente" & vbNewLine

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            sSql = ""
            sSql &= " update mov" & vbNewLine
            sSql &= "set mov.dt_impressao = GETDATE()" & vbNewLine
            sSql &= "from controle_db..mov_contribuicao_descr_tb mov " & vbNewLine
            sSql &= "inner join controle_db..lista_contribuicao_liberacao_tmp_tb lib " & vbNewLine
            sSql &= "on mov.cod_ean_cliente = lib.cod_ean_cliente" & vbNewLine

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


        Catch ex As Exception

        End Try

    End Sub



#End Region

#Region "Lista Nova"

    Public Function CarregaListaNova(ByVal cidade As String, ByVal cod As Integer) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "select cod, nome_cliente, endereco, bairro, cidade, " & vbNewLine
            sSql &= "uf, cep, telefone1, cod_empresa, cod_cidade, cod_bairro,cod_regiao " & vbNewLine
            sSql &= "from controle_db.dbo." & cidade & vbNewLine
            sSql &= " where dt_inclusao_cadastro is null " & vbNewLine
            If cod > 0 Then
                sSql &= " and cod =" & cod
            End If

            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function IncluiRegistroListaNovaRequisicaoLista(ByVal contribuinte As CContribuinte, ByVal cidade As String) As Boolean
        Dim sSql As String
        Dim dia As Integer = 0
        Dim ds As New DataSet

        Try

            sSql = ""
            sSql &= "INSERT INTO controle_db.dbo.requisicao_lista_tb " & vbNewLine
            sSql &= "(requisicao_id, confirmado, tipo_mov, tipo_cobranca, cobrador " & vbNewLine
            sSql &= ", cod_categoria, cod_grupo, cod_opera, cod_especie, data_mov, cod_ean_cliente, cod_contribuinte, nome_contribuinte, valor " & vbNewLine
            sSql &= ", tipo_contrib, cod_recibo, dia, cidade, cod_cobrador, cod_cidade, cod_empresa, situacao, cod_bairro, cod_usuario, dt_nc " & vbNewLine
            sSql &= ", observacao, dt_inclusao, dt_alteracao, dt_impressao) " & vbNewLine
            sSql &= "values (99, 1, 'REQUISICAO', 'INSTITUICAO', 1, 10, 4, 1, 1, GETDATE()" & vbNewLine
            sSql &= ", '" & contribuinte.Cod_EAN_Cliente & "'," & contribuinte.Cod_contribuinte & ", '" & contribuinte.Nome_contribuinte & "'," & contribuinte.Valor & ", 1, 1, " & vbNewLine
            sSql &= dia & ", '" & contribuinte.Cidade & "',  1, " & contribuinte.Cod_Cidade & vbNewLine
            sSql &= ", 1, 1, " & contribuinte.Cod_Bairro & ", 99, '1999-11-11', null, getdate(), null, null)"


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function CarregaListaNovaOi(uf As String) As DataSet
        Try
            Dim sSql As String = String.Empty
            If uf.Equals("MG") Then
                sSql &= " select  SUBSTRING(convert(varchar(10),column3),0,3) as DDD,SUBSTRING(convert(varchar(10),column3),3,10)as telefone1,column6 as nome_cliente,Column7  + ' '+ isnull(Column8,'') as endereco, Column8 as Bairro,Column9 as CEP, Column10 as Cidade, case when cliente_oi = 'sim' then 'S' else 'N' end as clienteOi, 'MG' as UF, 1 as cod_empresa, 99 as cod_cidade, 99 as cod_bairro from lista_nova_db..cidades_mg_oi_tb "
            ElseIf uf.Equals("RJ") Then
                sSql &= "select  SUBSTRING(convert(varchar(10),column3),0,3) as DDD,SUBSTRING(convert(varchar(10),column3),3,10)as telefone1,column6 as nome_cliente,Column8  + ' '+ isnull(Column9,'') as endereco, Column10 as Bairro,Column11 as CEP, Column12 as Cidade, 'S' as clienteOi, 'RJ' as UF, 1 as cod_empresa, 99 as cod_cidade, 99 as cod_bairro from lista_nova_db..RJ_completa_tb  "
            ElseIf uf.Equals("BA") Then
                sSql &= "select  column5 as DDD, column6 as telefone1,column8 as nome_cliente, isnull(Column9,'') as endereco, Column10 as Bairro,Column13 as CEP, Column11 as Cidade, 'S' as clienteOi, 'BA' as UF, 1 as cod_empresa, 99 as cod_cidade, 99 as cod_bairro from lista_nova_db..BA_lista_completa_tb "
            ElseIf uf.Equals("RJ_2") Then
                sSql &= "Select substring(str(TELEFONE),0,3) as DDD, substring(str(TELEFONE),3,10)as telefone1, CLIENTE_NOME as nome_cliente, isnull(LOGRADOURO_NOME,'') as endereco, BAIRRO as Bairro, CEP as CEP, CIDADE as Cidade, 'S' as clienteOi, 'RJ' as UF, 1 as cod_empresa, 99 as cod_cidade, 99 as cod_bairro from lista_nova_db..[ LISTA_NOVA_RJ_MAIO_9_TB]"
            ElseIf uf.Equals("MIX") Then
                sSql &= "Select trim(str(DDD)) as DDD, substring(str(TELEFONE_FIXO),3,10)as telefone1, NOME as nome_cliente, isnull(ENDERECO,'') as endereco, BAIRRO as Bairro, CEP as CEP, MUNICIPIO as Cidade, 'S' as clienteOi, UF, 1 as cod_empresa, 99 as cod_cidade, 99 as cod_bairro from lista_nova_db..LISTA_NOVA_17_OUTUBRO_TB"
            ElseIf uf.Equals("RJ_3") Then
                sSql &= "Select substring(str(TELEFONE),0,3) as DDD, substring(str(TELEFONE),3,10)as telefone1, CLIENTE_NOME as nome_cliente, isnull(LOGRADOURO_NOME,'') as endereco, BAIRRO as Bairro, CEP as CEP, CIDADE as Cidade, 'S' as clienteOi, 'RJ' as UF, 1 as cod_empresa, 99 as cod_cidade, 99 as cod_bairro from lista_nova_db..LISTA_NOVA_21_OUTUBRO_TB where processado is null and executar = 'S'  "
            ElseIf uf.Equals("MG_2") Then
                sSql &= "Select substring(str(TELEFONE),0,3) as DDD, substring(str(TELEFONE),3,10)as telefone1, CLIENTE_NOME as nome_cliente, isnull(LOGRADOURO_NOME,'') as endereco, BAIRRO as Bairro, CEP as CEP, CIDADE as Cidade, 'S' as clienteOi, 'RJ' as UF, 1 as cod_empresa, 99 as cod_cidade, 99 as cod_bairro from lista_nova_db..LISTA_NOVA_22_OUTUBRO_TB where processado is null and executar = 'S'  "
            ElseIf uf.Equals("MG_3") Then
                sSql &= " Select substring(str(TELEFONE),0,3) as DDD, substring(str(TELEFONE),3,10)as telefone1, isnull (substring(RTRIM(LTRIM(STR(CELULAR, 25, 0))),3,20),'')as telefone2, CLIENTE_NOME as nome_cliente, isnull(LOGRADOURO_NOME,'') as endereco, BAIRRO as Bairro, CEP as CEP, CIDADE as Cidade, 'N' as clienteOi, UF, 1 as cod_empresa, 99 as cod_cidade, 99 as cod_bairro , doc as cpf,''as Identificacao from lista_nova_db..LISTA_NOVA_29_OUTUBRO_TB"
            ElseIf uf.Equals("CEMIG") Then
                ' sSql &= "Select substring(Telefone_Cemig,0,3) as DDD, substring(Telefone_Cemig,3,10)as telefone1, NOME as nome_cliente, isnull(Endereco,'') as endereco, BAIRRO as Bairro, CEP as CEP, CIDADE as Cidade, 'N' as clienteOi,  UF, 1 as cod_empresa, 99 as cod_cidade, 99 as cod_bairro , Identificação as Identificacao from lista_nova_db..LISTA_NOVA_CEMIG_0411_TB "
                'sSql &= "Select DDD, telefone As telefone1, NOME As nome_cliente, cpf, isnull(Endereco + ' ' + Numero + ' ' + complemento,'') as endereco, BAIRRO as Bairro, CEP as CEP, CIDADE as Cidade, 'N' as clienteOi,  UF, 1 as cod_empresa, 99 as cod_cidade, 99 as cod_bairro , [nº instalação] as Identificacao from lista_nova_db..LISTA_NOVA_CEMIG_06_NOV_TB"
                ' sSql &= "Select substring(RTRIM(LTRIM(STR(Telefone_Cemig, 25, 0))),0,3) as DDD, substring(RTRIM(LTRIM(STR(Telefone_Cemig, 25, 0))),3,10)as telefone1, NOME as nome_cliente, cpf,isnull(Endereco + ' ' + Numero + ' ' + complemento,'') as endereco, BAIRRO as Bairro, CEP as CEP, CIDADE as Cidade, 'N' as clienteOi,  UF, 1 as cod_empresa, 99 as cod_cidade, 99 as cod_bairro , Identificação as Identificacao from  lista_nova_db..MAILING_CEMIG_PF_0611_tb"
                sSql &= "Select case when fixo_1 <> '' then substring(FIXO_1,0,3) else substring(celular_1,0,3) end as DDD
                        , case when fixo_1 <> '' then substring(FIXO_1,3,10) else  substring(celular_1,3,10) end as telefone1
                        , NOME_COMPLETO as nome_cliente, cpf,isnull(Endereco,'')  as endereco, BAIRRO as Bairro, CEP as CEP, MUNICIPIO as Cidade, 'N' as clienteOi,  UF, 1 as cod_empresa, 99 as cod_cidade, 99 as cod_bairro , Identificacao as Identificacao 
                        FROM LISTA_NOVA_DB..LISTA_NOVA_CEMIG_0711_TB"
            End If
            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try

    End Function

    Public Function IncluiRegistroListaNovaRequisicaoOi(ByVal contribuinte As CContribuinte) As Boolean
        Dim sSql As String

        Try

            sSql = ""

            'sSql = ""
            'sSql &= "INSERT INTO controle_db.dbo.requisicao_tb " & vbNewLine
            'sSql &= "(numero_requisicao,cod_ean, DDD,telefone,valor,operador_id,operador,categoria,mesRef,dt_inclusao,usuario,nome_cliente,Atual)" & vbNewLine
            'sSql &= "Values (59, '" & contribuinte.Cod_EAN_Cliente & "', '" & contribuinte.DDD & "', '" & contribuinte.Telefone1 & "'," & contribuinte.Valor & ", 1, 'INSTITUIÇAO', " & vbNewLine
            'sSql &= contribuinte.Cod_Categoria & ", '09/2019', getdate(), 'Admin', '" & contribuinte.NomeCliente1 & "', 'S')"


            sSql &= "insert into controle_db..requisicao_Lista_tb " & vbNewLine
            sSql &= " (requisicao_id,confirmado,tipo_mov,tipo_cobranca,cobrador,cod_categoria,cod_grupo, " & vbNewLine
            sSql &= " cod_opera,cod_especie,data_mov,cod_ean_cliente,cod_contribuinte,nome_contribuinte, " & vbNewLine
            sSql &= " valor,tipo_contrib,cod_recibo,dia,cidade,cod_cobrador,cod_cidade,cod_empresa,situacao, " & vbNewLine
            sSql &= " cod_bairro,cod_usuario,dt_nc,observacao,dt_inclusao,dt_alteracao,dt_impressao) " & vbNewLine

            sSql &= " values " & vbNewLine
            sSql &= " (66,1,'REQUISICAO','INSTITUICAO','INSTITUICAO',10,4," & vbNewLine
            sSql &= " 1,1,getdate(), '" & contribuinte.Cod_EAN_Cliente & "', '" & contribuinte.CodCliente & "','" & contribuinte.NomeCliente1 & "'" & vbNewLine
            sSql &= " ,10,1,1,10,'" & contribuinte.Cidade & "', 1, '" & contribuinte.Cod_Cidade & "',1,1," & vbNewLine
            sSql &= " 99,99,'1999-11-11 00:00:00.000',null,getdate(),null,null)" & vbNewLine




            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            sSql = ""
            sSql &= "insert into controle_db..lista_telefonica_tb " & vbNewLine
            sSql &= "(cod_ean,DDD,telefone,valor,nome_cliente,categoria,mesSituacao,dt_inclusao,usuario,cod_cidade) " & vbNewLine
            sSql &= "Values ('" & contribuinte.Cod_EAN_Cliente & "', '" & contribuinte.DDD & "', '" & contribuinte.Telefone1 & "'," & contribuinte.Valor & vbNewLine
            sSql &= ", '" & contribuinte.NomeCliente1 & "' , 10, '09/2019', getdate(), 'ADMIN', " & vbNewLine
            sSql &= contribuinte.Cod_Cidade & " )"


            'ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            'MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function


    Public Function IncluiRegistroListaNovaRequisicaoCemig(contribuinte As CContribuinte,
                                                           numeroInstalacao As String) As Boolean
        Dim sSql As String

        Try

            sSql = ""
            sSql &= " insert into integracao_cemig_db.DBO.requisicao_TB                                                                                                            
                     (numero_requisicao, cod_ean, DDD, telefone, valor, operador_id, operador, aceite, categoria,nome_cliente,                                                        
                      mesRef, dt_inclusao, dt_alteracao, usuario,atual,Numero_instalacao)   "
            sSql &= $" values 
                    (01,'{contribuinte.Cod_EAN_Cliente}','{contribuinte.DDD}','{contribuinte.Telefone1}',10,1,'INSTITUICAO',NULL,10,'{contribuinte.NomeCliente1}',
                    '12/2024',GETDATE(), null,'Admin', 'S','{numeroInstalacao}') "


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            sSql = ""
            sSql &= "insert into integracao_cemig_db..lista_telefonica_tb " & vbNewLine
            sSql &= "(cod_ean,DDD,telefone,valor,nome_cliente,categoria,mesSituacao,dt_inclusao,usuario,cod_cidade) " & vbNewLine
            sSql &= "Values ('" & contribuinte.Cod_EAN_Cliente & "', '" & contribuinte.DDD & "', '" & contribuinte.Telefone1 & "'," & contribuinte.Valor & vbNewLine
            sSql &= ", '" & contribuinte.NomeCliente1 & "' , 10, '07/2023', getdate(), 'ADMIN', " & vbNewLine
            sSql &= contribuinte.Cod_Cidade & " )"


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            'MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function


    Public Function VerificaInclusaoCliente(DDD As String,
                                            telefone As String,
                                            ByRef cod_ean_existente As String,
                                            ByRef cod_cliente_existente As String,
                                            IsOi As Boolean) As Boolean
        Try
            Dim sSql As String
            Dim ds As DataSet

            sSql = ""
            If IsOi Then
                sSql &= "select cod_ean_cliente,cod_cliente from controle_db.dbo.cliente_tb" & vbNewLine
                sSql &= " where DDD = '" & DDD & "'" & vbNewLine
                sSql &= " and telefone1 = '" & telefone & "'" & vbNewLine

            Else
                sSql &= "select cod_ean_cliente,cod_cliente from integracao_cemig_db.dbo.cliente_tb" & vbNewLine
                sSql &= " where DDD = '" & DDD & "'" & vbNewLine
                sSql &= " and telefone1 = '" & telefone & "'" & vbNewLine
            End If

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    cod_ean_existente = ds.Tables(0).Rows(0).Item(0)
                    cod_cliente_existente = ds.Tables(0).Rows(0).Item(1)
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            'MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function VerificaInclusaoRequisicaLista(cod_ean_cliente As String,
                                                  integracao As String) As Boolean
        Try
            Dim sSql As String = String.Empty
            Dim ds As DataSet

            If integracao.Equals("Oi", StringComparison.InvariantCultureIgnoreCase) Then
                sSql = "select * from controle_db.dbo.requisicao_lista_tb" & vbNewLine
                sSql &= $" where cod_ean_cliente = '{cod_ean_cliente}'{vbNewLine}"
            ElseIf integracao.Equals("Cemig", StringComparison.InvariantCultureIgnoreCase) Then
                sSql = "select * from integracao_cemig_db.dbo.requisicao_tb" & vbNewLine
                sSql &= $" where cod_ean = '{cod_ean_cliente}'{vbNewLine}"
            End If

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    Return False
                End If
            End If

            Return True
        Catch ex As Exception
            'MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function RetornaCodCidade(ByVal cidade As String) As Integer
        Dim sSql As String
        Dim ds As DataSet
        Dim cod_cidade As Integer = 0

        Try

            sSql = ""

            sSql = ""
            sSql &= "select cod_cidade from controle_db.dbo.cidade_tb" & vbNewLine
            sSql &= " where nome_cidade = '" & cidade & "'" & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    cod_cidade = ds.Tables(0).Rows(0).Item(0)
                    Return cod_cidade
                Else
                    Return 0
                End If
            End If

            Return cod_cidade

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function

    Public Function ExclusaoOutraOperadora(ByVal DDD As String, ByVal telefone As String, ByVal cod_ean As String) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= " update controle_db.dbo.requisicao_tb  " & vbNewLine
            sSql &= " set atual = 'Q', aceite = 'Q', confirmado = 'Q' " & vbNewLine
            sSql &= " where atual = 'S' and DDD = '" & DDD & "' and telefone = '" & telefone & "'"
            sSql &= "" & vbNewLine
            sSql &= " delete from controle_db..lista_telefonica_tb    " & vbNewLine
            sSql &= " where DDD = '" & DDD & "' and telefone = '" & telefone & "'"
            sSql &= "" & vbNewLine
            sSql &= " DELETE FROM controle_db..requisicao_lista_tb " & vbNewLine
            sSql &= " where cod_ean_cliente = '" & cod_ean & "'"


            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function UpdateOutraOperadora(ByVal DDD As String, ByVal telefone As String, ByVal cod_ean As String) As Boolean
        Try
            Dim sSql As String

            sSql = ""
            sSql &= " UPDATE controle_db.dbo.requisicao_tb  " & vbNewLine
            sSql &= " SET aceite = 'O', confirmado = 'O' " & vbNewLine
            sSql &= " where atual = 'S' and telefone = '" & telefone & "' and ddd = '" & DDD & "'"
            sSql &= "" & vbNewLine
            sSql &= " update controle_db..lista_telefonica_tb    " & vbNewLine
            sSql &= " set situacao = 'O', dt_alteracao = getdate()    " & vbNewLine
            sSql &= " where telefone = '" & telefone & "' and ddd = '" & DDD & "'"
            sSql &= "" & vbNewLine
            sSql &= " update controle_db..requisicao_lista_tb " & vbNewLine
            sSql &= " set confirmado = 9, tipo_mov = 'OUTRA OPERADORA', DT_NC = GETDATE()   " & vbNewLine
            sSql &= " where cod_ean_cliente = '" & cod_ean & "'"

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)


            Return True

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Function CarregaFichasExportacaoOi() As DataSet
        Dim sSql As String
        Dim ds As DataSet = New DataSet

        Try
            sSql = ""
            sSql = sSql + "Select top 50" & vbNewLine
            sSql = sSql + "requisicao_id, cod_ean, ddd, telefone, valor, operador_id, operador,  " & vbNewLine
            sSql = sSql + "aceite, categoria, mesRef, mesRefSeguinte, dt_inclusao,  " & vbNewLine
            sSql = sSql + "dt_alteracao, usuario, nome_cliente, cpf, autorizante, observacao,  " & vbNewLine
            sSql = sSql + "nao_conforme, hist_aceite " & vbNewLine
            sSql = sSql + "into ##requisicao_exportar" & vbNewLine
            sSql = sSql + "from controle_db..requisicao_oi_tb " & vbNewLine
            sSql = sSql + "where exportado is null " & vbNewLine
            sSql = sSql + "select " & vbNewLine
            sSql = sSql + "cod_ean, ddd, telefone, valor, operador_id, operador,  " & vbNewLine
            sSql = sSql + "aceite, categoria, mesRef, mesRefSeguinte, dt_inclusao,  " & vbNewLine
            sSql = sSql + "dt_alteracao, usuario, nome_cliente, cpf, autorizante, observacao,  " & vbNewLine
            sSql = sSql + "nao_conforme, hist_aceite " & vbNewLine
            sSql = sSql + "from ##requisicao_exportar" & vbNewLine
            ' REALIZA O UPDATE DOS DADOS EXPORTADOS
            sSql = sSql + "update a" & vbNewLine
            sSql = sSql + "set exportado = 'S'  " & vbNewLine
            sSql = sSql + "from controle_db..requisicao_oi_tb a  " & vbNewLine
            sSql = sSql + "inner join ##requisicao_exportar b  " & vbNewLine
            sSql = sSql + "on a.requisicao_id = b.requisicao_id " & vbNewLine

            If Not IsDBNull(ds) Then
                ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
            Else
                ds = Nothing
            End If
            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function AtualizaFichasExportacaoOi() As Boolean
        Dim sSql As String

        Try
            sSql = ""
            sSql = sSql + "update a" & vbNewLine
            sSql = sSql + "set exportado = 'S'  " & vbNewLine
            sSql = sSql + "from controle_db..requisicao_oi_tb a  " & vbNewLine
            sSql = sSql + "inner join ##requisicao_exportar b  " & vbNewLine
            sSql = sSql + "on a.requisicao_id = b.requisicao_id " & vbNewLine

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function


    Public Function ImportaParaRequisicaoOi(ByVal dt As DataTable) As Boolean
        Dim sSql As String

        Try
            If Not IsDBNull(dt) Then
                ' ds.Tables(0).Rows(0).Item(0)
            End If

            sSql = ""
            sSql = sSql + "insert into controle_db..requisicao_oi_tb (cod_ean, ddd, telefone, valor, operador_id, operador,aceite, categoria, mesRef, mesRefSeguinte, dt_inclusao,dt_alteracao, usuario, nome_cliente, cpf, autorizante, observacao,nao_conforme, hist_aceite)" & vbNewLine
            sSql = sSql + "set exportado = 'S'  " & vbNewLine

            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function


#End Region

#Region "cemig"


    Public Function CarregaHistoricoContribuinteCemig(ByVal cod_ean As String) As DataSet
        Dim sSql As String

        Try

            sSql = ""
            sSql &= "exec integracao_cemig_db.dbo.HistoricoContribuinte_SPS " & vbNewLine
            sSql &= "@cod_ean = '" & cod_ean & "'"


            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function RetornaMovimento(movimento As String, novaInclusao As String) As DataSet
        Try
            Dim sSql As String = $"exec integracao_cemig_db..RetornaMovimento '{movimento}' ,'{novaInclusao}'"
            Return ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function RetornaNumeroRemessa() As Integer
        Try
            Dim sSql As String = $"select max(isnull(versao,1)) + 1 from integracao_cemig_db..arquivo_versao_gerado_tb"
            Dim remessa As Integer = 0
            Dim ds As DataSet = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    remessa = ds.Tables(0).Rows(0).Item(0)
                    Return remessa
                Else
                    Return 1
                End If
            End If
            Return remessa
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function RetornaAutorizacaoDebito(id As String, movimento As String) As String
        Try
            Dim sSql As String = $" exec integracao_cemig_db..AutorizacaoDebito {id}, 1 "
            Dim autorizacao As String = String.Empty
            Dim ds As DataSet = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    autorizacao = ds.Tables(0).Rows(0).Item(0)
                    Return autorizacao
                Else
                    Return String.Empty
                End If
            End If
            Return String.Empty
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function


    Public Function RetornaItemDebito(id As String, dataCobranca As String, movimento As String) As String
        Try
            Dim sSql As String = $" exec integracao_cemig_db..AutorizacaoDebito {id}, 2 "
            Dim autorizacao As String = String.Empty
            Dim ds As DataSet = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    autorizacao = ds.Tables(0).Rows(0).Item(0)
                    Return autorizacao
                Else
                    Return String.Empty
                End If
            End If
            Return String.Empty
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function RetornaGrupoParcelas(id As String, movimento As String) As String
        Try
            Dim sSql As String = $" exec integracao_cemig_db..AutorizacaoDebito {id}, 3 "
            Dim autorizacao As String = String.Empty
            Dim ds As DataSet = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
            If (ds.Tables(0).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(0).Rows(0).Item(0)) Then
                    autorizacao = ds.Tables(0).Rows(0).Item(0)
                    Return autorizacao
                Else
                    Return String.Empty
                End If
            End If
            Return String.Empty
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function AtualizaRegistroProcessado(id As String) As Boolean
        Try
            Dim sSql As String = $" update integracao_cemig_db..requisicao_cemig_tb 
                                    set exportado = 'S'                                        
                                    where requisicao_id ={id}"
            Dim autorizacao As String = String.Empty
            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
            Return True
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function

    Public Function InsereControleProcessamento(movimento As Integer,
                                                versao As Integer,
                                                qtdRegistros As Integer,
                                                qtdLinhas As Integer) As Boolean
        Try
            Dim sSql As String = $" insert into integracao_cemig_db..arquivo_versao_gerado_tb
                                    (layout_id,versao,qtd_registros,qtd_linhas,dt_inclusao,usuario)
                                    values ({movimento},{versao},{qtdRegistros},{qtdLinhas},getdate(),'AACI')"
            Dim autorizacao As String = String.Empty
            ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)
            Return True
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
    End Function


#End Region


End Class

