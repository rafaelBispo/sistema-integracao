﻿Public Class RelatoriosDAO


#Region "Variáveis"


    Private Cod_Cidade As Integer
    Private Nome_Cidade As String
    Private Uf_Cidade As String

    Private Cod_Bairro As Integer
    Private Nome_Bairro As String
    Private Cep_Bairro As String
    Private Cod_Cidade_Bairro As Integer
    Private Cod_Regiao_Bairro As Integer

    Private ConnectionHelper As Util.ConnectionHelper


#End Region


#Region "Construtores"

    Sub New(Optional ByVal p_Cod_Cidade As Decimal = 0, _
        Optional ByVal p_Nome_Cidade As String = "", _
        Optional ByVal p_Uf_Cidade As String = "", _
        Optional ByVal p_Cod_Bairro As Integer = 0, _
        Optional ByVal p_Nome_Bairro As String = "", _
        Optional ByVal p_Cep_Bairro As Integer = 0, _
        Optional ByVal p_Cod_Cidade_Bairro As Integer = 0, _
        Optional ByVal p_Cod_Regiao_Bairro As Integer = 0)

        Cod_Cidade = p_Cod_Cidade
        Nome_Cidade = p_Nome_Cidade
        Uf_Cidade = p_Uf_Cidade
        Cod_Bairro = p_Cod_Bairro
        Nome_Bairro = p_Nome_Bairro
        Cep_Bairro = p_Cep_Bairro
        Cod_Cidade_Bairro = p_Cod_Cidade_Bairro
        Cod_Regiao_Bairro = p_Cod_Regiao_Bairro


        ConnectionHelper = New Util.ConnectionHelper(System.Reflection.Assembly.GetExecutingAssembly)

    End Sub

    Sub New(ByRef p_connectionHelper As Util.ConnectionHelper, _
           Optional ByVal p_Cod_Cidade As Decimal = 0, _
           Optional ByVal p_Nome_Cidade As String = "", _
           Optional ByVal p_Uf_Cidade As String = "", _
           Optional ByVal p_Cod_Bairro As Integer = 0, _
           Optional ByVal p_Nome_Bairro As String = "", _
           Optional ByVal p_Cep_Bairro As Integer = 0, _
           Optional ByVal p_Cod_Cidade_Bairro As Integer = 0, _
           Optional ByVal p_Cod_Regiao_Bairro As Integer = 0)

        Cod_Cidade = p_Cod_Cidade
        Nome_Cidade = p_Nome_Cidade
        Uf_Cidade = p_Uf_Cidade
        Cod_Bairro = p_Cod_Bairro
        Nome_Bairro = p_Nome_Bairro
        Cep_Bairro = p_Cep_Bairro
        Cod_Cidade_Bairro = p_Cod_Cidade_Bairro
        Cod_Regiao_Bairro = p_Cod_Regiao_Bairro

        ConnectionHelper = p_connectionHelper

    End Sub


#End Region



#Region "metodos"


    Public Function GeraRecibo() As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try
            sSql = ""
            sSql = sSql + "SELECT T_CLIENTES.nome_cliente, T_CLIENTES.endereco, T_CLIENTES.bairro, T_CLIENTES.cod_ean_cliente, T_CLIENTES.data_cadastro, T_ESPECIE.nome_especie, T_ULT_MOV.dt_mov, T_ULT_MOV.valor, T_CLIENTES.cidade, T_CLIENTES.telefone1, T_CLIENTES.cod_grupo, T_CLIENTES.cod_categoria, T_CLIENTES.dia_limite, T_CLIENTES.observacao, T_CLIENTES.telefone2, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.data_mov, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.valor, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_categoria, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.confirmado, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_recibo, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_opera, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_regiao, T_CLIENTES.cod_regiao, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.valor_ext " & vbNewLine
            sSql = sSql + "FROM   ((controle_db.dbo.lista_contribuicao_liberacao_tmp_tb T_LISTA_CONTRIBUICAO_LIB_TMP1_1 INNER JOIN controle_db.dbo.cliente_tb T_CLIENTES ON T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_ean_cliente=T_CLIENTES.cod_ean_cliente) INNER JOIN controle_db.dbo.especie_tb T_ESPECIE ON (T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_especie=T_ESPECIE.cod_especie) AND (T_CLIENTES.cod_especie=T_ESPECIE.cod_especie)) INNER JOIN controle_db.dbo.ult_mov_tb T_ULT_MOV ON T_CLIENTES.cod_ean_cliente=T_ULT_MOV.cod_ean_cliente " & vbNewLine
            sSql = sSql + "ORDER BY T_CLIENTES.cod_regiao, T_CLIENTES.nome_cliente " & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function GeraReciboRetido() As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try
            sSql = ""
            sSql = sSql + "SELECT T_CLIENTES.nome_cliente, T_CLIENTES.endereco, T_CLIENTES.bairro, T_CLIENTES.cod_ean_cliente, T_CLIENTES.DATA_CAD, T_ESPECIE.nome_especie, T_ULT_MOV.dt_mov, T_ULT_MOV.valor, T_CLIENTES.cidade, T_CLIENTES.telefone1, T_CLIENTES.cod_grupo, T_CLIENTES.cod_categoria, T_CLIENTES.dia_limite, T_CLIENTES.observacao, T_CLIENTES.telefone2, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.data_mov, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.valor, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_categoria, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.confirmado, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_recibo, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_opera, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_regiao, T_CLIENTES.cod_regiao, T_LISTA_CONTRIBUICAO_LIB_TMP1_1.valor_ext " & vbNewLine
            sSql = sSql + "FROM   ((controle_db.dbo.lista_contribuicao_liberacao_tmp_tb T_LISTA_CONTRIBUICAO_LIB_TMP1_1 INNER JOIN controle_db.dbo.LISTA_RETIDA_TMP_TB T_CLIENTES ON T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_ean_cliente=T_CLIENTES.cod_ean_cliente) INNER JOIN controle_db.dbo.especie_tb T_ESPECIE ON (T_LISTA_CONTRIBUICAO_LIB_TMP1_1.cod_especie=T_ESPECIE.cod_especie) AND (T_CLIENTES.cod_especie=T_ESPECIE.cod_especie)) INNER JOIN controle_db.dbo.ult_mov_tb T_ULT_MOV ON T_CLIENTES.cod_ean_cliente=T_ULT_MOV.cod_ean_cliente " & vbNewLine
            sSql = sSql + "ORDER BY T_CLIENTES.cod_regiao, T_CLIENTES.nome_cliente " & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function

    Public Function FichasAbertasAnalitico() As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try
            sSql = ""

            sSql = sSql + "select   requisicao_lista_tb.cod_ean_cliente" & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.nome_contribuinte  " & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.cidade  " & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.dia" & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.tipo_mov" & vbNewLine
            sSql = sSql + ", case when requisicao_lista_tb.confirmado = 1 then 'Confirmado' else 'A confirmar' end as confirmado" & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.cod_categoria" & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.valor" & vbNewLine
            sSql = sSql + ", usuario_tb.usuario " & vbNewLine
            sSql = sSql + "from controle_db.dbo.requisicao_lista_tb requisicao_lista_tb" & vbNewLine
            sSql = sSql + "INNER JOIN controle_db.dbo.usuario_tb usuario_tb" & vbNewLine
            sSql = sSql + "on usuario_tb.cod_funcionario = requisicao_lista_tb.cod_opera" & vbNewLine
            sSql = sSql + "where tipo_mov in ('ABERTO', 'BORDERO')" & vbNewLine
            sSql = sSql + "and usuario_tb.cod_funcionario not in (1,12)" & vbNewLine
            sSql = sSql + "order by requisicao_lista_tb.cidade  " & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.dia,usuario_tb.cod_funcionario" & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function

    Public Function FichasAbertasAnaliticoPorCidade(ByVal cidade As String) As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try
            sSql = ""

            sSql = sSql + "select requisicao_lista_tb.cod_ean_cliente" & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.nome_contribuinte  " & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.cidade  " & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.dia" & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.tipo_mov" & vbNewLine
            sSql = sSql + ", case when requisicao_lista_tb.confirmado = 1 then 'Confirmado' else 'A confirmar' end as confirmado" & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.cod_categoria" & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.valor" & vbNewLine
            sSql = sSql + ", usuario_tb.usuario " & vbNewLine
            sSql = sSql + "from controle_db.dbo.requisicao_lista_tb requisicao_lista_tb" & vbNewLine
            sSql = sSql + "INNER JOIN controle_db.dbo.usuario_tb usuario_tb" & vbNewLine
            sSql = sSql + "on usuario_tb.cod_funcionario = requisicao_lista_tb.cod_opera" & vbNewLine
            sSql = sSql + "where tipo_mov in ('ABERTO', 'BORDERO')" & vbNewLine
            sSql = sSql + "and usuario_tb.cod_funcionario not in (1,12)" & vbNewLine
            sSql = sSql + "and requisicao_lista_tb.cidade = '" & cidade & "'" & vbNewLine
            sSql = sSql + "order by requisicao_lista_tb.cidade  " & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.dia,usuario_tb.cod_funcionario" & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try



    End Function


    Public Function FichasAbertasSintetico() As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try
            sSql = ""

            sSql = sSql + "select  requisicao_lista_tb.cidade  " & vbNewLine
            sSql = sSql + ", , requisicao_lista_tb.dia" & vbNewLine
            sSql = sSql + ", , sum(requisicao_lista_tb.valor) as valor" & vbNewLine
            sSql = sSql + "from controle_db.dbo.requisicao_lista_tb requisicao_lista_tb" & vbNewLine
            sSql = sSql + "INNER JOIN controle_db.dbo.usuario_tb usuario_tb" & vbNewLine
            sSql = sSql + "on usuario_tb.cod_funcionario = requisicao_lista_tb.cod_opera" & vbNewLine
            sSql = sSql + "where tipo_mov in ('ABERTO', 'BORDERO')" & vbNewLine
            sSql = sSql + "and usuario_tb.cod_funcionario not in (1,12)" & vbNewLine
            sSql = sSql + "group by requisicao_lista_tb.cidade  " & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.dia" & vbNewLine
            sSql = sSql + "order by requisicao_lista_tb.cidade  " & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.dia" & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try



    End Function

    Public Function FichasEmAbertoAnalitico() As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try
            sSql = ""

            sSql = sSql + "select " & vbNewLine
            sSql = sSql + "requisicao_lista_tb.cod_ean_cliente" & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.nome_contribuinte  " & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.cidade  " & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.dia" & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.tipo_mov" & vbNewLine
            sSql = sSql + ", case when requisicao_lista_tb.confirmado = 1 then 'Confirmado' else 'A confirmar' end as confirmado" & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.cod_categoria" & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.valor" & vbNewLine
            sSql = sSql + ", usuario_tb.usuario " & vbNewLine
            sSql = sSql + "from controle_db.dbo.requisicao_lista_tb requisicao_lista_tb" & vbNewLine
            sSql = sSql + "INNER JOIN controle_db.dbo.usuario_tb usuario_tb" & vbNewLine
            sSql = sSql + "on usuario_tb.cod_funcionario = requisicao_lista_tb.cod_opera" & vbNewLine
            sSql = sSql + "where tipo_mov in ('REQUISICAO', 'LIBERACAO')" & vbNewLine
            sSql = sSql + "and usuario_tb.cod_funcionario not in (1,12)" & vbNewLine
            sSql = sSql + "order by requisicao_lista_tb.cidade  " & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.dia,usuario_tb.cod_funcionario" & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try


    End Function

    Public Function FichasEmAbertoAnaliticoPorCidade(ByVal cidade As String) As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try
            sSql = ""

            sSql = sSql + "select requisicao_lista_tb.cod_ean_cliente" & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.nome_contribuinte  " & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.cidade  " & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.dia" & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.tipo_mov" & vbNewLine
            sSql = sSql + ", case when requisicao_lista_tb.confirmado = 1 then 'Confirmado' else 'A confirmar' end as confirmado" & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.cod_categoria" & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.valor" & vbNewLine
            sSql = sSql + ", usuario_tb.usuario " & vbNewLine
            sSql = sSql + "from controle_db.dbo.requisicao_lista_tb requisicao_lista_tb" & vbNewLine
            sSql = sSql + "INNER JOIN controle_db.dbo.usuario_tb usuario_tb" & vbNewLine
            sSql = sSql + "on usuario_tb.cod_funcionario = requisicao_lista_tb.cod_opera" & vbNewLine
            sSql = sSql + "where tipo_mov in ('REQUISICAO', 'LIBERACAO')" & vbNewLine
            sSql = sSql + "and usuario_tb.cod_funcionario not in (1,12)" & vbNewLine
            sSql = sSql + "and requisicao_lista_tb.cidade = '" & cidade & "'" & vbNewLine
            sSql = sSql + "order by requisicao_lista_tb.cidade  " & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.dia,usuario_tb.cod_funcionario" & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try



    End Function

    Public Function FichasNaoImpressasOperadorCidade(ByVal cidade As String, ByVal Operador As String) As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try
            sSql = ""

            sSql = sSql + "select requisicao_lista_tb.cod_ean_cliente" & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.nome_contribuinte  " & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.cidade  " & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.dia" & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.tipo_mov" & vbNewLine
            sSql = sSql + ", case when requisicao_lista_tb.confirmado = 1 then 'Confirmado' else 'A confirmar' end as confirmado" & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.cod_categoria" & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.valor" & vbNewLine
            sSql = sSql + ", usuario_tb.usuario " & vbNewLine
            sSql = sSql + "from controle_db.dbo.requisicao_lista_tb requisicao_lista_tb" & vbNewLine
            sSql = sSql + "INNER JOIN controle_db.dbo.usuario_tb usuario_tb" & vbNewLine
            sSql = sSql + "on usuario_tb.cod_funcionario = requisicao_lista_tb.cod_opera" & vbNewLine
            sSql = sSql + "where situacao = 2" & vbNewLine
            sSql = sSql + "and usuario_tb.cod_funcionario not in (1,12)" & vbNewLine
            sSql = sSql + "and requisicao_lista_tb.cidade = '" & cidade & "'" & vbNewLine
            sSql = sSql + "and usuario_tb.usuario = '" & Operador & "'" & vbNewLine
            sSql = sSql + "order by requisicao_lista_tb.cidade  " & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.dia,usuario_tb.cod_funcionario" & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try



    End Function


    Public Function FichasEmAbertoSintetico() As DataSet
        Dim sSql As String
        Dim ds As DataSet

        Try
            sSql = ""

            sSql = sSql + "select  requisicao_lista_tb.cidade  " & vbNewLine
            sSql = sSql + ", , requisicao_lista_tb.dia" & vbNewLine
            sSql = sSql + ", , sum(requisicao_lista_tb.valor) as valor" & vbNewLine
            sSql = sSql + "from controle_db.dbo.requisicao_lista_tb requisicao_lista_tb" & vbNewLine
            sSql = sSql + "INNER JOIN controle_db.dbo.usuario_tb usuario_tb" & vbNewLine
            sSql = sSql + "on usuario_tb.cod_funcionario = requisicao_lista_tb.cod_opera" & vbNewLine
            sSql = sSql + "where tipo_mov in ('REQUISICAO', 'LIBERACAO')" & vbNewLine
            sSql = sSql + "and usuario_tb.cod_funcionario not in (1,12)" & vbNewLine
            sSql = sSql + "group by requisicao_lista_tb.cidade  " & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.dia" & vbNewLine
            sSql = sSql + "order by requisicao_lista_tb.cidade  " & vbNewLine
            sSql = sSql + ", requisicao_lista_tb.dia" & vbNewLine

            ds = ConnectionHelper.ExecuteSQL(CommandType.Text, sSql)

            Return ds

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try



    End Function


#End Region



End Class
