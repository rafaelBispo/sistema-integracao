﻿Public Class tapiRequestMakeCall

    Declare Auto Function tapiRequestMakeCall Lib "TAPI32.dll" (ByVal DestAddress As String, ByVal AppName As String, ByVal CalledParty As String, ByVal Comment As String) As Integer
    Const TAPIERR_CONNECTED As Short = 0
    Const TAPIERR_NOREQUESTRECIPIENT As Short = -2
    Const TAPIERR_REQUESTQUEUEFULL As Short = -3
    Const TAPIERR_INVALDESTADDRESS As Short = -4

    Function chamar(ByVal numero)
        Dim Str As String
        Dim t As Short
        Dim buff As String
        Str = Trim(numero)
        Try
            t = tapiRequestMakeCall(Str, "Dial", Str, "")
        Catch ex As Exception
            Return "Error"
        End Try
        If t <> 0 Then
            buff = "Error"
            Select Case t
                Case TAPIERR_NOREQUESTRECIPIENT
                    buff = buff & "Não foi possivel iniciar a discagem"
                Case TAPIERR_REQUESTQUEUEFULL
                    buff = buff & "A fila de solicitações pendentes de discagem do Windows está cheia."
                Case TAPIERR_INVALDESTADDRESS
                    buff = buff & "O numero de telefone não é valido."
                Case Else
                    buff = buff & "Erro desconhecido."
            End Select
        Else
            buff = "Discando"
        End If
        Return buff
    End Function

End Class
