﻿using System;
using System.Security;
using System.Windows.Forms;
using System.IO;
using System.Net;

namespace FTP_Upload_Download
{

    public partial class Form1 : Form
    {

        public string pCaminhoArquivo = "";

        public string CaminhoArquivo
        {
            get { return pCaminhoArquivo; }
            set { pCaminhoArquivo = value; }
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void btnEnviarArquivo_Click(object sender, EventArgs e)
        {
            if (validaInformacaoServidorFTP())
            {
                if (!string.IsNullOrEmpty(txtArquivoUpload.Text))
                {
                    //string urlArquivoEnviar = txtEnderecoServidorFTP.Text + "/sistemas/" + Path.GetFileName(txtArquivoUpload.Text);
                        string urlArquivoEnviar = txtEnderecoServidorFTP.Text + Path.GetFileName(txtArquivoUpload.Text);
                    try
                    {
                        lblStatus.Text = "Preparando para envio";
                        lblStatus.ForeColor = System.Drawing.Color.Blue;
                        FTP.EnviarArquivoFTP(txtArquivoUpload.Text, urlArquivoEnviar,txtUsuario.Text,txtSenha.Text);
                        lblStatus.Text = "Arquivo Enviado";
                        lblStatus.ForeColor = System.Drawing.Color.Green;
                        MessageBox.Show("Arquivo enviado com sucesso!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch(Exception ex)
                    {
                        lblStatus.Text = "Erro ao enviar o arquivo";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        MessageBox.Show("Erro " + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                lblStatus.Text = "Erro ao enviar o arquivo";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                MessageBox.Show("Informações do sevidor incompletas", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBaixarArquivo_Click(object sender, EventArgs e)
        {
            lblStatus.Text = "Validando arquivo...";
            lblStatus.ForeColor = System.Drawing.Color.Blue;

            if (validaInformacaoServidorFTP())
            {
                if (validaInformacaoDownload())
                {
                    try
                    {
                        lblStatus.Text = "Realizando download do arquivo...";
                        lblStatus.ForeColor = System.Drawing.Color.Blue;
                        FTP.BaixarArquivoFTP(txtArquivoDownload.Text, txtBaixarPara.Text,txtUsuario.Text,txtSenha.Text);
                        lblStatus.Text = "Download Realizado!";
                        lblStatus.ForeColor = System.Drawing.Color.Green;
                        MessageBox.Show("Download de arquivo realizado com sucesso!", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ProcessaArquivoCSV(txtBaixarPara.Text);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Erro " + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Informações para download incompletas", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Informações do sevidor incompletas", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
           }
        }

        private bool validaInformacaoServidorFTP()
        {
            if (string.IsNullOrEmpty(txtUsuario.Text) || string.IsNullOrEmpty(txtSenha.Text) || string.IsNullOrEmpty(txtEnderecoServidorFTP.Text))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool validaInformacaoDownload()
        {
            if (string.IsNullOrEmpty(txtArquivoDownload.Text) || string.IsNullOrEmpty(txtBaixarPara.Text))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void btnProcurar_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd1 = new OpenFileDialog();
            //define as propriedades do controle 
            //OpenFileDialog
            ofd1.Multiselect = false;
            ofd1.Title = "Selecionar Arquivos";
            ofd1.InitialDirectory = @"C:\Dados\";
            //filtra para exibir somente arquivos de imagens
            ofd1.Filter = "All files (*.*)|*.*";
            ofd1.CheckFileExists = true;
            ofd1.CheckPathExists = true;
            ofd1.RestoreDirectory = true;
            
            DialogResult dr = ofd1.ShowDialog();

            if (dr == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    txtArquivoUpload.Text = ofd1.FileName;
                }
                catch (SecurityException ex)
                {
                    // O usuário  não possui permissão para ler arquivos
                    MessageBox.Show("Erro de segurança Contate o administrador de segurança da rede.\n\n" +
                                                "Mensagem : " + ex.Message + "\n\n" +
                                                "Detalhes (enviar ao suporte):\n\n" + ex.StackTrace);
                }
                catch (Exception ex)
                {
                    // Não pode carregar a imagem (problemas de permissão)
                    MessageBox.Show("Você pode não ter permissão para ler o arquivo , ou " +
                                               " ele pode estar corrompido.\n\nErro reportado : " + ex.Message);
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {

                lblStatus.Text = "Iniciando Sincronização";
                lblStatus.ForeColor = System.Drawing.Color.Yellow;

                string urlArquivoEnviar = txtEnderecoServidorFTP.Text + Path.GetFileName(txtArquivoUpload.Text);

                FtpWebRequest fwr = (FtpWebRequest)FtpWebRequest.Create(new Uri(urlArquivoEnviar));

                fwr.Credentials = new NetworkCredential(txtUsuario.Text, txtSenha.Text);

                fwr.Method = WebRequestMethods.Ftp.ListDirectory;

                fwr.EnableSsl = true;

                fwr.UsePassive = true;

                StreamReader sr = new StreamReader(fwr.GetResponse().GetResponseStream());

                string str = sr.ReadLine();

                while (str != null)
                {

                    listArquivos.Items.Add(str);
                    Console.WriteLine(str);


                    str = sr.ReadLine();
                }

                sr.Close();

                //sr = Nothing;

                //fwr = Nothing;

                if (CaminhoArquivo != "")
                {
                    txtArquivoUpload.Text = CaminhoArquivo;
                }


                lblStatus.Text = "FTP Sincronizado!";
                lblStatus.ForeColor = System.Drawing.Color.Green;


            }
            catch (Exception ex)
            {
                lblStatus.Text = "Falha na sincronização!";
                lblStatus.ForeColor = System.Drawing.Color.Red;
                // Não pode carregar a imagem (problemas de permissão)
                MessageBox.Show("Não foi possivel realizar a sincronização " + ex.Message);
            }
        }

        private void listArquivos_SelectedIndexChanged(object sender, EventArgs e)
        {
            string arquivo = "";

            try
            {
                arquivo = listArquivos.GetItemText(listArquivos.SelectedItem);
                txtArquivoDownload.Text = txtArquivoDownload.Text + arquivo;
                txtBaixarPara.Text = txtBaixarPara.Text + arquivo;
                btnBaixarArquivo.Enabled = true;
            }
            catch (Exception ex)
            {
               MessageBox.Show("Ocorreu um erro ao validar o arquivo" + ex.Message);
            }

            
        }

        public void ProcessaArquivoCSV(string diretorioArquivo)
        {

            try
            {

                DialogResult result = MessageBox.Show("Deseja processar o arquivo recebido gerando a planilha de consulta?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    GerenciaPlanilha.LerArquivoCSV(diretorioArquivo);
                    MessageBox.Show("O arquivo Excel foi criado com sucesso!");
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro ao gerar o arquivo" + ex.Message);
            }
        }


    }
}
