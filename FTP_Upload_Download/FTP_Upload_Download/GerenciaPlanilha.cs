﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using SISINT001;
using System.Data;


namespace FTP_Upload_Download
{
    class GerenciaPlanilha
    {
        //Referencia Necessária para realizar a leitura do arquivo 

        public static void LerArquivoCSV(string local)
        {  
              try 
            {
                string telefones = "";
                RNGenerico rn = new RNGenerico();
                DataSet dataSet = new DataSet("Planilha");


                //Declaro o StreamReader para o caminho onde se encontra o arquivo 
                StreamReader rd = new StreamReader(local); 
                //Declaro uma string que será utilizada para receber a linha completa do arquivo 
                string linha = null; 
                //Declaro um array do tipo string que será utilizado para adicionar o conteudo da linha separado 
                string[] linhaseparada = null; 
                //realizo o while para ler o conteudo da linha 
                while ((linha = rd.ReadLine()) != null) 
                { 
                    //com o split adiciono a string 'quebrada' dentro do array 
                    linhaseparada = linha.Split(';'); 
                    //aqui incluo o método necessário para continuar o trabalho 
                    if (!linhaseparada[2].Contains("telefone"))
                    {
                        telefones += "'" + linhaseparada[2] + "',";
                    }
                } 
                rd.Close();

                //apaga o ultimo caracter da string retirando o ","
                telefones = telefones.Remove(telefones.Length - 1);

                //dataSet = rn.RetornaLigacoesPlanilhaOi(telefones);
                //CriarArquivoExcel(dataSet, local);

            } 
            catch 
            {
                throw;
            } 
        }

        private static void CriarArquivoExcel(DataSet dados, string local)
        {
            try
            {

                //local = local.Remove(local.Length - 3);
                //local += "xls";


                //Microsoft.Office.Interop.Excel.Application xlApp;
                //Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
                //Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
                //object misValue = System.Reflection.Missing.Value;

                //xlApp = new Microsoft.Office.Interop.Excel.Application();
                
                //xlWorkBook = xlApp.Workbooks.Add(misValue);

                //xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                ////MONTA CABEÇALHO
                //xlWorkSheet.Cells[1, 1] = "DDD";
                //xlWorkSheet.Cells[1, 2] = "Telefone";
                //xlWorkSheet.Cells[1, 3] = "Nome Cliente";
                //xlWorkSheet.Cells[1, 4] = "Operador";
                //xlWorkSheet.Cells[1, 5] = "data Inclusao";

                //int n = 2;
                //foreach (DataRow dr in dados.Tables[0].Rows)
                //{
                //    xlWorkSheet.Cells[n, 1] = dr[0].ToString();
                //    xlWorkSheet.Cells[n, 2] = dr[1].ToString();
                //    xlWorkSheet.Cells[n, 3] = dr[2].ToString();
                //    xlWorkSheet.Cells[n, 4] = dr[3].ToString();
                //    xlWorkSheet.Cells[n, 5] = dr[4].ToString();
                //    n = n + 1; 
                //}

                //xlApp.Columns.AutoFit();
                //xlWorkBook.SaveAs(local, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue,
                //Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                //xlWorkBook.Close(true, misValue, misValue);
                //xlApp.Quit();

                //liberarObjetos(xlWorkSheet);
                //liberarObjetos(xlWorkBook);
                //liberarObjetos(xlApp);

                
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private static void liberarObjetos(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                obj = null;
                throw;
            }
            finally
            {
                GC.Collect();
            }
        }



    }
}
