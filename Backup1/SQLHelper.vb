Imports System
Imports System.IO
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices
Imports Alianca.Seguranca.BancoDados
Imports System.Data.SqlClient

Imports Alianca.Seguranca.Web

'<Assembly: ApplicationName("SABL0101")> 
'<Assembly: ApplicationActivation(ActivationOption.Server)> 
'<Assembly: ApplicationAccessControl(False, AccessChecksLevel:=AccessChecksLevelOption.ApplicationComponent)> 
Namespace Alianca
    Namespace Seguranca
        Namespace BancoDados
#Region "cCon"
            ' The SQL class is intended to encapsulate high performance, scalable best practices for 
            ' common uses of SqlClient.

            ' ===============================================================================
            ' Release history
            ' VERSION	DESCRIPTION
            '   2.0	Added support for FillDataset, UpdateDataset and "Param" helper methods
            '
            ' ===============================================================================

            Public NotInheritable Class cCon

                Public Enum Ambientes
                    Produ��o = 2
                    Qualidade = 3
                    Desenvolvimento = 4
                    Extrator = 5
                    PRODUCAO_ABS = 6
                    QUALIDADE_ABS = 7
                    DESENVOLVIMENTO_ABS = 8
                End Enum
                Public Enum TimeTimeout
                    Padr�o = 30
                    Pequeno = 60
                    M�dio = 120
                    Grande = 240
                    Enorme = 600
                    Infinito = 0
                End Enum
#Region "vari�veis das propriedades"
                Private Shared ConnectionString As String
                'Private Shared Connection As SqlConnection
                Private Shared Transaction() As SqlTransaction
                Private Shared TranConnection() As SqlConnection
                Private Shared _configurado As Boolean
                Private Shared configurando As Boolean
                Private Shared _bancodedados As String
                Private Shared _AssemblyInfo As System.Reflection.Assembly
                Private Shared Trace As New DataSet
                Private Shared ConnectionOleDB As OleDb.OleDbConnection
                Private Shared _ConnectionTimeout As TimeTimeout = TimeTimeout.Infinito
                'A variavel indica se as variaveis passadas para a configura��o da
                'conex�o � valida
                Protected Shared _AplicacaoValida As Boolean
                Protected Shared _Sistema As String
                Protected Shared _Aplicacao As String
                Protected Shared _Ambiente As Ambientes
                Protected Shared DescricaoArquivo As String
                Protected Shared _DescricaoErro As String

#End Region
#Region "private utility methods, constructors e propriedades"

                Public Shared ReadOnly Property configurado() As Boolean
                    Get
                        configurado = _configurado
                    End Get
                End Property

                Public Shared ReadOnly Property AssemblyInfo() As System.Reflection.Assembly
                    Get
                        AssemblyInfo = _AssemblyInfo
                    End Get
                End Property
                Public Shared ReadOnly Property DescricaoErro() As String
                    Get
                        DescricaoErro = _DescricaoErro
                    End Get
                End Property
                Public Shared ReadOnly Property AplicacaoValida() As Boolean
                    Get
                        AplicacaoValida = _AplicacaoValida
                    End Get
                End Property
                Public Shared ReadOnly Property Sistema() As String
                    Get
                        Sistema = _Sistema
                    End Get
                End Property
                Public Shared ReadOnly Property Aplicacao() As String
                    Get
                        Aplicacao = _Aplicacao
                    End Get
                End Property
                Public Shared ReadOnly Property Ambiente() As Ambientes
                    Get
                        Ambiente = _Ambiente
                    End Get
                End Property
                ' Since this class provides only static methods, make the default constructor private to prevent 
                ' instances from being created with "new SQL(Sistema, nome_Aplicacao, Ambiente, chave_Aplicacao)".
                Public Sub New(ByVal AssemblyInfo As System.Reflection.Assembly, _
                                                                      ByVal eAmbiente As Ambientes)
                    Dim atributos As New atributos
                    Dim sistema As String = atributos.ReadAssemblyProduct(AssemblyInfo)
                    Dim Titulo As String = atributos.ReadAssemblyTitle(AssemblyInfo)
                    Dim FileDescription As String = atributos.ReadAssemblyDescription(AssemblyInfo)
                    cCon.ConfiguraConexao(sistema, Titulo, eAmbiente, FileDescription)
                End Sub ' New
                Public Sub New()

                End Sub
                ' This method is used to attach array of SqlParameters to a SqlCommand.
                ' This method will assign a value of DbNull to any parameter with a direction of
                ' InputOutput and a value of null.  
                ' This behavior will prevent default values from being used, but
                ' this will be the less common case than an intended pure output parameter (derived as InputOutput)
                ' where the user provided no input value.
                ' Parameters:
                ' -command - The command to which the parameters will be added
                ' -commandParameters - an array of SqlParameters to be added to command
                Private Shared Sub AttachParameters(ByVal command As SqlCommand, ByVal commandParameters() As SqlParameter)
                    Try
                        If (command Is Nothing) Then Throw New ArgumentNullException("command")
                        If (Not commandParameters Is Nothing) Then
                            Dim p As SqlParameter
                            For Each p In commandParameters
                                If (Not p Is Nothing) Then
                                    ' Check for derived output value with no value assigned
                                    If (p.Direction = ParameterDirection.InputOutput OrElse p.Direction = ParameterDirection.Input) AndAlso p.Value Is Nothing Then
                                        p.Value = DBNull.Value
                                    End If
                                    command.Parameters.Add(p)
                                End If
                            Next p
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "1 - AttachParameters")
                    End Try
                End Sub ' AttachParameters
                Private Shared Sub AttachParameters(ByVal command As OleDb.OleDbCommand, ByVal commandParameters() As OleDb.OleDbParameter)
                    Try
                        If (command Is Nothing) Then Throw New ArgumentNullException("command")
                        If (Not commandParameters Is Nothing) Then
                            Dim p As OleDb.OleDbParameter
                            For Each p In commandParameters
                                If (Not p Is Nothing) Then
                                    ' Check for derived output value with no value assigned
                                    If (p.Direction = ParameterDirection.InputOutput OrElse p.Direction = ParameterDirection.Input) AndAlso p.Value Is Nothing Then
                                        p.Value = DBNull.Value
                                    End If
                                    command.Parameters.Add(p)
                                End If
                            Next p
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "2 - AttachParameters")
                    End Try
                End Sub ' AttachParameters


                ' This method assigns dataRow column values to an array of SqlParameters.
                ' Parameters:
                ' -commandParameters: Array of SqlParameters to be assigned values
                ' -dataRow: the dataRow used to hold the stored procedure' s parameter values
                Private Overloads Shared Sub AssignParameterValues(ByVal commandParameters() As SqlParameter, ByVal dataRow As DataRow)
                    Try
                        If commandParameters Is Nothing OrElse dataRow Is Nothing Then
                            ' Do nothing if we get no data    
                            Exit Sub
                        End If

                        ' Set the parameters values
                        Dim commandParameter As SqlParameter
                        Dim i As Integer
                        For Each commandParameter In commandParameters
                            ' Check the parameter name
                            If (commandParameter.ParameterName Is Nothing OrElse commandParameter.ParameterName.Length <= 1) Then
                                Throw New Exception(String.Format("Please provide a valid parameter name on the parameter #{0}, the ParameterName property has the following value: ' {1}' .", i, commandParameter.ParameterName))
                            End If
                            If dataRow.Table.Columns.IndexOf(commandParameter.ParameterName.Substring(1)) <> -1 Then
                                commandParameter.Value = dataRow(commandParameter.ParameterName.Substring(1))
                            End If
                            i = i + 1
                        Next
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "1 - AssignParameterValues")
                    End Try
                End Sub

                ' This method assigns an array of values to an array of SqlParameters.
                ' Parameters:
                ' -commandParameters - array of SqlParameters to be assigned values
                ' -array of objects holding the values to be assigned
                Private Overloads Shared Sub AssignParameterValues(ByVal commandParameters() As SqlParameter, ByVal parameterValues() As Object)
                    Try
                        Dim i As Integer
                        Dim j As Integer

                        If (commandParameters Is Nothing) AndAlso (parameterValues Is Nothing) Then
                            ' Do nothing if we get no data
                            Return
                        End If

                        ' We must have the same number of values as we pave parameters to put them in
                        If commandParameters.Length <> parameterValues.Length Then
                            Throw New ArgumentException("Parameter count does not match Parameter Value count.")
                        End If

                        ' Value array
                        j = commandParameters.Length - 1
                        For i = 0 To j
                            ' If the current array value derives from IDbDataParameter, then assign its Value property
                            If TypeOf parameterValues(i) Is IDbDataParameter Then
                                Dim paramInstance As IDbDataParameter = CType(parameterValues(i), IDbDataParameter)
                                If (paramInstance.Value Is Nothing) Then
                                    commandParameters(i).Value = DBNull.Value
                                Else
                                    commandParameters(i).Value = paramInstance.Value
                                End If
                            ElseIf (parameterValues(i) Is Nothing) Then
                                commandParameters(i).Value = DBNull.Value
                            Else
                                commandParameters(i).Value = parameterValues(i)
                            End If
                        Next
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "2 - AssignParameterValues")
                    End Try
                End Sub ' AssignParameterValues

                ' This method opens (if necessary) and assigns a connection, transaction, command type and parameters 
                ' to the provided command.
                ' Parameters:
                ' -command - the SqlCommand to be prepared
                ' -connection - a valid SqlConnection, on which to execute this command
                ' -transaction - a valid SqlTransaction, or ' null' 
                ' -commandType - the CommandType (stored procedure, text, etc.)
                ' -commandText - the stored procedure name or T-SQL command
                ' -commandParameters - an array of SqlParameters to be associated with the command or ' null' if no parameters are required
                Private Shared Sub PrepareCommand(ByVal command As SqlCommand, _
                                                  ByVal connection As SqlConnection, _
                                                  ByVal transaction As SqlTransaction, _
                                                  ByVal commandType As CommandType, _
                                                  ByVal commandText As String, _
                                                  ByVal commandParameters() As SqlParameter, ByRef mustCloseConnection As Boolean)

                    Try
                        If (command Is Nothing) Then Throw New ArgumentNullException("command")
                        If (commandText Is Nothing OrElse commandText.Length = 0) Then Throw New ArgumentNullException("commandText")

                        ' If the provided connection is not open, we will open it
                        If connection.State <> ConnectionState.Open Then
                            PrepararConexao(connection)
                            mustCloseConnection = True
                        Else
                            mustCloseConnection = False
                        End If

                        ' Associate the connection with the command
                        command.Connection = connection

                        ' Set the command text (stored procedure name or SQL statement)
                        command.CommandText = commandText

                        ' If we were provided a transaction, assign it.
                        If Not (transaction Is Nothing) Then
                            If transaction.Connection Is Nothing Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                            command.Transaction = transaction
                        End If

                        ' Set the command type
                        command.CommandType = commandType
                        command.CommandTimeout = ConnectionTimeout

                        ' Attach the command parameters if they are provided
                        If Not (commandParameters Is Nothing) Then
                            AttachParameters(command, commandParameters)
                        End If
                        Return
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "1 - PrepareCommand")
                    End Try
                End Sub ' PrepareCommand
                ' This method opens (if necessary) and assigns a connection, transaction, command type and parameters 
                ' to the provided command.
                ' Parameters:
                ' -command - the oledbcommand to be prepared
                ' -connection - a valid oledb.oledebconnection, on which to execute this command
                ' -transaction - a valid oledbtransaction, or ' null' 
                ' -commandType - the CommandType (stored procedure, text, etc.)
                ' -commandText - the stored procedure name or T-SQL command
                ' -commandParameters - an array of oledbparameters to be associated with the command or ' null' if no parameters are required
                Private Shared Sub PrepareCommand(ByVal command As OleDb.OleDbCommand, _
                                                  ByVal connection As OleDb.OleDbConnection, _
                                                  ByVal transaction As OleDb.OleDbTransaction, _
                                                  ByVal commandType As CommandType, _
                                                  ByVal commandText As String, _
                                                  ByVal commandParameters() As OleDb.OleDbParameter, ByRef mustCloseConnection As Boolean)
                    Try
                        If (command Is Nothing) Then Throw New ArgumentNullException("command")
                        If (commandText Is Nothing OrElse commandText.Length = 0) Then Throw New ArgumentNullException("commandText")

                        ' If the provided connection is not open, we will open it
                        If connection.State <> ConnectionState.Open Then
                            connection.Open()
                            mustCloseConnection = True
                        Else
                            mustCloseConnection = False
                        End If

                        ' Associate the connection with the command
                        command.Connection = connection

                        ' Set the command text (stored procedure name or SQL statement)
                        command.CommandText = commandText

                        ' If we were provided a transaction, assign it.
                        If Not (transaction Is Nothing) Then
                            If transaction.Connection Is Nothing Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                            command.Transaction = transaction
                        End If

                        ' Set the command type
                        command.CommandType = commandType
                        command.CommandTimeout = ConnectionTimeout

                        ' Attach the command parameters if they are provided
                        If Not (commandParameters Is Nothing) Then
                            AttachParameters(command, commandParameters)
                        End If
                        Return
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "2 - PrepareCommand")
                    End Try
                End Sub ' PrepareCommand

                ' Esse carrega os parametros de configura��o para as conex�es com o Banco de Dados
                ' Sistema - Sistema ao qual a aplica��o est� ligada
                ' Nome da Aplica��o
                ' Ambiente de execu��o
                ' Chave da Aplica��o - File
                ' Retorna 0 quando n�o ocorrer erros e 1 quando ocorrer.

                Private Overloads Shared Function ConfiguraConexao(ByVal eSistema As String, _
                                                                  ByVal eAplicacao As String, _
                                                                  ByVal eAmbiente As Ambientes, _
                                                                 ByVal eDescricaoArquivo As String) As Integer
                    Try
                        Dim oArquivo As System.IO.File
                        Dim oLeitura As System.IO.StreamReader
                        Dim Arquivo As String
                        Dim Provider As String
                        Dim SQL As String
                        Dim CommandText As String
                        Dim ds_param As DataSet

                        If _configurado = False Then
                            _ConnectionTimeout = TimeTimeout.Padr�o
                            configurando = True
                            _Ambiente = eAmbiente
                            _DescricaoErro = ""
                            Arquivo = "C:\segbr\msdisk.dat"
                            ConnectionString = ""
                            ConnectionString = oArquivo.OpenText(Arquivo).ReadLine.ToString

                            If ValidaAmbiente() Then
                                _AplicacaoValida = ValidaVersao(eAplicacao, eDescricaoArquivo)
                            Else
                                _AplicacaoValida = True
                            End If


                            If _AplicacaoValida Then
                                CommandText = "Set NoCount on Exec Controle_Sistema_db.dbo.obter_parametro_conexao2_sps " & _
                                "'" & eSistema & "'," & _
                                "'" & _Ambiente.ToString & "'"

                                ds_param = ConfigExecuteDataset(CommandType.Text, CommandText)
                                If ds_param.Tables(0).Rows.Count > 0 Then
                                    Dim servidor As String
                                    Dim banco As String
                                    Dim usuario As String
                                    Dim senha As String

                                    servidor = "SISAB015\EXTRATOR"
                                    servidor = "SISAB003"
                                    banco = "SEGUROS_DB"
                                    servidor = ds_param.Tables(0).Rows(0).Item("servidor").ToString
                                    banco = ds_param.Tables(0).Rows(0).Item("banco").ToString
                                    usuario = ds_param.Tables(0).Rows(0).Item("usuario").ToString
                                    senha = ds_param.Tables(0).Rows(0).Item("senha").ToString
                                    If ds_param.Tables(0).Rows(0).Item("cifrado").ToString = "S" Then
                                        senha = AbrePassword(senha)
                                    End If

                                    If servidor = "" Or banco = "" Or usuario = "" Or senha = "" Then
                                        ConnectionString = ""
                                        ConfiguraConexao = 1
                                        _DescricaoErro = "N�o Foi possivel obter os parametros de conex�o para o Sistema/Ambiente"
                                        _configurado = False
                                        Throw New ArgumentException("N�o foi Configurado")

                                    Else
                                        _bancodedados = banco
                                        _Sistema = eSistema
                                        _Aplicacao = eAplicacao
                                        _Ambiente = eAmbiente
                                        ConnectionString = "server=" & servidor & ";uid=" & usuario & ";pwd=" & senha & ";Pooling=false;Application Name=" & eAplicacao & ";database=" & banco & ";Connect Timeout=" & _ConnectionTimeout & ";"
                                        ConfiguraConexao = 0
                                        _configurado = True
                                    End If
                                Else
                                    ConfiguraConexao = 1
                                    _DescricaoErro = "N�o foi poss�vel encontrar uma senha v�lida para a aplica��o/Ambiente" & vbCr & "Entre em contato com o Administrador de Banco de Dados"
                                    _configurado = False
                                    Throw New ArgumentException("N�o foi Configurado")

                                End If

                            Else
                                ConfiguraConexao = 1
                                _configurado = False
                                configurando = False
                                _DescricaoErro = "N�o foi poss�vel validar a aplica��o para o Ambiente de " & _Ambiente.ToString & vbCr & "Entre em contato com o Administrador de Banco de Dados"
                            End If
                            configurando = False
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex, "1 - ConfiguraConexao")
                    End Try

                End Function

                '---------------------------------------------------------------
                ' fabreu - 2011-03-14
                ' Inicio - Conex�o Local Usu�rio da M�quina
                '---------------------------------------------------------------
                ' Esse carrega os parametros de configura��o para as conex�es com o Banco de Dados
                ' utilizando o usu�rio da maquina
                ' Sistema - Sistema ao qual a aplica��o est� ligada
                ' Nome da Aplica��o
                ' Ambiente de execu��o
                ' Chave da Aplica��o - File
                ' Retorna 0 quando n�o ocorrer erros e 1 quando ocorrer.

                Private Overloads Shared Function ConfiguraConexaoAutWindows(ByVal eSistema As String, _
                                                                  ByVal eAplicacao As String, _
                                                                  ByVal eAmbiente As Ambientes, _
                                                                 ByVal eDescricaoArquivo As String) As Integer
                    Try
                        Dim oArquivo As System.IO.File
                        Dim oLeitura As System.IO.StreamReader
                        Dim Arquivo As String
                        Dim Provider As String
                        Dim SQL As String
                        Dim CommandText As String
                        Dim ds_param As DataSet

                        If _configurado = False Then
                            _ConnectionTimeout = TimeTimeout.Padr�o
                            configurando = True
                            _Ambiente = eAmbiente
                            _DescricaoErro = ""
                            Arquivo = "C:\segbr\msdisk.dat"
                            ConnectionString = ""
                            ConnectionString = oArquivo.OpenText(Arquivo).ReadLine.ToString

                            If ValidaAmbiente() Then
                                _AplicacaoValida = ValidaVersao(eAplicacao, eDescricaoArquivo)
                            Else
                                _AplicacaoValida = True
                            End If


                            If _AplicacaoValida Then
                                CommandText = "Set NoCount on Exec Controle_Sistema_db.dbo.obter_parametro_conexao2_sps " & _
                                "'" & eSistema & "'," & _
                                "'" & _Ambiente.ToString & "'"

                                ds_param = ConfigExecuteDataset(CommandType.Text, CommandText)
                                If ds_param.Tables(0).Rows.Count > 0 Then
                                    Dim servidor As String
                                    Dim banco As String

                                    servidor = ds_param.Tables(0).Rows(0).Item("servidor").ToString
                                    banco = ds_param.Tables(0).Rows(0).Item("banco").ToString

                                    'If ds_param.Tables(0).Rows(0).Item("cifrado").ToString = "S" Then
                                    'senha = AbrePassword(senha)
                                    'End If

                                    If servidor = "" Or banco = "" Then

                                        ConnectionString = ""
                                        ConfiguraConexaoAutWindows = 1
                                        _DescricaoErro = "N�o Foi possivel obter os parametros de conex�o para o Sistema/Ambiente"
                                        _configurado = False
                                        Throw New ArgumentException("N�o foi Configurado")

                                    Else
                                        _bancodedados = banco
                                        _Sistema = eSistema
                                        _Aplicacao = eAplicacao
                                        _Ambiente = eAmbiente
                                        ConnectionString = "server=" & servidor & ";database=" & banco & ";Integrated Security=SSPI;"

                                        ConfiguraConexaoAutWindows = 0
                                        _configurado = True
                                    End If
                                Else
                                    ConfiguraConexaoAutWindows = 1
                                    _DescricaoErro = "N�o foi poss�vel encontrar uma senha v�lida para a aplica��o/Ambiente" & vbCr & "Entre em contato com o Administrador de Banco de Dados"
                                    _configurado = False
                                    Throw New ArgumentException("N�o foi Configurado")

                                End If

                            Else
                                ConfiguraConexaoAutWindows = 1
                                _configurado = False
                                configurando = False
                                _DescricaoErro = "N�o foi poss�vel validar a aplica��o para o Ambiente de " & _Ambiente.ToString & vbCr & "Entre em contato com o Administrador de Banco de Dados"
                            End If
                            configurando = False
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex, "1 - ConfiguraConexao")
                    End Try

                End Function
                '---------------------------------------------------------------
                ' fabreu - 2011-03-16
                ' Fim - Conex�o Local Usu�rio da M�quina
                '---------------------------------------------------------------




                Private Overloads Shared Function ValidaVersao(ByVal Sigla_Recurso As String, _
                                          ByVal eDescricaoArquivo As String) As Boolean

                    Try
                        Dim RetSenha As String
                        Dim ReSSenha As String
                        Dim eSQL As String
                        Dim RsRte As DataSet
                        Dim CommandText As String
                        Dim eChave As String


                        CommandText = "Set NoCount on Exec segab_db.dbo.Retorna_Versao_recurso_sps '" & Sigla_Recurso & "'"
                        RsRte = ConfigExecuteDataset(CommandType.Text, CommandText)


                        ReSSenha = RsRte.Tables(0).Rows(0).Item("Senha").ToString

                        RetSenha = GeraPassword(eDescricaoArquivo, ReSSenha)

                        If RetSenha = ReSSenha Then
                            ValidaVersao = True
                        Else
                            ValidaVersao = False
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "1 - ValidaVersao")
                    End Try
                End Function
                Private Shared Function GeraPassword(ByVal inpt As String, ByVal senha As String) As String
                    Try
                        Dim temp As String
                        Dim tempA As String
                        Dim Rand As String
                        Dim rad As String
                        Dim crntASC As String
                        Dim j As Integer
                        Dim i As Integer
                        Dim eChave As String


Top:
                        eChave = Mid(senha, Len(senha) - 3)
                        Rand = Left(senha, 3)
                        rad = Left(Rand, 1)
                        If Left(Rand, 1) = "-" Then
                            GoTo Top
                        End If

                        For j = 1 To Len(inpt)
                            crntASC = Asc(Mid(inpt, j, 1)).ToString
                            tempA = ((CInt((crntASC)) Xor (CInt(Rand) + j + CInt(rad))) + (j + CInt(rad))).ToString
                            If Len(tempA) = 4 Then
                                temp = temp & tempA
                            ElseIf Len(tempA) = 3 Then
                                temp = temp & "0" & tempA
                            ElseIf Len(tempA) = 2 Then
                                temp = temp & "00" & tempA
                            ElseIf Len(tempA) = 1 Then
                                temp = temp & "000" & tempA
                            End If
                        Next j
                        temp = Rand & temp
                        GeraPassword = temp & eChave
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "1 - GeraPassword")
                    End Try
                End Function

                Private Shared Function AbrePassword(ByVal inpt As String) As String
                    Try
                        Dim rand As String
                        Dim j As Integer
                        Dim z As Integer
                        Dim tempa As String
                        Dim temp As String

                        inpt = Mid(inpt, 1, Len(inpt) - 4)
                        rand = Left(inpt, 3)
                        For j = 4 To (Len(inpt) - 3) Step 4
                            z = z + 1
                            tempa = Mid(inpt, j, 4)
                            tempa = (((CInt(tempa) - (z + CInt(Left(rand, 1)))) Xor (CInt(rand) + z + CInt(Left(rand, 1))))).ToString
                            temp = temp & Chr(CInt(tempa))
                        Next j
                        AbrePassword = temp

                        Exit Function
errAbre:
                        AbrePassword = ""
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "1 - AbrePassword")
                    End Try
                End Function
                'Enum com a defini��o dos Ambientes dispon�veis para conex�o
                '
                Private Shared Function ValidaAmbiente() As Boolean
                    Try
                        If _Ambiente = Ambientes.Produ��o Or _Ambiente = Ambientes.PRODUCAO_ABS Then
                            ValidaAmbiente = True
                        Else
                            ValidaAmbiente = False
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "1 - ValidaAmbiente")
                    End Try
                End Function
                Private Shared Function PrepararConexao(ByVal Connection As SqlConnection) As SqlConnection
                    Try
                        If (Connection Is Nothing) Then
                            Connection = New SqlConnection

                        End If
                        If Connection.State = ConnectionState.Closed Then
                            Connection.ConnectionString = ConnectionString
                            Connection.Open()

                        End If

                        If Connection.State = ConnectionState.Open Then
                            PrepararConexao = Connection
                        Else
                            PrepararConexao = Nothing
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "1 - PrepararConexao")
                    End Try
                End Function

                Private Shared Function IniTrace(ByVal tipo As Integer) As Integer
                End Function
                Private Shared Sub FimTrace(ByVal tipo As Integer)
                    'Dim oArquivo As System.IO.File
                    'Dim oDir As System.IO.Directory
                    'Dim oLeitura As System.IO.StreamWriter
                    'If Dir("c:\segbr") = "" Then
                    '    oDir.CreateDirectory("C:\Segbr")
                    'End If

                    'If Not oArquivo.Exists("c:\segbr\" & Year(Now) & Month(Now) & Day(Now) & ".XML") Then
                    '    oArquivo.Create("c:\segbr\" & Year(Now) & Month(Now) & Day(Now) & ".XML")
                    'End If
                End Sub

#End Region
#Region "metodos extras"
                Public Shared Property BancodeDados() As String
                    Get
                        BancodeDados = _bancodedados
                    End Get
                    Set(ByVal Value As String)
                        ConnectionString = ConnectionString.Replace(_bancodedados, Value)
                        _bancodedados = Value
                    End Set
                End Property
                Public Shared Property ConnectionTimeout() As TimeTimeout
                    Get
                        ConnectionTimeout = _ConnectionTimeout
                    End Get
                    Set(ByVal Value As TimeTimeout)
                        Dim TimeoutAtual As String
                        TimeoutAtual = "Connect Timeout=" & _ConnectionTimeout & ";"
                        Dim TimeoutNovo As String
                        TimeoutNovo = "Connect Timeout=" & Value & ";"
                        ConnectionString = ConnectionString.Replace(TimeoutAtual, TimeoutNovo)
                        _ConnectionTimeout = Value
                    End Set
                End Property


                Public Overloads Shared Function ConfiguraConexao(ByVal AssemblyInfo As System.Reflection.Assembly, _
                                                                      ByVal eAmbiente As Ambientes) As Integer
                    Dim atributos As New atributos
                    Dim sistema As String = atributos.ReadAssemblyProduct(AssemblyInfo)
                    Dim Titulo As String = atributos.ReadAssemblyTitle(AssemblyInfo)
                    Dim FileDescription As String = atributos.ReadAssemblyDescription(AssemblyInfo)
                    _AssemblyInfo = AssemblyInfo
                    Return cCon.ConfiguraConexao(sistema, Titulo, eAmbiente, FileDescription)
                End Function

                ' ------------------------------------------------------
                ' fabreu - 14-03-2011
                ' Inicio - Conex�o Local Usu�rio da M�quina
                Public Overloads Shared Function ConfiguraConexaoAutWindows(ByVal AssemblyInfo As System.Reflection.Assembly, _
                                                                      ByVal eAmbiente As Ambientes) As Integer
                    Dim atributos As New atributos
                    Dim sistema As String = atributos.ReadAssemblyProduct(AssemblyInfo)
                    Dim Titulo As String = atributos.ReadAssemblyTitle(AssemblyInfo)
                    Dim FileDescription As String = atributos.ReadAssemblyDescription(AssemblyInfo)
                    _AssemblyInfo = AssemblyInfo
                    Return cCon.ConfiguraConexaoAutWindows(sistema, Titulo, eAmbiente, FileDescription)
                End Function
                ' fabreu - 14-03-2011
                ' Fim - Conex�o Local Usu�rio da M�quina
                ' ------------------------------------------------------

                Public Shared Function ResetAutWindows(Optional ByVal AssemblyInfo As System.Reflection.Assembly = Nothing, _
                                                                                           Optional ByVal eAmbiente As Ambientes = Nothing) As Integer
                    ConnectionString = Nothing
                    'Private Shared Connection As SqlConnection
                    If Not Transaction Is Nothing Then
                        If Transaction.Length > 0 Then

                            Dim count As Integer = Transaction.Length - 1
                            Do While count >= 0
                                If Not Transaction(count) Is Nothing Then
                                    Try
                                        Transaction(count).Rollback()
                                    Catch ex As Exception

                                    End Try

                                    Try
                                        Transaction(count).Connection.Close()
                                    Catch ex As Exception

                                    End Try

                                    Transaction(count) = Nothing
                                End If
                                count = count - 1
                            Loop
                        End If
                    End If
                    If Not TranConnection Is Nothing Then
                        If TranConnection.Length > 0 Then

                            Dim count As Integer = TranConnection.Length - 1
                            Do While count >= 0
                                If Not Transaction(count) Is Nothing Then
                                    TranConnection(count) = Nothing
                                End If
                                count = count - 1
                            Loop
                        End If
                    End If
                    _configurado = Nothing
                    configurando = Nothing
                    _bancodedados = Nothing
                    _AssemblyInfo = Nothing
                    Trace = Nothing
                    ConnectionOleDB = Nothing
                    'A variavel indica se as variaveis passadas para a configura��o da
                    'conex�o � valida
                    _AplicacaoValida = Nothing
                    _Sistema = Nothing
                    _Aplicacao = Nothing
                    _Ambiente = Nothing
                    DescricaoArquivo = Nothing
                    _DescricaoErro = Nothing
                    ResetAutWindows = 0
                    If Not AssemblyInfo Is Nothing Then
                        ResetAutWindows = ConfiguraConexaoAutWindows(AssemblyInfo, eAmbiente)
                    End If
                End Function
                Public Shared Function Reset(Optional ByVal AssemblyInfo As System.Reflection.Assembly = Nothing, _
                                                                            Optional ByVal eAmbiente As Ambientes = Nothing) As Integer
                    Try
                        ConnectionString = Nothing
                        'Private Shared Connection As SqlConnection
                        If Not Transaction Is Nothing Then
                            If Transaction.Length > 0 Then

                                Dim count As Integer = Transaction.Length - 1
                                Do While count >= 0
                                    If Not Transaction(count) Is Nothing Then
                                        Try
                                            Transaction(count).Rollback()
                                        Catch ex As Exception

                                        End Try

                                        Try
                                            Transaction(count).Connection.Close()
                                        Catch ex As Exception

                                        End Try
                                        Try
                                            Transaction(count) = Nothing
                                        Catch ex As Exception

                                        End Try

                                    End If
                                    count = count - 1
                                Loop
                            End If
                        End If
                        If Not TranConnection Is Nothing Then
                            If TranConnection.Length > 0 Then

                                Dim count As Integer = TranConnection.Length - 1
                                Do While count >= 0
                                    If Not Transaction(count) Is Nothing Then
                                        TranConnection(count) = Nothing
                                    End If
                                    count = count - 1
                                Loop
                            End If
                        End If
                        _configurado = Nothing
                        configurando = Nothing
                        _bancodedados = Nothing
                        _AssemblyInfo = Nothing
                        Trace = Nothing
                        ConnectionOleDB = Nothing
                        'A variavel indica se as variaveis passadas para a configura��o da
                        'conex�o � valida
                        _AplicacaoValida = Nothing
                        _Sistema = Nothing
                        _Aplicacao = Nothing
                        _Ambiente = Nothing
                        DescricaoArquivo = Nothing
                        _DescricaoErro = Nothing
                    Catch ex As Exception
                        Try
                            _configurado = Nothing
                            configurando = Nothing
                            _bancodedados = Nothing
                            _AssemblyInfo = Nothing
                            Trace = Nothing
                            ConnectionOleDB = Nothing
                            'A variavel indica se as variaveis passadas para a configura��o da
                            'conex�o � valida
                            _AplicacaoValida = Nothing
                            _Sistema = Nothing
                            _Aplicacao = Nothing
                            _Ambiente = Nothing
                            DescricaoArquivo = Nothing
                            _DescricaoErro = Nothing
                        Catch ex2 As Exception

                        End Try
                    End Try
                    Reset = 0
                    Try
                        If Not AssemblyInfo Is Nothing Then
                            Reset = ConfiguraConexao(AssemblyInfo, eAmbiente)
                        End If
                    Catch ex As Exception

                    End Try
                End Function
                Public Shared Function ReConfiguraConexao(Optional ByVal AssemblyInfo As System.Reflection.Assembly = Nothing, _
                    Optional ByVal eAmbiente As Ambientes = Nothing) As Integer
                    ReConfiguraConexao = Reset(AssemblyInfo, eAmbiente)
                End Function

                'fabreu - 2011-05-09 - Procedimento de Reconex�o usando usu�rio da m�quina
                Public Shared Function ReConfiguraConexaoAutWindows(Optional ByVal AssemblyInfo As System.Reflection.Assembly = Nothing, _
                    Optional ByVal eAmbiente As Ambientes = Nothing) As Integer
                    ReConfiguraConexaoAutWindows = ResetAutWindows(AssemblyInfo, eAmbiente)
                End Function

#End Region

#Region "ExecuteNonQuery"

                ' Execute a SqlCommand (that returns no resultset and takes no parameters) against the database specified in 
                ' the connection string. 
                ' e.g.:  
                '  Dim result As Integer =  ExecuteNonQuery(connString, CommandType.StoredProcedure, "PublishOrders")
                ' Parameters:
                ' -connectionString - a valid connection string for a SqlConnection
                ' -commandType - the CommandType (stored procedure, text, etc.)
                ' -commandText - the stored procedure name or T-SQL command
                ' Returns: An int representing the number of rows affected by the command
                Public Overloads Shared Function ExecuteNonQuery(ByVal commandType As CommandType, _
                                                               ByVal commandText As String) As Integer
                    Try
                        ' Pass through the call providing null for the set of SqlParameters
                        If configurado = True Then
                            Return ExecuteNonQuery(commandType, commandText, CType(Nothing, SqlParameter()))
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("1 - ExecuteNonQuery")

                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteNonQuery

                ' Execute a SqlCommand (that returns no resultset) against the database specified in the connection string 
                ' using the provided parameters.
                ' e.g.:  
                ' Dim result As Integer = ExecuteNonQuery(connString, CommandType.StoredProcedure, "PublishOrders", new SqlParameter("@prodid", 24))
                ' Parameters:
                ' -connectionString - a valid connection string for a SqlConnection
                ' -commandType - the CommandType (stored procedure, text, etc.)
                ' -commandText - the stored procedure name or T-SQL command
                ' -commandParameters - an array of SqlParamters used to execute the command
                ' Returns: An int representing the number of rows affected by the command
                Public Overloads Shared Function InternalExecuteNonQuery(ByVal commandType As CommandType, _
                                                                 ByVal commandText As String, _
                                                                 ByVal ParamArray commandParameters() As SqlParameter) As Integer
                    Try
                        If configurado = True Then
                            If (ConnectionString Is Nothing OrElse ConnectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
                            ' Create & open a SqlConnection, and dispose of it after we are done
                            Dim connection As SqlConnection
                            Try
                                connection = New SqlConnection(ConnectionString)
                                connection.Open()

                                ' Call the overload that takes a connection in place of the connection string
                                Return ExecuteNonQuery(commandType, commandText, commandParameters)
                            Finally
                                If Not connection Is Nothing Then connection.Dispose()
                            End Try
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("1 - InternalExecuteNonQuery")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteNonQuery

                ' Execute a stored procedure via a SqlCommand (that returns no resultset) against the database specified in 
                ' the connection string using the provided parameter values.  This method will discover the parameters for the 
                ' stored procedure, and assign the values based on parameter order.
                ' This method provides no access to output parameters or the stored procedure' s return value parameter.
                ' e.g.:  
                '  Dim result As Integer = ExecuteNonQuery(connString, "PublishOrders", 24, 36)
                ' Parameters:
                ' -connectionString - a valid connection string for a SqlConnection
                ' -spName - the name of the stored procedure
                ' -parameterValues - an array of objects to be assigned as the input values of the stored procedure
                ' Returns: An int representing the number of rows affected by the command
                Public Overloads Shared Function ExecuteNonQuery(ByVal spName As String, _
                                                                 ByVal ParamArray parameterValues() As Object) As Integer
                    Try
                        If configurado = True Then

                            If (ConnectionString Is Nothing OrElse ConnectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                            Dim commandParameters As SqlParameter()

                            ' If we receive parameter values, we need to figure out where they go
                            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)

                                commandParameters = SQLParameterCache.GetSpParameterSet(ConnectionString, spName)

                                ' Assign the provided values to these parameters based on parameter order
                                AssignParameterValues(commandParameters, parameterValues)

                                ' Call the overload that takes an array of SqlParameters
                                Return ExecuteNonQuery(CommandType.StoredProcedure, spName, commandParameters)
                                ' Otherwise we can just call the SP without params
                            Else
                                Return ExecuteNonQuery(CommandType.StoredProcedure, spName)
                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("2 - ExecuteNonQuery")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteNonQuery

                ' Execute a SqlCommand (that returns no resultset and takes no parameters) against the provided SqlConnection. 
                ' e.g.:  
                ' Dim result As Integer = ExecuteNonQuery(conn, CommandType.StoredProcedure, "PublishOrders")
                ' Parameters:
                ' -connection - a valid SqlConnection
                ' -commandType - the CommandType (stored procedure, text, etc.)
                ' -commandText - the stored procedure name or T-SQL command 
                ' Returns: An int representing the number of rows affected by the command
                Private Overloads Shared Function ExecuteNonQuery(ByVal connection As SqlConnection, _
                                                                 ByVal commandType As CommandType, _
                                                                 ByVal commandText As String) As Integer
                    Try
                        ' Pass through the call providing null for the set of SqlParameters
                        Return ExecuteNonQuery(commandType, commandText, CType(Nothing, SqlParameter()))
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "3 - ExecuteNonQuery")
                    End Try

                End Function ' ExecuteNonQuery

                ' Execute a SqlCommand (that returns no resultset) against the specified SqlConnection 
                ' using the provided parameters.
                ' e.g.:  
                '  Dim result As Integer = ExecuteNonQuery(conn, CommandType.StoredProcedure, "PublishOrders", new SqlParameter("@prodid", 24))
                ' Parameters:
                ' -connection - a valid SqlConnection 
                ' -commandType - the CommandType (stored procedure, text, etc.)
                ' -commandText - the stored procedure name or T-SQL command 
                ' -commandParameters - an array of SqlParamters used to execute the command 
                ' Returns: An int representing the number of rows affected by the command 
                Public Overloads Shared Function ExecuteNonQuery(ByVal commandType As CommandType, _
                                                                 ByVal commandText As String, _
                                                                 ByVal ParamArray commandParameters() As SqlParameter) As Integer
                    Try
                        If configurado = True Then
                            Dim connection As SqlConnection
                            connection = PrepararConexao(connection)
                            If Not connection Is Nothing Then

                                ' Create a command and prepare it for execution
                                Dim cmd As New SqlCommand
                                Dim retval As Integer
                                Dim mustCloseConnection As Boolean = False

                                PrepareCommand(cmd, connection, CType(Nothing, SqlTransaction), commandType, commandText, commandParameters, mustCloseConnection)

                                ' Finally, execute the command
                                retval = cmd.ExecuteNonQuery()

                                ' Detach the SqlParameters from the command object, so they can be used again
                                cmd.Parameters.Clear()

                                connection.Close()

                                Return retval
                            Else
                                _DescricaoErro = "Conex�o N�o Dispon�vel"
                                Throw New ArgumentException("4 - ExecuteNonQuery")
                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("4 - ExecuteNonQuery")

                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteNonQuery

                ' Execute a stored procedure via a SqlCommand (that returns no resultset) against the specified SqlConnection 
                ' using the provided parameter values.  This method will discover the parameters for the 
                ' stored procedure, and assign the values based on parameter order.
                ' This method provides no access to output parameters or the stored procedure' s return value parameter.
                ' e.g.:  
                '  Dim result As integer = ExecuteNonQuery(conn, "PublishOrders", 24, 36)
                ' Parameters:
                ' -connection - a valid SqlConnection
                ' -spName - the name of the stored procedure 
                ' -parameterValues - an array of objects to be assigned as the input values of the stored procedure 
                ' Returns: An int representing the number of rows affected by the command 
                Private Overloads Shared Function ExecuteNonQuery(ByVal connection As SqlConnection, _
                                                                 ByVal spName As String, _
                                                                 ByVal ParamArray parameterValues() As Object) As Integer
                    Try
                        If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
                        If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
                        Dim commandParameters As SqlParameter()

                        ' If we receive parameter values, we need to figure out where they go
                        If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                            ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                            commandParameters = SQLParameterCache.GetSpParameterSet(connection, spName)

                            ' Assign the provided values to these parameters based on parameter order
                            AssignParameterValues(commandParameters, parameterValues)

                            ' Call the overload that takes an array of SqlParameters
                            Return ExecuteNonQuery(CommandType.StoredProcedure, spName, commandParameters)
                        Else ' Otherwise we can just call the SP without params
                            Return ExecuteNonQuery(connection, CommandType.StoredProcedure, spName)
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "5 - ExecuteNonQuery")
                    End Try
                End Function ' ExecuteNonQuery

                ' Execute a SqlCommand (that returns no resultset and takes no parameters) against the provided SqlTransaction.
                ' e.g.:  
                '  Dim result As Integer = ExecuteNonQuery(trans, CommandType.StoredProcedure, "PublishOrders")
                ' Parameters:
                ' -transaction - a valid SqlTransaction associated with the connection 
                ' -commandType - the CommandType (stored procedure, text, etc.) 
                ' -commandText - the stored procedure name or T-SQL command 
                ' Returns: An int representing the number of rows affected by the command 
                Public Overloads Shared Function ExecuteNonQuery(ByVal num_transaction As Integer, _
                                                                 ByVal commandType As CommandType, _
                                                                 ByVal commandText As String) As Integer
                    Try
                        ' Pass through the call providing null for the set of SqlParameters
                        If configurado Then
                            Return ExecuteNonQuery(num_transaction, commandType, commandText, CType(Nothing, SqlParameter()))
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("6 - ExecuteNonQuery")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteNonQuery

                ' Execute a SqlCommand (that returns no resultset) against the specified SqlTransaction
                ' using the provided parameters.
                ' e.g.:  
                ' Dim result As Integer = ExecuteNonQuery(trans, CommandType.StoredProcedure, "GetOrders", new SqlParameter("@prodid", 24))
                ' Parameters:
                ' -transaction - a valid SqlTransaction 
                ' -commandType - the CommandType (stored procedure, text, etc.) 
                ' -commandText - the stored procedure name or T-SQL command 
                ' -commandParameters - an array of SqlParamters used to execute the command 
                ' Returns: An int representing the number of rows affected by the command 
                Public Overloads Shared Function ExecuteNonQuery(ByVal num_transaction As Integer, _
                                                                 ByVal commandType As CommandType, _
                                                                 ByVal commandText As String, _
                                                                 ByVal ParamArray commandParameters() As SqlParameter) As Integer

                    Try
                        If configurado Then
                            If (Transaction(num_transaction) Is Nothing) Then Throw New ArgumentNullException("transaction")
                            If Not (Transaction Is Nothing) AndAlso (Transaction(num_transaction).Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")

                            ' Create a command and prepare it for execution
                            Dim cmd As New SqlCommand
                            Dim retval As Integer
                            Dim mustCloseConnection As Boolean = False

                            PrepareCommand(cmd, Transaction(num_transaction).Connection, Transaction(num_transaction), commandType, commandText, commandParameters, mustCloseConnection)

                            ' Finally, execute the command
                            retval = cmd.ExecuteNonQuery()

                            ' Detach the SqlParameters from the command object, so they can be used again
                            cmd.Parameters.Clear()

                            Return retval
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("7 - ExecuteNonQuery")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteNonQuery

                ' Execute a stored procedure via a SqlCommand (that returns no resultset) against the specified SqlTransaction 
                ' using the provided parameter values.  This method will discover the parameters for the 
                ' stored procedure, and assign the values based on parameter order.
                ' This method provides no access to output parameters or the stored procedure' s return value parameter.
                ' e.g.:  
                ' Dim result As Integer = SQL.ExecuteNonQuery(trans, "PublishOrders", 24, 36)
                ' Parameters:
                ' -transaction - a valid SqlTransaction 
                ' -spName - the name of the stored procedure 
                ' -parameterValues - an array of objects to be assigned as the input values of the stored procedure 
                ' Returns: An int representing the number of rows affected by the command 
                Public Overloads Shared Function ExecuteNonQuery(ByVal num_transaction As Integer, _
                                                                 ByVal spName As String, _
                                                                 ByVal ParamArray parameterValues() As Object) As Integer
                    Try
                        If configurado Then
                            If (Transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                            If Not (Transaction Is Nothing) AndAlso (Transaction(num_transaction).Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                            Dim commandParameters As SqlParameter()

                            ' If we receive parameter values, we need to figure out where they go
                            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                                commandParameters = SQLParameterCache.GetSpParameterSet(Transaction(num_transaction).Connection, spName)

                                ' Assign the provided values to these parameters based on parameter order
                                AssignParameterValues(commandParameters, parameterValues)

                                ' Call the overload that takes an array of SqlParameters
                                Return ExecuteNonQuery(num_transaction, CommandType.StoredProcedure, spName, commandParameters)
                            Else ' Otherwise we can just call the SP without params
                                Return ExecuteNonQuery(num_transaction, CommandType.StoredProcedure, spName)
                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("8 - ExecuteNonQuery")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteNonQuery

#End Region

#Region "ExecuteDataset"

                ' Execute a SqlCommand (that returns a resultset and takes no parameters) against the database specified in 
                ' the connection string. 
                ' e.g.:  
                ' Dim ds As DataSet = SQL.ExecuteDataset("", commandType.StoredProcedure, "GetOrders")
                ' Parameters:
                ' -commandType - the CommandType (stored procedure, text, etc.)
                ' -commandText - the stored procedure name or T-SQL command
                ' Returns: A dataset containing the resultset generated by the command
                Public Overloads Shared Function ExecuteDataset(ByVal commandType As CommandType, _
                                                                ByVal commandText As String) As DataSet
                    Try
                        ' Pass through the call providing null for the set of SqlParameters
                        If configurado Then
                            Return ExecuteDataset(commandType, commandText, CType(Nothing, SqlParameter()))
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("1 - ExecuteDataset")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteDataset
                Private Overloads Shared Function ConfigExecuteDataset(ByVal commandType As CommandType, _
                                                            ByVal commandText As String) As DataSet
                    Try
                        ' Pass through the call providing null for the set of SqlParameters
                        If configurado Or Mid(ConnectionString, 1, 8) = "Provider" Then
                            Return ConfigExecuteDataset(commandType, commandText, CType(Nothing, SqlParameter()))
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("1 - ConfigExecuteDataset")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteDataset


                ' Execute a SqlCommand (that returns a resultset) against the database specified in the connection string 
                ' using the provided parameters.
                ' e.g.:  
                ' Dim ds As Dataset = ExecuteDataset(connString, CommandType.StoredProcedure, "GetOrders", new SqlParameter("@prodid", 24))
                ' Parameters:
                ' -connectionString - a valid connection string for a SqlConnection
                ' -commandType - the CommandType (stored procedure, text, etc.)
                ' -commandText - the stored procedure name or T-SQL command
                ' -commandParameters - an array of SqlParamters used to execute the command
                ' Returns: A dataset containing the resultset generated by the command
                Public Overloads Shared Function ExecuteDataset(ByVal commandType As CommandType, _
                                                                ByVal commandText As String, _
                                                                ByVal ParamArray commandParameters() As SqlParameter) As DataSet
                    Try
                        Dim Connection As SqlConnection
                        If configurado Then
                            Try


                                Connection = PrepararConexao(Connection)
                                ' Call the overload that takes a connection in place of the connection string
                                Return ExecuteDataset(Connection, commandType, commandText, commandParameters)
                            Catch ex As AcessoBDException
                                Throw ex
                            Catch ex As Exception
                                _DescricaoErro = ex.Message & " - " & ex.Source
                                Throw New AcessoBDException(_DescricaoErro, ex)
                            Finally
                                If Not ConnectionOleDB Is Nothing Then
                                    ConnectionOleDB.Dispose()
                                End If
                                If Not Connection Is Nothing Then
                                    Connection.Dispose()
                                End If
                            End Try
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("2 - ExecuteDataset")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteDataset
                Private Overloads Shared Function ConfigExecuteDataset(ByVal commandType As CommandType, _
                                                            ByVal commandText As String, _
                                                            ByVal ParamArray commandParameters() As SqlParameter) As DataSet
                    Try
                        Dim Connection As SqlConnection
                        Dim ConnectionOleDB As OleDb.OleDbConnection
                        If configurado Or Mid(ConnectionString, 1, 8) = "Provider" Then
                            Try


                                ConnectionOleDB = New OleDb.OleDbConnection(ConnectionString)
                                ConnectionOleDB.Open()
                                Dim parametros() As OleDb.OleDbParameter
                                Return ExecuteDataset(ConnectionOleDB, commandType, commandText, parametros)
                            Catch ex As Exception
                                Throw New AcessoBDException("As informa��es do arquivo MSDISK.DAT n�o est�o corretas", ex, "2 - ConfigExecuteDataset")
                            Finally
                                If Not ConnectionOleDB Is Nothing Then
                                    ConnectionOleDB.Dispose()
                                End If
                                If Not Connection Is Nothing Then
                                    Connection.Dispose()
                                End If
                            End Try
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("2 - ConfigExecuteDataset")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteDataset


                ' Execute a stored procedure via a SqlCommand (that returns a resultset) against the database specified in 
                ' the connection string using the provided parameter values.  This method will discover the parameters for the 
                ' stored procedure, and assign the values based on parameter order.
                ' This method provides no access to output parameters or the stored procedure' s return value parameter.
                ' e.g.:  
                ' Dim ds As Dataset= ExecuteDataset(connString, "GetOrders", 24, 36)
                ' Parameters:
                ' -connectionString - a valid connection string for a SqlConnection
                ' -spName - the name of the stored procedure
                ' -parameterValues - an array of objects to be assigned as the input values of the stored procedure
                ' Returns: A dataset containing the resultset generated by the command
                Public Overloads Shared Function ExecuteDataset(ByVal spName As String, _
                                                                ByVal ParamArray parameterValues() As Object) As DataSet

                    Try
                        If configurado Then
                            If (ConnectionString Is Nothing OrElse ConnectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
                            Dim commandParameters As SqlParameter()

                            ' If we receive parameter values, we need to figure out where they go
                            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                                commandParameters = SQLParameterCache.GetSpParameterSet(ConnectionString, spName)

                                ' Assign the provided values to these parameters based on parameter order
                                AssignParameterValues(commandParameters, parameterValues)

                                ' Call the overload that takes an array of SqlParameters
                                Return ExecuteDataset(CommandType.StoredProcedure, spName, commandParameters)
                            Else ' Otherwise we can just call the SP without params
                                Return ExecuteDataset(CommandType.StoredProcedure, spName)
                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("3 - ExecuteDataset")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteDataset

                ' Execute a SqlCommand (that returns a resultset and takes no parameters) against the provided SqlConnection. 
                ' e.g.:  
                ' Dim ds As Dataset = ExecuteDataset(conn, CommandType.StoredProcedure, "GetOrders")
                ' Parameters:
                ' -connection - a valid SqlConnection
                ' -commandType - the CommandType (stored procedure, text, etc.)
                ' -commandText - the stored procedure name or T-SQL command
                ' Returns: A dataset containing the resultset generated by the command
                Private Overloads Shared Function ExecuteDataset(ByVal connection As SqlConnection, _
                                                                ByVal commandType As CommandType, _
                                                                ByVal commandText As String) As DataSet
                    Try
                        ' Pass through the call providing null for the set of SqlParameters
                        Return ExecuteDataset(connection, commandType, commandText, CType(Nothing, SqlParameter()))
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "4 - ExecuteDataset")
                    End Try
                End Function ' ExecuteDataset

                ' Execute a SqlCommand (that returns a resultset) against the specified SqlConnection 
                ' using the provided parameters.
                ' e.g.:  
                ' Dim ds As Dataset = ExecuteDataset(conn, CommandType.StoredProcedure, "GetOrders", new SqlParameter("@prodid", 24))
                ' Parameters:
                ' -connection - a valid SqlConnection
                ' -commandType - the CommandType (stored procedure, text, etc.)
                ' -commandText - the stored procedure name or T-SQL command
                ' -commandParameters - an array of SqlParamters used to execute the command
                ' Returns: A dataset containing the resultset generated by the command
                Private Overloads Shared Function ExecuteDataset(ByVal connection As SqlConnection, _
                                                                ByVal commandType As CommandType, _
                                                                ByVal commandText As String, _
                                                                ByVal ParamArray commandParameters() As SqlParameter) As DataSet
                    Try
                        If (connection Is Nothing) Then Throw New ArgumentNullException("Faltando Conex�o")
                        ' Create a command and prepare it for execution
                        Dim cmd As New SqlCommand
                        Dim ds As New DataSet
                        Dim dataAdatpter As SqlDataAdapter
                        Dim mustCloseConnection As Boolean = False


                        PrepareCommand(cmd, connection, CType(Nothing, SqlTransaction), commandType, commandText, commandParameters, mustCloseConnection)

                        Try
                            ' Create the DataAdapter & DataSet
                            dataAdatpter = New SqlDataAdapter(cmd)

                            ' Fill the DataSet using default values for DataTable names, etc
                            dataAdatpter.Fill(ds)

                            ' Detach the SqlParameters from the command object, so they can be used again
                            cmd.Parameters.Clear()
                        Catch ex As AcessoBDException
                            Throw ex
                        Catch ex As Exception
                            Throw New AcessoBDException(ex, "5 - ExecuteDataset")
                        Finally
                            If (Not dataAdatpter Is Nothing) Then dataAdatpter.Dispose()
                        End Try
                        If (mustCloseConnection) Then connection.Close()

                        ' Return the dataset
                        Return ds
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "6 - ExecuteDataset")
                    End Try
                End Function ' ExecuteDataset
                ' Execute a SqlCommand (that returns a resultset) against the specified SqlConnection 
                ' using the provided parameters.
                ' e.g.:  
                ' Dim ds As Dataset = ExecuteDataset(conn, CommandType.StoredProcedure, "GetOrders", new SqlParameter("@prodid", 24))
                ' Parameters:
                ' -connection - a valid oledb.oledbconnection
                ' -commandType - the CommandType (stored procedure, text, etc.)
                ' -commandText - the stored procedure name or T-SQL command
                ' -commandParameters - an array of SqlParamters used to execute the command
                ' Returns: A dataset containing the resultset generated by the command
                Private Overloads Shared Function ExecuteDataset(ByVal connection As OleDb.OleDbConnection, _
                                                                ByVal commandType As CommandType, _
                                                                ByVal commandText As String, _
                                                                ByVal ParamArray commandParameters() As OleDb.OleDbParameter) As DataSet
                    Try
                        If (connection Is Nothing) Then Throw New ArgumentNullException("Faltando Conex�o")
                        ' Create a command and prepare it for execution
                        Dim cmd As New OleDb.OleDbCommand
                        Dim ds As New DataSet
                        Dim dataAdatpter As OleDb.OleDbDataAdapter
                        Dim mustCloseConnection As Boolean = False


                        PrepareCommand(cmd, connection, CType(Nothing, OleDb.OleDbTransaction), commandType, commandText, commandParameters, mustCloseConnection)

                        Try
                            ' Create the DataAdapter & DataSet
                            dataAdatpter = New OleDb.OleDbDataAdapter(cmd)
                            ' Fill the DataSet using default values for DataTable names, etc
                            dataAdatpter.Fill(ds)

                            ' Detach the SqlParameters from the command object, so they can be used again
                            cmd.Parameters.Clear()
                        Finally
                            If (Not dataAdatpter Is Nothing) Then dataAdatpter.Dispose()
                        End Try
                        If (mustCloseConnection) Then connection.Close()

                        ' Return the dataset
                        Return ds
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "7 - ExecuteDataset")
                    End Try
                End Function ' ExecuteDataset

                ' Execute a stored procedure via a SqlCommand (that returns a resultset) against the specified SqlConnection 
                ' using the provided parameter values.  This method will discover the parameters for the 
                ' stored procedure, and assign the values based on parameter order.
                ' This method provides no access to output parameters or the stored procedure' s return value parameter.
                ' e.g.:  
                ' Dim ds As Dataset = ExecuteDataset(conn, "GetOrders", 24, 36)
                ' Parameters:
                ' -connection - a valid SqlConnection
                ' -spName - the name of the stored procedure
                ' -parameterValues - an array of objects to be assigned as the input values of the stored procedure
                ' Returns: A dataset containing the resultset generated by the command
                Private Overloads Shared Function ExecuteDataset(ByVal connection As SqlConnection, _
                                                                ByVal spName As String, _
                                                                ByVal ParamArray parameterValues() As Object) As DataSet
                    Try
                        If (connection Is Nothing) Then Throw New ArgumentNullException("Faltando Conex�o")
                        If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                        Dim commandParameters As SqlParameter()

                        ' If we receive parameter values, we need to figure out where they go
                        If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then

                            ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                            commandParameters = SQLParameterCache.GetSpParameterSet(connection, spName)

                            ' Assign the provided values to these parameters based on parameter order
                            AssignParameterValues(commandParameters, parameterValues)

                            ' Call the overload that takes an array of SqlParameters
                            Return ExecuteDataset(connection, CommandType.StoredProcedure, spName, commandParameters)
                        Else ' Otherwise we can just call the SP without params
                            Return ExecuteDataset(connection, CommandType.StoredProcedure, spName)
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "8 - ExecuteDataset")
                    End Try
                End Function ' ExecuteDataset

                ' Execute a SqlCommand (that returns a resultset and takes no parameters) against the provided SqlTransaction. 
                ' e.g.:  
                ' Dim ds As Dataset = ExecuteDataset(trans, CommandType.StoredProcedure, "GetOrders")
                ' Parameters
                ' -transaction - a valid SqlTransaction
                ' -commandType - the CommandType (stored procedure, text, etc.)
                ' -commandText - the stored procedure name or T-SQL command
                ' Returns: A dataset containing the resultset generated by the command
                Public Overloads Shared Function ExecuteDataset(ByVal num_transaction As Integer, _
                                                                ByVal commandType As CommandType, _
                                                                ByVal commandText As String) As DataSet
                    Try
                        ' Pass through the call providing null for the set of SqlParameters
                        If configurado Then
                            Return ExecuteDataset(num_transaction, commandType, commandText, CType(Nothing, SqlParameter()))
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("9 - ExecuteDataset")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteDataset

                ' Execute a SqlCommand (that returns a resultset) against the specified SqlTransaction
                ' using the provided parameters.
                ' e.g.:  
                ' Dim ds As Dataset = ExecuteDataset(trans, CommandType.StoredProcedure, "GetOrders", new SqlParameter("@prodid", 24))
                ' Parameters
                ' -transaction - a valid SqlTransaction 
                ' -commandType - the CommandType (stored procedure, text, etc.)
                ' -commandText - the stored procedure name or T-SQL command
                ' -commandParameters - an array of SqlParamters used to execute the command
                ' Returns: A dataset containing the resultset generated by the command
                Public Overloads Shared Function ExecuteDataset(ByVal num_transaction As Integer, _
                                                                ByVal commandType As CommandType, _
                                                                ByVal commandText As String, _
                                                                ByVal ParamArray commandParameters() As SqlParameter) As DataSet
                    Try
                        If configurado Then
                            If (Transaction(num_transaction) Is Nothing) Then Throw New ArgumentNullException("transaction")
                            If Not (Transaction Is Nothing) AndAlso (Transaction(num_transaction).Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")

                            ' Create a command and prepare it for execution
                            Dim cmd As New SqlCommand
                            Dim ds As New DataSet
                            Dim dataAdatpter As SqlDataAdapter
                            Dim mustCloseConnection As Boolean = False

                            PrepareCommand(cmd, Transaction(num_transaction).Connection, Transaction(num_transaction), commandType, commandText, commandParameters, mustCloseConnection)

                            Try
                                ' Create the DataAdapter & DataSet
                                dataAdatpter = New SqlDataAdapter(cmd)

                                ' Fill the DataSet using default values for DataTable names, etc
                                dataAdatpter.Fill(ds)

                                ' Detach the SqlParameters from the command object, so they can be used again
                                cmd.Parameters.Clear()
                            Finally
                                If (Not dataAdatpter Is Nothing) Then dataAdatpter.Dispose()
                            End Try

                            ' Return the dataset
                            Return ds
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("10 - ExecuteDataset")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteDataset

                ' Execute a stored procedure via a SqlCommand (that returns a resultset) against the specified
                ' SqlTransaction using the provided parameter values.  This method will discover the parameters for the 
                ' stored procedure, and assign the values based on parameter order.
                ' This method provides no access to output parameters or the stored procedure' s return value parameter.
                ' e.g.:  
                ' Dim ds As Dataset = ExecuteDataset(trans, "GetOrders", 24, 36)
                ' Parameters:
                ' -transaction - a valid SqlTransaction 
                ' -spName - the name of the stored procedure
                ' -parameterValues - an array of objects to be assigned as the input values of the stored procedure
                ' Returns: A dataset containing the resultset generated by the command
                Public Overloads Shared Function ExecuteDataset(ByVal num_transaction As Integer, _
                                                                ByVal spName As String, _
                                                                ByVal ParamArray parameterValues() As Object) As DataSet
                    Try

                        If configurado Then
                            If (Transaction(num_transaction) Is Nothing) Then Throw New ArgumentNullException("transaction")
                            If Not (Transaction(num_transaction) Is Nothing) AndAlso (Transaction(num_transaction).Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                            Dim commandParameters As SqlParameter()

                            ' If we receive parameter values, we need to figure out where they go
                            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                                commandParameters = SQLParameterCache.GetSpParameterSet(Transaction(num_transaction).Connection, spName)

                                ' Assign the provided values to these parameters based on parameter order
                                AssignParameterValues(commandParameters, parameterValues)

                                ' Call the overload that takes an array of SqlParameters
                                Return ExecuteDataset(num_transaction, CommandType.StoredProcedure, spName, commandParameters)
                            Else ' Otherwise we can just call the SP without params
                                Return ExecuteDataset(num_transaction, CommandType.StoredProcedure, spName)
                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("11 - ExecuteDataset")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception

                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteDataset

#End Region

#Region "ExecuteReader"
                ' this enum is used to indicate whether the connection was provided by the caller, or created by SQL, so that
                ' we can set the appropriate CommandBehavior when calling ExecuteReader()
                Private Enum SqlConnectionOwnership

                    ' Connection is owned and managed by SQL
                    Internal
                    ' Connection is owned and managed by the caller
                    [External]
                End Enum ' SqlConnectionOwnership

                ' Create and prepare a SqlCommand, and call ExecuteReader with the appropriate CommandBehavior.
                ' If we created and opened the connection, we want the connection to be closed when the DataReader is closed.
                ' If the caller provided the connection, we want to leave it to them to manage.
                ' Parameters:
                ' -connection - a valid SqlConnection, on which to execute this command 
                ' -transaction - a valid SqlTransaction, or ' null' 
                ' -commandType - the CommandType (stored procedure, text, etc.) 
                ' -commandText - the stored procedure name or T-SQL command 
                ' -commandParameters - an array of SqlParameters to be associated with the command or ' null' if no parameters are required 
                ' -connectionOwnership - indicates whether the connection parameter was provided by the caller, or created by SQL 
                ' Returns: SqlDataReader containing the results of the command 
                Private Overloads Shared Function ExecuteReader(ByVal num_transaction As Integer, _
                                                                ByVal commandType As CommandType, _
                                                                ByVal commandText As String, _
                                                                ByVal commandParameters() As SqlParameter, _
                                                                ByVal connectionOwnership As SqlConnectionOwnership) As SqlDataReader

                    Try
                        Dim Connection As SqlConnection
                        Connection = PrepararConexao(Connection)
                        Dim mustCloseConnection As Boolean = False
                        ' Create a command and prepare it for execution
                        Dim cmd As New SqlCommand
                        Try
                            ' Create a reader
                            Dim dataReader As SqlDataReader
                            If num_transaction = -1 Then
                                PrepareCommand(cmd, Connection, CType(Nothing, SqlTransaction), commandType, commandText, commandParameters, mustCloseConnection)
                            Else
                                PrepareCommand(cmd, Connection, Transaction(num_transaction), commandType, commandText, commandParameters, mustCloseConnection)
                            End If
                            ' Call ExecuteReader with the appropriate CommandBehavior
                            If connectionOwnership = SqlConnectionOwnership.External Then
                                dataReader = cmd.ExecuteReader()
                            Else
                                dataReader = cmd.ExecuteReader(CommandBehavior.CloseConnection)
                            End If

                            ' Detach the SqlParameters from the command object, so they can be used again
                            Dim canClear As Boolean = True
                            Dim commandParameter As SqlParameter
                            For Each commandParameter In cmd.Parameters
                                If commandParameter.Direction <> ParameterDirection.Input Then
                                    canClear = False
                                End If
                            Next

                            If (canClear) Then cmd.Parameters.Clear()

                            Return dataReader
                        Catch
                            If (mustCloseConnection) Then Connection.Close()
                            Throw
                        End Try
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "1 - ExecuteReader")
                    End Try
                End Function ' ExecuteReader

                ' Execute a SqlCommand (that returns a resultset and takes no parameters) against the database specified in 
                ' the connection string. 
                ' e.g.:  
                ' Dim dr As SqlDataReader = ExecuteReader(connString, CommandType.StoredProcedure, "GetOrders")
                ' Parameters:
                ' -connectionString - a valid connection string for a SqlConnection 
                ' -commandType - the CommandType (stored procedure, text, etc.) 
                ' -commandText - the stored procedure name or T-SQL command 
                ' Returns: A SqlDataReader containing the resultset generated by the command 
                Private Overloads Shared Function ExecuteReader(ByVal commandType As CommandType, _
                                                               ByVal commandText As String) As SqlDataReader
                    Try
                        ' Pass through the call providing null for the set of SqlParameters
                        Return ExecuteReader(commandType, commandText, CType(Nothing, SqlParameter()))
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex)
                    End Try
                End Function ' ExecuteReader

                ' Execute a SqlCommand (that returns a resultset) against the database specified in the connection string 
                ' using the provided parameters.
                ' e.g.:  
                ' Dim dr As SqlDataReader = ExecuteReader(connString, CommandType.StoredProcedure, "GetOrders", new SqlParameter("@prodid", 24))
                ' Parameters:
                ' -connectionString - a valid connection string for a SqlConnection 
                ' -commandType - the CommandType (stored procedure, text, etc.) 
                ' -commandText - the stored procedure name or T-SQL command 
                ' -commandParameters - an array of SqlParamters used to execute the command 
                ' Returns: A SqlDataReader containing the resultset generated by the command 
                Public Overloads Shared Function ExecuteReader(ByVal commandType As CommandType, _
                                                               ByVal commandText As String, _
                                                               ByVal ParamArray commandParameters() As SqlParameter) As SqlDataReader
                    Try
                        Dim Connection As SqlConnection
                        If configurado Then
                            ' Create & open a SqlConnection
                            Connection = PrepararConexao(Connection)
                            Try
                                ' Call the private overload that takes an internally owned connection in place of the connection string
                                Return ExecuteReader(-1, commandType, commandText, commandParameters, SqlConnectionOwnership.Internal)
                            Catch
                                ' If we fail to return the SqlDatReader, we need to close the connection ourselves
                                If Not Connection Is Nothing Then Connection.Dispose()
                                Throw
                            End Try
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("2 - ExecuteReader")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteReader

                ' Execute a stored procedure via a SqlCommand (that returns a resultset) against the database specified in 
                ' the connection string using the provided parameter values.  This method will discover the parameters for the 
                ' stored procedure, and assign the values based on parameter order.
                ' This method provides no access to output parameters or the stored procedure' s return value parameter.
                ' e.g.:  
                ' Dim dr As SqlDataReader = ExecuteReader(connString, "GetOrders", 24, 36)
                ' Parameters:
                ' -connectionString - a valid connection string for a SqlConnection 
                ' -spName - the name of the stored procedure 
                ' -parameterValues - an array of objects to be assigned as the input values of the stored procedure 
                ' Returns: A SqlDataReader containing the resultset generated by the command 
                Public Overloads Shared Function ExecuteReader(ByVal spName As String, _
                                                               ByVal ParamArray parameterValues() As Object) As SqlDataReader
                    Try
                        Dim data As SqlDataReader
                        If configurado Then
                            If (ConnectionString Is Nothing OrElse ConnectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                            Dim commandParameters As SqlParameter()

                            ' If we receive parameter values, we need to figure out where they go
                            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                                commandParameters = SQLParameterCache.GetSpParameterSet(ConnectionString, spName)

                                ' Assign the provided values to these parameters based on parameter order
                                AssignParameterValues(commandParameters, parameterValues)

                                ' Call the overload that takes an array of SqlParameters
                                Return (ExecuteReader(CommandType.StoredProcedure, spName, commandParameters))
                                ' Otherwise we can just call the SP without params
                            Else
                                Return (ExecuteReader(ConnectionString, CommandType.StoredProcedure, spName))
                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("3 - ExecuteReader")

                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)

                    End Try
                End Function ' ExecuteReader

                ' Execute a SqlCommand (that returns a resultset and takes no parameters) against the provided SqlConnection. 
                ' e.g.:  
                ' Dim dr As SqlDataReader = ExecuteReader(conn, CommandType.StoredProcedure, "GetOrders")
                ' Parameters:
                ' -connection - a valid SqlConnection 
                ' -commandType - the CommandType (stored procedure, text, etc.) 
                ' -commandText - the stored procedure name or T-SQL command 
                ' Returns: A SqlDataReader containing the resultset generated by the command 
                Private Overloads Shared Function ExecuteReader(ByVal connection As SqlConnection, _
                                                               ByVal commandType As CommandType, _
                                                               ByVal commandText As String) As SqlDataReader
                    Try
                        Return ExecuteReader(connection, commandType, commandText, CType(Nothing, SqlParameter()))
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "4 - ExecuteReader")
                    End Try
                End Function ' ExecuteReader

                ' Execute a SqlCommand (that returns a resultset) against the specified SqlConnection 
                ' using the provided parameters.
                ' e.g.:  
                ' Dim dr As SqlDataReader = ExecuteReader(conn, CommandType.StoredProcedure, "GetOrders", new SqlParameter("@prodid", 24))
                ' Parameters:
                ' -connection - a valid SqlConnection 
                ' -commandType - the CommandType (stored procedure, text, etc.) 
                ' -commandText - the stored procedure name or T-SQL command 
                ' -commandParameters - an array of SqlParamters used to execute the command 
                ' Returns: A SqlDataReader containing the resultset generated by the command 
                Private Overloads Shared Function ExecuteReader(ByVal connection As SqlConnection, _
                                                               ByVal commandType As CommandType, _
                                                               ByVal commandText As String, _
                                                               ByVal ParamArray commandParameters() As SqlParameter) As SqlDataReader
                    Try
                        ' Pass through the call to private overload using a null transaction value
                        Return ExecuteReader(-1, commandType, commandText, commandParameters, SqlConnectionOwnership.External)
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "5 - ExecuteReader")
                    End Try
                End Function ' ExecuteReader

                ' Execute a stored procedure via a SqlCommand (that returns a resultset) against the specified SqlConnection 
                ' using the provided parameter values.  This method will discover the parameters for the 
                ' stored procedure, and assign the values based on parameter order.
                ' This method provides no access to output parameters or the stored procedure' s return value parameter.
                ' e.g.:  
                ' Dim dr As SqlDataReader = ExecuteReader(conn, "GetOrders", 24, 36)
                ' Parameters:
                ' -connection - a valid SqlConnection 
                ' -spName - the name of the stored procedure 
                ' -parameterValues - an array of objects to be assigned as the input values of the stored procedure 
                ' Returns: A SqlDataReader containing the resultset generated by the command 
                Private Overloads Shared Function ExecuteReader(ByVal connection As SqlConnection, _
                                                               ByVal spName As String, _
                                                               ByVal ParamArray parameterValues() As Object) As SqlDataReader
                    Try
                        If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
                        If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                        Dim commandParameters As SqlParameter()
                        ' If we receive parameter values, we need to figure out where they go
                        If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                            commandParameters = SQLParameterCache.GetSpParameterSet(connection, spName)

                            AssignParameterValues(commandParameters, parameterValues)

                            Return ExecuteReader(connection, CommandType.StoredProcedure, spName, commandParameters)
                        Else ' Otherwise we can just call the SP without params
                            Return ExecuteReader(connection, CommandType.StoredProcedure, spName)
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "6 - ExecuteReader")
                    End Try
                End Function ' ExecuteReader

                ' Execute a SqlCommand (that returns a resultset and takes no parameters) against the provided SqlTransaction.
                ' e.g.:  
                ' Dim dr As SqlDataReader = ExecuteReader(trans, CommandType.StoredProcedure, "GetOrders")
                ' Parameters:
                ' -transaction - a valid SqlTransaction  
                ' -commandType - the CommandType (stored procedure, text, etc.) 
                ' -commandText - the stored procedure name or T-SQL command 
                ' Returns: A SqlDataReader containing the resultset generated by the command 
                Public Overloads Shared Function ExecuteReader(ByVal num_transaction As Integer, _
                                                               ByVal commandType As CommandType, _
                                                               ByVal commandText As String) As SqlDataReader
                    Try
                        ' Pass through the call providing null for the set of SqlParameters
                        If configurado Then
                            Return ExecuteReader(num_transaction, commandType, commandText, CType(Nothing, SqlParameter()))
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("7 - ExecuteReader")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteReader

                ' Execute a SqlCommand (that returns a resultset) against the specified SqlTransaction
                ' using the provided parameters.
                ' e.g.:  
                ' Dim dr As SqlDataReader = ExecuteReader(trans, CommandType.StoredProcedure, "GetOrders", new SqlParameter("@prodid", 24))
                ' Parameters:
                ' -transaction - a valid SqlTransaction 
                ' -commandType - the CommandType (stored procedure, text, etc.)
                ' -commandText - the stored procedure name or T-SQL command 
                ' -commandParameters - an array of SqlParamters used to execute the command 
                ' Returns: A SqlDataReader containing the resultset generated by the command 
                Public Overloads Shared Function ExecuteReader(ByVal num_transaction As Integer, _
                                                               ByVal commandType As CommandType, _
                                                               ByVal commandText As String, _
                                                               ByVal ParamArray commandParameters() As SqlParameter) As SqlDataReader
                    Try
                        If configurado Then
                            If (Transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                            If Not (Transaction Is Nothing) AndAlso (Transaction(num_transaction).Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                            ' Pass through to private overload, indicating that the connection is owned by the caller
                            Return ExecuteReader(num_transaction, commandType, commandText, commandParameters, SqlConnectionOwnership.External)
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("8 - ExecuteReader")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteReader

                ' Execute a stored procedure via a SqlCommand (that returns a resultset) against the specified SqlTransaction 
                ' using the provided parameter values.  This method will discover the parameters for the 
                ' stored procedure, and assign the values based on parameter order.
                ' This method provides no access to output parameters or the stored procedure' s return value parameter.
                ' e.g.:  
                ' Dim dr As SqlDataReader = ExecuteReader(trans, "GetOrders", 24, 36)
                ' Parameters:
                ' -transaction - a valid SqlTransaction 
                ' -spName - the name of the stored procedure 
                ' -parameterValues - an array of objects to be assigned as the input values of the stored procedure
                ' Returns: A SqlDataReader containing the resultset generated by the command
                Public Overloads Shared Function ExecuteReader(ByVal num_transaction As Integer, _
                                                               ByVal spName As String, _
                                                               ByVal ParamArray parameterValues() As Object) As SqlDataReader
                    Try
                        If configurado Then
                            If (Transaction(num_transaction) Is Nothing) Then Throw New ArgumentNullException("transaction")
                            If Not (Transaction(num_transaction) Is Nothing) AndAlso (Transaction(num_transaction).Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                            Dim commandParameters As SqlParameter()

                            ' If we receive parameter values, we need to figure out where they go
                            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                                commandParameters = SQLParameterCache.GetSpParameterSet(Transaction(num_transaction).Connection, spName)

                                AssignParameterValues(commandParameters, parameterValues)

                                Return ExecuteReader(num_transaction, CommandType.StoredProcedure, spName, commandParameters)
                            Else ' Otherwise we can just call the SP without params
                                Return ExecuteReader(num_transaction, CommandType.StoredProcedure, spName)
                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("9 - ExecuteReader")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)

                    End Try
                End Function ' ExecuteReader

#End Region

#Region "ExecuteScalar"

                ' Execute a SqlCommand (that returns a 1x1 resultset and takes no parameters) against the database specified in 
                ' the connection string. 
                ' e.g.:  
                ' Dim orderCount As Integer = CInt(ExecuteScalar(connString, CommandType.StoredProcedure, "GetOrderCount"))
                ' Parameters:
                ' -connectionString - a valid connection string for a SqlConnection 
                ' -commandType - the CommandType (stored procedure, text, etc.) 
                ' -commandText - the stored procedure name or T-SQL command 
                ' Returns: An object containing the value in the 1x1 resultset generated by the command
                Public Overloads Shared Function ExecuteScalar(ByVal commandType As CommandType, _
                                                               ByVal commandText As String) As Object
                    Try
                        ' Pass through the call providing null for the set of SqlParameters
                        If configurado Then
                            Return ExecuteScalar(commandType, commandText, CType(Nothing, SqlParameter()))
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("1 - ExecuteScalar")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteScalar

                ' Execute a SqlCommand (that returns a 1x1 resultset) against the database specified in the connection string 
                ' using the provided parameters.
                ' e.g.:  
                ' Dim orderCount As Integer = Cint(ExecuteScalar(connString, CommandType.StoredProcedure, "GetOrderCount", new SqlParameter("@prodid", 24)))
                ' Parameters:
                ' -connectionString - a valid connection string for a SqlConnection 
                ' -commandType - the CommandType (stored procedure, text, etc.) 
                ' -commandText - the stored procedure name or T-SQL command 
                ' -commandParameters - an array of SqlParamters used to execute the command 
                ' Returns: An object containing the value in the 1x1 resultset generated by the command 
                'Public Overloads Shared Function ExecuteScalar(ByVal commandType As CommandType, _
                '                                               ByVal commandText As String, _
                '                                               ByVal ParamArray commandParameters() As SqlParameter) As Object
                '    If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
                '    ' Create & open a SqlConnection, and dispose of it after we are done.
                '    Dim connection As SqlConnection
                '    Try
                '        connection = New SqlConnection(connectionString)
                '        connection.Open()

                '        ' Call the overload that takes a connection in place of the connection string
                '        Return ExecuteScalar(commandType, commandText, commandParameters)
                '    Finally
                '        If Not connection Is Nothing Then connection.Dispose()
                '    End Try
                'End Function ' ExecuteScalar

                ' Execute a stored procedure via a SqlCommand (that returns a 1x1 resultset) against the database specified in 
                ' the connection string using the provided parameter values.  This method will discover the parameters for the 
                ' stored procedure, and assign the values based on parameter order.
                ' This method provides no access to output parameters or the stored procedure' s return value parameter.
                ' e.g.:  
                ' Dim orderCount As Integer = CInt(ExecuteScalar(connString, "GetOrderCount", 24, 36))
                ' Parameters:
                ' -connectionString - a valid connection string for a SqlConnection 
                ' -spName - the name of the stored procedure 
                ' -parameterValues - an array of objects to be assigned as the input values of the stored procedure 
                ' Returns: An object containing the value in the 1x1 resultset generated by the command 
                Private Overloads Shared Function ExecuteScalar(ByVal connectionString As String, _
                                                               ByVal spName As String, _
                                                               ByVal ParamArray parameterValues() As Object) As Object
                    Try
                        If configurado Then
                            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                            Dim commandParameters As SqlParameter()

                            ' If we receive parameter values, we need to figure out where they go
                            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                                commandParameters = SQLParameterCache.GetSpParameterSet(connectionString, spName)

                                ' Assign the provided values to these parameters based on parameter order
                                AssignParameterValues(commandParameters, parameterValues)

                                ' Call the overload that takes an array of SqlParameters
                                Return ExecuteScalar(connectionString, CommandType.StoredProcedure, spName, commandParameters)
                                ' Otherwise we can just call the SP without params
                            Else
                                Return ExecuteScalar(connectionString, CommandType.StoredProcedure, spName)
                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("2 - ExecuteScalar")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteScalar

                ' Execute a SqlCommand (that returns a 1x1 resultset and takes no parameters) against the provided SqlConnection. 
                ' e.g.:  
                ' Dim orderCount As Integer = CInt(ExecuteScalar(conn, CommandType.StoredProcedure, "GetOrderCount"))
                ' Parameters:
                ' -connection - a valid SqlConnection 
                ' -commandType - the CommandType (stored procedure, text, etc.) 
                ' -commandText - the stored procedure name or T-SQL command 
                ' Returns: An object containing the value in the 1x1 resultset generated by the command 
                Private Overloads Shared Function ExecuteScalar(ByVal connection As SqlConnection, _
                                                               ByVal commandType As CommandType, _
                                                               ByVal commandText As String) As Object
                    Try
                        If configurado Then
                            ' Pass through the call providing null for the set of SqlParameters
                            Return ExecuteScalar(commandType, commandText, CType(Nothing, SqlParameter()))
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("3 - ExecuteScalar")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteScalar

                ' Execute a SqlCommand (that returns a 1x1 resultset) against the specified SqlConnection 
                ' using the provided parameters.
                ' e.g.:  
                ' Dim orderCount As Integer = CInt(ExecuteScalar(conn, CommandType.StoredProcedure, "GetOrderCount", new SqlParameter("@prodid", 24)))
                ' Parameters:
                ' -connection - a valid SqlConnection 
                ' -commandType - the CommandType (stored procedure, text, etc.) 
                ' -commandText - the stored procedure name or T-SQL command 
                ' -commandParameters - an array of SqlParamters used to execute the command 
                ' Returns: An object containing the value in the 1x1 resultset generated by the command 
                Public Overloads Shared Function ExecuteScalar(ByVal commandType As CommandType, _
                                                               ByVal commandText As String, _
                                                               ByVal ParamArray commandParameters() As SqlParameter) As Object
                    Try
                        Dim Connection As SqlConnection
                        If configurado Then
                            Connection = PrepararConexao(Connection)

                            ' Create a command and prepare it for execution
                            Dim cmd As New SqlCommand
                            Dim retval As Object
                            Dim mustCloseConnection As Boolean = False

                            PrepareCommand(cmd, Connection, CType(Nothing, SqlTransaction), commandType, commandText, commandParameters, mustCloseConnection)

                            ' Execute the command & return the results
                            retval = cmd.ExecuteScalar()

                            ' Detach the SqlParameters from the command object, so they can be used again
                            cmd.Parameters.Clear()

                            If (mustCloseConnection) Then Connection.Close()

                            Return retval

                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("4 - ExecuteScalar")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteScalar

                ' Execute a stored procedure via a SqlCommand (that returns a 1x1 resultset) against the specified SqlConnection 
                ' using the provided parameter values.  This method will discover the parameters for the 
                ' stored procedure, and assign the values based on parameter order.
                ' This method provides no access to output parameters or the stored procedure' s return value parameter.
                ' e.g.:  
                ' Dim orderCount As Integer = CInt(ExecuteScalar(conn, "GetOrderCount", 24, 36))
                ' Parameters:
                ' -connection - a valid SqlConnection 
                ' -spName - the name of the stored procedure 
                ' -parameterValues - an array of objects to be assigned as the input values of the stored procedure 
                ' Returns: An object containing the value in the 1x1 resultset generated by the command 
                Public Overloads Shared Function ExecuteScalar(ByVal spName As String, _
                                                               ByVal ParamArray parameterValues() As Object) As Object
                    Try
                        Dim Connection As SqlConnection
                        Connection = PrepararConexao(Connection)
                        If configurado Then
                            If (Connection Is Nothing) Then Throw New ArgumentNullException("connection")
                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                            Dim commandParameters As SqlParameter()

                            ' If we receive parameter values, we need to figure out where they go
                            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                                commandParameters = SQLParameterCache.GetSpParameterSet(Connection, spName)

                                ' Assign the provided values to these parameters based on parameter order
                                AssignParameterValues(commandParameters, parameterValues)

                                ' Call the overload that takes an array of SqlParameters
                                Return ExecuteScalar(CommandType.StoredProcedure, spName, commandParameters)
                            Else ' Otherwise we can just call the SP without params
                                Return ExecuteScalar(Connection, CommandType.StoredProcedure, spName)
                            End If

                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("5 - ExecuteScalar")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteScalar

                ' Execute a SqlCommand (that returns a 1x1 resultset and takes no parameters) against the provided SqlTransaction.
                ' e.g.:  
                ' Dim orderCount As Integer  = CInt(ExecuteScalar(trans, CommandType.StoredProcedure, "GetOrderCount"))
                ' Parameters:
                ' -transaction - a valid SqlTransaction 
                ' -commandType - the CommandType (stored procedure, text, etc.) 
                ' -commandText - the stored procedure name or T-SQL command 
                ' Returns: An object containing the value in the 1x1 resultset generated by the command 
                Public Overloads Shared Function ExecuteScalar(ByVal transaction As SqlTransaction, _
                                                               ByVal commandType As CommandType, _
                                                               ByVal commandText As String) As Object
                    Try
                        ' Pass through the call providing null for the set of SqlParameters
                        If configurado Then
                            Return ExecuteScalar(transaction, commandType, commandText, CType(Nothing, SqlParameter()))
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("6 - ExecuteScalar")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteScalar

                ' Execute a SqlCommand (that returns a 1x1 resultset) against the specified SqlTransaction
                ' using the provided parameters.
                ' e.g.:  
                ' Dim orderCount As Integer = CInt(ExecuteScalar(trans, CommandType.StoredProcedure, "GetOrderCount", new SqlParameter("@prodid", 24)))
                ' Parameters:
                ' -transaction - a valid SqlTransaction  
                ' -commandType - the CommandType (stored procedure, text, etc.) 
                ' -commandText - the stored procedure name or T-SQL command 
                ' -commandParameters - an array of SqlParamters used to execute the command 
                ' Returns: An object containing the value in the 1x1 resultset generated by the command 
                Public Overloads Shared Function ExecuteScalar(ByVal transaction As SqlTransaction, _
                                                               ByVal commandType As CommandType, _
                                                               ByVal commandText As String, _
                                                               ByVal ParamArray commandParameters() As SqlParameter) As Object
                    Try
                        If configurado Then
                            If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                            If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")

                            ' Create a command and prepare it for execution
                            Dim cmd As New SqlCommand
                            Dim retval As Object
                            Dim mustCloseConnection As Boolean = False

                            PrepareCommand(cmd, transaction.Connection, transaction, commandType, commandText, commandParameters, mustCloseConnection)

                            ' Execute the command & return the results
                            retval = cmd.ExecuteScalar()

                            ' Detach the SqlParameters from the command object, so they can be used again
                            cmd.Parameters.Clear()

                            Return retval
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("7 - ExecuteScalar")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteScalar

                ' Execute a stored procedure via a SqlCommand (that returns a 1x1 resultset) against the specified SqlTransaction 
                ' using the provided parameter values.  This method will discover the parameters for the 
                ' stored procedure, and assign the values based on parameter order.
                ' This method provides no access to output parameters or the stored procedure' s return value parameter.
                ' e.g.:  
                ' Dim orderCount As Integer = CInt(ExecuteScalar(trans, "GetOrderCount", 24, 36))
                ' Parameters:
                ' -transaction - a valid SqlTransaction 
                ' -spName - the name of the stored procedure 
                ' -parameterValues - an array of objects to be assigned as the input values of the stored procedure 
                ' Returns: An object containing the value in the 1x1 resultset generated by the command 
                Public Overloads Shared Function ExecuteScalar(ByVal transaction As SqlTransaction, _
                                                               ByVal spName As String, _
                                                               ByVal ParamArray parameterValues() As Object) As Object
                    Try
                        If configurado Then
                            If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                            If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                            Dim commandParameters As SqlParameter()
                            ' If we receive parameter values, we need to figure out where they go
                            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                                commandParameters = SQLParameterCache.GetSpParameterSet(transaction.Connection, spName)

                                ' Assign the provided values to these parameters based on parameter order
                                AssignParameterValues(commandParameters, parameterValues)

                                ' Call the overload that takes an array of SqlParameters
                                Return ExecuteScalar(transaction, CommandType.StoredProcedure, spName, commandParameters)
                            Else ' Otherwise we can just call the SP without params
                                Return ExecuteScalar(transaction, CommandType.StoredProcedure, spName)
                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("8 - ExecuteScalar")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteScalar

#End Region

#Region "ExecuteXmlReader"

                ' Execute a SqlCommand (that returns a resultset and takes no parameters) against the provided SqlConnection. 
                ' e.g.:  
                ' Dim r As XmlReader = ExecuteXmlReader(conn, CommandType.StoredProcedure, "GetOrders")
                ' Parameters:
                ' -connection - a valid SqlConnection 
                ' -commandType - the CommandType (stored procedure, text, etc.) 
                ' -commandText - the stored procedure name or T-SQL command using "FOR XML AUTO" 
                ' Returns: An XmlReader containing the resultset generated by the command 
                Public Overloads Shared Function ExecuteXmlReader(ByVal commandType As CommandType, _
                                                                  ByVal commandText As String) As XmlReader
                    Try
                        If configurado Then
                            ' Pass through the call providing null for the set of SqlParameters
                            Return ExecuteXmlReader(commandType, commandText, CType(Nothing, SqlParameter()))
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("1 - ExecuteXmlReader")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteXmlReader

                ' Execute a SqlCommand (that returns a resultset) against the specified SqlConnection 
                ' using the provided parameters.
                ' e.g.:  
                ' Dim r As XmlReader = ExecuteXmlReader(conn, CommandType.StoredProcedure, "GetOrders", new SqlParameter("@prodid", 24))
                ' Parameters:
                ' -connection - a valid SqlConnection 
                ' -commandType - the CommandType (stored procedure, text, etc.) 
                ' -commandText - the stored procedure name or T-SQL command using "FOR XML AUTO" 
                ' -commandParameters - an array of SqlParamters used to execute the command 
                ' Returns: An XmlReader containing the resultset generated by the command 
                Public Overloads Shared Function ExecuteXmlReader(ByVal commandType As CommandType, _
                                                                  ByVal commandText As String, _
                                                                  ByVal ParamArray commandParameters() As SqlParameter) As XmlReader
                    Try
                        Dim Connection As SqlConnection
                        If configurado Then
                            ' Pass through the call using a null transaction value

                            ' Create a command and prepare it for execution
                            Connection = PrepararConexao(Connection)
                            Dim cmd As New SqlCommand
                            Dim mustCloseConnection As Boolean = False
                            Try
                                Dim retval As XmlReader

                                PrepareCommand(cmd, Connection, CType(Nothing, SqlTransaction), commandType, commandText, commandParameters, mustCloseConnection)

                                ' Create the DataAdapter & DataSet
                                retval = cmd.ExecuteXmlReader()

                                ' Detach the SqlParameters from the command object, so they can be used again
                                cmd.Parameters.Clear()

                                Return retval
                            Catch
                                If (mustCloseConnection) Then Connection.Close()
                                Throw
                            End Try

                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("2 - ExecuteXmlReader")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteXmlReader

                ' Execute a stored procedure via a SqlCommand (that returns a resultset) against the specified SqlConnection 
                ' using the provided parameter values.  This method will discover the parameters for the 
                ' stored procedure, and assign the values based on parameter order.
                ' This method provides no access to output parameters or the stored procedure' s return value parameter.
                ' e.g.:  
                ' Dim r As XmlReader = ExecuteXmlReader(conn, "GetOrders", 24, 36)
                ' Parameters:
                ' -connection - a valid SqlConnection 
                ' -spName - the name of the stored procedure using "FOR XML AUTO" 
                ' -parameterValues - an array of objects to be assigned as the input values of the stored procedure 
                ' Returns: An XmlReader containing the resultset generated by the command 
                Public Overloads Shared Function ExecuteXmlReader(ByVal spName As String, _
                                                                  ByVal ParamArray parameterValues() As Object) As XmlReader

                    Try
                        If configurado Then
                            Dim Connection As SqlConnection
                            Connection = PrepararConexao(Connection)

                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                            Dim commandParameters As SqlParameter()

                            ' If we receive parameter values, we need to figure out where they go
                            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                                commandParameters = SQLParameterCache.GetSpParameterSet(Connection, spName)

                                ' Assign the provided values to these parameters based on parameter order
                                AssignParameterValues(commandParameters, parameterValues)

                                ' Call the overload that takes an array of SqlParameters
                                Return ExecuteXmlReader(CommandType.StoredProcedure, spName, commandParameters)
                                ' Otherwise we can just call the SP without params
                            Else
                                Return ExecuteXmlReader(CommandType.StoredProcedure, spName)
                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("3 - ExecuteXmlReader")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteXmlReader


                ' Execute a SqlCommand (that returns a resultset and takes no parameters) against the provided SqlTransaction
                ' e.g.:  
                ' Dim r As XmlReader = ExecuteXmlReader(trans, CommandType.StoredProcedure, "GetOrders")
                ' Parameters:
                ' -transaction - a valid SqlTransaction
                ' -commandType - the CommandType (stored procedure, text, etc.) 
                ' -commandText - the stored procedure name or T-SQL command using "FOR XML AUTO" 
                ' Returns: An XmlReader containing the resultset generated by the command 
                Public Overloads Shared Function ExecuteXmlReader(ByVal transaction As SqlTransaction, _
                                                                  ByVal commandType As CommandType, _
                                                                  ByVal commandText As String) As XmlReader
                    Try
                        If configurado Then
                            ' Pass through the call providing null for the set of SqlParameters
                            Return ExecuteXmlReader(transaction, commandType, commandText, CType(Nothing, SqlParameter()))
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("4 - ExecuteXmlReader")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteXmlReader

                ' Execute a SqlCommand (that returns a resultset) against the specified SqlTransaction
                ' using the provided parameters.
                ' e.g.:  
                ' Dim r As XmlReader = ExecuteXmlReader(trans, CommandType.StoredProcedure, "GetOrders", new SqlParameter("@prodid", 24))
                ' Parameters:
                ' -transaction - a valid SqlTransaction
                ' -commandType - the CommandType (stored procedure, text, etc.) 
                ' -commandText - the stored procedure name or T-SQL command using "FOR XML AUTO" 
                ' -commandParameters - an array of SqlParamters used to execute the command 
                ' Returns: An XmlReader containing the resultset generated by the command
                Public Overloads Shared Function ExecuteXmlReader(ByVal transaction As SqlTransaction, _
                                                                  ByVal commandType As CommandType, _
                                                                  ByVal commandText As String, _
                                                                  ByVal ParamArray commandParameters() As SqlParameter) As XmlReader
                    Try
                        If configurado Then
                            ' Create a command and prepare it for execution
                            If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                            If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")

                            Dim cmd As New SqlCommand

                            Dim retval As XmlReader
                            Dim mustCloseConnection As Boolean = False

                            PrepareCommand(cmd, transaction.Connection, transaction, commandType, commandText, commandParameters, mustCloseConnection)

                            ' Create the DataAdapter & DataSet
                            retval = cmd.ExecuteXmlReader()

                            ' Detach the SqlParameters from the command object, so they can be used again
                            cmd.Parameters.Clear()

                            Return retval

                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("5 - ExecuteXmlReader")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteXmlReader

                ' Execute a stored procedure via a SqlCommand (that returns a resultset) against the specified SqlTransaction 
                ' using the provided parameter values.  This method will discover the parameters for the 
                ' stored procedure, and assign the values based on parameter order.
                ' This method provides no access to output parameters or the stored procedure' s return value parameter.
                ' e.g.:  
                ' Dim r As XmlReader = ExecuteXmlReader(trans, "GetOrders", 24, 36)
                ' Parameters:
                ' -transaction - a valid SqlTransaction
                ' -spName - the name of the stored procedure 
                ' -parameterValues - an array of objects to be assigned as the input values of the stored procedure 
                ' Returns: A dataset containing the resultset generated by the command
                Public Overloads Shared Function ExecuteXmlReader(ByVal transaction As SqlTransaction, _
                                                                  ByVal spName As String, _
                                                                  ByVal ParamArray parameterValues() As Object) As XmlReader
                    Try
                        If configurado Then
                            If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                            If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                            Dim commandParameters As SqlParameter()

                            ' If we receive parameter values, we need to figure out where they go
                            If Not (parameterValues Is Nothing) AndAlso parameterValues.Length > 0 Then
                                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                                commandParameters = SQLParameterCache.GetSpParameterSet(transaction.Connection, spName)

                                ' Assign the provided values to these parameters based on parameter order
                                AssignParameterValues(commandParameters, parameterValues)

                                ' Call the overload that takes an array of SqlParameters
                                Return ExecuteXmlReader(transaction, CommandType.StoredProcedure, spName, commandParameters)
                                ' Otherwise we can just call the SP without params
                            Else
                                Return ExecuteXmlReader(transaction, CommandType.StoredProcedure, spName)
                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("6 - ExecuteXmlReader")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function ' ExecuteXmlReader

#End Region

#Region "FillDataset"
                ' Execute a SqlCommand (that returns a resultset and takes no parameters) against the database specified in 
                ' the connection string. 
                ' e.g.:  
                '   FillDataset (connString, CommandType.StoredProcedure, "GetOrders", ds, new String() {"orders"})
                ' Parameters:    
                ' -connectionString: A valid connection string for a SqlConnection
                ' -commandType: the CommandType (stored procedure, text, etc.)
                ' -commandText: the stored procedure name or T-SQL command
                ' -dataSet: A dataset wich will contain the resultset generated by the command
                ' -tableNames: this array will be used to create table mappings allowing the DataTables to be referenced
                '               by a user defined name (probably the actual table name)
                Private Overloads Shared Sub FillDataset(ByVal connectionString As String, ByVal commandType As CommandType, ByVal commandText As String, ByVal dataSet As DataSet, ByVal tableNames() As String)
                    Try
                        If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
                        If (dataSet Is Nothing) Then Throw New ArgumentNullException("dataSet")

                        ' Create & open a SqlConnection, and dispose of it after we are done
                        Dim connection As SqlConnection
                        Try
                            connection = New SqlConnection(connectionString)

                            connection.Open()

                            ' Call the overload that takes a connection in place of the connection string
                            FillDataset(commandType, commandText, dataSet, tableNames)
                        Finally
                            If Not connection Is Nothing Then connection.Dispose()
                        End Try
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "1 - FillDataset")
                    End Try
                End Sub

                ' Execute a SqlCommand (that returns a resultset) against the database specified in the connection string 
                ' using the provided parameters.
                ' e.g.:  
                '   FillDataset (connString, CommandType.StoredProcedure, "GetOrders", ds, new String() = {"orders"}, new SqlParameter("@prodid", 24))
                ' Parameters:    
                ' -connectionString: A valid connection string for a SqlConnection
                ' -commandType: the CommandType (stored procedure, text, etc.)
                ' -commandText: the stored procedure name or T-SQL command
                ' -dataSet: A dataset wich will contain the resultset generated by the command
                ' -tableNames: this array will be used to create table mappings allowing the DataTables to be referenced
                '               by a user defined name (probably the actual table name)
                ' -commandParameters: An array of SqlParamters used to execute the command
                Private Overloads Shared Sub FillDataset(ByVal connectionString As String, ByVal commandType As CommandType, ByVal commandText As String, ByVal dataSet As DataSet, _
                    ByVal tableNames() As String, ByVal ParamArray commandParameters() As SqlParameter)
                    Try
                        If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
                        If (dataSet Is Nothing) Then Throw New ArgumentNullException("dataSet")

                        ' Create & open a SqlConnection, and dispose of it after we are done
                        Dim connection As SqlConnection
                        Try
                            connection = New SqlConnection(connectionString)

                            connection.Open()

                            ' Call the overload that takes a connection in place of the connection string
                            FillDataset(commandType, commandText, dataSet, tableNames, commandParameters)
                        Finally
                            If Not connection Is Nothing Then connection.Dispose()
                        End Try
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "2 - FillDataset")
                    End Try
                End Sub

                ' Execute a stored procedure via a SqlCommand (that returns a resultset) against the database specified in 
                ' the connection string using the provided parameter values.  This method will query the database to discover the parameters for the 
                ' stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
                ' This method provides no access to output parameters or the stored procedure' s return value parameter.
                ' e.g.:  
                '   FillDataset (connString, CommandType.StoredProcedure, "GetOrders", ds, new String() {"orders"}, 24)
                ' Parameters:
                ' -connectionString: A valid connection string for a SqlConnection
                ' -spName: the name of the stored procedure
                ' -dataSet: A dataset wich will contain the resultset generated by the command
                ' -tableNames: this array will be used to create table mappings allowing the DataTables to be referenced
                '             by a user defined name (probably the actual table name)
                ' -parameterValues: An array of objects to be assigned As the input values of the stored procedure
                Private Overloads Shared Sub FillDataset(ByVal connectionString As String, ByVal spName As String, _
                    ByVal dataSet As DataSet, ByVal tableNames As String(), ByVal ParamArray parameterValues() As Object)
                    Try
                        If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
                        If (dataSet Is Nothing) Then Throw New ArgumentNullException("dataSet")

                        ' Create & open a SqlConnection, and dispose of it after we are done
                        Dim connection As SqlConnection
                        Try
                            connection = New SqlConnection(connectionString)

                            connection.Open()

                            ' Call the overload that takes a connection in place of the connection string
                            FillDataset(spName, dataSet, tableNames, parameterValues)
                        Finally
                            If Not connection Is Nothing Then connection.Dispose()
                        End Try
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "3 - FillDataset")
                    End Try
                End Sub

                ' Execute a SqlCommand (that returns a resultset and takes no parameters) against the provided SqlConnection. 
                ' e.g.:  
                '   FillDataset (conn, CommandType.StoredProcedure, "GetOrders", ds, new String() {"orders"})
                ' Parameters:
                ' -connection: A valid SqlConnection
                ' -commandType: the CommandType (stored procedure, text, etc.)
                ' -commandText: the stored procedure name or T-SQL command
                ' -dataSet: A dataset wich will contain the resultset generated by the command
                ' -tableNames: this array will be used to create table mappings allowing the DataTables to be referenced
                ' by a user defined name (probably the actual table name)
                Public Overloads Shared Sub FillDataset(ByVal commandType As CommandType, _
                    ByVal commandText As String, ByVal dataSet As DataSet, ByVal tableNames As String())
                    Try
                        If configurado Then
                            FillDataset(commandType, commandText, dataSet, tableNames, Nothing)
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("4 - FillDataset")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Sub

                ' Execute a SqlCommand (that returns a resultset) against the specified SqlConnection 
                ' using the provided parameters.
                ' e.g.:  
                '   FillDataset (conn, CommandType.StoredProcedure, "GetOrders", ds, new String() {"orders"}, new SqlParameter("@prodid", 24))
                ' Parameters:
                ' -connection: A valid SqlConnection
                ' -commandType: the CommandType (stored procedure, text, etc.)
                ' -commandText: the stored procedure name or T-SQL command
                ' -dataSet: A dataset wich will contain the resultset generated by the command
                ' -tableNames: this array will be used to create table mappings allowing the DataTables to be referenced
                ' by a user defined name (probably the actual table name)
                ' -commandParameters: An array of SqlParamters used to execute the command
                Public Overloads Shared Sub FillDataset(ByVal commandType As CommandType, _
                ByVal commandText As String, ByVal dataSet As DataSet, ByVal tableNames As String(), _
                    ByVal ParamArray commandParameters() As SqlParameter)
                    Try
                        If configurado Then
                            FillDataset(-1, commandType, commandText, dataSet, tableNames, commandParameters)
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("5 - FillDataset")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Sub

                ' Execute a stored procedure via a SqlCommand (that returns a resultset) against the specified SqlConnection 
                ' using the provided parameter values.  This method will query the database to discover the parameters for the 
                ' stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
                ' This method provides no access to output parameters or the stored procedure' s return value parameter.
                ' e.g.:  
                ' FillDataset (conn, "GetOrders", ds, new string() {"orders"}, 24, 36)
                ' Parameters:
                ' -connection: A valid SqlConnection
                ' -spName: the name of the stored procedure
                ' -dataSet: A dataset wich will contain the resultset generated by the command
                ' -tableNames: this array will be used to create table mappings allowing the DataTables to be referenced
                '             by a user defined name (probably the actual table name)
                ' -parameterValues: An array of objects to be assigned as the input values of the stored procedure
                Public Overloads Shared Sub FillDataset(ByVal spName As String, ByVal dataSet As DataSet, _
                    ByVal tableNames() As String, ByVal ParamArray parameterValues() As Object)
                    Try
                        If configurado Then
                            Dim Connection As SqlConnection
                            Connection = PrepararConexao(Connection)

                            If (dataSet Is Nothing) Then Throw New ArgumentNullException("dataSet")
                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                            ' If we receive parameter values, we need to figure out where they go
                            If Not parameterValues Is Nothing AndAlso parameterValues.Length > 0 Then

                                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                                Dim commandParameters() As SqlParameter = SQLParameterCache.GetSpParameterSet(Connection, spName)

                                ' Assign the provided values to these parameters based on parameter order
                                AssignParameterValues(commandParameters, parameterValues)

                                ' Call the overload that takes an array of SqlParameters
                                FillDataset(CommandType.StoredProcedure, spName, dataSet, tableNames, commandParameters)
                            Else ' Otherwise we can just call the SP without params
                                FillDataset(CommandType.StoredProcedure, spName, dataSet, tableNames)
                            End If

                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("6 - FillDataset")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Sub

                ' Execute a SqlCommand (that returns a resultset and takes no parameters) against the provided SqlTransaction. 
                ' e.g.:  
                '   FillDataset (trans, CommandType.StoredProcedure, "GetOrders", ds, new string() {"orders"})
                ' Parameters:
                ' -transaction: A valid SqlTransaction
                ' -commandType: the CommandType (stored procedure, text, etc.)
                ' -commandText: the stored procedure name or T-SQL command
                ' -dataSet: A dataset wich will contain the resultset generated by the command
                ' -tableNames: this array will be used to create table mappings allowing the DataTables to be referenced
                '             by a user defined name (probably the actual table name)
                Public Overloads Shared Sub FillDataset(ByVal num_transaction As Integer, ByVal commandType As CommandType, _
                    ByVal commandText As String, ByVal dataSet As DataSet, ByVal tableNames() As String)
                    Try
                        If configurado Then
                            FillDataset(num_transaction, commandType, commandText, dataSet, tableNames, Nothing)
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("7 - FillDataset")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Sub

                ' Execute a SqlCommand (that returns a resultset) against the specified SqlTransaction
                ' using the provided parameters.
                ' e.g.:  
                '   FillDataset(trans, CommandType.StoredProcedure, "GetOrders", ds, new string() {"orders"}, new SqlParameter("@prodid", 24))
                ' Parameters:
                ' -transaction: A valid SqlTransaction
                ' -commandType: the CommandType (stored procedure, text, etc.)
                ' -commandText: the stored procedure name or T-SQL command
                ' -dataSet: A dataset wich will contain the resultset generated by the command
                ' -tableNames: this array will be used to create table mappings allowing the DataTables to be referenced
                ' by a user defined name (probably the actual table name)
                ' -commandParameters: An array of SqlParamters used to execute the command
                Public Overloads Shared Sub FillDataset(ByVal num_transaction As Integer, ByVal commandType As CommandType, _
                    ByVal commandText As String, ByVal dataSet As DataSet, ByVal tableNames() As String, _
                    ByVal ParamArray commandParameters() As SqlParameter)
                    Try
                        If configurado Then
                            InternalFillDataset(num_transaction, commandType, commandText, dataSet, tableNames, commandParameters)
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("8 - FillDataset")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Sub

                ' Execute a stored procedure via a SqlCommand (that returns a resultset) against the specified 
                ' SqlTransaction using the provided parameter values.  This method will query the database to discover the parameters for the 
                ' stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
                ' This method provides no access to output parameters or the stored procedure' s return value parameter.
                ' e.g.:  
                '   FillDataset(trans, "GetOrders", ds, new String(){"orders"}, 24, 36)
                ' Parameters:
                ' -transaction: A valid SqlTransaction
                ' -spName: the name of the stored procedure
                ' -dataSet: A dataset wich will contain the resultset generated by the command
                ' -tableNames: this array will be used to create table mappings allowing the DataTables to be referenced
                '             by a user defined name (probably the actual table name)
                ' -parameterValues: An array of objects to be assigned as the input values of the stored procedure
                Public Overloads Shared Sub FillDataset(ByVal num_transaction As Integer, ByVal spName As String, _
                    ByVal dataSet As DataSet, ByVal tableNames() As String, ByVal ParamArray parameterValues() As Object)
                    Try
                        If configurado Then
                            If (Transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                            If Not (Transaction Is Nothing) AndAlso (Transaction(num_transaction).Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                            If (dataSet Is Nothing) Then Throw New ArgumentNullException("dataSet")
                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                            ' If we receive parameter values, we need to figure out where they go
                            If Not parameterValues Is Nothing AndAlso parameterValues.Length > 0 Then

                                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                                Dim commandParameters() As SqlParameter = SQLParameterCache.GetSpParameterSet(Transaction(num_transaction).Connection, spName)

                                ' Assign the provided values to these parameters based on parameter order
                                AssignParameterValues(commandParameters, parameterValues)

                                ' Call the overload that takes an array of SqlParameters
                                FillDataset(num_transaction, CommandType.StoredProcedure, spName, dataSet, tableNames, commandParameters)
                            Else ' Otherwise we can just call the SP without params
                                FillDataset(num_transaction, CommandType.StoredProcedure, spName, dataSet, tableNames)
                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("9 - FillDataset")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Sub

                ' Private helper method that execute a SqlCommand (that returns a resultset) against the specified SqlTransaction and SqlConnection
                ' using the provided parameters.
                ' e.g.:  
                '   FillDataset(conn, trans, CommandType.StoredProcedure, "GetOrders", ds, new String() {"orders"}, new SqlParameter("@prodid", 24))
                ' Parameters:
                ' -connection: A valid SqlConnection
                ' -transaction: A valid SqlTransaction
                ' -commandType: the CommandType (stored procedure, text, etc.)
                ' -commandText: the stored procedure name or T-SQL command
                ' -dataSet: A dataset wich will contain the resultset generated by the command
                ' -tableNames: this array will be used to create table mappings allowing the DataTables to be referenced
                '             by a user defined name (probably the actual table name)
                ' -commandParameters: An array of SqlParamters used to execute the command
                Private Overloads Shared Sub InternalFillDataset(ByVal num_transaction As Integer, ByVal commandType As CommandType, _
                    ByVal commandText As String, ByVal dataSet As DataSet, ByVal tableNames() As String, _
                    ByVal ParamArray commandParameters() As SqlParameter)

                    Try
                        Dim Connection As SqlConnection
                        If (dataSet Is Nothing) Then Throw New ArgumentNullException("dataSet")

                        If Not num_transaction = -1 Then
                            Connection = Transaction(num_transaction).Connection
                        Else
                            Connection = PrepararConexao(Connection)
                        End If
                        ' Create a command and prepare it for execution
                        Dim command As New SqlCommand

                        Dim mustCloseConnection As Boolean = False
                        If num_transaction = -1 Then
                            PrepareCommand(command, Connection, Nothing, commandType, commandText, commandParameters, mustCloseConnection)
                        Else
                            PrepareCommand(command, Connection, Transaction(num_transaction), commandType, commandText, commandParameters, mustCloseConnection)
                        End If

                        ' Create the DataAdapter & DataSet
                        Dim dataAdapter As SqlDataAdapter = New SqlDataAdapter(command)

                        Try
                            ' Add the table mappings specified by the user
                            If Not tableNames Is Nothing AndAlso tableNames.Length > 0 Then

                                Dim tableName As String = "Table"
                                Dim index As Integer

                                For index = 0 To tableNames.Length - 1
                                    If (tableNames(index) Is Nothing OrElse tableNames(index).Length = 0) Then Throw New ArgumentException("The tableNames parameter must contain a list of tables, a value was provided as null or empty string.", "tableNames")
                                    dataAdapter.TableMappings.Add(tableName, tableNames(index))
                                    tableName = tableName & (index + 1).ToString()
                                Next
                            End If

                            ' Fill the DataSet using default values for DataTable names, etc
                            dataAdapter.Fill(dataSet)

                            ' Detach the SqlParameters from the command object, so they can be used again
                            command.Parameters.Clear()
                        Finally
                            If (Not dataAdapter Is Nothing) Then dataAdapter.Dispose()
                        End Try

                        If (mustCloseConnection) Then Connection.Close()
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "1 - InternalFillDataset")
                    End Try
                End Sub
#End Region

#Region "UpdateDataset"
                'N�o Utilizado
                ' Executes the respective command for each inserted, updated, or deleted row in the DataSet.
                ' e.g.:  
                '   UpdateDataset(conn, insertCommand, deleteCommand, updateCommand, dataSet, "Order")
                ' Parameters:
                ' -insertCommand: A valid transact-SQL statement or stored procedure to insert new records into the data source
                ' -deleteCommand: A valid transact-SQL statement or stored procedure to delete records from the data source
                ' -updateCommand: A valid transact-SQL statement or stored procedure used to update records in the data source
                ' -dataSet: the DataSet used to update the data source
                ' -tableName: the DataTable used to update the data source
                Private Overloads Shared Sub UpdateDataset(ByVal insertCommand As SqlCommand, ByVal deleteCommand As SqlCommand, ByVal updateCommand As SqlCommand, ByVal dataSet As DataSet, ByVal tableName As String)
                    Try
                        If (insertCommand Is Nothing) Then Throw New ArgumentNullException("insertCommand")
                        If (deleteCommand Is Nothing) Then Throw New ArgumentNullException("deleteCommand")
                        If (updateCommand Is Nothing) Then Throw New ArgumentNullException("updateCommand")
                        If (dataSet Is Nothing) Then Throw New ArgumentNullException("dataSet")
                        If (tableName Is Nothing OrElse tableName.Length = 0) Then Throw New ArgumentNullException("tableName")

                        ' Create a SqlDataAdapter, and dispose of it after we are done
                        Dim dataAdapter As New SqlDataAdapter
                        Try
                            ' Set the data adapter commands
                            dataAdapter.UpdateCommand = updateCommand
                            dataAdapter.InsertCommand = insertCommand
                            dataAdapter.DeleteCommand = deleteCommand

                            ' Update the dataset changes in the data source
                            dataAdapter.Update(dataSet, tableName)

                            ' Commit all the changes made to the DataSet
                            dataSet.AcceptChanges()
                        Finally
                            If (Not dataAdapter Is Nothing) Then dataAdapter.Dispose()
                        End Try
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "1 - UpdateDataset")
                    End Try
                End Sub
#End Region

#Region "CreateCommand"
                ' Simplify the creation of a Sql command object by allowing
                ' a stored procedure and optional parameters to be provided
                ' e.g.:  
                ' Dim command As SqlCommand = CreateCommand(conn, "AddCustomer", "CustomerID", "CustomerName")
                ' Parameters:
                ' -connection: A valid SqlConnection object
                ' -spName: the name of the stored procedure
                ' -sourceColumns: An array of string to be assigned as the source columns of the stored procedure parameters
                ' Returns:
                ' a valid SqlCommand object
                Private Overloads Shared Function CreateCommand(ByVal connection As SqlConnection, ByVal spName As String, ByVal ParamArray sourceColumns() As String) As SqlCommand
                    Try
                        If configurado Then
                            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
                            ' Create a SqlCommand
                            Dim cmd As New SqlCommand(spName, connection)
                            cmd.CommandType = CommandType.StoredProcedure

                            ' If we receive parameter values, we need to figure out where they go
                            If Not sourceColumns Is Nothing AndAlso sourceColumns.Length > 0 Then

                                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                                Dim commandParameters() As SqlParameter = SQLParameterCache.GetSpParameterSet(connection, spName)

                                ' Assign the provided source columns to these parameters based on parameter order
                                Dim index As Integer
                                For index = 0 To sourceColumns.Length - 1
                                    commandParameters(index).SourceColumn = sourceColumns(index)
                                Next

                                ' Attach the discovered parameters to the SqlCommand object
                                AttachParameters(cmd, commandParameters)
                            End If

                            CreateCommand = cmd
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("1 - CreateCommand")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function
#End Region

#Region "ExecuteNonQueryTypedParams"
                ' Execute a stored procedure via a SqlCommand (that returns no resultset) against the database specified in 
                ' the connection string using the dataRow column values as the stored procedure' s parameters values.
                ' This method will query the database to discover the parameters for the 
                ' stored procedure (the first time each stored procedure is called), and assign the values based on row values.
                ' Parameters:
                ' -connectionString: A valid connection string for a SqlConnection
                ' -spName: the name of the stored procedure
                ' -dataRow: The dataRow used to hold the stored procedure' s parameter values
                ' Returns:
                ' an int representing the number of rows affected by the command
                Private Overloads Shared Function ExecuteNonQueryTypedParams(ByVal connectionString As String, ByVal spName As String, ByVal dataRow As DataRow) As Integer
                    Try
                        If configurado Then
                            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                            ' If the row has values, the store procedure parameters must be initialized
                            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                                Dim commandParameters() As SqlParameter = SQLParameterCache.GetSpParameterSet(connectionString, spName)

                                ' Set the parameters values
                                AssignParameterValues(commandParameters, dataRow)

                                ExecuteNonQueryTypedParams = cCon.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, spName, commandParameters)
                            Else
                                ExecuteNonQueryTypedParams = cCon.ExecuteNonQuery(connectionString, CommandType.StoredProcedure, spName)
                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("1 - ExecuteNonQueryTypedParams")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function

                ' Execute a stored procedure via a SqlCommand (that returns no resultset) against the specified SqlConnection 
                ' using the dataRow column values as the stored procedure' s parameters values.  
                ' This method will query the database to discover the parameters for the 
                ' stored procedure (the first time each stored procedure is called), and assign the values based on row values.
                ' Parameters:
                ' -connection:a valid SqlConnection object
                ' -spName: the name of the stored procedure
                ' -dataRow:The dataRow used to hold the stored procedure' s parameter values.
                ' Returns:
                ' an int representing the number of rows affected by the command
                Private Overloads Shared Function ExecuteNonQueryTypedParams(ByVal connection As SqlConnection, ByVal spName As String, ByVal dataRow As DataRow) As Integer
                    Try
                        If configurado Then
                            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                            ' If the row has values, the store procedure parameters must be initialized
                            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                                Dim commandParameters() As SqlParameter = SQLParameterCache.GetSpParameterSet(connection, spName)

                                ' Set the parameters values
                                AssignParameterValues(commandParameters, dataRow)

                                ExecuteNonQueryTypedParams = cCon.ExecuteNonQuery(CommandType.StoredProcedure, spName, commandParameters)
                            Else
                                ExecuteNonQueryTypedParams = cCon.ExecuteNonQuery(connection, CommandType.StoredProcedure, spName)
                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("2 - ExecuteNonQueryTypedParams")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function

                ' Execute a stored procedure via a SqlCommand (that returns no resultset) against the specified
                ' SqlTransaction using the dataRow column values as the stored procedure' s parameters values.
                ' This method will query the database to discover the parameters for the 
                ' stored procedure (the first time each stored procedure is called), and assign the values based on row values.
                ' Parameters:
                ' -transaction:a valid SqlTransaction object
                ' -spName:the name of the stored procedure
                ' -dataRow:The dataRow used to hold the stored procedure' s parameter values.
                ' Returns:
                ' an int representing the number of rows affected by the command
                Public Overloads Shared Function ExecuteNonQueryTypedParams(ByVal num_transaction As Integer, ByVal spName As String, ByVal dataRow As DataRow) As Integer
                    Try
                        If configurado Then
                            If (Transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                            If Not (Transaction Is Nothing) AndAlso (Transaction(num_transaction).Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                            ' If the row has values, the store procedure parameters must be initialized
                            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                                Dim commandParameters() As SqlParameter = SQLParameterCache.GetSpParameterSet(Transaction(num_transaction).Connection, spName)

                                ' Set the parameters values
                                AssignParameterValues(commandParameters, dataRow)

                                ExecuteNonQueryTypedParams = cCon.ExecuteNonQuery(num_transaction, CommandType.StoredProcedure, spName, commandParameters)
                            Else

                                ExecuteNonQueryTypedParams = cCon.ExecuteNonQuery(num_transaction, CommandType.StoredProcedure, spName)
                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("3 - ExecuteNonQueryTypedParams")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function
#End Region

#Region "ExecuteDatasetTypedParams"
                'N�o Utilizado
                ' Execute a stored procedure via a SqlCommand (that returns a resultset) against the database specified in 
                ' the connection string using the dataRow column values as the stored procedure' s parameters values.
                ' This method will query the database to discover the parameters for the 
                ' stored procedure (the first time each stored procedure is called), and assign the values based on row values.
                ' Parameters:
                ' -connectionString: A valid connection string for a SqlConnection
                ' -spName: the name of the stored procedure
                ' -dataRow: The dataRow used to hold the stored procedure' s parameter values.
                ' Returns:
                ' a dataset containing the resultset generated by the command
                Private Overloads Shared Function ExecuteDatasetTypedParams(ByVal connectionString As String, ByVal spName As String, ByVal dataRow As DataRow) As DataSet
                    Try
                        If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
                        If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                        ' If the row has values, the store procedure parameters must be initialized
                        If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                            ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                            Dim commandParameters() As SqlParameter = SQLParameterCache.GetSpParameterSet(connectionString, spName)

                            ' Set the parameters values
                            AssignParameterValues(commandParameters, dataRow)

                            ExecuteDatasetTypedParams = cCon.ExecuteDataset(connectionString, CommandType.StoredProcedure, spName, commandParameters)
                        Else

                            ExecuteDatasetTypedParams = cCon.ExecuteDataset(connectionString, CommandType.StoredProcedure, spName)
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "1 - ExecuteDatasetTypedParams")
                    End Try
                End Function
                'N�o Utilizado
                ' Execute a stored procedure via a SqlCommand (that returns a resultset) against the specified SqlConnection 
                ' using the dataRow column values as the store procedure' s parameters values.
                ' This method will query the database to discover the parameters for the 
                ' stored procedure (the first time each stored procedure is called), and assign the values based on row values.
                ' Parameters:
                ' -connection: A valid SqlConnection object
                ' -spName: the name of the stored procedure
                ' -dataRow: The dataRow used to hold the stored procedure' s parameter values.
                ' Returns:
                ' a dataset containing the resultset generated by the command
                Private Overloads Shared Function ExecuteDatasetTypedParams(ByVal connection As SqlConnection, ByVal spName As String, ByVal dataRow As DataRow) As DataSet
                    Try
                        If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
                        If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                        ' If the row has values, the store procedure parameters must be initialized
                        If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                            ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                            Dim commandParameters() As SqlParameter = SQLParameterCache.GetSpParameterSet(connection, spName)

                            ' Set the parameters values
                            AssignParameterValues(commandParameters, dataRow)

                            ExecuteDatasetTypedParams = cCon.ExecuteDataset(connection, CommandType.StoredProcedure, spName, commandParameters)
                        Else

                            ExecuteDatasetTypedParams = cCon.ExecuteDataset(connection, CommandType.StoredProcedure, spName)
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "2 - ExecuteDatasetTypedParams")
                    End Try
                End Function
                'N�o Utilizado
                ' Execute a stored procedure via a SqlCommand (that returns a resultset) against the specified SqlTransaction 
                ' using the dataRow column values as the stored procedure' s parameters values.
                ' This method will query the database to discover the parameters for the 
                ' stored procedure (the first time each stored procedure is called), and assign the values based on row values.
                ' Parameters:
                ' -transaction: A valid SqlTransaction object
                ' -spName: the name of the stored procedure
                ' -dataRow: The dataRow used to hold the stored procedure' s parameter values.
                ' Returns:
                ' a dataset containing the resultset generated by the command
                Private Overloads Shared Function ExecuteDatasetTypedParams(ByVal num_transaction As Integer, ByVal spName As String, ByVal dataRow As DataRow) As DataSet
                    Try
                        If (Transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                        If Not (Transaction Is Nothing) AndAlso (Transaction(num_transaction).Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                        If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                        ' If the row has values, the store procedure parameters must be initialized
                        If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                            ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                            Dim commandParameters() As SqlParameter = SQLParameterCache.GetSpParameterSet(Transaction(num_transaction).Connection, spName)

                            ' Set the parameters values
                            AssignParameterValues(commandParameters, dataRow)

                            ExecuteDatasetTypedParams = cCon.ExecuteDataset(num_transaction, CommandType.StoredProcedure, spName, commandParameters)
                        Else

                            ExecuteDatasetTypedParams = cCon.ExecuteDataset(num_transaction, CommandType.StoredProcedure, spName)
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "3 - ExecuteDatasetTypedParams")
                    End Try
                End Function
#End Region

#Region "ExecuteReaderTypedParams"
                ' Execute a stored procedure via a SqlCommand (that returns a resultset) against the database specified in 
                ' the connection string using the dataRow column values as the stored procedure' s parameters values.
                ' This method will query the database to discover the parameters for the 
                ' stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
                ' Parameters:
                ' -connectionString: A valid connection string for a SqlConnection
                ' -spName: the name of the stored procedure
                ' -dataRow: The dataRow used to hold the stored procedure' s parameter values.
                ' Returns:
                ' a SqlDataReader containing the resultset generated by the command
                Private Overloads Shared Function ExecuteReaderTypedParams(ByVal connectionString As String, ByVal spName As String, ByVal dataRow As DataRow) As SqlDataReader
                    Try
                        If configurado Then
                            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                            ' If the row has values, the store procedure parameters must be initialized
                            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                                Dim commandParameters() As SqlParameter = SQLParameterCache.GetSpParameterSet(connectionString, spName)

                                ' Set the parameters values
                                AssignParameterValues(commandParameters, dataRow)

                                ExecuteReaderTypedParams = cCon.ExecuteReader(connectionString, CommandType.StoredProcedure, spName, commandParameters)
                            Else
                                ExecuteReaderTypedParams = cCon.ExecuteReader(connectionString, CommandType.StoredProcedure, spName)
                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("1 - ExecuteReaderTypedParams")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function

                ' Execute a stored procedure via a SqlCommand (that returns a resultset) against the specified SqlConnection 
                ' using the dataRow column values as the stored procedure' s parameters values.
                ' This method will query the database to discover the parameters for the 
                ' stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
                ' Parameters:
                ' -connection: A valid SqlConnection object
                ' -spName: The name of the stored procedure
                ' -dataRow: The dataRow used to hold the stored procedure' s parameter values.
                ' Returns:
                ' a SqlDataReader containing the resultset generated by the command
                Private Overloads Shared Function ExecuteReaderTypedParams(ByVal connection As SqlConnection, ByVal spName As String, ByVal dataRow As DataRow) As SqlDataReader
                    Try
                        If configurado Then
                            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                            ' If the row has values, the store procedure parameters must be initialized
                            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                                Dim commandParameters() As SqlParameter = SQLParameterCache.GetSpParameterSet(connection, spName)

                                ' Set the parameters values
                                AssignParameterValues(commandParameters, dataRow)

                                ExecuteReaderTypedParams = cCon.ExecuteReader(connection, CommandType.StoredProcedure, spName, commandParameters)
                            Else
                                ExecuteReaderTypedParams = cCon.ExecuteReader(connection, CommandType.StoredProcedure, spName)
                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("2 - ExecuteReaderTypedParams")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(DescricaoErro, ex)
                    End Try
                End Function

                ' Execute a stored procedure via a SqlCommand (that returns a resultset) against the specified SqlTransaction 
                ' using the dataRow column values as the stored procedure' s parameters values.
                ' This method will query the database to discover the parameters for the 
                ' stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
                ' Parameters:
                ' -transaction: A valid SqlTransaction object
                ' -spName" The name of the stored procedure
                ' -dataRow: The dataRow used to hold the stored procedure' s parameter values.
                ' Returns:
                ' a SqlDataReader containing the resultset generated by the command
                Public Overloads Shared Function ExecuteReaderTypedParams(ByVal num_transaction As Integer, ByVal spName As String, ByVal dataRow As DataRow) As SqlDataReader
                    Try
                        If configurado Then
                            If (Transaction(num_transaction) Is Nothing) Then Throw New ArgumentNullException("transaction")
                            If Not (Transaction(num_transaction) Is Nothing) AndAlso (Transaction(num_transaction).Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                            ' If the row has values, the store procedure parameters must be initialized
                            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                                Dim commandParameters() As SqlParameter = SQLParameterCache.GetSpParameterSet(Transaction(num_transaction).Connection, spName)

                                ' Set the parameters values
                                AssignParameterValues(commandParameters, dataRow)

                                ExecuteReaderTypedParams = cCon.ExecuteReader(num_transaction, CommandType.StoredProcedure, spName, commandParameters)
                            Else
                                ExecuteReaderTypedParams = cCon.ExecuteReader(num_transaction, CommandType.StoredProcedure, spName)
                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("3 - ExecuteReaderTypedParams")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function
#End Region

#Region "ExecuteScalarTypedParams"
                ' Execute a stored procedure via a SqlCommand (that returns a 1x1 resultset) against the database specified in 
                ' the connection string using the dataRow column values as the stored procedure' s parameters values.
                ' This method will query the database to discover the parameters for the 
                ' stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
                ' Parameters:
                ' -connectionString: A valid connection string for a SqlConnection
                ' -spName: The name of the stored procedure
                ' -dataRow: The dataRow used to hold the stored procedure' s parameter values.
                ' Returns:
                ' An object containing the value in the 1x1 resultset generated by the command</returns>
                Private Overloads Shared Function ExecuteScalarTypedParams(ByVal connectionString As String, ByVal spName As String, ByVal dataRow As DataRow) As Object
                    Try
                        If configurado Then
                            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
                            ' If the row has values, the store procedure parameters must be initialized
                            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                                Dim commandParameters() As SqlParameter = SQLParameterCache.GetSpParameterSet(connectionString, spName)

                                ' Set the parameters values
                                AssignParameterValues(commandParameters, dataRow)

                                ExecuteScalarTypedParams = cCon.ExecuteScalar(connectionString, CommandType.StoredProcedure, spName, commandParameters)
                            Else
                                ExecuteScalarTypedParams = cCon.ExecuteScalar(connectionString, CommandType.StoredProcedure, spName)
                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("1 - ExecuteScalarTypedParams")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function

                ' Execute a stored procedure via a SqlCommand (that returns a 1x1 resultset) against the specified SqlConnection 
                ' using the dataRow column values as the stored procedure' s parameters values.
                ' This method will query the database to discover the parameters for the 
                ' stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
                ' Parameters:
                ' -connection: A valid SqlConnection object
                ' -spName: the name of the stored procedure
                ' -dataRow: The dataRow used to hold the stored procedure' s parameter values.
                ' Returns: 
                ' an object containing the value in the 1x1 resultset generated by the command</returns>
                Private Overloads Shared Function ExecuteScalarTypedParams(ByVal connection As SqlConnection, ByVal spName As String, ByVal dataRow As DataRow) As Object
                    Try
                        If configurado Then
                            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                            ' If the row has values, the store procedure parameters must be initialized
                            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                                Dim commandParameters() As SqlParameter = SQLParameterCache.GetSpParameterSet(connection, spName)

                                ' Set the parameters values
                                AssignParameterValues(commandParameters, dataRow)

                                ExecuteScalarTypedParams = cCon.ExecuteScalar(CommandType.StoredProcedure, spName, commandParameters)
                            Else
                                ExecuteScalarTypedParams = cCon.ExecuteScalar(connection, CommandType.StoredProcedure, spName)
                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("2 - ExecuteScalarTypedParams")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function

                ' Execute a stored procedure via a SqlCommand (that returns a 1x1 resultset) against the specified SqlTransaction
                ' using the dataRow column values as the stored procedure' s parameters values.
                ' This method will query the database to discover the parameters for the 
                ' stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
                ' Parameters:
                ' -transaction: A valid SqlTransaction object
                ' -spName: the name of the stored procedure
                ' -dataRow: The dataRow used to hold the stored procedure' s parameter values.
                ' Returns: 
                ' an object containing the value in the 1x1 resultset generated by the command</returns>
                Public Overloads Shared Function ExecuteScalarTypedParams(ByVal transaction As SqlTransaction, ByVal spName As String, ByVal dataRow As DataRow) As Object
                    Try
                        If configurado Then
                            If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                            If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                            ' If the row has values, the store procedure parameters must be initialized
                            If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                                ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                                Dim commandParameters() As SqlParameter = SQLParameterCache.GetSpParameterSet(transaction.Connection, spName)

                                ' Set the parameters values
                                AssignParameterValues(commandParameters, dataRow)

                                ExecuteScalarTypedParams = cCon.ExecuteScalar(transaction, CommandType.StoredProcedure, spName, commandParameters)
                            Else
                                ExecuteScalarTypedParams = cCon.ExecuteScalar(transaction, CommandType.StoredProcedure, spName)
                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("3 - ExecuteScalarTypedParams")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function
#End Region

#Region "ExecuteXmlReaderTypedParams"
                ' Execute a stored procedure via a SqlCommand (that returns a resultset) against the specified SqlConnection 
                ' using the dataRow column values as the stored procedure' s parameters values.
                ' This method will query the database to discover the parameters for the 
                ' stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
                ' Parameters:
                ' -connection: A valid SqlConnection object
                ' -spName: the name of the stored procedure
                ' -dataRow: The dataRow used to hold the stored procedure' s parameter values.
                ' Returns: 
                ' an XmlReader containing the resultset generated by the command
                Private Overloads Shared Function ExecuteXmlReaderTypedParams(ByVal connection As SqlConnection, ByVal spName As String, ByVal dataRow As DataRow) As XmlReader
                    Try
                        If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
                        If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
                        ' If the row has values, the store procedure parameters must be initialized
                        If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                            ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                            Dim commandParameters() As SqlParameter = SQLParameterCache.GetSpParameterSet(connection, spName)

                            ' Set the parameters values
                            AssignParameterValues(commandParameters, dataRow)

                            ExecuteXmlReaderTypedParams = cCon.ExecuteXmlReader(CommandType.StoredProcedure, spName, commandParameters)
                        Else
                            ExecuteXmlReaderTypedParams = cCon.ExecuteXmlReader(CommandType.StoredProcedure, spName)
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "1 - ExecuteXmlReaderTypedParams")
                    End Try
                End Function

                ' Execute a stored procedure via a SqlCommand (that returns a resultset) against the specified SqlTransaction 
                ' using the dataRow column values as the stored procedure' s parameters values.
                ' This method will query the database to discover the parameters for the 
                ' stored procedure (the first time each stored procedure is called), and assign the values based on parameter order.
                ' Parameters:
                ' -transaction: A valid SqlTransaction object
                ' -spName: the name of the stored procedure
                ' -dataRow: The dataRow used to hold the stored procedure' s parameter values.
                ' Returns: 
                ' an XmlReader containing the resultset generated by the command
                Public Overloads Shared Function ExecuteXmlReaderTypedParams(ByVal transaction As SqlTransaction, ByVal spName As String, ByVal dataRow As DataRow) As XmlReader
                    Try
                        If (transaction Is Nothing) Then Throw New ArgumentNullException("transaction")
                        If Not (transaction Is Nothing) AndAlso (transaction.Connection Is Nothing) Then Throw New ArgumentException("The transaction was rollbacked or commited, please provide an open transaction.", "transaction")
                        If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
                        ' if the row has values, the store procedure parameters must be initialized
                        If (Not dataRow Is Nothing AndAlso dataRow.ItemArray.Length > 0) Then

                            ' Pull the parameters for this stored procedure from the parameter cache (or discover them & populate the cache)
                            Dim commandParameters() As SqlParameter = SQLParameterCache.GetSpParameterSet(transaction.Connection, spName)

                            ' Set the parameters values
                            AssignParameterValues(commandParameters, dataRow)

                            ExecuteXmlReaderTypedParams = cCon.ExecuteXmlReader(transaction, CommandType.StoredProcedure, spName, commandParameters)
                        Else
                            ExecuteXmlReaderTypedParams = cCon.ExecuteXmlReader(transaction, CommandType.StoredProcedure, spName)
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(ex, "2 - ExecuteXmlReaderTypedParams")
                    End Try
                End Function
#End Region

#Region "ControleTransacional"
                Public Shared Function BeginTransaction() As Integer
                    Try
                        If configurado Then
                            If Transaction Is Nothing Then
                                ReDim Preserve Transaction(0)
                            End If

                            Dim num As Integer = UBound(Transaction)

                            Dim cont As Integer = 0
                            Do While cont < num
                                If TranConnection(cont) Is Nothing Then
                                    TranConnection(cont) = New SqlConnection(ConnectionString)
                                    TranConnection(cont).Open()
                                    Transaction(cont) = TranConnection(cont).BeginTransaction
                                    Exit Do
                                End If
                                cont = cont + 1
                            Loop
                            If cont = num Then
                                ReDim Preserve TranConnection(num + 1)
                                ReDim Preserve Transaction(num + 1)
                                num = num + 1
                                TranConnection(num - 1) = New SqlConnection(ConnectionString)
                                TranConnection(num - 1).Open()

                                Transaction(num - 1) = TranConnection(num - 1).BeginTransaction
                                Return num - 1
                            Else
                                Return cont
                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("1 - BeginTransaction")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function

                Public Shared Function RollBackTransaction(ByVal num_tran As Integer) As Integer
                    Try
                        If configurado Then
                            Dim num As Integer = UBound(TranConnection)
                            num_tran = num_tran + 1
                            If num = num_tran Then
                                Transaction(num_tran - 1).Rollback()
                                Transaction(num_tran - 1) = Nothing
                                TranConnection(num_tran - 1).Close()
                                TranConnection(num_tran - 1) = Nothing
                                ReDim Preserve TranConnection(num - 1)
                                ReDim Preserve Transaction(num - 1)
                                If UBound(TranConnection) - 1 > 0 Then
                                    Do While TranConnection(UBound(TranConnection) - 1) Is Nothing
                                        ReDim Preserve TranConnection(UBound(TranConnection) - 1)
                                        ReDim Preserve Transaction(UBound(TranConnection) - 1)
                                    Loop
                                End If

                            Else

                                Transaction(num_tran - 1).Rollback()
                                Transaction(num_tran - 1) = Nothing
                                TranConnection(num_tran - 1).Close()
                                TranConnection(num_tran - 1) = Nothing
                                If UBound(TranConnection) - 1 > 0 Then
                                    Do While TranConnection(UBound(TranConnection) - 1) Is Nothing
                                        ReDim Preserve TranConnection(UBound(TranConnection) - 1)
                                        ReDim Preserve Transaction(UBound(TranConnection) - 1)
                                    Loop
                                End If

                            End If
                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("1 - RollBackTransaction")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function

                Public Shared Function CommitTransaction(ByVal num_tran As Integer) As Integer
                    Try
                        If configurado Then
                            Dim num As Integer = UBound(TranConnection)
                            num_tran = num_tran + 1
                            If num = num_tran Then
                                Transaction(num_tran - 1).Commit()
                                Transaction(num_tran - 1) = Nothing
                                TranConnection(num_tran - 1).Close()
                                TranConnection(num_tran - 1) = Nothing
                                ReDim Preserve TranConnection(num - 1)
                                ReDim Preserve Transaction(num - 1)
                                If UBound(TranConnection) - 1 > 0 Then
                                    Do While TranConnection(UBound(TranConnection) - 1) Is Nothing
                                        ReDim Preserve TranConnection(UBound(TranConnection) - 1)
                                        ReDim Preserve Transaction(UBound(TranConnection) - 1)
                                    Loop
                                End If
                            Else

                                Transaction(num_tran - 1).Commit()
                                Transaction(num_tran - 1) = Nothing
                                TranConnection(num_tran - 1).Close()
                                TranConnection(num_tran - 1) = Nothing
                                If UBound(TranConnection) - 1 > 0 Then
                                    Do While TranConnection(UBound(TranConnection) - 1) Is Nothing
                                        ReDim Preserve TranConnection(UBound(TranConnection) - 1)
                                        ReDim Preserve Transaction(UBound(TranConnection) - 1)
                                    Loop
                                End If

                            End If

                        Else
                            _DescricaoErro = "Conex�o n�o configurada, utilize o m�todo cCon.ConfiguraConexao"
                            Throw New ArgumentException("1 - CommitTransaction")
                        End If
                    Catch ex As AcessoBDException
                        Throw ex
                    Catch ex As Exception
                        Throw New AcessoBDException(_DescricaoErro, ex)
                    End Try
                End Function


#End Region

#Region "SQLParameterCache"
                ' SQLParameterCache provides functions to leverage a static cache of procedure parameters, and the
                ' ability to discover parameters for stored procedures at run-time.

                Private NotInheritable Class SQLParameterCache

#Region "private methods, variables, and constructors"


                    ' Since this class provides only static methods, make the default constructor private to prevent 
                    ' instances from being created with "new SQLParameterCache()".
                    Private Sub New()
                    End Sub ' New 

                    Private Shared paramCache As Hashtable = Hashtable.Synchronized(New Hashtable)

                    ' resolve at run time the appropriate set of SqlParameters for a stored procedure
                    ' Parameters:
                    ' - connectionString - a valid connection string for a SqlConnection
                    ' - spName - the name of the stored procedure
                    ' - includeReturnValueParameter - whether or not to include their return value parameter>
                    ' Returns: SqlParameter()
                    Private Shared Function DiscoverSpParameterSet(ByVal connection As SqlConnection, _
                                                                       ByVal spName As String, _
                                                                       ByVal includeReturnValueParameter As Boolean, _
                                                                       ByVal ParamArray parameterValues() As Object) As SqlParameter()
                        Try
                            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")
                            Dim cmd As New SqlCommand(spName, connection)
                            cmd.CommandType = CommandType.StoredProcedure
                            Dim discoveredParameters() As SqlParameter
                            connection.Open()
                            SqlCommandBuilder.DeriveParameters(cmd)
                            connection.Close()
                            If Not includeReturnValueParameter Then
                                cmd.Parameters.RemoveAt(0)
                            End If

                            discoveredParameters = New SqlParameter(cmd.Parameters.Count - 1) {}
                            cmd.Parameters.CopyTo(discoveredParameters, 0)

                            ' Init the parameters with a DBNull value
                            Dim discoveredParameter As SqlParameter
                            For Each discoveredParameter In discoveredParameters
                                discoveredParameter.Value = DBNull.Value
                            Next

                            Return discoveredParameters
                        Catch ex As AcessoBDException
                            Throw ex
                        Catch ex As Exception
                            Throw New AcessoBDException(ex, "1 - DiscoverSpParameterSet")
                        End Try

                    End Function ' DiscoverSpParameterSet

                    ' Deep copy of cached SqlParameter array
                    Private Shared Function CloneParameters(ByVal originalParameters() As SqlParameter) As SqlParameter()
                        Try
                            Dim i As Integer
                            Dim j As Integer = originalParameters.Length - 1
                            Dim clonedParameters(j) As SqlParameter

                            For i = 0 To j
                                clonedParameters(i) = CType(CType(originalParameters(i), ICloneable).Clone, SqlParameter)
                            Next

                            Return clonedParameters
                        Catch ex As AcessoBDException
                            Throw ex
                        Catch ex As Exception
                            Throw New AcessoBDException(ex, "1 - CloneParameters")
                        End Try
                    End Function ' CloneParameters

#End Region

#Region "caching functions"

                    ' add parameter array to the cache
                    ' Parameters
                    ' -connectionString - a valid connection string for a SqlConnection 
                    ' -commandText - the stored procedure name or T-SQL command 
                    ' -commandParameters - an array of SqlParamters to be cached 
                    Public Shared Sub CacheParameterSet(ByVal connectionString As String, _
                                                        ByVal commandText As String, _
                                                        ByVal ParamArray commandParameters() As SqlParameter)
                        Try
                            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
                            If (commandText Is Nothing OrElse commandText.Length = 0) Then Throw New ArgumentNullException("commandText")

                            Dim hashKey As String = connectionString + ":" + commandText

                            paramCache(hashKey) = commandParameters
                        Catch ex As AcessoBDException
                            Throw ex
                        Catch ex As Exception
                            Throw New AcessoBDException(ex, "1 - CacheParameterSet")
                        End Try
                    End Sub ' CacheParameterSet

                    ' retrieve a parameter array from the cache
                    ' Parameters:
                    ' -connectionString - a valid connection string for a SqlConnection 
                    ' -commandText - the stored procedure name or T-SQL command 
                    ' Returns: An array of SqlParamters 
                    Public Shared Function GetCachedParameterSet(ByVal connectionString As String, ByVal commandText As String) As SqlParameter()
                        Try
                            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
                            If (commandText Is Nothing OrElse commandText.Length = 0) Then Throw New ArgumentNullException("commandText")

                            Dim hashKey As String = connectionString + ":" + commandText
                            Dim cachedParameters As SqlParameter() = CType(paramCache(hashKey), SqlParameter())

                            If cachedParameters Is Nothing Then
                                Return Nothing
                            Else
                                Return CloneParameters(cachedParameters)
                            End If
                        Catch ex As AcessoBDException
                            Throw ex
                        Catch ex As Exception
                            Throw New AcessoBDException(ex, "1 - GetCachedParameterSet")
                        End Try
                    End Function ' GetCachedParameterSet

#End Region

#Region "Parameter Discovery Functions"
                    ' Retrieves the set of SqlParameters appropriate for the stored procedure.
                    ' This method will query the database for this information, and then store it in a cache for future requests.
                    ' Parameters:
                    ' -connectionString - a valid connection string for a SqlConnection 
                    ' -spName - the name of the stored procedure 
                    ' Returns: An array of SqlParameters
                    Public Overloads Shared Function GetSpParameterSet(ByVal connectionString As String, ByVal spName As String) As SqlParameter()
                        Try
                            Return GetSpParameterSet(connectionString, spName, False)
                        Catch ex As AcessoBDException
                            Throw ex
                        Catch ex As Exception
                            Throw New AcessoBDException(ex, "1 - GetSpParameterSet")
                        End Try
                    End Function ' GetSpParameterSet 

                    ' Retrieves the set of SqlParameters appropriate for the stored procedure.
                    ' This method will query the database for this information, and then store it in a cache for future requests.
                    ' Parameters:
                    ' -connectionString - a valid connection string for a SqlConnection
                    ' -spName - the name of the stored procedure 
                    ' -includeReturnValueParameter - a bool value indicating whether the return value parameter should be included in the results 
                    ' Returns: An array of SqlParameters 
                    Public Overloads Shared Function GetSpParameterSet(ByVal connectionString As String, _
                                                                       ByVal spName As String, _
                                                                       ByVal includeReturnValueParameter As Boolean) As SqlParameter()
                        Try
                            If (connectionString Is Nothing OrElse connectionString.Length = 0) Then Throw New ArgumentNullException("connectionString")
                            Dim connection As SqlConnection
                            Try
                                connection = New SqlConnection(connectionString)
                                GetSpParameterSet = GetSpParameterSetInternal(connection, spName, includeReturnValueParameter)
                            Finally
                                'If Not connection Is Nothing Then connection.Dispose()
                            End Try
                        Catch ex As AcessoBDException
                            Throw ex
                        Catch ex As Exception
                            Throw New AcessoBDException(ex, "2 - GetSpParameterSet")
                        End Try
                    End Function ' GetSpParameterSet

                    ' Retrieves the set of SqlParameters appropriate for the stored procedure.
                    ' This method will query the database for this information, and then store it in a cache for future requests.
                    ' Parameters:
                    ' -connection - a valid SqlConnection object
                    ' -spName - the name of the stored procedure 
                    ' -includeReturnValueParameter - a bool value indicating whether the return value parameter should be included in the results 
                    ' Returns: An array of SqlParameters 
                    Public Overloads Shared Function GetSpParameterSet(ByVal connection As SqlConnection, _
                                                                       ByVal spName As String) As SqlParameter()
                        Try
                            GetSpParameterSet = GetSpParameterSet(connection, spName, False)
                        Catch ex As AcessoBDException
                            Throw ex
                        Catch ex As Exception
                            Throw New AcessoBDException(ex, "3 - GetSpParameterSet")
                        End Try
                    End Function ' GetSpParameterSet

                    ' Retrieves the set of SqlParameters appropriate for the stored procedure.
                    ' This method will query the database for this information, and then store it in a cache for future requests.
                    ' Parameters:
                    ' -connection - a valid SqlConnection object
                    ' -spName - the name of the stored procedure 
                    ' -includeReturnValueParameter - a bool value indicating whether the return value parameter should be included in the results 
                    ' Returns: An array of SqlParameters 
                    Public Overloads Shared Function GetSpParameterSet(ByVal connection As SqlConnection, _
                                                                       ByVal spName As String, _
                                                                       ByVal includeReturnValueParameter As Boolean) As SqlParameter()
                        Try
                            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")
                            Dim clonedConnection As SqlConnection
                            Try
                                clonedConnection = CType((CType(connection, ICloneable).Clone), SqlConnection)
                                GetSpParameterSet = GetSpParameterSetInternal(clonedConnection, spName, includeReturnValueParameter)
                            Finally
                                If Not clonedConnection Is Nothing Then clonedConnection.Dispose()
                            End Try
                        Catch ex As AcessoBDException
                            Throw ex
                        Catch ex As Exception
                            Throw New AcessoBDException(ex, "4 - GetSpParameterSet")
                        End Try
                    End Function ' GetSpParameterSet

                    ' Retrieves the set of SqlParameters appropriate for the stored procedure.
                    ' This method will query the database for this information, and then store it in a cache for future requests.
                    ' Parameters:
                    ' -connection - a valid SqlConnection object
                    ' -spName - the name of the stored procedure 
                    ' -includeReturnValueParameter - a bool value indicating whether the return value parameter should be included in the results 
                    ' Returns: An array of SqlParameters 
                    Private Overloads Shared Function GetSpParameterSetInternal(ByVal connection As SqlConnection, _
                                                                    ByVal spName As String, _
                                                                    ByVal includeReturnValueParameter As Boolean) As SqlParameter()
                        Try
                            If (connection Is Nothing) Then Throw New ArgumentNullException("connection")

                            Dim cachedParameters() As SqlParameter
                            Dim hashKey As String

                            If (spName Is Nothing OrElse spName.Length = 0) Then Throw New ArgumentNullException("spName")

                            hashKey = connection.ConnectionString + ":" + spName + IIf(includeReturnValueParameter = True, ":include ReturnValue Parameter", "").ToString

                            cachedParameters = CType(paramCache(hashKey), SqlParameter())

                            If (cachedParameters Is Nothing) Then
                                Dim spParameters() As SqlParameter = DiscoverSpParameterSet(connection, spName, includeReturnValueParameter)
                                paramCache(hashKey) = spParameters
                                cachedParameters = spParameters

                            End If

                            Return CloneParameters(cachedParameters)
                        Catch ex As AcessoBDException
                            Throw ex
                        Catch ex As Exception
                            Throw New AcessoBDException(ex, "1 - GetSpParameterSetInternal")
                        End Try
                    End Function ' GetSpParameterSet
#End Region

                End Class ' SQLParameterCache 
#End Region

#Region "Tratamento de Erro"
                Private Class AcessoBDException

                    Inherits Exception


                    Public Sub New()
                        MyBase.New("N�o Identificado" & " - Erro de acesso ao Banco de Dados")
                    End Sub

                    Public Sub New(ByVal message As String, Optional ByVal cod_erro As String = "")
                        MyBase.New(cod_erro & " - " & message)
                    End Sub

                    Public Sub New(ByVal message As String, ByVal innerException As Exception, Optional ByVal cod_erro As String = "")
                        MyBase.New(cod_erro & " - " & message, innerException)
                    End Sub

                    Public Sub New(ByVal innerException As Exception, Optional ByVal cod_erro As String = "")
                        MyBase.New(cod_erro & " - Erro ", innerException)
                    End Sub


                End Class
#End Region
            End Class ' SQL
#End Region
        End Namespace
    End Namespace
End Namespace
