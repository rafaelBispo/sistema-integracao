Imports System
Imports System.IO
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices
Imports Alianca.Seguranca.BancoDados
Imports Alianca.Seguranca.Web

'<Assembly: ApplicationName("SABL0101")> 
'<Assembly: ApplicationActivation(ActivationOption.Server)> 
'<Assembly: ApplicationAccessControl(False, AccessChecksLevel:=AccessChecksLevelOption.ApplicationComponent)> 
Namespace Alianca
    Namespace Seguranca
        Public NotInheritable Class Erro
            Private Shared _Usuario_ID As Integer
            Private Shared _AssemblInfo As System.Reflection.Assembly
            Private Shared _ambiente As cCon.Ambientes
            Private Shared _bancodados As String
            Private Shared _post As String
            Private Shared _URL As String
            Private Shared _ambiente_id As Boolean
            Public Sub New()
            End Sub

            Public Shared Function TrataErro(ByVal erro As Exception) As String
                Try
                    Dim errMessage As String = ""
                    Dim tempException As Exception = erro

                    While (Not tempException Is Nothing)
                        errMessage += tempException.Message + Environment.NewLine + Environment.NewLine
                        tempException = tempException.InnerException
                    End While

                    Dim StackTrace As String
                    tempException = erro
                    While (Not tempException Is Nothing)
                        StackTrace += tempException.StackTrace + Environment.NewLine
                        tempException = tempException.InnerException
                    End While

                    Dim msg_retorno As String
                    Dim App_Assembly As System.Reflection.Assembly = System.Reflection.Assembly.GetExecutingAssembly
                    If cCon.Ambiente = cCon.Ambientes.Produ��o Then
                        configura_conexao()
                        msg_retorno = LogErro(App_Assembly, StackTrace, errMessage)
                        reload_conexao()
                    Else
                        msg_retorno = StackTrace & errMessage
                    End If
                    Return msg_retorno
                Catch ex As Exception
                    Return "0 - Opera��o n�o Realizada"
                End Try

            End Function
            Private Shared Sub configura_conexao()
                If cCon.configurado Then
                    _AssemblInfo = cCon.AssemblyInfo
                    _ambiente = cCon.Ambiente
                    _bancodados = cCon.BancodeDados
                    cCon.ReConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cCon.Ambientes.Produ��o)
                Else
                    cCon.ConfiguraConexao(System.Reflection.Assembly.GetExecutingAssembly, cCon.Ambientes.Produ��o)
                End If
            End Sub
            Private Shared Sub reload_conexao()
                If Not _AssemblInfo Is Nothing Then
                    cCon.Reset(_AssemblInfo, _ambiente)
                    cCon.BancodeDados = _bancodados
                Else
                    cCon.Reset()
                End If
            End Sub

            Private Shared Sub reload_conexaoAutWindows()
                If Not _AssemblInfo Is Nothing Then
                    cCon.ResetAutWindows(_AssemblInfo, _ambiente)
                    cCon.BancodeDados = _bancodados
                Else
                    cCon.ResetAutWindows()
                End If
            End Sub

            Private Shared Function LogErro(ByVal App_Assembly As System.Reflection.Assembly, ByVal StackTrace As String, ByVal errMessage As String) As String
                Return "Opera��o n�o Realizada"
            End Function
        End Class
    End Namespace
End Namespace
