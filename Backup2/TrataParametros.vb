Imports System
Imports System.IO
Imports Alianca.Seguranca.BancoDados
Imports Alianca.Sistema.ControleAmbientes
Namespace Alianca
    Namespace Sistema
        Namespace Funcoes
            Public Class TrataParametros

                ' Constantes
                Private Const gsCHAVESISTEMA = "@Li�c@"

                Private Shared gsPastaLocalSegbr As String
                Public Shared gsPastaServidorSegbr As String

                Public Shared gsHost As String
                Public Shared gsMac As String
                Public Shared gsIp As String

                Private Shared _gsCodAplicacao As String
                Shared ReadOnly Property gsCodAplicacao() As String
                    Get
                        gsCodAplicacao = _gsCodAplicacao
                    End Get
                End Property


                Private Shared _gsSIGLASISTEMA As String
                Shared ReadOnly Property gsSIGLASISTEMA() As String
                    Get
                        gsSIGLASISTEMA = _gsSIGLASISTEMA
                    End Get
                End Property


                Public Shared _gsParamAplicacao As String
                Shared ReadOnly Property gsParamAplicacao() As String
                    Get
                        gsParamAplicacao = _gsParamAplicacao
                    End Get
                End Property


                Private Shared _gsCPF As String
                Shared ReadOnly Property gsCPF() As String
                    Get
                        gsCPF = _gsCPF
                    End Get
                End Property

                Private Shared _glAmbiente_id As Alianca.Sistema.ControleAmbientes.Ambientes
                Shared ReadOnly Property glAmbiente_id() As Alianca.Sistema.ControleAmbientes.Ambientes
                    Get
                        glAmbiente_id = _glAmbiente_id
                    End Get
                End Property


                Private Shared _glAmbiente As Alianca.Sistema.ControleAmbientes.Ambientes
                Shared ReadOnly Property glAmbiente() As Alianca.Sistema.ControleAmbientes.Ambientes
                    Get
                        glAmbiente = _glAmbiente
                    End Get
                End Property


                Private Shared _glEmpresa_id As Long
                Public Shared ReadOnly Property glEmpresa_id() As Long
                    Get
                        glEmpresa_id = _glEmpresa_id
                    End Get
                End Property

                Private Shared _cLoginRede As String = Nothing
                Public Shared ReadOnly Property cLoginRede() As String
                    Get
                        If _cLoginRede Is Nothing Then
                            Dim dset As DataSet = cCon.ExecuteDataset(CommandType.Text, "SEGAB_DB..VERIFICA_CPF_USUARIO_SPS '" & gsCPF & "'")
                            _cLoginRede = dset.Tables(0).Rows(0).Item("login_rede")
                            _NomeUsuario = dset.Tables(0).Rows(0).Item("nome")
                            _LoginWeb = dset.Tables(0).Rows(0).Item("login_web")
                            cLoginRede = _cLoginRede
                        Else
                            cLoginRede = _cLoginRede
                        End If
                    End Get
                End Property

                Private Shared _NomeUsuario As String
                Public Shared ReadOnly Property NomeUsuario() As String
                    Get
                        If _NomeUsuario Is Nothing Then
                            Dim dset As DataSet = cCon.ExecuteDataset(CommandType.Text, "SEGAB_DB..VERIFICA_CPF_USUARIO_SPS '" & gsCPF & "'")
                            _cLoginRede = dset.Tables(0).Rows(0).Item("login_rede")
                            _NomeUsuario = dset.Tables(0).Rows(0).Item("nome")
                            _LoginWeb = dset.Tables(0).Rows(0).Item("login_web")
                            NomeUsuario = _NomeUsuario
                        Else
                            NomeUsuario = _NomeUsuario
                        End If
                    End Get
                End Property

                Private Shared _LoginWeb As String
                Private Shared ReadOnly Property LoginWeb() As String
                    Get
                        LoginWeb = _LoginWeb
                        If _LoginWeb Is Nothing Then
                            Dim dset As DataSet = cCon.ExecuteDataset(CommandType.Text, "SEGAB_DB..VERIFICA_CPF_USUARIO_SPS '" & gsCPF & "'")
                            _cLoginRede = dset.Tables(0).Rows(0).Item("login_rede")
                            _NomeUsuario = dset.Tables(0).Rows(0).Item("nome")
                            _LoginWeb = dset.Tables(0).Rows(0).Item("login_web")
                            LoginWeb = _LoginWeb
                        Else
                            LoginWeb = _LoginWeb
                        End If

                    End Get
                End Property

                Private Shared _Data_Sistema As Date
                Public Shared ReadOnly Property Data_Sistema() As String
                    Get
                        Data_Sistema = _Data_Sistema
                    End Get
                End Property

                Private Shared _Data_Contabilizacao As Date
                Public Shared ReadOnly Property Data_Contabilizacao() As Date
                    Get
                        Data_Contabilizacao = _Data_Contabilizacao
                    End Get
                End Property

                ' constantes de API
                Shared _AppEXEName As String
                Public Shared Property APPEXEName() As String
                    Get
                        APPEXEName = _AppEXEName
                    End Get
                    Set(ByVal Value As String)
                        _AppEXEName = Value
                    End Set
                End Property

                ' declara��es de API

                'Barney - 30/12/2003 - Fim _________
                Shared Function Trata_Parametros(ByVal sParametro As String, ByVal assemblyinfo As System.Reflection.Assembly) As Boolean

                    Dim vValoresPassados() As String
                    Dim lErro As Long
                    Dim sHostName As String
                    Dim sIp As String
                    Dim Diferenca As Long


                    Try

                        Trata_Parametros = False

                        _gsParamAplicacao = Left(sParametro, InStrRev(sParametro, " "))

                        ' pega ap�s o �ltimo espa�o em branco, onde est� a cadeia de criptografia
                        sParametro = Mid(sParametro, Len(gsParamAplicacao) + 1)
                        _gsParamAplicacao = Trim(gsParamAplicacao)

                        sParametro = Right(sParametro, Len(sParametro) - InStrRev(sParametro, " "))
                        ' criptografa 2 vezes, um com chave e depois volta de Ascii para String
                        sParametro = ConvASCIIString(sParametro)
                        sParametro = Decifra(sParametro, gsCHAVESISTEMA)
                        sParametro = Decifra(sParametro)

                        'pega os par�metros passados
                        vValoresPassados = Split(sParametro, "|")
                        'gsDHEntrada = CType(DataHoraAmbiente(), String)

                        If UBound(vValoresPassados) > 2 Then

                            _gsCodAplicacao = CType(vValoresPassados(0), String)

                            Try
                                Dim m_Title As System.Reflection.AssemblyTitleAttribute
                                m_Title = CType(assemblyinfo.GetCustomAttributes(GetType(System.Reflection.AssemblyTitleAttribute), False)(0), System.Reflection.AssemblyTitleAttribute)

                                If UCase(m_Title.Title.ToString) <> Replace(UCase(_gsCodAplicacao.ToString), ".EXE", "") Then
                                    Exit Function
                                End If
                            Catch ex As Exception
                                Throw
                            End Try

                            _glAmbiente_id = CLng(Trim(CType(vValoresPassados(1), String)))
                            _glEmpresa_id = CLng(Trim(vValoresPassados(2)))
                            _gsCPF = CStr(vValoresPassados(3))

                            If Len(Trim(gsCPF)) = 0 Then
                                Call GravaMensagem("N�o passou o CPF.", "exec", "trata_parametros", "Funcoes.bas")
                                Exit Function
                            End If

                            'TROCA O USERNAME

                            Trata_Parametros = True

                        End If
                    Catch Ex As Exception
                    End Try
                End Function
                Private Shared Sub GravaMensagem(ByVal PMensagem As String, ByVal PTipo As String, _
                       Optional ByVal PRotina As String = "", _
                       Optional ByVal PArquivo As String = "")
                    Try
                        'Dim Erro As New Erro.Log

                        'Erro.Usuario = CType(IIf(Trim(_cLoginRede) = "", gsIp & "~" & gsHost, _cLoginRede), String)
                        'Erro.NmProjeto = gsSIGLASISTEMA
                        'Erro.NmAplicacao = APPEXEName
                        'Erro.NmForm = PArquivo
                        ''Erro.NmArquivo = PArquivo
                        'Erro.NmRotina = PRotina

                        'Call Erro.Grava_Log_Erro(0, PMensagem, PTipo, CType(Obtem_agenda_diaria_id(gsParamAplicacao), Integer))


                    Catch
                    End Try

                    ' se houver erro, sai da rotina
                End Sub

                Public Shared Function DataHoraAmbiente(Optional ByVal AForma As String = "dd/mm/yyyy hh:nn:ss") As Date

                    Dim RX As DataSet

                    RX = cCon.ExecuteDataset(CommandType.Text, "SEL" & "ECT CONVERT(smalldatetime, GETDATE())")
                    DataHoraAmbiente = RX.Tables(0).Rows(0).Item(0)
                    RX = Nothing

                End Function
                Shared Function Obtem_agenda_diaria_id(ByVal OParametro As String) As Long
                    'Raphael Luiz Gagliardi 22/05/2001
                    'Obt�m agenda_diaria_id a partir do par�metro passado
                    'Se retornar 0 --> Manual
                    Dim lposicao1 As Long
                    Dim lposicao2 As Long

                    lposicao1 = InStr(1, OParametro, "||")
                    lposicao2 = InStr(CType(lposicao1 + 1, Integer), CType(OParametro, String), "||")
                    If lposicao1 > 0 And lposicao2 > 0 Then
                        Obtem_agenda_diaria_id = CType(Mid(OParametro, CType(lposicao1, Integer) + 2, CType(lposicao2, Integer) - CType(lposicao1, Integer) - 2), Long)
                    Else
                        Obtem_agenda_diaria_id = 0
                    End If

                End Function

                Private Shared Function ConvASCIIString(ByVal CadeiaASCII As String) As String
                    Dim Auxiliar As String
                    Dim VetorNumero() As String
                    Dim ONumero As Integer = 0
                    Auxiliar = ""
                    VetorNumero = Split(Mid(CadeiaASCII, 2), ",")
                    Dim numero As Integer = 0
                    Do While numero <= UBound(VetorNumero)
                        If IsNumeric(VetorNumero(numero)) Then
                            If Val(ONumero) < 256 Then
                                Auxiliar = Inc(Auxiliar, Chr(CType(Val(VetorNumero(numero)), Integer)))
                            Else
                                ConvASCIIString = ""
                                Exit Function
                            End If
                        Else
                            ConvASCIIString = ""
                            Exit Function
                        End If
                        numero = numero + 1
                    Loop
                    ConvASCIIString = Auxiliar
                End Function
                ' Respons�vel : Jo�o Mac-Cormick
                ' Dt Desenv : 03/06/2000
                '
                ' faz a express�o aumentar de um determinado
                ' incremento seja cadeia, num�rica ou l�gica
                Public Shared Function Inc(ByVal Expressao As String, Optional ByVal Incremento As Object = 1) As String
                    'If Not IsNull(Incremento) Then
                    Expressao = CType(Expressao, String) + CType(Incremento, String)
                    Return Expressao
                End Function

                '------------------------------------------------------------------------------------------
                '
                ' Respons�vel : Jo�o Mac-Cormick
                ' Dt Desenv : 03/06/2000
                '
                ' retorna uma cadeia decifrada por uma chave (opcional)
                Private Shared Function Decifra(ByVal Cadeia As String, Optional ByVal Chave As String = "") As String
                    Decifra = Criptografia(Cadeia, Chave, -1)
                End Function
                '------------------------------------------------------------------------------------------
                '
                ' Respons�vel : Jo�o Mac-Cormick
                ' Dt Desenv : 03/06/2000
                '
                ' retorna a criptografia de uma cadeia
                Private Shared Function Criptografia(ByVal Cadeia As String, Optional ByVal Chave As String = "", _
                              Optional ByVal Passo As Integer = 1) As String
                    If Passo = 0 Then
                        Passo = 1
                    End If
                    Dim Inicio As Integer
                    Dim Fim As Integer
                    Inicio = CType(Se(Passo > 0, 1, Len(Cadeia)), Integer)
                    Fim = CType(Se(Passo > 0, Len(Cadeia), 1), Integer)

                    Dim Indice As Integer
                    If Chave = "" Then
                        For Indice = Inicio To Fim Step Passo
                            Mid(Cadeia, Indice) = Chr(FazXOR(Cadeia, Indice))
                        Next Indice
                    Else
                        Dim posicao As Integer
                        For Indice = Inicio To Fim Step Passo
                            posicao = ((Indice - 1) Mod Len(Chave)) + 1
                            Mid(Cadeia, Indice) = Chr(FazXOR(Cadeia, Indice) Xor Asc(Mid(Chave, posicao, 1)))
                        Next Indice
                    End If
                    Criptografia = Cadeia
                End Function
                Private Shared Function FazXOR(ByVal Cadeia As String, ByVal Indice As Integer) As Integer
                    If Indice > 1 Then
                        FazXOR = Asc(Mid(Cadeia, Indice, 1)) Xor Asc(Mid(Cadeia, Indice - 1, 1))
                    Else
                        FazXOR = Asc(Mid(Cadeia, Indice, 1)) Xor Len(Cadeia)
                    End If
                End Function
                Public Shared Function Se(ByVal CondicaoVerdadeira As Boolean, _
                ByVal ParametroVerdadeiro As Object, _
                ByVal ParametroFalso As Object) As Object
                    Se = IIf(CondicaoVerdadeira, ParametroVerdadeiro, ParametroFalso)
                End Function
                '------------------------------------------------------------------------------------------
                '
                ' Respons�vel : Jo�o Mac-Cormick
                ' Dt Desenv : 03/06/2000
                '
                ' retorna uma cadeia cifrada por uma chave (opcional)
                Public Function Cifra(ByVal Cadeia As String, Optional ByVal Chave As String = "") As String
                    Cifra = Criptografia(Cadeia, Chave)
                End Function

                Function FormataData(ByVal DataBase As Date, ByVal TipoSQL As String, ByVal TipoDado As String) As String
                    Dim dado As String
                    '
                    ' Respons�vel : Carlos Rosa
                    ' Dt Desenv : 09/04/98
                    '
                    ' Recebe uma data, tipo de opera��o a ser efetuado no bco
                    ' de dados e o tipo de dados do registro como par�metros
                    ' Retorna :    '
                    ' Data formatada para Inclus�o, Altera��o, Dele��o ou Sele��o
                    ' "" para Erro de par�metro
                    '
                    ' TipoSQL   TipoDado
                    '---------  ---------------
                    ' "D"elete  "D"atetime
                    ' "I"nsert  "S"mallDateTime
                    ' "S"elect  "S"mallDateTime
                    ' "U"pdate
                    '
                    Dim ano As String = DataBase.Year.ToString
                    Dim mes As String = New String(CType("0", Char), 2 - DataBase.Month.ToString.Length) & DataBase.Month.ToString
                    Dim dia As String = New String(CType("0", Char), 2 - DataBase.Day.ToString.Length) & DataBase.Day.ToString
                    Dim hora As String = New String(CType("0", Char), 2 - DataBase.Hour.ToString.Length) & DataBase.Day.ToString
                    Dim minuto As String = New String(CType("0", Char), 2 - DataBase.Minute.ToString.Length) & DataBase.Day.ToString
                    Dim segundo As String = New String(CType("0", Char), 2 - DataBase.Second.ToString.Length) & DataBase.Day.ToString

                    Dim data As String = ano & "-" & mes & "-" & dia & " " & hora & ":" & minuto & ":" & segundo

                    If TipoSQL <> "D" And TipoSQL <> "I" And TipoSQL <> "S" And TipoSQL <> "U" Then
                        MsgBox("Par�metro incorreto !", MsgBoxStyle.Critical)
                        FormataData = ""
                        Exit Function
                    End If
                    If TipoDado <> "D" And TipoDado <> "S" Then
                        MsgBox("Par�metro incorreto !", MsgBoxStyle.Critical)
                        FormataData = ""
                        Exit Function
                    End If

                    If UCase(TipoSQL) = "S" Then
                        If UCase(TipoDado) = "D" Then
                            dado = "datetime"
                        ElseIf UCase(TipoDado) = "S" Then
                            dado = "smalldatetime"
                        End If
                        FormataData = " Convert( " & dado & ", ' " & data & "') "
                    Else ' "D", "I" ou "U"
                        FormataData = " Convert( " & dado & ", ' " & data & "') "
                    End If

                End Function

                Public Function TabTempUsr(Optional ByVal sUserName = "", Optional ByVal sMac = "") As String
                    ' haver� supress�o do MAC se o nome do usu�rio for maior que 14
                    ' a supress�o � necess�ria pois o MSSQL 6.5 aceita somente 30 caracteres
                    Dim sNaoUsado As String
                    Dim sDominio As String
                    Dim oSABL0010 As Object
                    Dim sLoginRede As String

                    oSABL0010 = CreateObject("SABL0010.cls00009")
                    sDominio = oSABL0010.RetornaValorAmbiente(gsSIGLASISTEMA, "Sistema", "Dominio", gsCHAVESISTEMA, glAmbiente_id)
                    'Call PegaInfoSistema(sNaoUsado, sNaoUsado, sNaoUsado, sNaoUsado, sNaoUsado, sLoginRede, sDominio)

                    '  If sUserName = "" Then sUserName = cUserName
                    If sUserName = "" Then sUserName = sLoginRede
                    If sMac = "" Then sMac = gsMac
                    TabTempUsr = Mid("##$" & sUserName & "$" + sMac, 1, 30)
                End Function
            End Class
        End Namespace
        Public Class ControleAmbientes
            Public Enum Ambientes
                Produ��o = 2
                Qualidade = 3
                Desenvolvimento = 4
                Extrator = 5
            End Enum

        End Class

    End Namespace

End Namespace


