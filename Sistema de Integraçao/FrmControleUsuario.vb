﻿Imports SISINT001

Public Class FrmControleUsuario


    Private Sub FrmControleUsuario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        CarregaComboOperador(0)
    End Sub

    Private Sub CarregaComboOperador(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboOperador.DataSource = Nothing
            cboOperador.Items.Clear()
            cboOperador.DisplayMember = "Nome_Operador"
            cboOperador.ValueMember = "Cod_Operador"
            cboOperador.DataSource = rn_generico.BuscarOperador(0)

            If cboOperador.Items.Count > 0 And cod_operador = 0 Then
                cboOperador.SelectedIndex = 0
            ElseIf cboOperador.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperador.Items.Count - 1
                    For Each linha As CGenerico In cboOperador.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperador.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperador.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalvar.Click
        Dim rn_generico As RNGenerico = New RNGenerico
        Try

            If rn_generico.DeletaPermissoes(cboOperador.SelectedValue) = True Then

                If chkAvulso.Checked = True Then
                    rn_generico.SalvaPermissoes(cboOperador.SelectedValue, cboOperador.Text, 5, "Avulso")
                End If
                If chkAvulsoB.Checked = True Then
                    rn_generico.SalvaPermissoes(cboOperador.SelectedValue, cboOperador.Text, 7, "Avulso B")
                End If
                If chkAvulsoRetido.Checked = True Then
                    rn_generico.SalvaPermissoes(cboOperador.SelectedValue, cboOperador.Text, 14, "Avulso Retido")
                End If
                If chkDoacao.Checked = True Then
                    rn_generico.SalvaPermissoes(cboOperador.SelectedValue, cboOperador.Text, 1, "Doação")
                End If
                If chkEspecial.Checked = True Then
                    rn_generico.SalvaPermissoes(cboOperador.SelectedValue, cboOperador.Text, 4, "Especial")
                End If
                If chkInativo.Checked = True Then
                    rn_generico.SalvaPermissoes(cboOperador.SelectedValue, cboOperador.Text, 8, "Inativo")
                End If
                If chkLista.Checked = True Then
                    rn_generico.SalvaPermissoes(cboOperador.SelectedValue, cboOperador.Text, 2, "Lista")
                End If
                If chkListaB.Checked = True Then
                    rn_generico.SalvaPermissoes(cboOperador.SelectedValue, cboOperador.Text, 1, "Lista B")
                End If
                If chkListaNova.Checked = True Then
                    rn_generico.SalvaPermissoes(cboOperador.SelectedValue, cboOperador.Text, 10, "Lista Nova")
                End If
                If chkMensal.Checked = True Then
                    rn_generico.SalvaPermissoes(cboOperador.SelectedValue, cboOperador.Text, 3, "Mensal")
                End If
                If chkMensalPrimeira.Checked = True Then
                    rn_generico.SalvaPermissoes(cboOperador.SelectedValue, cboOperador.Text, 6, "Mensal Primeira")
                End If
                If chkMensalRetido.Checked = True Then
                    rn_generico.SalvaPermissoes(cboOperador.SelectedValue, cboOperador.Text, 13, "Mensal Retido")
                End If
                If ChkPedirMensal.Checked = True Then
                    rn_generico.SalvaPermissoes(cboOperador.SelectedValue, cboOperador.Text, 12, "Pedir Mensal")
                End If
                If chkRecuperacao.Checked = True Then
                    rn_generico.SalvaPermissoes(cboOperador.SelectedValue, cboOperador.Text, 9, "Recuperação")
                End If
                If chkRecuperaRetido.Checked = True Then
                    rn_generico.SalvaPermissoes(cboOperador.SelectedValue, cboOperador.Text, 15, "Recuperacao Retido")
                End If

                MsgBox("Dados salvos com sucesso!")
                Me.Visible = False

            Else
                MsgBox("Ocorreu um erro ao salvar os dados")
            End If
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Close()
    End Sub


    Private Sub btnAlterar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnFinanceiro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFinanceiro.Click
        Dim frm As New FrmFinanceiro

        Try
            frm.Show()
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub btnRegras_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegras.Click
        Dim frm As New FrmRegrasRequisicao

        Try
            frm.Show()
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub btnGerarRequisicao_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGerarRequisicao.Click
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim mesSeguinte As String
        Dim NextMonth As Date

        Try

            NextMonth = DateAdd(DateInterval.Month, 1, CDate(DtMesAnoPesquisa.Text))
            mesSeguinte = NextMonth.Month & "/" & NextMonth.Year




            Dim result = MessageBox.Show("Deseja realizar o processamento da requisição?", "Atenção", _
                                MessageBoxButtons.YesNo, _
                                MessageBoxIcon.Question)

            If (result = DialogResult.Yes) Then
                If rn_generico.ValidaRemessa(DtMesAnoPesquisa.Text) = True Then
                    MsgBox("O processamento para o mês " & DtMesAnoPesquisa.Text & " já foi realizado, deseja reprocessar", MsgBoxStyle.YesNo, "Atenção")

                    If Windows.Forms.DialogResult.Yes Then
                        If (rn_generico.GerarRequisicao(DtMesAnoPesquisa.Text, mesSeguinte, "AACI")) = True Then

                            MsgBox("Operação realizada com sucesso!!", MsgBoxStyle.OkOnly, "Atenção")
                        Else
                            MsgBox("Ocorreu um erro ao gerar a remessa, verifique com o responsável", MsgBoxStyle.OkOnly, "Atenção")
                        End If
                    End If

                Else

                    If Windows.Forms.DialogResult.Yes Then
                        If (rn_generico.GerarRequisicao(DtMesAnoPesquisa.Text, mesSeguinte, "AACI")) = True Then

                            MsgBox("Operação realizada com sucesso!!", MsgBoxStyle.OkOnly, "Atenção")
                        Else
                            MsgBox("Ocorreu um erro ao gerar a remessa, verifique com o responsável", MsgBoxStyle.OkOnly, "Atenção")
                        End If
                    End If

                End If

            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub cboOperador_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboOperador.SelectedIndexChanged
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim ds As DataSet
        Try

            If cboOperador.SelectedValue > -1 Then


                ds = rn_generico.CarregaPermissoes(cboOperador.SelectedValue)

                If Not IsNothing(ds) Then


                    For Each dr As DataRow In ds.Tables(0).Rows

                        If dr.Item(0) = "Avulso" Then
                            chkAvulso.Checked = True
                        End If
                        If dr.Item(0) = "Avulso B" Then
                            chkAvulsoB.Checked = True
                        End If
                        If dr.Item(0) = "Avulso Retido" Then
                            chkAvulsoRetido.Checked = True
                        End If
                        If dr.Item(0) = "Doacao" Then
                            chkDoacao.Checked = True
                        End If
                        If dr.Item(0) = "Especial" Then
                            chkEspecial.Checked = True
                        End If
                        If dr.Item(0) = "Inativo" Then
                            chkInativo.Checked = True
                        End If
                        If dr.Item(0) = "Lista" Then
                            chkLista.Checked = True
                        End If
                        If dr.Item(0) = "Lista B" Then
                            chkListaB.Checked = True
                        End If
                        If dr.Item(0) = "Lista Nova" Then
                            chkListaNova.Checked = True
                        End If
                        If dr.Item(0) = "Mensal" Then
                            chkMensal.Checked = True
                        End If
                        If dr.Item(0) = "Mensal Primeira" Then
                            chkMensalPrimeira.Checked = True
                        End If
                        If dr.Item(0) = "Mensal Retido" Then
                            chkMensalRetido.Checked = True
                        End If
                        If dr.Item(0) = "Pedir Mensal" Then
                            ChkPedirMensal.Checked = True
                        End If
                        If dr.Item(0) = "Recuperacao" Then
                            chkRecuperacao.Checked = True
                        End If
                        If dr.Item(0) = "Recupera Retido" Then
                            chkRecuperaRetido.Checked = True
                        End If

                    Next

                End If
            End If
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub
End Class