﻿Public Class FrmPrint

    Public local As String
    Public isExcluido As Boolean

    Private Sub FrmPrint_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            If isExcluido = False Then
                PictureBox1.Width = frmTelefoneExclusao.Width
                PictureBox1.Height = frmTelefoneExclusao.Height
            Else
                PictureBox1.Width = FrmObservacaoExclusao.Width
                PictureBox1.Height = FrmObservacaoExclusao.Height
            End If

            Dim img = New Bitmap(PictureBox1.Image)

            img.Save(local, System.Drawing.Imaging.ImageFormat.Png)

        Catch ex As Exception

        End Try
    End Sub
End Class