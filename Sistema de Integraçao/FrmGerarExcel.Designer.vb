﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGerarExcel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.txtCaminho = New System.Windows.Forms.TextBox()
        Me.btnExcel = New System.Windows.Forms.Button()
        Me.cboOperador = New System.Windows.Forms.ComboBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.btnSair = New System.Windows.Forms.Button()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.rbOi = New System.Windows.Forms.RadioButton()
        Me.rbManual = New System.Windows.Forms.RadioButton()
        Me.cboCategoria = New System.Windows.Forms.ComboBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.grdRemessa = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.rbDetalhado = New System.Windows.Forms.RadioButton()
        Me.rbRapido = New System.Windows.Forms.RadioButton()
        CType(Me.grdRemessa, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(110, 101)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(263, 20)
        Me.txtEmail.TabIndex = 92
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.Location = New System.Drawing.Point(59, 102)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(49, 16)
        Me.Label45.TabIndex = 91
        Me.Label45.Text = "E-Mail:"
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(520, 134)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(29, 23)
        Me.btnBuscar.TabIndex = 90
        Me.btnBuscar.Text = "..."
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(-40, 178)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(46, 13)
        Me.Label44.TabIndex = 89
        Me.Label44.Text = "Arquivo:"
        '
        'txtCaminho
        '
        Me.txtCaminho.Location = New System.Drawing.Point(110, 136)
        Me.txtCaminho.Name = "txtCaminho"
        Me.txtCaminho.Size = New System.Drawing.Size(398, 20)
        Me.txtCaminho.TabIndex = 88
        '
        'btnExcel
        '
        Me.btnExcel.Location = New System.Drawing.Point(323, 179)
        Me.btnExcel.Name = "btnExcel"
        Me.btnExcel.Size = New System.Drawing.Size(107, 33)
        Me.btnExcel.TabIndex = 87
        Me.btnExcel.Text = "Gerar Excel"
        Me.btnExcel.UseVisualStyleBackColor = True
        '
        'cboOperador
        '
        Me.cboOperador.FormattingEnabled = True
        Me.cboOperador.Location = New System.Drawing.Point(110, 26)
        Me.cboOperador.Name = "cboOperador"
        Me.cboOperador.Size = New System.Drawing.Size(183, 21)
        Me.cboOperador.TabIndex = 93
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.Location = New System.Drawing.Point(29, 27)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(77, 16)
        Me.Label40.TabIndex = 94
        Me.Label40.Text = "Operadora:"
        '
        'btnSair
        '
        Me.btnSair.Location = New System.Drawing.Point(442, 179)
        Me.btnSair.Name = "btnSair"
        Me.btnSair.Size = New System.Drawing.Size(107, 33)
        Me.btnSair.TabIndex = 95
        Me.btnSair.Text = "Sair"
        Me.btnSair.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(45, 137)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 16)
        Me.Label1.TabIndex = 96
        Me.Label1.Text = "Caminho:"
        '
        'rbOi
        '
        Me.rbOi.AutoSize = True
        Me.rbOi.Checked = True
        Me.rbOi.Location = New System.Drawing.Point(18, 23)
        Me.rbOi.Name = "rbOi"
        Me.rbOi.Size = New System.Drawing.Size(35, 17)
        Me.rbOi.TabIndex = 97
        Me.rbOi.TabStop = True
        Me.rbOi.Text = "Oi"
        Me.rbOi.UseVisualStyleBackColor = True
        '
        'rbManual
        '
        Me.rbManual.AutoSize = True
        Me.rbManual.Location = New System.Drawing.Point(18, 46)
        Me.rbManual.Name = "rbManual"
        Me.rbManual.Size = New System.Drawing.Size(60, 17)
        Me.rbManual.TabIndex = 98
        Me.rbManual.TabStop = True
        Me.rbManual.Text = "Manual"
        Me.rbManual.UseVisualStyleBackColor = True
        '
        'cboCategoria
        '
        Me.cboCategoria.FormattingEnabled = True
        Me.cboCategoria.Location = New System.Drawing.Point(110, 62)
        Me.cboCategoria.Name = "cboCategoria"
        Me.cboCategoria.Size = New System.Drawing.Size(183, 21)
        Me.cboCategoria.TabIndex = 99
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(34, 64)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(73, 16)
        Me.Label17.TabIndex = 100
        Me.Label17.Text = "Categoria :"
        '
        'grdRemessa
        '
        Me.grdRemessa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdRemessa.Location = New System.Drawing.Point(12, 178)
        Me.grdRemessa.Name = "grdRemessa"
        Me.grdRemessa.Size = New System.Drawing.Size(305, 34)
        Me.grdRemessa.TabIndex = 101
        Me.grdRemessa.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rbManual)
        Me.GroupBox1.Controls.Add(Me.rbOi)
        Me.GroupBox1.Location = New System.Drawing.Point(311, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(96, 82)
        Me.GroupBox1.TabIndex = 102
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Sistema"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rbDetalhado)
        Me.GroupBox2.Controls.Add(Me.rbRapido)
        Me.GroupBox2.Location = New System.Drawing.Point(424, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(137, 82)
        Me.GroupBox2.TabIndex = 103
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Gerar Planilha em modo:"
        '
        'rbDetalhado
        '
        Me.rbDetalhado.AutoSize = True
        Me.rbDetalhado.Location = New System.Drawing.Point(18, 46)
        Me.rbDetalhado.Name = "rbDetalhado"
        Me.rbDetalhado.Size = New System.Drawing.Size(113, 17)
        Me.rbDetalhado.TabIndex = 98
        Me.rbDetalhado.TabStop = True
        Me.rbDetalhado.Text = "Lento e Detalhado"
        Me.rbDetalhado.UseVisualStyleBackColor = True
        '
        'rbRapido
        '
        Me.rbRapido.AutoSize = True
        Me.rbRapido.Checked = True
        Me.rbRapido.Location = New System.Drawing.Point(18, 23)
        Me.rbRapido.Name = "rbRapido"
        Me.rbRapido.Size = New System.Drawing.Size(59, 17)
        Me.rbRapido.TabIndex = 97
        Me.rbRapido.TabStop = True
        Me.rbRapido.Text = "Rápido"
        Me.rbRapido.UseVisualStyleBackColor = True
        '
        'FrmGerarExcel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(567, 222)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.grdRemessa)
        Me.Controls.Add(Me.cboCategoria)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnSair)
        Me.Controls.Add(Me.cboOperador)
        Me.Controls.Add(Me.Label40)
        Me.Controls.Add(Me.txtEmail)
        Me.Controls.Add(Me.Label45)
        Me.Controls.Add(Me.btnBuscar)
        Me.Controls.Add(Me.Label44)
        Me.Controls.Add(Me.txtCaminho)
        Me.Controls.Add(Me.btnExcel)
        Me.Name = "FrmGerarExcel"
        Me.Text = "Gerar Excel"
        CType(Me.grdRemessa, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents txtCaminho As System.Windows.Forms.TextBox
    Friend WithEvents btnExcel As System.Windows.Forms.Button
    Friend WithEvents cboOperador As System.Windows.Forms.ComboBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents btnSair As System.Windows.Forms.Button
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents rbOi As System.Windows.Forms.RadioButton
    Friend WithEvents rbManual As System.Windows.Forms.RadioButton
    Friend WithEvents cboCategoria As System.Windows.Forms.ComboBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents grdRemessa As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents rbDetalhado As System.Windows.Forms.RadioButton
    Friend WithEvents rbRapido As System.Windows.Forms.RadioButton
End Class
