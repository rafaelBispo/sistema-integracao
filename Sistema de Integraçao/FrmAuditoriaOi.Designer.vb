﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAuditoriaOi
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnReload = New System.Windows.Forms.Button()
        Me.chkPlanilhaInterna = New System.Windows.Forms.CheckBox()
        Me.chkIncluirCondicoes = New System.Windows.Forms.CheckBox()
        Me.TabCondicao = New System.Windows.Forms.TabControl()
        Me.Tab1 = New System.Windows.Forms.TabPage()
        Me.Filtros = New System.Windows.Forms.GroupBox()
        Me.ListViewTab1 = New System.Windows.Forms.ListView()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnExcluirOperadora = New System.Windows.Forms.Button()
        Me.chkTab1Condicao1 = New System.Windows.Forms.CheckBox()
        Me.lblHr1 = New System.Windows.Forms.Label()
        Me.txtTab1Hora1 = New System.Windows.Forms.TextBox()
        Me.dtTab1Condicao1 = New System.Windows.Forms.DateTimePicker()
        Me.lblDt1 = New System.Windows.Forms.Label()
        Me.cboOperadorTab1Cond1 = New System.Windows.Forms.ComboBox()
        Me.lblOpe1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtTelefone = New System.Windows.Forms.TextBox()
        Me.txtDDD = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnExcluirTelefone = New System.Windows.Forms.Button()
        Me.lblOpe2 = New System.Windows.Forms.Label()
        Me.dtFim = New System.Windows.Forms.DateTimePicker()
        Me.btnSair = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtInicio = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnPesquisar = New System.Windows.Forms.Button()
        Me.grdRemessa = New System.Windows.Forms.DataGridView()
        Me.lblNum = New System.Windows.Forms.Label()
        Me.lblNumRegistros = New System.Windows.Forms.Label()
        Me.TabControl = New System.Windows.Forms.TabControl()
        Me.TabParametros = New System.Windows.Forms.TabPage()
        Me.tabResultado = New System.Windows.Forms.TabPage()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.txtCaminho = New System.Windows.Forms.TextBox()
        Me.btnExcel = New System.Windows.Forms.Button()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.chkEnviaEmailCSV = New System.Windows.Forms.CheckBox()
        Me.btnBuscarCVS = New System.Windows.Forms.Button()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.txtCaminhoCVS = New System.Windows.Forms.TextBox()
        Me.btnImportar = New System.Windows.Forms.Button()
        Me.lblNumImportacao = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.TabCondicao.SuspendLayout()
        Me.Tab1.SuspendLayout()
        Me.Filtros.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.grdRemessa, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl.SuspendLayout()
        Me.TabParametros.SuspendLayout()
        Me.tabResultado.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.btnReload)
        Me.Panel1.Controls.Add(Me.chkPlanilhaInterna)
        Me.Panel1.Controls.Add(Me.chkIncluirCondicoes)
        Me.Panel1.Controls.Add(Me.TabCondicao)
        Me.Panel1.Controls.Add(Me.dtFim)
        Me.Panel1.Controls.Add(Me.btnSair)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.dtInicio)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.btnPesquisar)
        Me.Panel1.Location = New System.Drawing.Point(10, 25)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1263, 595)
        Me.Panel1.TabIndex = 1
        '
        'btnReload
        '
        Me.btnReload.Location = New System.Drawing.Point(457, 28)
        Me.btnReload.Name = "btnReload"
        Me.btnReload.Size = New System.Drawing.Size(97, 35)
        Me.btnReload.TabIndex = 82
        Me.btnReload.Text = "Recarregar Operadoras"
        Me.btnReload.UseVisualStyleBackColor = True
        '
        'chkPlanilhaInterna
        '
        Me.chkPlanilhaInterna.AutoSize = True
        Me.chkPlanilhaInterna.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkPlanilhaInterna.Location = New System.Drawing.Point(1029, 16)
        Me.chkPlanilhaInterna.Name = "chkPlanilhaInterna"
        Me.chkPlanilhaInterna.Size = New System.Drawing.Size(117, 20)
        Me.chkPlanilhaInterna.TabIndex = 81
        Me.chkPlanilhaInterna.Text = "Planilha Interna"
        Me.chkPlanilhaInterna.UseVisualStyleBackColor = True
        '
        'chkIncluirCondicoes
        '
        Me.chkIncluirCondicoes.AutoSize = True
        Me.chkIncluirCondicoes.Location = New System.Drawing.Point(17, 59)
        Me.chkIncluirCondicoes.Name = "chkIncluirCondicoes"
        Me.chkIncluirCondicoes.Size = New System.Drawing.Size(107, 17)
        Me.chkIncluirCondicoes.TabIndex = 80
        Me.chkIncluirCondicoes.Text = "Incluir Condições"
        Me.chkIncluirCondicoes.UseVisualStyleBackColor = True
        '
        'TabCondicao
        '
        Me.TabCondicao.Controls.Add(Me.Tab1)
        Me.TabCondicao.Location = New System.Drawing.Point(17, 83)
        Me.TabCondicao.Name = "TabCondicao"
        Me.TabCondicao.SelectedIndex = 0
        Me.TabCondicao.Size = New System.Drawing.Size(1235, 503)
        Me.TabCondicao.TabIndex = 79
        '
        'Tab1
        '
        Me.Tab1.BackColor = System.Drawing.Color.Silver
        Me.Tab1.Controls.Add(Me.Filtros)
        Me.Tab1.Controls.Add(Me.Label2)
        Me.Tab1.Controls.Add(Me.GroupBox1)
        Me.Tab1.Controls.Add(Me.GroupBox2)
        Me.Tab1.Location = New System.Drawing.Point(4, 22)
        Me.Tab1.Name = "Tab1"
        Me.Tab1.Padding = New System.Windows.Forms.Padding(3)
        Me.Tab1.Size = New System.Drawing.Size(1227, 477)
        Me.Tab1.TabIndex = 0
        Me.Tab1.Text = "Condição 1"
        '
        'Filtros
        '
        Me.Filtros.Controls.Add(Me.ListViewTab1)
        Me.Filtros.Location = New System.Drawing.Point(667, 6)
        Me.Filtros.Name = "Filtros"
        Me.Filtros.Size = New System.Drawing.Size(552, 457)
        Me.Filtros.TabIndex = 76
        Me.Filtros.TabStop = False
        Me.Filtros.Text = "Operadoras"
        '
        'ListViewTab1
        '
        Me.ListViewTab1.HideSelection = False
        Me.ListViewTab1.Location = New System.Drawing.Point(6, 19)
        Me.ListViewTab1.Name = "ListViewTab1"
        Me.ListViewTab1.Size = New System.Drawing.Size(529, 427)
        Me.ListViewTab1.TabIndex = 77
        Me.ListViewTab1.UseCompatibleStateImageBehavior = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Enabled = False
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Location = New System.Drawing.Point(18, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(515, 20)
        Me.Label2.TabIndex = 71
        Me.Label2.Text = "** As condições marcadas podem ser incluidas quando há uma particularidade do dia" &
    "."
        Me.Label2.UseCompatibleTextRendering = True
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.Silver
        Me.GroupBox1.Controls.Add(Me.btnExcluirOperadora)
        Me.GroupBox1.Controls.Add(Me.chkTab1Condicao1)
        Me.GroupBox1.Controls.Add(Me.lblHr1)
        Me.GroupBox1.Controls.Add(Me.txtTab1Hora1)
        Me.GroupBox1.Controls.Add(Me.dtTab1Condicao1)
        Me.GroupBox1.Controls.Add(Me.lblDt1)
        Me.GroupBox1.Controls.Add(Me.cboOperadorTab1Cond1)
        Me.GroupBox1.Controls.Add(Me.lblOpe1)
        Me.GroupBox1.Location = New System.Drawing.Point(18, 47)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(515, 131)
        Me.GroupBox1.TabIndex = 75
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Condição 1 "
        '
        'btnExcluirOperadora
        '
        Me.btnExcluirOperadora.Location = New System.Drawing.Point(319, 19)
        Me.btnExcluirOperadora.Name = "btnExcluirOperadora"
        Me.btnExcluirOperadora.Size = New System.Drawing.Size(97, 35)
        Me.btnExcluirOperadora.TabIndex = 80
        Me.btnExcluirOperadora.Text = "Excluir"
        Me.btnExcluirOperadora.UseVisualStyleBackColor = True
        '
        'chkTab1Condicao1
        '
        Me.chkTab1Condicao1.AutoSize = True
        Me.chkTab1Condicao1.Location = New System.Drawing.Point(189, 92)
        Me.chkTab1Condicao1.Name = "chkTab1Condicao1"
        Me.chkTab1Condicao1.Size = New System.Drawing.Size(111, 17)
        Me.chkTab1Condicao1.TabIndex = 78
        Me.chkTab1Condicao1.Text = "Incluir Condição 1"
        Me.chkTab1Condicao1.UseVisualStyleBackColor = True
        '
        'lblHr1
        '
        Me.lblHr1.AutoSize = True
        Me.lblHr1.Enabled = False
        Me.lblHr1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHr1.Location = New System.Drawing.Point(5, 91)
        Me.lblHr1.Name = "lblHr1"
        Me.lblHr1.Size = New System.Drawing.Size(74, 16)
        Me.lblHr1.TabIndex = 77
        Me.lblHr1.Text = "A partir de :"
        '
        'txtTab1Hora1
        '
        Me.txtTab1Hora1.Location = New System.Drawing.Point(81, 90)
        Me.txtTab1Hora1.Name = "txtTab1Hora1"
        Me.txtTab1Hora1.Size = New System.Drawing.Size(100, 20)
        Me.txtTab1Hora1.TabIndex = 76
        '
        'dtTab1Condicao1
        '
        Me.dtTab1Condicao1.CustomFormat = "dd/MM/yyyy"
        Me.dtTab1Condicao1.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtTab1Condicao1.Location = New System.Drawing.Point(81, 55)
        Me.dtTab1Condicao1.Name = "dtTab1Condicao1"
        Me.dtTab1Condicao1.Size = New System.Drawing.Size(200, 20)
        Me.dtTab1Condicao1.TabIndex = 74
        Me.dtTab1Condicao1.Value = New Date(2018, 9, 18, 0, 0, 0, 0)
        '
        'lblDt1
        '
        Me.lblDt1.AutoSize = True
        Me.lblDt1.Enabled = False
        Me.lblDt1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDt1.Location = New System.Drawing.Point(37, 55)
        Me.lblDt1.Name = "lblDt1"
        Me.lblDt1.Size = New System.Drawing.Size(39, 16)
        Me.lblDt1.TabIndex = 73
        Me.lblDt1.Text = "Data:"
        '
        'cboOperadorTab1Cond1
        '
        Me.cboOperadorTab1Cond1.FormattingEnabled = True
        Me.cboOperadorTab1Cond1.Location = New System.Drawing.Point(81, 19)
        Me.cboOperadorTab1Cond1.Name = "cboOperadorTab1Cond1"
        Me.cboOperadorTab1Cond1.Size = New System.Drawing.Size(189, 21)
        Me.cboOperadorTab1Cond1.TabIndex = 71
        '
        'lblOpe1
        '
        Me.lblOpe1.AutoSize = True
        Me.lblOpe1.Enabled = False
        Me.lblOpe1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOpe1.Location = New System.Drawing.Point(8, 21)
        Me.lblOpe1.Name = "lblOpe1"
        Me.lblOpe1.Size = New System.Drawing.Size(71, 16)
        Me.lblOpe1.TabIndex = 70
        Me.lblOpe1.Text = "Operador: "
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Silver
        Me.GroupBox2.Controls.Add(Me.txtTelefone)
        Me.GroupBox2.Controls.Add(Me.txtDDD)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.btnExcluirTelefone)
        Me.GroupBox2.Controls.Add(Me.lblOpe2)
        Me.GroupBox2.Location = New System.Drawing.Point(18, 184)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(515, 78)
        Me.GroupBox2.TabIndex = 76
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Condição 2 "
        '
        'txtTelefone
        '
        Me.txtTelefone.Location = New System.Drawing.Point(189, 25)
        Me.txtTelefone.Name = "txtTelefone"
        Me.txtTelefone.Size = New System.Drawing.Size(102, 20)
        Me.txtTelefone.TabIndex = 84
        '
        'txtDDD
        '
        Me.txtDDD.Location = New System.Drawing.Point(56, 24)
        Me.txtDDD.Name = "txtDDD"
        Me.txtDDD.Size = New System.Drawing.Size(48, 20)
        Me.txtDDD.TabIndex = 83
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Enabled = False
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(110, 26)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(78, 16)
        Me.Label4.TabIndex = 82
        Me.Label4.Text = "TELEFONE"
        '
        'btnExcluirTelefone
        '
        Me.btnExcluirTelefone.Location = New System.Drawing.Point(319, 19)
        Me.btnExcluirTelefone.Name = "btnExcluirTelefone"
        Me.btnExcluirTelefone.Size = New System.Drawing.Size(97, 35)
        Me.btnExcluirTelefone.TabIndex = 81
        Me.btnExcluirTelefone.Text = "Excluir"
        Me.btnExcluirTelefone.UseVisualStyleBackColor = True
        '
        'lblOpe2
        '
        Me.lblOpe2.AutoSize = True
        Me.lblOpe2.Enabled = False
        Me.lblOpe2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOpe2.Location = New System.Drawing.Point(12, 26)
        Me.lblOpe2.Name = "lblOpe2"
        Me.lblOpe2.Size = New System.Drawing.Size(37, 16)
        Me.lblOpe2.TabIndex = 70
        Me.lblOpe2.Text = "DDD"
        '
        'dtFim
        '
        Me.dtFim.CustomFormat = "dd/MM/yyyy"
        Me.dtFim.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtFim.Location = New System.Drawing.Point(226, 34)
        Me.dtFim.Name = "dtFim"
        Me.dtFim.Size = New System.Drawing.Size(200, 20)
        Me.dtFim.TabIndex = 74
        Me.dtFim.Value = New Date(2018, 9, 18, 0, 0, 0, 0)
        '
        'btnSair
        '
        Me.btnSair.Location = New System.Drawing.Point(1173, 59)
        Me.btnSair.Name = "btnSair"
        Me.btnSair.Size = New System.Drawing.Size(75, 23)
        Me.btnSair.TabIndex = 3
        Me.btnSair.Text = "Sair"
        Me.btnSair.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(223, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 16)
        Me.Label1.TabIndex = 73
        Me.Label1.Text = "Data Fim:"
        '
        'dtInicio
        '
        Me.dtInicio.CustomFormat = "dd/MM/yyyy"
        Me.dtInicio.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtInicio.Location = New System.Drawing.Point(17, 33)
        Me.dtInicio.Name = "dtInicio"
        Me.dtInicio.Size = New System.Drawing.Size(200, 20)
        Me.dtInicio.TabIndex = 72
        Me.dtInicio.Value = New Date(2018, 9, 18, 0, 0, 0, 0)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(14, 12)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(73, 16)
        Me.Label3.TabIndex = 71
        Me.Label3.Text = "Data Inicio:"
        '
        'btnPesquisar
        '
        Me.btnPesquisar.Location = New System.Drawing.Point(1151, 8)
        Me.btnPesquisar.Name = "btnPesquisar"
        Me.btnPesquisar.Size = New System.Drawing.Size(97, 35)
        Me.btnPesquisar.TabIndex = 67
        Me.btnPesquisar.Text = "Pesquisar"
        Me.btnPesquisar.UseVisualStyleBackColor = True
        '
        'grdRemessa
        '
        Me.grdRemessa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdRemessa.Location = New System.Drawing.Point(17, 16)
        Me.grdRemessa.Name = "grdRemessa"
        Me.grdRemessa.RowHeadersWidth = 51
        Me.grdRemessa.Size = New System.Drawing.Size(1140, 550)
        Me.grdRemessa.TabIndex = 2
        '
        'lblNum
        '
        Me.lblNum.AutoSize = True
        Me.lblNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNum.Location = New System.Drawing.Point(1167, 16)
        Me.lblNum.Name = "lblNum"
        Me.lblNum.Size = New System.Drawing.Size(101, 16)
        Me.lblNum.TabIndex = 78
        Me.lblNum.Text = "N° de Registros"
        '
        'lblNumRegistros
        '
        Me.lblNumRegistros.AutoSize = True
        Me.lblNumRegistros.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumRegistros.Location = New System.Drawing.Point(1167, 35)
        Me.lblNumRegistros.Name = "lblNumRegistros"
        Me.lblNumRegistros.Size = New System.Drawing.Size(14, 16)
        Me.lblNumRegistros.TabIndex = 79
        Me.lblNumRegistros.Text = "0"
        '
        'TabControl
        '
        Me.TabControl.Controls.Add(Me.TabParametros)
        Me.TabControl.Controls.Add(Me.tabResultado)
        Me.TabControl.Controls.Add(Me.TabPage1)
        Me.TabControl.Location = New System.Drawing.Point(12, 12)
        Me.TabControl.Name = "TabControl"
        Me.TabControl.SelectedIndex = 0
        Me.TabControl.Size = New System.Drawing.Size(1291, 659)
        Me.TabControl.TabIndex = 80
        '
        'TabParametros
        '
        Me.TabParametros.BackColor = System.Drawing.Color.LightGray
        Me.TabParametros.Controls.Add(Me.Panel1)
        Me.TabParametros.Location = New System.Drawing.Point(4, 22)
        Me.TabParametros.Name = "TabParametros"
        Me.TabParametros.Padding = New System.Windows.Forms.Padding(3)
        Me.TabParametros.Size = New System.Drawing.Size(1283, 633)
        Me.TabParametros.TabIndex = 0
        Me.TabParametros.Text = "Parâmetros"
        '
        'tabResultado
        '
        Me.tabResultado.Controls.Add(Me.txtEmail)
        Me.tabResultado.Controls.Add(Me.Label45)
        Me.tabResultado.Controls.Add(Me.btnBuscar)
        Me.tabResultado.Controls.Add(Me.Label44)
        Me.tabResultado.Controls.Add(Me.txtCaminho)
        Me.tabResultado.Controls.Add(Me.btnExcel)
        Me.tabResultado.Controls.Add(Me.grdRemessa)
        Me.tabResultado.Controls.Add(Me.lblNumRegistros)
        Me.tabResultado.Controls.Add(Me.lblNum)
        Me.tabResultado.Location = New System.Drawing.Point(4, 22)
        Me.tabResultado.Name = "tabResultado"
        Me.tabResultado.Padding = New System.Windows.Forms.Padding(3)
        Me.tabResultado.Size = New System.Drawing.Size(1283, 633)
        Me.tabResultado.TabIndex = 1
        Me.tabResultado.Text = "Resultado"
        Me.tabResultado.UseVisualStyleBackColor = True
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(824, 591)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(333, 20)
        Me.txtEmail.TabIndex = 86
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.Location = New System.Drawing.Point(766, 590)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(53, 20)
        Me.Label45.TabIndex = 85
        Me.Label45.Text = "E-Mail"
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(617, 587)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(29, 23)
        Me.btnBuscar.TabIndex = 84
        Me.btnBuscar.Text = "..."
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(20, 592)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(46, 13)
        Me.Label44.TabIndex = 83
        Me.Label44.Text = "Arquivo:"
        '
        'txtCaminho
        '
        Me.txtCaminho.Location = New System.Drawing.Point(72, 589)
        Me.txtCaminho.Name = "txtCaminho"
        Me.txtCaminho.Size = New System.Drawing.Size(539, 20)
        Me.txtCaminho.TabIndex = 82
        '
        'btnExcel
        '
        Me.btnExcel.Location = New System.Drawing.Point(652, 582)
        Me.btnExcel.Name = "btnExcel"
        Me.btnExcel.Size = New System.Drawing.Size(107, 33)
        Me.btnExcel.TabIndex = 80
        Me.btnExcel.Text = "Gerar Excel"
        Me.btnExcel.UseVisualStyleBackColor = True
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Controls.Add(Me.chkEnviaEmailCSV)
        Me.TabPage1.Controls.Add(Me.btnBuscarCVS)
        Me.TabPage1.Controls.Add(Me.Label49)
        Me.TabPage1.Controls.Add(Me.txtCaminhoCVS)
        Me.TabPage1.Controls.Add(Me.btnImportar)
        Me.TabPage1.Controls.Add(Me.lblNumImportacao)
        Me.TabPage1.Controls.Add(Me.Label47)
        Me.TabPage1.Controls.Add(Me.DataGridView1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Margin = New System.Windows.Forms.Padding(2)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(2)
        Me.TabPage1.Size = New System.Drawing.Size(1283, 633)
        Me.TabPage1.TabIndex = 2
        Me.TabPage1.Text = "Importar planilha Oi"
        '
        'chkEnviaEmailCSV
        '
        Me.chkEnviaEmailCSV.AutoSize = True
        Me.chkEnviaEmailCSV.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkEnviaEmailCSV.Location = New System.Drawing.Point(777, 21)
        Me.chkEnviaEmailCSV.Name = "chkEnviaEmailCSV"
        Me.chkEnviaEmailCSV.Size = New System.Drawing.Size(101, 20)
        Me.chkEnviaEmailCSV.TabIndex = 98
        Me.chkEnviaEmailCSV.Text = "Enviar Email"
        Me.chkEnviaEmailCSV.UseVisualStyleBackColor = True
        '
        'btnBuscarCVS
        '
        Me.btnBuscarCVS.Location = New System.Drawing.Point(619, 19)
        Me.btnBuscarCVS.Name = "btnBuscarCVS"
        Me.btnBuscarCVS.Size = New System.Drawing.Size(29, 23)
        Me.btnBuscarCVS.TabIndex = 97
        Me.btnBuscarCVS.Text = "..."
        Me.btnBuscarCVS.UseVisualStyleBackColor = True
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(22, 24)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(46, 13)
        Me.Label49.TabIndex = 96
        Me.Label49.Text = "Arquivo:"
        '
        'txtCaminhoCVS
        '
        Me.txtCaminhoCVS.Location = New System.Drawing.Point(74, 21)
        Me.txtCaminhoCVS.Name = "txtCaminhoCVS"
        Me.txtCaminhoCVS.Size = New System.Drawing.Size(539, 20)
        Me.txtCaminhoCVS.TabIndex = 95
        '
        'btnImportar
        '
        Me.btnImportar.Location = New System.Drawing.Point(653, 14)
        Me.btnImportar.Name = "btnImportar"
        Me.btnImportar.Size = New System.Drawing.Size(107, 33)
        Me.btnImportar.TabIndex = 94
        Me.btnImportar.Text = "Importar CVS"
        Me.btnImportar.UseVisualStyleBackColor = True
        '
        'lblNumImportacao
        '
        Me.lblNumImportacao.AutoSize = True
        Me.lblNumImportacao.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNumImportacao.Location = New System.Drawing.Point(1174, 98)
        Me.lblNumImportacao.Name = "lblNumImportacao"
        Me.lblNumImportacao.Size = New System.Drawing.Size(14, 16)
        Me.lblNumImportacao.TabIndex = 81
        Me.lblNumImportacao.Text = "0"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.Location = New System.Drawing.Point(1174, 80)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(101, 16)
        Me.Label47.TabIndex = 80
        Me.Label47.Text = "N° de Registros"
        '
        'DataGridView1
        '
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(16, 80)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowHeadersWidth = 51
        Me.DataGridView1.Size = New System.Drawing.Size(1140, 535)
        Me.DataGridView1.TabIndex = 3
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(1192, 683)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(107, 23)
        Me.Button1.TabIndex = 82
        Me.Button1.Text = "Sair"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'FrmAuditoriaOi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1309, 717)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.TabControl)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmAuditoriaOi"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Auditoria Oi"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TabCondicao.ResumeLayout(False)
        Me.Tab1.ResumeLayout(False)
        Me.Tab1.PerformLayout()
        Me.Filtros.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.grdRemessa, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl.ResumeLayout(False)
        Me.TabParametros.ResumeLayout(False)
        Me.tabResultado.ResumeLayout(False)
        Me.tabResultado.PerformLayout()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnPesquisar As System.Windows.Forms.Button
    Friend WithEvents grdRemessa As System.Windows.Forms.DataGridView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnSair As System.Windows.Forms.Button
    Friend WithEvents dtInicio As System.Windows.Forms.DateTimePicker
    Friend WithEvents Filtros As System.Windows.Forms.GroupBox
    Friend WithEvents dtFim As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ListViewTab1 As System.Windows.Forms.ListView
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblOpe2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblHr1 As System.Windows.Forms.Label
    Friend WithEvents txtTab1Hora1 As System.Windows.Forms.TextBox
    Friend WithEvents dtTab1Condicao1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDt1 As System.Windows.Forms.Label
    Friend WithEvents cboOperadorTab1Cond1 As System.Windows.Forms.ComboBox
    Friend WithEvents lblOpe1 As System.Windows.Forms.Label
    Friend WithEvents lblNum As System.Windows.Forms.Label
    Friend WithEvents lblNumRegistros As System.Windows.Forms.Label
    Friend WithEvents chkIncluirCondicoes As System.Windows.Forms.CheckBox
    Friend WithEvents TabCondicao As System.Windows.Forms.TabControl
    Friend WithEvents Tab1 As System.Windows.Forms.TabPage
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TabControl As System.Windows.Forms.TabControl
    Friend WithEvents TabParametros As System.Windows.Forms.TabPage
    Friend WithEvents tabResultado As System.Windows.Forms.TabPage
    Friend WithEvents btnExcel As System.Windows.Forms.Button
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents txtCaminho As System.Windows.Forms.TextBox
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents chkPlanilhaInterna As System.Windows.Forms.CheckBox
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents lblNumImportacao As Label
    Friend WithEvents Label47 As Label
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents Button1 As Button
    Friend WithEvents btnBuscarCVS As Button
    Friend WithEvents Label49 As Label
    Friend WithEvents txtCaminhoCVS As TextBox
    Friend WithEvents btnImportar As Button
    Friend WithEvents btnExcluirOperadora As Button
    Friend WithEvents txtTelefone As TextBox
    Friend WithEvents txtDDD As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents btnExcluirTelefone As Button
    Friend WithEvents chkTab1Condicao1 As CheckBox
    Friend WithEvents chkEnviaEmailCSV As CheckBox
    Friend WithEvents btnReload As Button
End Class
