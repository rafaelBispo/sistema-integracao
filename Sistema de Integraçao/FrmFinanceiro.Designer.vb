﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmFinanceiro
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.lblValorReq = New System.Windows.Forms.Label()
        Me.lblQuantidadeReq = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.gridRequisicao = New System.Windows.Forms.DataGridView()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.gridRequisicaoCategoria = New System.Windows.Forms.DataGridView()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtCoeficiente = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btnCalcular = New System.Windows.Forms.Button()
        Me.gpOperador4 = New System.Windows.Forms.GroupBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.lblValComissaoOp4 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.lblOp4FichaRecusada = New System.Windows.Forms.Label()
        Me.lblOperador4 = New System.Windows.Forms.Label()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.lblOp4FichaConf = New System.Windows.Forms.Label()
        Me.lblOp4ValorRecusada = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.lblOp4ValorConf = New System.Windows.Forms.Label()
        Me.gpOperador3 = New System.Windows.Forms.GroupBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.lblValComissaoOp3 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.lblOp3FichaRecusada = New System.Windows.Forms.Label()
        Me.lblOperador3 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.lblOp3FichaConf = New System.Windows.Forms.Label()
        Me.lblOp3ValorRecusada = New System.Windows.Forms.Label()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.lblOp3ValorConf = New System.Windows.Forms.Label()
        Me.gpOperador2 = New System.Windows.Forms.GroupBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.lblValComissaoOp2 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblOp2FichaRecusada = New System.Windows.Forms.Label()
        Me.lblOperador2 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.lblOp2FichaConf = New System.Windows.Forms.Label()
        Me.lblOp2ValorRecusada = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.lblOp2ValorConf = New System.Windows.Forms.Label()
        Me.gpOperador1 = New System.Windows.Forms.GroupBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.lblValComissaoOp1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.lblOp1FichaRecusada = New System.Windows.Forms.Label()
        Me.lblOperador1 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lblOp1FichaConf = New System.Windows.Forms.Label()
        Me.lblOp1ValorRecusada = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblOp1ValorConf = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblValorTotal = New System.Windows.Forms.Label()
        Me.lblCategoria = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblTotalFichas = New System.Windows.Forms.Label()
        Me.lblValorAberto = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lblFichaConfirmada = New System.Windows.Forms.Label()
        Me.lblFichaAberto = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.lblValorConfirmado = New System.Windows.Forms.Label()
        Me.lblValorRecusado = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lblFichaRecusada = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.txtFichasBloqueadas = New System.Windows.Forms.TextBox()
        Me.cboCidade = New System.Windows.Forms.ComboBox()
        Me.lblCidade = New System.Windows.Forms.Label()
        Me.chkZera = New System.Windows.Forms.CheckBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.txtFichasDisponiveis = New System.Windows.Forms.TextBox()
        Me.cboOperador = New System.Windows.Forms.ComboBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.btnSeparaFichas = New System.Windows.Forms.Button()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.txtQtdFichas = New System.Windows.Forms.TextBox()
        Me.btnPesquisar = New System.Windows.Forms.Button()
        Me.DtMesAnoPesquisa = New System.Windows.Forms.DateTimePicker()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.cboCategoria = New System.Windows.Forms.ComboBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.btnTrocarFichas = New System.Windows.Forms.Button()
        Me.GroupBox7 = New System.Windows.Forms.GroupBox()
        Me.cbParaOperadorTrocar = New System.Windows.Forms.ComboBox()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.txtQtdFichasTrocar = New System.Windows.Forms.TextBox()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.cbDeOperadorTrocar = New System.Windows.Forms.ComboBox()
        Me.cbCategoriaTrocar = New System.Windows.Forms.ComboBox()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.rdManual = New System.Windows.Forms.RadioButton()
        Me.rdOi = New System.Windows.Forms.RadioButton()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.txtqtdFichasResetar = New System.Windows.Forms.TextBox()
        Me.cboCidadeResetar = New System.Windows.Forms.ComboBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.cboOperadorResetar = New System.Windows.Forms.ComboBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.btnResetaPersonalizado = New System.Windows.Forms.Button()
        Me.cboCategoriaResetar = New System.Windows.Forms.ComboBox()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.gridRequisicao, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.gridRequisicaoCategoria, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.gpOperador4.SuspendLayout()
        Me.gpOperador3.SuspendLayout()
        Me.gpOperador2.SuspendLayout()
        Me.gpOperador1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TabControl2.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.TabControl1)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1160, 572)
        Me.Panel1.TabIndex = 0
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(489, 9)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(657, 560)
        Me.TabControl1.TabIndex = 2
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage1.Controls.Add(Me.lblValorReq)
        Me.TabPage1.Controls.Add(Me.lblQuantidadeReq)
        Me.TabPage1.Controls.Add(Me.Label26)
        Me.TabPage1.Controls.Add(Me.Label23)
        Me.TabPage1.Controls.Add(Me.gridRequisicao)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(649, 534)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Resumo Requisição"
        '
        'lblValorReq
        '
        Me.lblValorReq.AutoSize = True
        Me.lblValorReq.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorReq.Location = New System.Drawing.Point(332, 54)
        Me.lblValorReq.Name = "lblValorReq"
        Me.lblValorReq.Size = New System.Drawing.Size(72, 20)
        Me.lblValorReq.TabIndex = 4
        Me.lblValorReq.Text = "R$ 0,00"
        '
        'lblQuantidadeReq
        '
        Me.lblQuantidadeReq.AutoSize = True
        Me.lblQuantidadeReq.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQuantidadeReq.Location = New System.Drawing.Point(332, 20)
        Me.lblQuantidadeReq.Name = "lblQuantidadeReq"
        Me.lblQuantidadeReq.Size = New System.Drawing.Size(19, 20)
        Me.lblQuantidadeReq.TabIndex = 3
        Me.lblQuantidadeReq.Text = "0"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(17, 54)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(308, 20)
        Me.Label26.TabIndex = 2
        Me.Label26.Text = "Valor total das fichas na Requisição: "
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(17, 20)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(309, 20)
        Me.Label23.TabIndex = 1
        Me.Label23.Text = "Quantidade de fichas na Requisição: "
        '
        'gridRequisicao
        '
        Me.gridRequisicao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridRequisicao.Location = New System.Drawing.Point(6, 98)
        Me.gridRequisicao.Name = "gridRequisicao"
        Me.gridRequisicao.Size = New System.Drawing.Size(637, 418)
        Me.gridRequisicao.TabIndex = 0
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage2.Controls.Add(Me.Label30)
        Me.TabPage2.Controls.Add(Me.gridRequisicaoCategoria)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(649, 534)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Categoria"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(6, 11)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(225, 20)
        Me.Label30.TabIndex = 2
        Me.Label30.Text = "Dados da Requisição atual"
        '
        'gridRequisicaoCategoria
        '
        Me.gridRequisicaoCategoria.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridRequisicaoCategoria.Location = New System.Drawing.Point(6, 45)
        Me.gridRequisicaoCategoria.Name = "gridRequisicaoCategoria"
        Me.gridRequisicaoCategoria.Size = New System.Drawing.Size(637, 336)
        Me.gridRequisicaoCategoria.TabIndex = 1
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage3.Controls.Add(Me.GroupBox3)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(649, 534)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Operador"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtCoeficiente)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.btnCalcular)
        Me.GroupBox3.Controls.Add(Me.gpOperador4)
        Me.GroupBox3.Controls.Add(Me.gpOperador3)
        Me.GroupBox3.Controls.Add(Me.gpOperador2)
        Me.GroupBox3.Controls.Add(Me.gpOperador1)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 8)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(602, 403)
        Me.GroupBox3.TabIndex = 2
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Operador"
        '
        'txtCoeficiente
        '
        Me.txtCoeficiente.Location = New System.Drawing.Point(406, 376)
        Me.txtCoeficiente.Name = "txtCoeficiente"
        Me.txtCoeficiente.Size = New System.Drawing.Size(100, 20)
        Me.txtCoeficiente.TabIndex = 27
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(255, 379)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(151, 13)
        Me.Label9.TabIndex = 26
        Me.Label9.Text = "Percentual de Comissão :"
        '
        'btnCalcular
        '
        Me.btnCalcular.Location = New System.Drawing.Point(515, 374)
        Me.btnCalcular.Name = "btnCalcular"
        Me.btnCalcular.Size = New System.Drawing.Size(75, 23)
        Me.btnCalcular.TabIndex = 25
        Me.btnCalcular.Text = "Calcular"
        Me.btnCalcular.UseVisualStyleBackColor = True
        '
        'gpOperador4
        '
        Me.gpOperador4.Controls.Add(Me.Label33)
        Me.gpOperador4.Controls.Add(Me.lblValComissaoOp4)
        Me.gpOperador4.Controls.Add(Me.Label37)
        Me.gpOperador4.Controls.Add(Me.Label38)
        Me.gpOperador4.Controls.Add(Me.lblOp4FichaRecusada)
        Me.gpOperador4.Controls.Add(Me.lblOperador4)
        Me.gpOperador4.Controls.Add(Me.Label41)
        Me.gpOperador4.Controls.Add(Me.Label42)
        Me.gpOperador4.Controls.Add(Me.lblOp4FichaConf)
        Me.gpOperador4.Controls.Add(Me.lblOp4ValorRecusada)
        Me.gpOperador4.Controls.Add(Me.Label45)
        Me.gpOperador4.Controls.Add(Me.lblOp4ValorConf)
        Me.gpOperador4.Location = New System.Drawing.Point(301, 183)
        Me.gpOperador4.Name = "gpOperador4"
        Me.gpOperador4.Size = New System.Drawing.Size(289, 162)
        Me.gpOperador4.TabIndex = 23
        Me.gpOperador4.TabStop = False
        Me.gpOperador4.Visible = False
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.ForeColor = System.Drawing.Color.Blue
        Me.Label33.Location = New System.Drawing.Point(28, 131)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(124, 13)
        Me.Label33.TabIndex = 28
        Me.Label33.Text = "VALOR COMISSÃO :"
        '
        'lblValComissaoOp4
        '
        Me.lblValComissaoOp4.AutoSize = True
        Me.lblValComissaoOp4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValComissaoOp4.ForeColor = System.Drawing.Color.Blue
        Me.lblValComissaoOp4.Location = New System.Drawing.Point(162, 131)
        Me.lblValComissaoOp4.Name = "lblValComissaoOp4"
        Me.lblValComissaoOp4.Size = New System.Drawing.Size(14, 13)
        Me.lblValComissaoOp4.TabIndex = 29
        Me.lblValComissaoOp4.Text = "0"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.ForeColor = System.Drawing.Color.Green
        Me.Label37.Location = New System.Drawing.Point(4, 42)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(152, 13)
        Me.Label37.TabIndex = 12
        Me.Label37.Text = "FICHAS CONFIRMADAS :"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.Location = New System.Drawing.Point(70, 21)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(84, 13)
        Me.Label38.TabIndex = 20
        Me.Label38.Text = "OPERADOR :"
        '
        'lblOp4FichaRecusada
        '
        Me.lblOp4FichaRecusada.AutoSize = True
        Me.lblOp4FichaRecusada.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOp4FichaRecusada.ForeColor = System.Drawing.Color.Red
        Me.lblOp4FichaRecusada.Location = New System.Drawing.Point(162, 87)
        Me.lblOp4FichaRecusada.Name = "lblOp4FichaRecusada"
        Me.lblOp4FichaRecusada.Size = New System.Drawing.Size(14, 13)
        Me.lblOp4FichaRecusada.TabIndex = 17
        Me.lblOp4FichaRecusada.Text = "0"
        '
        'lblOperador4
        '
        Me.lblOperador4.AutoSize = True
        Me.lblOperador4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOperador4.Location = New System.Drawing.Point(162, 21)
        Me.lblOperador4.Name = "lblOperador4"
        Me.lblOperador4.Size = New System.Drawing.Size(43, 13)
        Me.lblOperador4.TabIndex = 21
        Me.lblOperador4.Text = "NOME"
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.ForeColor = System.Drawing.Color.Red
        Me.Label41.Location = New System.Drawing.Point(28, 109)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(128, 13)
        Me.Label41.TabIndex = 18
        Me.Label41.Text = "VALOR RECUSADO :"
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.ForeColor = System.Drawing.Color.Red
        Me.Label42.Location = New System.Drawing.Point(18, 87)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(138, 13)
        Me.Label42.TabIndex = 16
        Me.Label42.Text = "FICHAS RECUSADAS :"
        '
        'lblOp4FichaConf
        '
        Me.lblOp4FichaConf.AutoSize = True
        Me.lblOp4FichaConf.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOp4FichaConf.ForeColor = System.Drawing.Color.Green
        Me.lblOp4FichaConf.Location = New System.Drawing.Point(162, 42)
        Me.lblOp4FichaConf.Name = "lblOp4FichaConf"
        Me.lblOp4FichaConf.Size = New System.Drawing.Size(14, 13)
        Me.lblOp4FichaConf.TabIndex = 13
        Me.lblOp4FichaConf.Text = "0"
        '
        'lblOp4ValorRecusada
        '
        Me.lblOp4ValorRecusada.AutoSize = True
        Me.lblOp4ValorRecusada.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOp4ValorRecusada.ForeColor = System.Drawing.Color.Red
        Me.lblOp4ValorRecusada.Location = New System.Drawing.Point(162, 109)
        Me.lblOp4ValorRecusada.Name = "lblOp4ValorRecusada"
        Me.lblOp4ValorRecusada.Size = New System.Drawing.Size(14, 13)
        Me.lblOp4ValorRecusada.TabIndex = 19
        Me.lblOp4ValorRecusada.Text = "0"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.ForeColor = System.Drawing.Color.Green
        Me.Label45.Location = New System.Drawing.Point(14, 64)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(142, 13)
        Me.Label45.TabIndex = 14
        Me.Label45.Text = "VALOR CONFIRMADO :"
        '
        'lblOp4ValorConf
        '
        Me.lblOp4ValorConf.AutoSize = True
        Me.lblOp4ValorConf.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOp4ValorConf.ForeColor = System.Drawing.Color.Green
        Me.lblOp4ValorConf.Location = New System.Drawing.Point(162, 64)
        Me.lblOp4ValorConf.Name = "lblOp4ValorConf"
        Me.lblOp4ValorConf.Size = New System.Drawing.Size(14, 13)
        Me.lblOp4ValorConf.TabIndex = 15
        Me.lblOp4ValorConf.Text = "0"
        '
        'gpOperador3
        '
        Me.gpOperador3.Controls.Add(Me.Label29)
        Me.gpOperador3.Controls.Add(Me.lblValComissaoOp3)
        Me.gpOperador3.Controls.Add(Me.Label27)
        Me.gpOperador3.Controls.Add(Me.Label28)
        Me.gpOperador3.Controls.Add(Me.lblOp3FichaRecusada)
        Me.gpOperador3.Controls.Add(Me.lblOperador3)
        Me.gpOperador3.Controls.Add(Me.Label31)
        Me.gpOperador3.Controls.Add(Me.Label32)
        Me.gpOperador3.Controls.Add(Me.lblOp3FichaConf)
        Me.gpOperador3.Controls.Add(Me.lblOp3ValorRecusada)
        Me.gpOperador3.Controls.Add(Me.Label35)
        Me.gpOperador3.Controls.Add(Me.lblOp3ValorConf)
        Me.gpOperador3.Location = New System.Drawing.Point(6, 183)
        Me.gpOperador3.Name = "gpOperador3"
        Me.gpOperador3.Size = New System.Drawing.Size(289, 162)
        Me.gpOperador3.TabIndex = 24
        Me.gpOperador3.TabStop = False
        Me.gpOperador3.Visible = False
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.ForeColor = System.Drawing.Color.Blue
        Me.Label29.Location = New System.Drawing.Point(28, 131)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(124, 13)
        Me.Label29.TabIndex = 26
        Me.Label29.Text = "VALOR COMISSÃO :"
        '
        'lblValComissaoOp3
        '
        Me.lblValComissaoOp3.AutoSize = True
        Me.lblValComissaoOp3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValComissaoOp3.ForeColor = System.Drawing.Color.Blue
        Me.lblValComissaoOp3.Location = New System.Drawing.Point(162, 131)
        Me.lblValComissaoOp3.Name = "lblValComissaoOp3"
        Me.lblValComissaoOp3.Size = New System.Drawing.Size(14, 13)
        Me.lblValComissaoOp3.TabIndex = 27
        Me.lblValComissaoOp3.Text = "0"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.Green
        Me.Label27.Location = New System.Drawing.Point(4, 42)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(152, 13)
        Me.Label27.TabIndex = 12
        Me.Label27.Text = "FICHAS CONFIRMADAS :"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(70, 21)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(84, 13)
        Me.Label28.TabIndex = 20
        Me.Label28.Text = "OPERADOR :"
        '
        'lblOp3FichaRecusada
        '
        Me.lblOp3FichaRecusada.AutoSize = True
        Me.lblOp3FichaRecusada.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOp3FichaRecusada.ForeColor = System.Drawing.Color.Red
        Me.lblOp3FichaRecusada.Location = New System.Drawing.Point(162, 87)
        Me.lblOp3FichaRecusada.Name = "lblOp3FichaRecusada"
        Me.lblOp3FichaRecusada.Size = New System.Drawing.Size(14, 13)
        Me.lblOp3FichaRecusada.TabIndex = 17
        Me.lblOp3FichaRecusada.Text = "0"
        '
        'lblOperador3
        '
        Me.lblOperador3.AutoSize = True
        Me.lblOperador3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOperador3.Location = New System.Drawing.Point(162, 21)
        Me.lblOperador3.Name = "lblOperador3"
        Me.lblOperador3.Size = New System.Drawing.Size(43, 13)
        Me.lblOperador3.TabIndex = 21
        Me.lblOperador3.Text = "NOME"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.ForeColor = System.Drawing.Color.Red
        Me.Label31.Location = New System.Drawing.Point(28, 109)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(128, 13)
        Me.Label31.TabIndex = 18
        Me.Label31.Text = "VALOR RECUSADO :"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.ForeColor = System.Drawing.Color.Red
        Me.Label32.Location = New System.Drawing.Point(18, 87)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(138, 13)
        Me.Label32.TabIndex = 16
        Me.Label32.Text = "FICHAS RECUSADAS :"
        '
        'lblOp3FichaConf
        '
        Me.lblOp3FichaConf.AutoSize = True
        Me.lblOp3FichaConf.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOp3FichaConf.ForeColor = System.Drawing.Color.Green
        Me.lblOp3FichaConf.Location = New System.Drawing.Point(162, 42)
        Me.lblOp3FichaConf.Name = "lblOp3FichaConf"
        Me.lblOp3FichaConf.Size = New System.Drawing.Size(14, 13)
        Me.lblOp3FichaConf.TabIndex = 13
        Me.lblOp3FichaConf.Text = "0"
        '
        'lblOp3ValorRecusada
        '
        Me.lblOp3ValorRecusada.AutoSize = True
        Me.lblOp3ValorRecusada.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOp3ValorRecusada.ForeColor = System.Drawing.Color.Red
        Me.lblOp3ValorRecusada.Location = New System.Drawing.Point(162, 109)
        Me.lblOp3ValorRecusada.Name = "lblOp3ValorRecusada"
        Me.lblOp3ValorRecusada.Size = New System.Drawing.Size(14, 13)
        Me.lblOp3ValorRecusada.TabIndex = 19
        Me.lblOp3ValorRecusada.Text = "0"
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.Color.Green
        Me.Label35.Location = New System.Drawing.Point(14, 64)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(142, 13)
        Me.Label35.TabIndex = 14
        Me.Label35.Text = "VALOR CONFIRMADO :"
        '
        'lblOp3ValorConf
        '
        Me.lblOp3ValorConf.AutoSize = True
        Me.lblOp3ValorConf.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOp3ValorConf.ForeColor = System.Drawing.Color.Green
        Me.lblOp3ValorConf.Location = New System.Drawing.Point(162, 64)
        Me.lblOp3ValorConf.Name = "lblOp3ValorConf"
        Me.lblOp3ValorConf.Size = New System.Drawing.Size(14, 13)
        Me.lblOp3ValorConf.TabIndex = 15
        Me.lblOp3ValorConf.Text = "0"
        '
        'gpOperador2
        '
        Me.gpOperador2.Controls.Add(Me.Label24)
        Me.gpOperador2.Controls.Add(Me.lblValComissaoOp2)
        Me.gpOperador2.Controls.Add(Me.Label4)
        Me.gpOperador2.Controls.Add(Me.Label7)
        Me.gpOperador2.Controls.Add(Me.lblOp2FichaRecusada)
        Me.gpOperador2.Controls.Add(Me.lblOperador2)
        Me.gpOperador2.Controls.Add(Me.Label21)
        Me.gpOperador2.Controls.Add(Me.Label22)
        Me.gpOperador2.Controls.Add(Me.lblOp2FichaConf)
        Me.gpOperador2.Controls.Add(Me.lblOp2ValorRecusada)
        Me.gpOperador2.Controls.Add(Me.Label25)
        Me.gpOperador2.Controls.Add(Me.lblOp2ValorConf)
        Me.gpOperador2.Location = New System.Drawing.Point(301, 14)
        Me.gpOperador2.Name = "gpOperador2"
        Me.gpOperador2.Size = New System.Drawing.Size(289, 163)
        Me.gpOperador2.TabIndex = 23
        Me.gpOperador2.TabStop = False
        Me.gpOperador2.Visible = False
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.Blue
        Me.Label24.Location = New System.Drawing.Point(28, 132)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(124, 13)
        Me.Label24.TabIndex = 24
        Me.Label24.Text = "VALOR COMISSÃO :"
        '
        'lblValComissaoOp2
        '
        Me.lblValComissaoOp2.AutoSize = True
        Me.lblValComissaoOp2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValComissaoOp2.ForeColor = System.Drawing.Color.Blue
        Me.lblValComissaoOp2.Location = New System.Drawing.Point(162, 132)
        Me.lblValComissaoOp2.Name = "lblValComissaoOp2"
        Me.lblValComissaoOp2.Size = New System.Drawing.Size(14, 13)
        Me.lblValComissaoOp2.TabIndex = 25
        Me.lblValComissaoOp2.Text = "0"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Green
        Me.Label4.Location = New System.Drawing.Point(4, 42)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(152, 13)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "FICHAS CONFIRMADAS :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(70, 21)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(84, 13)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "OPERADOR :"
        '
        'lblOp2FichaRecusada
        '
        Me.lblOp2FichaRecusada.AutoSize = True
        Me.lblOp2FichaRecusada.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOp2FichaRecusada.ForeColor = System.Drawing.Color.Red
        Me.lblOp2FichaRecusada.Location = New System.Drawing.Point(162, 87)
        Me.lblOp2FichaRecusada.Name = "lblOp2FichaRecusada"
        Me.lblOp2FichaRecusada.Size = New System.Drawing.Size(14, 13)
        Me.lblOp2FichaRecusada.TabIndex = 17
        Me.lblOp2FichaRecusada.Text = "0"
        '
        'lblOperador2
        '
        Me.lblOperador2.AutoSize = True
        Me.lblOperador2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOperador2.Location = New System.Drawing.Point(162, 21)
        Me.lblOperador2.Name = "lblOperador2"
        Me.lblOperador2.Size = New System.Drawing.Size(43, 13)
        Me.lblOperador2.TabIndex = 21
        Me.lblOperador2.Text = "NOME"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.ForeColor = System.Drawing.Color.Red
        Me.Label21.Location = New System.Drawing.Point(28, 109)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(128, 13)
        Me.Label21.TabIndex = 18
        Me.Label21.Text = "VALOR RECUSADO :"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.Red
        Me.Label22.Location = New System.Drawing.Point(18, 87)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(138, 13)
        Me.Label22.TabIndex = 16
        Me.Label22.Text = "FICHAS RECUSADAS :"
        '
        'lblOp2FichaConf
        '
        Me.lblOp2FichaConf.AutoSize = True
        Me.lblOp2FichaConf.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOp2FichaConf.ForeColor = System.Drawing.Color.Green
        Me.lblOp2FichaConf.Location = New System.Drawing.Point(162, 42)
        Me.lblOp2FichaConf.Name = "lblOp2FichaConf"
        Me.lblOp2FichaConf.Size = New System.Drawing.Size(14, 13)
        Me.lblOp2FichaConf.TabIndex = 13
        Me.lblOp2FichaConf.Text = "0"
        '
        'lblOp2ValorRecusada
        '
        Me.lblOp2ValorRecusada.AutoSize = True
        Me.lblOp2ValorRecusada.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOp2ValorRecusada.ForeColor = System.Drawing.Color.Red
        Me.lblOp2ValorRecusada.Location = New System.Drawing.Point(162, 109)
        Me.lblOp2ValorRecusada.Name = "lblOp2ValorRecusada"
        Me.lblOp2ValorRecusada.Size = New System.Drawing.Size(14, 13)
        Me.lblOp2ValorRecusada.TabIndex = 19
        Me.lblOp2ValorRecusada.Text = "0"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.ForeColor = System.Drawing.Color.Green
        Me.Label25.Location = New System.Drawing.Point(14, 64)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(142, 13)
        Me.Label25.TabIndex = 14
        Me.Label25.Text = "VALOR CONFIRMADO :"
        '
        'lblOp2ValorConf
        '
        Me.lblOp2ValorConf.AutoSize = True
        Me.lblOp2ValorConf.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOp2ValorConf.ForeColor = System.Drawing.Color.Green
        Me.lblOp2ValorConf.Location = New System.Drawing.Point(162, 64)
        Me.lblOp2ValorConf.Name = "lblOp2ValorConf"
        Me.lblOp2ValorConf.Size = New System.Drawing.Size(14, 13)
        Me.lblOp2ValorConf.TabIndex = 15
        Me.lblOp2ValorConf.Text = "0"
        '
        'gpOperador1
        '
        Me.gpOperador1.Controls.Add(Me.Label15)
        Me.gpOperador1.Controls.Add(Me.lblValComissaoOp1)
        Me.gpOperador1.Controls.Add(Me.Label2)
        Me.gpOperador1.Controls.Add(Me.Label19)
        Me.gpOperador1.Controls.Add(Me.lblOp1FichaRecusada)
        Me.gpOperador1.Controls.Add(Me.lblOperador1)
        Me.gpOperador1.Controls.Add(Me.Label13)
        Me.gpOperador1.Controls.Add(Me.Label11)
        Me.gpOperador1.Controls.Add(Me.lblOp1FichaConf)
        Me.gpOperador1.Controls.Add(Me.lblOp1ValorRecusada)
        Me.gpOperador1.Controls.Add(Me.Label5)
        Me.gpOperador1.Controls.Add(Me.lblOp1ValorConf)
        Me.gpOperador1.Location = New System.Drawing.Point(6, 14)
        Me.gpOperador1.Name = "gpOperador1"
        Me.gpOperador1.Size = New System.Drawing.Size(289, 163)
        Me.gpOperador1.TabIndex = 22
        Me.gpOperador1.TabStop = False
        Me.gpOperador1.Visible = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Blue
        Me.Label15.Location = New System.Drawing.Point(28, 132)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(124, 13)
        Me.Label15.TabIndex = 22
        Me.Label15.Text = "VALOR COMISSÃO :"
        '
        'lblValComissaoOp1
        '
        Me.lblValComissaoOp1.AutoSize = True
        Me.lblValComissaoOp1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValComissaoOp1.ForeColor = System.Drawing.Color.Blue
        Me.lblValComissaoOp1.Location = New System.Drawing.Point(162, 132)
        Me.lblValComissaoOp1.Name = "lblValComissaoOp1"
        Me.lblValComissaoOp1.Size = New System.Drawing.Size(14, 13)
        Me.lblValComissaoOp1.TabIndex = 23
        Me.lblValComissaoOp1.Text = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Green
        Me.Label2.Location = New System.Drawing.Point(4, 42)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(152, 13)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "FICHAS CONFIRMADAS :"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(70, 21)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(84, 13)
        Me.Label19.TabIndex = 20
        Me.Label19.Text = "OPERADOR :"
        '
        'lblOp1FichaRecusada
        '
        Me.lblOp1FichaRecusada.AutoSize = True
        Me.lblOp1FichaRecusada.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOp1FichaRecusada.ForeColor = System.Drawing.Color.Red
        Me.lblOp1FichaRecusada.Location = New System.Drawing.Point(162, 87)
        Me.lblOp1FichaRecusada.Name = "lblOp1FichaRecusada"
        Me.lblOp1FichaRecusada.Size = New System.Drawing.Size(14, 13)
        Me.lblOp1FichaRecusada.TabIndex = 17
        Me.lblOp1FichaRecusada.Text = "0"
        '
        'lblOperador1
        '
        Me.lblOperador1.AutoSize = True
        Me.lblOperador1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOperador1.Location = New System.Drawing.Point(162, 21)
        Me.lblOperador1.Name = "lblOperador1"
        Me.lblOperador1.Size = New System.Drawing.Size(43, 13)
        Me.lblOperador1.TabIndex = 21
        Me.lblOperador1.Text = "NOME"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Red
        Me.Label13.Location = New System.Drawing.Point(28, 109)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(128, 13)
        Me.Label13.TabIndex = 18
        Me.Label13.Text = "VALOR RECUSADO :"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.ForeColor = System.Drawing.Color.Red
        Me.Label11.Location = New System.Drawing.Point(18, 87)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(138, 13)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "FICHAS RECUSADAS :"
        '
        'lblOp1FichaConf
        '
        Me.lblOp1FichaConf.AutoSize = True
        Me.lblOp1FichaConf.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOp1FichaConf.ForeColor = System.Drawing.Color.Green
        Me.lblOp1FichaConf.Location = New System.Drawing.Point(162, 42)
        Me.lblOp1FichaConf.Name = "lblOp1FichaConf"
        Me.lblOp1FichaConf.Size = New System.Drawing.Size(14, 13)
        Me.lblOp1FichaConf.TabIndex = 13
        Me.lblOp1FichaConf.Text = "0"
        '
        'lblOp1ValorRecusada
        '
        Me.lblOp1ValorRecusada.AutoSize = True
        Me.lblOp1ValorRecusada.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOp1ValorRecusada.ForeColor = System.Drawing.Color.Red
        Me.lblOp1ValorRecusada.Location = New System.Drawing.Point(162, 109)
        Me.lblOp1ValorRecusada.Name = "lblOp1ValorRecusada"
        Me.lblOp1ValorRecusada.Size = New System.Drawing.Size(14, 13)
        Me.lblOp1ValorRecusada.TabIndex = 19
        Me.lblOp1ValorRecusada.Text = "0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Green
        Me.Label5.Location = New System.Drawing.Point(14, 64)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(142, 13)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "VALOR CONFIRMADO :"
        '
        'lblOp1ValorConf
        '
        Me.lblOp1ValorConf.AutoSize = True
        Me.lblOp1ValorConf.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOp1ValorConf.ForeColor = System.Drawing.Color.Green
        Me.lblOp1ValorConf.Location = New System.Drawing.Point(162, 64)
        Me.lblOp1ValorConf.Name = "lblOp1ValorConf"
        Me.lblOp1ValorConf.Size = New System.Drawing.Size(14, 13)
        Me.lblOp1ValorConf.TabIndex = 15
        Me.lblOp1ValorConf.Text = "0"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TabControl2)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(480, 561)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Finanças"
        '
        'TabControl2
        '
        Me.TabControl2.Controls.Add(Me.TabPage4)
        Me.TabControl2.Controls.Add(Me.TabPage5)
        Me.TabControl2.Location = New System.Drawing.Point(6, 19)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(464, 536)
        Me.TabControl2.TabIndex = 3
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.Gainsboro
        Me.TabPage4.Controls.Add(Me.GroupBox4)
        Me.TabPage4.Controls.Add(Me.GroupBox2)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(456, 510)
        Me.TabPage4.TabIndex = 0
        Me.TabPage4.Text = "Requisição"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label1)
        Me.GroupBox4.Controls.Add(Me.lblValorTotal)
        Me.GroupBox4.Controls.Add(Me.lblCategoria)
        Me.GroupBox4.Controls.Add(Me.Label20)
        Me.GroupBox4.Controls.Add(Me.Label3)
        Me.GroupBox4.Controls.Add(Me.lblTotalFichas)
        Me.GroupBox4.Controls.Add(Me.lblValorAberto)
        Me.GroupBox4.Controls.Add(Me.Label8)
        Me.GroupBox4.Controls.Add(Me.Label14)
        Me.GroupBox4.Controls.Add(Me.lblFichaConfirmada)
        Me.GroupBox4.Controls.Add(Me.lblFichaAberto)
        Me.GroupBox4.Controls.Add(Me.Label6)
        Me.GroupBox4.Controls.Add(Me.Label16)
        Me.GroupBox4.Controls.Add(Me.lblValorConfirmado)
        Me.GroupBox4.Controls.Add(Me.lblValorRecusado)
        Me.GroupBox4.Controls.Add(Me.Label12)
        Me.GroupBox4.Controls.Add(Me.Label10)
        Me.GroupBox4.Controls.Add(Me.lblFichaRecusada)
        Me.GroupBox4.Location = New System.Drawing.Point(3, 279)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(401, 224)
        Me.GroupBox4.TabIndex = 22
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Dados"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(98, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(86, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "CATEGORIA :"
        '
        'lblValorTotal
        '
        Me.lblValorTotal.AutoSize = True
        Me.lblValorTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorTotal.ForeColor = System.Drawing.Color.Blue
        Me.lblValorTotal.Location = New System.Drawing.Point(190, 60)
        Me.lblValorTotal.Name = "lblValorTotal"
        Me.lblValorTotal.Size = New System.Drawing.Size(14, 13)
        Me.lblValorTotal.TabIndex = 21
        Me.lblValorTotal.Text = "0"
        '
        'lblCategoria
        '
        Me.lblCategoria.AutoSize = True
        Me.lblCategoria.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCategoria.Location = New System.Drawing.Point(190, 16)
        Me.lblCategoria.Name = "lblCategoria"
        Me.lblCategoria.Size = New System.Drawing.Size(43, 13)
        Me.lblCategoria.TabIndex = 1
        Me.lblCategoria.Text = "NOME"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Blue
        Me.Label20.Location = New System.Drawing.Point(11, 60)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(173, 13)
        Me.Label20.TabIndex = 20
        Me.Label20.Text = "VALOR TOTAL DE FICHAS  :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Blue
        Me.Label3.Location = New System.Drawing.Point(38, 38)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(146, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "N° TOTAL DE FICHAS  :"
        '
        'lblTotalFichas
        '
        Me.lblTotalFichas.AutoSize = True
        Me.lblTotalFichas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalFichas.ForeColor = System.Drawing.Color.Blue
        Me.lblTotalFichas.Location = New System.Drawing.Point(190, 38)
        Me.lblTotalFichas.Name = "lblTotalFichas"
        Me.lblTotalFichas.Size = New System.Drawing.Size(14, 13)
        Me.lblTotalFichas.TabIndex = 3
        Me.lblTotalFichas.Text = "0"
        '
        'lblValorAberto
        '
        Me.lblValorAberto.AutoSize = True
        Me.lblValorAberto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorAberto.Location = New System.Drawing.Point(190, 193)
        Me.lblValorAberto.Name = "lblValorAberto"
        Me.lblValorAberto.Size = New System.Drawing.Size(14, 13)
        Me.lblValorAberto.TabIndex = 15
        Me.lblValorAberto.Text = "0"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Green
        Me.Label8.Location = New System.Drawing.Point(32, 82)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(152, 13)
        Me.Label8.TabIndex = 4
        Me.Label8.Text = "FICHAS CONFIRMADAS :"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(52, 193)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(132, 13)
        Me.Label14.TabIndex = 14
        Me.Label14.Text = "VALOR EM ABERTO :"
        '
        'lblFichaConfirmada
        '
        Me.lblFichaConfirmada.AutoSize = True
        Me.lblFichaConfirmada.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFichaConfirmada.ForeColor = System.Drawing.Color.Green
        Me.lblFichaConfirmada.Location = New System.Drawing.Point(190, 82)
        Me.lblFichaConfirmada.Name = "lblFichaConfirmada"
        Me.lblFichaConfirmada.Size = New System.Drawing.Size(14, 13)
        Me.lblFichaConfirmada.TabIndex = 5
        Me.lblFichaConfirmada.Text = "0"
        '
        'lblFichaAberto
        '
        Me.lblFichaAberto.AutoSize = True
        Me.lblFichaAberto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFichaAberto.Location = New System.Drawing.Point(190, 171)
        Me.lblFichaAberto.Name = "lblFichaAberto"
        Me.lblFichaAberto.Size = New System.Drawing.Size(14, 13)
        Me.lblFichaAberto.TabIndex = 13
        Me.lblFichaAberto.Text = "0"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Green
        Me.Label6.Location = New System.Drawing.Point(42, 104)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(142, 13)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "VALOR CONFIRMADO :"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(49, 171)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(135, 13)
        Me.Label16.TabIndex = 12
        Me.Label16.Text = "FICHAS EM ABERTO :"
        '
        'lblValorConfirmado
        '
        Me.lblValorConfirmado.AutoSize = True
        Me.lblValorConfirmado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorConfirmado.ForeColor = System.Drawing.Color.Green
        Me.lblValorConfirmado.Location = New System.Drawing.Point(190, 104)
        Me.lblValorConfirmado.Name = "lblValorConfirmado"
        Me.lblValorConfirmado.Size = New System.Drawing.Size(14, 13)
        Me.lblValorConfirmado.TabIndex = 7
        Me.lblValorConfirmado.Text = "0"
        '
        'lblValorRecusado
        '
        Me.lblValorRecusado.AutoSize = True
        Me.lblValorRecusado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorRecusado.ForeColor = System.Drawing.Color.Red
        Me.lblValorRecusado.Location = New System.Drawing.Point(190, 149)
        Me.lblValorRecusado.Name = "lblValorRecusado"
        Me.lblValorRecusado.Size = New System.Drawing.Size(14, 13)
        Me.lblValorRecusado.TabIndex = 11
        Me.lblValorRecusado.Text = "0"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Red
        Me.Label12.Location = New System.Drawing.Point(46, 127)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(138, 13)
        Me.Label12.TabIndex = 8
        Me.Label12.Text = "FICHAS RECUSADAS :"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Red
        Me.Label10.Location = New System.Drawing.Point(56, 149)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(128, 13)
        Me.Label10.TabIndex = 10
        Me.Label10.Text = "VALOR RECUSADO :"
        '
        'lblFichaRecusada
        '
        Me.lblFichaRecusada.AutoSize = True
        Me.lblFichaRecusada.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFichaRecusada.ForeColor = System.Drawing.Color.Red
        Me.lblFichaRecusada.Location = New System.Drawing.Point(190, 127)
        Me.lblFichaRecusada.Name = "lblFichaRecusada"
        Me.lblFichaRecusada.Size = New System.Drawing.Size(14, 13)
        Me.lblFichaRecusada.TabIndex = 9
        Me.lblFichaRecusada.Text = "0"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label52)
        Me.GroupBox2.Controls.Add(Me.txtFichasBloqueadas)
        Me.GroupBox2.Controls.Add(Me.cboCidade)
        Me.GroupBox2.Controls.Add(Me.lblCidade)
        Me.GroupBox2.Controls.Add(Me.chkZera)
        Me.GroupBox2.Controls.Add(Me.Label43)
        Me.GroupBox2.Controls.Add(Me.txtFichasDisponiveis)
        Me.GroupBox2.Controls.Add(Me.cboOperador)
        Me.GroupBox2.Controls.Add(Me.Label40)
        Me.GroupBox2.Controls.Add(Me.btnSeparaFichas)
        Me.GroupBox2.Controls.Add(Me.Label39)
        Me.GroupBox2.Controls.Add(Me.Label36)
        Me.GroupBox2.Controls.Add(Me.txtQtdFichas)
        Me.GroupBox2.Controls.Add(Me.btnPesquisar)
        Me.GroupBox2.Controls.Add(Me.DtMesAnoPesquisa)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.cboCategoria)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 6)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(448, 263)
        Me.GroupBox2.TabIndex = 19
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Consulta"
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.ForeColor = System.Drawing.Color.Red
        Me.Label52.Location = New System.Drawing.Point(6, 213)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(118, 13)
        Me.Label52.TabIndex = 90
        Me.Label52.Text = "Fichas Bloqueadas:"
        '
        'txtFichasBloqueadas
        '
        Me.txtFichasBloqueadas.Location = New System.Drawing.Point(129, 210)
        Me.txtFichasBloqueadas.Name = "txtFichasBloqueadas"
        Me.txtFichasBloqueadas.Size = New System.Drawing.Size(90, 20)
        Me.txtFichasBloqueadas.TabIndex = 89
        '
        'cboCidade
        '
        Me.cboCidade.FormattingEnabled = True
        Me.cboCidade.Location = New System.Drawing.Point(106, 75)
        Me.cboCidade.Name = "cboCidade"
        Me.cboCidade.Size = New System.Drawing.Size(178, 21)
        Me.cboCidade.TabIndex = 86
        Me.cboCidade.Visible = False
        '
        'lblCidade
        '
        Me.lblCidade.AutoSize = True
        Me.lblCidade.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCidade.Location = New System.Drawing.Point(40, 78)
        Me.lblCidade.Name = "lblCidade"
        Me.lblCidade.Size = New System.Drawing.Size(61, 13)
        Me.lblCidade.TabIndex = 87
        Me.lblCidade.Text = "CIDADE :"
        Me.lblCidade.Visible = False
        '
        'chkZera
        '
        Me.chkZera.AutoSize = True
        Me.chkZera.Location = New System.Drawing.Point(317, 180)
        Me.chkZera.Name = "chkZera"
        Me.chkZera.Size = New System.Drawing.Size(123, 17)
        Me.chkZera.TabIndex = 85
        Me.chkZera.Text = "Zerar Requisição"
        Me.chkZera.UseVisualStyleBackColor = True
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.ForeColor = System.Drawing.Color.ForestGreen
        Me.Label43.Location = New System.Drawing.Point(6, 183)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(117, 13)
        Me.Label43.TabIndex = 84
        Me.Label43.Text = "Fichas disponíveis:"
        '
        'txtFichasDisponiveis
        '
        Me.txtFichasDisponiveis.Location = New System.Drawing.Point(129, 180)
        Me.txtFichasDisponiveis.Name = "txtFichasDisponiveis"
        Me.txtFichasDisponiveis.Size = New System.Drawing.Size(90, 20)
        Me.txtFichasDisponiveis.TabIndex = 83
        '
        'cboOperador
        '
        Me.cboOperador.FormattingEnabled = True
        Me.cboOperador.Location = New System.Drawing.Point(257, 151)
        Me.cboOperador.Name = "cboOperador"
        Me.cboOperador.Size = New System.Drawing.Size(183, 21)
        Me.cboOperador.TabIndex = 81
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.Location = New System.Drawing.Point(256, 132)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(86, 16)
        Me.Label40.TabIndex = 82
        Me.Label40.Text = "Operadora:"
        '
        'btnSeparaFichas
        '
        Me.btnSeparaFichas.Location = New System.Drawing.Point(332, 203)
        Me.btnSeparaFichas.Name = "btnSeparaFichas"
        Me.btnSeparaFichas.Size = New System.Drawing.Size(110, 23)
        Me.btnSeparaFichas.TabIndex = 80
        Me.btnSeparaFichas.Text = "Serparar Fichas"
        Me.btnSeparaFichas.UseVisualStyleBackColor = True
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Location = New System.Drawing.Point(6, 154)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(103, 13)
        Me.Label39.TabIndex = 77
        Me.Label39.Text = "Total de Fichas :"
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Location = New System.Drawing.Point(6, 132)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(113, 13)
        Me.Label36.TabIndex = 76
        Me.Label36.Text = "SEPARAR FICHAS"
        '
        'txtQtdFichas
        '
        Me.txtQtdFichas.Enabled = False
        Me.txtQtdFichas.Location = New System.Drawing.Point(129, 151)
        Me.txtQtdFichas.Name = "txtQtdFichas"
        Me.txtQtdFichas.Size = New System.Drawing.Size(90, 20)
        Me.txtQtdFichas.TabIndex = 75
        '
        'btnPesquisar
        '
        Me.btnPesquisar.Location = New System.Drawing.Point(305, 22)
        Me.btnPesquisar.Name = "btnPesquisar"
        Me.btnPesquisar.Size = New System.Drawing.Size(88, 23)
        Me.btnPesquisar.TabIndex = 74
        Me.btnPesquisar.Text = "Pesquisar"
        Me.btnPesquisar.UseVisualStyleBackColor = True
        '
        'DtMesAnoPesquisa
        '
        Me.DtMesAnoPesquisa.CustomFormat = "MM/yyyy"
        Me.DtMesAnoPesquisa.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DtMesAnoPesquisa.Location = New System.Drawing.Point(106, 49)
        Me.DtMesAnoPesquisa.Name = "DtMesAnoPesquisa"
        Me.DtMesAnoPesquisa.Size = New System.Drawing.Size(178, 20)
        Me.DtMesAnoPesquisa.TabIndex = 73
        Me.DtMesAnoPesquisa.Value = New Date(2016, 12, 1, 0, 0, 0, 0)
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(27, 52)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(73, 13)
        Me.Label18.TabIndex = 19
        Me.Label18.Text = "MÊS REF. :"
        '
        'cboCategoria
        '
        Me.cboCategoria.FormattingEnabled = True
        Me.cboCategoria.Location = New System.Drawing.Point(106, 22)
        Me.cboCategoria.Name = "cboCategoria"
        Me.cboCategoria.Size = New System.Drawing.Size(178, 21)
        Me.cboCategoria.TabIndex = 16
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(14, 25)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(86, 13)
        Me.Label17.TabIndex = 18
        Me.Label17.Text = "CATEGORIA :"
        '
        'TabPage5
        '
        Me.TabPage5.BackColor = System.Drawing.Color.Gainsboro
        Me.TabPage5.Controls.Add(Me.GroupBox5)
        Me.TabPage5.Controls.Add(Me.rdManual)
        Me.TabPage5.Controls.Add(Me.rdOi)
        Me.TabPage5.Controls.Add(Me.Label47)
        Me.TabPage5.Controls.Add(Me.txtqtdFichasResetar)
        Me.TabPage5.Controls.Add(Me.cboCidadeResetar)
        Me.TabPage5.Controls.Add(Me.Label34)
        Me.TabPage5.Controls.Add(Me.cboOperadorResetar)
        Me.TabPage5.Controls.Add(Me.Label44)
        Me.TabPage5.Controls.Add(Me.btnResetaPersonalizado)
        Me.TabPage5.Controls.Add(Me.cboCategoriaResetar)
        Me.TabPage5.Controls.Add(Me.Label46)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(456, 510)
        Me.TabPage5.TabIndex = 1
        Me.TabPage5.Text = "Zerar Requisição Personalizado"
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.Color.Silver
        Me.GroupBox5.Controls.Add(Me.btnTrocarFichas)
        Me.GroupBox5.Controls.Add(Me.GroupBox7)
        Me.GroupBox5.Controls.Add(Me.GroupBox6)
        Me.GroupBox5.Location = New System.Drawing.Point(7, 232)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(397, 271)
        Me.GroupBox5.TabIndex = 99
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Trocar Fichas Oi Operadoras"
        '
        'btnTrocarFichas
        '
        Me.btnTrocarFichas.Location = New System.Drawing.Point(255, 235)
        Me.btnTrocarFichas.Name = "btnTrocarFichas"
        Me.btnTrocarFichas.Size = New System.Drawing.Size(110, 23)
        Me.btnTrocarFichas.TabIndex = 91
        Me.btnTrocarFichas.Text = "Trocar Fichas"
        Me.btnTrocarFichas.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.cbParaOperadorTrocar)
        Me.GroupBox7.Controls.Add(Me.Label51)
        Me.GroupBox7.Location = New System.Drawing.Point(15, 155)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(350, 63)
        Me.GroupBox7.TabIndex = 1
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Para:"
        '
        'cbParaOperadorTrocar
        '
        Me.cbParaOperadorTrocar.FormattingEnabled = True
        Me.cbParaOperadorTrocar.Location = New System.Drawing.Point(143, 23)
        Me.cbParaOperadorTrocar.Name = "cbParaOperadorTrocar"
        Me.cbParaOperadorTrocar.Size = New System.Drawing.Size(178, 21)
        Me.cbParaOperadorTrocar.TabIndex = 99
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label51.Location = New System.Drawing.Point(51, 24)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(90, 16)
        Me.Label51.TabIndex = 100
        Me.Label51.Text = "Operadora :"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.Label49)
        Me.GroupBox6.Controls.Add(Me.txtQtdFichasTrocar)
        Me.GroupBox6.Controls.Add(Me.Label48)
        Me.GroupBox6.Controls.Add(Me.cbDeOperadorTrocar)
        Me.GroupBox6.Controls.Add(Me.cbCategoriaTrocar)
        Me.GroupBox6.Controls.Add(Me.Label50)
        Me.GroupBox6.Location = New System.Drawing.Point(15, 29)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(350, 110)
        Me.GroupBox6.TabIndex = 0
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "De:"
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.Location = New System.Drawing.Point(6, 82)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(136, 13)
        Me.Label49.TabIndex = 100
        Me.Label49.Text = "Quantidade de fichas :"
        '
        'txtQtdFichasTrocar
        '
        Me.txtQtdFichasTrocar.Location = New System.Drawing.Point(143, 79)
        Me.txtQtdFichasTrocar.Name = "txtQtdFichasTrocar"
        Me.txtQtdFichasTrocar.Size = New System.Drawing.Size(178, 20)
        Me.txtQtdFichasTrocar.TabIndex = 99
        Me.txtQtdFichasTrocar.Text = "0"
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label48.Location = New System.Drawing.Point(56, 52)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(86, 13)
        Me.Label48.TabIndex = 91
        Me.Label48.Text = "CATEGORIA :"
        '
        'cbDeOperadorTrocar
        '
        Me.cbDeOperadorTrocar.FormattingEnabled = True
        Me.cbDeOperadorTrocar.Location = New System.Drawing.Point(143, 19)
        Me.cbDeOperadorTrocar.Name = "cbDeOperadorTrocar"
        Me.cbDeOperadorTrocar.Size = New System.Drawing.Size(178, 21)
        Me.cbDeOperadorTrocar.TabIndex = 97
        '
        'cbCategoriaTrocar
        '
        Me.cbCategoriaTrocar.FormattingEnabled = True
        Me.cbCategoriaTrocar.Location = New System.Drawing.Point(143, 49)
        Me.cbCategoriaTrocar.Name = "cbCategoriaTrocar"
        Me.cbCategoriaTrocar.Size = New System.Drawing.Size(178, 21)
        Me.cbCategoriaTrocar.TabIndex = 90
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label50.Location = New System.Drawing.Point(51, 20)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(90, 16)
        Me.Label50.TabIndex = 98
        Me.Label50.Text = "Operadora :"
        '
        'rdManual
        '
        Me.rdManual.AutoSize = True
        Me.rdManual.Checked = True
        Me.rdManual.Location = New System.Drawing.Point(258, 21)
        Me.rdManual.Name = "rdManual"
        Me.rdManual.Size = New System.Drawing.Size(114, 17)
        Me.rdManual.TabIndex = 98
        Me.rdManual.TabStop = True
        Me.rdManual.Text = "Sistema Manual"
        Me.rdManual.UseVisualStyleBackColor = True
        '
        'rdOi
        '
        Me.rdOi.AutoSize = True
        Me.rdOi.Location = New System.Drawing.Point(122, 21)
        Me.rdOi.Name = "rdOi"
        Me.rdOi.Size = New System.Drawing.Size(85, 17)
        Me.rdOi.TabIndex = 97
        Me.rdOi.TabStop = True
        Me.rdOi.Text = "Sistema Oi"
        Me.rdOi.UseVisualStyleBackColor = True
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Location = New System.Drawing.Point(57, 94)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(136, 13)
        Me.Label47.TabIndex = 96
        Me.Label47.Text = "Quantidade de fichas :"
        '
        'txtqtdFichasResetar
        '
        Me.txtqtdFichasResetar.Location = New System.Drawing.Point(194, 91)
        Me.txtqtdFichasResetar.Name = "txtqtdFichasResetar"
        Me.txtqtdFichasResetar.Size = New System.Drawing.Size(178, 20)
        Me.txtqtdFichasResetar.TabIndex = 95
        Me.txtqtdFichasResetar.Text = "0"
        '
        'cboCidadeResetar
        '
        Me.cboCidadeResetar.FormattingEnabled = True
        Me.cboCidadeResetar.Location = New System.Drawing.Point(194, 153)
        Me.cboCidadeResetar.Name = "cboCidadeResetar"
        Me.cboCidadeResetar.Size = New System.Drawing.Size(178, 21)
        Me.cboCidadeResetar.TabIndex = 93
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(131, 156)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(61, 13)
        Me.Label34.TabIndex = 94
        Me.Label34.Text = "CIDADE :"
        '
        'cboOperadorResetar
        '
        Me.cboOperadorResetar.FormattingEnabled = True
        Me.cboOperadorResetar.Location = New System.Drawing.Point(194, 121)
        Me.cboOperadorResetar.Name = "cboOperadorResetar"
        Me.cboOperadorResetar.Size = New System.Drawing.Size(178, 21)
        Me.cboOperadorResetar.TabIndex = 91
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.Location = New System.Drawing.Point(102, 122)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(90, 16)
        Me.Label44.TabIndex = 92
        Me.Label44.Text = "Operadora :"
        '
        'btnResetaPersonalizado
        '
        Me.btnResetaPersonalizado.Location = New System.Drawing.Point(262, 187)
        Me.btnResetaPersonalizado.Name = "btnResetaPersonalizado"
        Me.btnResetaPersonalizado.Size = New System.Drawing.Size(110, 23)
        Me.btnResetaPersonalizado.TabIndex = 90
        Me.btnResetaPersonalizado.Text = "Resetar Fichas"
        Me.btnResetaPersonalizado.UseVisualStyleBackColor = True
        '
        'cboCategoriaResetar
        '
        Me.cboCategoriaResetar.FormattingEnabled = True
        Me.cboCategoriaResetar.Location = New System.Drawing.Point(194, 61)
        Me.cboCategoriaResetar.Name = "cboCategoriaResetar"
        Me.cboCategoriaResetar.Size = New System.Drawing.Size(178, 21)
        Me.cboCategoriaResetar.TabIndex = 88
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.Location = New System.Drawing.Point(107, 64)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(86, 13)
        Me.Label46.TabIndex = 89
        Me.Label46.Text = "CATEGORIA :"
        '
        'FrmFinanceiro
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1185, 588)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmFinanceiro"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Controle Financeiro"
        Me.Panel1.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.gridRequisicao, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.gridRequisicaoCategoria, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.gpOperador4.ResumeLayout(False)
        Me.gpOperador4.PerformLayout()
        Me.gpOperador3.ResumeLayout(False)
        Me.gpOperador3.PerformLayout()
        Me.gpOperador2.ResumeLayout(False)
        Me.gpOperador2.PerformLayout()
        Me.gpOperador1.ResumeLayout(False)
        Me.gpOperador1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.TabControl2.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents cboCategoria As System.Windows.Forms.ComboBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents lblValorAberto As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents lblFichaAberto As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents lblValorRecusado As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lblFichaRecusada As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents lblValorConfirmado As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblFichaConfirmada As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblTotalFichas As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblCategoria As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DtMesAnoPesquisa As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblValorTotal As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents btnPesquisar As System.Windows.Forms.Button
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents btnSeparaFichas As System.Windows.Forms.Button
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtQtdFichas As System.Windows.Forms.TextBox
    Friend WithEvents cboOperador As System.Windows.Forms.ComboBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents txtFichasDisponiveis As System.Windows.Forms.TextBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents lblValorReq As System.Windows.Forms.Label
    Friend WithEvents lblQuantidadeReq As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents gridRequisicao As System.Windows.Forms.DataGridView
    Friend WithEvents gridRequisicaoCategoria As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtCoeficiente As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents btnCalcular As System.Windows.Forms.Button
    Friend WithEvents gpOperador4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents lblValComissaoOp4 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents lblOp4FichaRecusada As System.Windows.Forms.Label
    Friend WithEvents lblOperador4 As System.Windows.Forms.Label
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents lblOp4FichaConf As System.Windows.Forms.Label
    Friend WithEvents lblOp4ValorRecusada As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents lblOp4ValorConf As System.Windows.Forms.Label
    Friend WithEvents gpOperador3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents lblValComissaoOp3 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents lblOp3FichaRecusada As System.Windows.Forms.Label
    Friend WithEvents lblOperador3 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents lblOp3FichaConf As System.Windows.Forms.Label
    Friend WithEvents lblOp3ValorRecusada As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents lblOp3ValorConf As System.Windows.Forms.Label
    Friend WithEvents gpOperador2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents lblValComissaoOp2 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblOp2FichaRecusada As System.Windows.Forms.Label
    Friend WithEvents lblOperador2 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents lblOp2FichaConf As System.Windows.Forms.Label
    Friend WithEvents lblOp2ValorRecusada As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents lblOp2ValorConf As System.Windows.Forms.Label
    Friend WithEvents gpOperador1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents lblValComissaoOp1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents lblOp1FichaRecusada As System.Windows.Forms.Label
    Friend WithEvents lblOperador1 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lblOp1FichaConf As System.Windows.Forms.Label
    Friend WithEvents lblOp1ValorRecusada As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblOp1ValorConf As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents chkZera As System.Windows.Forms.CheckBox
    Friend WithEvents cboCidade As System.Windows.Forms.ComboBox
    Friend WithEvents lblCidade As System.Windows.Forms.Label
    Friend WithEvents TabControl2 As TabControl
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents TabPage5 As TabPage
    Friend WithEvents cboCidadeResetar As ComboBox
    Friend WithEvents Label34 As Label
    Friend WithEvents cboOperadorResetar As ComboBox
    Friend WithEvents Label44 As Label
    Friend WithEvents btnResetaPersonalizado As Button
    Friend WithEvents cboCategoriaResetar As ComboBox
    Friend WithEvents Label46 As Label
    Friend WithEvents Label47 As Label
    Friend WithEvents txtqtdFichasResetar As TextBox
    Friend WithEvents rdManual As RadioButton
    Friend WithEvents rdOi As RadioButton
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents btnTrocarFichas As Button
    Friend WithEvents GroupBox7 As GroupBox
    Friend WithEvents cbParaOperadorTrocar As ComboBox
    Friend WithEvents Label51 As Label
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents Label49 As Label
    Friend WithEvents txtQtdFichasTrocar As TextBox
    Friend WithEvents Label48 As Label
    Friend WithEvents cbDeOperadorTrocar As ComboBox
    Friend WithEvents cbCategoriaTrocar As ComboBox
    Friend WithEvents Label50 As Label
    Friend WithEvents Label52 As Label
    Friend WithEvents txtFichasBloqueadas As TextBox
End Class
