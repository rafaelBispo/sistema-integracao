﻿Imports SISINT001
Imports Util

Public Class FrmCadUsuario

    Private Sub btnSalvar_Click(sender As System.Object, e As System.EventArgs) Handles btnSalvar.Click
        Dim rn As RNGenerico = New RNGenerico
        Dim usuario As CGenerico = New CGenerico
        Try


            If ValidaCampos() = False Then
                Exit Sub
            End If

            usuario = AtribuiCamposAoObjeto()

            If rn.CadastroUsuario(usuario, "I") = True Then
                MsgBox("Dados salvos com sucesso!", MessageBoxButtons.OK, "Confirmação")
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnAlterar_Click(sender As System.Object, e As System.EventArgs) Handles btnAlterar.Click
        Dim rn As RNGenerico = New RNGenerico
        Dim usuario As CGenerico = New CGenerico
        Try


            If ValidaCampos() = False Then
                Exit Sub
            End If

            usuario = AtribuiCamposAoObjeto()

            If rn.CadastroUsuario(usuario, "A") = True Then
                MsgBox("Dados salvos com sucesso!", MsgBoxStyle.OkOnly, "Confirmação!")
            End If

            LimparCampos()

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnExcluir_Click(sender As System.Object, e As System.EventArgs) Handles btnExcluir.Click
        Dim rn As RNGenerico = New RNGenerico
        Dim usuario As CGenerico = New CGenerico
        Try


            If ValidaCampos() = False Then
                Exit Sub
            End If

            usuario = AtribuiCamposAoObjeto()

            If rn.CadastroUsuario(usuario, "E") = True Then
                MsgBox("Dados salvos com sucesso!", "Confirmação", MessageBoxButtons.OK)
            End If

            btnSalvar.Enabled = False
            btnExcluir.Enabled = False
            btnAlterar.Enabled = False
            btnNovo.Enabled = True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnNovo_Click(sender As System.Object, e As System.EventArgs) Handles btnNovo.Click
        Dim rn As RNGenerico = New RNGenerico
        Try

            LimparCampos()

            txtCodigo.Text = rn.RetornaIDFuncionario()

            btnSalvar.Enabled = True
            btnExcluir.Enabled = True
            btnAlterar.Enabled = False

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub btnSair_Click(sender As System.Object, e As System.EventArgs) Handles btnSair.Click
        Me.Close()
    End Sub

    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Dim rn As RNGenerico = New RNGenerico
        Dim usuarios As List(Of CGenerico) = New List(Of CGenerico)
        Dim usuario As CGenerico = New CGenerico
        Dim dados_acessos As List(Of CGenerico) = New List(Of CGenerico)
        Dim dados_acesso As CGenerico = New CGenerico

        Try

            usuarios = rn.BuscarFuncionarioPeloNome(cboNome.Text)

            For Each usuario In usuarios
                'popula a tela
                txtCodigo.Text = usuario.Cod_Ean_Funcionario
                txtNome.Text = usuario.Nome_Funcionario
                txtCnpjCpf.Text = usuario.Cpf
                txtRg.Text = usuario.RG
                txtEmail.Text = usuario.Email
                txtDDD.Text = usuario.DDD
                txtTelefone1.Text = usuario.Telefone
                txtTelefone2.Text = usuario.Celular
                txtEndereco.Text = usuario.Endereco
                txtUF.Text = usuario.Uf_Cidade
                txtCEP.Text = usuario.Cep
                txtDtNc.Text = usuario.Data_Nascimento
                txtdtAdmissao.Text = usuario.Data_admissao
                txtDtDemissao.Text = usuario.Data_demissao
                txtObs.Text = usuario.Observacao
                txtMeta1Qtd.Text = usuario.Quant_1
                txtMeta1Valor.Text = usuario.Meta_1
                txtMeta1Comissao.Text = usuario.Comissao_1
                txtMeta2Qtd.Text = usuario.Quant_2
                txtMeta2Valor.Text = usuario.Meta_2
                txtMeta2Comissao.Text = usuario.Comissao_2
                txtMeta3Qtd.Text = usuario.Quant_3
                txtMeta3Valor.Text = usuario.Meta_3
                txtMeta3Comissao.Text = usuario.Comissao_3

                CarregaComboBairros(usuario.Cod_Bairro)
                CarregaComboCargo(usuario.Cod_Cargo)
                CarregaComboCidades(usuario.Cod_Cidade)

                If usuario.Sexo = "Masculino" Then
                    cboSexo.SelectedIndex = 1
                Else
                    cboSexo.SelectedIndex = 2
                End If


            Next

            ' MsgBox("Nome do funcionario: " + cboNome.Text + ", Código do funcionario: " + cboNome.SelectedValue.ToString(), MsgBoxStyle.Information)

            dados_acessos = rn.BuscaDadosAcesso(cboNome.SelectedValue)

            For Each dados_acesso In dados_acessos
                txtUsuario.Text = dados_acesso.Usuario
                txtSenha.Text = dados_acesso.Senha
                txtConfirmaSenha.Text = dados_acesso.Senha

                If dados_acesso.Acesso = 1 Then
                    cboNivelAcesso.SelectedIndex = 1
                Else
                    cboNivelAcesso.SelectedIndex = 2
                End If
            Next
            btnSalvar.Enabled = False
            btnAlterar.Enabled = True
            btnExcluir.Enabled = True
            btnNovo.Enabled = True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub LimparCampos()

        'DADOS DO LOGIN
        txtUsuario.Text = ""
        txtConfirmaSenha.Text = ""
        txtSenha.Text = ""
        cboNivelAcesso.SelectedIndex = 0

        'DADOS DO USUARIO
        txtCEP.Text = ""
        txtCnpjCpf.Text = ""
        txtCodigo.Text = ""
        txtDDD.Text = ""
        txtdtAdmissao.Text = ""
        txtDtDemissao.Text = ""
        txtDtNc.Text = ""
        txtEmail.Text = ""
        txtEndereco.Text = ""
        txtMeta1Comissao.Text = ""
        txtMeta1Qtd.Text = ""
        txtMeta1Valor.Text = ""
        txtMeta2Comissao.Text = ""
        txtMeta2Qtd.Text = ""
        txtMeta2Valor.Text = ""
        txtMeta3Comissao.Text = ""
        txtMeta3Qtd.Text = ""
        txtMeta3Valor.Text = ""
        txtNome.Text = ""
        txtObs.Text = ""
        txtRg.Text = ""
        txtTelefone1.Text = ""
        txtTelefone2.Text = ""
        txtUF.Text = ""
        cboBairro.SelectedIndex = 0
        cboCidade.SelectedIndex = 0
        cboSexo.SelectedIndex = 0
        cboCargo.SelectedIndex = 0
        cboCidade.SelectedIndex = 0
        cboNome.SelectedIndex = 0

    End Sub

    Public Function AtribuiCamposAoObjeto() As CGenerico
        Dim usuario As CGenerico = New CGenerico
        Try

            usuario.Cod_Ean_Funcionario = txtCodigo.Text
            usuario.Nome_Funcionario = IIf(txtNome.Text = "", "", txtNome.Text)
            usuario.Cpf = IIf(txtCnpjCpf.Text = "", "1", txtCnpjCpf.Text)
            usuario.RG = IIf(txtRg.Text = "", "1", txtRg.Text)
            usuario.Email = IIf(txtEmail.Text = "", "1", txtEmail.Text)
            usuario.DDD = IIf(txtDDD.Text = "", "1", txtDDD.Text)
            usuario.Telefone = IIf(txtTelefone1.Text = "", "1", txtTelefone1.Text)
            usuario.Celular = IIf(txtTelefone2.Text = "", txtTelefone1.Text, txtTelefone2.Text)
            usuario.Endereco = IIf(txtEndereco.Text = "", "1", txtEndereco.Text)
            usuario.Uf_Cidade = IIf(txtUF.Text = "", "MG", txtUF.Text)
            usuario.Cep = IIf(txtCEP.Text = "", "1", txtCEP.Text)
            usuario.Data_Nascimento = IIf(IsDBNull(txtDtNc.Text), "1900-01-01", txtDtNc.Text)
            usuario.Data_admissao = IIf(IsDBNull(txtdtAdmissao.Text), "1900-01-01", txtdtAdmissao.Text)
            usuario.Data_demissao = IIf(IsDBNull(txtDtDemissao.Text), "1900-01-01", txtDtDemissao.Text)
            usuario.Observacao = txtObs.Text
            usuario.Quant_1 = IIf(txtMeta1Qtd.Text = "", 0, txtMeta1Qtd.Text)
            usuario.Meta_1 = IIf(txtMeta1Valor.Text = "", 0, txtMeta1Valor.Text)
            usuario.Comissao_1 = IIf(txtMeta1Comissao.Text = "", 0, txtMeta1Comissao.Text)
            usuario.Quant_2 = IIf(txtMeta2Qtd.Text = "", 0, txtMeta2Qtd.Text)
            usuario.Meta_2 = IIf(txtMeta2Valor.Text = "", 0, txtMeta2Valor.Text)
            usuario.Comissao_2 = IIf(txtMeta2Comissao.Text = "", 0, txtMeta2Comissao.Text)
            usuario.Quant_3 = IIf(txtMeta3Qtd.Text = "", 0, txtMeta3Qtd.Text)
            usuario.Meta_3 = IIf(txtMeta3Valor.Text = "", 0, txtMeta3Valor.Text)
            usuario.Comissao_3 = IIf(txtMeta3Comissao.Text = "", 0, txtMeta3Comissao.Text)

            If cboCidade.SelectedValue <= 0 Then
                usuario.Cod_Cidade = 1
            Else
                usuario.Cod_Cidade = cboCidade.SelectedValue
            End If

            If cboCidade.Text = "Selecione..." Then
                usuario.Cidade = "JUIZ DE FORA"
            Else
                usuario.Cidade = cboCidade.Text
            End If

            If cboBairro.SelectedValue <= 0 Then
                usuario.Cod_Bairro = 1
            Else
                usuario.Cod_Bairro = cboBairro.SelectedValue
            End If

            If cboBairro.Text = "Selecione..." Then
                usuario.Bairro = "CENTRO"
            Else
                usuario.Bairro = cboBairro.Text
            End If

            If cboCargo.SelectedValue <= 0 Then
                usuario.Cod_Cargo = 1
            Else
                usuario.Cod_Cargo = cboCargo.SelectedValue
            End If

            If cboSexo.SelectedValue <= 0 Then
                usuario.Sexo = "FEMININO"
            Else
                usuario.Sexo = cboSexo.SelectedValue
            End If

            usuario.Usuario = txtUsuario.Text
            usuario.Senha = txtSenha.Text
            usuario.Cod_empresa = 1
            If cboNivelAcesso.SelectedIndex = 1 Then
                usuario.Acesso = 1
            ElseIf cboNivelAcesso.SelectedIndex = 2 Then
                usuario.Acesso = 6
            Else
                usuario.Acesso = 6
            End If

            Return usuario

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return Nothing
        End Try
    End Function


    Public Function ValidaCampos() As Boolean

        If txtUsuario.Text.Trim = "" Then
            MessageBox.Show("Informe o usuário!", "Chave", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtUsuario.Focus()
            Return False
        End If


        If txtSenha.Text.Trim = "" Then
            MessageBox.Show("Informe a sua senha!", "Senha", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtSenha.Focus()
            Return False

        End If

        If txtConfirmaSenha.Text.Trim <> txtSenha.Text.Trim Then
            MessageBox.Show("As senhas não conferem!", "Senha", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            txtSenha.Focus()
            Return False

        End If

        If cboNivelAcesso.SelectedIndex = 0 Then
            MessageBox.Show("Por favor selecione o nível de acesso do usuário", "Nível de Acesso", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            cboNivelAcesso.Focus()
            Return False

        End If

        Return True

    End Function

    Private Sub FrmCadUsuario_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            CarregaComboOperador(0)
            CarregaComboCargo(0)
            CarregaComboCidades(0)
            CarregaComboBairros(0)

            cboSexo.SelectedIndex = 0
            cboNivelAcesso.SelectedIndex = 0
            btnSalvar.Enabled = False
            btnAlterar.Enabled = False
            btnExcluir.Enabled = False


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub CarregaComboOperador(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboNome.DataSource = Nothing
            cboNome.Items.Clear()
            cboNome.DisplayMember = "Nome_Operador"
            cboNome.ValueMember = "Cod_Operador"
            cboNome.DataSource = rn_generico.BuscarOperador(0)

            If cboNome.Items.Count > 0 And cod_operador = 0 Then
                cboNome.SelectedIndex = 0
            ElseIf cboNome.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboNome.Items.Count - 1
                    For Each linha As CGenerico In cboNome.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboNome.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboNome.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboCargo(ByVal cod_cargo As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCargo.DataSource = Nothing
            cboCargo.Items.Clear()
            cboCargo.DisplayMember = "Nome_Cargo"
            cboCargo.ValueMember = "Cod_Cargo"
            cboCargo.DataSource = rn_generico.BuscarCargo(0)

            If cboCargo.Items.Count > 0 And cod_cargo = 0 Then
                cboCargo.SelectedIndex = 0
            ElseIf cboCargo.Items.Count > 0 And cod_cargo <> 0 Then
                For i As Integer = 0 To cboCargo.Items.Count - 1
                    For Each linha As CGenerico In cboCargo.DataSource
                        If linha.Cod_Cargo = cod_cargo Then
                            cboCargo.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCargo.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboCidades(ByVal cod_cidade As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCidade.DataSource = Nothing
            cboCidade.Items.Clear()
            cboCidade.DisplayMember = "Nome_cidade"
            cboCidade.ValueMember = "Cod_Cidade"
            cboCidade.DataSource = rn_generico.BuscarCidades(0)

            If cboCidade.Items.Count = 0 Then
                cboCidade.SelectedIndex = 0
            ElseIf cboCidade.Items.Count > 0 And cod_cidade <> 0 Then

                For i As Integer = 0 To cboCidade.Items.Count - 1
                    For Each linha As CGenerico In cboCidade.DataSource
                        If linha.Cod_Cidade = cod_cidade Then
                            cboCidade.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next

            Else

                cboCidade.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CarregaComboBairros(ByVal cod_bairro As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try



            cboBairro.DataSource = Nothing
            cboBairro.Items.Clear()
            cboBairro.DisplayMember = "Nome_Bairro"
            cboBairro.ValueMember = "Cod_Bairro"
            cboBairro.DataSource = rn_generico.BuscarBairros(0)

            If cboBairro.Items.Count > 0 And cod_bairro = 0 Then
                cboBairro.SelectedIndex = 0

            ElseIf cboBairro.Items.Count > 0 And cod_bairro <> 0 Then
                For i As Integer = 0 To cboBairro.Items.Count - 1
                    For Each linha As CGenerico In cboBairro.DataSource
                        If linha.Cod_Bairro = cod_bairro Then
                            cboBairro.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboBairro.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub
End Class