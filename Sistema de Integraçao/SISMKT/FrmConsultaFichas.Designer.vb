﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConsultaFichas
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cboOperador2 = New System.Windows.Forms.ComboBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.txtdiaPesquisa = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.cboCidadeLote = New System.Windows.Forms.ComboBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.txtTelefone = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtNome = New System.Windows.Forms.TextBox()
        Me.cboCategoria = New System.Windows.Forms.ComboBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.btnPesquisar = New System.Windows.Forms.Button()
        Me.cboNome = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.grdRemessa = New System.Windows.Forms.DataGridView()
        Me.btnSair = New System.Windows.Forms.Button()
        Me.lblTotalFichas = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblValor = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.btnLibera = New System.Windows.Forms.Button()
        Me.btnRecusa = New System.Windows.Forms.Button()
        Me.TabInformacoes = New System.Windows.Forms.TabControl()
        Me.tabHistorico = New System.Windows.Forms.TabPage()
        Me.GrdHistorico = New System.Windows.Forms.DataGridView()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.txtValDoacao = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txtMelhorDia = New System.Windows.Forms.TextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.chkInativo = New System.Windows.Forms.CheckBox()
        Me.chkNaoPedirMensal = New System.Windows.Forms.CheckBox()
        Me.chkNaoPedirExtra = New System.Windows.Forms.CheckBox()
        Me.chkTelDesl = New System.Windows.Forms.CheckBox()
        Me.txtDtCadastro = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtObs = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtReferencia = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtDtNcMp = New System.Windows.Forms.TextBox()
        Me.txtDtNc = New System.Windows.Forms.TextBox()
        Me.txtRegiao = New System.Windows.Forms.TextBox()
        Me.txtCEP = New System.Windows.Forms.TextBox()
        Me.txtUF = New System.Windows.Forms.TextBox()
        Me.cboBairro = New System.Windows.Forms.ComboBox()
        Me.txtDDD = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.cboCategoriaDetalhe = New System.Windows.Forms.ComboBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.cboEspecie = New System.Windows.Forms.ComboBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.cboOperador = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.cboCidade = New System.Windows.Forms.ComboBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtTelefone2 = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtTelefone1 = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtRg = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtCnpjCpf = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtEndereco = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtNomeCliente2 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtNomeCliente1 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtCodBarra = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.tabConcluir = New System.Windows.Forms.TabPage()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.txtValorConcluir = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.txtDiaConcluir = New System.Windows.Forms.TextBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.txtDDDConcluir = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtTelefoneConcluir = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.txtNomClienteConcluir = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.txtObsConcluir = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.btnNaoContribuir = New System.Windows.Forms.Button()
        Me.btnContribuir = New System.Windows.Forms.Button()
        Me.chkHeader = New System.Windows.Forms.CheckBox()
        Me.Panel1.SuspendLayout()
        CType(Me.grdRemessa, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabInformacoes.SuspendLayout()
        Me.tabHistorico.SuspendLayout()
        CType(Me.GrdHistorico, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        Me.tabConcluir.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.cboOperador2)
        Me.Panel1.Controls.Add(Me.Label41)
        Me.Panel1.Controls.Add(Me.txtdiaPesquisa)
        Me.Panel1.Controls.Add(Me.Label40)
        Me.Panel1.Controls.Add(Me.cboCidadeLote)
        Me.Panel1.Controls.Add(Me.Label39)
        Me.Panel1.Controls.Add(Me.txtTelefone)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.txtNome)
        Me.Panel1.Controls.Add(Me.cboCategoria)
        Me.Panel1.Controls.Add(Me.Label26)
        Me.Panel1.Controls.Add(Me.btnPesquisar)
        Me.Panel1.Controls.Add(Me.cboNome)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1127, 65)
        Me.Panel1.TabIndex = 1
        '
        'cboOperador2
        '
        Me.cboOperador2.FormattingEnabled = True
        Me.cboOperador2.Location = New System.Drawing.Point(848, 37)
        Me.cboOperador2.Name = "cboOperador2"
        Me.cboOperador2.Size = New System.Drawing.Size(176, 21)
        Me.cboOperador2.TabIndex = 89
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.Location = New System.Drawing.Point(755, 39)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(77, 16)
        Me.Label41.TabIndex = 90
        Me.Label41.Text = "Operadora:"
        '
        'txtdiaPesquisa
        '
        Me.txtdiaPesquisa.Location = New System.Drawing.Point(597, 37)
        Me.txtdiaPesquisa.Name = "txtdiaPesquisa"
        Me.txtdiaPesquisa.Size = New System.Drawing.Size(69, 20)
        Me.txtdiaPesquisa.TabIndex = 83
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.Location = New System.Drawing.Point(559, 39)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(32, 16)
        Me.Label40.TabIndex = 82
        Me.Label40.Text = "Dia:"
        '
        'cboCidadeLote
        '
        Me.cboCidadeLote.FormattingEnabled = True
        Me.cboCidadeLote.Location = New System.Drawing.Point(834, 12)
        Me.cboCidadeLote.Name = "cboCidadeLote"
        Me.cboCidadeLote.Size = New System.Drawing.Size(187, 21)
        Me.cboCidadeLote.TabIndex = 81
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.Location = New System.Drawing.Point(777, 13)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(55, 16)
        Me.Label39.TabIndex = 80
        Me.Label39.Text = "Cidade:"
        '
        'txtTelefone
        '
        Me.txtTelefone.Location = New System.Drawing.Point(401, 14)
        Me.txtTelefone.Name = "txtTelefone"
        Me.txtTelefone.Size = New System.Drawing.Size(116, 20)
        Me.txtTelefone.TabIndex = 77
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(331, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(65, 16)
        Me.Label4.TabIndex = 76
        Me.Label4.Text = "Telefone:"
        '
        'txtNome
        '
        Me.txtNome.Location = New System.Drawing.Point(64, 14)
        Me.txtNome.Name = "txtNome"
        Me.txtNome.Size = New System.Drawing.Size(261, 20)
        Me.txtNome.TabIndex = 73
        '
        'cboCategoria
        '
        Me.cboCategoria.FormattingEnabled = True
        Me.cboCategoria.Location = New System.Drawing.Point(596, 12)
        Me.cboCategoria.Name = "cboCategoria"
        Me.cboCategoria.Size = New System.Drawing.Size(175, 21)
        Me.cboCategoria.TabIndex = 69
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(523, 13)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(73, 16)
        Me.Label26.TabIndex = 68
        Me.Label26.Text = "Categoria: "
        '
        'btnPesquisar
        '
        Me.btnPesquisar.Location = New System.Drawing.Point(1038, 13)
        Me.btnPesquisar.Name = "btnPesquisar"
        Me.btnPesquisar.Size = New System.Drawing.Size(75, 23)
        Me.btnPesquisar.TabIndex = 67
        Me.btnPesquisar.Text = "Pesquisar"
        Me.btnPesquisar.UseVisualStyleBackColor = True
        '
        'cboNome
        '
        Me.cboNome.Enabled = False
        Me.cboNome.FormattingEnabled = True
        Me.cboNome.Location = New System.Drawing.Point(64, 14)
        Me.cboNome.Name = "cboNome"
        Me.cboNome.Size = New System.Drawing.Size(261, 21)
        Me.cboNome.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(14, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nome:"
        '
        'grdRemessa
        '
        Me.grdRemessa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdRemessa.Location = New System.Drawing.Point(12, 83)
        Me.grdRemessa.Name = "grdRemessa"
        Me.grdRemessa.Size = New System.Drawing.Size(995, 280)
        Me.grdRemessa.TabIndex = 2
        '
        'btnSair
        '
        Me.btnSair.Location = New System.Drawing.Point(1041, 673)
        Me.btnSair.Name = "btnSair"
        Me.btnSair.Size = New System.Drawing.Size(75, 23)
        Me.btnSair.TabIndex = 3
        Me.btnSair.Text = "Sair"
        Me.btnSair.UseVisualStyleBackColor = True
        '
        'lblTotalFichas
        '
        Me.lblTotalFichas.AutoSize = True
        Me.lblTotalFichas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalFichas.Location = New System.Drawing.Point(1023, 117)
        Me.lblTotalFichas.Name = "lblTotalFichas"
        Me.lblTotalFichas.Size = New System.Drawing.Size(16, 16)
        Me.lblTotalFichas.TabIndex = 80
        Me.lblTotalFichas.Text = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(1019, 89)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(120, 16)
        Me.Label2.TabIndex = 79
        Me.Label2.Text = "Total de Fichas:"
        '
        'lblValor
        '
        Me.lblValor.AutoSize = True
        Me.lblValor.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValor.Location = New System.Drawing.Point(1023, 182)
        Me.lblValor.Name = "lblValor"
        Me.lblValor.Size = New System.Drawing.Size(16, 16)
        Me.lblValor.TabIndex = 82
        Me.lblValor.Text = "0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(1023, 149)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(93, 16)
        Me.Label5.TabIndex = 81
        Me.Label5.Text = "Valor Total :"
        '
        'btnLibera
        '
        Me.btnLibera.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLibera.Location = New System.Drawing.Point(1017, 391)
        Me.btnLibera.Name = "btnLibera"
        Me.btnLibera.Size = New System.Drawing.Size(109, 59)
        Me.btnLibera.TabIndex = 84
        Me.btnLibera.Text = "Libera para Faturamento"
        Me.btnLibera.UseVisualStyleBackColor = True
        '
        'btnRecusa
        '
        Me.btnRecusa.Location = New System.Drawing.Point(1041, 456)
        Me.btnRecusa.Name = "btnRecusa"
        Me.btnRecusa.Size = New System.Drawing.Size(75, 23)
        Me.btnRecusa.TabIndex = 85
        Me.btnRecusa.Text = "Recusa"
        Me.btnRecusa.UseVisualStyleBackColor = True
        '
        'TabInformacoes
        '
        Me.TabInformacoes.Controls.Add(Me.tabHistorico)
        Me.TabInformacoes.Controls.Add(Me.TabPage2)
        Me.TabInformacoes.Controls.Add(Me.tabConcluir)
        Me.TabInformacoes.Location = New System.Drawing.Point(12, 369)
        Me.TabInformacoes.Name = "TabInformacoes"
        Me.TabInformacoes.SelectedIndex = 0
        Me.TabInformacoes.Size = New System.Drawing.Size(999, 337)
        Me.TabInformacoes.TabIndex = 86
        '
        'tabHistorico
        '
        Me.tabHistorico.Controls.Add(Me.GrdHistorico)
        Me.tabHistorico.Location = New System.Drawing.Point(4, 22)
        Me.tabHistorico.Name = "tabHistorico"
        Me.tabHistorico.Padding = New System.Windows.Forms.Padding(3)
        Me.tabHistorico.Size = New System.Drawing.Size(991, 311)
        Me.tabHistorico.TabIndex = 0
        Me.tabHistorico.Text = "Histórico"
        Me.tabHistorico.UseVisualStyleBackColor = True
        '
        'GrdHistorico
        '
        Me.GrdHistorico.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GrdHistorico.Location = New System.Drawing.Point(6, 6)
        Me.GrdHistorico.Name = "GrdHistorico"
        Me.GrdHistorico.Size = New System.Drawing.Size(979, 299)
        Me.GrdHistorico.TabIndex = 3
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.txtValDoacao)
        Me.TabPage2.Controls.Add(Me.Label29)
        Me.TabPage2.Controls.Add(Me.txtMelhorDia)
        Me.TabPage2.Controls.Add(Me.Label22)
        Me.TabPage2.Controls.Add(Me.chkInativo)
        Me.TabPage2.Controls.Add(Me.chkNaoPedirMensal)
        Me.TabPage2.Controls.Add(Me.chkNaoPedirExtra)
        Me.TabPage2.Controls.Add(Me.chkTelDesl)
        Me.TabPage2.Controls.Add(Me.txtDtCadastro)
        Me.TabPage2.Controls.Add(Me.Label30)
        Me.TabPage2.Controls.Add(Me.txtObs)
        Me.TabPage2.Controls.Add(Me.Label25)
        Me.TabPage2.Controls.Add(Me.txtReferencia)
        Me.TabPage2.Controls.Add(Me.Label24)
        Me.TabPage2.Controls.Add(Me.txtDtNcMp)
        Me.TabPage2.Controls.Add(Me.txtDtNc)
        Me.TabPage2.Controls.Add(Me.txtRegiao)
        Me.TabPage2.Controls.Add(Me.txtCEP)
        Me.TabPage2.Controls.Add(Me.txtUF)
        Me.TabPage2.Controls.Add(Me.cboBairro)
        Me.TabPage2.Controls.Add(Me.txtDDD)
        Me.TabPage2.Controls.Add(Me.Label34)
        Me.TabPage2.Controls.Add(Me.cboCategoriaDetalhe)
        Me.TabPage2.Controls.Add(Me.Label28)
        Me.TabPage2.Controls.Add(Me.cboEspecie)
        Me.TabPage2.Controls.Add(Me.Label27)
        Me.TabPage2.Controls.Add(Me.cboOperador)
        Me.TabPage2.Controls.Add(Me.Label3)
        Me.TabPage2.Controls.Add(Me.Label21)
        Me.TabPage2.Controls.Add(Me.Label20)
        Me.TabPage2.Controls.Add(Me.Label19)
        Me.TabPage2.Controls.Add(Me.Label18)
        Me.TabPage2.Controls.Add(Me.Label17)
        Me.TabPage2.Controls.Add(Me.Label16)
        Me.TabPage2.Controls.Add(Me.cboCidade)
        Me.TabPage2.Controls.Add(Me.Label15)
        Me.TabPage2.Controls.Add(Me.txtTelefone2)
        Me.TabPage2.Controls.Add(Me.Label14)
        Me.TabPage2.Controls.Add(Me.txtTelefone1)
        Me.TabPage2.Controls.Add(Me.Label13)
        Me.TabPage2.Controls.Add(Me.txtRg)
        Me.TabPage2.Controls.Add(Me.Label12)
        Me.TabPage2.Controls.Add(Me.txtCnpjCpf)
        Me.TabPage2.Controls.Add(Me.Label11)
        Me.TabPage2.Controls.Add(Me.txtEndereco)
        Me.TabPage2.Controls.Add(Me.Label10)
        Me.TabPage2.Controls.Add(Me.txtEmail)
        Me.TabPage2.Controls.Add(Me.Label9)
        Me.TabPage2.Controls.Add(Me.txtNomeCliente2)
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Controls.Add(Me.txtNomeCliente1)
        Me.TabPage2.Controls.Add(Me.Label8)
        Me.TabPage2.Controls.Add(Me.txtCodBarra)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.txtCodigo)
        Me.TabPage2.Controls.Add(Me.Label23)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(991, 311)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Informações"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'txtValDoacao
        '
        Me.txtValDoacao.Location = New System.Drawing.Point(833, 151)
        Me.txtValDoacao.Name = "txtValDoacao"
        Me.txtValDoacao.ReadOnly = True
        Me.txtValDoacao.Size = New System.Drawing.Size(73, 20)
        Me.txtValDoacao.TabIndex = 125
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(830, 132)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(43, 16)
        Me.Label29.TabIndex = 124
        Me.Label29.Text = "Valor:"
        '
        'txtMelhorDia
        '
        Me.txtMelhorDia.Location = New System.Drawing.Point(741, 151)
        Me.txtMelhorDia.Name = "txtMelhorDia"
        Me.txtMelhorDia.ReadOnly = True
        Me.txtMelhorDia.Size = New System.Drawing.Size(73, 20)
        Me.txtMelhorDia.TabIndex = 123
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(738, 132)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(76, 16)
        Me.Label22.TabIndex = 122
        Me.Label22.Text = "Melhor Dia:"
        '
        'chkInativo
        '
        Me.chkInativo.AutoSize = True
        Me.chkInativo.Enabled = False
        Me.chkInativo.Location = New System.Drawing.Point(778, 222)
        Me.chkInativo.Name = "chkInativo"
        Me.chkInativo.Size = New System.Drawing.Size(58, 17)
        Me.chkInativo.TabIndex = 121
        Me.chkInativo.Text = "Inativo"
        Me.chkInativo.UseVisualStyleBackColor = True
        '
        'chkNaoPedirMensal
        '
        Me.chkNaoPedirMensal.AutoSize = True
        Me.chkNaoPedirMensal.Enabled = False
        Me.chkNaoPedirMensal.Location = New System.Drawing.Point(778, 268)
        Me.chkNaoPedirMensal.Name = "chkNaoPedirMensal"
        Me.chkNaoPedirMensal.Size = New System.Drawing.Size(109, 17)
        Me.chkNaoPedirMensal.TabIndex = 120
        Me.chkNaoPedirMensal.Text = "Não pedir Mensal"
        Me.chkNaoPedirMensal.UseVisualStyleBackColor = True
        '
        'chkNaoPedirExtra
        '
        Me.chkNaoPedirExtra.AutoSize = True
        Me.chkNaoPedirExtra.Enabled = False
        Me.chkNaoPedirExtra.Location = New System.Drawing.Point(778, 244)
        Me.chkNaoPedirExtra.Name = "chkNaoPedirExtra"
        Me.chkNaoPedirExtra.Size = New System.Drawing.Size(98, 17)
        Me.chkNaoPedirExtra.TabIndex = 119
        Me.chkNaoPedirExtra.Text = "Não pedir extra"
        Me.chkNaoPedirExtra.UseVisualStyleBackColor = True
        '
        'chkTelDesl
        '
        Me.chkTelDesl.AutoSize = True
        Me.chkTelDesl.Enabled = False
        Me.chkTelDesl.Location = New System.Drawing.Point(778, 291)
        Me.chkTelDesl.Name = "chkTelDesl"
        Me.chkTelDesl.Size = New System.Drawing.Size(91, 17)
        Me.chkTelDesl.TabIndex = 118
        Me.chkTelDesl.Text = "Tel Desligado"
        Me.chkTelDesl.UseVisualStyleBackColor = True
        '
        'txtDtCadastro
        '
        Me.txtDtCadastro.Location = New System.Drawing.Point(696, 194)
        Me.txtDtCadastro.Name = "txtDtCadastro"
        Me.txtDtCadastro.ReadOnly = True
        Me.txtDtCadastro.Size = New System.Drawing.Size(105, 20)
        Me.txtDtCadastro.TabIndex = 117
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(693, 175)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(98, 16)
        Me.Label30.TabIndex = 116
        Me.Label30.Text = "Data Cadastro:"
        '
        'txtObs
        '
        Me.txtObs.Location = New System.Drawing.Point(100, 261)
        Me.txtObs.Multiline = True
        Me.txtObs.Name = "txtObs"
        Me.txtObs.ReadOnly = True
        Me.txtObs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtObs.Size = New System.Drawing.Size(657, 44)
        Me.txtObs.TabIndex = 115
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(8, 261)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(86, 16)
        Me.Label25.TabIndex = 114
        Me.Label25.Text = "Observação:"
        '
        'txtReferencia
        '
        Me.txtReferencia.Location = New System.Drawing.Point(102, 226)
        Me.txtReferencia.Name = "txtReferencia"
        Me.txtReferencia.ReadOnly = True
        Me.txtReferencia.Size = New System.Drawing.Size(655, 20)
        Me.txtReferencia.TabIndex = 113
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(8, 227)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(77, 16)
        Me.Label24.TabIndex = 112
        Me.Label24.Text = "Referência:"
        '
        'txtDtNcMp
        '
        Me.txtDtNcMp.Location = New System.Drawing.Point(582, 195)
        Me.txtDtNcMp.Name = "txtDtNcMp"
        Me.txtDtNcMp.ReadOnly = True
        Me.txtDtNcMp.Size = New System.Drawing.Size(105, 20)
        Me.txtDtNcMp.TabIndex = 111
        '
        'txtDtNc
        '
        Me.txtDtNc.Location = New System.Drawing.Point(467, 195)
        Me.txtDtNc.Name = "txtDtNc"
        Me.txtDtNc.ReadOnly = True
        Me.txtDtNc.Size = New System.Drawing.Size(105, 20)
        Me.txtDtNc.TabIndex = 110
        '
        'txtRegiao
        '
        Me.txtRegiao.Location = New System.Drawing.Point(405, 195)
        Me.txtRegiao.Name = "txtRegiao"
        Me.txtRegiao.ReadOnly = True
        Me.txtRegiao.Size = New System.Drawing.Size(53, 20)
        Me.txtRegiao.TabIndex = 109
        '
        'txtCEP
        '
        Me.txtCEP.Location = New System.Drawing.Point(310, 195)
        Me.txtCEP.Name = "txtCEP"
        Me.txtCEP.ReadOnly = True
        Me.txtCEP.Size = New System.Drawing.Size(88, 20)
        Me.txtCEP.TabIndex = 108
        '
        'txtUF
        '
        Me.txtUF.Location = New System.Drawing.Point(259, 195)
        Me.txtUF.Name = "txtUF"
        Me.txtUF.ReadOnly = True
        Me.txtUF.Size = New System.Drawing.Size(41, 20)
        Me.txtUF.TabIndex = 107
        '
        'cboBairro
        '
        Me.cboBairro.Enabled = False
        Me.cboBairro.FormattingEnabled = True
        Me.cboBairro.Location = New System.Drawing.Point(11, 195)
        Me.cboBairro.Name = "cboBairro"
        Me.cboBairro.Size = New System.Drawing.Size(239, 21)
        Me.cboBairro.TabIndex = 106
        '
        'txtDDD
        '
        Me.txtDDD.Location = New System.Drawing.Point(370, 109)
        Me.txtDDD.Name = "txtDDD"
        Me.txtDDD.ReadOnly = True
        Me.txtDDD.Size = New System.Drawing.Size(56, 20)
        Me.txtDDD.TabIndex = 105
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(367, 90)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(41, 16)
        Me.Label34.TabIndex = 104
        Me.Label34.Text = "DDD:"
        '
        'cboCategoriaDetalhe
        '
        Me.cboCategoriaDetalhe.Enabled = False
        Me.cboCategoriaDetalhe.FormattingEnabled = True
        Me.cboCategoriaDetalhe.Location = New System.Drawing.Point(738, 67)
        Me.cboCategoriaDetalhe.Name = "cboCategoriaDetalhe"
        Me.cboCategoriaDetalhe.Size = New System.Drawing.Size(239, 21)
        Me.cboCategoriaDetalhe.TabIndex = 103
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(735, 48)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(70, 16)
        Me.Label28.TabIndex = 102
        Me.Label28.Text = "Categoria:"
        '
        'cboEspecie
        '
        Me.cboEspecie.Enabled = False
        Me.cboEspecie.FormattingEnabled = True
        Me.cboEspecie.Location = New System.Drawing.Point(738, 108)
        Me.cboEspecie.Name = "cboEspecie"
        Me.cboEspecie.Size = New System.Drawing.Size(239, 21)
        Me.cboEspecie.TabIndex = 101
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(735, 89)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(61, 16)
        Me.Label27.TabIndex = 100
        Me.Label27.Text = "Espécie:"
        '
        'cboOperador
        '
        Me.cboOperador.Enabled = False
        Me.cboOperador.FormattingEnabled = True
        Me.cboOperador.Location = New System.Drawing.Point(738, 24)
        Me.cboOperador.Name = "cboOperador"
        Me.cboOperador.Size = New System.Drawing.Size(239, 21)
        Me.cboOperador.TabIndex = 99
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(735, 6)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(69, 16)
        Me.Label3.TabIndex = 98
        Me.Label3.Text = "Operador:"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(577, 174)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(85, 16)
        Me.Label21.TabIndex = 96
        Me.Label21.Text = "Data NC MP:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(462, 174)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(62, 16)
        Me.Label20.TabIndex = 95
        Me.Label20.Text = "Data NC:"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(400, 174)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(56, 16)
        Me.Label19.TabIndex = 94
        Me.Label19.Text = "Região:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(305, 174)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(38, 16)
        Me.Label18.TabIndex = 93
        Me.Label18.Text = "CEP:"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(254, 174)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(29, 16)
        Me.Label17.TabIndex = 92
        Me.Label17.Text = "UF:"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(6, 174)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(47, 16)
        Me.Label16.TabIndex = 91
        Me.Label16.Text = "Bairro:"
        '
        'cboCidade
        '
        Me.cboCidade.Enabled = False
        Me.cboCidade.FormattingEnabled = True
        Me.cboCidade.Location = New System.Drawing.Point(583, 151)
        Me.cboCidade.Name = "cboCidade"
        Me.cboCidade.Size = New System.Drawing.Size(145, 21)
        Me.cboCidade.TabIndex = 90
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(580, 132)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(55, 16)
        Me.Label15.TabIndex = 89
        Me.Label15.Text = "Cidade:"
        '
        'txtTelefone2
        '
        Me.txtTelefone2.Location = New System.Drawing.Point(583, 109)
        Me.txtTelefone2.Name = "txtTelefone2"
        Me.txtTelefone2.ReadOnly = True
        Me.txtTelefone2.Size = New System.Drawing.Size(145, 20)
        Me.txtTelefone2.TabIndex = 88
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(580, 90)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(72, 16)
        Me.Label14.TabIndex = 87
        Me.Label14.Text = "Telefone2:"
        '
        'txtTelefone1
        '
        Me.txtTelefone1.Location = New System.Drawing.Point(432, 109)
        Me.txtTelefone1.Name = "txtTelefone1"
        Me.txtTelefone1.ReadOnly = True
        Me.txtTelefone1.Size = New System.Drawing.Size(140, 20)
        Me.txtTelefone1.TabIndex = 86
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(429, 90)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(72, 16)
        Me.Label13.TabIndex = 85
        Me.Label13.Text = "Telefone1:"
        '
        'txtRg
        '
        Me.txtRg.Location = New System.Drawing.Point(583, 67)
        Me.txtRg.Name = "txtRg"
        Me.txtRg.ReadOnly = True
        Me.txtRg.Size = New System.Drawing.Size(145, 20)
        Me.txtRg.TabIndex = 84
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(580, 48)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(90, 16)
        Me.Label12.TabIndex = 83
        Me.Label12.Text = "Insc. Est./ RG:"
        '
        'txtCnpjCpf
        '
        Me.txtCnpjCpf.Location = New System.Drawing.Point(583, 25)
        Me.txtCnpjCpf.Name = "txtCnpjCpf"
        Me.txtCnpjCpf.ReadOnly = True
        Me.txtCnpjCpf.Size = New System.Drawing.Size(145, 20)
        Me.txtCnpjCpf.TabIndex = 82
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(580, 6)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(76, 16)
        Me.Label11.TabIndex = 81
        Me.Label11.Text = "CNPJ/CPF:"
        '
        'txtEndereco
        '
        Me.txtEndereco.Location = New System.Drawing.Point(9, 151)
        Me.txtEndereco.Name = "txtEndereco"
        Me.txtEndereco.ReadOnly = True
        Me.txtEndereco.Size = New System.Drawing.Size(563, 20)
        Me.txtEndereco.TabIndex = 80
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(6, 132)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(70, 16)
        Me.Label10.TabIndex = 79
        Me.Label10.Text = "Endereço:"
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(9, 109)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.ReadOnly = True
        Me.txtEmail.Size = New System.Drawing.Size(352, 20)
        Me.txtEmail.TabIndex = 78
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(6, 90)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(49, 16)
        Me.Label9.TabIndex = 77
        Me.Label9.Text = "E-mail:"
        '
        'txtNomeCliente2
        '
        Me.txtNomeCliente2.Location = New System.Drawing.Point(155, 67)
        Me.txtNomeCliente2.Name = "txtNomeCliente2"
        Me.txtNomeCliente2.ReadOnly = True
        Me.txtNomeCliente2.Size = New System.Drawing.Size(417, 20)
        Me.txtNomeCliente2.TabIndex = 76
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(152, 48)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 16)
        Me.Label7.TabIndex = 75
        Me.Label7.Text = "Nome 2:"
        '
        'txtNomeCliente1
        '
        Me.txtNomeCliente1.Location = New System.Drawing.Point(155, 25)
        Me.txtNomeCliente1.Name = "txtNomeCliente1"
        Me.txtNomeCliente1.ReadOnly = True
        Me.txtNomeCliente1.Size = New System.Drawing.Size(417, 20)
        Me.txtNomeCliente1.TabIndex = 74
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(152, 6)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(48, 16)
        Me.Label8.TabIndex = 73
        Me.Label8.Text = "Nome:"
        '
        'txtCodBarra
        '
        Me.txtCodBarra.Location = New System.Drawing.Point(9, 67)
        Me.txtCodBarra.Name = "txtCodBarra"
        Me.txtCodBarra.ReadOnly = True
        Me.txtCodBarra.Size = New System.Drawing.Size(136, 20)
        Me.txtCodBarra.TabIndex = 72
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(6, 48)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(75, 16)
        Me.Label6.TabIndex = 71
        Me.Label6.Text = "Cód. Barra:"
        '
        'txtCodigo
        '
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.Location = New System.Drawing.Point(9, 25)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.ReadOnly = True
        Me.txtCodigo.Size = New System.Drawing.Size(136, 20)
        Me.txtCodigo.TabIndex = 70
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(6, 6)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(55, 16)
        Me.Label23.TabIndex = 69
        Me.Label23.Text = "Código:"
        '
        'tabConcluir
        '
        Me.tabConcluir.Controls.Add(Me.Label38)
        Me.tabConcluir.Controls.Add(Me.txtValorConcluir)
        Me.tabConcluir.Controls.Add(Me.Label36)
        Me.tabConcluir.Controls.Add(Me.txtDiaConcluir)
        Me.tabConcluir.Controls.Add(Me.Label37)
        Me.tabConcluir.Controls.Add(Me.txtDDDConcluir)
        Me.tabConcluir.Controls.Add(Me.Label33)
        Me.tabConcluir.Controls.Add(Me.txtTelefoneConcluir)
        Me.tabConcluir.Controls.Add(Me.Label35)
        Me.tabConcluir.Controls.Add(Me.txtNomClienteConcluir)
        Me.tabConcluir.Controls.Add(Me.Label32)
        Me.tabConcluir.Controls.Add(Me.txtObsConcluir)
        Me.tabConcluir.Controls.Add(Me.Label31)
        Me.tabConcluir.Controls.Add(Me.btnNaoContribuir)
        Me.tabConcluir.Controls.Add(Me.btnContribuir)
        Me.tabConcluir.Location = New System.Drawing.Point(4, 22)
        Me.tabConcluir.Name = "tabConcluir"
        Me.tabConcluir.Padding = New System.Windows.Forms.Padding(3)
        Me.tabConcluir.Size = New System.Drawing.Size(991, 311)
        Me.tabConcluir.TabIndex = 2
        Me.tabConcluir.Text = "Revisão"
        Me.tabConcluir.UseVisualStyleBackColor = True
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Location = New System.Drawing.Point(739, 40)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(24, 13)
        Me.Label38.TabIndex = 130
        Me.Label38.Text = "R$ "
        '
        'txtValorConcluir
        '
        Me.txtValorConcluir.Location = New System.Drawing.Point(768, 37)
        Me.txtValorConcluir.Name = "txtValorConcluir"
        Me.txtValorConcluir.Size = New System.Drawing.Size(57, 20)
        Me.txtValorConcluir.TabIndex = 129
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.Location = New System.Drawing.Point(760, 18)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(43, 16)
        Me.Label36.TabIndex = 128
        Me.Label36.Text = "Valor:"
        '
        'txtDiaConcluir
        '
        Me.txtDiaConcluir.Location = New System.Drawing.Point(655, 37)
        Me.txtDiaConcluir.Name = "txtDiaConcluir"
        Me.txtDiaConcluir.Size = New System.Drawing.Size(73, 20)
        Me.txtDiaConcluir.TabIndex = 127
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.Location = New System.Drawing.Point(652, 18)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(76, 16)
        Me.Label37.TabIndex = 126
        Me.Label37.Text = "Melhor Dia:"
        '
        'txtDDDConcluir
        '
        Me.txtDDDConcluir.Location = New System.Drawing.Point(443, 37)
        Me.txtDDDConcluir.Name = "txtDDDConcluir"
        Me.txtDDDConcluir.Size = New System.Drawing.Size(56, 20)
        Me.txtDDDConcluir.TabIndex = 124
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(440, 18)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(41, 16)
        Me.Label33.TabIndex = 123
        Me.Label33.Text = "DDD:"
        '
        'txtTelefoneConcluir
        '
        Me.txtTelefoneConcluir.Location = New System.Drawing.Point(505, 37)
        Me.txtTelefoneConcluir.Name = "txtTelefoneConcluir"
        Me.txtTelefoneConcluir.Size = New System.Drawing.Size(140, 20)
        Me.txtTelefoneConcluir.TabIndex = 122
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(502, 18)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(72, 16)
        Me.Label35.TabIndex = 121
        Me.Label35.Text = "Telefone1:"
        '
        'txtNomClienteConcluir
        '
        Me.txtNomClienteConcluir.Location = New System.Drawing.Point(14, 37)
        Me.txtNomClienteConcluir.Name = "txtNomClienteConcluir"
        Me.txtNomClienteConcluir.Size = New System.Drawing.Size(417, 20)
        Me.txtNomClienteConcluir.TabIndex = 120
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(11, 18)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(48, 16)
        Me.Label32.TabIndex = 119
        Me.Label32.Text = "Nome:"
        '
        'txtObsConcluir
        '
        Me.txtObsConcluir.Location = New System.Drawing.Point(103, 83)
        Me.txtObsConcluir.Multiline = True
        Me.txtObsConcluir.Name = "txtObsConcluir"
        Me.txtObsConcluir.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtObsConcluir.Size = New System.Drawing.Size(717, 81)
        Me.txtObsConcluir.TabIndex = 117
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(11, 83)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(86, 16)
        Me.Label31.TabIndex = 116
        Me.Label31.Text = "Observação:"
        '
        'btnNaoContribuir
        '
        Me.btnNaoContribuir.Location = New System.Drawing.Point(842, 82)
        Me.btnNaoContribuir.Name = "btnNaoContribuir"
        Me.btnNaoContribuir.Size = New System.Drawing.Size(106, 23)
        Me.btnNaoContribuir.TabIndex = 69
        Me.btnNaoContribuir.Text = "Excluir"
        Me.btnNaoContribuir.UseVisualStyleBackColor = True
        '
        'btnContribuir
        '
        Me.btnContribuir.Location = New System.Drawing.Point(842, 35)
        Me.btnContribuir.Name = "btnContribuir"
        Me.btnContribuir.Size = New System.Drawing.Size(106, 23)
        Me.btnContribuir.TabIndex = 68
        Me.btnContribuir.Text = "Atualizar"
        Me.btnContribuir.UseVisualStyleBackColor = True
        '
        'chkHeader
        '
        Me.chkHeader.AutoSize = True
        Me.chkHeader.Location = New System.Drawing.Point(63, 94)
        Me.chkHeader.Name = "chkHeader"
        Me.chkHeader.Size = New System.Drawing.Size(15, 14)
        Me.chkHeader.TabIndex = 91
        Me.chkHeader.UseVisualStyleBackColor = True
        '
        'FrmConsultaFichas
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1145, 708)
        Me.Controls.Add(Me.chkHeader)
        Me.Controls.Add(Me.TabInformacoes)
        Me.Controls.Add(Me.btnRecusa)
        Me.Controls.Add(Me.btnLibera)
        Me.Controls.Add(Me.lblValor)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblTotalFichas)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnSair)
        Me.Controls.Add(Me.grdRemessa)
        Me.Controls.Add(Me.Panel1)
        Me.MinimizeBox = False
        Me.Name = "FrmConsultaFichas"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Consulta Fichas"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.grdRemessa, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabInformacoes.ResumeLayout(False)
        Me.tabHistorico.ResumeLayout(False)
        CType(Me.GrdHistorico, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.tabConcluir.ResumeLayout(False)
        Me.tabConcluir.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnPesquisar As System.Windows.Forms.Button
    Friend WithEvents cboNome As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboCategoria As System.Windows.Forms.ComboBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents grdRemessa As System.Windows.Forms.DataGridView
    Friend WithEvents btnSair As System.Windows.Forms.Button
    Friend WithEvents lblTotalFichas As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblValor As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtNome As System.Windows.Forms.TextBox
    Friend WithEvents txtTelefone As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnLibera As System.Windows.Forms.Button
    Friend WithEvents btnRecusa As System.Windows.Forms.Button
    Friend WithEvents TabInformacoes As System.Windows.Forms.TabControl
    Friend WithEvents tabHistorico As System.Windows.Forms.TabPage
    Friend WithEvents GrdHistorico As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents txtValDoacao As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents txtMelhorDia As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents chkInativo As System.Windows.Forms.CheckBox
    Friend WithEvents chkNaoPedirMensal As System.Windows.Forms.CheckBox
    Friend WithEvents chkNaoPedirExtra As System.Windows.Forms.CheckBox
    Friend WithEvents chkTelDesl As System.Windows.Forms.CheckBox
    Friend WithEvents txtDtCadastro As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents txtObs As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txtReferencia As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtDtNcMp As System.Windows.Forms.TextBox
    Friend WithEvents txtDtNc As System.Windows.Forms.TextBox
    Friend WithEvents txtRegiao As System.Windows.Forms.TextBox
    Friend WithEvents txtCEP As System.Windows.Forms.TextBox
    Friend WithEvents txtUF As System.Windows.Forms.TextBox
    Friend WithEvents cboBairro As System.Windows.Forms.ComboBox
    Friend WithEvents txtDDD As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents cboCategoriaDetalhe As System.Windows.Forms.ComboBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents cboEspecie As System.Windows.Forms.ComboBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents cboOperador As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents cboCidade As System.Windows.Forms.ComboBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtTelefone2 As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtTelefone1 As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtRg As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtCnpjCpf As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtEndereco As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtNomeCliente2 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtNomeCliente1 As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtCodBarra As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents tabConcluir As System.Windows.Forms.TabPage
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents txtValorConcluir As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtDiaConcluir As System.Windows.Forms.TextBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents txtDDDConcluir As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents txtTelefoneConcluir As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents txtNomClienteConcluir As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents txtObsConcluir As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents btnContribuir As System.Windows.Forms.Button
    Friend WithEvents btnNaoContribuir As System.Windows.Forms.Button
    Friend WithEvents cboCidadeLote As System.Windows.Forms.ComboBox
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents chkHeader As System.Windows.Forms.CheckBox
    Friend WithEvents txtdiaPesquisa As System.Windows.Forms.TextBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents cboOperador2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label41 As System.Windows.Forms.Label
End Class
