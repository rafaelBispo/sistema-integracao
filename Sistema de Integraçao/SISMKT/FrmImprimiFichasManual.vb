﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text
Imports Microsoft.Office.Interop
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient


Public Class FrmImprimiFichasManual

#Region "Enum"

    Private Enum ColunasGridFaturamento
        Cod_EAN_Cliente = 0
        Nome_contribuinte = 1
        Data_mov = 2
        Tipo_mov = 3
        Tipo_cobranca = 4
        Cod_Categoria = 5
        DDD = 6
        Telefone1 = 7
        Valor = 8
        Cod_recibo = 9
        Cidade = 10
    End Enum

    Private Enum ColunasGridHistorico
        DDD = 0
        Telefone = 1
        Valor = 2
        categoria = 3
        mesRef = 4
        Situacao = 5
        MotivoSituacao = 6
        Data_Situacao = 7
    End Enum

    Private Enum ColunasGridMovimentacao

        mov_contribuicao_id = 0
        tipo_mov = 1
        tipo_cobranca = 2
        cobrador = 3
        data_mov = 4
        total_fichas = 5
        valor_fichas = 6
    End Enum

    Enum xlsOption
        xlsOpen
    End Enum

#End Region

#Region "variaveis"

    Public telefone As String
    Private lstContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstContribuinteSelecionada As New List(Of CRetorno)
    Private v_ambiente As String
    Private rnContribuinte As RNContribuinte = New RNContribuinte
    Private rnRequisicao As RNRequisicao = New RNRequisicao
    Private contribuinte As CContribuinte = New CContribuinte
    Private lstrequisicao As New SortableBindingList(Of CContribuinte)
    Private lstrequisicaoSelecionada As New List(Of CContribuinte)
    Private lstMovimentacao As New SortableBindingList(Of CRequisicao)
    Private lstRetornoContribuintes As New SortableBindingList(Of CContribuinte)
    Public mNivelAcesso As Integer
    Public mUsuario As String
    Public mCodUsuario As Integer
    Public atualizou As Boolean
    Public bindingSourceContribuite As BindingSource = New BindingSource
    Public bindingSourceMovimentacao As BindingSource = New BindingSource


    Public Property NivelAcesso() As Integer
        Get
            Return mNivelAcesso
        End Get
        Set(ByVal value As Integer)
            mNivelAcesso = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return mUsuario
        End Get
        Set(ByVal value As String)
            mUsuario = value
        End Set
    End Property

    Public Property CodUsuario() As Integer
        Get
            Return mCodUsuario
        End Get
        Set(ByVal value As Integer)
            mCodUsuario = value
        End Set
    End Property

#End Region



#Region "Metodos"



    Private Sub carregaGrid()

        Dim objContribuintes As CContribuinte = New CContribuinte()

        Dim bindingSourceApolice As BindingSource = New BindingSource

        Try



            lstContribuintes = New SortableBindingList(Of CContribuinte)(rnContribuinte.CarregarGridOi(objContribuintes, "Remessa"))

            If (lstContribuintes.Count > 0) Then

                bindingSourceApolice.DataSource = lstContribuintes

                grdFaturamento.DataSource = bindingSourceApolice.DataSource
                'grdRemessa.Sort(grdRemessa.Columns(ColunasGridRemessa.Nome), ListSortDirection.Ascending)
                'lblNumRegistros.Text = lstApolices.Count


            End If


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CriarColunasGrid()


        Try

            grdFaturamento.DataSource = Nothing
            grdFaturamento.Columns.Clear()

            grdFaturamento.AutoGenerateColumns = False

            grdFaturamento.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            'Dim checkboxColumn As New DataGridViewCheckBoxColumn
            'checkboxColumn.Width = 32
            'checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_EAN_Cliente"
            column.HeaderText = "Cód. EAN Cliente"
            column.Width = 80
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Nome_contribuinte"
            column.HeaderText = "Nome contribuinte"
            column.Width = 300
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Data_mov"
            column.HeaderText = "Data Movimentação"
            column.Width = 80
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Tipo_mov"
            column.HeaderText = "Tipo Movimentação"
            column.Width = 80
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Tipo_cobranca"
            column.HeaderText = "Tipo Cobranca"
            column.Width = 80
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_Categoria"
            column.HeaderText = "Cód Categoria"
            column.Width = 80
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)


            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "valor"
            column.HeaderText = "Valor"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            grdFaturamento.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_recibo"
            column.HeaderText = "Cód. Recibo"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            grdFaturamento.Columns.Add(column)

            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cidade"
            column.HeaderText = "Cidade"
            column.Width = 80
            column.ReadOnly = True
            column.Visible = True
            grdFaturamento.Columns.Add(column)




        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

#End Region


    Private Sub CarregaComboCidades(ByVal cod_cidade As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCidadeLote.DataSource = Nothing
            cboCidadeLote.Items.Clear()
            cboCidadeLote.DisplayMember = "Nome_cidade"
            cboCidadeLote.ValueMember = "Cod_Cidade"
            cboCidadeLote.DataSource = rn_generico.BuscarCidades(0)

            If cboCidadeLote.Items.Count = 0 Then
                cboCidadeLote.SelectedIndex = 0
            ElseIf cboCidadeLote.Items.Count > 0 And cod_cidade <> 0 Then

                For i As Integer = 0 To cboCidadeLote.Items.Count - 1
                    For Each linha As CGenerico In cboCidadeLote.DataSource
                        If linha.Cod_Cidade = cod_cidade Then
                            cboCidadeLote.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next

            Else

                cboCidadeLote.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub


    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""
        Dim cidade As String = ""
        Dim dia As Integer = 0
        Dim operador_id As Integer = 0
        Dim rn As RNRequisicao = New RNRequisicao

        Try

            Me.Cursor = Cursors.WaitCursor


            cidade = cboCidadeLote.Text
            dia = txtDia.Text
            If cboOperador2.SelectedValue <> 0 Then
                operador_id = cboOperador2.SelectedValue
            Else
                operador_id = 0
            End If

            carregaFichasFaturamento(cidade, dia, operador_id)

            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try

    End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Close()
    End Sub

    Private Sub FrmConsultaContribuinte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        CriarColunasGrid()
        CarregaComboOperador(0)
        CarregaComboCidades(0)

    End Sub


    Private Sub FrmLiberaFaturamento_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""

        Try


        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub carregaFichasFaturamento(ByVal cidade As String, ByVal dia As Integer, ByVal operador_id As Integer)
        Dim objRetorno As CRetorno = New CRetorno()
        Dim dt As DataTable = New DataTable
        Dim ds As DataSet = New DataSet
        Dim dr As DataRow
        Dim valor As Single = 0
        Dim strValor As String = ""
        Dim totalValorFichas As Decimal = 0


        Try

            grdFaturamento.DataSource = Nothing

            ds = rnRequisicao.CarregaFichasImprimirManual(cidade, dia, operador_id)

            If Not ds Is Nothing Then
                If (ds.Tables(0).Rows.Count > 0) Then
                    If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                        dt = ds.Tables(0)
                    End If
                End If

                grdFaturamento.DataSource = dt


                If (grdFaturamento.Rows.Count = 0) Then

                    grdFaturamento.DataSource = Nothing
                    MsgBox("Não existe registros a serem processados!")

                Else

                    lblTotalFichas.Text = grdFaturamento.Rows.Count - 1

                    For Each col As DataGridViewRow In grdFaturamento.Rows

                        totalValorFichas = totalValorFichas + col.Cells(6).Value

                    Next

                    strValor = valor.ToString("C")
                    valor = totalValorFichas
                    lblValor.Text = valor.ToString("R$ #,###.00")

                End If
            Else
                grdFaturamento.DataSource = Nothing
                MsgBox("Não existe registros a serem processados!")
            End If

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub



    Private Sub CarregaComboOperador(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboOperador2.DataSource = Nothing
            cboOperador2.Items.Clear()
            cboOperador2.DisplayMember = "Nome_Operador"
            cboOperador2.ValueMember = "Cod_Operador"
            cboOperador2.DataSource = rn_generico.BuscarOperador(0)

            If cboOperador2.Items.Count > 0 And cod_operador = 0 Then
                cboOperador2.SelectedIndex = 0
            ElseIf cboOperador2.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperador2.Items.Count - 1
                    For Each linha As CGenerico In cboOperador2.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperador2.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperador2.SelectedIndex = 1
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub btnImprimirRecibo_Click(sender As System.Object, e As System.EventArgs) Handles btnImprimirRecibo.Click
        Dim strReportName As String
        Dim frm As FrmImprimiRelatorios = New FrmImprimiRelatorios

        Dim crtableLogoninfos As New TableLogOnInfos()
        Dim crtableLogoninfo As New TableLogOnInfo()
        Dim crConnectionInfo As New ConnectionInfo()
        Dim ds As DataSet

        Dim ultimo_recibo As Long = 0
        Dim ClienteSelecionado As String
        Dim Cod_cobradorSelecionado As String
        Dim CobradorSelecionado As String
        Dim cod_mov_contribuicao As Long
        Dim totalFIchas As Long = 0
        Dim totalValorFichas As Decimal = 0
        Dim dt As DataTable = New DataTable
        Dim valor As Single = 0
        Dim strValor As String = ""
        Dim CodReciboExistente As String = ""
        Dim valorModificado As Decimal = 0


        Try



            For Each linha As DataGridViewRow In grdFaturamento.Rows

                ClienteSelecionado = linha.Cells(0).Value
                Cod_cobradorSelecionado = cboOperador2.SelectedValue
                CobradorSelecionado = cboOperador2.Text
                CodReciboExistente = linha.Cells(5).Value
                valorModificado = linha.Cells(7).Value

                If ClienteSelecionado <> Nothing Then
                    If CodReciboExistente > 1 Then
                        rnRequisicao.GravaTabelaLiberacao(cod_mov_contribuicao, CodReciboExistente, ClienteSelecionado, valorModificado)
                    Else
                        rnRequisicao.GravaTabelaLiberacao(cod_mov_contribuicao, ultimo_recibo, ClienteSelecionado, valorModificado)
                        ultimo_recibo = ultimo_recibo + 1
                        linha.Cells(7).Value = ultimo_recibo
                    End If
                End If
            Next




            ' carrega o relatorio desejado
            strReportName = "ReciboLista1"

            'define o caminho e nome do relatorio

            Dim strReportPath As String = "C:\Digital\SISINTEGRACAO\RPT" & "\" & strReportName & ".rpt"
            'Dim strReportPath As String = "C:\GIT\Sistema de Integraçao\SISINT001\RPT" & "\" & strReportName & ".rpt"
            '
            'verifiqa se o arquivo existe
            If Not IO.File.Exists(strReportPath) Then
                MsgBox("Relatorio não localizado :" & vbCrLf & strReportPath, MsgBoxStyle.Critical)
            End If
            '
            'instancia o relaorio e carrega
            Dim CR As New ReportDocument
            CR.Load(strReportPath)
            CR.SetDatabaseLogon("sa", "xucrutis", "DESKTOP-36CEDMG\SISINTEGRACAO", "controle_db")
            'CR.SetDatabaseLogon("sa", "xucrutis", "AACI-SERV\SISINTEGRACAO", "controle_db")
            ' CR.SetDatabaseLogon("sa", "Xucrutis2017", "AACI-SERV\SISINTEGRACAO)", "controle_db", True)
            'CR.VerifyDatabase()

            Dim dao As RequisicaoDAO = New RequisicaoDAO
            ds = dao.GeraRecibo()

            Dim dtCliente As DataTable = ds.Tables(0)
            Dim rptCliente As New ReportDocument

            rptCliente.Load(strReportPath)
            'Dim rptCliente As New ReciboLista1 ''nome do relatorio

            'rptCliente.SetDatabaseLogon("sa", "Xucrutis2017", "NOVAJFDSK030\SISINTEGRACAO", "controle_db")
            rptCliente.SetDatabaseLogon("sa", "xucrutis", "AACI-SERV\SISINTEGRACAO", "controle_db")

            rptCliente.SetDataSource(dtCliente)

            'frm.CrystalReportViewer.ReportSource = rptCliente
            'frm.CrystalReportViewer.RefreshReport()
            frm.Show()


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub


End Class