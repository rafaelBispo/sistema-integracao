﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001

Public Class FrmPrincipal_MKT

#Region "Variaveis"

    Dim v_ambiente As String
    Dim mNivelAcesso As Integer
    Dim mUsuario As String
    Dim mCodUsuario As Integer


    Public Property NivelAcesso() As Integer
        Get
            Return mNivelAcesso
        End Get
        Set(ByVal value As Integer)
            mNivelAcesso = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return mUsuario
        End Get
        Set(ByVal value As String)
            mUsuario = value
        End Set
    End Property

    Public Property CodUsuario() As Integer
        Get
            Return mCodUsuario
        End Get
        Set(ByVal value As Integer)
            mCodUsuario = value
        End Set
    End Property

#End Region

    Private Sub instanciaVariaviesGlobais()

        Dim mensagem As New Util.ConnectionHelper(System.Reflection.Assembly.GetExecutingAssembly)
        Dim v_ambiente As String

        v_ambiente = mensagem.RetornaMenssage()

    End Sub


    Private Sub FrmPrincipal_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim mensagem As New Util.ConnectionHelper(System.Reflection.Assembly.GetExecutingAssembly)
        Dim rn As RNContribuinte = New RNContribuinte

        If NivelAcesso = 6 Then
            btnGerencial.Visible = False
            SuporteToolStripMenuItem.Visible = False
            RelatôriosToolStripMenuItem.Visible = False
            AbrirChamadoToolStripMenuItem.Visible = False
            UsuarioToolStripMenuItem.Visible = False
            GerêncialToolStripMenuItem.Visible = False
            btnFaturamento.Visible = False
            btnBaixa.Visible = False
            btnBordero.Visible = False
            btnGerencial.Visible = False
            btnListaContribuintes.Visible = False
            btnSupervisao.Visible = False
            btnCosultaFichas.Visible = False
        ElseIf NivelAcesso = 1 Then
            btnGerencial.Visible = True
            SuporteToolStripMenuItem.Visible = True
            RelatôriosToolStripMenuItem.Visible = True
            UsuarioToolStripMenuItem.Visible = True
            AbrirChamadoToolStripMenuItem.Visible = True
            GerêncialToolStripMenuItem.Visible = True
            btnFaturamento.Visible = True
            btnBaixa.Visible = True
            btnBordero.Visible = True
            btnGerencial.Visible = True
            btnListaContribuintes.Visible = True
            btnSupervisao.Visible = True
            btnCosultaFichas.Visible = True
        End If


        stripUsuario.Text = "USUARIO: " + mUsuario


        instanciaVariaviesGlobais()
        CarregaComboCategorias(0)
        ' carregaValoreAtuais()


        '    rn.AtualizaTabelaContribuinte()

    End Sub

    Private Sub btnContribuinte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmContribuinte.Show()

    End Sub

    Private Sub btnRemessa_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FrmGerarRemessa.Show()


    End Sub

    Private Sub btnRetorno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FrmRetorno.Show()
    End Sub

    Private Sub btnConsulta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FrmConsultaContribuinte.Show()
    End Sub


    Private Sub btnIncluirManual_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FrmInclusaoManual.Show()
    End Sub

    Private Sub btnConsCat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FrmConsultaUltimaMovimentacao.Usuario = Usuario
        FrmConsultaUltimaMovimentacao.CodUsuario = CodUsuario
        FrmConsultaUltimaMovimentacao.Show()
    End Sub

    Private Sub btnGerencial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGerencial.Click
        FrmRequisicaoManual.Show()
    End Sub

    Private Sub FrmPrincipal_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Application.Exit()
    End Sub

    Private Sub btnConsultaRequisicoes_Click(sender As System.Object, e As System.EventArgs)
        Dim Frm = New FrmConsultaRequisicaoRealizada


        Frm.Usuario = Usuario
        Frm.CodUsuario = CodUsuario
        Frm.Show()
    End Sub

    Private Sub btnRecuperacao_Click(sender As System.Object, e As System.EventArgs)
        FrmRecuperacao.Show()
    End Sub

    Private Sub btnCaixa_Click(sender As System.Object, e As System.EventArgs)
        FrmAcertoCaixa.Show()
    End Sub

    Private Sub btnAbrirChamado_Click(sender As System.Object, e As System.EventArgs) Handles btnAbrirChamado.Click
        Dim Frm = New FrmAbrirChamado
        Frm.Usuario = Usuario
        Frm.CodUsuario = CodUsuario
        Frm.Show()
    End Sub

    Private Sub btnFaturamento_Click(sender As System.Object, e As System.EventArgs) Handles btnFaturamento.Click
        FrmLiberaFaturamento.Show()
    End Sub

    Private Sub CadastroContribuinteToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CadastroContribuinteToolStripMenuItem.Click
        frmContribuinte.Show()
    End Sub

    Private Sub TrocarDeUsuárioToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles TrocarDeUsuárioToolStripMenuItem.Click
        LoginForm_MKT.Show()
        Me.Close()
    End Sub

    Private Sub SuporteToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SuporteToolStripMenuItem.Click
        Dim resultado As Boolean
        Dim rn As RNContribuinte = New RNContribuinte
        Try

            MsgBox("O processamento requerido pode ocasionar inconsistência no banco de dados, precisamente na tabela de clientes, deseja continuar?", MsgBoxStyle.YesNo, "Atenção")
            If Windows.Forms.DialogResult.Yes Then
                resultado = rn.AtualizaTabelaCliente()
            End If

            If resultado = True Then

                MsgBox("Base atualizada com sucesso!")

            Else

                MsgBox("Ocorreu um erro na atualização da base, entre em contato com o responsável!")

            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub AbrirChamadoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AbrirChamadoToolStripMenuItem.Click
        FrmAbrirChamado.Show()
    End Sub

    Private Sub btnBordero_Click(sender As System.Object, e As System.EventArgs) Handles btnBordero.Click
        FrmBordero.Show()
    End Sub

    Private Sub btnBaixa_Click(sender As System.Object, e As System.EventArgs) Handles btnBaixa.Click
        FrmBaixa.Show()
    End Sub

    Private Sub LiberaFaturamentoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles LiberaFaturamentoToolStripMenuItem.Click
        FrmLiberaFaturamento.Show()
    End Sub

    Private Sub BorderôToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles BorderôToolStripMenuItem.Click
        FrmBordero.Show()
    End Sub

    Private Sub BaixaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles BaixaToolStripMenuItem.Click
        FrmBaixa.Show()
    End Sub

    Private Sub GerêncialToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles GerêncialToolStripMenuItem1.Click
        FrmRequisicaoManual.Show()
    End Sub

  
    Private Sub btnListaContribuintes_Click(sender As System.Object, e As System.EventArgs) Handles btnListaContribuintes.Click
        FrmListaContribuintes.Show()
    End Sub

    Private Sub btnSupervisao_Click(sender As System.Object, e As System.EventArgs) Handles btnSupervisao.Click
        FrmSupervisao.Show()
    End Sub

    Private Sub btnCosultaFichas_Click(sender As System.Object, e As System.EventArgs) Handles btnCosultaFichas.Click
        Dim frm As FrmConsultaFichas = New FrmConsultaFichas()
        frm.Show()
    End Sub


   
    Private Sub OperadorSintéticoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OperadorSintéticoToolStripMenuItem.Click
        Dim frm As FrmFiltroRelatorios = New FrmFiltroRelatorios()

        frm.Relatorio = "HistOperadoraSint"
        'frm.param1 = "data_mov"
        'frm.param2 = "data_mov"
        frm.Show()


    End Sub


    Private Sub HistóricoOperadoraSintéticoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles HistóricoOperadoraSintéticoToolStripMenuItem.Click
        Dim frm As FrmFiltroRelatorios = New FrmFiltroRelatorios()

        frm.Relatorio = "SitRecOpera"
        'frm.param1 = "data_mov"
        'frm.param2 = "data_mov"
        frm.Show()

    End Sub

    Private Sub HistóricoOperadorAnaliticoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles HistóricoOperadorAnaliticoToolStripMenuItem.Click
        Dim frm As FrmFiltroRelatorios = New FrmFiltroRelatorios()
        frm.Relatorio = "HistOperadoraAnalitico"
        frm.Show()
    End Sub

    Private Sub AnaliticoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AnaliticoToolStripMenuItem.Click
        Dim frm As FrmRelatorios = New FrmRelatorios()

        frm.NomeRelatorio = "FichasEmAbertoAnalitico"
        frm.Show()
    End Sub

    Private Sub SinteticoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SinteticoToolStripMenuItem.Click
        Dim frm As FrmRelatorios = New FrmRelatorios()

        frm.NomeRelatorio = "FichasEmAbertoSintetico"
        frm.Show()
    End Sub

    Private Sub AnaliticoPorCidadeToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AnaliticoPorCidadeToolStripMenuItem.Click
        Dim frm As FrmRelatorios = New FrmRelatorios()

        frm.NomeRelatorio = "FichasEmAbertoAnaliticoPorCidade"
        frm.Show()
    End Sub

    Private Sub ExcluirContribuinteToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ExcluirContribuinteToolStripMenuItem.Click
        frmTelefoneExclusao.Show()
    End Sub

    Private Sub AnaliticoToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles AnaliticoToolStripMenuItem1.Click
        Dim frm As FrmRelatorios = New FrmRelatorios()

        frm.NomeRelatorio = "FichasAbertasAnalitico"
        frm.Show()
    End Sub

    Private Sub AnaliticoPorCidadeToolStripMenuItem1_Click(sender As System.Object, e As System.EventArgs) Handles AnaliticoPorCidadeToolStripMenuItem1.Click
        Dim frm As FrmRelatorios = New FrmRelatorios()

        frm.NomeRelatorio = "FichasAbertasAnaliticoPorCidade"
        frm.Show()
    End Sub

    Private Sub SintêticoToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SintêticoToolStripMenuItem.Click
        Dim frm As FrmRelatorios = New FrmRelatorios()

        frm.NomeRelatorio = "FichasAbertasSintetico"
        frm.Show()
    End Sub

    Private Sub UsuarioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UsuarioToolStripMenuItem.Click
        Dim frm As FrmCadUsuario = New FrmCadUsuario()
        frm.Show()
    End Sub

    Private Sub AnaliseExclusãoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AnaliseExclusãoToolStripMenuItem.Click
        FrmAnaliseExclusaoContribuinte.Show()
    End Sub



#Region "Resumo"

    Private Sub CarregaComboCategorias(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCategoria.DataSource = Nothing
            cboCategoria.Items.Clear()
            cboCategoria.DisplayMember = "Nome_Categoria"
            cboCategoria.ValueMember = "Cod_Categoria"
            cboCategoria.DataSource = rn_generico.BuscarCategoriaPorOperador(CodUsuario)

            If cboCategoria.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoria.SelectedIndex = 0
            ElseIf cboCategoria.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoria.Items.Count - 1
                    For Each linha As CGenerico In cboCategoria.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoria.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoria.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""
        Dim tipoPesquisa As Integer = 0

        Try
            Me.Cursor = Cursors.WaitCursor

            carregaGridRequisicoesManualPorPeriodo(dtInicial.Text, dtFinal.Text, 0, cboCategoria.SelectedValue)


            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub carregaGridRequisicoesManualPorPeriodo(ByVal dtInicial As Date, ByVal dtFinal As Date, ByVal tipoAceite As Integer, ByVal categoria As Integer)

        Dim objGenerico As CGenerico = New CGenerico()
        Dim totalValorFichas As Integer
        Dim valor As Single
        Dim strValor As String
        Dim rnGenerico As New RNGenerico
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable

        Try

            ds = rnGenerico.CarregarRequisicaoManualPorPeriodoDataCategoria(dtInicial, dtFinal, tipoAceite, categoria, CodUsuario)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If

            For Each row As DataRow In dt.Rows

                totalValorFichas = row.Item(0)
                lblSolicitacaoAceita.Text = row.Item(1)
                lblSolicitacaoFila.Text = row.Item(2)


            Next

            strValor = valor.ToString("C")
            valor = totalValorFichas
            lblValor.Text = valor.ToString("R$ #,###.00")



        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub btnAtualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAtualizar.Click
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""
        Dim tipoPesquisa As Integer = 0

        Try
            Me.Cursor = Cursors.WaitCursor

            carregaValoreAtuais()


            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub carregaValoreAtuais()

        Dim objGenerico As CGenerico = New CGenerico()
        Dim totalValorFichasMensal As Decimal
        Dim totalValorFichasDiario As Decimal
        Dim valor As Single
        Dim strValor As String
        Dim rnGenerico As New RNGenerico
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable

        Try

            'CARREGA OS VALORES MENSAIS

            ds = rnGenerico.CarregarManualValoresAtuaisMes(CodUsuario)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If

            For Each row As DataRow In dt.Rows

                totalValorFichasMensal = row.Item(0)
                lblSolicitacaoAceitaMensal.Text = row.Item(1)
                lblSolicitacaoFilaMensal.Text = row.Item(2)


            Next

            strValor = valor.ToString("C")
            valor = totalValorFichasMensal
            lblValorMensal.Text = valor.ToString("R$ #,###.00")

            ds.Clear()
            dt.Clear()

            'CARREGA OS VALORES DIARIOS

            ds = rnGenerico.CarregarManualValoresAtuaisDia(CodUsuario)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If

            For Each row As DataRow In dt.Rows

                totalValorFichasDiario = row.Item(0)
                lblSolicitacaoAceitaDiario.Text = row.Item(1)
                lblSolicitacaoFilaDiario.Text = row.Item(2)


            Next

            strValor = valor.ToString("C")
            valor = totalValorFichasDiario
            lblValorDiario.Text = valor.ToString("R$ #,###.00")



        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub


#End Region

    Private Sub FichasNãoImpressasToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles FichasNãoImpressasToolStripMenuItem.Click
        Dim frm As FrmRelatorios = New FrmRelatorios()

        frm.NomeRelatorio = "FichasNaoImpressasOperadorCidade"
        frm.Show()
    End Sub

End Class
