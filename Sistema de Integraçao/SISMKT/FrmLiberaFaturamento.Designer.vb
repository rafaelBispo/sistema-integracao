﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmLiberaFaturamento
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.chkMensais = New System.Windows.Forms.CheckBox()
        Me.cboOperador2 = New System.Windows.Forms.ComboBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.grdMovimentacao = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnNovo = New System.Windows.Forms.Button()
        Me.btnConsultarLote = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtConsultaLote = New System.Windows.Forms.TextBox()
        Me.btnSalvarLote = New System.Windows.Forms.Button()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtLote = New System.Windows.Forms.TextBox()
        Me.cboOperador = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtDia = New System.Windows.Forms.TextBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.cboCidadeLote = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnPesquisar = New System.Windows.Forms.Button()
        Me.grdFaturamento = New System.Windows.Forms.DataGridView()
        Me.btnSair = New System.Windows.Forms.Button()
        Me.lblTotalFichas = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblValor = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TabInformacoes = New System.Windows.Forms.TabControl()
        Me.tabHistorico = New System.Windows.Forms.TabPage()
        Me.GrdHistorico = New System.Windows.Forms.DataGridView()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.txtValor = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtCod_Ean = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboCidade = New System.Windows.Forms.ComboBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.btnSalvar = New System.Windows.Forms.Button()
        Me.txtDDD = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtTelefone2 = New System.Windows.Forms.TextBox()
        Me.txtNome2 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.cboCategoria2 = New System.Windows.Forms.ComboBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.rdExtornar = New System.Windows.Forms.RadioButton()
        Me.rdIncluir = New System.Windows.Forms.RadioButton()
        Me.txtLoteCodCliente = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnImprimirRecibo = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnImprimirReciboRetido = New System.Windows.Forms.Button()
        Me.btnReImprimir = New System.Windows.Forms.Button()
        Me.btnHistorico = New System.Windows.Forms.Button()
        Me.chkMensalEspecial = New System.Windows.Forms.CheckBox()
        Me.Panel1.SuspendLayout()
        CType(Me.grdMovimentacao, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.grdFaturamento, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabInformacoes.SuspendLayout()
        Me.tabHistorico.SuspendLayout()
        CType(Me.GrdHistorico, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.chkMensalEspecial)
        Me.Panel1.Controls.Add(Me.chkMensais)
        Me.Panel1.Controls.Add(Me.cboOperador2)
        Me.Panel1.Controls.Add(Me.Label40)
        Me.Panel1.Controls.Add(Me.grdMovimentacao)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.cboOperador)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.txtDia)
        Me.Panel1.Controls.Add(Me.Label32)
        Me.Panel1.Controls.Add(Me.cboCidadeLote)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.btnPesquisar)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1194, 133)
        Me.Panel1.TabIndex = 1
        '
        'chkMensais
        '
        Me.chkMensais.AutoSize = True
        Me.chkMensais.Location = New System.Drawing.Point(254, 52)
        Me.chkMensais.Name = "chkMensais"
        Me.chkMensais.Size = New System.Drawing.Size(103, 17)
        Me.chkMensais.TabIndex = 89
        Me.chkMensais.Text = "Imprimir Mensais"
        Me.chkMensais.UseVisualStyleBackColor = True
        '
        'cboOperador2
        '
        Me.cboOperador2.FormattingEnabled = True
        Me.cboOperador2.Location = New System.Drawing.Point(207, 25)
        Me.cboOperador2.Name = "cboOperador2"
        Me.cboOperador2.Size = New System.Drawing.Size(176, 21)
        Me.cboOperador2.TabIndex = 87
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.Location = New System.Drawing.Point(204, 7)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(77, 16)
        Me.Label40.TabIndex = 88
        Me.Label40.Text = "Operadora:"
        '
        'grdMovimentacao
        '
        Me.grdMovimentacao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdMovimentacao.Location = New System.Drawing.Point(702, 7)
        Me.grdMovimentacao.Name = "grdMovimentacao"
        Me.grdMovimentacao.Size = New System.Drawing.Size(481, 121)
        Me.grdMovimentacao.TabIndex = 79
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnNovo)
        Me.GroupBox1.Controls.Add(Me.btnConsultarLote)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.txtConsultaLote)
        Me.GroupBox1.Controls.Add(Me.btnSalvarLote)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.txtLote)
        Me.GroupBox1.Location = New System.Drawing.Point(425, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(271, 125)
        Me.GroupBox1.TabIndex = 78
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Lote"
        '
        'btnNovo
        '
        Me.btnNovo.Location = New System.Drawing.Point(9, 69)
        Me.btnNovo.Name = "btnNovo"
        Me.btnNovo.Size = New System.Drawing.Size(75, 23)
        Me.btnNovo.TabIndex = 82
        Me.btnNovo.Text = "Novo"
        Me.btnNovo.UseVisualStyleBackColor = True
        '
        'btnConsultarLote
        '
        Me.btnConsultarLote.Location = New System.Drawing.Point(137, 77)
        Me.btnConsultarLote.Name = "btnConsultarLote"
        Me.btnConsultarLote.Size = New System.Drawing.Size(96, 23)
        Me.btnConsultarLote.TabIndex = 81
        Me.btnConsultarLote.Text = "Consultar Lote"
        Me.btnConsultarLote.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(134, 16)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(99, 16)
        Me.Label13.TabIndex = 79
        Me.Label13.Text = "Consultar Lote :"
        '
        'txtConsultaLote
        '
        Me.txtConsultaLote.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConsultaLote.Location = New System.Drawing.Point(137, 38)
        Me.txtConsultaLote.Name = "txtConsultaLote"
        Me.txtConsultaLote.Size = New System.Drawing.Size(105, 22)
        Me.txtConsultaLote.TabIndex = 0
        '
        'btnSalvarLote
        '
        Me.btnSalvarLote.Location = New System.Drawing.Point(9, 96)
        Me.btnSalvarLote.Name = "btnSalvarLote"
        Me.btnSalvarLote.Size = New System.Drawing.Size(75, 23)
        Me.btnSalvarLote.TabIndex = 78
        Me.btnSalvarLote.Text = "Salvar"
        Me.btnSalvarLote.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(6, 16)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(40, 16)
        Me.Label12.TabIndex = 76
        Me.Label12.Text = "Lote :"
        '
        'txtLote
        '
        Me.txtLote.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtLote.Location = New System.Drawing.Point(9, 37)
        Me.txtLote.Name = "txtLote"
        Me.txtLote.ReadOnly = True
        Me.txtLote.Size = New System.Drawing.Size(105, 22)
        Me.txtLote.TabIndex = 77
        '
        'cboOperador
        '
        Me.cboOperador.FormattingEnabled = True
        Me.cboOperador.Location = New System.Drawing.Point(9, 25)
        Me.cboOperador.Name = "cboOperador"
        Me.cboOperador.Size = New System.Drawing.Size(187, 21)
        Me.cboOperador.TabIndex = 75
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 7)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(92, 16)
        Me.Label4.TabIndex = 74
        Me.Label4.Text = "Responsável:"
        '
        'txtDia
        '
        Me.txtDia.Location = New System.Drawing.Point(216, 101)
        Me.txtDia.Name = "txtDia"
        Me.txtDia.Size = New System.Drawing.Size(49, 20)
        Me.txtDia.TabIndex = 73
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(213, 82)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(35, 16)
        Me.Label32.TabIndex = 72
        Me.Label32.Text = "Dia :"
        '
        'cboCidadeLote
        '
        Me.cboCidadeLote.FormattingEnabled = True
        Me.cboCidadeLote.Location = New System.Drawing.Point(9, 101)
        Me.cboCidadeLote.Name = "cboCidadeLote"
        Me.cboCidadeLote.Size = New System.Drawing.Size(187, 21)
        Me.cboCidadeLote.TabIndex = 71
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 82)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 16)
        Me.Label1.TabIndex = 70
        Me.Label1.Text = "Cidade:"
        '
        'btnPesquisar
        '
        Me.btnPesquisar.Location = New System.Drawing.Point(281, 99)
        Me.btnPesquisar.Name = "btnPesquisar"
        Me.btnPesquisar.Size = New System.Drawing.Size(75, 23)
        Me.btnPesquisar.TabIndex = 67
        Me.btnPesquisar.Text = "Pesquisar"
        Me.btnPesquisar.UseVisualStyleBackColor = True
        '
        'grdFaturamento
        '
        Me.grdFaturamento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdFaturamento.Location = New System.Drawing.Point(12, 151)
        Me.grdFaturamento.Name = "grdFaturamento"
        Me.grdFaturamento.Size = New System.Drawing.Size(972, 276)
        Me.grdFaturamento.TabIndex = 2
        '
        'btnSair
        '
        Me.btnSair.Location = New System.Drawing.Point(994, 637)
        Me.btnSair.Name = "btnSair"
        Me.btnSair.Size = New System.Drawing.Size(75, 23)
        Me.btnSair.TabIndex = 3
        Me.btnSair.Text = "Sair"
        Me.btnSair.UseVisualStyleBackColor = True
        '
        'lblTotalFichas
        '
        Me.lblTotalFichas.AutoSize = True
        Me.lblTotalFichas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalFichas.Location = New System.Drawing.Point(10, 143)
        Me.lblTotalFichas.Name = "lblTotalFichas"
        Me.lblTotalFichas.Size = New System.Drawing.Size(16, 16)
        Me.lblTotalFichas.TabIndex = 80
        Me.lblTotalFichas.Text = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 115)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(120, 16)
        Me.Label2.TabIndex = 79
        Me.Label2.Text = "Total de Fichas:"
        '
        'lblValor
        '
        Me.lblValor.AutoSize = True
        Me.lblValor.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValor.Location = New System.Drawing.Point(10, 212)
        Me.lblValor.Name = "lblValor"
        Me.lblValor.Size = New System.Drawing.Size(16, 16)
        Me.lblValor.TabIndex = 82
        Me.lblValor.Text = "0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(10, 175)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(93, 16)
        Me.Label5.TabIndex = 81
        Me.Label5.Text = "Valor Total :"
        '
        'TabInformacoes
        '
        Me.TabInformacoes.Controls.Add(Me.tabHistorico)
        Me.TabInformacoes.Controls.Add(Me.TabPage2)
        Me.TabInformacoes.Location = New System.Drawing.Point(12, 441)
        Me.TabInformacoes.Name = "TabInformacoes"
        Me.TabInformacoes.SelectedIndex = 0
        Me.TabInformacoes.Size = New System.Drawing.Size(976, 223)
        Me.TabInformacoes.TabIndex = 83
        '
        'tabHistorico
        '
        Me.tabHistorico.Controls.Add(Me.GrdHistorico)
        Me.tabHistorico.Location = New System.Drawing.Point(4, 22)
        Me.tabHistorico.Name = "tabHistorico"
        Me.tabHistorico.Padding = New System.Windows.Forms.Padding(3)
        Me.tabHistorico.Size = New System.Drawing.Size(968, 197)
        Me.tabHistorico.TabIndex = 0
        Me.tabHistorico.Text = "Histórico"
        Me.tabHistorico.UseVisualStyleBackColor = True
        '
        'GrdHistorico
        '
        Me.GrdHistorico.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GrdHistorico.Location = New System.Drawing.Point(6, 6)
        Me.GrdHistorico.Name = "GrdHistorico"
        Me.GrdHistorico.Size = New System.Drawing.Size(956, 180)
        Me.GrdHistorico.TabIndex = 3
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.txtValor)
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Controls.Add(Me.txtCod_Ean)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.cboCidade)
        Me.TabPage2.Controls.Add(Me.Label15)
        Me.TabPage2.Controls.Add(Me.btnSalvar)
        Me.TabPage2.Controls.Add(Me.txtDDD)
        Me.TabPage2.Controls.Add(Me.Label8)
        Me.TabPage2.Controls.Add(Me.txtTelefone2)
        Me.TabPage2.Controls.Add(Me.txtNome2)
        Me.TabPage2.Controls.Add(Me.Label9)
        Me.TabPage2.Controls.Add(Me.Label10)
        Me.TabPage2.Controls.Add(Me.Label11)
        Me.TabPage2.Controls.Add(Me.cboCategoria2)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(968, 197)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Informações"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'txtValor
        '
        Me.txtValor.Location = New System.Drawing.Point(520, 44)
        Me.txtValor.Name = "txtValor"
        Me.txtValor.Size = New System.Drawing.Size(137, 20)
        Me.txtValor.TabIndex = 101
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(451, 46)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(46, 16)
        Me.Label7.TabIndex = 100
        Me.Label7.Text = "Valor: "
        '
        'txtCod_Ean
        '
        Me.txtCod_Ean.Location = New System.Drawing.Point(104, 46)
        Me.txtCod_Ean.Name = "txtCod_Ean"
        Me.txtCod_Ean.Size = New System.Drawing.Size(137, 20)
        Me.txtCod_Ean.TabIndex = 99
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(24, 48)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(63, 16)
        Me.Label6.TabIndex = 98
        Me.Label6.Text = "Cod Ean:"
        '
        'cboCidade
        '
        Me.cboCidade.FormattingEnabled = True
        Me.cboCidade.Location = New System.Drawing.Point(528, 131)
        Me.cboCidade.Name = "cboCidade"
        Me.cboCidade.Size = New System.Drawing.Size(201, 21)
        Me.cboCidade.TabIndex = 97
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(459, 133)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(55, 16)
        Me.Label15.TabIndex = 96
        Me.Label15.Text = "Cidade:"
        '
        'btnSalvar
        '
        Me.btnSalvar.Location = New System.Drawing.Point(839, 43)
        Me.btnSalvar.Name = "btnSalvar"
        Me.btnSalvar.Size = New System.Drawing.Size(75, 23)
        Me.btnSalvar.TabIndex = 95
        Me.btnSalvar.Text = "Salvar"
        Me.btnSalvar.UseVisualStyleBackColor = True
        '
        'txtDDD
        '
        Me.txtDDD.Location = New System.Drawing.Point(102, 129)
        Me.txtDDD.Name = "txtDDD"
        Me.txtDDD.Size = New System.Drawing.Size(66, 20)
        Me.txtDDD.TabIndex = 94
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(49, 130)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(44, 16)
        Me.Label8.TabIndex = 93
        Me.Label8.Text = "DDD: "
        '
        'txtTelefone2
        '
        Me.txtTelefone2.Location = New System.Drawing.Point(252, 129)
        Me.txtTelefone2.Name = "txtTelefone2"
        Me.txtTelefone2.Size = New System.Drawing.Size(185, 20)
        Me.txtTelefone2.TabIndex = 92
        '
        'txtNome2
        '
        Me.txtNome2.Location = New System.Drawing.Point(98, 88)
        Me.txtNome2.Name = "txtNome2"
        Me.txtNome2.Size = New System.Drawing.Size(327, 20)
        Me.txtNome2.TabIndex = 90
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(175, 131)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(65, 16)
        Me.Label9.TabIndex = 91
        Me.Label9.Text = "Telefone:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(39, 90)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(48, 16)
        Me.Label10.TabIndex = 87
        Me.Label10.Text = "Nome:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(447, 89)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(73, 16)
        Me.Label11.TabIndex = 88
        Me.Label11.Text = "Categoria: "
        '
        'cboCategoria2
        '
        Me.cboCategoria2.FormattingEnabled = True
        Me.cboCategoria2.Location = New System.Drawing.Point(538, 88)
        Me.cboCategoria2.Name = "cboCategoria2"
        Me.cboCategoria2.Size = New System.Drawing.Size(263, 21)
        Me.cboCategoria2.TabIndex = 89
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rdExtornar)
        Me.GroupBox2.Controls.Add(Me.rdIncluir)
        Me.GroupBox2.Controls.Add(Me.lblValor)
        Me.GroupBox2.Controls.Add(Me.txtLoteCodCliente)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.lblTotalFichas)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Location = New System.Drawing.Point(994, 151)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(212, 245)
        Me.GroupBox2.TabIndex = 84
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Incluir Manualmente"
        '
        'rdExtornar
        '
        Me.rdExtornar.AutoSize = True
        Me.rdExtornar.Location = New System.Drawing.Point(105, 31)
        Me.rdExtornar.Name = "rdExtornar"
        Me.rdExtornar.Size = New System.Drawing.Size(64, 17)
        Me.rdExtornar.TabIndex = 77
        Me.rdExtornar.TabStop = True
        Me.rdExtornar.Text = "Extornar"
        Me.rdExtornar.UseVisualStyleBackColor = True
        '
        'rdIncluir
        '
        Me.rdIncluir.AutoSize = True
        Me.rdIncluir.Checked = True
        Me.rdIncluir.Location = New System.Drawing.Point(9, 31)
        Me.rdIncluir.Name = "rdIncluir"
        Me.rdIncluir.Size = New System.Drawing.Size(53, 17)
        Me.rdIncluir.TabIndex = 76
        Me.rdIncluir.TabStop = True
        Me.rdIncluir.Text = "Incluir"
        Me.rdIncluir.UseVisualStyleBackColor = True
        '
        'txtLoteCodCliente
        '
        Me.txtLoteCodCliente.Location = New System.Drawing.Point(9, 80)
        Me.txtLoteCodCliente.Name = "txtLoteCodCliente"
        Me.txtLoteCodCliente.Size = New System.Drawing.Size(161, 20)
        Me.txtLoteCodCliente.TabIndex = 75
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 61)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(102, 16)
        Me.Label3.TabIndex = 74
        Me.Label3.Text = "Código Cliente :"
        '
        'btnImprimirRecibo
        '
        Me.btnImprimirRecibo.Enabled = False
        Me.btnImprimirRecibo.Location = New System.Drawing.Point(994, 463)
        Me.btnImprimirRecibo.Name = "btnImprimirRecibo"
        Me.btnImprimirRecibo.Size = New System.Drawing.Size(90, 44)
        Me.btnImprimirRecibo.TabIndex = 85
        Me.btnImprimirRecibo.Text = "Imprimir Recibo"
        Me.btnImprimirRecibo.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(994, 574)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(158, 44)
        Me.Button1.TabIndex = 86
        Me.Button1.Text = "Verifica Fichas Não Atendeu"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnImprimirReciboRetido
        '
        Me.btnImprimirReciboRetido.Enabled = False
        Me.btnImprimirReciboRetido.Location = New System.Drawing.Point(994, 520)
        Me.btnImprimirReciboRetido.Name = "btnImprimirReciboRetido"
        Me.btnImprimirReciboRetido.Size = New System.Drawing.Size(158, 44)
        Me.btnImprimirReciboRetido.TabIndex = 87
        Me.btnImprimirReciboRetido.Text = "Imprimir Recibo Retido"
        Me.btnImprimirReciboRetido.UseVisualStyleBackColor = True
        '
        'btnReImprimir
        '
        Me.btnReImprimir.Location = New System.Drawing.Point(1099, 463)
        Me.btnReImprimir.Name = "btnReImprimir"
        Me.btnReImprimir.Size = New System.Drawing.Size(107, 44)
        Me.btnReImprimir.TabIndex = 88
        Me.btnReImprimir.Text = "Recibo Mensal com Especial"
        Me.btnReImprimir.UseVisualStyleBackColor = True
        '
        'btnHistorico
        '
        Me.btnHistorico.Location = New System.Drawing.Point(1099, 402)
        Me.btnHistorico.Name = "btnHistorico"
        Me.btnHistorico.Size = New System.Drawing.Size(107, 44)
        Me.btnHistorico.TabIndex = 89
        Me.btnHistorico.Text = "Histórico"
        Me.btnHistorico.UseVisualStyleBackColor = True
        '
        'chkMensalEspecial
        '
        Me.chkMensalEspecial.AutoSize = True
        Me.chkMensalEspecial.Location = New System.Drawing.Point(254, 74)
        Me.chkMensalEspecial.Name = "chkMensalEspecial"
        Me.chkMensalEspecial.Size = New System.Drawing.Size(164, 17)
        Me.chkMensalEspecial.TabIndex = 90
        Me.chkMensalEspecial.Text = "Imprimir Mensal com Especial"
        Me.chkMensalEspecial.UseVisualStyleBackColor = True
        '
        'FrmLiberaFaturamento
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1218, 674)
        Me.Controls.Add(Me.btnHistorico)
        Me.Controls.Add(Me.btnReImprimir)
        Me.Controls.Add(Me.btnImprimirReciboRetido)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnImprimirRecibo)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.TabInformacoes)
        Me.Controls.Add(Me.btnSair)
        Me.Controls.Add(Me.grdFaturamento)
        Me.Controls.Add(Me.Panel1)
        Me.MinimizeBox = False
        Me.Name = "FrmLiberaFaturamento"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Libera Faturamento"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.grdMovimentacao, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.grdFaturamento, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabInformacoes.ResumeLayout(False)
        Me.tabHistorico.ResumeLayout(False)
        CType(Me.GrdHistorico, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnPesquisar As System.Windows.Forms.Button
    Friend WithEvents grdFaturamento As System.Windows.Forms.DataGridView
    Friend WithEvents btnSair As System.Windows.Forms.Button
    Friend WithEvents lblTotalFichas As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblValor As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TabInformacoes As System.Windows.Forms.TabControl
    Friend WithEvents tabHistorico As System.Windows.Forms.TabPage
    Friend WithEvents GrdHistorico As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents txtValor As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtCod_Ean As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cboCidade As System.Windows.Forms.ComboBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents btnSalvar As System.Windows.Forms.Button
    Friend WithEvents txtDDD As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtTelefone2 As System.Windows.Forms.TextBox
    Friend WithEvents txtNome2 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cboCategoria2 As System.Windows.Forms.ComboBox
    Friend WithEvents cboCidadeLote As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtDia As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents cboOperador As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnConsultarLote As System.Windows.Forms.Button
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtConsultaLote As System.Windows.Forms.TextBox
    Friend WithEvents btnSalvarLote As System.Windows.Forms.Button
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtLote As System.Windows.Forms.TextBox
    Friend WithEvents btnNovo As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtLoteCodCliente As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents grdMovimentacao As System.Windows.Forms.DataGridView
    Friend WithEvents rdExtornar As System.Windows.Forms.RadioButton
    Friend WithEvents rdIncluir As System.Windows.Forms.RadioButton
    Friend WithEvents btnImprimirRecibo As System.Windows.Forms.Button
    Friend WithEvents cboOperador2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents chkMensais As System.Windows.Forms.CheckBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btnImprimirReciboRetido As System.Windows.Forms.Button
    Friend WithEvents btnReImprimir As System.Windows.Forms.Button
    Friend WithEvents btnHistorico As System.Windows.Forms.Button
    Friend WithEvents chkMensalEspecial As System.Windows.Forms.CheckBox
End Class
