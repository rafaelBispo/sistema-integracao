﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text
Imports Microsoft.Office.Interop
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data.SqlClient


Public Class FrmLiberaFaturamento

#Region "Enum"

    Private Enum ColunasGridFaturamento
        Cod_EAN_Cliente = 0
        Nome_contribuinte = 1
        Data_mov = 2
        Tipo_mov = 3
        Tipo_cobranca = 4
        Cod_Categoria = 5
        DDD = 6
        Telefone1 = 7
        Valor = 8
        Cod_recibo = 9
        Cidade = 10
    End Enum

    Private Enum ColunasGridHistorico
        DDD = 0
        Telefone = 1
        Valor = 2
        categoria = 3
        mesRef = 4
        Situacao = 5
        MotivoSituacao = 6
        Data_Situacao = 7
    End Enum

    Private Enum ColunasGridMovimentacao

        mov_contribuicao_id = 0
        tipo_mov = 1
        tipo_cobranca = 2
        cobrador = 3
        data_mov = 4
        total_fichas = 5
        valor_fichas = 6
    End Enum

    Enum xlsOption
        xlsOpen
    End Enum

#End Region

#Region "variaveis"

    Public telefone As String
    Private lstContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstContribuinteSelecionada As New List(Of CRetorno)
    Private v_ambiente As String
    Private rnContribuinte As RNContribuinte = New RNContribuinte
    Private rnRequisicao As RNRequisicao = New RNRequisicao
    Private contribuinte As CContribuinte = New CContribuinte
    Private lstrequisicao As New SortableBindingList(Of CContribuinte)
    Private lstrequisicaoSelecionada As New List(Of CContribuinte)
    Private lstMovimentacao As New SortableBindingList(Of CRequisicao)
    Private lstRetornoContribuintes As New SortableBindingList(Of CContribuinte)
    Public mNivelAcesso As Integer
    Public mUsuario As String
    Public mCodUsuario As Integer
    Public atualizou As Boolean
    Public bindingSourceContribuite As BindingSource = New BindingSource
    Public bindingSourceMovimentacao As BindingSource = New BindingSource


    Public Property NivelAcesso() As Integer
        Get
            Return mNivelAcesso
        End Get
        Set(ByVal value As Integer)
            mNivelAcesso = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return mUsuario
        End Get
        Set(ByVal value As String)
            mUsuario = value
        End Set
    End Property

    Public Property CodUsuario() As Integer
        Get
            Return mCodUsuario
        End Get
        Set(ByVal value As Integer)
            mCodUsuario = value
        End Set
    End Property

#End Region



#Region "Metodos"



    Private Sub carregaGrid()

        Dim objContribuintes As CContribuinte = New CContribuinte()

        Dim bindingSourceApolice As BindingSource = New BindingSource

        Try



            lstContribuintes = New SortableBindingList(Of CContribuinte)(rnContribuinte.CarregarGridOi(objContribuintes, "Remessa"))

            If (lstContribuintes.Count > 0) Then

                bindingSourceApolice.DataSource = lstContribuintes

                grdFaturamento.DataSource = bindingSourceApolice.DataSource
                'grdRemessa.Sort(grdRemessa.Columns(ColunasGridRemessa.Nome), ListSortDirection.Ascending)
                'lblNumRegistros.Text = lstApolices.Count


            End If


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CriarColunasGrid()


        Try

            grdFaturamento.DataSource = Nothing
            grdFaturamento.Columns.Clear()

            grdFaturamento.AutoGenerateColumns = False

            grdFaturamento.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            'Dim checkboxColumn As New DataGridViewCheckBoxColumn
            'checkboxColumn.Width = 32
            'checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_EAN_Cliente"
            column.HeaderText = "Cód. EAN Cliente"
            column.Width = 80
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Nome_contribuinte"
            column.HeaderText = "Nome contribuinte"
            column.Width = 300
            column.ReadOnly = False
            grdFaturamento.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Data_mov"
            column.HeaderText = "Data Movimentação"
            column.Width = 80
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Tipo_mov"
            column.HeaderText = "Tipo Movimentação"
            column.Width = 80
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Dia"
            column.HeaderText = "Dia"
            column.Width = 30
            column.ReadOnly = False
            grdFaturamento.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Tipo_cobranca"
            column.HeaderText = "Tipo Cobranca"
            column.Width = 80
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_Categoria"
            column.HeaderText = "Cód Categoria"
            column.Width = 80
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_opera"
            column.HeaderText = "Cód Operador"
            column.Width = 80
            column.ReadOnly = False
            grdFaturamento.Columns.Add(column)


            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "valor"
            column.HeaderText = "Valor"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = False
            column.Visible = True
            grdFaturamento.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_recibo"
            column.HeaderText = "Cód. Recibo"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            grdFaturamento.Columns.Add(column)

            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cidade"
            column.HeaderText = "Cidade"
            column.Width = 80
            column.ReadOnly = True
            column.Visible = True
            grdFaturamento.Columns.Add(column)

            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "dt_impressao"
            column.HeaderText = "Data de Impressão"
            column.Width = 150
            column.ReadOnly = True
            column.Visible = True
            grdFaturamento.Columns.Add(column)




        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CriarColunasGridHistorico()

        Try

            GrdHistorico.DataSource = Nothing
            GrdHistorico.Columns.Clear()

            GrdHistorico.AutoGenerateColumns = False

            GrdHistorico.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            'Dim checkboxColumn As New DataGridViewCheckBoxColumn
            'checkboxColumn.Width = 32
            'checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DDD"
            column.HeaderText = "DDD"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Telefone1"
            column.HeaderText = "Telefone"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "valor"
            column.HeaderText = "Valor"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            GrdHistorico.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "cod_categoria"
            column.HeaderText = "Categoria"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            GrdHistorico.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "mesRef"
            column.HeaderText = "Mês Referência"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            GrdHistorico.Columns.Add(column)

            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "situacao"
            column.HeaderText = "Situacao Faturamento"
            column.Width = 160
            column.ReadOnly = False
            column.Visible = True
            GrdHistorico.Columns.Add(column)

            '7
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "motivoSituacao"
            column.HeaderText = "Motivo Situação"
            column.Visible = True
            column.Width = 150
            column.ReadOnly = False
            GrdHistorico.Columns.Add(column)

            '8
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "dt_situacao"
            column.HeaderText = "Data Situação"
            column.Visible = True
            column.Width = 150
            column.ReadOnly = False
            GrdHistorico.Columns.Add(column)



        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CriarColunasGridMovimentacao()

        Try

            grdMovimentacao.DataSource = Nothing
            grdMovimentacao.Columns.Clear()

            grdMovimentacao.AutoGenerateColumns = False

            grdMovimentacao.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            'Dim checkboxColumn As New DataGridViewCheckBoxColumn
            'checkboxColumn.Width = 32
            'checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "mov_contribuicao_id"
            column.HeaderText = "Cód. Contribuicao"
            column.Width = 80
            column.ReadOnly = True
            grdMovimentacao.Columns.Add(column)

            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "tipo_mov"
            column.HeaderText = "Tipo movimentação"
            column.Width = 80
            column.ReadOnly = True
            grdMovimentacao.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "tipo_cobranca"
            column.HeaderText = "Tipo cobranca"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            grdMovimentacao.Columns.Add(column)

            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "cobrador"
            column.HeaderText = "Cobrador"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            grdMovimentacao.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "data_mov"
            column.HeaderText = "Data movimentação"
            column.Width = 160
            column.ReadOnly = False
            column.Visible = True
            grdMovimentacao.Columns.Add(column)

            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "total_fichas"
            column.HeaderText = "Total fichas"
            column.Visible = True
            column.Width = 150
            column.ReadOnly = False
            grdMovimentacao.Columns.Add(column)

            '7
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "valor_fichas"
            column.HeaderText = "Valor fichas"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            grdMovimentacao.Columns.Add(column)




        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

#End Region


    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""
        Dim cidade As String = ""
        Dim dia As Integer = 0
        Dim operador_id As Integer = 0
        Dim rn As RNRequisicao = New RNRequisicao
        Dim imprimirMensal As String

        Try

            Me.Cursor = Cursors.WaitCursor

            rnRequisicao.DeletaDadosTabelaLiberacao()
            btnNovo.Enabled = True

            txtLoteCodCliente.Enabled = False

            If cboCidadeLote.SelectedIndex = 0 Or cboCidadeLote.Text = "Selecione..." Then
                MsgBox("Opção invalida, selecionar a cidade!", MsgBoxStyle.Critical, "Atenção!")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If

            If (chkMensalEspecial.Checked = True) Then
                carregaFichasFaturamentoMensalEspecial(cidade, dia, operador_id)
            Else

                If txtDia.Text = "" Then
                    MsgBox("Opção Dia não foi selecionada, com isso ira ser retornada fichas de acordo com as outras premissas determinadas", MsgBoxStyle.Information, "Atenção!")
                End If

                If (chkMensais.Checked = True) Then
                    imprimirMensal = "S"
                Else
                    imprimirMensal = "N"
                End If



                cidade = cboCidadeLote.Text
                If txtDia.Text <> "" Then
                    dia = txtDia.Text
                End If

                If imprimirMensal = "N" And cboOperador2.SelectedValue < 0 Then
                    MsgBox("Opção invalida, selecionar o operador!", MsgBoxStyle.Critical, "Atenção!")
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If

                If cboOperador2.SelectedValue > 0 Then
                    operador_id = cboOperador2.SelectedValue
                Else
                    operador_id = 0
                End If


                carregaFichasFaturamento(cidade, dia, operador_id, imprimirMensal)
            End If


            txtLoteCodCliente.Focus()


            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try

    End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Close()
    End Sub

    Private Sub FrmConsultaContribuinte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        CriarColunasGrid()
        CarregaComboOperador(0)
        CarregaComboCidades(0)
        CriarColunasGridHistorico()
        CriarColunasGridMovimentacao()

        btnPesquisar.Enabled = False
        cboCidadeLote.Enabled = False
        txtDia.Enabled = False
        cboOperador.Enabled = False
        cboOperador2.Enabled = False
        btnImprimirRecibo.Enabled = False
        btnImprimirReciboRetido.Enabled = True
        txtLoteCodCliente.Enabled = False

        btnReImprimir.Enabled = False

        CarregaMovimentacoes()




    End Sub


    Private Sub FrmLiberaFaturamento_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""

        Try


        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub carregaFichasFaturamento(ByVal cidade As String, ByVal dia As Integer, ByVal operador_id As Integer, ByVal imprimirMensal As String)
        Dim objRetorno As CRetorno = New CRetorno()
        Dim dt As DataTable = New DataTable
        Dim dt2 As DataTable = New DataTable
        Dim ds As DataSet = New DataSet
        Dim dr As DataRow
        Dim dr2 As DataRow
        Dim valor As Single = 0
        Dim strValor As String = ""
        Dim totalValorFichas As Decimal = 0


        Try


            If Not grdFaturamento.DataSource Is Nothing Then

                Dim result = MessageBox.Show("A seleção atual já contém registros, deseja limpar a grid?", "Atenção", _
                                            MessageBoxButtons.YesNo, _
                                            MessageBoxIcon.Question)


                If (result = DialogResult.No) Then
                   

                    dt2 = grdFaturamento.DataSource

                    ds = rnRequisicao.CarregaFichasFaturamento(cidade, dia, operador_id, imprimirMensal)

                    If Not ds Is Nothing Then
                        If (ds.Tables(0).Rows.Count > 0) Then
                            If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                                dt = ds.Tables(0)
                            End If
                        End If
                    End If

                    For Each row As DataRow In dt.Rows

                        dr2 = dt2.NewRow()
                        dr2("cod_ean_cliente") = row("cod_ean_cliente")
                        dr2("nome_contribuinte") = row("nome_contribuinte")
                        dr2("data_mov") = row("data_mov")
                        dr2("tipo_mov") = row("tipo_mov")
                        dr2("dia") = row("dia")
                        dr2("tipo_cobranca") = row("tipo_cobranca")
                        dr2("cod_categoria") = row("cod_categoria")
                        dr2("cod_opera") = row("cod_opera")
                        dr2("valor") = row("valor")
                        dr2("cod_recibo") = row("cod_recibo")
                        dr2("cidade") = row("cidade")
                        dr2("dt_impressao") = row("dt_impressao")

                        dt2.Rows.Add(dr2)

                    Next

                    grdFaturamento.DataSource = dt2


                    lblTotalFichas.Text = grdFaturamento.Rows.Count - 1

                    For Each col As DataGridViewRow In grdFaturamento.Rows

                        totalValorFichas = totalValorFichas + col.Cells(8).Value

                    Next

                    strValor = valor.ToString("C")
                    valor = totalValorFichas
                    lblValor.Text = valor.ToString("R$ #,###.00")

                Else
                    grdFaturamento.DataSource = Nothing


                    ds = rnRequisicao.CarregaFichasFaturamento(cidade, dia, operador_id, imprimirMensal)

                    If Not ds Is Nothing Then
                        If (ds.Tables(0).Rows.Count > 0) Then
                            If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                                dt = ds.Tables(0)
                            End If
                        End If

                        grdFaturamento.DataSource = dt


                        If (grdFaturamento.Rows.Count = 0) Then

                            grdFaturamento.DataSource = Nothing
                            MsgBox("Não existe registros a serem processados!")

                        Else

                            lblTotalFichas.Text = grdFaturamento.Rows.Count - 1

                            For Each col As DataGridViewRow In grdFaturamento.Rows

                                totalValorFichas = totalValorFichas + col.Cells(8).Value

                            Next

                            strValor = valor.ToString("C")
                            valor = totalValorFichas
                            lblValor.Text = valor.ToString("R$ #,###.00")

                        End If
                    Else
                        grdFaturamento.DataSource = Nothing
                        MsgBox("Não existe registros a serem processados!")
                    End If

                End If

            Else

                ds = rnRequisicao.CarregaFichasFaturamento(cidade, dia, operador_id, imprimirMensal)

                If Not ds Is Nothing Then
                    If (ds.Tables(0).Rows.Count > 0) Then
                        If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                            dt = ds.Tables(0)
                        End If
                    End If

                    grdFaturamento.DataSource = dt


                    If (grdFaturamento.Rows.Count = 0) Then

                        grdFaturamento.DataSource = Nothing
                        MsgBox("Não existe registros a serem processados!")

                    Else

                        lblTotalFichas.Text = grdFaturamento.Rows.Count - 1

                        For Each col As DataGridViewRow In grdFaturamento.Rows

                            totalValorFichas = totalValorFichas + col.Cells(8).Value

                        Next

                        strValor = valor.ToString("C")
                        valor = totalValorFichas
                        lblValor.Text = valor.ToString("R$ #,###.00")

                    End If
                Else
                    grdFaturamento.DataSource = Nothing
                    MsgBox("Não existe registros a serem processados!")
                End If

            End If

            Dim count As Integer = 0
            Dim count_retido As Integer = 0


            For Each linha As DataGridViewRow In grdFaturamento.Rows

                If Not IsDBNull(linha.Cells(11).Value) And Not IsNothing(linha.Cells(11).Value) Then
                    'coloco cor na linha
                    linha.DefaultCellStyle.BackColor = Color.Yellow
                    linha.DefaultCellStyle.ForeColor = Color.Black
                    count = count + 1
                End If

                If (linha.Cells(6).Value = 13) Or (linha.Cells(6).Value = 14) Or (linha.Cells(6).Value = 15) Then
                    count_retido = count_retido + 1
                End If

            Next

            If count > 0 Then
                MsgBox("Por favor verifique as linhas destacadas em amarelo, e a coluna de data de impressão, pois os recibos já foram impressos", MsgBoxStyle.Exclamation, "Atenção")
                MsgBox("Para realizar a impressão das fichas já emitidas clique em 2° via de recibo!", MsgBoxStyle.Exclamation, "Atenção")
            End If

            If count_retido > 0 Then
                MsgBox("Para imprimir os recibos da categoria RETIDO, clique em Imprimir Recibo Retido!", MsgBoxStyle.Exclamation, "Atenção")
            End If

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub carregaFichasFaturamentoMensalEspecial(ByVal cidade As String, ByVal dia As Integer, ByVal operador_id As Integer)
        Dim objRetorno As CRetorno = New CRetorno()
        Dim dt As DataTable = New DataTable
        Dim dt2 As DataTable = New DataTable
        Dim ds As DataSet = New DataSet
        Dim dr As DataRow
        Dim dr2 As DataRow
        Dim valor As Single = 0
        Dim strValor As String = ""
        Dim totalValorFichas As Decimal = 0


        Try


            If Not grdFaturamento.DataSource Is Nothing Then

                Dim result = MessageBox.Show("A seleção atual já contém registros, deseja limpar a grid?", "Atenção", _
                                            MessageBoxButtons.YesNo, _
                                            MessageBoxIcon.Question)


                If (result = DialogResult.No) Then


                    dt2 = grdFaturamento.DataSource

                    ds = rnRequisicao.CarregaFichasFaturamentoMensalEspecial(cidade, dia, operador_id)

                    If Not ds Is Nothing Then
                        If (ds.Tables(0).Rows.Count > 0) Then
                            If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                                dt = ds.Tables(0)
                            End If
                        End If
                    End If

                    For Each row As DataRow In dt.Rows

                        dr2 = dt2.NewRow()
                        dr2("cod_ean_cliente") = row("cod_ean_cliente")
                        dr2("nome_contribuinte") = row("nome_contribuinte")
                        dr2("data_mov") = row("data_mov")
                        dr2("tipo_mov") = row("tipo_mov")
                        dr2("dia") = row("dia")
                        dr2("tipo_cobranca") = row("tipo_cobranca")
                        dr2("cod_categoria") = row("cod_categoria")
                        dr2("cod_opera") = row("cod_opera")
                        dr2("valor") = row("valor")
                        dr2("cod_recibo") = row("cod_recibo")
                        dr2("cidade") = row("cidade")
                        dr2("dt_impressao") = row("dt_impressao")

                        dt2.Rows.Add(dr2)

                    Next

                    grdFaturamento.DataSource = dt2


                    lblTotalFichas.Text = grdFaturamento.Rows.Count - 1

                    For Each col As DataGridViewRow In grdFaturamento.Rows

                        totalValorFichas = totalValorFichas + col.Cells(8).Value

                    Next

                    strValor = valor.ToString("C")
                    valor = totalValorFichas
                    lblValor.Text = valor.ToString("R$ #,###.00")

                Else
                    grdFaturamento.DataSource = Nothing


                    ds = rnRequisicao.CarregaFichasFaturamentoMensalEspecial(cidade, dia, operador_id)

                    If Not ds Is Nothing Then
                        If (ds.Tables(0).Rows.Count > 0) Then
                            If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                                dt = ds.Tables(0)
                            End If
                        End If

                        grdFaturamento.DataSource = dt


                        If (grdFaturamento.Rows.Count = 0) Then

                            grdFaturamento.DataSource = Nothing
                            MsgBox("Não existe registros a serem processados!")

                        Else

                            lblTotalFichas.Text = grdFaturamento.Rows.Count - 1

                            For Each col As DataGridViewRow In grdFaturamento.Rows

                                totalValorFichas = totalValorFichas + col.Cells(8).Value

                            Next

                            strValor = valor.ToString("C")
                            valor = totalValorFichas
                            lblValor.Text = valor.ToString("R$ #,###.00")

                        End If
                    Else
                        grdFaturamento.DataSource = Nothing
                        MsgBox("Não existe registros a serem processados!")
                    End If

                End If

            Else

                ds = rnRequisicao.CarregaFichasFaturamentoMensalEspecial(cidade, dia, operador_id)

                If Not ds Is Nothing Then
                    If (ds.Tables(0).Rows.Count > 0) Then
                        If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                            dt = ds.Tables(0)
                        End If
                    End If

                    grdFaturamento.DataSource = dt


                    If (grdFaturamento.Rows.Count = 0) Then

                        grdFaturamento.DataSource = Nothing
                        MsgBox("Não existe registros a serem processados!")

                    Else

                        lblTotalFichas.Text = grdFaturamento.Rows.Count - 1

                        For Each col As DataGridViewRow In grdFaturamento.Rows

                            totalValorFichas = totalValorFichas + col.Cells(8).Value

                        Next

                        strValor = valor.ToString("C")
                        valor = totalValorFichas
                        lblValor.Text = valor.ToString("R$ #,###.00")

                    End If
                Else
                    grdFaturamento.DataSource = Nothing
                    MsgBox("Não existe registros a serem processados!")
                End If

            End If

            Dim count As Integer = 0
            Dim count_retido As Integer = 0


            For Each linha As DataGridViewRow In grdFaturamento.Rows

                If Not IsDBNull(linha.Cells(11).Value) And Not IsNothing(linha.Cells(11).Value) Then
                    'coloco cor na linha
                    linha.DefaultCellStyle.BackColor = Color.Yellow
                    linha.DefaultCellStyle.ForeColor = Color.Black
                    count = count + 1
                End If

                If (linha.Cells(6).Value = 13) Or (linha.Cells(6).Value = 14) Or (linha.Cells(6).Value = 15) Then
                    count_retido = count_retido + 1
                End If

            Next

            If count > 0 Then
                MsgBox("Por favor verifique as linhas destacadas em amarelo, e a coluna de data de impressão, pois os recibos já foram impressos", MsgBoxStyle.Exclamation, "Atenção")
                MsgBox("Para realizar a impressão das fichas já emitidas clique em 2° via de recibo!", MsgBoxStyle.Exclamation, "Atenção")
            End If

            If count_retido > 0 Then
                MsgBox("Para imprimir os recibos da categoria RETIDO, clique em Imprimir Recibo Retido!", MsgBoxStyle.Exclamation, "Atenção")
            End If

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub



    Private Sub CarregaComboCidades(ByVal cod_cidade As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCidadeLote.DataSource = Nothing
            cboCidadeLote.Items.Clear()
            cboCidadeLote.DisplayMember = "Nome_cidade"
            cboCidadeLote.ValueMember = "Cod_Cidade"
            cboCidadeLote.DataSource = rn_generico.BuscarCidades(0)

            If cboCidadeLote.Items.Count = 0 Then
                cboCidadeLote.SelectedIndex = 0
            ElseIf cboCidadeLote.Items.Count > 0 And cod_cidade <> 0 Then

                For i As Integer = 0 To cboCidadeLote.Items.Count - 1
                    For Each linha As CGenerico In cboCidadeLote.DataSource
                        If linha.Cod_Cidade = cod_cidade Then
                            cboCidadeLote.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next

            Else

                cboCidadeLote.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CarregaComboCidadesDetalhe(ByVal cod_cidade As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCidade.DataSource = Nothing
            cboCidade.Items.Clear()
            cboCidade.DisplayMember = "Nome_cidade"
            cboCidade.ValueMember = "Cod_Cidade"
            cboCidade.DataSource = rn_generico.BuscarCidades(0)

            If cboCidade.Items.Count = 0 Then
                cboCidade.SelectedIndex = 0
            ElseIf cboCidade.Items.Count > 0 And cod_cidade <> 0 Then

                For i As Integer = 0 To cboCidade.Items.Count - 1
                    For Each linha As CGenerico In cboCidade.DataSource
                        If linha.Cod_Cidade = cod_cidade Then
                            cboCidade.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next

            Else

                cboCidade.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub btnSalvar_Click(sender As System.Object, e As System.EventArgs) Handles btnSalvar.Click
        Dim rn As RNGenerico = New RNGenerico
        Dim lista As CListaTelefonica = New CListaTelefonica

        Try

            'ATRIBUI OS VALORES AO OBJETO
            lista.Cod_cliente = txtCod_Ean.Text
            lista.Nome_cliente = txtNome2.Text
            lista.DDD = txtDDD.Text
            lista.Telefone = txtTelefone2.Text
            lista.Valor = txtValor.Text
            lista.Cod_cidade = cboCidade.SelectedValue


            If (rn.Atualiza_Lista_Telefonica(lista)) = True Then
                MsgBox("Informações da Lista Telefônica foram atualizadas com sucesso!")
            Else
                MsgBox("Ocorreu um erro ao atualizar as informações")
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub CarregaComboOperador(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboOperador.DataSource = Nothing
            cboOperador.Items.Clear()
            cboOperador.DisplayMember = "Nome_Operador"
            cboOperador.ValueMember = "Cod_Operador"
            cboOperador.DataSource = rn_generico.BuscarOperador(0)

            If cboOperador.Items.Count > 0 And cod_operador = 0 Then
                cboOperador.SelectedIndex = 0
            ElseIf cboOperador.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperador.Items.Count - 1
                    For Each linha As CGenerico In cboOperador.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperador.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperador.SelectedIndex = 1
            End If


            cboOperador2.DataSource = Nothing
            cboOperador2.Items.Clear()
            cboOperador2.DisplayMember = "Nome_Operador"
            cboOperador2.ValueMember = "Cod_Operador"
            cboOperador2.DataSource = rn_generico.BuscarOperador(0)

            If cboOperador2.Items.Count > 0 And cod_operador = 0 Then
                cboOperador2.SelectedIndex = 0
            ElseIf cboOperador2.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperador2.Items.Count - 1
                    For Each linha As CGenerico In cboOperador2.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperador2.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperador2.SelectedIndex = 1
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboCategorias(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCategoria2.DataSource = Nothing
            cboCategoria2.Items.Clear()
            cboCategoria2.DisplayMember = "Nome_Categoria"
            cboCategoria2.ValueMember = "Cod_Categoria"
            cboCategoria2.DataSource = rn_generico.BuscarCategoriaPorOperador(CodUsuario)

            If cboCategoria2.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoria2.SelectedIndex = 0
            ElseIf cboCategoria2.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoria2.Items.Count - 1
                    For Each linha As CGenerico In cboCategoria2.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoria2.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoria2.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaMovimentacoes()

        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim operador As String = ""
        Try

            If cboOperador.SelectedValue > 0 Then
                operador = cboOperador.Text
            End If

            lstMovimentacao = New SortableBindingList(Of CRequisicao)(rnRequisicao.CarregaMovimentacoes(0, "LIBERACAO", operador, ""))


            If (lstMovimentacao.Count > 0) Then

                bindingSourceMovimentacao.DataSource = lstMovimentacao

                grdMovimentacao.DataSource = bindingSourceMovimentacao.DataSource

            Else
                grdMovimentacao.DataSource = Nothing
                '  MsgBox("Não existe registros a serem visualizados!")

            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub CarregaHistoricoImpressoes()

        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim operador As String = ""
        Try

            rnRequisicao.DeletaDadosTabelaLiberacao()
            If cboOperador.SelectedValue > 0 Then
                operador = cboOperador.Text
            End If

            lstMovimentacao = New SortableBindingList(Of CRequisicao)(rnRequisicao.CarregaMovimentacoes(0, "BORDERO", operador, ""))


            If (lstMovimentacao.Count > 0) Then

                bindingSourceMovimentacao.DataSource = lstMovimentacao

                grdMovimentacao.DataSource = bindingSourceMovimentacao.DataSource

            Else
                grdMovimentacao.DataSource = Nothing
                '  MsgBox("Não existe registros a serem visualizados!")

            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub



    Private Sub btnImprimirRecibo_Click(sender As System.Object, e As System.EventArgs) Handles btnImprimirRecibo.Click
        Dim strReportName As String
        Dim frm As FrmImprimiRelatorios = New FrmImprimiRelatorios

        Dim crtableLogoninfos As New TableLogOnInfos()
        Dim crtableLogoninfo As New TableLogOnInfo()
        Dim crConnectionInfo As New ConnectionInfo()
        Dim CrTables As Tables
        Dim CrTable As Table
        Dim Arquivo As String
        Dim ConnectionString As String
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable



        Try


            ' carrega o relatorio desejado            
            strReportName = "ReciboLista1"

            If cboCidadeLote.Text = "BARBACENA" Then
                strReportName = "ReciboBarbacena"
            End If

                'define o caminho e nome do relatorio

                Dim strReportPath As String = "C:\Digital\SISINTEGRACAO\RPT" & "\" & strReportName & ".rpt"
                'Dim strReportPath As String = "C:\GIT\Sistema de Integraçao\SISINT001\RPT" & "\" & strReportName & ".rpt"
                '
                'verifiqa se o arquivo existe
                If Not IO.File.Exists(strReportPath) Then
                    MsgBox("Relatorio não localizado :" & vbCrLf & strReportPath, MsgBoxStyle.Critical)
                End If
                '
                'instancia o relaorio e carrega
                'Dim CR As New ReportDocument
                'CR.Load(strReportPath)
                'CR.SetDatabaseLogon("sa", "Xucrutis2017", "NOVAJFDSK030\SISINTEGRACAO", "controle_db")
                'CR.SetDatabaseLogon("sa", "xucrutis", "AACI-SERV\SISINTEGRACAO", "controle_db")
                ' CR.SetDatabaseLogon("sa", "Xucrutis2017", "AACI-SERV\SISINTEGRACAO)", "controle_db", True)
                'CR.VerifyDatabase()

                Dim dao As RequisicaoDAO = New RequisicaoDAO
                ds = dao.GeraRecibo()

                ' atualiza fichas que estão com o nome em branco
                dao.verificaFichasComProblemas_SPU()

                'atualiza a data de impressão da ficha
                dao.Atualiza_data_ImpressaoFicha()


                Dim dtCliente As DataTable = ds.Tables(0)
                Dim rptCliente As New ReportDocument

                rptCliente.Load(strReportPath)
            'Dim rptCliente As New ReciboLista1 ''nome do relatorio

            'rptCliente.SetDatabaseLogon("sa", "Xucrutis2017", "NOVAJFLAP044\SISTINTEGRACAO", "controle_db")
            rptCliente.SetDatabaseLogon("sa", "xucrutis", "DESKTOP-36CEDMG\SISINTEGRACAO", "controle_db")

            rptCliente.SetDataSource(dtCliente)

            'frm.CrystalReportViewer.ReportSource = rptCliente
            'frm.CrystalReportViewer.RefreshReport()
            frm.Show()

                btnImprimirRecibo.Enabled = False
                btnNovo.Enabled = True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub btnNovo_Click(sender As System.Object, e As System.EventArgs) Handles btnNovo.Click
        Dim rnRequisicao As RNRequisicao = New RNRequisicao

        Try

            rnRequisicao.DeletaDadosTabelaLiberacao()

            btnPesquisar.Enabled = True
            cboCidadeLote.Enabled = True
            txtDia.Enabled = True
            cboOperador.Enabled = True
            cboOperador2.Enabled = True
            txtLoteCodCliente.Enabled = True
            btnImprimirRecibo.Enabled = False

            txtLote.Text = rnRequisicao.RetornaUltimaMovimentacao()
            grdFaturamento.DataSource = Nothing
            txtConsultaLote.Text = ""
            cboOperador.SelectedIndex = 0
            cboOperador2.SelectedIndex = 0
            lblTotalFichas.Text = 0
            lblValor.Text = 0

            txtLoteCodCliente.Focus()

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub txtLoteCodCliente_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtLoteCodCliente.KeyPress
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim totalLinhas As Integer
        Dim dt As DataTable = New DataTable
        Dim dt2 As DataTable = New DataTable
        Dim ds As DataSet = New DataSet
        Dim dr As DataRow
        Dim dr2 As DataRow
        Dim valor As Single = 0
        Dim strValor As String = ""
        Dim totalValorFichas As Decimal = 0

        Try

            If e.KeyChar = Chr(Keys.Enter) Then 'Detecta se tecla Enter foi pressionada


                Me.Cursor = Cursors.WaitCursor

                ds = rnRequisicao.CarregaFichaPorCodEan(txtLoteCodCliente.Text)

                If ds.Tables(0).Rows.Count = 0 Then
                    ds = rnRequisicao.CarregaContribuintePorCodEan(txtLoteCodCliente.Text)
                End If


                If (ds.Tables(0).Rows.Count > 0) Then
                    If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                        dt = ds.Tables(0)
                    End If


                    dr = dt.Rows(0)

                    If rdIncluir.Checked = True Then


                        For Each col As DataGridViewRow In grdFaturamento.Rows

                            If Not col.Cells(0).Value = Nothing Then
                                If dr("cod_ean_cliente") = col.Cells(0).Value Then
                                    MsgBox("Contibuinte já consta na lista para liberação")
                                    Exit Sub
                                End If
                            End If
                        Next

                        If Not grdFaturamento.DataSource Is Nothing Then
                            dt2 = grdFaturamento.DataSource
                        End If

                        If Not grdFaturamento.DataSource Is Nothing Then


                            dt2 = grdFaturamento.DataSource

                            dr2 = dt2.NewRow()
                            dr2("cod_ean_cliente") = dr("cod_ean_cliente")
                            dr2("nome_contribuinte") = dr("nome_contribuinte")
                            dr2("data_mov") = dr("data_mov")
                            dr2("tipo_mov") = dr("tipo_mov")
                            dr2("dia") = dr("dia")
                            dr2("tipo_cobranca") = dr("tipo_cobranca")
                            dr2("cod_categoria") = dr("cod_categoria")
                            dr2("cod_opera") = dr("cod_opera")
                            dr2("valor") = dr("valor")
                            dr2("cod_recibo") = dr("cod_recibo").ToString()
                            dr2("cidade") = dr("cidade")

                            dt2.Rows.Add(dr2)
                            grdFaturamento.DataSource = dt2

                        Else

                            grdFaturamento.DataSource = dt
                        End If

                    End If



                    If rdExtornar.Checked Then

                        For Each col As DataGridViewRow In grdFaturamento.Rows

                            If Not col.Cells(0).Value = Nothing Then
                                If dr("cod_ean_cliente") = col.Cells(0).Value Then
                                    grdFaturamento.Rows.RemoveAt(col.Index)
                                    Exit For
                                End If
                            End If
                        Next

                    End If

                    lblTotalFichas.Text = grdFaturamento.Rows.Count - 1

                    For Each col As DataGridViewRow In grdFaturamento.Rows

                        totalValorFichas = totalValorFichas + col.Cells(8).Value

                    Next

                    strValor = valor.ToString("C")
                    valor = totalValorFichas
                    lblValor.Text = valor.ToString("R$ #,###.00")

                    txtLoteCodCliente.Text = ""

                    Me.Cursor = Cursors.Default

                End If

                Me.Cursor = Cursors.Default

            End If

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return
        End Try

    End Sub



    Private Sub btnSalvarLote_Click(sender As System.Object, e As System.EventArgs) Handles btnSalvarLote.Click
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim recibo As String = ""
        Dim ultimo_recibo As String = ""
        Dim ClienteSelecionado As String
        Dim Cod_cobradorSelecionado As String
        Dim CobradorSelecionado As String
        Dim cod_mov_contribuicao As Long
        Dim totalFIchas As Long = 0
        Dim totalValorFichas As Decimal = 0
        Dim dt As DataTable = New DataTable
        Dim valor As Single = 0
        Dim strValor As String = ""
        Dim CodReciboExistente As String = ""
        Dim valorModificado As Decimal = 0
        Dim cod_impressos(9999) As String
        Dim cont As Integer = 0

        Try
            cod_impressos(cont) = "Fichas que já foram impressas"

            Me.Cursor = Cursors.WaitCursor

            If cboOperador.Text = "" Or cboOperador.Text = "Selecione..." Then
                MsgBox("Por favor selecione um responsável")
                cboOperador.Focus()
                Me.Cursor = Cursors.Default
                Exit Sub
            End If

            ' SE ESTIVER EDITANDO UM LOTE SOBRESCREVE ELE
            If txtLote.Text <> "" Then
                If rnRequisicao.DeletaDadosTabelaLiberacao() = True Then

                    recibo = rnRequisicao.RetornaUltimoRecibo()
                    ultimo_recibo = recibo + 1
                    ultimo_recibo = "*" + CStr(ultimo_recibo) + "*"

                    cod_mov_contribuicao = txtLote.Text

                    For Each linha As DataGridViewRow In grdFaturamento.Rows

                        ClienteSelecionado = linha.Cells(0).Value
                        Cod_cobradorSelecionado = cboOperador.SelectedValue
                        CobradorSelecionado = cboOperador.Text
                        CodReciboExistente = linha.Cells(9).Value
                        valorModificado = linha.Cells(8).Value

                        If ClienteSelecionado <> Nothing Then
                            If (Replace(CodReciboExistente, "*", "") > 1) Then
                                If rnRequisicao.VerficaFichaImpressa(ClienteSelecionado) = False Then
                                    rnRequisicao.Atualiza_Movimentacao_Liberacao(cod_mov_contribuicao, CodReciboExistente, Cod_cobradorSelecionado, CobradorSelecionado, ClienteSelecionado, valorModificado)
                                    rnRequisicao.GravaTabelaLiberacao(cod_mov_contribuicao, CodReciboExistente, ClienteSelecionado, valorModificado)
                                Else
                                    cod_impressos(cont) = ClienteSelecionado
                                End If
                            Else
                                If rnRequisicao.VerficaFichaImpressa(ClienteSelecionado) = False Then
                                    rnRequisicao.Atualiza_Movimentacao_Liberacao(cod_mov_contribuicao, ultimo_recibo, Cod_cobradorSelecionado, CobradorSelecionado, ClienteSelecionado, valorModificado)
                                    rnRequisicao.GravaTabelaLiberacao(cod_mov_contribuicao, ultimo_recibo, ClienteSelecionado, valorModificado)
                                    'linha.Cells(7).Value = Replace(ultimo_recibo, "*", "")
                                    linha.Cells(9).Value = ultimo_recibo
                                    ultimo_recibo = Replace(ultimo_recibo, "*", "") + 1
                                    ultimo_recibo = "*" + CStr(ultimo_recibo) + "*"
                                Else
                                    rnRequisicao.Atualiza_Movimentacao_Liberacao(cod_mov_contribuicao, ultimo_recibo, Cod_cobradorSelecionado, CobradorSelecionado, ClienteSelecionado, valorModificado)
                                    ultimo_recibo = Replace(ultimo_recibo, "*", "") + 1
                                    ultimo_recibo = "*" + CStr(ultimo_recibo) + "*"
                                    cod_impressos(cont) = ClienteSelecionado
                                    cont = cont + 1
                                End If

                            End If
                            rnRequisicao.GravaTabelaLiberacao(cod_mov_contribuicao, ultimo_recibo, ClienteSelecionado, valorModificado)
                        End If
                    Next

                End If


                'salva os valores totais
                totalFIchas = grdFaturamento.Rows.Count - 1
                For Each col As DataGridViewRow In grdFaturamento.Rows
                    totalValorFichas = totalValorFichas + col.Cells(8).Value
                Next

                strValor = valor.ToString("C")
                valor = totalValorFichas
                lblValor.Text = valor.ToString("R$ #,###.00")

                txtLoteCodCliente.Text = ""

                If rnRequisicao.GravaNumerosMovimentacao(cod_mov_contribuicao, cboOperador.Text, cboOperador.SelectedValue, 1, totalFIchas, totalValorFichas, "BORDERO") = True Then
                    MsgBox("Dados gravados com sucesso!", MsgBoxStyle.Information)
                    btnImprimirRecibo.Enabled = True
                    btnImprimirReciboRetido.Enabled = True
                    btnReImprimir.Enabled = True
                End If

                btnNovo.Enabled = True
                btnReImprimir.Enabled = True
                Me.Cursor = Cursors.Default
                Exit Sub
            End If

            recibo = rnRequisicao.RetornaUltimoRecibo()
            ultimo_recibo = recibo + 1
            ultimo_recibo = "*" + CStr(recibo) + "*"
            cod_mov_contribuicao = txtLote.Text

            If rnRequisicao.DeletaDadosTabelaLiberacao() = True Then

                For Each linha As DataGridViewRow In grdFaturamento.Rows


                    ClienteSelecionado = linha.Cells(0).Value
                    CodReciboExistente = linha.Cells(9).Value
                    Cod_cobradorSelecionado = cboOperador.SelectedValue
                    CobradorSelecionado = cboOperador.Text
                    valorModificado = linha.Cells(8).Value


                    If CLng(Replace(CodReciboExistente, "*", "")) > 1 Then
                        If rnRequisicao.VerficaFichaImpressa(ClienteSelecionado) = False Then
                            rnRequisicao.Atualiza_Movimentacao_Liberacao(cod_mov_contribuicao, CodReciboExistente, Cod_cobradorSelecionado, CobradorSelecionado, ClienteSelecionado, valorModificado)
                            rnRequisicao.GravaTabelaLiberacao(cod_mov_contribuicao, CodReciboExistente, ClienteSelecionado, valorModificado)
                        Else
                            rnRequisicao.Atualiza_Movimentacao_Liberacao(cod_mov_contribuicao, CodReciboExistente, Cod_cobradorSelecionado, CobradorSelecionado, ClienteSelecionado, valorModificado)
                            ultimo_recibo = Replace(ultimo_recibo, "*", "") + 1
                            ultimo_recibo = "*" + CStr(ultimo_recibo) + "*"
                            cod_impressos(cont) = ClienteSelecionado
                            cont = cont + 1
                        End If
                    Else
                        If rnRequisicao.VerficaFichaImpressa(ClienteSelecionado) = False Then
                            rnRequisicao.Atualiza_Movimentacao_Liberacao(cod_mov_contribuicao, ultimo_recibo, Cod_cobradorSelecionado, CobradorSelecionado, ClienteSelecionado, valorModificado)
                            rnRequisicao.GravaTabelaLiberacao(cod_mov_contribuicao, ultimo_recibo, ClienteSelecionado, valorModificado)
                            'linha.Cells(7).Value = Replace(ultimo_recibo, "*", "")
                            linha.Cells(9).Value = ultimo_recibo
                            ultimo_recibo = Replace(ultimo_recibo, "*", "") + 1
                            ultimo_recibo = "*" + CStr(ultimo_recibo) + "*"
                        Else
                            rnRequisicao.Atualiza_Movimentacao_Liberacao(cod_mov_contribuicao, ultimo_recibo, Cod_cobradorSelecionado, CobradorSelecionado, ClienteSelecionado, valorModificado)
                            rnRequisicao.GravaTabelaLiberacao(cod_mov_contribuicao, ultimo_recibo, ClienteSelecionado, valorModificado)
                            ultimo_recibo = Replace(ultimo_recibo, "*", "") + 1
                            ultimo_recibo = "*" + CStr(ultimo_recibo) + "*"
                            cod_impressos(cont) = ClienteSelecionado
                            cont = cont + 1
                        End If
                    End If
                Next

            End If

            'salva os valores totais
            totalFIchas = grdFaturamento.Rows.Count - 1



            For Each col As DataGridViewRow In grdFaturamento.Rows

                totalValorFichas = totalValorFichas + col.Cells(8).Value

            Next

            strValor = valor.ToString("C")
            valor = totalValorFichas
            lblValor.Text = valor.ToString("R$ #,###.00")


            If rnRequisicao.GravaNumerosMovimentacao(cod_mov_contribuicao, cboOperador.Text, cboOperador.SelectedValue, 1, totalFIchas, totalValorFichas, "BORDERO") = True Then

                MsgBox("Dados gravados com sucesso!", MsgBoxStyle.Information)

                btnImprimirRecibo.Enabled = True
                btnImprimirReciboRetido.Enabled = True
            Else

                MsgBox("Ocorreu um erro ao gravar os registros!", MsgBoxStyle.Information)

            End If

            btnNovo.Enabled = True
            btnReImprimir.Enabled = True

            Me.Cursor = Cursors.Default


        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return
        End Try

    End Sub

    Private Sub btnConsultarLote_Click(sender As System.Object, e As System.EventArgs) Handles btnConsultarLote.Click
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim cod_mov_contribuicao As Long
        Dim ds As DataSet
        Dim dt As DataTable = New DataTable
        Dim valor As Single = 0
        Dim strValor As String = ""
        Dim totalValorFichas As Decimal = 0
        Dim ClienteClicado As String
        Dim cod_recibo As String
        Dim valorModificado As Decimal = 0

        Try

            If txtConsultaLote.Text = "" Then
                MsgBox("Por favor insira o lote a ser consultado", MsgBoxStyle.Critical, "Atenção!!")
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor

            rnRequisicao.DeletaDadosTabelaLiberacao()

            btnNovo.Enabled = True
            txtLoteCodCliente.Enabled = True
            txtLote.Text = txtConsultaLote.Text

            cod_mov_contribuicao = txtConsultaLote.Text

            ds = rnRequisicao.CarregaMovimentacaoDescr(cod_mov_contribuicao)


            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                    btnImprimirRecibo.Enabled = True
                    btnImprimirReciboRetido.Enabled = True
                End If
            Else
                btnImprimirRecibo.Enabled = False
                btnImprimirReciboRetido.Enabled = True
            End If

            grdFaturamento.DataSource = dt


            lblTotalFichas.Text = grdFaturamento.Rows.Count - 1

            Dim count As Integer = 0

            For Each col As DataGridViewRow In grdFaturamento.Rows

                totalValorFichas = totalValorFichas + col.Cells(8).Value

                If Not IsDBNull(col.Cells(11).Value) Then
                    'coloco cor na linha
                    col.DefaultCellStyle.BackColor = Color.Yellow
                    col.DefaultCellStyle.ForeColor = Color.Black
                    count = count + 1

                End If

            Next

            strValor = valor.ToString("C")
            valor = totalValorFichas
            lblValor.Text = valor.ToString("R$ #,###.00")

            txtLoteCodCliente.Text = ""


            'grava dados na tabela de liberacao
            If rnRequisicao.DeletaDadosTabelaLiberacao() = True Then

                dt = grdFaturamento.DataSource

                For Each dr As DataRow In dt.Rows

                    ClienteClicado = dr("cod_ean_cliente")
                    cod_recibo = dr("cod_recibo")
                    valorModificado = dr("valor")

                    If ClienteClicado <> Nothing Then
                        rnRequisicao.GravaTabelaLiberacao(cod_mov_contribuicao, cod_recibo, ClienteClicado, valorModificado)
                    End If
                Next

            End If

            txtLoteCodCliente.Focus()
            btnImprimirRecibo.Enabled = True
            btnImprimirReciboRetido.Enabled = True
            cboOperador.Enabled = True
            cboOperador2.Enabled = True
            btnSalvar.Enabled = True
            btnNovo.Enabled = False
            cboCidadeLote.Enabled = True
            txtDia.Enabled = True
            btnPesquisar.Enabled = True

            If count > 0 Then
                MsgBox("Por favor verifique as linhas destacadas em amarelo, e a coluna de data de impressão, pois os recibos já foram impressos", MsgBoxStyle.Exclamation, "Atenção")
            End If

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return
        End Try



    End Sub



    Private Sub grdFaturamento_CellContentDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdFaturamento.CellContentDoubleClick

        Dim ClienteClicado As String
        Dim TelefoneSelecionado As String = ""
        Dim rn As RNRequisicao = New RNRequisicao
        Dim rnContribuinte As RNContribuinte = New RNContribuinte

        Try

            Me.Cursor = Cursors.WaitCursor

            ClienteClicado = CType(grdFaturamento.Rows(e.RowIndex).Cells(ColunasGridFaturamento.Cod_EAN_Cliente).Value, String)



            For Each ContribuinteRequisicao As CContribuinte In lstContribuintes
                If ContribuinteRequisicao.Cod_EAN_Cliente = ClienteClicado Then

                End If
            Next


            If (e.RowIndex > -1) Then

                lstRetornoContribuintes = New SortableBindingList(Of CContribuinte)(rn.CarregaHistoricoContribuinte(ClienteClicado))

                If (lstRetornoContribuintes.Count > 0) Then

                    bindingSourceContribuite.DataSource = lstRetornoContribuintes

                    GrdHistorico.DataSource = bindingSourceContribuite.DataSource

                Else

                    GrdHistorico.DataSource = Nothing

                End If


                contribuinte = rnContribuinte.PesquisarPorCodBarra(ClienteClicado)


                If Not contribuinte Is Nothing Then
                    'popula a tela
                    txtNome2.Text = contribuinte.NomeCliente1
                    txtCod_Ean.Text = contribuinte.Cod_EAN_Cliente
                    txtTelefone2.Text = contribuinte.Telefone1
                    txtDDD.Text = contribuinte.DDD
                    txtValor.Text = Convert.ToDouble(contribuinte.Valor).ToString("#,###.00")
                    CarregaComboCidadesDetalhe(contribuinte.Cod_Cidade)
                    CarregaComboCategorias(contribuinte.Cod_Categoria)

                End If



            End If
            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return
        End Try
    End Sub


    Private Sub grdFaturamento_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdFaturamento.CellContentClick

        Dim ClienteClicado As String
        Dim TelefoneSelecionado As String = ""
        Dim rn As RNRequisicao = New RNRequisicao
        Dim rnContribuinte As RNContribuinte = New RNContribuinte

        Try

            Me.Cursor = Cursors.WaitCursor

            ClienteClicado = CType(grdFaturamento.Rows(e.RowIndex).Cells(ColunasGridFaturamento.Cod_EAN_Cliente).Value, String)



            For Each ContribuinteRequisicao As CContribuinte In lstContribuintes
                If ContribuinteRequisicao.Cod_EAN_Cliente = ClienteClicado Then

                End If
            Next


            If (e.RowIndex > -1) Then

                lstRetornoContribuintes = New SortableBindingList(Of CContribuinte)(rn.CarregaHistoricoContribuinte(ClienteClicado))

                If (lstRetornoContribuintes.Count > 0) Then

                    bindingSourceContribuite.DataSource = lstRetornoContribuintes

                    GrdHistorico.DataSource = bindingSourceContribuite.DataSource

                Else

                    GrdHistorico.DataSource = Nothing

                End If


                contribuinte = rnContribuinte.PesquisarPorCodBarra(ClienteClicado)


                If Not contribuinte Is Nothing Then
                    'popula a tela
                    txtNome2.Text = contribuinte.NomeCliente1
                    txtCod_Ean.Text = contribuinte.Cod_EAN_Cliente
                    txtTelefone2.Text = contribuinte.Telefone1
                    txtDDD.Text = contribuinte.DDD
                    txtValor.Text = Convert.ToDouble(contribuinte.Valor).ToString("#,###.00")
                    CarregaComboCidadesDetalhe(contribuinte.Cod_Cidade)
                    CarregaComboCategorias(contribuinte.Cod_Categoria)

                End If



            End If
            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        rnRequisicao.DeletaDadosTabelaLiberacao()
        FrmFichasNA.Show()

    End Sub

    Private Sub btnImprimirReciboRetido_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimirReciboRetido.Click
        Dim strReportName As String
        Dim frm As FrmImprimiRelatorios = New FrmImprimiRelatorios

        Dim crtableLogoninfos As New TableLogOnInfos()
        Dim crtableLogoninfo As New TableLogOnInfo()
        Dim crConnectionInfo As New ConnectionInfo()
        Dim ds As DataSet


        Try

            ' carrega o relatorio desejado
            strReportName = "ReciboListaRetido"

            'define o caminho e nome do relatorio

            Dim strReportPath As String = "C:\Digital\SISINTEGRACAO\RPT" & "\" & strReportName & ".rpt"
            'Dim strReportPath As String = "C:\GIT\Sistema de Integraçao\SISINT001\RPT" & "\" & strReportName & ".rpt"
            '
            'verifiqa se o arquivo existe
            If Not IO.File.Exists(strReportPath) Then
                MsgBox("Relatorio não localizado :" & vbCrLf & strReportPath, MsgBoxStyle.Critical)
            End If
            '
            'instancia o relaorio e carrega
            'Dim CR As New ReportDocument
            'CR.Load(strReportPath)
            'CR.SetDatabaseLogon("sa", "xucrutis", "DESKTOP-36CEDMG\SISINTEGRACAO", "controle_db")
            'CR.SetDatabaseLogon("sa", "xucrutis", "AACI-SERV\SISINTEGRACAO", "controle_db")
            ' CR.SetDatabaseLogon("sa", "Xucrutis2017", "AACI-SERV\SISINTEGRACAO)", "controle_db", True)
            'CR.VerifyDatabase()

            Dim dao As RequisicaoDAO = New RequisicaoDAO
            ds = dao.GeraReciboRetido()

            ' atualiza fichas que estão com o nome em branco
            dao.verificaFichasComProblemas_SPU()

            'atualiza a data de impressão da ficha
            dao.Atualiza_data_ImpressaoFicha()


            Dim dtCliente As DataTable = ds.Tables(0)
            Dim rptCliente As New ReportDocument

            rptCliente.Load(strReportPath)
            'Dim rptCliente As New ReciboLista1 ''nome do relatorio

            'rptCliente.SetDatabaseLogon("sa", "Xucrutis2017", "NOVAJFDSK030\SISINTEGRACAO", "controle_db")
            rptCliente.SetDatabaseLogon("sa", "xucrutis", "DESKTOP-36CEDMG\SISINTEGRACAO", "controle_db")

            rptCliente.SetDataSource(dtCliente)

            'frm.CrystalReportViewer.ReportSource = rptCliente
            'frm.CrystalReportViewer.RefreshReport()
            frm.Show()



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub btnHistorico_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHistorico.Click
        CarregaHistoricoImpressoes()
    End Sub

    Private Sub btnReImprimir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReImprimir.Click
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim recibo As String = ""
        Dim ultimo_recibo As String = ""
        Dim ClienteSelecionado As String
        Dim Cod_cobradorSelecionado As String
        Dim CobradorSelecionado As String
        Dim cod_mov_contribuicao As Long
        Dim totalFIchas As Long = 0
        Dim totalValorFichas As Decimal = 0
        Dim dt As DataTable = New DataTable
        Dim valor As Single = 0
        Dim strValor As String = ""
        Dim CodReciboExistente As String = ""
        Dim valorModificado As Decimal = 0
        Dim cod_impressos(9999) As String
        Dim cont As Integer = 0


        Try

            rnRequisicao.DeletaDadosTabelaLiberacao()

            ' SE ESTIVER EDITANDO UM LOTE SOBRESCREVE ELE
                If rnRequisicao.DeletaDadosTabelaLiberacao() = True Then

                cod_mov_contribuicao = txtLote.Text

                    For Each linha As DataGridViewRow In grdFaturamento.Rows

                        ClienteSelecionado = linha.Cells(0).Value
                        Cod_cobradorSelecionado = cboOperador.SelectedValue
                        CobradorSelecionado = cboOperador.Text
                        CodReciboExistente = linha.Cells(9).Value
                        valorModificado = linha.Cells(8).Value

                        rnRequisicao.GravaTabelaLiberacao(cod_mov_contribuicao, CodReciboExistente, ClienteSelecionado, valorModificado)

                    Next
                    'salva os valores totais
                    totalFIchas = grdFaturamento.Rows.Count - 1
                    For Each col As DataGridViewRow In grdFaturamento.Rows
                        totalValorFichas = totalValorFichas + col.Cells(8).Value
                    Next

                    strValor = valor.ToString("C")
                    valor = totalValorFichas
                    lblValor.Text = valor.ToString("R$ #,###.00")

                    txtLoteCodCliente.Text = ""

                rnRequisicao.GravaNumerosMovimentacao(cod_mov_contribuicao, cboOperador.Text, cboOperador.SelectedValue, 1, totalFIchas, totalValorFichas, "BORDERO")
                


                End If

            '=================================================================================
            'PARTE DA IMPRESSAO
            '=================================================================================

            Dim strReportName As String
            Dim frm As FrmImprimiRelatorios = New FrmImprimiRelatorios

            Dim crtableLogoninfos As New TableLogOnInfos()
            Dim crtableLogoninfo As New TableLogOnInfo()
            Dim crConnectionInfo As New ConnectionInfo()
            Dim ds As DataSet = New DataSet


            ' carrega o relatorio desejado
            strReportName = "ReciboLista1"

            'define o caminho e nome do relatorio

            Dim strReportPath As String = "C:\Digital\SISINTEGRACAO\RPT" & "\" & strReportName & ".rpt"
            'Dim strReportPath As String = "C:\GIT\Sistema de Integraçao\SISINT001\RPT" & "\" & strReportName & ".rpt"
            '
            'verifiqa se o arquivo existe
            If Not IO.File.Exists(strReportPath) Then
                MsgBox("Relatorio não localizado :" & vbCrLf & strReportPath, MsgBoxStyle.Critical)
            End If
            '

            Dim dao As RequisicaoDAO = New RequisicaoDAO
            ds = dao.GeraRecibo()

            ' atualiza fichas que estão com o nome em branco
            dao.verificaFichasComProblemas_SPU()

            'atualiza a data de impressão da ficha
            dao.Atualiza_data_ImpressaoFicha()


            Dim dtCliente As DataTable = ds.Tables(0)
            Dim rptCliente As New ReportDocument

            rptCliente.Load(strReportPath)

            'rptCliente.SetDatabaseLogon("sa", "Xucrutis2017", "NOVAJFDSK030\SISINTEGRACAO", "controle_db")
            rptCliente.SetDatabaseLogon("sa", "xucrutis", "DESKTOP-36CEDMG\SISINTEGRACAO", "controle_db")

            rptCliente.SetDataSource(dtCliente)

            'frm.CrystalReportViewer.ReportSource = rptCliente
            'frm.CrystalReportViewer.RefreshReport()
            frm.Show()



            Me.Cursor = Cursors.Default

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub grdMovimentacao_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdMovimentacao.CellContentClick
        Dim movimentoClicado As Long = 0
        Dim rn As RNGenerico = New RNGenerico
        Dim ds As DataSet
        Dim dt As DataTable = New DataTable
        Dim valor As Single = 0
        Dim strValor As String = ""
        Dim totalValorFichas As Decimal = 0

        Try

            Me.Cursor = Cursors.WaitCursor

            rnRequisicao.DeletaDadosTabelaLiberacao()

            movimentoClicado = CType(grdMovimentacao.Rows(e.RowIndex).Cells(ColunasGridMovimentacao.mov_contribuicao_id).Value, Long)

            ds = rnRequisicao.CarregaMovimentacaoDescr(movimentoClicado)


            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If

            grdFaturamento.DataSource = dt


            lblTotalFichas.Text = grdFaturamento.Rows.Count - 1

            For Each col As DataGridViewRow In grdFaturamento.Rows

                totalValorFichas = totalValorFichas + col.Cells(8).Value

            Next

            strValor = valor.ToString("C")
            valor = totalValorFichas
            lblValor.Text = valor.ToString("R$ #,###.00")

            txtLoteCodCliente.Enabled = True
            txtLoteCodCliente.Text = ""

            btnReImprimir.Enabled = True
            txtLote.Text = movimentoClicado
            cboOperador.Enabled = True
            cboOperador2.Enabled = True

            For Each linha As DataGridViewRow In grdFaturamento.Rows

                If Not IsDBNull(linha.Cells(11).Value) Then
                    'coloco cor na linha
                    linha.DefaultCellStyle.BackColor = Color.Yellow
                    linha.DefaultCellStyle.ForeColor = Color.White

                End If

            Next

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return
        End Try
    End Sub
End Class