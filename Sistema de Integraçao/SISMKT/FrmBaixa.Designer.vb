﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmBaixa
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.grdMovimentacao = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cboCidade = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cboOperador = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtDataMov = New System.Windows.Forms.MaskedTextBox()
        Me.btnPesquisar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnNovo = New System.Windows.Forms.Button()
        Me.btnSalvarBaixa = New System.Windows.Forms.Button()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtBaixa = New System.Windows.Forms.TextBox()
        Me.btnConsultarBordero = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtConsultaBordero = New System.Windows.Forms.TextBox()
        Me.grdFaturamento = New System.Windows.Forms.DataGridView()
        Me.btnSair = New System.Windows.Forms.Button()
        Me.lblFichasCanceladas = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblValorFichasCanceladas = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblValorFichasAbertas = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.lblFichaAberta = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.lblValorFichasProxDia = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lblFichasProxDia = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.lblValorFichasBaixadas = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblFichasBaixadas = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.cboAcao = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.rdExtornar = New System.Windows.Forms.RadioButton()
        Me.rdIncluir = New System.Windows.Forms.RadioButton()
        Me.txtLoteCodRecibo = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.grdMovimentacao, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.grdFaturamento, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.grdMovimentacao)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1146, 152)
        Me.Panel1.TabIndex = 1
        '
        'grdMovimentacao
        '
        Me.grdMovimentacao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdMovimentacao.Location = New System.Drawing.Point(551, 6)
        Me.grdMovimentacao.Name = "grdMovimentacao"
        Me.grdMovimentacao.Size = New System.Drawing.Size(588, 141)
        Me.grdMovimentacao.TabIndex = 79
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cboCidade)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.cboOperador)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtDataMov)
        Me.GroupBox1.Controls.Add(Me.btnPesquisar)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.btnNovo)
        Me.GroupBox1.Controls.Add(Me.btnSalvarBaixa)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.txtBaixa)
        Me.GroupBox1.Location = New System.Drawing.Point(-1, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(546, 144)
        Me.GroupBox1.TabIndex = 78
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Lote"
        '
        'cboCidade
        '
        Me.cboCidade.FormattingEnabled = True
        Me.cboCidade.Location = New System.Drawing.Point(116, 118)
        Me.cboCidade.Name = "cboCidade"
        Me.cboCidade.Size = New System.Drawing.Size(256, 24)
        Me.cboCidade.TabIndex = 92
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(113, 100)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(66, 20)
        Me.Label7.TabIndex = 91
        Me.Label7.Text = "CIdade:"
        '
        'cboOperador
        '
        Me.cboOperador.FormattingEnabled = True
        Me.cboOperador.Location = New System.Drawing.Point(116, 76)
        Me.cboOperador.Name = "cboOperador"
        Me.cboOperador.Size = New System.Drawing.Size(256, 24)
        Me.cboOperador.TabIndex = 90
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(113, 58)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(83, 20)
        Me.Label4.TabIndex = 89
        Me.Label4.Text = "Cobrador:"
        '
        'txtDataMov
        '
        Me.txtDataMov.Enabled = False
        Me.txtDataMov.Location = New System.Drawing.Point(253, 35)
        Me.txtDataMov.Mask = "##/##/####"
        Me.txtDataMov.Name = "txtDataMov"
        Me.txtDataMov.Size = New System.Drawing.Size(118, 22)
        Me.txtDataMov.TabIndex = 88
        '
        'btnPesquisar
        '
        Me.btnPesquisar.Enabled = False
        Me.btnPesquisar.Location = New System.Drawing.Point(444, 27)
        Me.btnPesquisar.Name = "btnPesquisar"
        Me.btnPesquisar.Size = New System.Drawing.Size(96, 34)
        Me.btnPesquisar.TabIndex = 87
        Me.btnPesquisar.Text = "Pesquisar"
        Me.btnPesquisar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(250, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(141, 20)
        Me.Label1.TabIndex = 85
        Me.Label1.Text = "Data Movimento :"
        '
        'btnNovo
        '
        Me.btnNovo.Location = New System.Drawing.Point(16, 33)
        Me.btnNovo.Name = "btnNovo"
        Me.btnNovo.Size = New System.Drawing.Size(75, 23)
        Me.btnNovo.TabIndex = 82
        Me.btnNovo.Text = "Novo"
        Me.btnNovo.UseVisualStyleBackColor = True
        '
        'btnSalvarBaixa
        '
        Me.btnSalvarBaixa.Enabled = False
        Me.btnSalvarBaixa.Location = New System.Drawing.Point(465, 79)
        Me.btnSalvarBaixa.Name = "btnSalvarBaixa"
        Me.btnSalvarBaixa.Size = New System.Drawing.Size(75, 23)
        Me.btnSalvarBaixa.TabIndex = 78
        Me.btnSalvarBaixa.Text = "Salvar"
        Me.btnSalvarBaixa.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(113, 12)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(74, 20)
        Me.Label12.TabIndex = 76
        Me.Label12.Text = "Borderô:"
        '
        'txtBaixa
        '
        Me.txtBaixa.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBaixa.Location = New System.Drawing.Point(116, 32)
        Me.txtBaixa.Name = "txtBaixa"
        Me.txtBaixa.Size = New System.Drawing.Size(105, 26)
        Me.txtBaixa.TabIndex = 77
        '
        'btnConsultarBordero
        '
        Me.btnConsultarBordero.Enabled = False
        Me.btnConsultarBordero.Location = New System.Drawing.Point(1051, 186)
        Me.btnConsultarBordero.Name = "btnConsultarBordero"
        Me.btnConsultarBordero.Size = New System.Drawing.Size(96, 34)
        Me.btnConsultarBordero.TabIndex = 81
        Me.btnConsultarBordero.Text = "Consultar Baixa"
        Me.btnConsultarBordero.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(896, 170)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(138, 20)
        Me.Label13.TabIndex = 79
        Me.Label13.Text = "Consultar Baixa :"
        '
        'txtConsultaBordero
        '
        Me.txtConsultaBordero.Enabled = False
        Me.txtConsultaBordero.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConsultaBordero.Location = New System.Drawing.Point(899, 192)
        Me.txtConsultaBordero.Name = "txtConsultaBordero"
        Me.txtConsultaBordero.Size = New System.Drawing.Size(114, 26)
        Me.txtConsultaBordero.TabIndex = 80
        '
        'grdFaturamento
        '
        Me.grdFaturamento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdFaturamento.Location = New System.Drawing.Point(12, 170)
        Me.grdFaturamento.Name = "grdFaturamento"
        Me.grdFaturamento.Size = New System.Drawing.Size(871, 490)
        Me.grdFaturamento.TabIndex = 2
        '
        'btnSair
        '
        Me.btnSair.Location = New System.Drawing.Point(1083, 643)
        Me.btnSair.Name = "btnSair"
        Me.btnSair.Size = New System.Drawing.Size(75, 23)
        Me.btnSair.TabIndex = 3
        Me.btnSair.Text = "Sair"
        Me.btnSair.UseVisualStyleBackColor = True
        '
        'lblFichasCanceladas
        '
        Me.lblFichasCanceladas.AutoSize = True
        Me.lblFichasCanceladas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFichasCanceladas.ForeColor = System.Drawing.Color.Red
        Me.lblFichasCanceladas.Location = New System.Drawing.Point(184, 299)
        Me.lblFichasCanceladas.Name = "lblFichasCanceladas"
        Me.lblFichasCanceladas.Size = New System.Drawing.Size(19, 20)
        Me.lblFichasCanceladas.TabIndex = 80
        Me.lblFichasCanceladas.Text = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Location = New System.Drawing.Point(10, 299)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(175, 20)
        Me.Label2.TabIndex = 79
        Me.Label2.Text = "Fichas Canceladas:"
        '
        'lblValorFichasCanceladas
        '
        Me.lblValorFichasCanceladas.AutoSize = True
        Me.lblValorFichasCanceladas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorFichasCanceladas.ForeColor = System.Drawing.Color.Red
        Me.lblValorFichasCanceladas.Location = New System.Drawing.Point(214, 327)
        Me.lblValorFichasCanceladas.Name = "lblValorFichasCanceladas"
        Me.lblValorFichasCanceladas.Size = New System.Drawing.Size(19, 20)
        Me.lblValorFichasCanceladas.TabIndex = 82
        Me.lblValorFichasCanceladas.Text = "0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Red
        Me.Label5.Location = New System.Drawing.Point(10, 326)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(213, 20)
        Me.Label5.TabIndex = 81
        Me.Label5.Text = "Valor Total Cancelado : "
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblValorFichasAbertas)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.lblFichaAberta)
        Me.GroupBox2.Controls.Add(Me.Label20)
        Me.GroupBox2.Controls.Add(Me.lblValorFichasProxDia)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.lblFichasProxDia)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.lblValorFichasBaixadas)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.lblFichasBaixadas)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.cboAcao)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.rdExtornar)
        Me.GroupBox2.Controls.Add(Me.rdIncluir)
        Me.GroupBox2.Controls.Add(Me.lblValorFichasCanceladas)
        Me.GroupBox2.Controls.Add(Me.txtLoteCodRecibo)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.lblFichasCanceladas)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Location = New System.Drawing.Point(889, 228)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(298, 414)
        Me.GroupBox2.TabIndex = 84
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Baixa"
        '
        'lblValorFichasAbertas
        '
        Me.lblValorFichasAbertas.AutoSize = True
        Me.lblValorFichasAbertas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorFichasAbertas.Location = New System.Drawing.Point(212, 214)
        Me.lblValorFichasAbertas.Name = "lblValorFichasAbertas"
        Me.lblValorFichasAbertas.Size = New System.Drawing.Size(19, 20)
        Me.lblValorFichasAbertas.TabIndex = 104
        Me.lblValorFichasAbertas.Text = "0"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(10, 214)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(205, 20)
        Me.Label18.TabIndex = 103
        Me.Label18.Text = "Valor Total em Aberto :"
        '
        'lblFichaAberta
        '
        Me.lblFichaAberta.AutoSize = True
        Me.lblFichaAberta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFichaAberta.Location = New System.Drawing.Point(171, 187)
        Me.lblFichaAberta.Name = "lblFichaAberta"
        Me.lblFichaAberta.Size = New System.Drawing.Size(19, 20)
        Me.lblFichaAberta.TabIndex = 102
        Me.lblFichaAberta.Text = "0"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(10, 187)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(163, 20)
        Me.Label20.TabIndex = 101
        Me.Label20.Text = "Fichas em Aberto:"
        '
        'lblValorFichasProxDia
        '
        Me.lblValorFichasProxDia.AutoSize = True
        Me.lblValorFichasProxDia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorFichasProxDia.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblValorFichasProxDia.Location = New System.Drawing.Point(205, 381)
        Me.lblValorFichasProxDia.Name = "lblValorFichasProxDia"
        Me.lblValorFichasProxDia.Size = New System.Drawing.Size(19, 20)
        Me.lblValorFichasProxDia.TabIndex = 100
        Me.lblValorFichasProxDia.Text = "0"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label14.Location = New System.Drawing.Point(10, 381)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(203, 20)
        Me.Label14.TabIndex = 99
        Me.Label14.Text = "Valor Total Próx. Dia : "
        '
        'lblFichasProxDia
        '
        Me.lblFichasProxDia.AutoSize = True
        Me.lblFichasProxDia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFichasProxDia.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblFichasProxDia.Location = New System.Drawing.Point(174, 354)
        Me.lblFichasProxDia.Name = "lblFichasProxDia"
        Me.lblFichasProxDia.Size = New System.Drawing.Size(19, 20)
        Me.lblFichasProxDia.TabIndex = 98
        Me.lblFichasProxDia.Text = "0"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.Label16.Location = New System.Drawing.Point(10, 354)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(161, 20)
        Me.Label16.TabIndex = 97
        Me.Label16.Text = "Fichas Próx. Dia :"
        '
        'lblValorFichasBaixadas
        '
        Me.lblValorFichasBaixadas.AutoSize = True
        Me.lblValorFichasBaixadas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorFichasBaixadas.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblValorFichasBaixadas.Location = New System.Drawing.Point(196, 271)
        Me.lblValorFichasBaixadas.Name = "lblValorFichasBaixadas"
        Me.lblValorFichasBaixadas.Size = New System.Drawing.Size(19, 20)
        Me.lblValorFichasBaixadas.TabIndex = 96
        Me.lblValorFichasBaixadas.Text = "0"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label8.Location = New System.Drawing.Point(10, 271)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(186, 20)
        Me.Label8.TabIndex = 95
        Me.Label8.Text = "Valor Total Baixado :"
        '
        'lblFichasBaixadas
        '
        Me.lblFichasBaixadas.AutoSize = True
        Me.lblFichasBaixadas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFichasBaixadas.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.lblFichasBaixadas.Location = New System.Drawing.Point(165, 244)
        Me.lblFichasBaixadas.Name = "lblFichasBaixadas"
        Me.lblFichasBaixadas.Size = New System.Drawing.Size(19, 20)
        Me.lblFichasBaixadas.TabIndex = 94
        Me.lblFichasBaixadas.Text = "0"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.Label10.Location = New System.Drawing.Point(10, 244)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(154, 20)
        Me.Label10.TabIndex = 93
        Me.Label10.Text = "Fichas Baixadas:"
        '
        'cboAcao
        '
        Me.cboAcao.FormattingEnabled = True
        Me.cboAcao.Items.AddRange(New Object() {"Baixar", "Cancelar", "Próx. Dia"})
        Me.cboAcao.Location = New System.Drawing.Point(10, 47)
        Me.cboAcao.Name = "cboAcao"
        Me.cboAcao.Size = New System.Drawing.Size(182, 24)
        Me.cboAcao.TabIndex = 92
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(7, 27)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(52, 20)
        Me.Label6.TabIndex = 91
        Me.Label6.Text = "Ação:"
        '
        'rdExtornar
        '
        Me.rdExtornar.AutoSize = True
        Me.rdExtornar.Location = New System.Drawing.Point(109, 87)
        Me.rdExtornar.Name = "rdExtornar"
        Me.rdExtornar.Size = New System.Drawing.Size(82, 21)
        Me.rdExtornar.TabIndex = 77
        Me.rdExtornar.TabStop = True
        Me.rdExtornar.Text = "Extornar"
        Me.rdExtornar.UseVisualStyleBackColor = True
        '
        'rdIncluir
        '
        Me.rdIncluir.AutoSize = True
        Me.rdIncluir.Checked = True
        Me.rdIncluir.Location = New System.Drawing.Point(13, 87)
        Me.rdIncluir.Name = "rdIncluir"
        Me.rdIncluir.Size = New System.Drawing.Size(66, 21)
        Me.rdIncluir.TabIndex = 76
        Me.rdIncluir.TabStop = True
        Me.rdIncluir.Text = "Incluir"
        Me.rdIncluir.UseVisualStyleBackColor = True
        '
        'txtLoteCodRecibo
        '
        Me.txtLoteCodRecibo.Enabled = False
        Me.txtLoteCodRecibo.Location = New System.Drawing.Point(13, 136)
        Me.txtLoteCodRecibo.Name = "txtLoteCodRecibo"
        Me.txtLoteCodRecibo.Size = New System.Drawing.Size(161, 22)
        Me.txtLoteCodRecibo.TabIndex = 75
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(10, 117)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(128, 20)
        Me.Label3.TabIndex = 74
        Me.Label3.Text = "Código Recibo :"
        '
        'FrmBaixa
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1199, 674)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnSair)
        Me.Controls.Add(Me.grdFaturamento)
        Me.Controls.Add(Me.btnConsultarBordero)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.txtConsultaBordero)
        Me.MinimizeBox = False
        Me.Name = "FrmBaixa"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Baixa de Recibos"
        Me.Panel1.ResumeLayout(False)
        CType(Me.grdMovimentacao, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.grdFaturamento, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents grdFaturamento As System.Windows.Forms.DataGridView
    Friend WithEvents btnSair As System.Windows.Forms.Button
    Friend WithEvents lblFichasCanceladas As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblValorFichasCanceladas As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnConsultarBordero As System.Windows.Forms.Button
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtConsultaBordero As System.Windows.Forms.TextBox
    Friend WithEvents btnSalvarBaixa As System.Windows.Forms.Button
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtBaixa As System.Windows.Forms.TextBox
    Friend WithEvents btnNovo As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtLoteCodRecibo As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents grdMovimentacao As System.Windows.Forms.DataGridView
    Friend WithEvents rdExtornar As System.Windows.Forms.RadioButton
    Friend WithEvents rdIncluir As System.Windows.Forms.RadioButton
    Friend WithEvents txtDataMov As System.Windows.Forms.MaskedTextBox
    Friend WithEvents btnPesquisar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboOperador As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblValorFichasAbertas As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents lblFichaAberta As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents lblValorFichasProxDia As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents lblFichasProxDia As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents lblValorFichasBaixadas As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblFichasBaixadas As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cboAcao As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cboCidade As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
End Class
