﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text
Imports Microsoft.Office.Interop
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared


Public Class FrmBordero

#Region "Enum"

    Private Enum ColunasGridFaturamento
        Cod_EAN_Cliente = 0
        Nome_contribuinte = 1
        Data_mov = 2
        Tipo_mov = 3
        Tipo_cobranca = 4
        Cod_Categoria = 5
        DDD = 6
        Telefone1 = 7
        Valor = 8
        Cod_recibo = 9
        Cidade = 10
    End Enum

    Private Enum ColunasGridHistorico
        DDD = 0
        Telefone = 1
        Valor = 2
        categoria = 3
        mesRef = 4
        Situacao = 5
        MotivoSituacao = 6
        Data_Situacao = 7
    End Enum

    Private Enum ColunasGridMovimentacao

        mov_contribuicao_id = 0
        tipo_mov = 1
        tipo_cobranca = 2
        cobrador = 3
        data_mov = 4
        total_fichas = 5
        valor_fichas = 6
    End Enum

    Enum xlsOption
        xlsOpen
    End Enum

#End Region

#Region "variaveis"

    Public telefone As String
    Private lstContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstContribuinteSelecionada As New List(Of CRetorno)
    Private v_ambiente As String
    Private rnContribuinte As RNContribuinte = New RNContribuinte
    Private rnRequisicao As RNRequisicao = New RNRequisicao
    Private contribuinte As CContribuinte = New CContribuinte
    Private lstrequisicao As New SortableBindingList(Of CContribuinte)
    Private lstrequisicaoSelecionada As New List(Of CContribuinte)
    Private lstMovimentacao As New SortableBindingList(Of CRequisicao)
    Public mNivelAcesso As Integer
    Public mUsuario As String
    Public mCodUsuario As Integer
    Public atualizou As Boolean
    Public bindingSourceContribuite As BindingSource = New BindingSource
    Public bindingSourceMovimentacao As BindingSource = New BindingSource
    Public cod_mov_contribuicao As Long


    Public Property NivelAcesso() As Integer
        Get
            Return mNivelAcesso
        End Get
        Set(ByVal value As Integer)
            mNivelAcesso = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return mUsuario
        End Get
        Set(ByVal value As String)
            mUsuario = value
        End Set
    End Property

    Public Property CodUsuario() As Integer
        Get
            Return mCodUsuario
        End Get
        Set(ByVal value As Integer)
            mCodUsuario = value
        End Set
    End Property

#End Region



#Region "Metodos"



    Private Sub carregaGrid()

        Dim objContribuintes As CContribuinte = New CContribuinte()

        Dim bindingSourceApolice As BindingSource = New BindingSource

        Try



            lstContribuintes = New SortableBindingList(Of CContribuinte)(rnContribuinte.CarregarGridOi(objContribuintes, "Remessa"))

            If (lstContribuintes.Count > 0) Then

                bindingSourceApolice.DataSource = lstContribuintes

                grdFaturamento.DataSource = bindingSourceApolice.DataSource

            End If


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CriarColunasGrid()


        Try

            grdFaturamento.DataSource = Nothing
            grdFaturamento.Columns.Clear()

            grdFaturamento.AutoGenerateColumns = False

            grdFaturamento.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            Dim column As New DataGridViewTextBoxColumn


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_EAN_Cliente"
            column.HeaderText = "Cód. EAN Cliente"
            column.Width = 80
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Nome_contribuinte"
            column.HeaderText = "Nome contribuinte"
            column.Width = 300
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Data_mov"
            column.HeaderText = "Data Movimentação"
            column.Width = 80
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Tipo_mov"
            column.HeaderText = "Tipo Movimentação"
            column.Width = 80
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Tipo_cobranca"
            column.HeaderText = "Tipo Cobranca"
            column.Width = 80
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_Categoria"
            column.HeaderText = "Cód Categoria"
            column.Width = 80
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)


            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "valor"
            column.HeaderText = "Valor"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            grdFaturamento.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_recibo"
            column.HeaderText = "Cód. Recibo"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            grdFaturamento.Columns.Add(column)

            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cidade"
            column.HeaderText = "Cidade"
            column.Width = 80
            column.ReadOnly = True
            column.Visible = True
            grdFaturamento.Columns.Add(column)




        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CriarColunasGridMovimentacao()

        Try

            grdMovimentacao.DataSource = Nothing
            grdMovimentacao.Columns.Clear()

            grdMovimentacao.AutoGenerateColumns = False

            grdMovimentacao.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft


            Dim column As New DataGridViewTextBoxColumn


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "mov_contribuicao_id"
            column.HeaderText = "Cód. Contribuicao"
            column.Width = 80
            column.ReadOnly = True
            grdMovimentacao.Columns.Add(column)

            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "tipo_mov"
            column.HeaderText = "Tipo movimentação"
            column.Width = 80
            column.ReadOnly = True
            grdMovimentacao.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "tipo_cobranca"
            column.HeaderText = "Tipo cobranca"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            grdMovimentacao.Columns.Add(column)

            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "cobrador"
            column.HeaderText = "Responsável"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            grdMovimentacao.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "data_mov"
            column.HeaderText = "Data movimentação"
            column.Width = 160
            column.ReadOnly = False
            column.Visible = True
            grdMovimentacao.Columns.Add(column)

            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "total_fichas"
            column.HeaderText = "Total fichas"
            column.Visible = True
            column.Width = 150
            column.ReadOnly = False
            grdMovimentacao.Columns.Add(column)

            '7
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "valor_fichas"
            column.HeaderText = "Valor fichas"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            grdMovimentacao.Columns.Add(column)




        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

#End Region

    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""
        Dim cidade As String = ""
        Dim dia As Integer = 0
        Dim operador_id As Integer = 0
        Dim rn As RNRequisicao = New RNRequisicao

        Try

            Me.Cursor = Cursors.WaitCursor

            carregaFichasFaturamento(cidade, dia, operador_id)

            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try

    End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Close()
    End Sub

    Private Sub FrmConsultaContribuinte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        CriarColunasGrid()
        CarregaComboOperador(0)
        CriarColunasGridMovimentacao()

        CarregaMovimentacoes()

        cboOperador.Enabled = True
        txtDataMov.Enabled = False
        btnPesquisar.Enabled = False
        txtConsultaBordero.Enabled = False
        btnConsultarBordero.Enabled = False
        btnSalvarBordero.Enabled = False




    End Sub


    Private Sub FrmLiberaFaturamento_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""

        Try


        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub carregaFichasFaturamento(ByVal cidade As String, ByVal dia As Integer, ByVal operador_id As Integer)
        Dim objRetorno As CRetorno = New CRetorno()
        Dim dt As DataTable = New DataTable
        Dim ds As DataSet = New DataSet
        Dim dr As DataRow
        Dim valor As Single = 0
        Dim strValor As String = ""
        Dim totalValorFichas As Decimal = 0


        Try

            grdFaturamento.DataSource = Nothing

            ds = rnRequisicao.CarregaFichasFaturamentoBordero(cidade, dia, operador_id)

            If Not ds Is Nothing Then
                If (ds.Tables(0).Rows.Count > 0) Then
                    If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                        dt = ds.Tables(0)
                    End If
                End If

                grdFaturamento.DataSource = dt


                If (grdFaturamento.Rows.Count = 0) Then

                    grdFaturamento.DataSource = Nothing
                    MsgBox("Não existe registros a serem processados!")

                Else

                    lblTotalFichas.Text = grdFaturamento.Rows.Count - 1

                    For Each col As DataGridViewRow In grdFaturamento.Rows

                        totalValorFichas = totalValorFichas + col.Cells(6).Value

                    Next

                    strValor = valor.ToString("C")
                    valor = totalValorFichas
                    lblValor.Text = valor.ToString("R$ #,###.00")

                End If
            Else
                grdFaturamento.DataSource = Nothing
                MsgBox("Não existe registros a serem processados!")
            End If

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CarregaComboOperador(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboOperador.DataSource = Nothing
            cboOperador.Items.Clear()
            cboOperador.DisplayMember = "Nome_Operador"
            cboOperador.ValueMember = "Cod_Operador"
            cboOperador.DataSource = rn_generico.BuscarOperador(0)

            If cboOperador.Items.Count > 0 And cod_operador = 0 Then
                cboOperador.SelectedIndex = 0
            ElseIf cboOperador.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperador.Items.Count - 1
                    For Each linha As CGenerico In cboOperador.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperador.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperador.SelectedIndex = 1
            End If

            cboOperador1.DataSource = Nothing
            cboOperador1.Items.Clear()
            cboOperador1.DisplayMember = "Nome_Operador"
            cboOperador1.ValueMember = "Cod_Operador"
            cboOperador1.DataSource = rn_generico.BuscarOperador(0)

            If cboOperador1.Items.Count > 0 And cod_operador = 0 Then
                cboOperador1.SelectedIndex = 0
            ElseIf cboOperador1.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperador1.Items.Count - 1
                    For Each linha As CGenerico In cboOperador1.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperador1.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperador1.SelectedIndex = 1
            End If
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaMovimentacoes()

        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim operador As String = ""
        Try

            If cboOperador.SelectedValue > 0 Then
                operador = cboOperador.Text
            End If

            lstMovimentacao = New SortableBindingList(Of CRequisicao)(rnRequisicao.CarregaMovimentacoes(0, "BORDERO", operador, txtDataMov.Text))


            If (lstMovimentacao.Count > 0) Then

                bindingSourceMovimentacao.DataSource = lstMovimentacao

                grdMovimentacao.DataSource = bindingSourceMovimentacao.DataSource

            Else
                ' grdMovimentacao.DataSource = Nothing
                ' MsgBox("Não existe registros a serem visualizados!")

            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub



    Private Sub btnImprimirRecibo_Click(sender As System.Object, e As System.EventArgs)
        Dim strReportName As String
        Dim frm As FrmRelatorios = New FrmRelatorios
        Try


            ' carrega o relatorio desejado
            strReportName = "ReciboLista1"

            'define o caminho e nome do relatorio
            Dim strReportPath As String = "C:\Digital\RPT" & "\" & strReportName & ".rpt"
            '
            'verifiqa se o arquivo existe
            If Not IO.File.Exists(strReportPath) Then
                Throw (New Exception("Relatorio não localizado :" & vbCrLf & strReportPath))
            End If
            '
            'instancia o relaorio e carrega
            Dim CR As New ReportDocument
            CR.Load(strReportPath)
            '
            ' atribui os parametros declarados aos objetos relacionados
            Dim crParameterDiscreteValue As ParameterDiscreteValue
            Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            Dim crParameterFieldLocation As ParameterFieldDefinition
            Dim crParameterValues As ParameterValues
            Dim crParameterDiscreteValue2 As ParameterDiscreteValue
            Dim crParameterFieldDefinitions2 As ParameterFieldDefinitions
            Dim crParameterFieldLocation2 As ParameterFieldDefinition
            Dim crParameterValues2 As ParameterValues
            '
            ' Pega a coleção de parametros do relatorio
            crParameterFieldDefinitions = CR.DataDefinition.ParameterFields
            crParameterFieldDefinitions2 = CR.DataDefinition.ParameterFields
            '
            ' define o primeiro parametro
            ' - pega o parametro e diz a ela para usar os valores atuais
            ' - define o valor do parametro
            ' - inclui e aplica o valor
            ' - repete para cada parametro se for o caso (não é o caso deste exemplo)

            '--------------------------- FILTRO 1
            ' Vamos usar o parametro 'situacao'
            crParameterFieldLocation = crParameterFieldDefinitions.Item("situacao")
            crParameterValues = crParameterFieldLocation.CurrentValues
            crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue

            'obtem o valor da caixa de texto
            crParameterDiscreteValue.Value = 2
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldLocation.ApplyCurrentValues(crParameterValues)

            '--------------------------- FILTRO 2
            ' Vamos usar o parametro 'mesRef'
            crParameterFieldLocation2 = crParameterFieldDefinitions2.Item("cod_mov_contribuicao")
            crParameterValues2 = crParameterFieldLocation2.CurrentValues
            crParameterDiscreteValue2 = New CrystalDecisions.Shared.ParameterDiscreteValue

            'obtem o valor da caixa de texto
            crParameterDiscreteValue2.Value = txtBordero.Text
            crParameterValues2.Add(crParameterDiscreteValue2)
            crParameterFieldLocation2.ApplyCurrentValues(crParameterValues2)


            '
            ' Define a fonte do controle Crystal Report Viewer como sendo o relatorio definido acima
            'frm.CrystalReportViewer1.ReportSource = CR


        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnNovo_Click(sender As System.Object, e As System.EventArgs) Handles btnNovo.Click
        Dim rnRequisicao As RNRequisicao = New RNRequisicao

        Try

            cboOperador1.Enabled = True
            txtDataMov.Enabled = True
            btnPesquisar.Enabled = True
            txtConsultaBordero.Enabled = True
            btnConsultarBordero.Enabled = True
            btnSalvarBordero.Enabled = True
            txtConsultaBordero.Enabled = True
            txtLoteCodCliente.Enabled = True
            txtBordero.Text = rnRequisicao.RetornaUltimaMovimentacao()

            grdFaturamento.DataSource = Nothing

            txtLoteCodCliente.Focus()


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub txtLoteCodCliente_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtLoteCodCliente.KeyPress
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim dt As DataTable = New DataTable
        Dim dt2 As DataTable = New DataTable
        Dim ds As DataSet = New DataSet
        Dim dr As DataRow
        Dim dr2 As DataRow
        Dim valor As Single = 0
        Dim strValor As String = ""
        Dim totalValorFichas As Decimal = 0
        Dim codRecibo As String = ""

        Try


            If e.KeyChar = Chr(Keys.Enter) Then 'Detecta se tecla Enter foi pressionada

                Me.Cursor = Cursors.WaitCursor

                codRecibo = txtLoteCodCliente.Text
                codRecibo = "*" + codRecibo + "*"

                ds = rnRequisicao.CarregaFichaPorCodRecibo(codRecibo)

                If (ds.Tables(0).Rows.Count > 0) Then
                    If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                        dt = ds.Tables(0)
                    End If


                    dr = dt.Rows(0)

                    If rdIncluir.Checked = True Then


                        For Each col As DataGridViewRow In grdFaturamento.Rows

                            If Not col.Cells(0).Value = Nothing Then
                                If dr("cod_ean_cliente") = col.Cells(0).Value Then
                                    MsgBox("Contibuinte já consta na lista para liberação")
                                    Exit Sub
                                End If
                            End If
                        Next

                        If Not grdFaturamento.DataSource Is Nothing Then
                            dt2 = grdFaturamento.DataSource
                        End If

                        If Not grdFaturamento.DataSource Is Nothing Then


                            dr2 = dt2.NewRow()
                            dr2("cod_ean_cliente") = dr("cod_ean_cliente")
                            dr2("nome_contribuinte") = dr("nome_contribuinte")
                            dr2("data_mov") = dr("data_mov")
                            dr2("tipo_mov") = "BORDERÔ"
                            dr2("tipo_cobranca") = "MENSAGEIRO"
                            dr2("cod_categoria") = dr("cod_categoria")
                            dr2("valor") = dr("valor")
                            dr2("cod_recibo") = dr("cod_recibo")
                            dr2("cidade") = dr("cidade")

                            dt2.Rows.Add(dr2)
                            grdFaturamento.DataSource = dt2

                        Else

                            grdFaturamento.DataSource = dt
                        End If

                    End If

                    If rdExtornar.Checked Then

                        For Each col As DataGridViewRow In grdFaturamento.Rows

                            If Not col.Cells(0).Value = Nothing Then
                                If dr("cod_ean_cliente") = col.Cells(0).Value Then
                                    grdFaturamento.Rows.RemoveAt(col.Index)
                                    Exit For
                                End If
                            End If
                        Next

                    End If

                    lblTotalFichas.Text = grdFaturamento.Rows.Count - 1

                    For Each col As DataGridViewRow In grdFaturamento.Rows

                        totalValorFichas = totalValorFichas + col.Cells(6).Value

                    Next

                    strValor = valor.ToString("C")
                    valor = totalValorFichas
                    lblValor.Text = valor.ToString("R$ #,###.00")

                    txtLoteCodCliente.Text = ""

                    Me.Cursor = Cursors.Default

                End If

                Me.Cursor = Cursors.Default

            End If

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return
        End Try
    End Sub



    Private Sub btnSalvarBordero_Click(sender As System.Object, e As System.EventArgs) Handles btnSalvarBordero.Click
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim ultimo_recibo As Long = 0
        Dim ReciboClicado As String
        Dim cod_cobradorClicado As Long
        Dim cobradorClicado As String
        Dim cod_bordero As Long
        Dim totalFIchas As Long = 0
        Dim totalValorFichas As Decimal = 0
        Dim bordero_id As Integer = 0
        Dim ClienteClicado As String = ""

        Try

            Me.Cursor = Cursors.WaitCursor

            If cboOperador1.SelectedValue <= 0 Or cboOperador1.Text = "Selecione..." Then
                MsgBox("Por favor selecione um cobrador")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If

            cod_bordero = txtBordero.Text
            cod_mov_contribuicao = txtBordero.Text 'verificar

            bordero_id = rnRequisicao.RetornaIdBordero()

            For Each linha As DataGridViewRow In grdFaturamento.Rows

                cod_cobradorClicado = cboOperador1.SelectedValue
                cobradorClicado = cboOperador1.Text
                ReciboClicado = linha.Cells(7).Value
                ClienteClicado = linha.Cells(0).Value

                rnRequisicao.Atualiza_Movimentacao(cod_mov_contribuicao, bordero_id, ReciboClicado, cod_cobradorClicado, cobradorClicado, ClienteClicado)

                rnRequisicao.GeraRegistroBordero(cod_mov_contribuicao, ReciboClicado, ClienteClicado, bordero_id)

            Next

            'salva os valores totais
            totalFIchas = grdFaturamento.Rows.Count - 1

            For Each col As DataGridViewRow In grdFaturamento.Rows

                totalValorFichas = totalValorFichas + col.Cells(6).Value

            Next

            lblValor.Text = totalValorFichas

            rnRequisicao.GravaNumerosMovimentacao(cod_mov_contribuicao, cboOperador1.Text, cboOperador1.SelectedValue, 1, totalFIchas, totalValorFichas, "ABERTO")
            
            MsgBox("Dados gravados com sucesso!", MsgBoxStyle.Information)



            cboOperador1.Enabled = False
            txtDataMov.Enabled = False
            btnPesquisar.Enabled = False
            txtConsultaBordero.Enabled = False
            btnConsultarBordero.Enabled = False
            btnSalvarBordero.Enabled = False

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return
        End Try

    End Sub

    Private Sub btnConsultarBordero_Click(sender As System.Object, e As System.EventArgs) Handles btnConsultarBordero.Click
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim cod_mov_contribuicao As Long
        Dim ds As DataSet
        Dim dt As DataTable = New DataTable
        Dim valor As Single = 0
        Dim strValor As String = ""
        Dim totalValorFichas As Decimal = 0

        Try

            Me.Cursor = Cursors.WaitCursor

            cod_mov_contribuicao = txtConsultaBordero.Text

            ds = rnRequisicao.CarregaMovimentacaoDescrBordero(cod_mov_contribuicao)


            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If

            grdFaturamento.DataSource = dt


            lblTotalFichas.Text = grdFaturamento.Rows.Count - 1

            For Each col As DataGridViewRow In grdFaturamento.Rows

                totalValorFichas = totalValorFichas + col.Cells(6).Value

            Next

            strValor = valor.ToString("C")
            valor = totalValorFichas
            lblValor.Text = valor.ToString("R$ #,###.00")

            txtLoteCodCliente.Text = ""

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return
        End Try



    End Sub

    Private Sub grdMovimentacao_CellContentDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdMovimentacao.CellContentDoubleClick
        Dim movimentoClicado As Long = 0
        Dim rn As RNGenerico = New RNGenerico
        Dim ds As DataSet
        Dim dt As DataTable = New DataTable
        Dim valor As Single = 0
        Dim strValor As String = ""
        Dim totalValorFichas As Decimal = 0

        Try

            Me.Cursor = Cursors.WaitCursor

            movimentoClicado = CType(grdMovimentacao.Rows(e.RowIndex).Cells(ColunasGridMovimentacao.mov_contribuicao_id).Value, Long)

            ds = rnRequisicao.CarregaMovimentacaoDescrBordero(movimentoClicado)


            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If

            grdFaturamento.DataSource = dt


            lblTotalFichas.Text = grdFaturamento.Rows.Count - 1

            For Each col As DataGridViewRow In grdFaturamento.Rows

                totalValorFichas = totalValorFichas + col.Cells(6).Value

            Next

            strValor = valor.ToString("C")
            valor = totalValorFichas
            lblValor.Text = valor.ToString("R$ #,###.00")

            txtLoteCodCliente.Text = ""
            txtLoteCodCliente.Enabled = True

            txtBordero.Text = movimentoClicado

            btnSalvarBordero.Enabled = True

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return
        End Try
    End Sub

    Private Sub btnPesquisar_Click_1(sender As System.Object, e As System.EventArgs) Handles btnPesquisar.Click
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim data_mov As String = ""
        Dim ds As DataSet
        Dim dt As DataTable = New DataTable
        Dim valor As Single = 0
        Dim strValor As String = ""
        Dim totalValorFichas As Decimal = 0

        Try

            Me.Cursor = Cursors.WaitCursor

            data_mov = txtDataMov.Text

            ds = rnRequisicao.CarregaMovimentacaoDescrBorderoDataMov(data_mov, cboOperador.Text)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If

            grdFaturamento.DataSource = dt


            lblTotalFichas.Text = grdFaturamento.Rows.Count - 1

            For Each col As DataGridViewRow In grdFaturamento.Rows

                totalValorFichas = totalValorFichas + col.Cells(6).Value

            Next

            strValor = valor.ToString("C")
            valor = totalValorFichas
            lblValor.Text = valor.ToString("R$ #,###.00")

            txtLoteCodCliente.Text = ""
            txtLoteCodCliente.Enabled = True

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return
        End Try
    End Sub

    
    Private Sub cboOperador_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOperador.SelectedValueChanged
        Try

            CarregaMovimentacoes()

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return
        End Try
    End Sub

    Private Sub txtDataMov_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtDataMov.TextChanged
        Dim data As String = ""

        Try
            data = Replace(txtDataMov.Text, "_", "")

            If IsDate(data) Then
                If Trim(data.Length) = 10 Then
                    CarregaMovimentacoes()
                End If
            End If

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return
        End Try
    End Sub
End Class