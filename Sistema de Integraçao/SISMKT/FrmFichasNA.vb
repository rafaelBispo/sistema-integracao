﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text
Imports Microsoft.Office.Interop


Public Class FrmFichasNA

#Region "Enum"

    Private Enum ColunasGridRemessa

        Selecao = 0
        Cod_EAN_CLIENTE = 1
        Nome = 2
        DDD = 3
        Telefone = 4
        Valor = 5
        Observacao = 6

    End Enum

    Private Enum ColunasGridHistorico

        Cod_EAN_Cliente = 0
        Nome_contribuinte = 1
        Data_mov = 2
        Tipo_mov = 3
        Tipo_cobranca = 4
        Cod_Categoria = 5
        DDD = 6
        Telefone1 = 7
        Valor = 8
        Cod_recibo = 9
        Cidade = 10

    End Enum

    Enum xlsOption
        xlsOpen
    End Enum

#End Region

#Region "variaveis"

    Public telefone As String
    Private lstContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstRetornoContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstContribuinteSelecionada As New List(Of CRetorno)
    Private v_ambiente As String
    Private rnContribuinte As RNContribuinte = New RNContribuinte
    Private rnRetorno As RNRetorno = New RNRetorno
    Private contribuinte As CContribuinte = New CContribuinte
    Private lstContribuintesOi As New SortableBindingList(Of CRetorno)
    Private lstListaTelefonica As New SortableBindingList(Of CListaTelefonica)
    Private lstContribuinteSelecionadaOi As New List(Of CRetorno)
    Private rnRequisicao As RNRequisicao = New RNRequisicao
    Private lstrequisicao As New SortableBindingList(Of CContribuinte)
    Private lstrequisicaoSelecionada As New List(Of CContribuinte)
    Public mNivelAcesso As Integer
    Public mUsuario As String
    Public mCodUsuario As Integer
    Public atualizou As Boolean
    Public bindingSourceContribuite As BindingSource = New BindingSource


    Public Property NivelAcesso() As Integer
        Get
            Return mNivelAcesso
        End Get
        Set(ByVal value As Integer)
            mNivelAcesso = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return mUsuario
        End Get
        Set(ByVal value As String)
            mUsuario = value
        End Set
    End Property

    Public Property CodUsuario() As Integer
        Get
            Return mCodUsuario
        End Get
        Set(ByVal value As Integer)
            mCodUsuario = value
        End Set
    End Property

#End Region



#Region "Metodos"



    Private Sub CriarColunasGrid()

        Try

            grdRemessa.DataSource = Nothing
            grdRemessa.Columns.Clear()

            grdRemessa.AutoGenerateColumns = False

            grdRemessa.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            Dim checkboxColumn As New DataGridViewCheckBoxColumn
            checkboxColumn.Width = 32
            checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "cod_ean_cliente"
            column.HeaderText = "Cod. EAN Cliente"
            column.Width = 100
            column.ReadOnly = False
            column.Visible = True
            grdRemessa.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "nome_contribuinte"
            column.HeaderText = "Nome Contribuinte"
            column.Width = 300
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DDD"
            column.HeaderText = "DDD"
            column.Width = 50
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)


            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Telefone1"
            column.HeaderText = "Telefone"
            column.Width = 80
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Dia"
            column.HeaderText = "Dia"
            column.Width = 30
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Valor"
            column.HeaderText = "Valor"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            grdRemessa.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_opera"
            column.HeaderText = "Cód Operador"
            column.Width = 80
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)


            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Observacao"
            column.HeaderText = "Observação"
            column.Width = 160
            column.ReadOnly = False
            column.Visible = True
            grdRemessa.Columns.Add(column)



        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CriarColunasGridHistorico()

        Try

            GrdHistorico.DataSource = Nothing
            GrdHistorico.Columns.Clear()

            GrdHistorico.AutoGenerateColumns = False

            GrdHistorico.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            'Dim checkboxColumn As New DataGridViewCheckBoxColumn
            'checkboxColumn.Width = 32
            'checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_EAN_Cliente"
            column.HeaderText = "Cód. EAN Cliente"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Nome_contribuinte"
            column.HeaderText = "Nome contribuinte"
            column.Width = 300
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Data_mov"
            column.HeaderText = "Data Movimentação"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Tipo_mov"
            column.HeaderText = "Tipo Movimentação"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Tipo_cobranca"
            column.HeaderText = "Tipo Cobranca"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_Categoria"
            column.HeaderText = "Cód Categoria"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DDD"
            column.HeaderText = "DDD"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Telefone"
            column.HeaderText = "Telefone"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "valor"
            column.HeaderText = "Valor"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            GrdHistorico.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_recibo"
            column.HeaderText = "Cód. Recibo"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            GrdHistorico.Columns.Add(column)

            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cidade"
            column.HeaderText = "Cidade"
            column.Width = 80
            column.ReadOnly = True
            column.Visible = True
            GrdHistorico.Columns.Add(column)




        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

#End Region

    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""

        Try

            Me.Cursor = Cursors.WaitCursor


            carregaFeedGridCategoriaContribuinte()
            'CarregaPrimeiraLinha()


            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try

    End Sub



    Private Sub carregaFeedGridCategoriaContribuinte()

        Dim objRetorno As CRetorno = New CRetorno()
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim totalValorFichas As Decimal
        Dim ds As DataSet = New DataSet
        Dim valor As Single
        Dim strValor As String
        Dim dia As Integer
        Dim cod_opera As Integer

        Try

            If (txtdiaPesquisa.Text = "") Then
                dia = 0
            Else
                dia = txtdiaPesquisa.Text
            End If

            If (cboOperador2.SelectedValue <= 0) Then
                cod_opera = 0
            Else
                cod_opera = cboOperador2.SelectedValue
            End If


            grdRemessa.DataSource = Nothing

            lstrequisicao = New SortableBindingList(Of CContribuinte)(rnRequisicao.CarregaFichasNaoAtendeu(cboCategoria.SelectedValue, cboCidadeLote.Text, dia, cod_opera))


            If (lstrequisicao.Count > 0) Then

                bindingSourceContribuite.DataSource = lstrequisicao

                grdRemessa.DataSource = bindingSourceContribuite.DataSource

                For Each col As DataGridViewRow In grdRemessa.Rows

                    totalValorFichas = totalValorFichas + col.Cells(5).Value

                Next

                strValor = valor.ToString("C")
                valor = totalValorFichas
                lblValor.Text = valor.ToString("R$ #,###.00")
                lblTotalFichas.Text = grdRemessa.Rows.Count - 1



            Else
                grdRemessa.DataSource = Nothing
                MsgBox("Não existe registros a serem processados, verifique se houve a liberação das fichas!")

            End If



        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub
    Private Sub btnRetorno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            FrmRetorno.Show()

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Close()
    End Sub

    Private Sub FrmConsultaContribuinte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load



        Me.Cursor = Cursors.WaitCursor

        CriarColunasGrid()
        CriarColunasGridHistorico()
        CarregaComboCategorias(0)
        CarregaComboCidades(0)
        CarregaComboCidadesLote(0)
        CarregaComboOperadorPesquisa(0)
        CarregaComboOperador(0)

        'carregaFeedGridCategoriaContribuinte()
        'CarregaPrimeiraLinha()

        Me.Cursor = Cursors.Default



        atualizou = True

    End Sub

#Region "Carrega Combos"

    Private Sub CarregaComboCategorias(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCategoria.DataSource = Nothing
            cboCategoria.Items.Clear()
            cboCategoria.DisplayMember = "Nome_Categoria"
            cboCategoria.ValueMember = "Cod_Categoria"
            cboCategoria.DataSource = rn_generico.BuscarCategoriasPorRequisicaoOperador(CodUsuario)

            If cboCategoria.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoria.SelectedIndex = 0
            ElseIf cboCategoria.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoria.Items.Count - 1
                    For Each linha As CGenerico In cboCategoria.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoria.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoria.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboCategoriasDetalhe(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCategoriaDetalhe.DataSource = Nothing
            cboCategoriaDetalhe.Items.Clear()
            cboCategoriaDetalhe.DisplayMember = "Nome_Categoria"
            cboCategoriaDetalhe.ValueMember = "Cod_Categoria"
            cboCategoriaDetalhe.DataSource = rn_generico.BuscarCategoriaPorOperador(CodUsuario)

            If cboCategoriaDetalhe.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoriaDetalhe.SelectedIndex = 0
            ElseIf cboCategoriaDetalhe.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoriaDetalhe.Items.Count - 1
                    For Each linha As CGenerico In cboCategoriaDetalhe.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoriaDetalhe.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoriaDetalhe.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboContribuinte()
        Dim rn_contribuinte As RNContribuinte = New RNContribuinte
        Dim filtro As CContribuinte = New CContribuinte

        cboNome.DataSource = Nothing
        cboNome.Items.Clear()
        cboNome.DisplayMember = "NomeCliente1"
        cboNome.ValueMember = "Cod_cliente"
        cboNome.DataSource = rn_contribuinte.CarregarGrid(filtro, "Contribuinte")

        If cboNome.Items.Count > 0 Then
            cboNome.SelectedIndex = 0
        End If


    End Sub

    Private Sub CarregaComboBairros(ByVal cod_bairro As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try



            cboBairro.DataSource = Nothing
            cboBairro.Items.Clear()
            cboBairro.DisplayMember = "Nome_Bairro"
            cboBairro.ValueMember = "Cod_Bairro"
            cboBairro.DataSource = rn_generico.BuscarBairros(0)

            If cboBairro.Items.Count > 0 And cod_bairro = 0 Then
                cboBairro.SelectedIndex = 0

            ElseIf cboBairro.Items.Count > 0 And cod_bairro <> 0 Then
                For i As Integer = 0 To cboBairro.Items.Count - 1
                    For Each linha As CGenerico In cboBairro.DataSource
                        If linha.Cod_Bairro = cod_bairro Then
                            cboBairro.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboBairro.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboOperador(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboOperador.DataSource = Nothing
            cboOperador.Items.Clear()
            cboOperador.DisplayMember = "Nome_Operador"
            cboOperador.ValueMember = "Cod_Operador"
            cboOperador.DataSource = rn_generico.BuscarOperador(0)

            If cboOperador.Items.Count > 0 And cod_operador = 0 Then
                cboOperador.SelectedIndex = 0
            ElseIf cboOperador.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperador.Items.Count - 1
                    For Each linha As CGenerico In cboOperador.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperador.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperador.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboOperadorPesquisa(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboOperador2.DataSource = Nothing
            cboOperador2.Items.Clear()
            cboOperador2.DisplayMember = "Nome_Operador"
            cboOperador2.ValueMember = "Cod_Operador"
            cboOperador2.DataSource = rn_generico.BuscarOperador(0)

            If cboOperador2.Items.Count > 0 And cod_operador = 0 Then
                cboOperador2.SelectedIndex = 0
            ElseIf cboOperador2.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperador2.Items.Count - 1
                    For Each linha As CGenerico In cboOperador2.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperador2.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperador2.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboEspecie(ByVal cod_especie As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try


            cboEspecie.DataSource = Nothing
            cboEspecie.Items.Clear()
            cboEspecie.DisplayMember = "Nome_Especie"
            cboEspecie.ValueMember = "Cod_Especie"
            cboEspecie.DataSource = rn_generico.BuscarEspecie(0)

            If cboEspecie.Items.Count > 0 And cod_especie = 0 Then
                cboEspecie.SelectedIndex = 0
            ElseIf cboEspecie.Items.Count > 0 And cod_especie <> 0 Then
                For i As Integer = 0 To cboEspecie.Items.Count - 1
                    For Each linha As CGenerico In cboEspecie.DataSource
                        If linha.Cod_Especie = cod_especie Then
                            cboEspecie.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboEspecie.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try



    End Sub

#End Region

    Private Sub FrmConsultaUltimaMovimentacao_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""

        Try
            If atualizou = False Then
                If cboCategoria.SelectedIndex = 0 Or cboCategoria.Text = "Selecione..." Then
                    Exit Sub
                End If

                Me.Cursor = Cursors.WaitCursor

                'carregaFeedGridCategoriaContribuinte()
                'CarregaPrimeiraLinha()


                Me.Cursor = Cursors.Default
                atualizou = True
            End If

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub



    Private Sub txtNome_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNome.TextChanged
        Dim texto As String = Nothing
        Dim linhaVazia As Integer = 0
        Dim contador As Integer = 0

        If txtNome.Text <> String.Empty Then

            linhaVazia = grdRemessa.Rows.Count - 1

            'percorre cada linha do DataGridView
            For Each linha As DataGridViewRow In grdRemessa.Rows

                'percorre cada célula da linha
                For Each celula As DataGridViewCell In grdRemessa.Rows(linha.Index).Cells
                    If Not contador = linhaVazia Then
                        'se a coluna for a coluna 2 (Nome) então verifica o criterio
                        If celula.ColumnIndex = 2 Then

                            If celula.Value.ToString <> "" Then
                                texto = IIf(celula.Value.ToString = Nothing, "", celula.Value.ToString)
                                'se o texto informado estiver contido na célula então seleciona toda linha
                                If texto.Contains(txtNome.Text) Then
                                    'seleciona a linha
                                    grdRemessa.CurrentCell = celula
                                    grdRemessa.CurrentCell = grdRemessa.Rows(celula.RowIndex).Cells(2)
                                    Exit Sub
                                End If
                            End If
                            contador = contador + 1
                        End If
                    End If

                Next

            Next

        End If

    End Sub

    Private Sub txtTelefone_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTelefone.TextChanged
        Dim texto As String = Nothing
        Dim linhaVazia As Integer = 0
        Dim contador As Integer = 0

        If txtTelefone.Text <> String.Empty Then

            linhaVazia = grdRemessa.Rows.Count - 1

            'percorre cada linha do DataGridView
            For Each linha As DataGridViewRow In grdRemessa.Rows

                'percorre cada célula da linha
                For Each celula As DataGridViewCell In grdRemessa.Rows(linha.Index).Cells
                    If Not contador = linhaVazia Then
                        'se a coluna for a coluna 1 (telefone) então verifica o criterio
                        If celula.ColumnIndex = 1 Then

                            If celula.Value.ToString <> "" Then
                                texto = IIf(celula.Value.ToString = Nothing, "", celula.Value.ToString)
                                'se o texto informado estiver contido na célula então seleciona toda linha
                                If texto.Contains(txtTelefone.Text) Then
                                    'seleciona a linha
                                    grdRemessa.CurrentCell = celula
                                    grdRemessa.CurrentCell = grdRemessa.Rows(celula.RowIndex).Cells(2)
                                    Exit Sub
                                End If
                            End If
                            contador = contador + 1
                        End If
                    End If

                Next

            Next

        End If
    End Sub



    Private Sub CarregaComboCidades(ByVal cod_cidade As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCidade.DataSource = Nothing
            cboCidade.Items.Clear()
            cboCidade.DisplayMember = "Nome_cidade"
            cboCidade.ValueMember = "Cod_Cidade"
            cboCidade.DataSource = rn_generico.BuscarCidades(0)

            If cboCidade.Items.Count = 0 Then
                cboCidade.SelectedIndex = 0
            ElseIf cboCidade.Items.Count > 0 And cod_cidade <> 0 Then

                For i As Integer = 0 To cboCidade.Items.Count - 1
                    For Each linha As CGenerico In cboCidade.DataSource
                        If linha.Cod_Cidade = cod_cidade Then
                            cboCidade.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next

            Else

                cboCidade.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CarregaComboCidadesLote(ByVal cod_cidade As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCidadeLote.DataSource = Nothing
            cboCidadeLote.Items.Clear()
            cboCidadeLote.DisplayMember = "Nome_cidade"
            cboCidadeLote.ValueMember = "Cod_Cidade"
            cboCidadeLote.DataSource = rn_generico.BuscarCidades(0)

            If cboCidadeLote.Items.Count = 0 Then
                cboCidadeLote.SelectedIndex = 0
            ElseIf cboCidadeLote.Items.Count > 0 And cod_cidade <> 0 Then

                For i As Integer = 0 To cboCidadeLote.Items.Count - 1
                    For Each linha As CGenerico In cboCidadeLote.DataSource
                        If linha.Cod_Cidade = cod_cidade Then
                            cboCidadeLote.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next

            Else

                cboCidadeLote.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub btnLibera_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLibera.Click
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim ClienteSelecionado As String
        Dim isErro As Integer = 0

        Try

            Me.Cursor = Cursors.WaitCursor

            For Each linha As DataGridViewRow In grdRemessa.Rows

                ClienteSelecionado = linha.Cells(1).Value

                If (Convert.ToBoolean(linha.Cells(ColunasGridRemessa.Selecao).Value)) = True Then

                    contribuinte = rnContribuinte.PesquisarPorCodBarra(ClienteSelecionado)

                    ClienteSelecionado = CType(linha.Cells(ColunasGridRemessa.Cod_EAN_CLIENTE).Value, String)

                    If Not rnRequisicao.RecusaEspecialParaMensal(4, ClienteSelecionado) = True Then
                        isErro = isErro + 1
                    End If
                End If
            Next

            If isErro = 0 Then
                MsgBox("Registro salvo com sucesso!", MsgBoxStyle.Information, "Atenção")
            Else
                MsgBox("Ocorreu um erro na confirmação de " + isErro + " fichas, verifique novamente para liberar!! ", MsgBoxStyle.Information, "Atenção")
            End If


            lblValor.Text = ""
            lblTotalFichas.Text = 0

            carregaFeedGridCategoriaContribuinte()

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnRecusa_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRecusa.Click
        Dim rnRequisicao As RNRequisicao = New RNRequisicao

        Try

            Me.Cursor = Cursors.WaitCursor

            If rnRequisicao.ConfirmarContribuicao(contribuinte, "N") = True Then
                MsgBox("Registro salvo com sucesso!", MsgBoxStyle.Information, "Atenção")
            Else
                MsgBox("Ocorreu um erro na inclusão do registro", MsgBoxStyle.Information, "Atenção")
            End If

            carregaFeedGridCategoriaContribuinte()
            'CarregaPrimeiraLinha()

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub grdRemessa_CellContentDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdRemessa.CellContentDoubleClick

        Dim ClienteClicado As String
        Dim TelefoneSelecionado As String = ""
        Dim rn As RNRequisicao = New RNRequisicao
        Dim rnContribuinte As RNContribuinte = New RNContribuinte

        Try

            ClienteClicado = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.Cod_EAN_CLIENTE).Value, String)

            TelefoneSelecionado = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.Telefone).Value, String)



            For Each ContribuinteRequisicao As CContribuinte In lstContribuintes
                If ContribuinteRequisicao.Cod_EAN_Cliente = ClienteClicado Then

                End If
            Next


            If (e.RowIndex > -1) Then

                lstRetornoContribuintes = New SortableBindingList(Of CContribuinte)(rn.CarregaHistoricoContribuinte(ClienteClicado))

                If (lstRetornoContribuintes.Count > 0) Then

                    bindingSourceContribuite.DataSource = lstRetornoContribuintes

                    GrdHistorico.DataSource = bindingSourceContribuite.DataSource

                Else

                    GrdHistorico.DataSource = Nothing

                End If


                contribuinte = rnContribuinte.PesquisarPorCodBarra(ClienteClicado)

                If (contribuinte.Cod_EAN_Cliente = Nothing) Then
                    contribuinte = rnContribuinte.PesquisaClienteTabelaRetido(ClienteClicado)
                End If


                If Not contribuinte Is Nothing Then
                    'popula a tela
                    txtCodigo.Text = contribuinte.CodCliente
                    txtNomeCliente1.Text = contribuinte.NomeCliente1
                    txtNomeCliente2.Text = contribuinte.NomeCliente2
                    txtCodBarra.Text = contribuinte.Cod_EAN_Cliente
                    txtCnpjCpf.Text = contribuinte.CNPJ_CPF
                    txtRg.Text = contribuinte.IE_CI
                    txtEmail.Text = contribuinte.Email
                    txtTelefone1.Text = contribuinte.Telefone1
                    txtTelefone2.Text = contribuinte.Telefone2
                    txtEndereco.Text = contribuinte.Endereco
                    txtUF.Text = contribuinte.UF
                    txtCEP.Text = contribuinte.CEP

                    txtRegiao.Text = contribuinte.Cod_Regiao

                    txtDtNc.Text = contribuinte.DataNascimento
                    txtDtNcMp.Text = contribuinte.DT_NC_MP
                    txtDtCadastro.Text = contribuinte.DataCadastro
                    txtReferencia.Text = contribuinte.Referencia

                    txtObs.Text = contribuinte.Observacao

                    txtMelhorDia.Text = contribuinte.DiaLimite
                    txtValDoacao.Text = Convert.ToDouble(contribuinte.Valor).ToString("R$ #,###.00")
                    txtDDD.Text = contribuinte.DDD

                    CarregaComboCidades(contribuinte.Cod_Cidade)
                    CarregaComboCategoriasDetalhe(contribuinte.Cod_Categoria)
                    CarregaComboOperador(contribuinte.Cod_Operador)
                    CarregaComboBairros(contribuinte.Cod_Bairro)
                    CarregaComboEspecie(contribuinte.Cod_Especie)



                    If contribuinte.Descontinuado = "S" Then
                        chkInativo.Checked = True
                    Else
                        chkInativo.Checked = False
                    End If

                    If contribuinte.Nao_pedir_extra = "S" Then
                        chkNaoPedirExtra.Checked = True
                    Else
                        chkNaoPedirExtra.Checked = False
                    End If

                    If contribuinte.Nao_Pedir_Mensal = "S" Then
                        chkNaoPedirMensal.Checked = True
                    Else
                        chkNaoPedirMensal.Checked = False
                    End If

                    If contribuinte.Tel_desligado = "S" Then
                        chkTelDesl.Checked = True
                    Else
                        chkTelDesl.Checked = False
                    End If

                    txtNomClienteConcluir.Text = contribuinte.NomeCliente1
                    txtDiaConcluir.Text = contribuinte.DiaLimite
                    txtDDDConcluir.Text = contribuinte.DDD
                    txtTelefoneConcluir.Text = contribuinte.Telefone1
                    txtValorConcluir.Text = Convert.ToDouble(contribuinte.Valor).ToString("#,###.00")


                End If

            End If

            txtNomClienteConcluir.Enabled = True
            txtDiaConcluir.Enabled = True
            txtDDDConcluir.Enabled = True
            txtTelefoneConcluir.Enabled = True
            txtValorConcluir.Enabled = True



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub


    Private Sub CarregaPrimeiraLinha()
        Dim ClienteClicado As String
        Dim TelefoneSelecionado As String = ""
        Dim rn As RNRequisicao = New RNRequisicao
        Dim rnContribuinte As RNContribuinte = New RNContribuinte

        Try

            ClienteClicado = CType(grdRemessa.Rows(0).Cells(ColunasGridRemessa.Cod_EAN_CLIENTE).Value, String)

            TelefoneSelecionado = CType(grdRemessa.Rows(0).Cells(ColunasGridRemessa.Telefone).Value, String)



            For Each ContribuinteRequisicao As CContribuinte In lstContribuintes
                If ContribuinteRequisicao.Cod_EAN_Cliente = ClienteClicado Then

                End If
            Next


            If Not (ClienteClicado Is Nothing) Then

                lstRetornoContribuintes = New SortableBindingList(Of CContribuinte)(rn.CarregaHistoricoContribuinte(ClienteClicado))

                If (lstRetornoContribuintes.Count > 0) Then

                    bindingSourceContribuite.DataSource = lstRetornoContribuintes

                    GrdHistorico.DataSource = bindingSourceContribuite.DataSource

                Else

                    GrdHistorico.DataSource = Nothing

                End If


                contribuinte = rnContribuinte.PesquisarPorCodBarra(ClienteClicado)

                If (contribuinte.Cod_EAN_Cliente = Nothing) Then
                    contribuinte = rnContribuinte.PesquisaClienteTabelaRetido(ClienteClicado)
                End If


                If Not contribuinte Is Nothing Then
                    'popula a tela
                    txtCodigo.Text = contribuinte.CodCliente
                    txtNomeCliente1.Text = contribuinte.NomeCliente1
                    txtNomeCliente2.Text = contribuinte.NomeCliente2
                    txtCodBarra.Text = contribuinte.Cod_EAN_Cliente
                    txtCnpjCpf.Text = contribuinte.CNPJ_CPF
                    txtRg.Text = contribuinte.IE_CI
                    txtEmail.Text = contribuinte.Email
                    txtTelefone1.Text = contribuinte.Telefone1
                    txtTelefone2.Text = contribuinte.Telefone2
                    txtEndereco.Text = contribuinte.Endereco
                    txtUF.Text = contribuinte.UF
                    txtCEP.Text = contribuinte.CEP

                    txtRegiao.Text = contribuinte.Cod_Regiao

                    txtDtNc.Text = contribuinte.DataNascimento
                    txtDtNcMp.Text = contribuinte.DT_NC_MP
                    txtDtCadastro.Text = contribuinte.DataCadastro
                    txtReferencia.Text = contribuinte.Referencia

                    txtObs.Text = contribuinte.Observacao

                    txtMelhorDia.Text = contribuinte.DiaLimite
                    txtValDoacao.Text = Convert.ToDouble(contribuinte.Valor).ToString("R$ #,###.00")
                    txtDDD.Text = contribuinte.DDD

                    CarregaComboCidades(contribuinte.Cod_Cidade)
                    CarregaComboCategoriasDetalhe(contribuinte.Cod_Categoria)
                    CarregaComboOperador(contribuinte.Cod_Operador)
                    CarregaComboBairros(contribuinte.Cod_Bairro)
                    CarregaComboEspecie(contribuinte.Cod_Especie)



                    If contribuinte.Descontinuado = "S" Then
                        chkInativo.Checked = True
                    Else
                        chkInativo.Checked = False
                    End If

                    If contribuinte.Nao_pedir_extra = "S" Then
                        chkNaoPedirExtra.Checked = True
                    Else
                        chkNaoPedirExtra.Checked = False
                    End If

                    If contribuinte.Nao_Pedir_Mensal = "S" Then
                        chkNaoPedirMensal.Checked = True
                    Else
                        chkNaoPedirMensal.Checked = False
                    End If

                    If contribuinte.Tel_desligado = "S" Then
                        chkTelDesl.Checked = True
                    Else
                        chkTelDesl.Checked = False
                    End If

                End If

            End If

            'contribuinte = rnContribuinte.PesquisarContribuintePorTelefone(telefone, 1)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub


    Private Sub btnNaoContribuir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNaoContribuir.Click
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim Daogenerico As GenericoDAO = New GenericoDAO
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable

        Try

            Me.Cursor = Cursors.WaitCursor

            ds = Daogenerico.BuscarOperador(CodUsuario)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If

            contribuinte.Cod_Usuario = CodUsuario
            contribuinte.Cod_opera = dt.Rows(0).Item(0)
            contribuinte.Operador = dt.Rows(0).Item(1)

            If rnRequisicao.ConfirmarContribuicao(contribuinte, "N") = True Then
                MsgBox("Registro salvo com sucesso!", MsgBoxStyle.Information, "Atenção")
            Else
                MsgBox("Ocorreu um erro na inclusão do registro", MsgBoxStyle.Information, "Atenção")
            End If

            carregaFeedGridCategoriaContribuinte()
            CarregaPrimeiraLinha()

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnContribuir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContribuir.Click
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim Daogenerico As GenericoDAO = New GenericoDAO
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable

        Try

            Me.Cursor = Cursors.WaitCursor

            ds = Daogenerico.BuscarOperador(CodUsuario)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If

            contribuinte.Cod_Usuario = CodUsuario
            contribuinte.Cod_opera = dt.Rows(0).Item(0)
            contribuinte.Operador = dt.Rows(0).Item(1)

            If rnRequisicao.ConfirmarContribuicao(contribuinte, "S") = True Then
                MsgBox("Registro salvo com sucesso!", MsgBoxStyle.Information, "Atenção")
            Else
                MsgBox("Ocorreu um erro na inclusão do registro", MsgBoxStyle.Information, "Atenção")
            End If

            carregaFeedGridCategoriaContribuinte()
            CarregaPrimeiraLinha()

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub chkHeader_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkHeader.CheckedChanged

        Me.Cursor = Cursors.WaitCursor

        Dim i As Integer
        Dim valorCheckHeader As Boolean

        valorCheckHeader = chkHeader.Checked

        For i = 0 To grdRemessa.RowCount - 1

            If valorCheckHeader = True Then
                grdRemessa(ColunasGridRemessa.Selecao, i).Value = valorCheckHeader
            Else
                grdRemessa(ColunasGridRemessa.Selecao, i).Value = valorCheckHeader
            End If
        Next i

        Me.Cursor = Cursors.Default
        grdRemessa.EndEdit()
    End Sub

    Private Sub grdRemessa_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdRemessa.CellContentClick
        Dim ClienteClicado As String
        Dim TelefoneSelecionado As String = ""
        Dim rn As RNRequisicao = New RNRequisicao
        Dim rnContribuinte As RNContribuinte = New RNContribuinte

        Try

            ClienteClicado = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.Cod_EAN_CLIENTE).Value, String)

            TelefoneSelecionado = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.Telefone).Value, String)



            For Each ContribuinteRequisicao As CContribuinte In lstContribuintes
                If ContribuinteRequisicao.Cod_EAN_Cliente = ClienteClicado Then

                End If
            Next


            If (e.RowIndex > -1) Then

                lstRetornoContribuintes = New SortableBindingList(Of CContribuinte)(rn.CarregaHistoricoContribuinte(ClienteClicado))

                If (lstRetornoContribuintes.Count > 0) Then

                    bindingSourceContribuite.DataSource = lstRetornoContribuintes

                    GrdHistorico.DataSource = bindingSourceContribuite.DataSource

                Else

                    GrdHistorico.DataSource = Nothing

                End If


                contribuinte = rnContribuinte.PesquisarPorCodBarra(ClienteClicado)

                If (contribuinte.Cod_EAN_Cliente = Nothing) Then
                    contribuinte = rnContribuinte.PesquisaClienteTabelaRetido(ClienteClicado)
                End If


                If Not contribuinte Is Nothing Then
                    'popula a tela
                    txtCodigo.Text = contribuinte.CodCliente
                    txtNomeCliente1.Text = contribuinte.NomeCliente1
                    txtNomeCliente2.Text = contribuinte.NomeCliente2
                    txtCodBarra.Text = contribuinte.Cod_EAN_Cliente
                    txtCnpjCpf.Text = contribuinte.CNPJ_CPF
                    txtRg.Text = contribuinte.IE_CI
                    txtEmail.Text = contribuinte.Email
                    txtTelefone1.Text = contribuinte.Telefone1
                    txtTelefone2.Text = contribuinte.Telefone2
                    txtEndereco.Text = contribuinte.Endereco
                    txtUF.Text = contribuinte.UF
                    txtCEP.Text = contribuinte.CEP

                    txtRegiao.Text = contribuinte.Cod_Regiao

                    txtDtNc.Text = contribuinte.DataNascimento
                    txtDtNcMp.Text = contribuinte.DT_NC_MP
                    txtDtCadastro.Text = contribuinte.DataCadastro
                    txtReferencia.Text = contribuinte.Referencia

                    txtObs.Text = contribuinte.Observacao

                    txtMelhorDia.Text = contribuinte.DiaLimite
                    txtValDoacao.Text = Convert.ToDouble(contribuinte.Valor).ToString("R$ #,###.00")
                    txtDDD.Text = contribuinte.DDD

                    CarregaComboCidades(contribuinte.Cod_Cidade)
                    CarregaComboCategoriasDetalhe(contribuinte.Cod_Categoria)
                    CarregaComboOperador(contribuinte.Cod_Operador)
                    CarregaComboBairros(contribuinte.Cod_Bairro)
                    CarregaComboEspecie(contribuinte.Cod_Especie)



                    If contribuinte.Descontinuado = "S" Then
                        chkInativo.Checked = True
                    Else
                        chkInativo.Checked = False
                    End If

                    If contribuinte.Nao_pedir_extra = "S" Then
                        chkNaoPedirExtra.Checked = True
                    Else
                        chkNaoPedirExtra.Checked = False
                    End If

                    If contribuinte.Nao_Pedir_Mensal = "S" Then
                        chkNaoPedirMensal.Checked = True
                    Else
                        chkNaoPedirMensal.Checked = False
                    End If

                    If contribuinte.Tel_desligado = "S" Then
                        chkTelDesl.Checked = True
                    Else
                        chkTelDesl.Checked = False
                    End If

                    txtNomClienteConcluir.Text = contribuinte.NomeCliente1
                    txtDiaConcluir.Text = contribuinte.DiaLimite
                    txtDDDConcluir.Text = contribuinte.DDD
                    txtTelefoneConcluir.Text = contribuinte.Telefone1
                    txtValorConcluir.Text = Convert.ToDouble(contribuinte.Valor).ToString("#,###.00")


                End If

            End If

            txtNomClienteConcluir.Enabled = True
            txtDiaConcluir.Enabled = True
            txtDDDConcluir.Enabled = True
            txtTelefoneConcluir.Enabled = True
            txtValorConcluir.Enabled = True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub
End Class