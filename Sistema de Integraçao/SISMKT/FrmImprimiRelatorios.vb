﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class FrmImprimiRelatorios


    Dim objContribuintes As CContribuinte = New CContribuinte()
    Dim bindingSourceApolice As BindingSource = New BindingSource
    Private lstContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstContribuinteSe As New List(Of CContribuinte)


#Region "variaveis"

    Public pCod_ean As String
    Public pNome As String
    Public pTelefone As String
    Public psituacao As String
    Public pMesAnoRef As String
    Public pValor As Decimal
    Public MesAtuacao As String
    Public NextMonth As Date
    Public Consulta As Boolean = False

#End Region

#Region "Propriedades"

    Public Property Cod_Ean_Cliente() As String
        Get
            Return pCod_ean
        End Get
        Set(ByVal value As String)
            pCod_ean = value
        End Set
    End Property

    Public Property Telefone() As String
        Get
            Return pTelefone
        End Get
        Set(ByVal value As String)
            pTelefone = value
        End Set
    End Property

    Public Property Nome() As String
        Get
            Return pNome
        End Get
        Set(ByVal value As String)
            pNome = value
        End Set
    End Property

    Public Property Valor() As String
        Get
            Return pValor
        End Get
        Set(ByVal value As String)
            pValor = value
        End Set
    End Property


#End Region


    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Close()
    End Sub

    Private Sub FrmRelatorios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

End Class