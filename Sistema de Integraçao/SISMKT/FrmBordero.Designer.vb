﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmBordero
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.grdMovimentacao = New System.Windows.Forms.DataGridView()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cboOperador1 = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtDataMov = New System.Windows.Forms.MaskedTextBox()
        Me.btnPesquisar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboOperador = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnNovo = New System.Windows.Forms.Button()
        Me.btnSalvarBordero = New System.Windows.Forms.Button()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtBordero = New System.Windows.Forms.TextBox()
        Me.btnConsultarBordero = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtConsultaBordero = New System.Windows.Forms.TextBox()
        Me.grdFaturamento = New System.Windows.Forms.DataGridView()
        Me.btnSair = New System.Windows.Forms.Button()
        Me.lblTotalFichas = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblValor = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.rdExtornar = New System.Windows.Forms.RadioButton()
        Me.rdIncluir = New System.Windows.Forms.RadioButton()
        Me.txtLoteCodCliente = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.grdMovimentacao, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.grdFaturamento, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.grdMovimentacao)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1146, 156)
        Me.Panel1.TabIndex = 1
        '
        'grdMovimentacao
        '
        Me.grdMovimentacao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdMovimentacao.Location = New System.Drawing.Point(554, 28)
        Me.grdMovimentacao.Name = "grdMovimentacao"
        Me.grdMovimentacao.Size = New System.Drawing.Size(588, 122)
        Me.grdMovimentacao.TabIndex = 79
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cboOperador1)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txtDataMov)
        Me.GroupBox1.Controls.Add(Me.btnPesquisar)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cboOperador)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.btnNovo)
        Me.GroupBox1.Controls.Add(Me.btnSalvarBordero)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.txtBordero)
        Me.GroupBox1.Location = New System.Drawing.Point(-1, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(546, 125)
        Me.GroupBox1.TabIndex = 78
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Lote"
        '
        'cboOperador1
        '
        Me.cboOperador1.FormattingEnabled = True
        Me.cboOperador1.Location = New System.Drawing.Point(140, 84)
        Me.cboOperador1.Name = "cboOperador1"
        Me.cboOperador1.Size = New System.Drawing.Size(256, 24)
        Me.cboOperador1.TabIndex = 90
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(137, 63)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(83, 20)
        Me.Label6.TabIndex = 89
        Me.Label6.Text = "Cobrador:"
        '
        'txtDataMov
        '
        Me.txtDataMov.Enabled = False
        Me.txtDataMov.Location = New System.Drawing.Point(430, 34)
        Me.txtDataMov.Mask = "##/##/####"
        Me.txtDataMov.Name = "txtDataMov"
        Me.txtDataMov.Size = New System.Drawing.Size(100, 22)
        Me.txtDataMov.TabIndex = 88
        '
        'btnPesquisar
        '
        Me.btnPesquisar.Enabled = False
        Me.btnPesquisar.Location = New System.Drawing.Point(439, 63)
        Me.btnPesquisar.Name = "btnPesquisar"
        Me.btnPesquisar.Size = New System.Drawing.Size(96, 23)
        Me.btnPesquisar.TabIndex = 87
        Me.btnPesquisar.Text = "Pesquisar"
        Me.btnPesquisar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(406, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(141, 20)
        Me.Label1.TabIndex = 85
        Me.Label1.Text = "Data Movimento :"
        '
        'cboOperador
        '
        Me.cboOperador.FormattingEnabled = True
        Me.cboOperador.Location = New System.Drawing.Point(140, 34)
        Me.cboOperador.Name = "cboOperador"
        Me.cboOperador.Size = New System.Drawing.Size(256, 24)
        Me.cboOperador.TabIndex = 84
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(137, 13)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(225, 20)
        Me.Label4.TabIndex = 83
        Me.Label4.Text = "Responsavel pela Liberação:"
        '
        'btnNovo
        '
        Me.btnNovo.Location = New System.Drawing.Point(39, 75)
        Me.btnNovo.Name = "btnNovo"
        Me.btnNovo.Size = New System.Drawing.Size(75, 23)
        Me.btnNovo.TabIndex = 82
        Me.btnNovo.Text = "Novo"
        Me.btnNovo.UseVisualStyleBackColor = True
        '
        'btnSalvarBordero
        '
        Me.btnSalvarBordero.Enabled = False
        Me.btnSalvarBordero.Location = New System.Drawing.Point(439, 96)
        Me.btnSalvarBordero.Name = "btnSalvarBordero"
        Me.btnSalvarBordero.Size = New System.Drawing.Size(96, 23)
        Me.btnSalvarBordero.TabIndex = 78
        Me.btnSalvarBordero.Text = "Salvar"
        Me.btnSalvarBordero.UseVisualStyleBackColor = True
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(6, 16)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(74, 20)
        Me.Label12.TabIndex = 76
        Me.Label12.Text = "Borderô:"
        '
        'txtBordero
        '
        Me.txtBordero.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBordero.Location = New System.Drawing.Point(11, 38)
        Me.txtBordero.Name = "txtBordero"
        Me.txtBordero.ReadOnly = True
        Me.txtBordero.Size = New System.Drawing.Size(105, 26)
        Me.txtBordero.TabIndex = 77
        '
        'btnConsultarBordero
        '
        Me.btnConsultarBordero.Enabled = False
        Me.btnConsultarBordero.Location = New System.Drawing.Point(977, 241)
        Me.btnConsultarBordero.Name = "btnConsultarBordero"
        Me.btnConsultarBordero.Size = New System.Drawing.Size(96, 34)
        Me.btnConsultarBordero.TabIndex = 81
        Me.btnConsultarBordero.Text = "Consultar Borderô"
        Me.btnConsultarBordero.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(957, 186)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(156, 20)
        Me.Label13.TabIndex = 79
        Me.Label13.Text = "Consultar Borderô :"
        '
        'txtConsultaBordero
        '
        Me.txtConsultaBordero.Enabled = False
        Me.txtConsultaBordero.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtConsultaBordero.Location = New System.Drawing.Point(960, 207)
        Me.txtConsultaBordero.Name = "txtConsultaBordero"
        Me.txtConsultaBordero.Size = New System.Drawing.Size(114, 26)
        Me.txtConsultaBordero.TabIndex = 80
        '
        'grdFaturamento
        '
        Me.grdFaturamento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdFaturamento.Location = New System.Drawing.Point(12, 174)
        Me.grdFaturamento.Name = "grdFaturamento"
        Me.grdFaturamento.Size = New System.Drawing.Size(928, 505)
        Me.grdFaturamento.TabIndex = 2
        '
        'btnSair
        '
        Me.btnSair.Location = New System.Drawing.Point(946, 637)
        Me.btnSair.Name = "btnSair"
        Me.btnSair.Size = New System.Drawing.Size(75, 23)
        Me.btnSair.TabIndex = 3
        Me.btnSair.Text = "Sair"
        Me.btnSair.UseVisualStyleBackColor = True
        '
        'lblTotalFichas
        '
        Me.lblTotalFichas.AutoSize = True
        Me.lblTotalFichas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalFichas.Location = New System.Drawing.Point(10, 143)
        Me.lblTotalFichas.Name = "lblTotalFichas"
        Me.lblTotalFichas.Size = New System.Drawing.Size(19, 20)
        Me.lblTotalFichas.TabIndex = 80
        Me.lblTotalFichas.Text = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 115)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(145, 20)
        Me.Label2.TabIndex = 79
        Me.Label2.Text = "Total de Fichas:"
        '
        'lblValor
        '
        Me.lblValor.AutoSize = True
        Me.lblValor.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValor.Location = New System.Drawing.Point(10, 212)
        Me.lblValor.Name = "lblValor"
        Me.lblValor.Size = New System.Drawing.Size(19, 20)
        Me.lblValor.TabIndex = 82
        Me.lblValor.Text = "0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(10, 175)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(113, 20)
        Me.Label5.TabIndex = 81
        Me.Label5.Text = "Valor Total :"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.rdExtornar)
        Me.GroupBox2.Controls.Add(Me.rdIncluir)
        Me.GroupBox2.Controls.Add(Me.lblValor)
        Me.GroupBox2.Controls.Add(Me.txtLoteCodCliente)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.lblTotalFichas)
        Me.GroupBox2.Controls.Add(Me.Label2)
        Me.GroupBox2.Location = New System.Drawing.Point(946, 283)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(212, 295)
        Me.GroupBox2.TabIndex = 84
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Incluir Manualmente"
        '
        'rdExtornar
        '
        Me.rdExtornar.AutoSize = True
        Me.rdExtornar.Location = New System.Drawing.Point(105, 31)
        Me.rdExtornar.Name = "rdExtornar"
        Me.rdExtornar.Size = New System.Drawing.Size(82, 21)
        Me.rdExtornar.TabIndex = 77
        Me.rdExtornar.TabStop = True
        Me.rdExtornar.Text = "Extornar"
        Me.rdExtornar.UseVisualStyleBackColor = True
        '
        'rdIncluir
        '
        Me.rdIncluir.AutoSize = True
        Me.rdIncluir.Checked = True
        Me.rdIncluir.Location = New System.Drawing.Point(9, 31)
        Me.rdIncluir.Name = "rdIncluir"
        Me.rdIncluir.Size = New System.Drawing.Size(66, 21)
        Me.rdIncluir.TabIndex = 76
        Me.rdIncluir.TabStop = True
        Me.rdIncluir.Text = "Incluir"
        Me.rdIncluir.UseVisualStyleBackColor = True
        '
        'txtLoteCodCliente
        '
        Me.txtLoteCodCliente.Enabled = False
        Me.txtLoteCodCliente.Location = New System.Drawing.Point(9, 80)
        Me.txtLoteCodCliente.Name = "txtLoteCodCliente"
        Me.txtLoteCodCliente.Size = New System.Drawing.Size(161, 22)
        Me.txtLoteCodCliente.TabIndex = 75
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 61)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(128, 20)
        Me.Label3.TabIndex = 74
        Me.Label3.Text = "Código Recibo :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(552, 5)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(144, 20)
        Me.Label7.TabIndex = 86
        Me.Label7.Text = "Fichas a Borderar"
        '
        'FrmBordero
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1197, 686)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnSair)
        Me.Controls.Add(Me.grdFaturamento)
        Me.Controls.Add(Me.btnConsultarBordero)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.txtConsultaBordero)
        Me.MinimizeBox = False
        Me.Name = "FrmBordero"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Borderô"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.grdMovimentacao, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.grdFaturamento, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents grdFaturamento As System.Windows.Forms.DataGridView
    Friend WithEvents btnSair As System.Windows.Forms.Button
    Friend WithEvents lblTotalFichas As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblValor As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnConsultarBordero As System.Windows.Forms.Button
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtConsultaBordero As System.Windows.Forms.TextBox
    Friend WithEvents btnSalvarBordero As System.Windows.Forms.Button
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtBordero As System.Windows.Forms.TextBox
    Friend WithEvents btnNovo As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtLoteCodCliente As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents grdMovimentacao As System.Windows.Forms.DataGridView
    Friend WithEvents rdExtornar As System.Windows.Forms.RadioButton
    Friend WithEvents rdIncluir As System.Windows.Forms.RadioButton
    Friend WithEvents cboOperador As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtDataMov As System.Windows.Forms.MaskedTextBox
    Friend WithEvents btnPesquisar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboOperador1 As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
End Class
