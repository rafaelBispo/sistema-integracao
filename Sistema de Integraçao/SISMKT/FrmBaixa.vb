﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text
Imports Microsoft.Office.Interop
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared


Public Class FrmBaixa

#Region "Enum"

    Private Enum ColunasGridFaturamento
        Cod_EAN_Cliente = 0
        Nome_contribuinte = 1
        Data_mov = 2
        Tipo_mov = 3
        Tipo_cobranca = 4
        Cod_Categoria = 5
        DDD = 6
        Telefone1 = 7
        Valor = 8
        Cod_recibo = 9
        Cidade = 10
    End Enum

    Private Enum ColunasGridHistorico
        DDD = 0
        Telefone = 1
        Valor = 2
        categoria = 3
        mesRef = 4
        Situacao = 5
        MotivoSituacao = 6
        Data_Situacao = 7
    End Enum

    Private Enum ColunasGridMovimentacao

        mov_contribuicao_id = 0
        tipo_mov = 1
        tipo_cobranca = 2
        cobrador = 3
        data_mov = 4
        total_fichas = 5
        valor_fichas = 6
    End Enum

    Enum xlsOption
        xlsOpen
    End Enum

#End Region

#Region "variaveis"

    Public telefone As String
    Private lstContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstContribuinteSelecionada As New List(Of CRetorno)
    Private v_ambiente As String
    Private rnContribuinte As RNContribuinte = New RNContribuinte
    Private rnRequisicao As RNRequisicao = New RNRequisicao
    Private contribuinte As CContribuinte = New CContribuinte
    Private lstrequisicao As New SortableBindingList(Of CContribuinte)
    Private lstrequisicaoSelecionada As New List(Of CContribuinte)
    Private lstMovimentacao As New SortableBindingList(Of CRequisicao)
    Public mNivelAcesso As Integer
    Public mUsuario As String
    Public mCodUsuario As Integer
    Public atualizou As Boolean
    Public bindingSourceContribuite As BindingSource = New BindingSource
    Public bindingSourceMovimentacao As BindingSource = New BindingSource
    Public cod_mov_contribuicao As Long


    Public Property NivelAcesso() As Integer
        Get
            Return mNivelAcesso
        End Get
        Set(ByVal value As Integer)
            mNivelAcesso = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return mUsuario
        End Get
        Set(ByVal value As String)
            mUsuario = value
        End Set
    End Property

    Public Property CodUsuario() As Integer
        Get
            Return mCodUsuario
        End Get
        Set(ByVal value As Integer)
            mCodUsuario = value
        End Set
    End Property

#End Region



#Region "Metodos"



    Private Sub carregaGrid()

        Dim objContribuintes As CContribuinte = New CContribuinte()

        Dim bindingSourceApolice As BindingSource = New BindingSource

        Try



            lstContribuintes = New SortableBindingList(Of CContribuinte)(rnContribuinte.CarregarGridOi(objContribuintes, "Remessa"))

            If (lstContribuintes.Count > 0) Then

                bindingSourceApolice.DataSource = lstContribuintes

                grdFaturamento.DataSource = bindingSourceApolice.DataSource

            End If


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CriarColunasGrid()


        Try

            grdFaturamento.DataSource = Nothing
            grdFaturamento.Columns.Clear()

            grdFaturamento.AutoGenerateColumns = False

            grdFaturamento.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            Dim column As New DataGridViewTextBoxColumn


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_EAN_Cliente"
            column.HeaderText = "Cód. EAN Cliente"
            column.Width = 80
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Nome_contribuinte"
            column.HeaderText = "Nome contribuinte"
            column.Width = 300
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Data_mov"
            column.HeaderText = "Data Movimentação"
            column.Width = 80
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Tipo_mov"
            column.HeaderText = "Tipo Movimentação"
            column.Width = 80
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Tipo_cobranca"
            column.HeaderText = "Tipo Cobranca"
            column.Width = 80
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_Categoria"
            column.HeaderText = "Cód Categoria"
            column.Width = 80
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)


            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "valor"
            column.HeaderText = "Valor"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            grdFaturamento.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_recibo"
            column.HeaderText = "Cód. Recibo"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            grdFaturamento.Columns.Add(column)

            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cidade"
            column.HeaderText = "Cidade"
            column.Width = 80
            column.ReadOnly = True
            column.Visible = True
            grdFaturamento.Columns.Add(column)

            '4
            column = New DataGridViewTextBoxColumn
            column.HeaderText = "Modificado"
            column.Width = 0
            column.ReadOnly = True
            column.Visible = True
            grdFaturamento.Columns.Add(column)




        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CriarColunasGridMovimentacao()

        Try

            grdMovimentacao.DataSource = Nothing
            grdMovimentacao.Columns.Clear()

            grdMovimentacao.AutoGenerateColumns = False

            grdMovimentacao.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft


            Dim column As New DataGridViewTextBoxColumn


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "mov_contribuicao_id"
            column.HeaderText = "Cód. Contribuicao"
            column.Width = 80
            column.ReadOnly = True
            grdMovimentacao.Columns.Add(column)

            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "tipo_mov"
            column.HeaderText = "Tipo movimentação"
            column.Width = 80
            column.ReadOnly = True
            grdMovimentacao.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "tipo_cobranca"
            column.HeaderText = "Tipo cobranca"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            grdMovimentacao.Columns.Add(column)

            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "cobrador"
            column.HeaderText = "Cobrador"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            grdMovimentacao.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "data_mov"
            column.HeaderText = "Data movimentação"
            column.Width = 160
            column.ReadOnly = False
            column.Visible = True
            grdMovimentacao.Columns.Add(column)

            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "total_fichas"
            column.HeaderText = "Total fichas"
            column.Visible = True
            column.Width = 150
            column.ReadOnly = False
            grdMovimentacao.Columns.Add(column)

            '7
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "valor_fichas"
            column.HeaderText = "Valor fichas"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            grdMovimentacao.Columns.Add(column)




        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

#End Region

    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""
        Dim cidade As String = ""
        Dim dia As Integer = 0
        Dim rn As RNRequisicao = New RNRequisicao

        Try

            Me.Cursor = Cursors.WaitCursor

            carregaFichasFaturamento(cidade, dia, 0)

            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try

    End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Close()
    End Sub

    Private Sub FrmConsultaContribuinte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        CriarColunasGrid()
        CarregaComboOperador(0)
        CarregaComboCidades(0)
        CriarColunasGridMovimentacao()

        CarregaMovimentacoes()

        cboOperador.Enabled = True
        txtDataMov.Enabled = False
        btnPesquisar.Enabled = False
        txtConsultaBordero.Enabled = False
        btnConsultarBordero.Enabled = False
        btnSalvarBaixa.Enabled = False




    End Sub


    Private Sub FrmLiberaFaturamento_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""

        Try


        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub carregaFichasFaturamento(ByVal cidade As String, ByVal dia As Integer, ByVal operador_id As Integer)
        Dim objRetorno As CRetorno = New CRetorno()
        Dim dt As DataTable = New DataTable
        Dim ds As DataSet = New DataSet
        Dim dr As DataRow
        Dim valor As Single = 0
        Dim strValor As String = ""
        Dim totalValorFichas As Decimal = 0


        Try

            grdFaturamento.DataSource = Nothing

            ds = rnRequisicao.CarregaFichasFaturamentoBaixa(cidade, dia, 0)

            If Not ds Is Nothing Then
                If (ds.Tables(0).Rows.Count > 0) Then
                    If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                        dt = ds.Tables(0)
                    End If
                End If

                grdFaturamento.DataSource = dt


                If (grdFaturamento.Rows.Count = 0) Then

                    grdFaturamento.DataSource = Nothing
                    MsgBox("Não existe registros a serem processados!")

                Else

                    lblFichasCanceladas.Text = grdFaturamento.Rows.Count - 1

                    For Each col As DataGridViewRow In grdFaturamento.Rows

                        totalValorFichas = totalValorFichas + col.Cells(6).Value

                    Next

                    strValor = valor.ToString("C")
                    valor = totalValorFichas
                    lblValorFichasCanceladas.Text = valor.ToString("R$ #,###.00")

                End If
            Else
                grdFaturamento.DataSource = Nothing
                MsgBox("Não existe registros a serem processados!")
            End If

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CarregaComboOperador(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboOperador.DataSource = Nothing
            cboOperador.Items.Clear()
            cboOperador.DisplayMember = "Nome_Operador"
            cboOperador.ValueMember = "Cod_Operador"
            cboOperador.DataSource = rn_generico.BuscarOperador(0)

            If cboOperador.Items.Count > 0 And cod_operador = 0 Then
                cboOperador.SelectedIndex = 0
            ElseIf cboOperador.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperador.Items.Count - 1
                    For Each linha As CGenerico In cboOperador.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperador.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperador.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboCidades(ByVal cod_cidade As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCidade.DataSource = Nothing
            cboCidade.Items.Clear()
            cboCidade.DisplayMember = "Nome_cidade"
            cboCidade.ValueMember = "Cod_Cidade"
            cboCidade.DataSource = rn_generico.BuscarCidades(0)

            If cboCidade.Items.Count = 0 Then
                cboCidade.SelectedIndex = 0
            ElseIf cboCidade.Items.Count > 0 And cod_cidade <> 0 Then

                For i As Integer = 0 To cboCidade.Items.Count - 1
                    For Each linha As CGenerico In cboCidade.DataSource
                        If linha.Cod_Cidade = cod_cidade Then
                            cboCidade.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next

            Else

                cboCidade.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CarregaMovimentacoes()

        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim operador As String = ""
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable
        Dim valor As Single = 0
        Dim strValor As String = ""
        Dim totalValorFichas As Decimal = 0

        Try

            If cboOperador.SelectedValue > 0 Then
                operador = cboOperador.Text
            End If

            lstMovimentacao = New SortableBindingList(Of CRequisicao)(rnRequisicao.CarregaMovimentacoes(0, "ABERTO", operador, txtDataMov.Text))


            If (lstMovimentacao.Count > 0) Then

                bindingSourceMovimentacao.DataSource = lstMovimentacao

                grdMovimentacao.DataSource = bindingSourceMovimentacao.DataSource

            Else
                If lstMovimentacao.Count = 0 Then

                    ds = rnRequisicao.CarregaMovimentacoesDescrOpt(0, "ABERTO", operador, txtDataMov.Text)

                    If (ds.Tables(0).Rows.Count > 0) Then
                        If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                            dt = ds.Tables(0)
                        End If

                        limparCampos()

                        grdFaturamento.DataSource = dt


                        For Each col As DataGridViewRow In grdFaturamento.Rows

                            totalValorFichas = totalValorFichas + col.Cells(6).Value

                        Next

                        strValor = valor.ToString("C")
                        valor = totalValorFichas
                        lblValorFichasAbertas.Text = valor.ToString("R$ #,###.00")

                        txtLoteCodRecibo.Text = ""
                        txtLoteCodRecibo.Enabled = True
                        cboOperador.Enabled = True

                        lblFichaAberta.Text = grdFaturamento.RowCount - 1



                        txtBaixa.Text = dt.Rows(0).Item("cod_mov_contribuicao")

                    End If

                End If

                End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub



    Private Sub btnImprimirRecibo_Click(sender As System.Object, e As System.EventArgs)
        Dim strReportName As String
        Dim frm As FrmRelatorios = New FrmRelatorios
        Try


            ' carrega o relatorio desejado
            strReportName = "ReciboLista1"

            'define o caminho e nome do relatorio
            Dim strReportPath As String = "C:\Digital\RPT" & "\" & strReportName & ".rpt"
            '
            'verifiqa se o arquivo existe
            If Not IO.File.Exists(strReportPath) Then
                Throw (New Exception("Relatorio não localizado :" & vbCrLf & strReportPath))
            End If
            '
            'instancia o relaorio e carrega
            Dim CR As New ReportDocument
            CR.Load(strReportPath)
            '
            ' atribui os parametros declarados aos objetos relacionados
            Dim crParameterDiscreteValue As ParameterDiscreteValue
            Dim crParameterFieldDefinitions As ParameterFieldDefinitions
            Dim crParameterFieldLocation As ParameterFieldDefinition
            Dim crParameterValues As ParameterValues
            Dim crParameterDiscreteValue2 As ParameterDiscreteValue
            Dim crParameterFieldDefinitions2 As ParameterFieldDefinitions
            Dim crParameterFieldLocation2 As ParameterFieldDefinition
            Dim crParameterValues2 As ParameterValues
            '
            ' Pega a coleção de parametros do relatorio
            crParameterFieldDefinitions = CR.DataDefinition.ParameterFields
            crParameterFieldDefinitions2 = CR.DataDefinition.ParameterFields
            '
            ' define o primeiro parametro
            ' - pega o parametro e diz a ela para usar os valores atuais
            ' - define o valor do parametro
            ' - inclui e aplica o valor
            ' - repete para cada parametro se for o caso (não é o caso deste exemplo)

            '--------------------------- FILTRO 1
            ' Vamos usar o parametro 'situacao'
            crParameterFieldLocation = crParameterFieldDefinitions.Item("situacao")
            crParameterValues = crParameterFieldLocation.CurrentValues
            crParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue

            'obtem o valor da caixa de texto
            crParameterDiscreteValue.Value = 2
            crParameterValues.Add(crParameterDiscreteValue)
            crParameterFieldLocation.ApplyCurrentValues(crParameterValues)

            '--------------------------- FILTRO 2
            ' Vamos usar o parametro 'mesRef'
            crParameterFieldLocation2 = crParameterFieldDefinitions2.Item("cod_mov_contribuicao")
            crParameterValues2 = crParameterFieldLocation2.CurrentValues
            crParameterDiscreteValue2 = New CrystalDecisions.Shared.ParameterDiscreteValue

            'obtem o valor da caixa de texto
            crParameterDiscreteValue2.Value = txtBaixa.Text
            crParameterValues2.Add(crParameterDiscreteValue2)
            crParameterFieldLocation2.ApplyCurrentValues(crParameterValues2)


            '
            ' Define a fonte do controle Crystal Report Viewer como sendo o relatorio definido acima
            ' frm.CrystalReportViewer1.ReportSource = CR


        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnNovo_Click(sender As System.Object, e As System.EventArgs) Handles btnNovo.Click
        Dim rnRequisicao As RNRequisicao = New RNRequisicao

        Try

            cboOperador.Enabled = True
            txtDataMov.Enabled = True
            btnPesquisar.Enabled = True
            txtConsultaBordero.Enabled = True
            btnConsultarBordero.Enabled = True
            btnSalvarBaixa.Enabled = True

            txtBaixa.Text = rnRequisicao.RetornaUltimaMovimentacao()
            txtLoteCodRecibo.Enabled = True

            limparCampos()
            btnSalvarBaixa.Enabled = True
            cboOperador.Enabled = True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub txtLoteCodCliente_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtLoteCodRecibo.KeyPress
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim dt As DataTable = New DataTable
        Dim dt2 As DataTable = New DataTable
        Dim ds As DataSet = New DataSet
        Dim dr As DataRow
        Dim dr2 As DataRow
        Dim valor As Single = 0
        Dim strValor As String = ""
        Dim totalValorFichasAbertas As Decimal = 0
        Dim totalValorFichasBaixadas As Decimal = 0
        Dim totalValorFichasCanceladas As Decimal = 0
        Dim totalValorFichasProxDia As Decimal = 0
        Dim cod_recibo As String = ""
        Dim contador As Integer = 0


        Try

            If e.KeyChar = Chr(Keys.Enter) Then 'Detecta se tecla Enter foi pressionada

                Me.Cursor = Cursors.WaitCursor

                If cboAcao.SelectedIndex = -1 Then
                    MsgBox("Por favor selecione uma das opções para baixa!", MsgBoxStyle.Critical, "Atenção")
                    cboAcao.Focus()
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If

                cod_recibo = txtLoteCodRecibo.Text
                cod_recibo = "*" + cod_recibo + "*"

                ds = rnRequisicao.CarregaFichaPorCodRecibo(cod_recibo)

                If (ds.Tables(0).Rows.Count > 0) Then
                    If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                        dt = ds.Tables(0)
                    End If


                    dr = dt.Rows(0)

                    If rdIncluir.Checked = True Then

                        'For Each col As DataGridViewRow In grdFaturamento.Rows

                        '    If dr("cod_ean_cliente") = col.Cells(0).Value Then
                        '        MsgBox("Contibuinte já consta na lista para liberação")
                        '        Exit Sub
                        '    End If

                        'Next

                        If cboAcao.Text = "Baixar" Then

                            For Each linha As DataGridViewRow In grdFaturamento.Rows

                                If dr("cod_recibo") = linha.Cells(7).Value Then
                                    'coloco cor na linha
                                    linha.DefaultCellStyle.BackColor = Color.LightBlue
                                    linha.DefaultCellStyle.ForeColor = Color.White
                                    linha.Cells(3).Value = "BAIXADO"
                                    linha.Cells(9).Value = "1"
                                    Exit For

                                End If

                            Next

                        ElseIf cboAcao.Text = "Cancelar" Then

                            For Each linha As DataGridViewRow In grdFaturamento.Rows

                                If dr("cod_recibo") = linha.Cells(7).Value Then
                                    'coloco cor na linha
                                    linha.DefaultCellStyle.BackColor = Color.LightCoral
                                    linha.DefaultCellStyle.ForeColor = Color.White
                                    linha.Cells(3).Value = "CANCELADO"
                                End If

                            Next

                        ElseIf cboAcao.Text = "Próx. Dia" Then

                            For Each linha As DataGridViewRow In grdFaturamento.Rows

                                If dr("cod_recibo") = linha.Cells(7).Value Then
                                    'coloco cor na linha
                                    linha.DefaultCellStyle.BackColor = Color.Yellow
                                    linha.DefaultCellStyle.ForeColor = Color.Black
                                    linha.Cells(3).Value = "PROX.DIA"
                                End If

                            Next

                        End If

                        'calcula as fichas baixadas inicio
                        contador = 0
                        For Each col As DataGridViewRow In grdFaturamento.Rows
                            If col.Cells(3).Value = "BAIXADO" Then
                                contador = contador + 1
                                totalValorFichasBaixadas = totalValorFichasBaixadas + col.Cells(6).Value
                                lblFichasBaixadas.Text = contador

                            End If
                        Next
                        strValor = valor.ToString("C")
                        valor = totalValorFichasBaixadas
                        lblValorFichasBaixadas.Text = valor.ToString("R$ #,###.00")
                        'calcula as fichas baixadas fim


                        'calcula as fichas canceladas inicio
                        contador = 0
                        For Each col As DataGridViewRow In grdFaturamento.Rows
                            If col.Cells(3).Value = "CANCELADO" Then
                                contador = contador + 1
                                totalValorFichasCanceladas = totalValorFichasCanceladas + col.Cells(6).Value
                                lblFichasCanceladas.Text = contador
                            End If
                        Next
                        strValor = valor.ToString("C")
                        valor = totalValorFichasCanceladas
                        lblValorFichasCanceladas.Text = valor.ToString("R$ #,###.00")
                        'calcula as fichas canceladas fim


                        'calcula as fichas Prox.Dia inicio
                        contador = 0
                        For Each col As DataGridViewRow In grdFaturamento.Rows
                            If col.Cells(3).Value = "PROX.DIA" Then
                                contador = contador + 1
                                totalValorFichasProxDia = totalValorFichasProxDia + col.Cells(6).Value
                                lblFichasProxDia.Text = contador
                            End If
                        Next
                        strValor = valor.ToString("C")
                        valor = totalValorFichasProxDia
                        lblValorFichasProxDia.Text = valor.ToString("R$ #,###.00")
                        'calcula as fichas Prox.Dia fim

                        'calcula as fichas aberto inicio
                        contador = 0
                        For Each col As DataGridViewRow In grdFaturamento.Rows
                            If col.Cells(3).Value = "ABERTO" Then
                                contador = contador + 1
                                totalValorFichasAbertas = totalValorFichasAbertas + col.Cells(6).Value
                                lblFichaAberta.Text = contador
                            End If
                        Next
                        strValor = valor.ToString("C")
                        valor = totalValorFichasAbertas
                        lblValorFichasAbertas.Text = valor.ToString("R$ #,###.00")
                        'calcula as fichas aberto fim



                    End If

                    If rdExtornar.Checked Then

                        For Each linha As DataGridViewRow In grdFaturamento.Rows

                            If dr("cod_recibo") = linha.Cells(7).Value Then
                                'coloco cor na linha
                                linha.DefaultCellStyle.BackColor = Color.White
                                linha.DefaultCellStyle.ForeColor = Color.Black
                                linha.Cells(3).Value = "ABERTO"
                            End If

                        Next

                        'calcula as fichas baixadas inicio
                        contador = 0
                        For Each col As DataGridViewRow In grdFaturamento.Rows
                            If col.Cells(3).Value = "BAIXADO" Then
                                contador = contador + 1
                                totalValorFichasBaixadas = totalValorFichasBaixadas + col.Cells(6).Value
                                lblFichasBaixadas.Text = contador
                            End If
                        Next
                        strValor = valor.ToString("C")
                        valor = totalValorFichasBaixadas
                        lblValorFichasBaixadas.Text = valor.ToString("R$ #,###.00")
                        'calcula as fichas baixadas fim


                        'calcula as fichas canceladas inicio
                        contador = 0
                        For Each col As DataGridViewRow In grdFaturamento.Rows
                            If col.Cells(3).Value = "CANCELADO" Then
                                contador = contador + 1
                                totalValorFichasCanceladas = totalValorFichasCanceladas + col.Cells(6).Value
                                lblFichasCanceladas.Text = contador
                            End If
                        Next
                        strValor = valor.ToString("C")
                        valor = totalValorFichasCanceladas
                        lblValorFichasCanceladas.Text = valor.ToString("R$ #,###.00")
                        'calcula as fichas canceladas fim


                        'calcula as fichas Prox.Dia inicio
                        contador = 0
                        For Each col As DataGridViewRow In grdFaturamento.Rows
                            If col.Cells(3).Value = "PROX.DIA" Then
                                contador = contador + 1
                                totalValorFichasProxDia = totalValorFichasProxDia + col.Cells(6).Value
                                lblFichasProxDia.Text = contador
                            End If
                        Next
                        strValor = valor.ToString("C")
                        valor = totalValorFichasProxDia
                        lblValorFichasProxDia.Text = valor.ToString("R$ #,###.00")
                        'calcula as fichas Prox.Dia fim

                        'calcula as fichas aberto inicio
                        contador = 0
                        For Each col As DataGridViewRow In grdFaturamento.Rows
                            If col.Cells(3).Value = "ABERTO" Then
                                contador = contador + 1
                                totalValorFichasAbertas = totalValorFichasAbertas + col.Cells(6).Value
                                lblFichaAberta.Text = contador
                            End If
                        Next
                        strValor = valor.ToString("C")
                        valor = totalValorFichasAbertas
                        lblValorFichasAbertas.Text = valor.ToString("R$ #,###.00")
                        'calcula as fichas aberto fim

                    End If

                    txtLoteCodRecibo.Text = ""

                End If

            End If
            Me.Cursor = Cursors.Default



        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return
        End Try

    End Sub



    Private Sub btnSalvarBaixa_Click(sender As System.Object, e As System.EventArgs) Handles btnSalvarBaixa.Click
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim ultimo_recibo As String = ""
        Dim ReciboClicado As String = ""
        Dim cod_cobradorClicado As Long
        Dim cobradorClicado As String
        Dim cod_bordero As Long
        Dim totalFIchas As Long = 0
        Dim totalValorFichas As Decimal = 0
        Dim tipoMovClicado As String
        Dim cod_baixa As Long
        Dim ClienteClicado As String = ""

        Try

            Me.Cursor = Cursors.WaitCursor

            If cboOperador.Text = "" Or cboOperador.Text = "Selecione..." Then
                MsgBox("Por favor selecione um responsável")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If

            cod_bordero = txtBaixa.Text
            cod_mov_contribuicao = txtBaixa.Text 'verificar
            cod_baixa = rnRequisicao.RetornaIdBaixa()


            For Each linha As DataGridViewRow In grdFaturamento.Rows

                If (Convert.ToString(linha.Cells(9).Value)) = "1" Then

                    cod_cobradorClicado = cboOperador.SelectedValue
                    cobradorClicado = cboOperador.Text
                    ReciboClicado = linha.Cells(7).Value
                    tipoMovClicado = linha.Cells(3).Value
                    ClienteClicado = linha.Cells(0).Value

                    rnRequisicao.Atualiza_Movimentacao_baixa(cod_mov_contribuicao, ReciboClicado, cod_baixa, cod_cobradorClicado, cobradorClicado, tipoMovClicado)

                    rnRequisicao.GeraRegistroBaixa(cod_mov_contribuicao, ReciboClicado, ClienteClicado, cod_baixa, tipoMovClicado)

                End If

            Next

            'salva os valores totais
            totalFIchas = grdFaturamento.Rows.Count - 1

            For Each col As DataGridViewRow In grdFaturamento.Rows

                totalValorFichas = totalValorFichas + col.Cells(6).Value

            Next

            lblValorFichasCanceladas.Text = totalValorFichas

            MsgBox("Dados gravados com sucesso!", MsgBoxStyle.Information)



            cboOperador.Enabled = False
            txtDataMov.Enabled = False
            btnPesquisar.Enabled = False
            txtConsultaBordero.Enabled = False
            btnConsultarBordero.Enabled = False
            btnSalvarBaixa.Enabled = False

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return
        End Try

    End Sub

    Private Sub btnConsultarBordero_Click(sender As System.Object, e As System.EventArgs) Handles btnConsultarBordero.Click
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim cod_mov_contribuicao As Long
        Dim ds As DataSet
        Dim dt As DataTable = New DataTable
        Dim valor As Single = 0
        Dim strValor As String = ""
        Dim totalValorFichas As Decimal = 0

        Try
            Me.Cursor = Cursors.WaitCursor

            cod_mov_contribuicao = txtConsultaBordero.Text

            ds = rnRequisicao.CarregaMovimentacaoDescrFichasAbertas(cod_mov_contribuicao)


            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                    txtBaixa.Text = cod_mov_contribuicao
                End If


                grdFaturamento.DataSource = dt


                lblFichasCanceladas.Text = grdFaturamento.Rows.Count - 1

                For Each col As DataGridViewRow In grdFaturamento.Rows

                    totalValorFichas = totalValorFichas + col.Cells(6).Value

                Next

                strValor = valor.ToString("C")
                valor = totalValorFichas
                lblValorFichasCanceladas.Text = valor.ToString("R$ #,###.00")

                txtLoteCodRecibo.Text = ""
                txtLoteCodRecibo.Enabled = True
                btnSalvarBaixa.Enabled = True

            End If

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return
        End Try



    End Sub

    Private Sub grdMovimentacao_CellContentDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdMovimentacao.CellContentDoubleClick
        Dim movimentoClicado As Long = 0
        Try

            Me.Cursor = Cursors.WaitCursor

            movimentoClicado = CType(grdMovimentacao.Rows(e.RowIndex).Cells(ColunasGridMovimentacao.mov_contribuicao_id).Value, Long)

            CarregaMovimentacao(movimentoClicado)
          
            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return
        End Try
    End Sub

    Public Sub CarregaMovimentacao(ByVal movimentoClicado As Long)
        Dim rn As RNGenerico = New RNGenerico
        Dim ds As DataSet
        Dim dt As DataTable = New DataTable
        Dim valor As Single = 0
        Dim strValor As String = ""
        Dim totalValorFichas As Decimal = 0

        Try

            limparCampos()

            ds = rnRequisicao.CarregaMovimentacoesDescrBaixa(movimentoClicado)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If

            grdFaturamento.DataSource = dt


            lblFichaAberta.Text = grdFaturamento.Rows.Count - 1

            For Each col As DataGridViewRow In grdFaturamento.Rows

                totalValorFichas = totalValorFichas + col.Cells(6).Value

            Next

            strValor = valor.ToString("C")
            valor = totalValorFichas
            lblValorFichasAbertas.Text = valor.ToString("R$ #,###.00")

            txtBaixa.Text = movimentoClicado
            txtLoteCodRecibo.Text = ""
            txtLoteCodRecibo.Enabled = True
            btnSalvarBaixa.Enabled = True
            cboOperador.Enabled = True

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return
        End Try

    End Sub

    Private Sub btnPesquisar_Click_1(sender As System.Object, e As System.EventArgs) Handles btnPesquisar.Click
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim data_mov As String = ""
        Dim ds As DataSet
        Dim dt As DataTable = New DataTable
        Dim valor As Single = 0
        Dim strValor As String = ""
        Dim totalValorFichas As Decimal = 0
        Dim cidade As String = ""
        Dim cobrador As String = ""

        Try

            If Not IsDate(txtDataMov.Text) Then
                MsgBox("Por favor insira uma data valida", MsgBoxStyle.Critical, "Atenção!")
                txtDataMov.Focus()
                Me.Cursor = Cursors.Default
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor

            data_mov = txtDataMov.Text

            If cboCidade.SelectedValue > 0 Then
                cidade = cboCidade.Text
            End If

            If cboOperador.SelectedValue > 0 Then
                cobrador = cboOperador.Text
            End If
            '

            If cboOperador.SelectedValue >= 0 Then
                ds = rnRequisicao.CarregaMovimentacaoDescrFichasAbertasDataMovCobrador(data_mov, cobrador, cidade)
            Else
                ds = rnRequisicao.CarregaMovimentacaoDescrFichasAbertasDataMov(data_mov, cidade)

            End If

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If

                limparCampos()

                grdFaturamento.DataSource = dt

                For Each col As DataGridViewRow In grdFaturamento.Rows

                    totalValorFichas = totalValorFichas + col.Cells(6).Value

                Next

                strValor = valor.ToString("C")
                valor = totalValorFichas
                lblValorFichasAbertas.Text = valor.ToString("R$ #,###.00")

                txtLoteCodRecibo.Text = ""
                txtLoteCodRecibo.Enabled = True
                cboOperador.Enabled = True

                lblFichaAberta.Text = grdFaturamento.RowCount - 1



                txtBaixa.Text = dt.Rows(0).Item("cod_mov_contribuicao")

            Else
                MsgBox("Não foram encontrados registros para a data selecionada.", MsgBoxStyle.Exclamation, "Atenção!!")
                grdFaturamento.DataSource = Nothing
                lblFichaAberta.Text = "0"
                lblValorFichasAbertas.Text = "R$ 0,00"
                Me.Cursor = Cursors.Default
                Exit Sub
            End If

            Me.Cursor = Cursors.Default


        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return
        End Try
    End Sub


    Public Sub limparCampos()

        Try
            lblFichaAberta.Text = 0
            lblFichasBaixadas.Text = 0
            lblFichasCanceladas.Text = 0
            lblFichasProxDia.Text = 0

            lblValorFichasAbertas.Text = "R$ 0,00"
            lblValorFichasBaixadas.Text = "R$ 0,00"
            lblValorFichasCanceladas.Text = "R$ 0,00"
            lblValorFichasProxDia.Text = "R$ 0,00"
            txtBaixa.Text = ""

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

   
    Private Sub cboOperador_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboOperador.SelectedValueChanged

        Try

            CarregaMovimentacoes()

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub txtDataMov_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtDataMov.TextChanged
        Dim data As String = ""

        Try
            data = Replace(txtDataMov.Text, "_", "")

            If IsDate(data) Then
                If Trim(data.Length) = 10 Then
                    CarregaMovimentacoes()
                End If
            End If

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return
        End Try
    End Sub

   
    Private Sub txtDataMov_Leave(sender As System.Object, e As System.EventArgs) Handles txtDataMov.Leave
        Dim data As String = ""

        Try
            data = Replace(txtDataMov.Text, "_", "")

            If IsDate(data) Then
                If Trim(data.Length) = 10 Then
                    CarregaMovimentacoes()
                End If
            End If

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return
        End Try
    End Sub
End Class