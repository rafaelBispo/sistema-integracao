﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class FrmFiltroRelatorios


    Dim objContribuintes As CContribuinte = New CContribuinte()
    Dim bindingSourceApolice As BindingSource = New BindingSource
    Private lstContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstContribuinteSe As New List(Of CContribuinte)


#Region "variaveis"

    Public pCod_ean As String
    Public pNome As String
    Public pTelefone As String
    Public psituacao As String
    Public pMesAnoRef As String
    Public pValor As Decimal
    Public MesAtuacao As String
    Public NextMonth As Date
    Public Consulta As Boolean = False
    Public Relatorio As String = ""
    Public param1 As String = ""
    Public param2 As String = ""

#End Region

#Region "Propriedades"

    Public Property Cod_Ean_Cliente() As String
        Get
            Return pCod_ean
        End Get
        Set(ByVal value As String)
            pCod_ean = value
        End Set
    End Property

    Public Property Telefone() As String
        Get
            Return pTelefone
        End Get
        Set(ByVal value As String)
            pTelefone = value
        End Set
    End Property

    Public Property Nome() As String
        Get
            Return pNome
        End Get
        Set(ByVal value As String)
            pNome = value
        End Set
    End Property

    Public Property Valor() As String
        Get
            Return pValor
        End Get
        Set(ByVal value As String)
            pValor = value
        End Set
    End Property


#End Region


    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FrmConsultaUltimaMovimentacao.Enabled = True
        Close()
    End Sub





    Private Sub FrmRelatorios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            GeraRelatorio()

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub


    Private Sub btnFiltrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFiltrar.Click

        GeraRelatorio()

    End Sub

    Private Sub GeraRelatorio()
        Dim strReportName As String
        Dim frm As FrmImprimiRelatorios = New FrmImprimiRelatorios

        Dim crtableLogoninfos As New TableLogOnInfos()
        Dim crtableLogoninfo As New TableLogOnInfo()
        Dim crConnectionInfo As New ConnectionInfo()
        Dim ds As DataSet
        Dim dao As RequisicaoDAO = New RequisicaoDAO


        Try

            ' carrega o relatorio desejado
            strReportName = Relatorio

            'define o caminho e nome do relatorio

            Dim strReportPath As String = "C:\Digital\SISINTEGRACAO\RPT" & "\" & strReportName & ".rpt"
            'Dim strReportPath As String = "C:\GIT\Sistema de Integraçao\SISINT001\RPT" & "\" & strReportName & ".rpt"
            '
            'verifiqa se o arquivo existe
            If Not IO.File.Exists(strReportPath) Then
                MsgBox("Relatorio não localizado :" & vbCrLf & strReportPath, MsgBoxStyle.Critical)
            End If

            If Relatorio = "HistOperadoraSint" Then
                ds = dao.GeraRelatorioHistOperadorSintetico()
            ElseIf Relatorio = "SitRecOpera" Then
                ds = dao.GeraRelatorioSitRecOpera()
            ElseIf Relatorio = "HistOperadoraAnalitico" Then
                ds = dao.GeraRelatorioHistOperadoraAnalitico()
            End If

            Dim dtCliente As DataTable = ds.Tables(0)
            Dim rptCliente As New ReportDocument

            rptCliente.Load(strReportPath)
            'Dim rptCliente As New ReciboLista1 ''nome do relatorio

            'rptCliente.SetDatabaseLogon("sa", "Xucrutis2017", "NOVAJFDSK030\SISINTEGRACAO", "controle_db")
            rptCliente.SetDatabaseLogon("sa", "xucrutis", "DESKTOP-36CEDMG\SISINTEGRACAO", "controle_db")

            rptCliente.SetDataSource(dtCliente)

            ' frm.CrystalReportViewer.ReportSource = rptCliente
            'frm.CrystalReportViewer.RefreshReport()
            frm.Show()
            Me.Close()

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub
End Class