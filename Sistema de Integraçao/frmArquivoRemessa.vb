﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text
Imports Microsoft.Office.Interop

Public Class frmArquivoRemessa

    Public jaGravou As Boolean = False
    Private lstHistoricoOi As New SortableBindingList(Of CRetorno)
    Public bindingSourceContribuite As BindingSource = New BindingSource


    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        Dim rnRetorno As RNRetorno = New RNRetorno
        Dim nome_arquivo As String
        Dim data_servico As String
        Dim UF As String

        Try
            btnImportar.Enabled = True

            If OpenFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                Dim sr As New System.IO.StreamReader(OpenFileDialog1.FileName)
                txtCaminho.Text = OpenFileDialog1.FileName.ToString()

                nome_arquivo = OpenFileDialog1.SafeFileName.ToString()

                data_servico = nome_arquivo.Substring(26, 8)
                UF = nome_arquivo.Substring(14, 2)

                If rnRetorno.IsArquivoRemessaProcessado(nome_arquivo) = True Then
                    lblStatusArquivo.Text = "Arquivo processado"
                    lblStatusArquivo.ForeColor = Color.Red

                    jaGravou = True

                    Dim result = MessageBox.Show("Arquivo já processado, Deseja reprocessar o arquivo?", "Atenção", _
                            MessageBoxButtons.YesNo, _
                            MessageBoxIcon.Question)


                    If (result = DialogResult.Yes) Then
                        btnImportar.Enabled = True

                        rnRetorno.ApagaArquivoRemessaProcessado(nome_arquivo)
                        jaGravou = False


                    Else
                        lblStatusArquivo.Text = "Arquivo processado"
                        lblStatusArquivo.ForeColor = Color.Red

                        sr.Close()
                        sr.Close()

                    End If
                    lblStatusArquivo.Text = "Arquivo processado"
                    lblStatusArquivo.ForeColor = Color.Blue

                End If
            End If


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub btnImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportar.Click
        'Declara uma variável do tipo StreamWriter
        Dim Leitura As StreamReader
        Dim rnRetorno As RNRetorno = New RNRetorno
        Dim retornoList As List(Of CRetorno) = New List(Of CRetorno)
        Dim HeaderList As List(Of CRetorno) = New List(Of CRetorno)
        Dim TraillerList As List(Of CRetorno) = New List(Of CRetorno)
        Dim GravarList As List(Of CRetorno) = New List(Of CRetorno)
        Dim usuario As String = ""
        Dim id_arq As Integer
        Dim nome_arq As String = ""
        Dim UF As String


        Try

            'Verifica se já existe um arquivo com este nome
            If File.Exists(txtCaminho.Text) = False Then
                MsgBox("Este arquivo não existe")
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor

            'Cria um novo arquivo de acordo com o nome digitado e passa para o objeto StremWriter
            Leitura = File.OpenText(txtCaminho.Text)

            nome_arq = System.IO.Path.GetFileName(OpenFileDialog1.FileName)
            UF = nome_arq.Substring(14, 2)


            'Declara as variáveis dos campos
            Dim strLinha As String

            ''retorna id do arquivo
            'id_arq = rnRetorno.RetornaIdArquivo()

            'Faz um loop dentro das linhas do arquivo
            While (Leitura.Peek() > -1)

                'Parra toda a linha atual para uma variável 
                strLinha = Leitura.ReadLine

                If (strLinha.Substring(0, 1) = "1") Then
                    HeaderList.Add(rnRetorno.LeituraArquivoRemessa(strLinha))
                ElseIf (strLinha.Substring(0, 1) = "3") Then
                    TraillerList.Add(rnRetorno.LeituraArquivoRemessa(strLinha))
                Else
                    retornoList.Add(rnRetorno.LeituraArquivoRemessa(strLinha))
                End If

                'If jaGravou = False Then
                '    'preenche a lista com todas as informações do arquivo
                '    GravarList.Add(rnRetorno.LeituraArquivo(strLinha))
                'End If

            End While

            Leitura.Dispose()
            Leitura.Close()

            For Each reg As CRetorno In HeaderList
                rnRetorno.GravaDadosArquivoRemessa(reg, nome_arq, id_arq, UF)
            Next

            id_arq = rnRetorno.RetornaIdArquivoRemessa(nome_arq)

            For Each reg As CRetorno In retornoList
                rnRetorno.GravaDadosArquivoRemessa(reg, nome_arq, id_arq, UF)
            Next

            For Each reg As CRetorno In TraillerList
                rnRetorno.GravaDadosArquivoRemessa(reg, nome_arq, id_arq, UF)
            Next


            MsgBox("Base atualizada com sucesso")
            lblStatusArquivo.Text = "Arquivo Processado"

            Me.Cursor = Cursors.Default


            Dim arquivoADeletar As String = ""
            arquivoADeletar = txtCaminho.Text
            Leitura.Dispose()
            Leitura.Close()
            FileClose(1)
            Leitura.Dispose()
            Leitura.Close()
            FileClose(1)


        Catch ex As Exception
            ' Show the exception's message.
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub frmArquivoRemessa_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        dtRemessa.Text = Date.Now

    End Sub

    Private Sub btnConsultar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsultar.Click
        Dim rnRetorno As RNRetorno = New RNRetorno
        Dim rn As RNGenerico = New RNGenerico
        Dim dataRemessa As String = ""
        Dim isEnviado As Boolean = False
        Dim ds As DataSet = New DataSet
        Dim dt As System.Data.DataTable = New System.Data.DataTable
        Dim UltimaSituacao As String = ""
        Try

            lblStsTelefone.Text = ""
            lblUltimaSituacao.Text = ""

            Me.Cursor = Cursors.WaitCursor

            dataRemessa = dtRemessa.Text
            dataRemessa = Replace(dataRemessa, "/", "")
            dataRemessa = dataRemessa.Substring(2, 4) + dataRemessa.Substring(0, 2)


            isEnviado = rnRetorno.ConsultaTelefoneEnviadoRemessa(txtDDD.Text, txtTelefone.Text, dataRemessa)



            If isEnviado = True Then

                lblStsTelefone.Text = "Telefone enviado na remessa de " & dtRemessa.Text & "!"
                lblStsTelefone.ForeColor = Color.Red

            Else
                lblStsTelefone.Text = "Telefone não enviado na remessa"
                lblStsTelefone.ForeColor = Color.Green
            End If


            'CARREGA HISTORICO OI

            ds = rn.RetornaHistoricoOi(txtTelefone.Text, txtDDD.Text)
            If Not ds Is Nothing Then

                If (ds.Tables(0).Rows.Count > 0) Then
                    dt = ds.Tables(0)
                    gridResultado.DataSource = dt
                Else
                    gridResultado.DataSource = Nothing
                End If
            End If
            'CARREGA ULTIMA SITUACAO OI

            ds = Nothing

            ds = rnRetorno.RetornaUltimaSituacaoOi(txtTelefone.Text, txtDDD.Text)

            If ds Is Nothing Then

                lblUltimaSituacao.Text = "Sem faturamento em aberto"

            Else
                If (ds.Tables(0).Rows.Count > 0) Then
                    dt = ds.Tables(0)

                    UltimaSituacao = dt.Rows(0).Item(0).ToString()

                    If UltimaSituacao = "afaturar" Then
                        UltimaSituacao = "A faturar"
                    ElseIf UltimaSituacao = "faturado" Then
                        UltimaSituacao = "Faturado"
                    End If

                    lblUltimaSituacao.Text = UltimaSituacao

                Else
                    lblUltimaSituacao.Text = "Sem faturamento em aberto"
                End If

            End If

            MsgBox(lblStsTelefone.Text, MsgBoxStyle.Information, "Atenção")

            Me.Cursor = Cursors.Default


        Catch ex As Exception
            ' Show the exception's message.
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Me.Close()
    End Sub
End Class