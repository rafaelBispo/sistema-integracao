﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text
Imports System.Net
'Imports FTP_Upload_Download
Imports Microsoft.Office.Interop
Imports Microsoft.Office.Interop.Excel
Imports System.Net.Mail
Imports System.Windows.Forms.Screen
Imports System.Drawing.Graphics

Public Class FrmGerarExcel

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Me.Close()
    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click
        If FolderBrowserDialog1.ShowDialog() = DialogResult.OK Then
            txtCaminho.Text = FolderBrowserDialog1.SelectedPath.ToString()
        End If
    End Sub

    Private Sub rbOi_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbOi.CheckedChanged
        If rbOi.Checked Then
            rbManual.Checked = False
        End If
    End Sub

    Private Sub rbManual_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbManual.CheckedChanged
        If rbManual.Checked Then
            rbOi.Checked = False
        End If
    End Sub

    Private Sub btnExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcel.Click
        Dim rnGenerico As New RNGenerico
        Dim ds As System.Data.DataSet = New System.Data.DataSet
        Dim NomeArquivo As String = ""
        Dim sistema As String
        Dim Simplificado As Boolean = True

        Try

            Me.Cursor = Cursors.WaitCursor


            If txtCaminho.Text = "" Then
                MessageBox.Show("Por favor selecione onde o arquivo deve ser criado", "Atenção", _
                         MessageBoxButtons.OK, _
                         MessageBoxIcon.Error)
                txtCaminho.Focus()
                Me.Cursor = Cursors.Default
                Exit Sub
            End If



            If cboCategoria.SelectedValue < 0 Then
                NomeArquivo = "Planilha Requisição " + cboOperador.Text + Date.Now.Second.ToString() + ".xlsx"
            Else
                NomeArquivo = "Planilha Requisição " + cboOperador.Text + " Categoria " + cboCategoria.Text + Date.Now.Second.ToString() + ".xlsx"
            End If

            NomeArquivo = txtCaminho.Text + "\" + NomeArquivo

            If rbManual.Checked Then
                sistema = "manual"
            Else
                sistema = "oi"
            End If

            If rbRapido.Checked Then
                Simplificado = True
            Else
                Simplificado = False
            End If

            ds = rnGenerico.RetornaPlanilhaImprimir(cboOperador.Text, cboCategoria.SelectedValue, sistema, Simplificado)

            grdRemessa.DataSource = ds.Tables(0)

            ExportarParaExcel(grdRemessa, NomeArquivo, Simplificado)

            If txtEmail.Text <> "" Then
                Dim result = MessageBox.Show("Deseja enviar um e-mail com a planilha gerada?", "Atenção", _
                     MessageBoxButtons.YesNo, _
                     MessageBoxIcon.Question)

                If (result = DialogResult.Yes) Then
                    If txtEmail.Text <> "" Then
                        txtCaminho.Text = NomeArquivo
                        EnviaEmail()
                    End If
                End If
            End If

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            MessageBox.Show("Erro : " + ex.Message)
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Public Sub ExportarParaExcel(ByVal dgvName As DataGridView, ByVal fileName As String, ByVal Simplicado As Boolean)
        ' Criar uma nova instância do Excel 
        Dim objExcelApp As New Excel.Application()
        Dim objExcelBook As Excel.Workbook
        Dim objExcelSheet As Excel.Worksheet
        Dim pintarLinha As Boolean = False
        Try

            ' Altera o tipo/localização para Inglês. Existe incompatibilidade  
            ' entre algumas versões de Excel vs Sistema Operativo 
            Dim oldCI As CultureInfo = CurrentThread.CurrentCulture
            CurrentThread.CurrentCulture = New CultureInfo("en-US")
            ' Adiciona um workbook e activa a worksheet actual 
            objExcelBook = objExcelApp.Workbooks.Add
            objExcelSheet = CType(objExcelBook.Worksheets(1), Excel.Worksheet)

            ' Ciclo nos cabeçalhos para escrever os títulos a bold/negrito 
            objExcelSheet.Cells(1, 1) = "Cód. EAN Cliente"
            objExcelSheet.Cells(1, 2) = "DDD"
            objExcelSheet.Cells(1, 3) = "Telefone "
            objExcelSheet.Cells(1, 4) = "Nome Cliente "
            objExcelSheet.Cells(1, 5) = "Titular"
            objExcelSheet.Cells(1, 6) = "Valor"
            objExcelSheet.Cells(1, 7) = "Aceite"
            objExcelSheet.Cells(1, 8) = "Valor Total"
            If cboCategoria.Text = "Mensal" Or cboCategoria.Text = "Especial" Then
                objExcelSheet.Cells(1, 9) = "Ultimo Aceite"
            End If

            ' Aqui estou formatando o cabeçalho
            objExcelSheet.Range("A1", "O1").Font.Name = "Verdana"
            objExcelSheet.Range("A1", "O1").Font.Bold = True
            objExcelSheet.Range("A1", "O1").Font.Size = 10
            objExcelSheet.Range("A1", "O1").Font.ColorIndex = 2
            objExcelSheet.Range("A1", "O1").Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
            objExcelSheet.Range("A1", "O1").Interior.ColorIndex = 25
            objExcelSheet.Range("A1", "O1").Borders.ColorIndex = 1

            ' Ciclo nas linhas/células 
            Dim dgvRowIndex As Integer = 2

            If Simplicado = True Then 'GERA A PLANILHA COMUM SEM COLORIR AS LINHAS - MAIS RAPIDO

                    For Each row As DataGridViewRow In dgvName.Rows
                        Dim dgvCellIndex As Integer = 1

                        For Each cell As DataGridViewCell In row.Cells
                            If IsDBNull(cell.Value) Then
                                objExcelSheet.Cells(dgvRowIndex, (dgvCellIndex - 1)) = ""
                                objExcelSheet.Cells(dgvRowIndex, (dgvCellIndex - 1)).Borders.ColorIndex = 1
                                dgvCellIndex += 1
                            Else
                                objExcelSheet.Cells(dgvRowIndex, dgvCellIndex) = cell.Value
                                objExcelSheet.Cells(dgvRowIndex, dgvCellIndex).Borders.ColorIndex = 1
                                'APAGA A COLUNA COD_EMPRESA
                            'If dgvCellIndex = 1 Then
                            '    objExcelSheet.Cells(dgvRowIndex, dgvCellIndex).Delete()
                            'End If
                                dgvCellIndex += 1
                            End If
                        Next
                        dgvRowIndex += 1
                    Next

            Else 'GERA A PLANILHA COLORINDO AS LINHAS - MAIS LENTO

                For Each row As DataGridViewRow In dgvName.Rows
                    Dim dgvCellIndex As Integer = 1
                    pintarLinha = False
                    For Each cell As DataGridViewCell In row.Cells

                        If dgvCellIndex > 1 Then

                            If IsDBNull(cell.Value) Then
                                objExcelSheet.Cells(dgvRowIndex, (dgvCellIndex - 1)) = ""
                                objExcelSheet.Cells(dgvRowIndex, (dgvCellIndex - 1)).Borders.ColorIndex = 1
                                dgvCellIndex += 1
                            ElseIf dgvCellIndex = 1 And CStr(cell.Value) = "2" Then
                                pintarLinha = True
                                objExcelSheet.Cells(dgvRowIndex, (dgvCellIndex - 1)) = CStr(cell.Value)
                                objExcelSheet.Cells(dgvRowIndex, (dgvCellIndex - 1)).Borders.ColorIndex = 1
                                'APAGA A COLUNA COD_EMPRESA
                                If dgvCellIndex = 1 Then
                                    objExcelSheet.Cells(dgvRowIndex, dgvCellIndex).Delete()
                                End If
                                dgvCellIndex += 1
                            Else
                                objExcelSheet.Cells(dgvRowIndex, (dgvCellIndex - 1)) = CStr(cell.Value)
                                objExcelSheet.Cells(dgvRowIndex, (dgvCellIndex - 1)).Borders.ColorIndex = 1
                                'APAGA A COLUNA COD_EMPRESA
                                If dgvCellIndex = 1 Then
                                    objExcelSheet.Cells(dgvRowIndex, dgvCellIndex).Delete()
                                End If
                                dgvCellIndex += 1
                            End If

                            If pintarLinha = True Then
                                objExcelSheet.Cells(dgvRowIndex, dgvCellIndex - 1).Interior.ColorIndex = 15
                            End If

                        Else 'If dgvCellIndex = 1 Then
                            If IsDBNull(cell.Value) Then
                                objExcelSheet.Cells(dgvRowIndex, dgvCellIndex) = ""
                                objExcelSheet.Cells(dgvRowIndex, dgvCellIndex).Borders.ColorIndex = 1
                                dgvCellIndex += 1
                            ElseIf dgvCellIndex = 1 And CStr(cell.Value) = "2" Then
                                pintarLinha = True
                                objExcelSheet.Cells(dgvRowIndex, dgvCellIndex) = CStr(cell.Value)
                                objExcelSheet.Cells(dgvRowIndex, dgvCellIndex).Borders.ColorIndex = 1
                                'APAGA A COLUNA COD_EMPRESA
                                If dgvCellIndex = 1 Then
                                    objExcelSheet.Cells(dgvRowIndex, dgvCellIndex).Delete()
                                End If
                                dgvCellIndex += 1
                            Else
                                objExcelSheet.Cells(dgvRowIndex, dgvCellIndex) = CStr(cell.Value)
                                objExcelSheet.Cells(dgvRowIndex, dgvCellIndex).Borders.ColorIndex = 1
                                'APAGA A COLUNA COD_EMPRESA
                                If dgvCellIndex = 1 Then
                                    objExcelSheet.Cells(dgvRowIndex, dgvCellIndex).Delete()
                                End If
                                dgvCellIndex += 1
                            End If

                            If pintarLinha = True Then
                                objExcelSheet.Cells(dgvRowIndex, dgvCellIndex - 1).Interior.ColorIndex = 15
                            End If

                        End If

                    Next
                    dgvRowIndex += 1
                Next

            End If
                        ' Ajusta o largura das colunas automaticamente 
                        objExcelSheet.Columns.AutoFit()


                        ' Caso a opção seja gravar (xlsSaveAs) grava o ficheiro e fecha 
                        ' o Workbook/Excel. Caso contrário (xlsOpen) abre o Excel 

                        objExcelBook.SaveAs(fileName)
                        objExcelBook.Close()
                        objExcelApp.Quit()
                        MessageBox.Show("Arquivo gerado com sucesso para: " & fileName)


                        ' Altera a tipo/localização para actual 
                        CurrentThread.CurrentCulture = oldCI
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        Finally
            objExcelSheet = Nothing
            objExcelBook = Nothing
            objExcelApp = Nothing
            ' O GC(garbage collector) recolhe a memória não usada pelo sistema.  
            ' O método Collect() força a recolha e a opção WaitForPendingFinalizers  
            ' espera até estar completo. Desta forma o EXCEL.EXE não fica no  
            ' Task Manager(gestor tarefas) ocupando memória desnecessariamente 
            ' (devem ser chamados duas vezes para maior garantia) 
            GC.Collect()
            GC.WaitForPendingFinalizers()
            GC.Collect()
            GC.WaitForPendingFinalizers()
        End Try


    End Sub

    Private Sub CarregaComboCategorias(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCategoria.DataSource = Nothing
            cboCategoria.Items.Clear()
            cboCategoria.DisplayMember = "Nome_Categoria"
            cboCategoria.ValueMember = "Cod_Categoria"
            cboCategoria.DataSource = rn_generico.BuscarCategoria(0)

            If cboCategoria.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoria.SelectedIndex = 0
            ElseIf cboCategoria.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoria.Items.Count - 1
                    For Each linha As CGenerico In cboCategoria.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoria.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoria.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboOperador(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboOperador.DataSource = Nothing
            cboOperador.Items.Clear()
            cboOperador.DisplayMember = "Nome_Operador"
            cboOperador.ValueMember = "Cod_Operador"
            cboOperador.DataSource = rn_generico.BuscarOperador(0)

            If cboOperador.Items.Count > 0 And cod_operador = 0 Then
                cboOperador.SelectedIndex = 0
            ElseIf cboOperador.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperador.Items.Count - 1
                    For Each linha As CGenerico In cboOperador.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperador.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperador.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub FrmGerarExcel_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        CarregaComboCategorias(0)
        CarregaComboOperador(0)
    End Sub

    Private Sub EnviaEmail()
        Try

            'create the mail message
            Dim SmtpServer As New SmtpClient()
            Dim mail As New MailMessage()
            SmtpServer.Credentials = New Net.NetworkCredential("aaci.jf.mg@gmail.com", "kwcmuyymqekemvtz")
            SmtpServer.Host = "smtp.gmail.com"
            SmtpServer.Port = 587 'server para gmail
            SmtpServer.EnableSsl = True
            mail.From = New MailAddress("aaci.jf.mg@gmail.com")
            mail.To.Add(txtEmail.Text) 'Email que recebera a Mensagem
            mail.Subject = "Planilha Auditoria AACI"
            mail.Body = ""

            'Este codigo PlainView e caso o email não suporte Html, nisto ele irá informar este erro

            Dim plainView As AlternateView = AlternateView.CreateAlternateViewFromString("Segue em anexo a planilha.", Nothing, "text/plain")

            'crie um corpo de texto caso deseje assim você poderá colocar um texto no meio de duas imagens
            Dim linha1 As String = ""
            If Date.Now.Hour < 12 Then
                linha1 = "Bom dia,"
            Else
                linha1 = "Boa tarde,"
            End If

            Dim linha2 As String = "como solicitado, segue a planilha com as ligações efetuadas no periodo solicitado."
            Dim linha3 As String = "Atenciosamente."

            Dim htmlView As AlternateView = AlternateView.CreateAlternateViewFromString("<p> " + linha1 + "</p> <p>&nbsp;</p> <p>" + linha2 + "</p> <p>&nbsp;</p> <img src=cid:companylogo> <p>&nbsp;</p> <p>" + linha3 + "</p> <p><br /> <br /> --</p> <p>&nbsp;</p> <p>att..</p> <p>&nbsp;</p> <p><strong>AACI - ASSOCIA&Ccedil;&Atilde;O DE APOIO AS CRIAN&Ccedil;AS E IDOSOS</strong></p>   ", Nothing, "text/html")


            mail.Attachments.Add(New Attachment(txtCaminho.Text)) 'Adiciona o anexo a enviar

            With mail
                .AlternateViews.Add(plainView)
                .AlternateViews.Add(htmlView)

            End With

            mail.IsBodyHtml = True

            SmtpServer.Send(mail)
            MsgBox("E-mail enviado com sucesso", vbInformation, "Ok")





        Catch ex As Exception  'caso haja algum erro crie uma msg de aviso.
            MsgBox(ex)
        Finally
            '  File.Delete("")
        End Try
    End Sub
End Class