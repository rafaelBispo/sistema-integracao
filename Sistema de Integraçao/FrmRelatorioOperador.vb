﻿Imports SISINT001

Public Class FrmRelatorioOperador

    Private Enum ColunasGridRequisicao

        Operador = 0
        
    End Enum


    Private Sub btnPesquisar_Click(sender As System.Object, e As System.EventArgs) Handles btnPesquisar.Click
        Dim dataInicio As Date
        Dim dataFim As Date
        Dim ds As DataSet = New DataSet
        Dim dtResumoMensal As DataTable = New DataTable
        Dim dtResumoMensalComDesconto As DataTable = New DataTable
        Dim dtResumoPeriodo As DataTable = New DataTable
        Dim dt As DataTable = New DataTable
        Dim rn As RNGenerico = New RNGenerico
        Dim filtraData As String

        Try

            dataInicio = CDate(dtInicio.Value)
            dataFim = CDate(dtFinal.Value)
            If chkFiltrarData.Checked = True Then
                filtraData = "S"
            Else
                filtraData = "N"
            End If

            gridResultadoOperadorMensal.DataSource = Nothing
            gridResultadoOperadorOutros.DataSource = Nothing

            ds = rn.RetornaDadosRelatorio(dataInicio, dataFim, filtraData)

            'Resumo mensal sem descontos
            If (ds.Tables(0).Rows.Count > 0) Then
                dtResumoMensal = ds.Tables(0)
                lblValorReq.Text = dtResumoMensal.Rows(0).Item(0)
            End If

            'Resumo mensal com descontos
            If (ds.Tables(2).Rows.Count > 0) Then
                dtResumoMensalComDesconto = ds.Tables(2)
                lblValorPrevisto.Text = dtResumoMensalComDesconto.Rows(0).Item(0)
            End If

            'Resumo Periodo
            If (ds.Tables(1).Rows.Count > 0) Then
                dtResumoPeriodo = ds.Tables(1)
                gridRequisicao.DataSource = dtResumoPeriodo
            End If

            'Resumo por semana 1
            If (ds.Tables(3).Rows.Count > 0) Then
                dt = ds.Tables(3)
                lblPeriodo1.Text = IIf(IsDBNull(dt.Rows(0).Item(2)), "", CDate(dt.Rows(0).Item(0).ToString()) + " à " + CDate(dt.Rows(0).Item(1).ToString()))
                lblValor1.Text = IIf(IsDBNull(dt.Rows(0).Item(2)), "", dt.Rows(0).Item(2))
                dt.Clear()
                If lblValor1.Text <> "" Then
                    Panel2.Visible = True
                    lblPeriodo1.Visible = True
                Else
                    Panel2.Visible = False
                    lblPeriodo1.Visible = False
                End If
            Else
                dt.Clear()
                Panel2.Visible = False
                lblPeriodo1.Visible = False
            End If

            'Resumo por semana 2
            If (ds.Tables(4).Rows.Count > 0) Then
                dt = ds.Tables(4)
                lblPeriodo2.Text = IIf(IsDBNull(dt.Rows(0).Item(2)), "", CDate(dt.Rows(0).Item(0).ToString()) + " à " + CDate(dt.Rows(0).Item(1).ToString()))
                lblValor2.Text = IIf(IsDBNull(dt.Rows(0).Item(2)), "", dt.Rows(0).Item(2))
                dt.Clear()
                If lblValor2.Text <> "" Then
                    Panel3.Visible = True
                    lblPeriodo2.Visible = True
                Else
                    Panel3.Visible = False
                    lblPeriodo2.Visible = False
                End If
            Else
                dt.Clear()
                Panel3.Visible = False
                lblPeriodo2.Visible = False
            End If

                'Resumo por semana 3
            If (ds.Tables(5).Rows.Count > 0) Then
                dt = ds.Tables(5)
                lblPeriodo3.Text = IIf(IsDBNull(dt.Rows(0).Item(2)), "", CDate(dt.Rows(0).Item(0).ToString()) + " à " + CDate(dt.Rows(0).Item(1).ToString()))
                lblValor3.Text = IIf(IsDBNull(dt.Rows(0).Item(2)), "", dt.Rows(0).Item(2))
                dt.Clear()
                If lblValor3.Text <> "" Then
                    Panel4.Visible = True
                    lblPeriodo3.Visible = True
                Else
                    Panel4.Visible = False
                    lblPeriodo3.Visible = False
                End If
            Else
                dt.Clear()
                Panel4.Visible = False
                lblPeriodo3.Visible = False
            End If

                'Resumo por semana 4
            If (ds.Tables(6).Rows.Count > 0) Then
                dt = ds.Tables(6)
                lblPeriodo4.Text = IIf(IsDBNull(dt.Rows(0).Item(2)), "", CDate(dt.Rows(0).Item(0).ToString()) + " à " + CDate(dt.Rows(0).Item(1).ToString()))
                lblValor4.Text = IIf(IsDBNull(dt.Rows(0).Item(2)), "", dt.Rows(0).Item(2))
                dt.Clear()
                If lblValor4.Text <> "" Then
                    Panel5.Visible = True
                    lblPeriodo4.Visible = True
                Else
                    Panel5.Visible = False
                    lblPeriodo4.Visible = False
                End If
            Else
                dt.Clear()
                Panel5.Visible = False
                lblPeriodo4.Visible = False
            End If

            '    'Resumo por semana 5
            'If (ds.Tables(6).Rows.Count > 0) Then
            '    dt = ds.Tables(6)
            '    lblPeriodo5.Text = IIf(IsDBNull(dt.Rows(0).Item(2)), "", dt.Rows(0).Item(0).ToString() + " à " + dt.Rows(0).Item(1).ToString())
            '    lblValor5.Text = IIf(IsDBNull(dt.Rows(0).Item(2)), "", dt.Rows(0).Item(2))
            '    dt.Clear()
            '    If lblValor5.Text <> "" Then
            '        Panel6.Visible = True
            '        lblPeriodo5.Visible = True
            '    Else
            '        Panel6.Visible = False
            '        lblPeriodo5.Visible = False
            '    End If
            'Else
            '    dt.Clear()
            Panel6.Visible = False
            lblPeriodo5.Visible = False
            'End If

        Catch ex As Exception
            'MsgBox(ex)
        End Try
    End Sub

    Private Sub gridRequisicao_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridRequisicao.CellContentClick
        Dim dataInicio As Date
        Dim dataFim As Date
        Dim OperadorClicado As String
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable
        Dim rn As RNGenerico = New RNGenerico
        Dim filtraData As String
        Dim valorTotalMensal As Decimal = 0
        Dim valorTotalRequisicao As Decimal = 0
        Dim valorTotalOperador As Decimal = 0
        Dim valorTotalOutros As Decimal = 0
        Dim valorTotalArrecadadoOpera As Decimal = 0

        Try

            If chkFiltrarData.Checked = True Then
                filtraData = "S"
            Else
                filtraData = "N"
            End If


            OperadorClicado = CType(gridRequisicao.Rows(e.RowIndex).Cells(ColunasGridRequisicao.Operador).Value, String)

            dataInicio = CDate(dtInicio.Value)
            dataFim = CDate(dtFinal.Value)

            gridResultadoOperadorMensal.DataSource = Nothing
            gridResultadoOperadorOutros.DataSource = Nothing

            If (e.RowIndex > -1) Then

                ds = rn.RetornaDadosRelatorioOperador(dataInicio, dataFim, OperadorClicado, filtraData)

                If (ds.Tables(2).Rows.Count > 0) Then
                    dt = ds.Tables(2)
                    gridResultadoOperadorMensal.DataSource = dt

                    If dt.Rows.Count > 0 Then
                        For Each dr As DataRow In dt.Rows
                            valorTotalMensal += IIf(IsDBNull(dr.Item(8)), 0, dr.Item(8))
                            valorTotalRequisicao += IIf(IsDBNull(dr.Item(9)), 0, dr.Item(9))
                            valorTotalOperador += IIf(IsDBNull(dr.Item(10)), 0, dr.Item(10))
                        Next
                    End If
                End If

                If (ds.Tables(3).Rows.Count > 0) Then
                    dt = ds.Tables(3)
                    gridResultadoOperadorOutros.DataSource = dt

                    If dt.Rows.Count > 0 Then
                        For Each dr As DataRow In dt.Rows
                            valorTotalOutros += IIf(IsDBNull(dr.Item(8)), 0, dr.Item(8))
                        Next
                    End If


                End If


                lblValorTotalMensal.Text = "Valor Total : R$" + valorTotalMensal.ToString()
                lblValorTotalRequisicao.Text = "Valor Total Requisição: R$" + valorTotalRequisicao.ToString()
                lblValorTotalOperador.Text = "Valor Total Operador : R$" + valorTotalOperador.ToString()
                lblValorTotalOutros.Text = "Valor Total: R$" + valorTotalOutros.ToString()
                valorTotalArrecadadoOpera = valorTotalOutros + valorTotalOperador
                lblValorTotalArrecadadoOpera.Text = "R$" + valorTotalArrecadadoOpera.ToString()


                'colori as colunas com as informações importantes

                For Each col As DataGridViewRow In gridResultadoOperadorMensal.Rows

                    If Not IsDBNull(col.Cells(8).Value) Then
                        'coloco cor na celula
                        col.Cells(8).Style.ForeColor = Color.DodgerBlue
                    End If
                    If Not IsDBNull(col.Cells(9).Value) Then
                        'coloco cor na celula
                        col.Cells(9).Style.ForeColor = Color.Fuchsia
                    End If
                    If Not IsDBNull(col.Cells(10).Value) Then
                        'coloco cor na celula
                            col.Cells(10).Style.ForeColor = Color.LimeGreen
                    End If

                Next

            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub chkValorDiario_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkValorDiario.CheckedChanged

        Try

            If chkValorDiario.Checked = True Then
                chkFiltrarData.Checked = True
                dtInicio.Value = Date.Now
                dtFinal.Value = Date.Now
            Else
                chkFiltrarData.Checked = False
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    
    Private Sub btnPequisaRep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPequisaRep.Click
        Dim dataInicio As Date
        Dim dataFim As Date
        Dim ds As DataSet = New DataSet
        Dim dtResumoMensal As DataTable = New DataTable
        Dim dtResumoMensalComDesconto As DataTable = New DataTable
        Dim dtResumoPeriodo As DataTable = New DataTable
        Dim dt As DataTable = New DataTable
        Dim rn As RNGenerico = New RNGenerico
        Dim filtraData As String

        Try

            dataInicio = CDate(dtRepInicial.Value)
            dataFim = CDate(dtRepFinal.Value)
            If chkFiltaDataRep.Checked = True Then
                filtraData = "S"
            Else
                filtraData = "N"
            End If

            gridRepetidos.DataSource = Nothing

            ds = rn.RetornaRequisicoesRepetidas(dataInicio, dataFim, filtraData)

            gridRepetidos.DataSource = ds.Tables(0)


        Catch ex As Exception
            'MsgBox(ex)
        End Try
    End Sub
End Class