﻿Imports SISINT001
Imports Util

Public Class FrmConsultaRetornoOi

#Region "Variaveis"

    Private lstCriticados As New SortableBindingList(Of CGenerico)
    Private rnGenerico As RNGenerico = New RNGenerico
    Public bindingSourceCriticados As BindingSource = New BindingSource
    Public bindingSourceContribuite As BindingSource = New BindingSource
    Public bindingSourceDetalheTelefone As BindingSource = New BindingSource
    Private lstListaTelefonica As New SortableBindingList(Of CListaTelefonica)
    Private lstHistoricoOi As New SortableBindingList(Of CRetorno)
    Private rnRetorno As RNRetorno = New RNRetorno
    Public PosicaoGrid As Integer

#End Region

#Region "Enum"

    Private Enum ColunasGridRemessa

        DDD = 0
        Telefone = 1
        Nome = 2
        Valor = 3
        Cod_EAN_CLIENTE = 4
        cod_empresa = 6
        numeroLinha = 8

    End Enum

    Private Enum ColunasGridHistorico
        DDD = 0
        Telefone = 1
        Valor = 2
        categoria = 3
        mesRef = 4
        Situacao = 5
        MotivoSituacao = 6
        Data_Situacao = 7
    End Enum

    Private Enum ColunasGridHistoricoOi
        DDD = 0
        Telefone = 1
        Valor = 2
        categoria = 3
        mesRef = 4
        Situacao = 5
        MotivoSituacao = 6
        Data_Situacao = 7
    End Enum

    Enum xlsOption
        xlsOpen
    End Enum

#End Region

#Region "Cria Colunas"
    Private Sub CriarColunasGridCriticados()

        Try

            grdHistorioOi.DataSource = Nothing
            grdHistorioOi.Columns.Clear()

            grdHistorioOi.AutoGenerateColumns = False

            grdHistorioOi.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            Dim column As New DataGridViewTextBoxColumn

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StDDDMeioAcessoCobranca"
            column.HeaderText = "DDD"
            column.Width = 80
            column.ReadOnly = True
            grdHistorioOi.Columns.Add(column)

            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StNumMeioAcessoCobranca"
            column.HeaderText = "Telefone"
            column.Width = 80
            column.ReadOnly = True
            grdHistorioOi.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DVlrServico"
            column.HeaderText = "Valor"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            grdHistorioOi.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DdDataServico"
            column.HeaderText = "Data Remessa"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            grdHistorioOi.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DdDataSituacao"
            column.HeaderText = "Data Situação"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            grdHistorioOi.Columns.Add(column)


            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UsSitFaturamento"
            column.HeaderText = "Situacao Faturamento"
            column.Width = 160
            column.ReadOnly = False
            column.Visible = True
            grdHistorioOi.Columns.Add(column)

            '7
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UiMotivoSituacao"
            column.HeaderText = "Motivo Situação"
            column.Visible = True
            column.Width = 150
            column.ReadOnly = False
            grdHistorioOi.Columns.Add(column)

            '8
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StDescReferencia"
            column.HeaderText = "Cod_ean"
            column.Visible = True
            column.Width = 150
            column.ReadOnly = False
            grdHistorioOi.Columns.Add(column)

            '9
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "primeira_data_critica"
            column.HeaderText = "Data Primeira Critica"
            column.Visible = True
            column.Width = 150
            column.ReadOnly = False
            grdHistorioOi.Columns.Add(column)


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CriarColunasGridHistorico()

        Try

            GrdHistorico.DataSource = Nothing
            GrdHistorico.Columns.Clear()

            GrdHistorico.AutoGenerateColumns = False

            GrdHistorico.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            'Dim checkboxColumn As New DataGridViewCheckBoxColumn
            'checkboxColumn.Width = 32
            'checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_EAN_Cliente"
            column.HeaderText = "Cód. EAN Cliente"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Nome_contribuinte"
            column.HeaderText = "Nome contribuinte"
            column.Width = 300
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Data_mov"
            column.HeaderText = "Data Movimentação"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Tipo_mov"
            column.HeaderText = "Tipo Movimentação"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Tipo_cobranca"
            column.HeaderText = "Tipo Cobranca"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_Categoria"
            column.HeaderText = "Cód Categoria"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DDD"
            column.HeaderText = "DDD"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Telefone"
            column.HeaderText = "Telefone"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "valor"
            column.HeaderText = "Valor"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            GrdHistorico.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_recibo"
            column.HeaderText = "Cód. Recibo"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            GrdHistorico.Columns.Add(column)

            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cidade"
            column.HeaderText = "Cidade"
            column.Width = 80
            column.ReadOnly = True
            column.Visible = True
            GrdHistorico.Columns.Add(column)




        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CriarColunasGridHistoricoOi()

        Try

            gridHistoricoOi.DataSource = Nothing
            gridHistoricoOi.Columns.Clear()

            gridHistoricoOi.AutoGenerateColumns = False

            gridHistoricoOi.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            Dim column As New DataGridViewTextBoxColumn


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StDDDMeioAcessoCobranca"
            column.HeaderText = "DDD"
            column.Width = 80
            column.ReadOnly = True
            gridHistoricoOi.Columns.Add(column)

            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StNumMeioAcessoCobranca"
            column.HeaderText = "Telefone"
            column.Width = 80
            column.ReadOnly = True
            gridHistoricoOi.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DVlrServico"
            column.HeaderText = "Valor"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            gridHistoricoOi.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DdDataServico"
            column.HeaderText = "Data Remessa"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            gridHistoricoOi.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DdDataSituacao"
            column.HeaderText = "Data Situação"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            gridHistoricoOi.Columns.Add(column)


            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UsSitFaturamento"
            column.HeaderText = "Situacao Faturamento"
            column.Width = 160
            column.ReadOnly = False
            column.Visible = True
            gridHistoricoOi.Columns.Add(column)

            '7
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UiMotivoSituacao"
            column.HeaderText = "Motivo Situação"
            column.Visible = True
            column.Width = 150
            column.ReadOnly = False
            gridHistoricoOi.Columns.Add(column)

            '8
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StDescReferencia"
            column.HeaderText = "Cod_ean"
            column.Visible = True
            column.Width = 150
            column.ReadOnly = False
            gridHistoricoOi.Columns.Add(column)



        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

#End Region

#Region "Metodos"


    Private Sub btnPesquisar_Click(sender As Object, e As EventArgs) Handles btnPesquisar.Click
        Try
            Me.Cursor = Cursors.WaitCursor
            If cboTipoCritica.Text = "" Then
                MsgBox("Por favor selecione o tipo de critica a ser consultado!")
                Exit Sub
            End If
            carregaGridPorPeriodo(dtConsulta.Value, cboTipoCritica.Text)
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub carregaGridPorPeriodo(ByVal dtConsulta As Date, ByVal tipoCritica As String)

        Dim objGenerico As CGenerico = New CGenerico()
        Dim totalValorFichas As Integer
        Dim valor As Single
        Dim valor1 As Single
        Dim valor2 As Single
        Dim somaFichas As Integer
        Dim ds As DataSet = New DataSet

        Try
            ds = rnGenerico.CarregarCriticadosContestados(dtConsulta, tipoCritica)

            If (ds.Tables.Count > 0) Then
                CriarColunasGridCriticados()
                '----
                If Not IsNothing(ds.Tables(1)) Then
                    bindingSourceCriticados.DataSource = ds.Tables(1)
                    grdHistorioOi.DataSource = bindingSourceCriticados.DataSource
                End If
                '----
                If Not IsNothing(ds.Tables(2)) Then
                    bindingSourceCriticados.DataSource = ds.Tables(2)
                    grdExclusaoAuto.DataSource = bindingSourceCriticados.DataSource
                End If
                If Not IsNothing(ds.Tables(4)) Then
                    totalValorFichas = ds.Tables(4).Rows(0).Item(0)
                    'strValor = valor.ToString("C")
                    valor = totalValorFichas
                    lblValor.Text = valor.ToString("R$ #,###.00")
                Else
                    valor = 0
                    lblValor.Text = valor.ToString("R$ #,###.00")
                End If
                somaFichas = If(grdHistorioOi.Rows.Count > 0, (grdHistorioOi.Rows.Count - 1), 0)
                somaFichas += If(grdExclusaoAuto.Rows.Count > 0, (grdExclusaoAuto.Rows.Count - 1), 0)
                    lblTotalFichas.Text = somaFichas
                '--
                If Not IsNothing(ds.Tables(3)) Then
                    valor1 = ds.Tables(3).Rows(0).Item(0)
                    lblValorCriticasAuto.Text = valor1.ToString("R$ #,###.00")
                    valor2 = ds.Tables(3).Rows(1).Item(0)
                    lblValorExclusaoManual.Text = valor2.ToString("R$ #,###.00")
                End If
            Else
                    grdHistorioOi.DataSource = Nothing
                MsgBox("Não existe criticas a serem exibidas")
            End If

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub grdHistorioOi_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles grdHistorioOi.CellContentClick
        Try
            'Dim rn As RNGenerico = New RNGenerico
            'Dim rnContribuinte As RNContribuinte = New RNContribuinte
            'Dim dsMarketing As DataSet = New DataSet()

            'Dim ClienteClicado As String = CType(grdHistorioOi.Rows(e.RowIndex).Cells(ColunasGridRemessa.Cod_EAN_CLIENTE).Value, String)
            'Dim DDDSelecionado As String = CType(grdHistorioOi.Rows(e.RowIndex).Cells(ColunasGridRemessa.DDD).Value, String)
            'Dim TelefoneSelecionado As String = CType(grdHistorioOi.Rows(e.RowIndex).Cells(ColunasGridRemessa.Telefone).Value, String)
            'Dim valorEspecial As Decimal = CType(grdHistorioOi.Rows(e.RowIndex).Cells(ColunasGridRemessa.Valor).Value, Decimal)
            'PosicaoGrid = CType(grdHistorioOi.Rows(e.RowIndex).Cells(ColunasGridRemessa.numeroLinha).Value, Integer)

            'If (e.RowIndex > -1) Then
            '    lstListaTelefonica = New SortableBindingList(Of CListaTelefonica)(rn.Retorna_Historico(TelefoneSelecionado, DDDSelecionado, ClienteClicado))
            '    If (lstListaTelefonica.Count > 0) Then
            '        bindingSourceContribuite.DataSource = lstListaTelefonica
            '        GrdHistorico.DataSource = bindingSourceContribuite.DataSource
            '    Else
            '        GrdHistorico.DataSource = Nothing
            '    End If

            '    'CARREGA HISTORICO OI
            '    lstHistoricoOi = New SortableBindingList(Of CRetorno)(rnRetorno.RetornaHistoricoOi(TelefoneSelecionado, DDDSelecionado))
            '    If (lstHistoricoOi.Count > 0) Then
            '        bindingSourceDetalheTelefone.DataSource = lstHistoricoOi
            '        gridHistoricoOi.DataSource = bindingSourceDetalheTelefone.DataSource
            '    Else
            '        gridHistoricoOi.DataSource = Nothing
            '    End If
            'End If

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub FrmConsultaRetornoOi_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cboTipoCritica.SelectedIndex = 0

    End Sub

#End Region


End Class