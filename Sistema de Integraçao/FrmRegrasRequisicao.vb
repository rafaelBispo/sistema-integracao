﻿Imports SISINT001

Public Class FrmRegrasRequisicao



    Private Sub FrmRegrasRequisicao_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        CarregaComboOperador(0)
        CarregaComboCategorias(0)
    End Sub

    Private Sub CarregaComboOperador(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboOperador.DataSource = Nothing
            cboOperador.Items.Clear()
            cboOperador.DisplayMember = "Nome_Operador"
            cboOperador.ValueMember = "Cod_Operador"
            cboOperador.DataSource = rn_generico.BuscarOperador(0)

            If cboOperador.Items.Count > 0 And cod_operador = 0 Then
                cboOperador.SelectedIndex = 0
            ElseIf cboOperador.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperador.Items.Count - 1
                    For Each linha As CGenerico In cboOperador.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperador.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperador.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub
    Private Sub CarregaComboCategorias(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCategoria.DataSource = Nothing
            cboCategoria.Items.Clear()
            cboCategoria.DisplayMember = "Nome_Categoria"
            cboCategoria.ValueMember = "Cod_Categoria"
            cboCategoria.DataSource = rn_generico.BuscarCategoria(0)

            If cboCategoria.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoria.SelectedIndex = 0
            ElseIf cboCategoria.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoria.Items.Count - 1
                    For Each linha As CGenerico In cboCategoria.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoria.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoria.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalvar.Click
        Dim rn_generico As RNGenerico = New RNGenerico
        Try

            If rn_generico.DeletaRegras(cboOperador.SelectedValue) = True Then

                'REGISTRA OS CONTRIBUIU

                If chkSimAvulso.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 5, "Avulso", 5, 0)
                End If
                If chkSimAvulsoB.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 7, "Avulso B", 7, 0)
                End If
                If chkSimAvulsoRetido.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 14, "Avulso Retido", 14, 0)
                End If
                If chkSimDoacao.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 1, "Doação", 1, 0)
                End If
                If chkSimEspecial.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 4, "Especial", 4, 0)
                End If
                If chkSimInativo.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 8, "Inativo", 8, 0)
                End If
                If chkSimLista.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 2, "Lista", 2, 0)
                End If
                If chkSimListaB.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 1, "Lista B", 1, 0)
                End If
                If chkSimListaNova.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 10, "Lista Nova", 10, 0)
                End If
                If chkSimMensal.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 3, "Mensal", 3, 0)
                End If
                If chkSimMensalPrimeira.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 6, "Mensal Primeira", 6, 0)
                End If
                If chkSimMensalRetido.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 13, "Mensal Retido", 13, 0)
                End If
                If ChkSimPedirMensal.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 3, "Pedir Mensal", 3, 0)
                End If
                If chkSimRecuperacao.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 9, "Recuperação", 9, 0)
                End If
                If chkSimRecuperaRetido.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 15, "Recuperacao Retido", 15, 0)
                End If

                'REGISTRA OS NÃO CONTRIBUIU

                If chkNaoAvulso.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 5, "Avulso", 0, 5)
                End If
                If chkNaoAvulsoB.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 7, "Avulso B", 0, 7)
                End If
                If chkNaoAvulsoRetido.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 14, "Avulso Retido", 0, 14)
                End If
                If chkNaoDoacao.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 1, "Doação", 0, 1)
                End If
                If chkNaoEspecial.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 4, "Especial", 0, 4)
                End If
                If chkNaoInativo.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 8, "Inativo", 0, 8)
                End If
                If chkNaoLista.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 2, "Lista", 0, 2)
                End If
                If chkNaoListaB.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 1, "Lista B", 0, 1)
                End If
                If chkNaoListaNova.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 10, "Lista Nova", 0, 10)
                End If
                If chkNaoMensal.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 3, "Mensal", 0, 3)
                End If
                If chkNaoMensalPrimeira.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 6, "Mensal Primeira", 0, 6)
                End If
                If chkNaoMensalRetido.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 13, "Mensal Retido", 0, 13)
                End If
                If ChkNaoPedirMensal.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 3, "Pedir Mensal", 0, 3)
                End If
                If chkNaoRecuperacao.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 9, "Recuperação", 0, 9)
                End If
                If chkNaoRecuperaRetido.Checked = True Then
                    rn_generico.SalvaRegras(cboOperador.SelectedValue, cboOperador.Text, 15, "Recuperacao Retido", 0, 15)
                End If

                MsgBox("Dados salvos com sucesso!")
                Me.Visible = False

            Else
                MsgBox("Ocorreu um erro ao salvar os dados")
            End If
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Close()
    End Sub


    Private Sub btnAlterar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub btnFinanceiro_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim frm As New FrmFinanceiro

        Try
            frm.Show()
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub rbOperadora_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbOperadora.CheckedChanged
        If rbOperadora.Checked = True Then
            cboCategoria.Visible = False
            lblCategoria.Visible = False
        Else
            cboCategoria.Visible = True
            lblCategoria.Visible = True
        End If
    End Sub

    Private Sub rbCategoria_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbCategoria.CheckedChanged
        If rbCategoria.Checked = True Then
            cboOperador.Visible = False
            lblOperadora.Visible = False
        Else
            cboOperador.Visible = True
            lblOperadora.Visible = True
        End If
    End Sub

    Private Sub btnGrava_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrava.Click
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim diasAceite As Integer
        Dim diasRecusa As Integer
        Try

            diasAceite = txtDiasAceite.Text
            diasRecusa = txtDiasRecusa.Text

            If rn_generico.SalvaRegraEspecial(diasAceite, diasRecusa) = True Then

            End If

        Catch ex As Exception

        End Try
    End Sub
End Class