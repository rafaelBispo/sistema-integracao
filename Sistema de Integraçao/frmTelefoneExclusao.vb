﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Windows.Forms.Screen
Imports System.Drawing.Graphics
Imports System
Imports System.Net
Imports System.Net.Mail
Public Class frmTelefoneExclusao


#Region "variaveis"

    Public pValor As Decimal
    Public pMedia As Decimal
    Public pDiferenca As Decimal
    Public pTelefone As String

    Private lstListaTelefonica As New SortableBindingList(Of CListaTelefonica)
    Public bindingSourceContribuite As BindingSource = New BindingSource
    Private contribuinte As CContribuinte = New CContribuinte

    Public isExcluido As Boolean
    Public caminhoArquivoSalvo As String

#End Region

#Region "Propriedades"


    Public Property Valor() As String
        Get
            Return pValor
        End Get
        Set(ByVal value As String)
            pValor = value
        End Set
    End Property


    Public Property Media() As Decimal
        Get
            Return pMedia
        End Get
        Set(ByVal value As Decimal)
            pMedia = value
        End Set
    End Property


    Public Property Diferenca() As Decimal
        Get
            Return pDiferenca
        End Get
        Set(ByVal value As Decimal)
            pDiferenca = value
        End Set
    End Property

    Public Property Telefone() As String
        Get
            Return pTelefone
        End Get
        Set(ByVal value As String)
            pTelefone = value
        End Set
    End Property


#End Region

    Private Sub frmTelefoneExclusao_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            CarregaComboSolicitante()
            CarregaComboMotivo()


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboSolicitante()
        Try

            cboSolicitante.Items.Add("VIA TELEFONE")
            cboSolicitante.Items.Add("EMAIL")
            cboSolicitante.Items.Add("EMAIL OI")
            cboSolicitante.Items.Add("EMAIL ANATEL")
            cboSolicitante.Items.Add("EMAIL ALMAVIVA")
            cboSolicitante.Items.Add("EMAIL PROCON")
            cboSolicitante.Items.Add("OI TURBINA")

            cboSolicitante.SelectedIndex = 0

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub CarregaComboMotivo()
        Try

            cboMotivo.Items.Add("DESEJA PARAR COM A DOAÇÃO")
            cboMotivo.Items.Add("CANCELAMENTO DE DOAÇÃO")
            cboMotivo.Items.Add("COBRANÇA INDEVIDA")
            cboMotivo.Items.Add("PROCESSO JUDICIAL")

            cboMotivo.SelectedIndex = 0

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnExcluir_Click(sender As System.Object, e As System.EventArgs) Handles btnExcluir.Click
       
        ExcluirRegistro()

    End Sub

    Public Sub ExcluirRegistro()
        Dim rn As New RNContribuinte
        Dim ds As New DataSet
        Dim DDD As String = ""
        Dim Telefone As String = ""
        Dim motivo As String = ""
        Dim observacao As String = ""
        Dim solicitante As String = ""
        Dim dtExclusao As String = ""
        Dim cod_ean As String = ""
        Dim Frm = New FrmPrint

        Try

            Telefone = txtTelefonePesquisa.Text
            Telefone = Replace(Telefone, "-", "")
            DDD = txtDDDPesquisa.Text
            motivo = cboMotivo.Text
            solicitante = cboSolicitante.Text
            observacao = txtObsDetalhe.Text
            cod_ean = txtCodBarra.Text

            caminhoArquivoSalvo = "C:\teste\" & txtDDDPesquisa.Text + " " + txtTelefonePesquisa.Text & ".jpg"

            If rn.Excluir(cod_ean, DDD, Telefone, motivo, solicitante, observacao) = True Then

                lblMensagem.Visible = True

                Dim FrmMensagem As New FrmMensagem
                FrmMensagem.Show()


                MsgBox("Iniciando captura de tela", vbOKOnly, "atenção")
                Application.DoEvents()
                System.Threading.Thread.Sleep(1000)

                Frm.PictureBox1.Image = CapturaTela()
                Frm.local = caminhoArquivoSalvo

                Frm.Show()

                'verifica se vai enviar e-mail para o contribuinte
                If txtEmail.Text <> "" Then
                    Dim result = MessageBox.Show("Deseja enviar um e-mail para o contribuinte pelo e-mail " + txtEmail.Text + " ?", "Atenção", _
                            MessageBoxButtons.YesNo, _
                            MessageBoxIcon.Question)

                    If (result = DialogResult.Yes) Then
                        EnviaEmail()

                    End If

                End If
                FrmMensagem.Close()
            Else
                MsgBox("Ocorreu um erro ao realizar a exclusão")
            End If


            btnExcluir.Enabled = False


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Close()
    End Sub

    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Dim ClienteClicado As String
        Dim DDDSelecionado As String = ""
        Dim TelefoneSelecionado As String = ""
        Dim rn As RNGenerico = New RNGenerico
        Dim rnContribuinte As RNContribuinte = New RNContribuinte


        Try


            LimpaTela()


            DDDSelecionado = txtDDDPesquisa.Text
            TelefoneSelecionado = txtTelefonePesquisa.Text
            TelefoneSelecionado = Replace(TelefoneSelecionado, "-", "")

            lstListaTelefonica = New SortableBindingList(Of CListaTelefonica)(rn.Retorna_Historico(TelefoneSelecionado, DDDSelecionado, ClienteClicado))

            If (lstListaTelefonica.Count > 0) Then

                bindingSourceContribuite.DataSource = lstListaTelefonica

                GrdHistorico.DataSource = bindingSourceContribuite.DataSource

            Else

                GrdHistorico.DataSource = Nothing

            End If


            contribuinte = rnContribuinte.CarregarContribuintePorTelefone(TelefoneSelecionado, DDDSelecionado, ClienteClicado, "ConsultaUltimaMovimentacao")

            If (contribuinte.Cod_EAN_Cliente = Nothing Or contribuinte.Cod_EAN_Cliente = "") Then

                contribuinte = rnContribuinte.PesquisarTabelaRetidoPorTelefone(TelefoneSelecionado, DDDSelecionado, ClienteClicado)
            End If

            'BUSCA AS INFORMAÇÕES NA TABELA DE LISTA TELEFONICA SE ACHA COMPLETA COM OS DADOS DO CLIENTE
            If (contribuinte.Cod_EAN_Cliente = Nothing) Then
                contribuinte = rnContribuinte.BuscaListaTelefonica(TelefoneSelecionado, DDDSelecionado, ClienteClicado)
                If (contribuinte.Cod_EAN_Cliente = Nothing) Then
                    contribuinte = rnContribuinte.PesquisarPorCodBarra(contribuinte.Cod_EAN_Cliente)
                End If
            End If


            If Not contribuinte Is Nothing And Not contribuinte.Telefone1 Is Nothing Then
                'popula a tela
                txtCodigo.Text = contribuinte.CodCliente
                txtNomeCliente1.Text = contribuinte.NomeCliente1
                txtNomeCliente2.Text = contribuinte.NomeCliente2
                txtCodBarra.Text = contribuinte.Cod_EAN_Cliente
                txtCnpjCpf.Text = contribuinte.CNPJ_CPF
                txtRg.Text = contribuinte.IE_CI
                txtEmail.Text = contribuinte.Email
                txtTelefone1.Text = LTrim(RTrim(contribuinte.Telefone1))
                txtTelefone2.Text = LTrim(RTrim(contribuinte.Telefone2))
                txtEndereco.Text = contribuinte.Endereco
                txtUF.Text = contribuinte.UF
                txtCEP.Text = contribuinte.CEP

                txtRegiao.Text = contribuinte.Cod_Regiao

                txtDtNc.Text = contribuinte.DataNascimento
                txtDtNcMp.Text = contribuinte.DT_NC_MP
                txtDtCadastro.Text = contribuinte.DataCadastro
                txtReferencia.Text = contribuinte.Referencia

                txtObs.Text = contribuinte.Observacao

                txtMelhorDia.Text = contribuinte.DiaLimite
                If (contribuinte.Valor <= 5) Then
                    contribuinte.Valor = 20
                End If

                txtValDoacao.Text = Convert.ToDouble(LTrim(RTrim(contribuinte.Valor))).ToString("R$ #,###.00")

                TxtDDD.Text = contribuinte.DDD

                CarregaComboCidades(contribuinte.Cod_Cidade)
                CarregaComboCategoriasDetalhes(contribuinte.Cod_Categoria)
                CarregaComboOperadorDetalhe(contribuinte.Cod_Operador)
                CarregaComboBairros(contribuinte.Cod_Bairro)
                CarregaComboEspecie(contribuinte.Cod_Especie)

                'busca na tabela de exclusão
                Dim dsExclusao As DataSet = New DataSet
                Dim dt As DataTable = New DataTable
                Dim motivo As String = ""
                Dim observacao As String = ""
                Dim solicitante As String = ""
                Dim dtExclusao As String = ""

                dsExclusao = rnContribuinte.Busca_TelefoneExclusao(TelefoneSelecionado, DDDSelecionado)

                If (dsExclusao.Tables(0).Rows.Count > 0) Then
                    If dsExclusao.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                        dt = dsExclusao.Tables(0)
                    End If

                    Dim frm As New FrmObservacaoExclusao

                    For Each linha As DataRow In dt.Rows

                        frm.dtExclusao = IIf(IsDBNull(linha("dt_inclusao")), "--", linha("dt_inclusao"))
                        frm.motivo = IIf(IsDBNull(linha("motivo")), "Não Informado", linha("motivo"))
                        frm.observacao = IIf(IsDBNull(linha("observacao")), "--", linha("observacao"))
                        frm.solicitante = IIf(IsDBNull(linha("solicitante")), "Não Informado", linha("solicitante"))
                        frm.DDD = DDDSelecionado
                        frm.telefone = TelefoneSelecionado

                    Next

                    frm.Show()
                    MsgBox("Contribuinte já excluido", MsgBoxStyle.Critical, "Atenção!")
                    Application.DoEvents()
                    System.Threading.Thread.Sleep(1000)
                    btnExcluir.Enabled = False
                    frm.IniciaCaptura()

                Else

                    btnExcluir.Enabled = True
                End If



            Else

                'busca na tabela de exclusão
                Dim dsExclusao As DataSet = New DataSet
                Dim dt As DataTable = New DataTable
                Dim motivo As String = ""
                Dim observacao As String = ""
                Dim solicitante As String = ""
                Dim dtExclusao As String = ""

                dsExclusao = rnContribuinte.Busca_TelefoneExclusao(TelefoneSelecionado, DDDSelecionado)

                If (dsExclusao.Tables(0).Rows.Count > 0) Then
                    If dsExclusao.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                        dt = dsExclusao.Tables(0)
                    End If

                    Dim frm As New FrmObservacaoExclusao

                    For Each linha As DataRow In dt.Rows

                        frm.dtExclusao = IIf(IsDBNull(linha("dt_inclusao")), "--", linha("dt_inclusao"))
                        frm.motivo = IIf(IsDBNull(linha("motivo")), "--", linha("motivo"))
                        frm.observacao = IIf(IsDBNull(linha("observacao")), "--", linha("observacao"))
                        frm.solicitante = IIf(IsDBNull(linha("solicitante")), "--", linha("solicitante"))
                        frm.DDD = txtDDDPesquisa.Text
                        frm.telefone = txtTelefonePesquisa.Text

                    Next

                    frm.Show()
                    MsgBox("Contribuinte já excluido", MsgBoxStyle.Critical, "Atenção!")
                    Application.DoEvents()
                    System.Threading.Thread.Sleep(1000)
                    btnExcluir.Enabled = False
                    frm.IniciaCaptura()

                Else

                    Dim result = MessageBox.Show("Registro Não Encontrado, deseja incluir na lista de exclusão? ", "Atenção", _
                           MessageBoxButtons.YesNo, _
                           MessageBoxIcon.Question)

                    If (result = DialogResult.Yes) Then
                        ExcluirRegistro()
                    End If
                      
                    End If


            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub CarregaComboCidades(ByVal cod_cidade As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCidade.DataSource = Nothing
            cboCidade.Items.Clear()
            cboCidade.DisplayMember = "Nome_cidade"
            cboCidade.ValueMember = "Cod_Cidade"
            cboCidade.DataSource = rn_generico.BuscarCidades(0)

            If cboCidade.Items.Count = 0 Then
                cboCidade.SelectedIndex = 0
            ElseIf cboCidade.Items.Count > 0 And cod_cidade <> 0 Then

                For i As Integer = 0 To cboCidade.Items.Count - 1
                    For Each linha As CGenerico In cboCidade.DataSource
                        If linha.Cod_Cidade = cod_cidade Then
                            cboCidade.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next

            Else

                cboCidade.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CarregaComboBairros(ByVal cod_bairro As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try



            cboBairroDetalhe.DataSource = Nothing
            cboBairroDetalhe.Items.Clear()
            cboBairroDetalhe.DisplayMember = "Nome_Bairro"
            cboBairroDetalhe.ValueMember = "Cod_Bairro"
            cboBairroDetalhe.DataSource = rn_generico.BuscarBairros(0)

            If cboBairroDetalhe.Items.Count > 0 And cod_bairro = 0 Then
                cboBairroDetalhe.SelectedIndex = 0

            ElseIf cboBairroDetalhe.Items.Count > 0 And cod_bairro <> 0 Then
                For i As Integer = 0 To cboBairroDetalhe.Items.Count - 1
                    For Each linha As CGenerico In cboBairroDetalhe.DataSource
                        If linha.Cod_Bairro = cod_bairro Then
                            cboBairroDetalhe.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboBairroDetalhe.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboOperadorDetalhe(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboOperadorDetalhe.DataSource = Nothing
            cboOperadorDetalhe.Items.Clear()
            cboOperadorDetalhe.DisplayMember = "Nome_Operador"
            cboOperadorDetalhe.ValueMember = "Cod_Operador"
            cboOperadorDetalhe.DataSource = rn_generico.BuscarOperador(0)

            If cboOperadorDetalhe.Items.Count > 0 And cod_operador = 0 Then
                cboOperadorDetalhe.SelectedIndex = 0
            ElseIf cboOperadorDetalhe.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperadorDetalhe.Items.Count - 1
                    For Each linha As CGenerico In cboOperadorDetalhe.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperadorDetalhe.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperadorDetalhe.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboEspecie(ByVal cod_especie As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try


            cboEspecieDetalhe.DataSource = Nothing
            cboEspecieDetalhe.Items.Clear()
            cboEspecieDetalhe.DisplayMember = "Nome_Especie"
            cboEspecieDetalhe.ValueMember = "Cod_Especie"
            cboEspecieDetalhe.DataSource = rn_generico.BuscarEspecie(0)

            If cboEspecieDetalhe.Items.Count > 0 And cod_especie = 0 Then
                cboEspecieDetalhe.SelectedIndex = 0
            ElseIf cboEspecieDetalhe.Items.Count > 0 And cod_especie <> 0 Then
                For i As Integer = 0 To cboEspecieDetalhe.Items.Count - 1
                    For Each linha As CGenerico In cboEspecieDetalhe.DataSource
                        If linha.Cod_Especie = cod_especie Then
                            cboEspecieDetalhe.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboEspecieDetalhe.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try



    End Sub

    Private Sub CarregaComboCategoriasDetalhes(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCategoriaDetalhe.DataSource = Nothing
            cboCategoriaDetalhe.Items.Clear()
            cboCategoriaDetalhe.DisplayMember = "Nome_Categoria"
            cboCategoriaDetalhe.ValueMember = "Cod_Categoria"
            cboCategoriaDetalhe.DataSource = rn_generico.BuscarCategoria(0)

            If cboCategoriaDetalhe.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoriaDetalhe.SelectedIndex = 0
            ElseIf cboCategoriaDetalhe.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoriaDetalhe.Items.Count - 1
                    For Each linha As CGenerico In cboCategoriaDetalhe.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoriaDetalhe.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoriaDetalhe.SelectedIndex = 1
            End If



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Public Sub LimpaTela()

        Try



            txtCodBarra.Text = ""
            txtCodigo.Text = ""
            txtNomeCliente1.Text = ""
            txtNomeCliente2.Text = ""
            txtDDD.Text = ""
            txtTelefone1.Text = ""
            txtTelefone2.Text = ""
            txtCnpjCpf.Text = ""
            txtRg.Text = ""
            txtMelhorDia.Text = ""
            txtValDoacao.Text = ""
            txtDtNc.Text = ""
            txtDtNcMp.Text = ""
            txtReferencia.Text = ""
            txtRegiao.Text = ""
            'txtObs.Text = ""
            txtCEP.Text = ""
            txtUF.Text = ""
            txtEndereco.Text = ""
            txtTelefone1.Text = ""
            txtEmail.Text = ""
            txtDtCadastro.Text = ""

            If cboBairroDetalhe.Text <> "" Then
                cboBairroDetalhe.SelectedIndex = 0
            End If
            If cboCategoriaDetalhe.Text <> "" Then
                cboCategoriaDetalhe.SelectedIndex = 0
            End If
            If cboCidade.Text <> "" Then
                cboCidade.SelectedIndex = 0
            End If
            If cboEspecieDetalhe.Text <> "" Then
                cboEspecieDetalhe.SelectedIndex = 0
            End If
            If cboOperadorDetalhe.Text <> "" Then
                cboOperadorDetalhe.SelectedIndex = 0
            End If


            lblMensagem.Visible = False

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub salvaTela()
        Dim caminho As String = "c:\teste\"
        Try
            Dim ScreenSize As Size = New Size(Me.Width, Me.Height)
            Dim tela As New Bitmap(PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height, Imaging.PixelFormat.Format32bppArgb)
            Dim g As Graphics = FromImage(tela)

            g.CopyFromScreen(Me.Location.X - 3, Me.Location.Y - 3, -7, -3, Me.Size, CopyPixelOperation.SourceCopy)
            tela.Save(caminho & txtDDDPesquisa.Text + " " + txtTelefonePesquisa.Text & ".jpg", Imaging.ImageFormat.Jpeg)
            caminhoArquivoSalvo = caminho & txtDDDPesquisa.Text + " " + txtTelefonePesquisa.Text & ".jpg"
           
        Catch ex As Exception
            MsgBox("Erro : " & ex.Message)
        End Try

    End Sub

    Public Shared Function CapturaTela() As Bitmap
        Try
            Dim BMP As New Bitmap(frmTelefoneExclusao.Width, frmTelefoneExclusao.Height, Imaging.PixelFormat.Format32bppArgb)
            Dim GFX As Graphics = FromImage(BMP)
            GFX.CopyFromScreen(frmTelefoneExclusao.Location.X, frmTelefoneExclusao.Location.Y, 0, 0, frmTelefoneExclusao.Size, CopyPixelOperation.SourceCopy)
            Return BMP
        Catch ex As Exception
            MsgBox("Erro : " & ex.Message)
        End Try
    End Function

    Public Sub EnviaTela()
        Dim caminho As String = "c:\teste\"
        Try
            Dim BMP As New Bitmap(PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height, Imaging.PixelFormat.Format32bppArgb)
            Dim GFX As Graphics = FromImage(BMP)
            GFX.CopyFromScreen(Me.Location.X, Me.Location.Y, 0, 0, Me.Size, CopyPixelOperation.SourceCopy)
            BMP.Save(caminho & txtDDDPesquisa.Text + " " + txtTelefonePesquisa.Text & ".jpg", Imaging.ImageFormat.Jpeg)
            caminhoArquivoSalvo = caminho & txtDDDPesquisa.Text + " " + txtTelefonePesquisa.Text & ".jpg"
        Catch ex As Exception
            MsgBox("Erro : " & ex.Message)
        End Try
    End Sub

  
    Private Sub btnCapSalva_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCapSalva.Click
        salvaTela()
    End Sub


    Private Sub btnCapExibe_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCapExibe.Click
        Dim Frm = New FrmPrint

        Frm.PictureBox1.Image = CapturaTela()
        Frm.local = "C:\teste\" & txtDDDPesquisa.Text + " " + txtTelefonePesquisa.Text & ".jpg"
        'Frm.Show()
        'EnviaTela()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        FrmEnviaEmail.Show()
    End Sub

    Private Sub EnviaEmail()
        Try

            'create the mail message
            Dim SmtpServer As New SmtpClient()
            Dim mail As New MailMessage()
            SmtpServer.Credentials = New Net.NetworkCredential("aaci.jf.mg@gmail.com", "kwcmuyymqekemvtz")
            SmtpServer.Host = "smtp.gmail.com"
            SmtpServer.Port = 587 'server para gmail
            SmtpServer.EnableSsl = True
            mail.From = New MailAddress("aaci.jf.mg@gmail.com")
            mail.To.Add(txtEmail.Text) 'Email que recebera a Mensagem
            mail.Subject = "Exclusão Cadastro AACI"
            mail.Body = ""

            'Este codigo PlainView e caso o email não suporte Html, nisto ele irá informar este erro

            Dim plainView As AlternateView = AlternateView.CreateAlternateViewFromString("Boa tarde,como solicitado, estamos excluindo o seu cadastro do nosso sistema. Desculpe o transtorno, agradeço a compreensão.", Nothing, "text/plain")

            'crie um corpo de texto caso deseje assim você poderá colocar um texto no meio de duas imagens
            Dim linha1 As String = ""
            If Date.Now.Hour < 12 Then
                linha1 = "Bom dia,"
            Else
                linha1 = "Boa tarde,"
            End If

            Dim linha2 As String = "como solicitado, estamos excluindo o seu cadastro do nosso sistema."
            Dim linha3 As String = "desculpe o&nbsp;transtorno, agrade&ccedil;o a compreens&atilde;o."

            'Vamos Iniciar a Parte das Imagens
            ' as imagens ficam no prefixo 'cid' eles irão pegar os seus ContentId e irão destinar as imagens 
            ' você pode criar varias imagens colocando diferentes ContentId 
            ' Veja: <img src='cid:companylogo'> o LinkedResource ( que veremos logo a baixo ) irá destinar a imagem que tenha o  ContentId com o nome de  'companylogo'

            ' aqui você cria o corpo da mensagem com ele você pode criar e desenvolver mais


            'Dim htmlView As AlternateView = AlternateView.CreateAlternateViewFromString("<img src=cid:companylogo> <p> <class='body'>  " + body + "</div>  <img src=cid:Comlog>   ", Nothing, "text/html")
            Dim htmlView As AlternateView = AlternateView.CreateAlternateViewFromString("<p> " + linha1 + "</p> <p>&nbsp;</p> <p>" + linha2 + "</p> <p>&nbsp;</p> <img src=cid:companylogo> <p>&nbsp;</p> <p>" + linha3 + "</p> <p><br /> <br /> --</p> <p>&nbsp;</p> <p>att..</p> <p>&nbsp;</p> <p><strong>AACI - ASSOCIA&Ccedil;&Atilde;O DE APOIO AS CRIAN&Ccedil;AS E IDOSOS</strong></p>   ", Nothing, "text/html")


            'você irá dizer aonde estão as suas imagens e elas irão para Logo e Logo1

            Dim logo As New LinkedResource(caminhoArquivoSalvo)

            ' Lembra do ContentId então aqui que você irá dizer aonde fica as imagens
            ' Olhe ali o htmlview ele tem o ' <img scr=cid:companylogo> ' ele que diz a que a imagem que tenha a propriedade com o nome 'Companylogo' irá ficar aqui
            ' e é nesta parte aonde você diz o nome delas.

            logo.ContentId = "companylogo"

            ' ok agora o Sistema sabe que a ContentId da imagem 'logo' tem o nome de 'companylogo' mas ele não sabe aonde você quer adicionar, 
            'por isto você deve informar. 
            ' como temos mais de uma imagem você deve adicionar mais de uma linha conforme visto a baixo.
            htmlView.LinkedResources.Add(logo)

            'ok, agora só criar o email, digitando o Plainview aonde ele irá criar um segundo plano caso o email não aceite html e irá colocar a imagem caso aceite 

            With mail
                .AlternateViews.Add(plainView)
                .AlternateViews.Add(htmlView)

            End With

            mail.IsBodyHtml = True

            SmtpServer.Send(mail)
            MsgBox("E-mail enviado com sucesso", vbInformation, "Ok")





        Catch ex As Exception  'caso haja algum erro crie uma msg de aviso.
            MsgBox(ex)
        Finally
            '  File.Delete("")
        End Try
    End Sub

End Class