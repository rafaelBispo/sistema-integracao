﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text
Imports Microsoft.Office.Interop
Imports System.Net.Mail
Imports Microsoft.VisualBasic.ControlChars

Public Class FrmExportar

    Public Csv As ExportarCSV
    Public conv As Conversor
    Public NomeArquivo As String


    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click

        Try

            If GeraArquivoExportar() = True Then
                MsgBox("Arquivo Gerado com sucesso!", MsgBoxStyle.Information, "Atenção!")
                If chkEnviaEmail.Checked = True Then
                    Dim result = MessageBox.Show("Deseja enviar um e-mail com a planilha gerada?", "Atenção", _
                         MessageBoxButtons.YesNo, _
                         MessageBoxIcon.Question)

                    If (result = DialogResult.Yes) Then
                            EnviaEmail()
                    End If
                End If
            End If

        Catch ex As Exception
            MsgBox(ex)
        End Try



        'Dim seg As Integer = Now.Second
        'Dim destino As String = "C:\teste\ArquivoTeste" + seg.ToString + ".csv"

        '    Csv = New ExportarCSV()

        'Try
        '    Using CsvWriter As New StreamWriter(destino)
        '        'obtem o datatable e gera o arquivo csv
        '        CsvWriter.Write(Csv.CsvDoDataTable(PegaDados()))
        '    End Using

        '    If (MsgBox("Geração do arquivo concluida com sucesso. Deseja exibir o arquivo gerado ? ", MsgBoxStyle.YesNo) = DialogResult.Yes) Then
        '        System.Diagnostics.Process.Start(destino)
        '    End If
        'Catch ex As Exception
        '    Throw ex
        'End Try



    End Sub

    Public Function GeraArquivoExportar() As Boolean
        Dim dsArquivo As DataSet = New DataSet
        Dim rnGenerico As New RNGenerico
        Dim tipoArquivo As String = ""
        Dim objeto As String = ""


        Try
            Me.Cursor = Cursors.WaitCursor

            Dim dataInicio As DateTime = DateTime.ParseExact(dtInicio.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
            Dim fDtInicio As String = dataInicio.ToString("yyyyMMdd", CultureInfo.InvariantCulture)

            Dim dataFim As DateTime = DateTime.ParseExact(dtFim.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture)
            Dim fDtFim As String = dataFim.ToString("yyyyMMdd", CultureInfo.InvariantCulture)

            If chkReqOi.Checked Then
                tipoArquivo = "ROI_"
                dsArquivo = rnGenerico.CriaArquivoRequisicaoOiExportar(dtInicio.Value, dtFim.Value)
            ElseIf chkRequisicao_tb.Checked Then
                tipoArquivo = "REQ_"
                dsArquivo = rnGenerico.CriaArquivoRequisicaoExportar()
            ElseIf chkLista.Checked Then
                tipoArquivo = "LIS_"
                dsArquivo = rnGenerico.CriaArquivoListaTelefonicaExportar()
            ElseIf chkTelefoneExclusao.Checked Then
                tipoArquivo = "TEX_"
                dsArquivo = rnGenerico.CriaArquivoTelefoneExclusaoExportar()
            End If

            If dsArquivo.Tables(0).Rows.Count > 0 Then

                NomeArquivo = "C:\Digital\" + tipoArquivo + fDtInicio + "_ate_" + fDtFim + ".txt"


                Dim SW As New StreamWriter(NomeArquivo) ' Cria o arquivo de texto


                For Each row In dsArquivo.Tables(0).Rows 'select do detalhe
                    Dim linha As String = ""
                    For Each obj In row.ItemArray

                        objeto = obj.ToString()
                        'objeto = objeto.Replace(ControlChars.Back, "").ToString
                        'objeto = objeto.Replace(ControlChars.Cr, "").ToString
                        'objeto = objeto.Replace(ControlChars.CrLf, "").ToString
                        'objeto = objeto.Replace(ControlChars.Lf, "").ToString
                        objeto = objeto.Replace(Chr(10), "")
                        objeto = objeto.Replace(Chr(13), "")

                        'teste
                        If obj.ToString() = "*110805-181*" Then
                            MsgBox("aqui")
                        End If

                        If linha = "" Then
                            linha = "|" + obj.ToString()
                        Else
                            linha = linha + ";" + obj.ToString()
                        End If

                    Next

                    SW.WriteLine(linha) ' Grava o detalhe

                Next

                SW.Close() 'Fecha o arquivo de texto

                SW.Dispose() 'Libera a memória utilizada

            End If

            Me.Cursor = Cursors.Default

            Return True

        Catch ex As Exception

            'daoContribuinte.RollBackTransaction()
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Me.Cursor = Cursors.Default
            Return False

        End Try


    End Function

    Private Function PegaDados() As DataTable
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim ds As DataSet = New DataSet
        Try
            ds = rnRequisicao.CarregaFichasExportacaoOi()
            If Not IsDBNull(ds.Tables(0)) Then
                Return ds.Tables(0)
            Else
                Return Nothing
            End If

        Catch ex As Exception
            Throw ex
            Return Nothing
        End Try
    End Function

   
    Private Sub FrmExportar_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim dataInicio As Date
        Dim dataFim As Date
        Dim data As Date

        'PEGO O NUMERO DO DIA ATUAL
        Dim diaAtual As Integer = Date.Today.DayOfWeek

        'SUBTRAIO O DIA ATUAL E ACRESCENTO UM DIA PARA ENCONTRAR A SEGUNDA FEIRA
        Data = Date.Now.AddDays(-diaAtual + 1)

        'SUBTRAIO -7 DIAS PARA ENCONTRAR A SEGUNDA DA SEMANA PASSADA
        Data = Data.AddDays(-7)

        dataInicio = Data
        dataFim = dataInicio.AddDays(5)

        dtInicio.Value = dataInicio
        dtFim.Value = dataFim

        Dim arquivoExiste As Boolean
        arquivoExiste = My.Computer.FileSystem.FileExists(My.Application.Info.DirectoryPath & "/log.txt")
        If arquivoExiste = False Then
            If MessageBox.Show("O arquivo de log não existe, você gostaria de criar um agora ?", "Arquivo de Log", MessageBoxButtons.YesNo,
 MessageBoxIcon.Question) = DialogResult.Yes Then
                My.Computer.FileSystem.WriteAllText(My.Application.Info.DirectoryPath & "/log.txt", String.Empty, False)
                MsgBox("Arquivo de log criado!")
            End If
        End If
        Dim xLog As String = My.Application.Info.DirectoryPath & "/log.txt"
        Dim vt As String = vbCrLf & My.Application.Info.Version.ToString & " - " & My.User.Name & " - Programa Iniciado." & Date.Now
        Try
            My.Computer.FileSystem.WriteAllText(xLog, vt, True)
        Catch ex As Exception
            MessageBox.Show(ex.ToString, "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnLog_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLog.Click
        Dim xLog As String = My.Application.Info.DirectoryPath & "/log.txt"
        Diagnostics.Process.Start(xLog)
    End Sub

    Private Sub btnImportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportar.Click
        'Declara uma variável do tipo StreamWriter
        Dim Leitura As StreamReader
        Dim rnRetorno As RNRetorno = New RNRetorno
        Dim retornoList As List(Of CRetorno) = New List(Of CRetorno)
        Dim HeaderList As List(Of CRetorno) = New List(Of CRetorno)
        Dim TraillerList As List(Of CRetorno) = New List(Of CRetorno)
        Dim GravarList As List(Of CRetorno) = New List(Of CRetorno)
        Dim usuario As String = ""
        Dim id_arq As Integer
        Dim nome_arq As String = ""
        Dim UF As String
        Dim strLinhaAnterior As String = ""
        Dim strLinhaAUX As String = ""


        Try

            'Verifica se já existe um arquivo com este nome
            If File.Exists(txtArquivo.Text) = False Then
                MsgBox("Este arquivo não existe")
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor

            'Cria um novo arquivo de acordo com o nome digitado e passa para o objeto StremWriter
            Leitura = File.OpenText(txtArquivo.Text)

            nome_arq = NomeArquivo

            'Declara as variáveis dos campos
            Dim strLinha As String

            ''retorna id do arquivo
            'id_arq = rnRetorno.RetornaIdArquivo()

            'Faz um loop dentro das linhas do arquivo
            While (Leitura.Peek() > -1)

                'Para toda a linha atual para uma variável 
                strLinha = Leitura.ReadLine


                'IF PARA QUANDO A LINHA QUEBRAR FORA DO PADRÃO
                If strLinha <> "" Then
                    If Not strLinha.Substring(0, 1) = "|" Then

                        strLinhaAUX = strLinha

                        If strLinhaAnterior <> "" Then
                            strLinha = strLinhaAnterior + strLinhaAUX
                            strLinhaAnterior = ""
                        End If

                        'teste
                        If strLinha = "|338985;*1369200-201*;35;37222441;50;214;Edilaine;C;5;InManual;02/2021;12/01/2021 12:54:00;;AACI;Renato Mendes Ribeiro;;Renato Mendes Ribeiro - DN : 26/02/1938 ;FIZ A RETIRADA DE 50,00 PARA FEVEREIRO 2021 " Then
                            MsgBox("aqui")
                        End If



                        If nome_arq.Substring(0, 3) = "ROI" Then
                            rnRetorno.LeituraArquivoImportacao(strLinha, "Requisicao_oi_tb")
                        ElseIf nome_arq.Substring(0, 3) = "REQ" Then
                            rnRetorno.LeituraArquivoImportacao(strLinha, "Requisicao_tb")
                        ElseIf nome_arq.Substring(0, 3) = "LIS" Then
                            rnRetorno.LeituraArquivoImportacao(strLinha, "lista_telefonica_tb")
                        ElseIf nome_arq.Substring(0, 3) = "TEX" Then
                            rnRetorno.LeituraArquivoImportacao(strLinha, "telefone_exclusao")
                        End If

                    Else

                        strLinhaAnterior = strLinha

                        If nome_arq.Substring(0, 3) = "ROI" Then
                            rnRetorno.LeituraArquivoImportacao(strLinha, "Requisicao_oi_tb")
                        ElseIf nome_arq.Substring(0, 3) = "REQ" Then
                            rnRetorno.LeituraArquivoImportacao(strLinha, "Requisicao_tb")
                        ElseIf nome_arq.Substring(0, 3) = "LIS" Then
                            rnRetorno.LeituraArquivoImportacao(strLinha, "lista_telefonica_tb")
                        ElseIf nome_arq.Substring(0, 3) = "TEX" Then
                            rnRetorno.LeituraArquivoImportacao(strLinha, "telefone_exclusao")
                        End If
                    End If
                End If


            End While

            Leitura.Dispose()
            Leitura.Close()

            For Each reg As CRetorno In retornoList
                rnRetorno.GravaDadosArquivoRemessa(reg, nome_arq, id_arq, UF)
            Next

            MsgBox("Arquivo Processado com sucesso")

            Me.Cursor = Cursors.Default


            Dim arquivoADeletar As String = ""
            Leitura.Dispose()
            Leitura.Close()
            FileClose(1)
            Leitura.Dispose()
            Leitura.Close()
            FileClose(1)


        Catch ex As Exception
            ' Show the exception's message.
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub


    Private Sub btnArquivo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnArquivo.Click

        Dim AbrirComo As OpenFileDialog = New OpenFileDialog()
        Dim caminho As DialogResult


        AbrirComo.Title = "Abrir como"
        AbrirComo.InitialDirectory = "C:\DIGITAL"
        AbrirComo.FileName = "Nome Arquivo"
        AbrirComo.Filter = "Arquivos Textos (*.*)|*.*"
        caminho = AbrirComo.ShowDialog

        NomeArquivo = AbrirComo.SafeFileName

        AnalisaArquivoCarregado(NomeArquivo, txtArquivo.Text)

        If NomeArquivo = Nothing Then
            MessageBox.Show("Arquivo Inválido", "Arquivo", MessageBoxButtons.OK)
        Else
            txtArquivo.Text = AbrirComo.FileName
        End If
    End Sub

    Public Function ConverterCSV() As Boolean
        Dim dt As DataTable = New DataTable
        Dim rn As RNRequisicao = New RNRequisicao
        Try

            conv = New Conversor(NomeArquivo)

            dt = conv.ConverteCSVParaDataTable(True)

            If rn.ImportaParaRequisicaoOi(dt) Then
                MessageBox.Show("Arquivo importado com sucesso!!")
            Else
                MessageBox.Show("Ocorreu um erro ao importar o arquivo")
            End If


            Return True
        Catch ex As Exception

            MessageBox.Show(ex.Message)
            Return False
        End Try


    End Function

    Private Sub EnviaEmail()
        Try

            'create the mail message
            Dim SmtpServer As New SmtpClient()
            Dim mail As New MailMessage()
            SmtpServer.Credentials = New Net.NetworkCredential("aaci.jf.mg@gmail.com", "kwcmuyymqekemvtz")
            SmtpServer.Host = "smtp.gmail.com"
            SmtpServer.Port = 587 'server para gmail
            SmtpServer.EnableSsl = True
            mail.From = New MailAddress("aaci.jf.mg@gmail.com")
            mail.To.Add("aaci.jf.mg@gmail.com") 'Email que recebera a Mensagem
            mail.Subject = "Exportação Requisicao"
            mail.Body = ""

            'Este codigo PlainView e caso o email não suporte Html, nisto ele irá informar este erro

            Dim plainView As AlternateView = AlternateView.CreateAlternateViewFromString("Segue em anexo o arquivo de exportação.", Nothing, "text/plain")

            'crie um corpo de texto caso deseje assim você poderá colocar um texto no meio de duas imagens
            Dim linha1 As String = ""
            If Date.Now.Hour < 12 Then
                linha1 = "Bom dia,"
            Else
                linha1 = "Boa tarde,"
            End If

            Dim linha2 As String = "como solicitado, segue a planilha com as ligações efetuadas no periodo solicitado."
            Dim linha3 As String = "Atenciosamente."

            Dim htmlView As AlternateView = AlternateView.CreateAlternateViewFromString("<p> " + linha1 + "</p> <p>&nbsp;</p> <p>" + linha2 + "</p> <p>&nbsp;</p> <img src=cid:companylogo> <p>&nbsp;</p> <p>" + linha3 + "</p> <p><br /> <br /> --</p> <p>&nbsp;</p> <p>att..</p> <p>&nbsp;</p> <p><strong>AACI - ASSOCIA&Ccedil;&Atilde;O DE APOIO AS CRIAN&Ccedil;AS E IDOSOS</strong></p>   ", Nothing, "text/html")


            mail.Attachments.Add(New Attachment(NomeArquivo)) 'Adiciona o anexo a enviar

            With mail
                .AlternateViews.Add(plainView)
                .AlternateViews.Add(htmlView)

            End With

            mail.IsBodyHtml = True

            SmtpServer.Send(mail)
            MsgBox("E-mail enviado com sucesso", vbInformation, "Ok")





        Catch ex As Exception  'caso haja algum erro crie uma msg de aviso.
            MsgBox(ex)
        Finally
            '  File.Delete("")
        End Try
    End Sub

    Public Function AnalisaArquivoCarregado(ByVal NomeArquivo As String, ByVal CaminhoArquivo As String) As Boolean

        'Declara uma variável do tipo StreamWriter
        Dim Leitura As StreamReader
        Dim retornoList As List(Of CRetorno) = New List(Of CRetorno)
        Dim usuario As String = ""
        Dim nome_arq As String = ""
        Dim tipo_arq As String = ""
        Dim Contador As Integer = 0

        Try

            'Verifica se já existe um arquivo com este nome
            If File.Exists(CaminhoArquivo) = False Then
                MsgBox("Este arquivo não existe")
                Exit Function
            End If

            Me.Cursor = Cursors.WaitCursor

            'Cria um novo arquivo de acordo com o nome digitado e passa para o objeto StremWriter
            Leitura = File.OpenText(CaminhoArquivo)
            nome_arq = NomeArquivo

            If nome_arq.Substring(0, 3) = "ROI" Then
                tipo_arq = "Requisicao_oi_tb"
            ElseIf nome_arq.Substring(0, 3) = "REQ" Then
                tipo_arq = "Requisicao_tb"
            ElseIf nome_arq.Substring(0, 3) = "LIS" Then
                tipo_arq = "lista_telefonica_tb"
            ElseIf nome_arq.Substring(0, 3) = "TEX" Then
                tipo_arq = "telefone_exclusao_tb"
            End If

            'Declara as variáveis dos campos
            Dim strLinha As String

            'Faz um loop dentro das linhas do arquivo
            While (Leitura.Peek() > -1)

                'Para toda a linha atual para uma variável 
                strLinha = Leitura.ReadLine
                Contador = Contador + 1

            End While

            lstDados.Items.Add("O arquivo contem dados da tabela " + tipo_arq)
            lstDados.Items.Add("Numero de linhas do arquivo = " + Contador.ToString())


            Leitura.Dispose()
            Leitura.Close()

            Me.Cursor = Cursors.Default

            Dim arquivoADeletar As String = ""
            Leitura.Dispose()
            Leitura.Close()
            FileClose(1)
            Leitura.Dispose()
            Leitura.Close()
            FileClose(1)


        Catch ex As Exception
            ' Show the exception's message.
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False

        End Try

    End Function

    Private Sub chkReqOi_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkReqOi.CheckedChanged
        If chkReqOi.Checked = True Then
            chkRequisicao_tb.Checked = False
            chkLista.Checked = False
            chkTelefoneExclusao.Checked = False
        End If
    End Sub

    Private Sub chkRequisicao_tb_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRequisicao_tb.CheckedChanged
        If chkRequisicao_tb.Checked = True Then
            chkReqOi.Checked = False
            chkLista.Checked = False
            chkTelefoneExclusao.Checked = False
        End If
    End Sub

    Private Sub chkLista_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkLista.CheckedChanged
        If chkLista.Checked = True Then
            chkReqOi.Checked = False
            chkRequisicao_tb.Checked = False
            chkTelefoneExclusao.Checked = False
        End If
    End Sub

    Private Sub chkTelefoneExclusao_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTelefoneExclusao.CheckedChanged
        If chkTelefoneExclusao.Checked = True Then
            chkReqOi.Checked = False
            chkRequisicao_tb.Checked = False
            chkLista.Checked = False
        End If
    End Sub
End Class