﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmConsultaUltimaMovimentacao
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtDDDPesquisa = New System.Windows.Forms.TextBox()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.txtTelefonePesquisa = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtNome = New System.Windows.Forms.TextBox()
        Me.cboCategoria = New System.Windows.Forms.ComboBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.btnPesquisar = New System.Windows.Forms.Button()
        Me.cboNome = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.grdRemessa = New System.Windows.Forms.DataGridView()
        Me.btnSair = New System.Windows.Forms.Button()
        Me.btnRetorno = New System.Windows.Forms.Button()
        Me.btnExportar = New System.Windows.Forms.Button()
        Me.lblTotalFichas = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblValor = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TabInformacoes = New System.Windows.Forms.TabControl()
        Me.tabHistorico = New System.Windows.Forms.TabPage()
        Me.GrdHistorico = New System.Windows.Forms.DataGridView()
        Me.tbHistoricoOi = New System.Windows.Forms.TabPage()
        Me.gridHistoricoOi = New System.Windows.Forms.DataGridView()
        Me.tbInformacoes = New System.Windows.Forms.TabPage()
        Me.btnSalvar = New System.Windows.Forms.Button()
        Me.txtValDoacao = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txtMelhorDia = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.chkInativo = New System.Windows.Forms.CheckBox()
        Me.chkNaoPedirMensal = New System.Windows.Forms.CheckBox()
        Me.chkNaoPedirExtra = New System.Windows.Forms.CheckBox()
        Me.chkTelDesl = New System.Windows.Forms.CheckBox()
        Me.txtDtCadastro = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtObs = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtReferencia = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtDtNcMp = New System.Windows.Forms.TextBox()
        Me.txtDtNc = New System.Windows.Forms.TextBox()
        Me.txtRegiao = New System.Windows.Forms.TextBox()
        Me.txtCEP = New System.Windows.Forms.TextBox()
        Me.txtUF = New System.Windows.Forms.TextBox()
        Me.cboBairroDetalhe = New System.Windows.Forms.ComboBox()
        Me.txtDDD = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.cboCategoriaDetalhe = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cboEspecieDetalhe = New System.Windows.Forms.ComboBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.cboOperadorDetalhe = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.cboCidade = New System.Windows.Forms.ComboBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.txtTelefone2 = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtTelefone1 = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.txtRg = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.txtCnpjCpf = New System.Windows.Forms.TextBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.txtEndereco = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.txtNomeCliente2 = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.txtNomeCliente1 = New System.Windows.Forms.TextBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.txtCodBarra = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.tbConcluir = New System.Windows.Forms.TabPage()
        Me.Dados = New System.Windows.Forms.GroupBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.txtEmailContato = New System.Windows.Forms.TextBox()
        Me.txtWhatsApp = New System.Windows.Forms.TextBox()
        Me.txtValorTotal = New System.Windows.Forms.TextBox()
        Me.txtValorFicha = New System.Windows.Forms.TextBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.lblInformeValorTotal = New System.Windows.Forms.Label()
        Me.lblValorExtra = New System.Windows.Forms.Label()
        Me.txtValorExtra = New System.Windows.Forms.TextBox()
        Me.btnOutraOperadora = New System.Windows.Forms.Button()
        Me.btnLigar = New System.Windows.Forms.Button()
        Me.btnTelDesligado = New System.Windows.Forms.Button()
        Me.btnNaoAtendeu = New System.Windows.Forms.Button()
        Me.btnCancelarMensal = New System.Windows.Forms.Button()
        Me.chkNaoConforme = New System.Windows.Forms.CheckBox()
        Me.btnEnviarCancelar = New System.Windows.Forms.Button()
        Me.btnNaoContribuir = New System.Windows.Forms.Button()
        Me.btnContribuir = New System.Windows.Forms.Button()
        Me.rtxtObs = New System.Windows.Forms.RichTextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtCpf = New System.Windows.Forms.MaskedTextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtAutorizante = New System.Windows.Forms.TextBox()
        Me.cboCategoriaConcluir = New System.Windows.Forms.ComboBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txtMesRef = New System.Windows.Forms.DateTimePicker()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cboOperadorConcluir = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtCodEanConcluir = New System.Windows.Forms.MaskedTextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtNomeConcluir = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtTelefoneConcluir = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtValorConcluir = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.txtDDDConcluir = New System.Windows.Forms.TextBox()
        Me.DtMesAnoPesquisa = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.grdRemessa, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabInformacoes.SuspendLayout()
        Me.tabHistorico.SuspendLayout()
        CType(Me.GrdHistorico, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbHistoricoOi.SuspendLayout()
        CType(Me.gridHistoricoOi, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbInformacoes.SuspendLayout()
        Me.tbConcluir.SuspendLayout()
        Me.Dados.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.txtDDDPesquisa)
        Me.Panel1.Controls.Add(Me.Label47)
        Me.Panel1.Controls.Add(Me.txtTelefonePesquisa)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.txtNome)
        Me.Panel1.Controls.Add(Me.cboCategoria)
        Me.Panel1.Controls.Add(Me.Label26)
        Me.Panel1.Controls.Add(Me.btnPesquisar)
        Me.Panel1.Controls.Add(Me.cboNome)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1051, 53)
        Me.Panel1.TabIndex = 1
        '
        'txtDDDPesquisa
        '
        Me.txtDDDPesquisa.Location = New System.Drawing.Point(440, 13)
        Me.txtDDDPesquisa.Name = "txtDDDPesquisa"
        Me.txtDDDPesquisa.Size = New System.Drawing.Size(47, 20)
        Me.txtDDDPesquisa.TabIndex = 79
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.Location = New System.Drawing.Point(394, 14)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(40, 16)
        Me.Label47.TabIndex = 78
        Me.Label47.Text = "DDD:"
        '
        'txtTelefonePesquisa
        '
        Me.txtTelefonePesquisa.Location = New System.Drawing.Point(563, 13)
        Me.txtTelefonePesquisa.Name = "txtTelefonePesquisa"
        Me.txtTelefonePesquisa.Size = New System.Drawing.Size(116, 20)
        Me.txtTelefonePesquisa.TabIndex = 77
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(493, 15)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 16)
        Me.Label4.TabIndex = 76
        Me.Label4.Text = "Telefone:"
        '
        'txtNome
        '
        Me.txtNome.Location = New System.Drawing.Point(64, 14)
        Me.txtNome.Name = "txtNome"
        Me.txtNome.Size = New System.Drawing.Size(261, 20)
        Me.txtNome.TabIndex = 73
        '
        'cboCategoria
        '
        Me.cboCategoria.FormattingEnabled = True
        Me.cboCategoria.Location = New System.Drawing.Point(763, 13)
        Me.cboCategoria.Name = "cboCategoria"
        Me.cboCategoria.Size = New System.Drawing.Size(197, 21)
        Me.cboCategoria.TabIndex = 69
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(685, 15)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(72, 16)
        Me.Label26.TabIndex = 68
        Me.Label26.Text = "Categoria: "
        '
        'btnPesquisar
        '
        Me.btnPesquisar.Location = New System.Drawing.Point(966, 12)
        Me.btnPesquisar.Name = "btnPesquisar"
        Me.btnPesquisar.Size = New System.Drawing.Size(75, 23)
        Me.btnPesquisar.TabIndex = 67
        Me.btnPesquisar.Text = "Pesquisar"
        Me.btnPesquisar.UseVisualStyleBackColor = True
        '
        'cboNome
        '
        Me.cboNome.Enabled = False
        Me.cboNome.FormattingEnabled = True
        Me.cboNome.Location = New System.Drawing.Point(64, 14)
        Me.cboNome.Name = "cboNome"
        Me.cboNome.Size = New System.Drawing.Size(261, 21)
        Me.cboNome.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(14, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nome:"
        '
        'grdRemessa
        '
        Me.grdRemessa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdRemessa.Location = New System.Drawing.Point(12, 71)
        Me.grdRemessa.Name = "grdRemessa"
        Me.grdRemessa.Size = New System.Drawing.Size(1047, 296)
        Me.grdRemessa.TabIndex = 2
        '
        'btnSair
        '
        Me.btnSair.Location = New System.Drawing.Point(1073, 637)
        Me.btnSair.Name = "btnSair"
        Me.btnSair.Size = New System.Drawing.Size(75, 23)
        Me.btnSair.TabIndex = 3
        Me.btnSair.Text = "Sair"
        Me.btnSair.UseVisualStyleBackColor = True
        '
        'btnRetorno
        '
        Me.btnRetorno.Location = New System.Drawing.Point(1073, 588)
        Me.btnRetorno.Name = "btnRetorno"
        Me.btnRetorno.Size = New System.Drawing.Size(75, 23)
        Me.btnRetorno.TabIndex = 75
        Me.btnRetorno.Text = "Retorno"
        Me.btnRetorno.UseVisualStyleBackColor = True
        Me.btnRetorno.Visible = False
        '
        'btnExportar
        '
        Me.btnExportar.Location = New System.Drawing.Point(1073, 549)
        Me.btnExportar.Name = "btnExportar"
        Me.btnExportar.Size = New System.Drawing.Size(75, 23)
        Me.btnExportar.TabIndex = 76
        Me.btnExportar.Text = "Exportar"
        Me.btnExportar.UseVisualStyleBackColor = True
        Me.btnExportar.Visible = False
        '
        'lblTotalFichas
        '
        Me.lblTotalFichas.AutoSize = True
        Me.lblTotalFichas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalFichas.Location = New System.Drawing.Point(1077, 99)
        Me.lblTotalFichas.Name = "lblTotalFichas"
        Me.lblTotalFichas.Size = New System.Drawing.Size(15, 16)
        Me.lblTotalFichas.TabIndex = 80
        Me.lblTotalFichas.Text = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(1073, 71)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(119, 16)
        Me.Label2.TabIndex = 79
        Me.Label2.Text = "Total de Fichas:"
        '
        'lblValor
        '
        Me.lblValor.AutoSize = True
        Me.lblValor.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValor.Location = New System.Drawing.Point(1077, 168)
        Me.lblValor.Name = "lblValor"
        Me.lblValor.Size = New System.Drawing.Size(15, 16)
        Me.lblValor.TabIndex = 82
        Me.lblValor.Text = "0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(1077, 131)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(92, 16)
        Me.Label5.TabIndex = 81
        Me.Label5.Text = "Valor Total :"
        '
        'TabInformacoes
        '
        Me.TabInformacoes.Controls.Add(Me.tabHistorico)
        Me.TabInformacoes.Controls.Add(Me.tbHistoricoOi)
        Me.TabInformacoes.Controls.Add(Me.tbInformacoes)
        Me.TabInformacoes.Controls.Add(Me.tbConcluir)
        Me.TabInformacoes.Location = New System.Drawing.Point(12, 373)
        Me.TabInformacoes.Name = "TabInformacoes"
        Me.TabInformacoes.SelectedIndex = 0
        Me.TabInformacoes.Size = New System.Drawing.Size(1051, 333)
        Me.TabInformacoes.TabIndex = 83
        '
        'tabHistorico
        '
        Me.tabHistorico.Controls.Add(Me.GrdHistorico)
        Me.tabHistorico.Location = New System.Drawing.Point(4, 22)
        Me.tabHistorico.Name = "tabHistorico"
        Me.tabHistorico.Padding = New System.Windows.Forms.Padding(3)
        Me.tabHistorico.Size = New System.Drawing.Size(1043, 307)
        Me.tabHistorico.TabIndex = 0
        Me.tabHistorico.Text = "Histórico"
        Me.tabHistorico.UseVisualStyleBackColor = True
        '
        'GrdHistorico
        '
        Me.GrdHistorico.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GrdHistorico.Location = New System.Drawing.Point(6, 6)
        Me.GrdHistorico.Name = "GrdHistorico"
        Me.GrdHistorico.Size = New System.Drawing.Size(908, 295)
        Me.GrdHistorico.TabIndex = 3
        '
        'tbHistoricoOi
        '
        Me.tbHistoricoOi.Controls.Add(Me.gridHistoricoOi)
        Me.tbHistoricoOi.Location = New System.Drawing.Point(4, 22)
        Me.tbHistoricoOi.Name = "tbHistoricoOi"
        Me.tbHistoricoOi.Padding = New System.Windows.Forms.Padding(3)
        Me.tbHistoricoOi.Size = New System.Drawing.Size(1043, 307)
        Me.tbHistoricoOi.TabIndex = 3
        Me.tbHistoricoOi.Text = "Histórico Oi"
        Me.tbHistoricoOi.UseVisualStyleBackColor = True
        '
        'gridHistoricoOi
        '
        Me.gridHistoricoOi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridHistoricoOi.Location = New System.Drawing.Point(6, 5)
        Me.gridHistoricoOi.Name = "gridHistoricoOi"
        Me.gridHistoricoOi.Size = New System.Drawing.Size(908, 295)
        Me.gridHistoricoOi.TabIndex = 4
        '
        'tbInformacoes
        '
        Me.tbInformacoes.Controls.Add(Me.btnSalvar)
        Me.tbInformacoes.Controls.Add(Me.txtValDoacao)
        Me.tbInformacoes.Controls.Add(Me.Label29)
        Me.tbInformacoes.Controls.Add(Me.txtMelhorDia)
        Me.tbInformacoes.Controls.Add(Me.Label6)
        Me.tbInformacoes.Controls.Add(Me.chkInativo)
        Me.tbInformacoes.Controls.Add(Me.chkNaoPedirMensal)
        Me.tbInformacoes.Controls.Add(Me.chkNaoPedirExtra)
        Me.tbInformacoes.Controls.Add(Me.chkTelDesl)
        Me.tbInformacoes.Controls.Add(Me.txtDtCadastro)
        Me.tbInformacoes.Controls.Add(Me.Label30)
        Me.tbInformacoes.Controls.Add(Me.txtObs)
        Me.tbInformacoes.Controls.Add(Me.Label25)
        Me.tbInformacoes.Controls.Add(Me.txtReferencia)
        Me.tbInformacoes.Controls.Add(Me.Label24)
        Me.tbInformacoes.Controls.Add(Me.txtDtNcMp)
        Me.tbInformacoes.Controls.Add(Me.txtDtNc)
        Me.tbInformacoes.Controls.Add(Me.txtRegiao)
        Me.tbInformacoes.Controls.Add(Me.txtCEP)
        Me.tbInformacoes.Controls.Add(Me.txtUF)
        Me.tbInformacoes.Controls.Add(Me.cboBairroDetalhe)
        Me.tbInformacoes.Controls.Add(Me.txtDDD)
        Me.tbInformacoes.Controls.Add(Me.Label34)
        Me.tbInformacoes.Controls.Add(Me.cboCategoriaDetalhe)
        Me.tbInformacoes.Controls.Add(Me.Label7)
        Me.tbInformacoes.Controls.Add(Me.cboEspecieDetalhe)
        Me.tbInformacoes.Controls.Add(Me.Label27)
        Me.tbInformacoes.Controls.Add(Me.cboOperadorDetalhe)
        Me.tbInformacoes.Controls.Add(Me.Label8)
        Me.tbInformacoes.Controls.Add(Me.Label9)
        Me.tbInformacoes.Controls.Add(Me.Label10)
        Me.tbInformacoes.Controls.Add(Me.Label11)
        Me.tbInformacoes.Controls.Add(Me.Label15)
        Me.tbInformacoes.Controls.Add(Me.Label23)
        Me.tbInformacoes.Controls.Add(Me.Label31)
        Me.tbInformacoes.Controls.Add(Me.cboCidade)
        Me.tbInformacoes.Controls.Add(Me.Label32)
        Me.tbInformacoes.Controls.Add(Me.txtTelefone2)
        Me.tbInformacoes.Controls.Add(Me.Label33)
        Me.tbInformacoes.Controls.Add(Me.txtTelefone1)
        Me.tbInformacoes.Controls.Add(Me.Label35)
        Me.tbInformacoes.Controls.Add(Me.txtRg)
        Me.tbInformacoes.Controls.Add(Me.Label36)
        Me.tbInformacoes.Controls.Add(Me.txtCnpjCpf)
        Me.tbInformacoes.Controls.Add(Me.Label37)
        Me.tbInformacoes.Controls.Add(Me.txtEndereco)
        Me.tbInformacoes.Controls.Add(Me.Label38)
        Me.tbInformacoes.Controls.Add(Me.txtEmail)
        Me.tbInformacoes.Controls.Add(Me.Label39)
        Me.tbInformacoes.Controls.Add(Me.txtNomeCliente2)
        Me.tbInformacoes.Controls.Add(Me.Label40)
        Me.tbInformacoes.Controls.Add(Me.txtNomeCliente1)
        Me.tbInformacoes.Controls.Add(Me.Label41)
        Me.tbInformacoes.Controls.Add(Me.txtCodBarra)
        Me.tbInformacoes.Controls.Add(Me.Label42)
        Me.tbInformacoes.Controls.Add(Me.txtCodigo)
        Me.tbInformacoes.Controls.Add(Me.Label43)
        Me.tbInformacoes.Location = New System.Drawing.Point(4, 22)
        Me.tbInformacoes.Name = "tbInformacoes"
        Me.tbInformacoes.Padding = New System.Windows.Forms.Padding(3)
        Me.tbInformacoes.Size = New System.Drawing.Size(1043, 307)
        Me.tbInformacoes.TabIndex = 1
        Me.tbInformacoes.Text = "Informações"
        Me.tbInformacoes.UseVisualStyleBackColor = True
        '
        'btnSalvar
        '
        Me.btnSalvar.Location = New System.Drawing.Point(919, 278)
        Me.btnSalvar.Name = "btnSalvar"
        Me.btnSalvar.Size = New System.Drawing.Size(75, 23)
        Me.btnSalvar.TabIndex = 183
        Me.btnSalvar.Text = "Salvar"
        Me.btnSalvar.UseVisualStyleBackColor = True
        '
        'txtValDoacao
        '
        Me.txtValDoacao.Location = New System.Drawing.Point(842, 147)
        Me.txtValDoacao.Name = "txtValDoacao"
        Me.txtValDoacao.Size = New System.Drawing.Size(73, 20)
        Me.txtValDoacao.TabIndex = 182
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(839, 128)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(42, 16)
        Me.Label29.TabIndex = 181
        Me.Label29.Text = "Valor:"
        '
        'txtMelhorDia
        '
        Me.txtMelhorDia.Location = New System.Drawing.Point(750, 147)
        Me.txtMelhorDia.Name = "txtMelhorDia"
        Me.txtMelhorDia.Size = New System.Drawing.Size(73, 20)
        Me.txtMelhorDia.TabIndex = 180
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(747, 128)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(75, 16)
        Me.Label6.TabIndex = 179
        Me.Label6.Text = "Melhor Dia:"
        '
        'chkInativo
        '
        Me.chkInativo.AutoSize = True
        Me.chkInativo.Location = New System.Drawing.Point(787, 218)
        Me.chkInativo.Name = "chkInativo"
        Me.chkInativo.Size = New System.Drawing.Size(58, 17)
        Me.chkInativo.TabIndex = 178
        Me.chkInativo.Text = "Inativo"
        Me.chkInativo.UseVisualStyleBackColor = True
        '
        'chkNaoPedirMensal
        '
        Me.chkNaoPedirMensal.AutoSize = True
        Me.chkNaoPedirMensal.Location = New System.Drawing.Point(787, 264)
        Me.chkNaoPedirMensal.Name = "chkNaoPedirMensal"
        Me.chkNaoPedirMensal.Size = New System.Drawing.Size(109, 17)
        Me.chkNaoPedirMensal.TabIndex = 177
        Me.chkNaoPedirMensal.Text = "Não pedir Mensal"
        Me.chkNaoPedirMensal.UseVisualStyleBackColor = True
        '
        'chkNaoPedirExtra
        '
        Me.chkNaoPedirExtra.AutoSize = True
        Me.chkNaoPedirExtra.Location = New System.Drawing.Point(787, 240)
        Me.chkNaoPedirExtra.Name = "chkNaoPedirExtra"
        Me.chkNaoPedirExtra.Size = New System.Drawing.Size(98, 17)
        Me.chkNaoPedirExtra.TabIndex = 176
        Me.chkNaoPedirExtra.Text = "Não pedir extra"
        Me.chkNaoPedirExtra.UseVisualStyleBackColor = True
        '
        'chkTelDesl
        '
        Me.chkTelDesl.AutoSize = True
        Me.chkTelDesl.Location = New System.Drawing.Point(787, 287)
        Me.chkTelDesl.Name = "chkTelDesl"
        Me.chkTelDesl.Size = New System.Drawing.Size(91, 17)
        Me.chkTelDesl.TabIndex = 175
        Me.chkTelDesl.Text = "Tel Desligado"
        Me.chkTelDesl.UseVisualStyleBackColor = True
        '
        'txtDtCadastro
        '
        Me.txtDtCadastro.Location = New System.Drawing.Point(705, 190)
        Me.txtDtCadastro.Name = "txtDtCadastro"
        Me.txtDtCadastro.Size = New System.Drawing.Size(105, 20)
        Me.txtDtCadastro.TabIndex = 174
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(702, 171)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(97, 16)
        Me.Label30.TabIndex = 173
        Me.Label30.Text = "Data Cadastro:"
        '
        'txtObs
        '
        Me.txtObs.Location = New System.Drawing.Point(109, 257)
        Me.txtObs.Multiline = True
        Me.txtObs.Name = "txtObs"
        Me.txtObs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtObs.Size = New System.Drawing.Size(657, 44)
        Me.txtObs.TabIndex = 172
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(17, 257)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(85, 16)
        Me.Label25.TabIndex = 171
        Me.Label25.Text = "Observação:"
        '
        'txtReferencia
        '
        Me.txtReferencia.Location = New System.Drawing.Point(111, 222)
        Me.txtReferencia.Name = "txtReferencia"
        Me.txtReferencia.Size = New System.Drawing.Size(655, 20)
        Me.txtReferencia.TabIndex = 170
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(17, 223)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(76, 16)
        Me.Label24.TabIndex = 169
        Me.Label24.Text = "Referência:"
        '
        'txtDtNcMp
        '
        Me.txtDtNcMp.Location = New System.Drawing.Point(591, 191)
        Me.txtDtNcMp.Name = "txtDtNcMp"
        Me.txtDtNcMp.Size = New System.Drawing.Size(105, 20)
        Me.txtDtNcMp.TabIndex = 168
        '
        'txtDtNc
        '
        Me.txtDtNc.Location = New System.Drawing.Point(476, 191)
        Me.txtDtNc.Name = "txtDtNc"
        Me.txtDtNc.Size = New System.Drawing.Size(105, 20)
        Me.txtDtNc.TabIndex = 167
        '
        'txtRegiao
        '
        Me.txtRegiao.Location = New System.Drawing.Point(414, 191)
        Me.txtRegiao.Name = "txtRegiao"
        Me.txtRegiao.Size = New System.Drawing.Size(53, 20)
        Me.txtRegiao.TabIndex = 166
        '
        'txtCEP
        '
        Me.txtCEP.Location = New System.Drawing.Point(319, 191)
        Me.txtCEP.Name = "txtCEP"
        Me.txtCEP.Size = New System.Drawing.Size(88, 20)
        Me.txtCEP.TabIndex = 165
        '
        'txtUF
        '
        Me.txtUF.Location = New System.Drawing.Point(268, 191)
        Me.txtUF.Name = "txtUF"
        Me.txtUF.Size = New System.Drawing.Size(41, 20)
        Me.txtUF.TabIndex = 164
        '
        'cboBairroDetalhe
        '
        Me.cboBairroDetalhe.FormattingEnabled = True
        Me.cboBairroDetalhe.Location = New System.Drawing.Point(20, 191)
        Me.cboBairroDetalhe.Name = "cboBairroDetalhe"
        Me.cboBairroDetalhe.Size = New System.Drawing.Size(239, 21)
        Me.cboBairroDetalhe.TabIndex = 163
        '
        'txtDDD
        '
        Me.txtDDD.Location = New System.Drawing.Point(379, 105)
        Me.txtDDD.Name = "txtDDD"
        Me.txtDDD.Size = New System.Drawing.Size(56, 20)
        Me.txtDDD.TabIndex = 162
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(376, 86)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(40, 16)
        Me.Label34.TabIndex = 161
        Me.Label34.Text = "DDD:"
        '
        'cboCategoriaDetalhe
        '
        Me.cboCategoriaDetalhe.FormattingEnabled = True
        Me.cboCategoriaDetalhe.Location = New System.Drawing.Point(747, 63)
        Me.cboCategoriaDetalhe.Name = "cboCategoriaDetalhe"
        Me.cboCategoriaDetalhe.Size = New System.Drawing.Size(239, 21)
        Me.cboCategoriaDetalhe.TabIndex = 160
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(744, 44)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(69, 16)
        Me.Label7.TabIndex = 159
        Me.Label7.Text = "Categoria:"
        '
        'cboEspecieDetalhe
        '
        Me.cboEspecieDetalhe.FormattingEnabled = True
        Me.cboEspecieDetalhe.Location = New System.Drawing.Point(747, 104)
        Me.cboEspecieDetalhe.Name = "cboEspecieDetalhe"
        Me.cboEspecieDetalhe.Size = New System.Drawing.Size(239, 21)
        Me.cboEspecieDetalhe.TabIndex = 158
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(744, 85)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(60, 16)
        Me.Label27.TabIndex = 157
        Me.Label27.Text = "Espécie:"
        '
        'cboOperadorDetalhe
        '
        Me.cboOperadorDetalhe.FormattingEnabled = True
        Me.cboOperadorDetalhe.Location = New System.Drawing.Point(747, 20)
        Me.cboOperadorDetalhe.Name = "cboOperadorDetalhe"
        Me.cboOperadorDetalhe.Size = New System.Drawing.Size(239, 21)
        Me.cboOperadorDetalhe.TabIndex = 156
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(744, 2)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(68, 16)
        Me.Label8.TabIndex = 155
        Me.Label8.Text = "Operador:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(586, 170)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(84, 16)
        Me.Label9.TabIndex = 154
        Me.Label9.Text = "Data NC MP:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(471, 170)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(61, 16)
        Me.Label10.TabIndex = 153
        Me.Label10.Text = "Data NC:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(409, 170)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(55, 16)
        Me.Label11.TabIndex = 152
        Me.Label11.Text = "Região:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(314, 170)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(37, 16)
        Me.Label15.TabIndex = 151
        Me.Label15.Text = "CEP:"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(263, 170)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(28, 16)
        Me.Label23.TabIndex = 150
        Me.Label23.Text = "UF:"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(15, 170)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(46, 16)
        Me.Label31.TabIndex = 149
        Me.Label31.Text = "Bairro:"
        '
        'cboCidade
        '
        Me.cboCidade.FormattingEnabled = True
        Me.cboCidade.Location = New System.Drawing.Point(592, 147)
        Me.cboCidade.Name = "cboCidade"
        Me.cboCidade.Size = New System.Drawing.Size(145, 21)
        Me.cboCidade.TabIndex = 148
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(589, 128)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(54, 16)
        Me.Label32.TabIndex = 147
        Me.Label32.Text = "Cidade:"
        '
        'txtTelefone2
        '
        Me.txtTelefone2.Location = New System.Drawing.Point(592, 105)
        Me.txtTelefone2.Name = "txtTelefone2"
        Me.txtTelefone2.Size = New System.Drawing.Size(145, 20)
        Me.txtTelefone2.TabIndex = 146
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(589, 86)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(71, 16)
        Me.Label33.TabIndex = 145
        Me.Label33.Text = "Telefone2:"
        '
        'txtTelefone1
        '
        Me.txtTelefone1.Location = New System.Drawing.Point(441, 105)
        Me.txtTelefone1.Name = "txtTelefone1"
        Me.txtTelefone1.Size = New System.Drawing.Size(140, 20)
        Me.txtTelefone1.TabIndex = 144
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(438, 86)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(71, 16)
        Me.Label35.TabIndex = 143
        Me.Label35.Text = "Telefone1:"
        '
        'txtRg
        '
        Me.txtRg.Location = New System.Drawing.Point(592, 63)
        Me.txtRg.Name = "txtRg"
        Me.txtRg.Size = New System.Drawing.Size(145, 20)
        Me.txtRg.TabIndex = 142
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.Location = New System.Drawing.Point(589, 44)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(89, 16)
        Me.Label36.TabIndex = 141
        Me.Label36.Text = "Insc. Est./ RG:"
        '
        'txtCnpjCpf
        '
        Me.txtCnpjCpf.Location = New System.Drawing.Point(592, 21)
        Me.txtCnpjCpf.Name = "txtCnpjCpf"
        Me.txtCnpjCpf.Size = New System.Drawing.Size(145, 20)
        Me.txtCnpjCpf.TabIndex = 140
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.Location = New System.Drawing.Point(589, 2)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(75, 16)
        Me.Label37.TabIndex = 139
        Me.Label37.Text = "CNPJ/CPF:"
        '
        'txtEndereco
        '
        Me.txtEndereco.Location = New System.Drawing.Point(18, 147)
        Me.txtEndereco.Name = "txtEndereco"
        Me.txtEndereco.Size = New System.Drawing.Size(563, 20)
        Me.txtEndereco.TabIndex = 138
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.Location = New System.Drawing.Point(15, 128)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(69, 16)
        Me.Label38.TabIndex = 137
        Me.Label38.Text = "Endereço:"
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(18, 105)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(352, 20)
        Me.txtEmail.TabIndex = 136
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.Location = New System.Drawing.Point(15, 86)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(48, 16)
        Me.Label39.TabIndex = 135
        Me.Label39.Text = "E-mail:"
        '
        'txtNomeCliente2
        '
        Me.txtNomeCliente2.Location = New System.Drawing.Point(164, 63)
        Me.txtNomeCliente2.Name = "txtNomeCliente2"
        Me.txtNomeCliente2.Size = New System.Drawing.Size(417, 20)
        Me.txtNomeCliente2.TabIndex = 134
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.Location = New System.Drawing.Point(161, 44)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(57, 16)
        Me.Label40.TabIndex = 133
        Me.Label40.Text = "Nome 2:"
        '
        'txtNomeCliente1
        '
        Me.txtNomeCliente1.Location = New System.Drawing.Point(164, 21)
        Me.txtNomeCliente1.Name = "txtNomeCliente1"
        Me.txtNomeCliente1.Size = New System.Drawing.Size(417, 20)
        Me.txtNomeCliente1.TabIndex = 132
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.Location = New System.Drawing.Point(161, 2)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(47, 16)
        Me.Label41.TabIndex = 131
        Me.Label41.Text = "Nome:"
        '
        'txtCodBarra
        '
        Me.txtCodBarra.Location = New System.Drawing.Point(18, 63)
        Me.txtCodBarra.Name = "txtCodBarra"
        Me.txtCodBarra.Size = New System.Drawing.Size(136, 20)
        Me.txtCodBarra.TabIndex = 130
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Location = New System.Drawing.Point(15, 44)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(74, 16)
        Me.Label42.TabIndex = 129
        Me.Label42.Text = "Cód. Barra:"
        '
        'txtCodigo
        '
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.Location = New System.Drawing.Point(18, 21)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(136, 20)
        Me.txtCodigo.TabIndex = 128
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(15, 2)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(54, 16)
        Me.Label43.TabIndex = 127
        Me.Label43.Text = "Código:"
        '
        'tbConcluir
        '
        Me.tbConcluir.Controls.Add(Me.Dados)
        Me.tbConcluir.Location = New System.Drawing.Point(4, 22)
        Me.tbConcluir.Name = "tbConcluir"
        Me.tbConcluir.Padding = New System.Windows.Forms.Padding(3)
        Me.tbConcluir.Size = New System.Drawing.Size(1043, 307)
        Me.tbConcluir.TabIndex = 2
        Me.tbConcluir.Text = "Concluir"
        Me.tbConcluir.UseVisualStyleBackColor = True
        '
        'Dados
        '
        Me.Dados.Controls.Add(Me.GroupBox1)
        Me.Dados.Controls.Add(Me.txtValorTotal)
        Me.Dados.Controls.Add(Me.txtValorFicha)
        Me.Dados.Controls.Add(Me.Label44)
        Me.Dados.Controls.Add(Me.lblInformeValorTotal)
        Me.Dados.Controls.Add(Me.lblValorExtra)
        Me.Dados.Controls.Add(Me.txtValorExtra)
        Me.Dados.Controls.Add(Me.btnOutraOperadora)
        Me.Dados.Controls.Add(Me.btnLigar)
        Me.Dados.Controls.Add(Me.btnTelDesligado)
        Me.Dados.Controls.Add(Me.btnNaoAtendeu)
        Me.Dados.Controls.Add(Me.btnCancelarMensal)
        Me.Dados.Controls.Add(Me.chkNaoConforme)
        Me.Dados.Controls.Add(Me.btnEnviarCancelar)
        Me.Dados.Controls.Add(Me.btnNaoContribuir)
        Me.Dados.Controls.Add(Me.btnContribuir)
        Me.Dados.Controls.Add(Me.rtxtObs)
        Me.Dados.Controls.Add(Me.Label20)
        Me.Dados.Controls.Add(Me.txtCpf)
        Me.Dados.Controls.Add(Me.Label22)
        Me.Dados.Controls.Add(Me.Label21)
        Me.Dados.Controls.Add(Me.txtAutorizante)
        Me.Dados.Controls.Add(Me.cboCategoriaConcluir)
        Me.Dados.Controls.Add(Me.Label28)
        Me.Dados.Controls.Add(Me.txtMesRef)
        Me.Dados.Controls.Add(Me.Label12)
        Me.Dados.Controls.Add(Me.cboOperadorConcluir)
        Me.Dados.Controls.Add(Me.Label13)
        Me.Dados.Controls.Add(Me.txtCodEanConcluir)
        Me.Dados.Controls.Add(Me.Label14)
        Me.Dados.Controls.Add(Me.txtNomeConcluir)
        Me.Dados.Controls.Add(Me.Label16)
        Me.Dados.Controls.Add(Me.txtTelefoneConcluir)
        Me.Dados.Controls.Add(Me.Label17)
        Me.Dados.Controls.Add(Me.Label18)
        Me.Dados.Controls.Add(Me.txtValorConcluir)
        Me.Dados.Controls.Add(Me.Label19)
        Me.Dados.Controls.Add(Me.txtDDDConcluir)
        Me.Dados.Location = New System.Drawing.Point(6, 0)
        Me.Dados.Name = "Dados"
        Me.Dados.Size = New System.Drawing.Size(1034, 301)
        Me.Dados.TabIndex = 8
        Me.Dados.TabStop = False
        Me.Dados.Text = "Dados"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label45)
        Me.GroupBox1.Controls.Add(Me.Label46)
        Me.GroupBox1.Controls.Add(Me.txtEmailContato)
        Me.GroupBox1.Controls.Add(Me.txtWhatsApp)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.Blue
        Me.GroupBox1.Location = New System.Drawing.Point(582, 143)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(320, 85)
        Me.GroupBox1.TabIndex = 150
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Contatos"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.Location = New System.Drawing.Point(21, 20)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(42, 13)
        Me.Label45.TabIndex = 147
        Me.Label45.Text = "E-Mail"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.Location = New System.Drawing.Point(2, 49)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(65, 13)
        Me.Label46.TabIndex = 149
        Me.Label46.Text = "WhatsApp"
        '
        'txtEmailContato
        '
        Me.txtEmailContato.Location = New System.Drawing.Point(69, 14)
        Me.txtEmailContato.Name = "txtEmailContato"
        Me.txtEmailContato.Size = New System.Drawing.Size(235, 20)
        Me.txtEmailContato.TabIndex = 146
        '
        'txtWhatsApp
        '
        Me.txtWhatsApp.Location = New System.Drawing.Point(69, 46)
        Me.txtWhatsApp.Name = "txtWhatsApp"
        Me.txtWhatsApp.Size = New System.Drawing.Size(170, 20)
        Me.txtWhatsApp.TabIndex = 148
        '
        'txtValorTotal
        '
        Me.txtValorTotal.Enabled = False
        Me.txtValorTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtValorTotal.Location = New System.Drawing.Point(400, 166)
        Me.txtValorTotal.Name = "txtValorTotal"
        Me.txtValorTotal.Size = New System.Drawing.Size(86, 20)
        Me.txtValorTotal.TabIndex = 145
        '
        'txtValorFicha
        '
        Me.txtValorFicha.Location = New System.Drawing.Point(368, 235)
        Me.txtValorFicha.Name = "txtValorFicha"
        Me.txtValorFicha.Size = New System.Drawing.Size(69, 20)
        Me.txtValorFicha.TabIndex = 144
        Me.txtValorFicha.Visible = False
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.Location = New System.Drawing.Point(272, 238)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(101, 13)
        Me.Label44.TabIndex = 143
        Me.Label44.Text = "Valor da Ficha : "
        Me.Label44.Visible = False
        '
        'lblInformeValorTotal
        '
        Me.lblInformeValorTotal.AutoSize = True
        Me.lblInformeValorTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblInformeValorTotal.ForeColor = System.Drawing.Color.Red
        Me.lblInformeValorTotal.Location = New System.Drawing.Point(318, 167)
        Me.lblInformeValorTotal.Name = "lblInformeValorTotal"
        Me.lblInformeValorTotal.Size = New System.Drawing.Size(76, 15)
        Me.lblInformeValorTotal.TabIndex = 142
        Me.lblInformeValorTotal.Text = "Valor Total"
        '
        'lblValorExtra
        '
        Me.lblValorExtra.AutoSize = True
        Me.lblValorExtra.Location = New System.Drawing.Point(177, 169)
        Me.lblValorExtra.Name = "lblValorExtra"
        Me.lblValorExtra.Size = New System.Drawing.Size(58, 13)
        Me.lblValorExtra.TabIndex = 141
        Me.lblValorExtra.Text = "Valor Extra"
        Me.lblValorExtra.Visible = False
        '
        'txtValorExtra
        '
        Me.txtValorExtra.Location = New System.Drawing.Point(239, 166)
        Me.txtValorExtra.Name = "txtValorExtra"
        Me.txtValorExtra.Size = New System.Drawing.Size(69, 20)
        Me.txtValorExtra.TabIndex = 140
        Me.txtValorExtra.Visible = False
        '
        'btnOutraOperadora
        '
        Me.btnOutraOperadora.Location = New System.Drawing.Point(808, 235)
        Me.btnOutraOperadora.Name = "btnOutraOperadora"
        Me.btnOutraOperadora.Size = New System.Drawing.Size(94, 50)
        Me.btnOutraOperadora.TabIndex = 139
        Me.btnOutraOperadora.Text = "Outra Operadora Telefonica"
        Me.btnOutraOperadora.UseVisualStyleBackColor = True
        '
        'btnLigar
        '
        Me.btnLigar.Location = New System.Drawing.Point(515, 235)
        Me.btnLigar.Name = "btnLigar"
        Me.btnLigar.Size = New System.Drawing.Size(75, 49)
        Me.btnLigar.TabIndex = 138
        Me.btnLigar.Text = "Ligar"
        Me.btnLigar.UseVisualStyleBackColor = True
        Me.btnLigar.Visible = False
        '
        'btnTelDesligado
        '
        Me.btnTelDesligado.Location = New System.Drawing.Point(922, 235)
        Me.btnTelDesligado.Name = "btnTelDesligado"
        Me.btnTelDesligado.Size = New System.Drawing.Size(100, 49)
        Me.btnTelDesligado.TabIndex = 137
        Me.btnTelDesligado.Text = "Telefone Desligado"
        Me.btnTelDesligado.UseVisualStyleBackColor = True
        '
        'btnNaoAtendeu
        '
        Me.btnNaoAtendeu.Location = New System.Drawing.Point(922, 170)
        Me.btnNaoAtendeu.Name = "btnNaoAtendeu"
        Me.btnNaoAtendeu.Size = New System.Drawing.Size(100, 49)
        Me.btnNaoAtendeu.TabIndex = 136
        Me.btnNaoAtendeu.Text = "Próx. Dia"
        Me.btnNaoAtendeu.UseVisualStyleBackColor = True
        '
        'btnCancelarMensal
        '
        Me.btnCancelarMensal.Location = New System.Drawing.Point(596, 234)
        Me.btnCancelarMensal.Name = "btnCancelarMensal"
        Me.btnCancelarMensal.Size = New System.Drawing.Size(94, 50)
        Me.btnCancelarMensal.TabIndex = 135
        Me.btnCancelarMensal.Text = "Cancelar Mensal"
        Me.btnCancelarMensal.UseVisualStyleBackColor = True
        Me.btnCancelarMensal.Visible = False
        '
        'chkNaoConforme
        '
        Me.chkNaoConforme.AutoSize = True
        Me.chkNaoConforme.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNaoConforme.Location = New System.Drawing.Point(91, 274)
        Me.chkNaoConforme.Name = "chkNaoConforme"
        Me.chkNaoConforme.Size = New System.Drawing.Size(221, 17)
        Me.chkNaoConforme.TabIndex = 134
        Me.chkNaoConforme.Text = "Grau de parentesco Não conforme"
        Me.chkNaoConforme.UseVisualStyleBackColor = True
        '
        'btnEnviarCancelar
        '
        Me.btnEnviarCancelar.Location = New System.Drawing.Point(696, 234)
        Me.btnEnviarCancelar.Name = "btnEnviarCancelar"
        Me.btnEnviarCancelar.Size = New System.Drawing.Size(106, 50)
        Me.btnEnviarCancelar.TabIndex = 133
        Me.btnEnviarCancelar.Text = "Enviar para cancelamento"
        Me.btnEnviarCancelar.UseVisualStyleBackColor = True
        Me.btnEnviarCancelar.Visible = False
        '
        'btnNaoContribuir
        '
        Me.btnNaoContribuir.Location = New System.Drawing.Point(922, 106)
        Me.btnNaoContribuir.Name = "btnNaoContribuir"
        Me.btnNaoContribuir.Size = New System.Drawing.Size(100, 49)
        Me.btnNaoContribuir.TabIndex = 85
        Me.btnNaoContribuir.Text = "Não Contribuir"
        Me.btnNaoContribuir.UseVisualStyleBackColor = True
        '
        'btnContribuir
        '
        Me.btnContribuir.Location = New System.Drawing.Point(922, 41)
        Me.btnContribuir.Name = "btnContribuir"
        Me.btnContribuir.Size = New System.Drawing.Size(100, 49)
        Me.btnContribuir.TabIndex = 84
        Me.btnContribuir.Text = "Contribuir"
        Me.btnContribuir.UseVisualStyleBackColor = True
        '
        'rtxtObs
        '
        Me.rtxtObs.Location = New System.Drawing.Point(582, 42)
        Me.rtxtObs.Name = "rtxtObs"
        Me.rtxtObs.Size = New System.Drawing.Size(320, 95)
        Me.rtxtObs.TabIndex = 83
        Me.rtxtObs.Text = ""
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(579, 18)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(91, 16)
        Me.Label20.TabIndex = 82
        Me.Label20.Text = "Observação : "
        '
        'txtCpf
        '
        Me.txtCpf.Location = New System.Drawing.Point(91, 235)
        Me.txtCpf.Name = "txtCpf"
        Me.txtCpf.Size = New System.Drawing.Size(143, 20)
        Me.txtCpf.TabIndex = 81
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(52, 238)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(36, 13)
        Me.Label22.TabIndex = 80
        Me.Label22.Text = "CPF : "
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(36, 202)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(42, 13)
        Me.Label21.TabIndex = 79
        Me.Label21.Text = "Titular :"
        '
        'txtAutorizante
        '
        Me.txtAutorizante.Location = New System.Drawing.Point(92, 200)
        Me.txtAutorizante.Name = "txtAutorizante"
        Me.txtAutorizante.Size = New System.Drawing.Size(461, 20)
        Me.txtAutorizante.TabIndex = 78
        '
        'cboCategoriaConcluir
        '
        Me.cboCategoriaConcluir.Enabled = False
        Me.cboCategoriaConcluir.FormattingEnabled = True
        Me.cboCategoriaConcluir.Location = New System.Drawing.Point(93, 131)
        Me.cboCategoriaConcluir.Name = "cboCategoriaConcluir"
        Me.cboCategoriaConcluir.Size = New System.Drawing.Size(181, 21)
        Me.cboCategoriaConcluir.TabIndex = 5
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(3, 135)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(69, 16)
        Me.Label28.TabIndex = 74
        Me.Label28.Text = "Categoria:"
        '
        'txtMesRef
        '
        Me.txtMesRef.CustomFormat = "MM/yyyy"
        Me.txtMesRef.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txtMesRef.Location = New System.Drawing.Point(400, 132)
        Me.txtMesRef.Name = "txtMesRef"
        Me.txtMesRef.Size = New System.Drawing.Size(92, 20)
        Me.txtMesRef.TabIndex = 7
        Me.txtMesRef.Value = New Date(2016, 12, 1, 0, 0, 0, 0)
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(311, 134)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(82, 13)
        Me.Label12.TabIndex = 60
        Me.Label12.Text = "Mês Referência"
        '
        'cboOperadorConcluir
        '
        Me.cboOperadorConcluir.FormattingEnabled = True
        Me.cboOperadorConcluir.Location = New System.Drawing.Point(321, 19)
        Me.cboOperadorConcluir.Name = "cboOperadorConcluir"
        Me.cboOperadorConcluir.Size = New System.Drawing.Size(239, 21)
        Me.cboOperadorConcluir.TabIndex = 1
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(238, 20)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(76, 16)
        Me.Label13.TabIndex = 57
        Me.Label13.Text = "Operadora:"
        '
        'txtCodEanConcluir
        '
        Me.txtCodEanConcluir.Location = New System.Drawing.Point(93, 19)
        Me.txtCodEanConcluir.Name = "txtCodEanConcluir"
        Me.txtCodEanConcluir.Size = New System.Drawing.Size(141, 20)
        Me.txtCodEanConcluir.TabIndex = 0
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(43, 59)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(38, 13)
        Me.Label14.TabIndex = 10
        Me.Label14.Text = "Nome "
        '
        'txtNomeConcluir
        '
        Me.txtNomeConcluir.Location = New System.Drawing.Point(93, 56)
        Me.txtNomeConcluir.Name = "txtNomeConcluir"
        Me.txtNomeConcluir.Size = New System.Drawing.Size(375, 20)
        Me.txtNomeConcluir.TabIndex = 2
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(133, 100)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(49, 13)
        Me.Label16.TabIndex = 8
        Me.Label16.Text = "Telefone"
        '
        'txtTelefoneConcluir
        '
        Me.txtTelefoneConcluir.Location = New System.Drawing.Point(182, 97)
        Me.txtTelefoneConcluir.Name = "txtTelefoneConcluir"
        Me.txtTelefoneConcluir.Size = New System.Drawing.Size(113, 20)
        Me.txtTelefoneConcluir.TabIndex = 4
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(5, 22)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(65, 13)
        Me.Label17.TabIndex = 2
        Me.Label17.Text = "Código EAN"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(47, 169)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(31, 13)
        Me.Label18.TabIndex = 6
        Me.Label18.Text = "Valor"
        '
        'txtValorConcluir
        '
        Me.txtValorConcluir.Location = New System.Drawing.Point(92, 166)
        Me.txtValorConcluir.Name = "txtValorConcluir"
        Me.txtValorConcluir.Size = New System.Drawing.Size(69, 20)
        Me.txtValorConcluir.TabIndex = 6
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(48, 100)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(31, 13)
        Me.Label19.TabIndex = 4
        Me.Label19.Text = "DDD"
        '
        'txtDDDConcluir
        '
        Me.txtDDDConcluir.Location = New System.Drawing.Point(93, 97)
        Me.txtDDDConcluir.Name = "txtDDDConcluir"
        Me.txtDDDConcluir.Size = New System.Drawing.Size(34, 20)
        Me.txtDDDConcluir.TabIndex = 3
        '
        'DtMesAnoPesquisa
        '
        Me.DtMesAnoPesquisa.CustomFormat = "MM/yyyy"
        Me.DtMesAnoPesquisa.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DtMesAnoPesquisa.Location = New System.Drawing.Point(1071, 482)
        Me.DtMesAnoPesquisa.Name = "DtMesAnoPesquisa"
        Me.DtMesAnoPesquisa.Size = New System.Drawing.Size(113, 20)
        Me.DtMesAnoPesquisa.TabIndex = 85
        Me.DtMesAnoPesquisa.Value = New Date(2016, 12, 1, 0, 0, 0, 0)
        Me.DtMesAnoPesquisa.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(1069, 463)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(90, 16)
        Me.Label3.TabIndex = 84
        Me.Label3.Text = "Ano Mês Ref.:"
        Me.Label3.Visible = False
        '
        'FrmConsultaUltimaMovimentacao
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1198, 708)
        Me.Controls.Add(Me.DtMesAnoPesquisa)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TabInformacoes)
        Me.Controls.Add(Me.lblValor)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblTotalFichas)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnExportar)
        Me.Controls.Add(Me.btnRetorno)
        Me.Controls.Add(Me.btnSair)
        Me.Controls.Add(Me.grdRemessa)
        Me.Controls.Add(Me.Panel1)
        Me.MinimizeBox = False
        Me.Name = "FrmConsultaUltimaMovimentacao"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Consulta Requisição"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.grdRemessa, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabInformacoes.ResumeLayout(False)
        Me.tabHistorico.ResumeLayout(False)
        CType(Me.GrdHistorico, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbHistoricoOi.ResumeLayout(False)
        CType(Me.gridHistoricoOi, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbInformacoes.ResumeLayout(False)
        Me.tbInformacoes.PerformLayout()
        Me.tbConcluir.ResumeLayout(False)
        Me.Dados.ResumeLayout(False)
        Me.Dados.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnPesquisar As System.Windows.Forms.Button
    Friend WithEvents cboNome As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboCategoria As System.Windows.Forms.ComboBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents grdRemessa As System.Windows.Forms.DataGridView
    Friend WithEvents btnSair As System.Windows.Forms.Button
    Friend WithEvents btnRetorno As System.Windows.Forms.Button
    Friend WithEvents btnExportar As System.Windows.Forms.Button
    Friend WithEvents lblTotalFichas As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblValor As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtNome As System.Windows.Forms.TextBox
    Friend WithEvents txtTelefonePesquisa As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TabInformacoes As System.Windows.Forms.TabControl
    Friend WithEvents tabHistorico As System.Windows.Forms.TabPage
    Friend WithEvents GrdHistorico As System.Windows.Forms.DataGridView
    Friend WithEvents tbInformacoes As System.Windows.Forms.TabPage
    Friend WithEvents DtMesAnoPesquisa As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tbConcluir As System.Windows.Forms.TabPage
    Friend WithEvents Dados As System.Windows.Forms.GroupBox
    Friend WithEvents cboCategoriaConcluir As System.Windows.Forms.ComboBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents txtMesRef As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cboOperadorConcluir As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents txtCodEanConcluir As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtNomeConcluir As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtTelefoneConcluir As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtValorConcluir As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents txtDDDConcluir As System.Windows.Forms.TextBox
    Friend WithEvents rtxtObs As System.Windows.Forms.RichTextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtCpf As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtAutorizante As System.Windows.Forms.TextBox
    Friend WithEvents btnSalvar As System.Windows.Forms.Button
    Friend WithEvents txtValDoacao As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents txtMelhorDia As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents chkInativo As System.Windows.Forms.CheckBox
    Friend WithEvents chkNaoPedirMensal As System.Windows.Forms.CheckBox
    Friend WithEvents chkNaoPedirExtra As System.Windows.Forms.CheckBox
    Friend WithEvents chkTelDesl As System.Windows.Forms.CheckBox
    Friend WithEvents txtDtCadastro As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents txtObs As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txtReferencia As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtDtNcMp As System.Windows.Forms.TextBox
    Friend WithEvents txtDtNc As System.Windows.Forms.TextBox
    Friend WithEvents txtRegiao As System.Windows.Forms.TextBox
    Friend WithEvents txtCEP As System.Windows.Forms.TextBox
    Friend WithEvents txtUF As System.Windows.Forms.TextBox
    Friend WithEvents cboBairroDetalhe As System.Windows.Forms.ComboBox
    Friend WithEvents txtDDD As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents cboCategoriaDetalhe As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cboEspecieDetalhe As System.Windows.Forms.ComboBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents cboOperadorDetalhe As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents cboCidade As System.Windows.Forms.ComboBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents txtTelefone2 As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents txtTelefone1 As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents txtRg As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtCnpjCpf As System.Windows.Forms.TextBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents txtEndereco As System.Windows.Forms.TextBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents txtNomeCliente2 As System.Windows.Forms.TextBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents txtNomeCliente1 As System.Windows.Forms.TextBox
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents txtCodBarra As System.Windows.Forms.TextBox
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents btnNaoContribuir As System.Windows.Forms.Button
    Friend WithEvents btnContribuir As System.Windows.Forms.Button
    Friend WithEvents btnEnviarCancelar As System.Windows.Forms.Button
    Friend WithEvents chkNaoConforme As System.Windows.Forms.CheckBox
    Friend WithEvents btnCancelarMensal As System.Windows.Forms.Button
    Friend WithEvents tbHistoricoOi As System.Windows.Forms.TabPage
    Friend WithEvents gridHistoricoOi As System.Windows.Forms.DataGridView
    Friend WithEvents btnNaoAtendeu As System.Windows.Forms.Button
    Friend WithEvents btnTelDesligado As System.Windows.Forms.Button
    Friend WithEvents btnLigar As System.Windows.Forms.Button
    Friend WithEvents btnOutraOperadora As System.Windows.Forms.Button
    Friend WithEvents lblValorExtra As System.Windows.Forms.Label
    Friend WithEvents txtValorExtra As System.Windows.Forms.TextBox
    Friend WithEvents lblInformeValorTotal As System.Windows.Forms.Label
    Friend WithEvents txtValorFicha As System.Windows.Forms.TextBox
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents txtValorTotal As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents txtEmailContato As System.Windows.Forms.TextBox
    Friend WithEvents txtWhatsApp As System.Windows.Forms.TextBox
    Friend WithEvents txtDDDPesquisa As TextBox
    Friend WithEvents Label47 As Label
End Class
