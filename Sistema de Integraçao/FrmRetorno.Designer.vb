﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRetorno
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtCaminho = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnPesquisar = New System.Windows.Forms.Button()
        Me.gridRetorno = New System.Windows.Forms.DataGridView()
        Me.btnProcessar = New System.Windows.Forms.Button()
        Me.OpenFileDialog = New System.Windows.Forms.OpenFileDialog()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.btnSair = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblContestado = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lblAltConta = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblArrecadado = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.lblCriticado = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lblFaturado = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblAfaturar = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblStatusArquivo = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblArquivo = New System.Windows.Forms.Label()
        Me.lblDataArquivo = New System.Windows.Forms.Label()
        Me.lblTipoArq = New System.Windows.Forms.Label()
        Me.lblTotalReg = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblTotalArquivo = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.DtMesAnoPesquisa = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.chkProcessarTodos = New System.Windows.Forms.CheckBox()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.lblArquivosLote = New System.Windows.Forms.Label()
        Me.ProgressBar = New System.Windows.Forms.ProgressBar()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnCarregar = New System.Windows.Forms.Button()
        Me.lblArrecadadoAtual = New System.Windows.Forms.Label()
        CType(Me.gridRetorno, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtCaminho
        '
        Me.txtCaminho.Location = New System.Drawing.Point(80, 9)
        Me.txtCaminho.Name = "txtCaminho"
        Me.txtCaminho.Size = New System.Drawing.Size(758, 20)
        Me.txtCaminho.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(28, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Arquivo:"
        '
        'btnPesquisar
        '
        Me.btnPesquisar.Location = New System.Drawing.Point(1132, 9)
        Me.btnPesquisar.Name = "btnPesquisar"
        Me.btnPesquisar.Size = New System.Drawing.Size(133, 23)
        Me.btnPesquisar.TabIndex = 2
        Me.btnPesquisar.Text = "Pesquisar"
        Me.btnPesquisar.UseVisualStyleBackColor = True
        '
        'gridRetorno
        '
        Me.gridRetorno.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridRetorno.Location = New System.Drawing.Point(31, 46)
        Me.gridRetorno.Name = "gridRetorno"
        Me.gridRetorno.Size = New System.Drawing.Size(868, 570)
        Me.gridRetorno.TabIndex = 3
        '
        'btnProcessar
        '
        Me.btnProcessar.Location = New System.Drawing.Point(895, 9)
        Me.btnProcessar.Name = "btnProcessar"
        Me.btnProcessar.Size = New System.Drawing.Size(127, 23)
        Me.btnProcessar.TabIndex = 4
        Me.btnProcessar.Text = "Processar"
        Me.btnProcessar.UseVisualStyleBackColor = True
        '
        'OpenFileDialog
        '
        Me.OpenFileDialog.FileName = "OpenFileDialog"
        '
        'btnBuscar
        '
        Me.btnBuscar.Location = New System.Drawing.Point(844, 7)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(29, 23)
        Me.btnBuscar.TabIndex = 5
        Me.btnBuscar.Text = "..."
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'btnSair
        '
        Me.btnSair.Location = New System.Drawing.Point(1136, 593)
        Me.btnSair.Name = "btnSair"
        Me.btnSair.Size = New System.Drawing.Size(133, 23)
        Me.btnSair.TabIndex = 13
        Me.btnSair.Text = "Sair"
        Me.btnSair.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblContestado)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.lblAltConta)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.lblArrecadado)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.lblCriticado)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.lblFaturado)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.lblAfaturar)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Location = New System.Drawing.Point(905, 262)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(214, 354)
        Me.GroupBox2.TabIndex = 14
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Resumo Mensal"
        '
        'lblContestado
        '
        Me.lblContestado.AutoSize = True
        Me.lblContestado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContestado.Location = New System.Drawing.Point(21, 331)
        Me.lblContestado.Name = "lblContestado"
        Me.lblContestado.Size = New System.Drawing.Size(59, 16)
        Me.lblContestado.TabIndex = 21
        Me.lblContestado.Text = "R$ 0,00"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(20, 304)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(77, 16)
        Me.Label10.TabIndex = 22
        Me.Label10.Text = "Contestado"
        '
        'lblAltConta
        '
        Me.lblAltConta.AutoSize = True
        Me.lblAltConta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAltConta.Location = New System.Drawing.Point(18, 271)
        Me.lblAltConta.Name = "lblAltConta"
        Me.lblAltConta.Size = New System.Drawing.Size(59, 16)
        Me.lblAltConta.TabIndex = 19
        Me.lblAltConta.Text = "R$ 0,00"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(17, 244)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(124, 16)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "Alteração Validade"
        '
        'lblArrecadado
        '
        Me.lblArrecadado.AutoSize = True
        Me.lblArrecadado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblArrecadado.Location = New System.Drawing.Point(18, 216)
        Me.lblArrecadado.Name = "lblArrecadado"
        Me.lblArrecadado.Size = New System.Drawing.Size(59, 16)
        Me.lblArrecadado.TabIndex = 17
        Me.lblArrecadado.Text = "R$ 0,00"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(17, 189)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(80, 16)
        Me.Label13.TabIndex = 18
        Me.Label13.Text = "Arrecadado"
        '
        'lblCriticado
        '
        Me.lblCriticado.AutoSize = True
        Me.lblCriticado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCriticado.Location = New System.Drawing.Point(20, 160)
        Me.lblCriticado.Name = "lblCriticado"
        Me.lblCriticado.Size = New System.Drawing.Size(59, 16)
        Me.lblCriticado.TabIndex = 15
        Me.lblCriticado.Text = "R$ 0,00"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(19, 133)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(61, 16)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "Criticado"
        '
        'lblFaturado
        '
        Me.lblFaturado.AutoSize = True
        Me.lblFaturado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFaturado.Location = New System.Drawing.Point(18, 105)
        Me.lblFaturado.Name = "lblFaturado"
        Me.lblFaturado.Size = New System.Drawing.Size(59, 16)
        Me.lblFaturado.TabIndex = 13
        Me.lblFaturado.Text = "R$ 0,00"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(17, 78)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(62, 16)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "Faturado"
        '
        'lblAfaturar
        '
        Me.lblAfaturar.AutoSize = True
        Me.lblAfaturar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAfaturar.Location = New System.Drawing.Point(18, 52)
        Me.lblAfaturar.Name = "lblAfaturar"
        Me.lblAfaturar.Size = New System.Drawing.Size(59, 16)
        Me.lblAfaturar.TabIndex = 11
        Me.lblAfaturar.Text = "R$ 0,00"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(17, 25)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(62, 16)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "A Faturar"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.lblStatusArquivo)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.lblArquivo)
        Me.GroupBox1.Controls.Add(Me.lblDataArquivo)
        Me.GroupBox1.Controls.Add(Me.lblTipoArq)
        Me.GroupBox1.Controls.Add(Me.lblTotalReg)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.lblTotalArquivo)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Location = New System.Drawing.Point(905, 46)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(360, 210)
        Me.GroupBox1.TabIndex = 23
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Sobre o arquivo"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(20, 142)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 16)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Status Arquivo"
        '
        'lblStatusArquivo
        '
        Me.lblStatusArquivo.AutoSize = True
        Me.lblStatusArquivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatusArquivo.Location = New System.Drawing.Point(20, 167)
        Me.lblStatusArquivo.Name = "lblStatusArquivo"
        Me.lblStatusArquivo.Size = New System.Drawing.Size(0, 16)
        Me.lblStatusArquivo.TabIndex = 14
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(174, 84)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(105, 16)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "Data do Arquivo"
        '
        'lblArquivo
        '
        Me.lblArquivo.AutoSize = True
        Me.lblArquivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblArquivo.Location = New System.Drawing.Point(19, 59)
        Me.lblArquivo.Name = "lblArquivo"
        Me.lblArquivo.Size = New System.Drawing.Size(0, 16)
        Me.lblArquivo.TabIndex = 7
        '
        'lblDataArquivo
        '
        Me.lblDataArquivo.AutoSize = True
        Me.lblDataArquivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDataArquivo.Location = New System.Drawing.Point(174, 109)
        Me.lblDataArquivo.Name = "lblDataArquivo"
        Me.lblDataArquivo.Size = New System.Drawing.Size(0, 16)
        Me.lblDataArquivo.TabIndex = 12
        '
        'lblTipoArq
        '
        Me.lblTipoArq.AutoSize = True
        Me.lblTipoArq.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoArq.Location = New System.Drawing.Point(20, 31)
        Me.lblTipoArq.Name = "lblTipoArq"
        Me.lblTipoArq.Size = New System.Drawing.Size(104, 16)
        Me.lblTipoArq.TabIndex = 6
        Me.lblTipoArq.Text = "Tipo de Arquivo"
        '
        'lblTotalReg
        '
        Me.lblTotalReg.AutoSize = True
        Me.lblTotalReg.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalReg.Location = New System.Drawing.Point(174, 58)
        Me.lblTotalReg.Name = "lblTotalReg"
        Me.lblTotalReg.Size = New System.Drawing.Size(16, 16)
        Me.lblTotalReg.TabIndex = 8
        Me.lblTotalReg.Text = "0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(20, 84)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(136, 16)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Valor total do Aqruivo"
        '
        'lblTotalArquivo
        '
        Me.lblTotalArquivo.AutoSize = True
        Me.lblTotalArquivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalArquivo.Location = New System.Drawing.Point(20, 111)
        Me.lblTotalArquivo.Name = "lblTotalArquivo"
        Me.lblTotalArquivo.Size = New System.Drawing.Size(16, 16)
        Me.lblTotalArquivo.TabIndex = 9
        Me.lblTotalArquivo.Text = "0"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(173, 31)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(181, 16)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Qtd. de Registros  no arquivo"
        '
        'DtMesAnoPesquisa
        '
        Me.DtMesAnoPesquisa.CustomFormat = "MM/yyyy"
        Me.DtMesAnoPesquisa.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DtMesAnoPesquisa.Location = New System.Drawing.Point(1147, 283)
        Me.DtMesAnoPesquisa.Name = "DtMesAnoPesquisa"
        Me.DtMesAnoPesquisa.Size = New System.Drawing.Size(118, 20)
        Me.DtMesAnoPesquisa.TabIndex = 74
        Me.DtMesAnoPesquisa.Value = New Date(2016, 12, 1, 0, 0, 0, 0)
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(1144, 262)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(91, 16)
        Me.Label6.TabIndex = 73
        Me.Label6.Text = "Ano Mês Ref.:"
        '
        'chkProcessarTodos
        '
        Me.chkProcessarTodos.AutoSize = True
        Me.chkProcessarTodos.Checked = True
        Me.chkProcessarTodos.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkProcessarTodos.Location = New System.Drawing.Point(1024, 13)
        Me.chkProcessarTodos.Name = "chkProcessarTodos"
        Me.chkProcessarTodos.Size = New System.Drawing.Size(102, 17)
        Me.chkProcessarTodos.TabIndex = 75
        Me.chkProcessarTodos.Text = "Processar todos"
        Me.chkProcessarTodos.UseVisualStyleBackColor = True
        '
        'lblArquivosLote
        '
        Me.lblArquivosLote.AutoSize = True
        Me.lblArquivosLote.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblArquivosLote.Location = New System.Drawing.Point(29, 631)
        Me.lblArquivosLote.Name = "lblArquivosLote"
        Me.lblArquivosLote.Size = New System.Drawing.Size(179, 16)
        Me.lblArquivosLote.TabIndex = 76
        Me.lblArquivosLote.Text = "Processando arquivo: 0 de 0"
        '
        'ProgressBar
        '
        Me.ProgressBar.Location = New System.Drawing.Point(31, 657)
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(868, 23)
        Me.ProgressBar.TabIndex = 77
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnCarregar)
        Me.GroupBox3.Controls.Add(Me.lblArrecadadoAtual)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.Location = New System.Drawing.Point(905, 627)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(360, 62)
        Me.GroupBox3.TabIndex = 78
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Arrecadado"
        '
        'btnCarregar
        '
        Me.btnCarregar.Location = New System.Drawing.Point(262, 21)
        Me.btnCarregar.Name = "btnCarregar"
        Me.btnCarregar.Size = New System.Drawing.Size(92, 27)
        Me.btnCarregar.TabIndex = 1
        Me.btnCarregar.Text = "Carregar"
        Me.btnCarregar.UseVisualStyleBackColor = True
        '
        'lblArrecadadoAtual
        '
        Me.lblArrecadadoAtual.AutoSize = True
        Me.lblArrecadadoAtual.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblArrecadadoAtual.Location = New System.Drawing.Point(21, 30)
        Me.lblArrecadadoAtual.Name = "lblArrecadadoAtual"
        Me.lblArrecadadoAtual.Size = New System.Drawing.Size(61, 18)
        Me.lblArrecadadoAtual.TabIndex = 0
        Me.lblArrecadadoAtual.Text = "R$0.00"
        '
        'FrmRetorno
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1277, 701)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.ProgressBar)
        Me.Controls.Add(Me.lblArquivosLote)
        Me.Controls.Add(Me.chkProcessarTodos)
        Me.Controls.Add(Me.DtMesAnoPesquisa)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnSair)
        Me.Controls.Add(Me.btnBuscar)
        Me.Controls.Add(Me.btnProcessar)
        Me.Controls.Add(Me.gridRetorno)
        Me.Controls.Add(Me.btnPesquisar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtCaminho)
        Me.MaximizeBox = False
        Me.Name = "FrmRetorno"
        Me.Text = "Retorno"
        CType(Me.gridRetorno, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtCaminho As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnPesquisar As System.Windows.Forms.Button
    Friend WithEvents gridRetorno As System.Windows.Forms.DataGridView
    Friend WithEvents btnProcessar As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents btnSair As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblArrecadado As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lblCriticado As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lblFaturado As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblAfaturar As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblAltConta As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblContestado As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblStatusArquivo As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblArquivo As System.Windows.Forms.Label
    Friend WithEvents lblDataArquivo As System.Windows.Forms.Label
    Friend WithEvents lblTipoArq As System.Windows.Forms.Label
    Friend WithEvents lblTotalReg As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblTotalArquivo As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents DtMesAnoPesquisa As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents chkProcessarTodos As System.Windows.Forms.CheckBox
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents lblArquivosLote As System.Windows.Forms.Label
    Friend WithEvents ProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents btnCarregar As Button
    Friend WithEvents lblArrecadadoAtual As Label
End Class
