﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrevisaoRemessa
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.lblValorExclusoes = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.lblValorSemCriticas = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.lblvalorTotalInclusoes = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.lblValorContestados = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.lblPrevisaoRemessa = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.lblValoCriticados = New System.Windows.Forms.Label()
        Me.lblValorMensal = New System.Windows.Forms.Label()
        Me.lblvalorTotalRealizado = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.lblValorPrevisto = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.lblValor5 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.lblValor4 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.lblValor3 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lblValor2 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblValor1 = New System.Windows.Forms.Label()
        Me.lblPeriodo5 = New System.Windows.Forms.Label()
        Me.lblPeriodo4 = New System.Windows.Forms.Label()
        Me.lblPeriodo3 = New System.Windows.Forms.Label()
        Me.lblPeriodo2 = New System.Windows.Forms.Label()
        Me.lblPeriodo1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblValorReq = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.gridRequisicao = New System.Windows.Forms.DataGridView()
        Me.dtInicio = New System.Windows.Forms.DateTimePicker()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.dtFinal = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnPesquisar = New System.Windows.Forms.Button()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblValorTotalRequisicao = New System.Windows.Forms.Label()
        Me.lblValorTotalOperador = New System.Windows.Forms.Label()
        Me.lblValorTotalOutros = New System.Windows.Forms.Label()
        Me.lblValorTotalMensal = New System.Windows.Forms.Label()
        Me.gridResultadoOperadorOutros = New System.Windows.Forms.DataGridView()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.gridResultadoOperadorMensal = New System.Windows.Forms.DataGridView()
        Me.chkFiltrarData = New System.Windows.Forms.CheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblValorTotalArrecadadoOpera = New System.Windows.Forms.Label()
        Me.chkValorDiario = New System.Windows.Forms.CheckBox()
        Me.dtProximaRemessa = New System.Windows.Forms.DateTimePicker()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.dtUltimaRemessa = New System.Windows.Forms.DateTimePicker()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.btnPrevisao = New System.Windows.Forms.Button()
        Me.lblNuncTotal = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.lblNuncExisteRemessa = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.lblNuncCriticadosOi = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.lblNuncContestadosOi = New System.Windows.Forms.Label()
        Me.label37 = New System.Windows.Forms.Label()
        Me.TabControl1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.gridRequisicao, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel7.SuspendLayout()
        CType(Me.gridResultadoOperadorOutros, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridResultadoOperadorMensal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Location = New System.Drawing.Point(12, 59)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(611, 572)
        Me.TabControl1.TabIndex = 3
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.Gainsboro
        Me.TabPage2.Controls.Add(Me.lblNuncContestadosOi)
        Me.TabPage2.Controls.Add(Me.label37)
        Me.TabPage2.Controls.Add(Me.lblNuncCriticadosOi)
        Me.TabPage2.Controls.Add(Me.Label27)
        Me.TabPage2.Controls.Add(Me.lblNuncExisteRemessa)
        Me.TabPage2.Controls.Add(Me.Label24)
        Me.TabPage2.Controls.Add(Me.lblNuncTotal)
        Me.TabPage2.Controls.Add(Me.Label22)
        Me.TabPage2.Controls.Add(Me.btnPrevisao)
        Me.TabPage2.Controls.Add(Me.dtProximaRemessa)
        Me.TabPage2.Controls.Add(Me.Label13)
        Me.TabPage2.Controls.Add(Me.dtUltimaRemessa)
        Me.TabPage2.Controls.Add(Me.Label19)
        Me.TabPage2.Controls.Add(Me.lblValorExclusoes)
        Me.TabPage2.Controls.Add(Me.Label16)
        Me.TabPage2.Controls.Add(Me.lblValorSemCriticas)
        Me.TabPage2.Controls.Add(Me.Label15)
        Me.TabPage2.Controls.Add(Me.lblvalorTotalInclusoes)
        Me.TabPage2.Controls.Add(Me.Label14)
        Me.TabPage2.Controls.Add(Me.lblValorContestados)
        Me.TabPage2.Controls.Add(Me.Label20)
        Me.TabPage2.Controls.Add(Me.lblPrevisaoRemessa)
        Me.TabPage2.Controls.Add(Me.Label17)
        Me.TabPage2.Controls.Add(Me.lblValoCriticados)
        Me.TabPage2.Controls.Add(Me.lblValorMensal)
        Me.TabPage2.Controls.Add(Me.lblvalorTotalRealizado)
        Me.TabPage2.Controls.Add(Me.Label12)
        Me.TabPage2.Controls.Add(Me.Label11)
        Me.TabPage2.Controls.Add(Me.Label10)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(603, 546)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Previsão Requisição"
        '
        'lblValorExclusoes
        '
        Me.lblValorExclusoes.AutoSize = True
        Me.lblValorExclusoes.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorExclusoes.ForeColor = System.Drawing.Color.Red
        Me.lblValorExclusoes.Location = New System.Drawing.Point(268, 206)
        Me.lblValorExclusoes.Name = "lblValorExclusoes"
        Me.lblValorExclusoes.Size = New System.Drawing.Size(66, 18)
        Me.lblValorExclusoes.TabIndex = 19
        Me.lblValorExclusoes.Text = "R$ 0.00"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.ForeColor = System.Drawing.Color.Red
        Me.Label16.Location = New System.Drawing.Point(37, 206)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(168, 18)
        Me.Label16.TabIndex = 18
        Me.Label16.Text = "Valor de Exclusões : "
        '
        'lblValorSemCriticas
        '
        Me.lblValorSemCriticas.AutoSize = True
        Me.lblValorSemCriticas.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorSemCriticas.Location = New System.Drawing.Point(267, 178)
        Me.lblValorSemCriticas.Name = "lblValorSemCriticas"
        Me.lblValorSemCriticas.Size = New System.Drawing.Size(66, 18)
        Me.lblValorSemCriticas.TabIndex = 17
        Me.lblValorSemCriticas.Text = "R$ 0.00"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(36, 178)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(219, 18)
        Me.Label15.TabIndex = 16
        Me.Label15.Text = "Valor Fichas Sem Criticas : "
        '
        'lblvalorTotalInclusoes
        '
        Me.lblvalorTotalInclusoes.AutoSize = True
        Me.lblvalorTotalInclusoes.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblvalorTotalInclusoes.Location = New System.Drawing.Point(266, 46)
        Me.lblvalorTotalInclusoes.Name = "lblvalorTotalInclusoes"
        Me.lblvalorTotalInclusoes.Size = New System.Drawing.Size(66, 18)
        Me.lblvalorTotalInclusoes.TabIndex = 15
        Me.lblvalorTotalInclusoes.Text = "R$ 0.00"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(35, 46)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(225, 18)
        Me.Label14.TabIndex = 14
        Me.Label14.Text = "Valor Total Novas Inclusões:"
        '
        'lblValorContestados
        '
        Me.lblValorContestados.AutoSize = True
        Me.lblValorContestados.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorContestados.ForeColor = System.Drawing.Color.Red
        Me.lblValorContestados.Location = New System.Drawing.Point(269, 263)
        Me.lblValorContestados.Name = "lblValorContestados"
        Me.lblValorContestados.Size = New System.Drawing.Size(66, 18)
        Me.lblValorContestados.TabIndex = 13
        Me.lblValorContestados.Text = "R$ 0.00"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.ForeColor = System.Drawing.Color.Red
        Me.Label20.Location = New System.Drawing.Point(37, 263)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(186, 18)
        Me.Label20.TabIndex = 12
        Me.Label20.Text = "Valor de Contestados : "
        '
        'lblPrevisaoRemessa
        '
        Me.lblPrevisaoRemessa.AutoSize = True
        Me.lblPrevisaoRemessa.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPrevisaoRemessa.ForeColor = System.Drawing.Color.Blue
        Me.lblPrevisaoRemessa.Location = New System.Drawing.Point(271, 289)
        Me.lblPrevisaoRemessa.Name = "lblPrevisaoRemessa"
        Me.lblPrevisaoRemessa.Size = New System.Drawing.Size(66, 18)
        Me.lblPrevisaoRemessa.TabIndex = 11
        Me.lblPrevisaoRemessa.Text = "R$ 0.00"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.ForeColor = System.Drawing.Color.Blue
        Me.Label17.Location = New System.Drawing.Point(37, 289)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(228, 18)
        Me.Label17.TabIndex = 10
        Me.Label17.Text = "Valor Previsto da Remessa : "
        '
        'lblValoCriticados
        '
        Me.lblValoCriticados.AutoSize = True
        Me.lblValoCriticados.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValoCriticados.ForeColor = System.Drawing.Color.Red
        Me.lblValoCriticados.Location = New System.Drawing.Point(269, 234)
        Me.lblValoCriticados.Name = "lblValoCriticados"
        Me.lblValoCriticados.Size = New System.Drawing.Size(66, 18)
        Me.lblValoCriticados.TabIndex = 8
        Me.lblValoCriticados.Text = "R$ 0.00"
        '
        'lblValorMensal
        '
        Me.lblValorMensal.AutoSize = True
        Me.lblValorMensal.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorMensal.Location = New System.Drawing.Point(266, 74)
        Me.lblValorMensal.Name = "lblValorMensal"
        Me.lblValorMensal.Size = New System.Drawing.Size(66, 18)
        Me.lblValorMensal.TabIndex = 7
        Me.lblValorMensal.Text = "R$ 0.00"
        '
        'lblvalorTotalRealizado
        '
        Me.lblvalorTotalRealizado.AutoSize = True
        Me.lblvalorTotalRealizado.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblvalorTotalRealizado.Location = New System.Drawing.Point(266, 18)
        Me.lblvalorTotalRealizado.Name = "lblvalorTotalRealizado"
        Me.lblvalorTotalRealizado.Size = New System.Drawing.Size(66, 18)
        Me.lblvalorTotalRealizado.TabIndex = 6
        Me.lblvalorTotalRealizado.Text = "R$ 0.00"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Red
        Me.Label12.Location = New System.Drawing.Point(38, 234)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(167, 18)
        Me.Label12.TabIndex = 5
        Me.Label12.Text = "Valor de Criticados : "
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(35, 74)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(208, 18)
        Me.Label11.TabIndex = 4
        Me.Label11.Text = "Valor de Fichas Mensais : "
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(35, 18)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(180, 18)
        Me.Label10.TabIndex = 3
        Me.Label10.Text = "Valor Total Realizado: "
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage1.Controls.Add(Me.lblValorPrevisto)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.Panel1)
        Me.TabPage1.Controls.Add(Me.lblValorReq)
        Me.TabPage1.Controls.Add(Me.Label26)
        Me.TabPage1.Controls.Add(Me.gridRequisicao)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(603, 546)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Resumo"
        '
        'lblValorPrevisto
        '
        Me.lblValorPrevisto.AutoSize = True
        Me.lblValorPrevisto.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorPrevisto.Location = New System.Drawing.Point(261, 32)
        Me.lblValorPrevisto.Name = "lblValorPrevisto"
        Me.lblValorPrevisto.Size = New System.Drawing.Size(66, 18)
        Me.lblValorPrevisto.TabIndex = 8
        Me.lblValorPrevisto.Text = "R$ 0,00"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(17, 32)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(228, 18)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Valor total Previsto Operador"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Panel6)
        Me.Panel1.Controls.Add(Me.Panel5)
        Me.Panel1.Controls.Add(Me.Panel4)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.lblPeriodo5)
        Me.Panel1.Controls.Add(Me.lblPeriodo4)
        Me.Panel1.Controls.Add(Me.lblPeriodo3)
        Me.Panel1.Controls.Add(Me.lblPeriodo2)
        Me.Panel1.Controls.Add(Me.lblPeriodo1)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Location = New System.Drawing.Point(351, 59)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(236, 481)
        Me.Panel1.TabIndex = 6
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.lblValor5)
        Me.Panel6.Location = New System.Drawing.Point(17, 389)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(200, 50)
        Me.Panel6.TabIndex = 15
        '
        'lblValor5
        '
        Me.lblValor5.AutoSize = True
        Me.lblValor5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValor5.Location = New System.Drawing.Point(12, 17)
        Me.lblValor5.Name = "lblValor5"
        Me.lblValor5.Size = New System.Drawing.Size(27, 18)
        Me.lblValor5.TabIndex = 17
        Me.lblValor5.Text = "R$"
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.lblValor4)
        Me.Panel5.Location = New System.Drawing.Point(17, 315)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(200, 50)
        Me.Panel5.TabIndex = 14
        '
        'lblValor4
        '
        Me.lblValor4.AutoSize = True
        Me.lblValor4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValor4.Location = New System.Drawing.Point(12, 15)
        Me.lblValor4.Name = "lblValor4"
        Me.lblValor4.Size = New System.Drawing.Size(27, 18)
        Me.lblValor4.TabIndex = 19
        Me.lblValor4.Text = "R$"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.lblValor3)
        Me.Panel4.Location = New System.Drawing.Point(17, 241)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(200, 50)
        Me.Panel4.TabIndex = 13
        '
        'lblValor3
        '
        Me.lblValor3.AutoSize = True
        Me.lblValor3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValor3.Location = New System.Drawing.Point(12, 14)
        Me.lblValor3.Name = "lblValor3"
        Me.lblValor3.Size = New System.Drawing.Size(27, 18)
        Me.lblValor3.TabIndex = 18
        Me.lblValor3.Text = "R$"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.lblValor2)
        Me.Panel3.Location = New System.Drawing.Point(17, 167)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(200, 50)
        Me.Panel3.TabIndex = 12
        '
        'lblValor2
        '
        Me.lblValor2.AutoSize = True
        Me.lblValor2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValor2.Location = New System.Drawing.Point(12, 16)
        Me.lblValor2.Name = "lblValor2"
        Me.lblValor2.Size = New System.Drawing.Size(27, 18)
        Me.lblValor2.TabIndex = 17
        Me.lblValor2.Text = "R$"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lblValor1)
        Me.Panel2.Location = New System.Drawing.Point(17, 93)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(200, 50)
        Me.Panel2.TabIndex = 11
        '
        'lblValor1
        '
        Me.lblValor1.AutoSize = True
        Me.lblValor1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValor1.Location = New System.Drawing.Point(12, 15)
        Me.lblValor1.Name = "lblValor1"
        Me.lblValor1.Size = New System.Drawing.Size(27, 18)
        Me.lblValor1.TabIndex = 16
        Me.lblValor1.Text = "R$"
        '
        'lblPeriodo5
        '
        Me.lblPeriodo5.AutoSize = True
        Me.lblPeriodo5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodo5.Location = New System.Drawing.Point(14, 368)
        Me.lblPeriodo5.Name = "lblPeriodo5"
        Me.lblPeriodo5.Size = New System.Drawing.Size(56, 18)
        Me.lblPeriodo5.TabIndex = 10
        Me.lblPeriodo5.Text = "--/--/----"
        '
        'lblPeriodo4
        '
        Me.lblPeriodo4.AutoSize = True
        Me.lblPeriodo4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodo4.Location = New System.Drawing.Point(14, 294)
        Me.lblPeriodo4.Name = "lblPeriodo4"
        Me.lblPeriodo4.Size = New System.Drawing.Size(56, 18)
        Me.lblPeriodo4.TabIndex = 9
        Me.lblPeriodo4.Text = "--/--/----"
        '
        'lblPeriodo3
        '
        Me.lblPeriodo3.AutoSize = True
        Me.lblPeriodo3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodo3.Location = New System.Drawing.Point(14, 220)
        Me.lblPeriodo3.Name = "lblPeriodo3"
        Me.lblPeriodo3.Size = New System.Drawing.Size(56, 18)
        Me.lblPeriodo3.TabIndex = 8
        Me.lblPeriodo3.Text = "--/--/----"
        '
        'lblPeriodo2
        '
        Me.lblPeriodo2.AutoSize = True
        Me.lblPeriodo2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodo2.Location = New System.Drawing.Point(14, 146)
        Me.lblPeriodo2.Name = "lblPeriodo2"
        Me.lblPeriodo2.Size = New System.Drawing.Size(56, 18)
        Me.lblPeriodo2.TabIndex = 7
        Me.lblPeriodo2.Text = "--/--/----"
        '
        'lblPeriodo1
        '
        Me.lblPeriodo1.AutoSize = True
        Me.lblPeriodo1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodo1.Location = New System.Drawing.Point(14, 72)
        Me.lblPeriodo1.Name = "lblPeriodo1"
        Me.lblPeriodo1.Size = New System.Drawing.Size(56, 18)
        Me.lblPeriodo1.TabIndex = 6
        Me.lblPeriodo1.Text = "--/--/----"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(14, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(177, 20)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Resumo por Semana"
        '
        'lblValorReq
        '
        Me.lblValorReq.AutoSize = True
        Me.lblValorReq.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorReq.Location = New System.Drawing.Point(261, 9)
        Me.lblValorReq.Name = "lblValorReq"
        Me.lblValorReq.Size = New System.Drawing.Size(66, 18)
        Me.lblValorReq.TabIndex = 4
        Me.lblValorReq.Text = "R$ 0,00"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(17, 9)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(228, 18)
        Me.Label26.TabIndex = 2
        Me.Label26.Text = "Valor total das fichas no mês"
        '
        'gridRequisicao
        '
        Me.gridRequisicao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridRequisicao.Location = New System.Drawing.Point(6, 59)
        Me.gridRequisicao.Name = "gridRequisicao"
        Me.gridRequisicao.Size = New System.Drawing.Size(332, 481)
        Me.gridRequisicao.TabIndex = 0
        '
        'dtInicio
        '
        Me.dtInicio.CustomFormat = ""
        Me.dtInicio.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtInicio.Location = New System.Drawing.Point(91, 12)
        Me.dtInicio.Name = "dtInicio"
        Me.dtInicio.Size = New System.Drawing.Size(178, 20)
        Me.dtInicio.TabIndex = 75
        Me.dtInicio.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(12, 15)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(76, 13)
        Me.Label18.TabIndex = 74
        Me.Label18.Text = "Data Inicial:"
        '
        'dtFinal
        '
        Me.dtFinal.CustomFormat = ""
        Me.dtFinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtFinal.Location = New System.Drawing.Point(353, 12)
        Me.dtFinal.Name = "dtFinal"
        Me.dtFinal.Size = New System.Drawing.Size(178, 20)
        Me.dtFinal.TabIndex = 77
        Me.dtFinal.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(274, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 13)
        Me.Label1.TabIndex = 76
        Me.Label1.Text = "Data final:"
        '
        'btnPesquisar
        '
        Me.btnPesquisar.Location = New System.Drawing.Point(544, 10)
        Me.btnPesquisar.Name = "btnPesquisar"
        Me.btnPesquisar.Size = New System.Drawing.Size(75, 23)
        Me.btnPesquisar.TabIndex = 78
        Me.btnPesquisar.Text = "Pesquisar"
        Me.btnPesquisar.UseVisualStyleBackColor = True
        '
        'Panel7
        '
        Me.Panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel7.Controls.Add(Me.Label8)
        Me.Panel7.Controls.Add(Me.Label2)
        Me.Panel7.Controls.Add(Me.lblValorTotalRequisicao)
        Me.Panel7.Controls.Add(Me.lblValorTotalOperador)
        Me.Panel7.Controls.Add(Me.lblValorTotalOutros)
        Me.Panel7.Controls.Add(Me.lblValorTotalMensal)
        Me.Panel7.Controls.Add(Me.gridResultadoOperadorOutros)
        Me.Panel7.Controls.Add(Me.Label7)
        Me.Panel7.Controls.Add(Me.Label6)
        Me.Panel7.Controls.Add(Me.gridResultadoOperadorMensal)
        Me.Panel7.Location = New System.Drawing.Point(629, 10)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(681, 617)
        Me.Panel7.TabIndex = 79
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(358, 21)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(17, 24)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "-"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 2)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(186, 20)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Resumo por Operador"
        '
        'lblValorTotalRequisicao
        '
        Me.lblValorTotalRequisicao.AutoSize = True
        Me.lblValorTotalRequisicao.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorTotalRequisicao.ForeColor = System.Drawing.Color.Fuchsia
        Me.lblValorTotalRequisicao.Location = New System.Drawing.Point(375, 27)
        Me.lblValorTotalRequisicao.Name = "lblValorTotalRequisicao"
        Me.lblValorTotalRequisicao.Size = New System.Drawing.Size(164, 13)
        Me.lblValorTotalRequisicao.TabIndex = 16
        Me.lblValorTotalRequisicao.Text = "Valor Total Requisição : R$"
        '
        'lblValorTotalOperador
        '
        Me.lblValorTotalOperador.AutoSize = True
        Me.lblValorTotalOperador.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorTotalOperador.ForeColor = System.Drawing.Color.LimeGreen
        Me.lblValorTotalOperador.Location = New System.Drawing.Point(375, 49)
        Me.lblValorTotalOperador.Name = "lblValorTotalOperador"
        Me.lblValorTotalOperador.Size = New System.Drawing.Size(149, 13)
        Me.lblValorTotalOperador.TabIndex = 15
        Me.lblValorTotalOperador.Text = "Valor Total Operador: R$"
        '
        'lblValorTotalOutros
        '
        Me.lblValorTotalOutros.AutoSize = True
        Me.lblValorTotalOutros.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorTotalOutros.ForeColor = System.Drawing.Color.LimeGreen
        Me.lblValorTotalOutros.Location = New System.Drawing.Point(398, 323)
        Me.lblValorTotalOutros.Name = "lblValorTotalOutros"
        Me.lblValorTotalOutros.Size = New System.Drawing.Size(126, 18)
        Me.lblValorTotalOutros.TabIndex = 14
        Me.lblValorTotalOutros.Text = "Valor Total : R$"
        '
        'lblValorTotalMensal
        '
        Me.lblValorTotalMensal.AutoSize = True
        Me.lblValorTotalMensal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorTotalMensal.ForeColor = System.Drawing.Color.DodgerBlue
        Me.lblValorTotalMensal.Location = New System.Drawing.Point(375, 10)
        Me.lblValorTotalMensal.Name = "lblValorTotalMensal"
        Me.lblValorTotalMensal.Size = New System.Drawing.Size(97, 13)
        Me.lblValorTotalMensal.TabIndex = 13
        Me.lblValorTotalMensal.Text = "Valor Total : R$"
        '
        'gridResultadoOperadorOutros
        '
        Me.gridResultadoOperadorOutros.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridResultadoOperadorOutros.Location = New System.Drawing.Point(3, 348)
        Me.gridResultadoOperadorOutros.Name = "gridResultadoOperadorOutros"
        Me.gridResultadoOperadorOutros.Size = New System.Drawing.Size(673, 262)
        Me.gridResultadoOperadorOutros.TabIndex = 12
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(3, 323)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(290, 18)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Valor Arrecadação Outras Categorias"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(3, 46)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(275, 18)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Valor Arrecadação Mensal/Especial"
        '
        'gridResultadoOperadorMensal
        '
        Me.gridResultadoOperadorMensal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridResultadoOperadorMensal.Location = New System.Drawing.Point(3, 71)
        Me.gridResultadoOperadorMensal.Name = "gridResultadoOperadorMensal"
        Me.gridResultadoOperadorMensal.Size = New System.Drawing.Size(673, 242)
        Me.gridResultadoOperadorMensal.TabIndex = 7
        '
        'chkFiltrarData
        '
        Me.chkFiltrarData.AutoSize = True
        Me.chkFiltrarData.Location = New System.Drawing.Point(12, 38)
        Me.chkFiltrarData.Name = "chkFiltrarData"
        Me.chkFiltrarData.Size = New System.Drawing.Size(149, 17)
        Me.chkFiltrarData.TabIndex = 80
        Me.chkFiltrarData.Text = "Filtrar operadores por data"
        Me.chkFiltrarData.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.Red
        Me.Label4.Location = New System.Drawing.Point(19, 634)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(264, 13)
        Me.Label4.TabIndex = 81
        Me.Label4.Text = "* Valores se referindo a (Requisição - Valores Mensais)"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(634, 637)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(279, 18)
        Me.Label9.TabIndex = 82
        Me.Label9.Text = "Valor Arrecadado previsto Operador"
        '
        'lblValorTotalArrecadadoOpera
        '
        Me.lblValorTotalArrecadadoOpera.AutoSize = True
        Me.lblValorTotalArrecadadoOpera.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorTotalArrecadadoOpera.Location = New System.Drawing.Point(911, 637)
        Me.lblValorTotalArrecadadoOpera.Name = "lblValorTotalArrecadadoOpera"
        Me.lblValorTotalArrecadadoOpera.Size = New System.Drawing.Size(29, 18)
        Me.lblValorTotalArrecadadoOpera.TabIndex = 83
        Me.lblValorTotalArrecadadoOpera.Text = "R$"
        '
        'chkValorDiario
        '
        Me.chkValorDiario.AutoSize = True
        Me.chkValorDiario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkValorDiario.Location = New System.Drawing.Point(353, 38)
        Me.chkValorDiario.Name = "chkValorDiario"
        Me.chkValorDiario.Size = New System.Drawing.Size(174, 19)
        Me.chkValorDiario.TabIndex = 84
        Me.chkValorDiario.Text = "Verificar Valores Diario"
        Me.chkValorDiario.UseVisualStyleBackColor = True
        '
        'dtProximaRemessa
        '
        Me.dtProximaRemessa.CustomFormat = ""
        Me.dtProximaRemessa.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtProximaRemessa.Location = New System.Drawing.Point(204, 368)
        Me.dtProximaRemessa.Name = "dtProximaRemessa"
        Me.dtProximaRemessa.Size = New System.Drawing.Size(121, 20)
        Me.dtProximaRemessa.TabIndex = 81
        Me.dtProximaRemessa.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(39, 374)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(160, 13)
        Me.Label13.TabIndex = 80
        Me.Label13.Text = "Envio da pr´xima Remessa:"
        '
        'dtUltimaRemessa
        '
        Me.dtUltimaRemessa.CustomFormat = ""
        Me.dtUltimaRemessa.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtUltimaRemessa.Location = New System.Drawing.Point(205, 332)
        Me.dtUltimaRemessa.Name = "dtUltimaRemessa"
        Me.dtUltimaRemessa.Size = New System.Drawing.Size(120, 20)
        Me.dtUltimaRemessa.TabIndex = 79
        Me.dtUltimaRemessa.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(39, 338)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(155, 13)
        Me.Label19.TabIndex = 78
        Me.Label19.Text = "Envio da Ultima Remessa:"
        '
        'btnPrevisao
        '
        Me.btnPrevisao.Location = New System.Drawing.Point(251, 405)
        Me.btnPrevisao.Name = "btnPrevisao"
        Me.btnPrevisao.Size = New System.Drawing.Size(75, 23)
        Me.btnPrevisao.TabIndex = 82
        Me.btnPrevisao.Text = "Previsão"
        Me.btnPrevisao.UseVisualStyleBackColor = True
        '
        'lblNuncTotal
        '
        Me.lblNuncTotal.AutoSize = True
        Me.lblNuncTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNuncTotal.ForeColor = System.Drawing.Color.MediumVioletRed
        Me.lblNuncTotal.Location = New System.Drawing.Point(267, 102)
        Me.lblNuncTotal.Name = "lblNuncTotal"
        Me.lblNuncTotal.Size = New System.Drawing.Size(52, 13)
        Me.lblNuncTotal.TabIndex = 84
        Me.lblNuncTotal.Text = "R$ 0.00"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.ForeColor = System.Drawing.Color.MediumVioletRed
        Me.Label22.Location = New System.Drawing.Point(36, 102)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(163, 13)
        Me.Label22.TabIndex = 83
        Me.Label22.Text = "Valor Fichas Sem Criticas : "
        '
        'lblNuncExisteRemessa
        '
        Me.lblNuncExisteRemessa.AutoSize = True
        Me.lblNuncExisteRemessa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNuncExisteRemessa.ForeColor = System.Drawing.Color.MediumVioletRed
        Me.lblNuncExisteRemessa.Location = New System.Drawing.Point(267, 120)
        Me.lblNuncExisteRemessa.Name = "lblNuncExisteRemessa"
        Me.lblNuncExisteRemessa.Size = New System.Drawing.Size(52, 13)
        Me.lblNuncExisteRemessa.TabIndex = 86
        Me.lblNuncExisteRemessa.Text = "R$ 0.00"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.MediumVioletRed
        Me.Label24.Location = New System.Drawing.Point(36, 120)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(174, 13)
        Me.Label24.TabIndex = 85
        Me.Label24.Text = "Valor Existentes na remessa: "
        '
        'lblNuncCriticadosOi
        '
        Me.lblNuncCriticadosOi.AutoSize = True
        Me.lblNuncCriticadosOi.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNuncCriticadosOi.ForeColor = System.Drawing.Color.MediumVioletRed
        Me.lblNuncCriticadosOi.Location = New System.Drawing.Point(267, 138)
        Me.lblNuncCriticadosOi.Name = "lblNuncCriticadosOi"
        Me.lblNuncCriticadosOi.Size = New System.Drawing.Size(52, 13)
        Me.lblNuncCriticadosOi.TabIndex = 88
        Me.lblNuncCriticadosOi.Text = "R$ 0.00"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.ForeColor = System.Drawing.Color.MediumVioletRed
        Me.Label27.Location = New System.Drawing.Point(36, 138)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(124, 13)
        Me.Label27.TabIndex = 87
        Me.Label27.Text = "Valor Criticados Oi : "
        '
        'lblNuncContestadosOi
        '
        Me.lblNuncContestadosOi.AutoSize = True
        Me.lblNuncContestadosOi.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNuncContestadosOi.ForeColor = System.Drawing.Color.MediumVioletRed
        Me.lblNuncContestadosOi.Location = New System.Drawing.Point(268, 156)
        Me.lblNuncContestadosOi.Name = "lblNuncContestadosOi"
        Me.lblNuncContestadosOi.Size = New System.Drawing.Size(52, 13)
        Me.lblNuncContestadosOi.TabIndex = 90
        Me.lblNuncContestadosOi.Text = "R$ 0.00"
        '
        'label37
        '
        Me.label37.AutoSize = True
        Me.label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label37.ForeColor = System.Drawing.Color.MediumVioletRed
        Me.label37.Location = New System.Drawing.Point(37, 156)
        Me.label37.Name = "label37"
        Me.label37.Size = New System.Drawing.Size(130, 13)
        Me.label37.TabIndex = 89
        Me.label37.Text = "Valor Contestados Oi:"
        '
        'frmPrevisaoRemessa
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1322, 664)
        Me.Controls.Add(Me.chkValorDiario)
        Me.Controls.Add(Me.lblValorTotalArrecadadoOpera)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.chkFiltrarData)
        Me.Controls.Add(Me.Panel7)
        Me.Controls.Add(Me.btnPesquisar)
        Me.Controls.Add(Me.dtFinal)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dtInicio)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "frmPrevisaoRemessa"
        Me.Text = "Relatório Operador"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.gridRequisicao, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        CType(Me.gridResultadoOperadorOutros, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridResultadoOperadorMensal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents lblValorReq As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents gridRequisicao As System.Windows.Forms.DataGridView
    Friend WithEvents dtInicio As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents dtFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnPesquisar As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblPeriodo5 As System.Windows.Forms.Label
    Friend WithEvents lblPeriodo4 As System.Windows.Forms.Label
    Friend WithEvents lblPeriodo3 As System.Windows.Forms.Label
    Friend WithEvents lblPeriodo2 As System.Windows.Forms.Label
    Friend WithEvents lblPeriodo1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblValor5 As System.Windows.Forms.Label
    Friend WithEvents lblValor4 As System.Windows.Forms.Label
    Friend WithEvents lblValor3 As System.Windows.Forms.Label
    Friend WithEvents lblValor2 As System.Windows.Forms.Label
    Friend WithEvents lblValor1 As System.Windows.Forms.Label
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents gridResultadoOperadorMensal As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkFiltrarData As System.Windows.Forms.CheckBox
    Friend WithEvents lblValorPrevisto As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents gridResultadoOperadorOutros As System.Windows.Forms.DataGridView
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblValorTotalOutros As System.Windows.Forms.Label
    Friend WithEvents lblValorTotalMensal As System.Windows.Forms.Label
    Friend WithEvents lblValorTotalRequisicao As System.Windows.Forms.Label
    Friend WithEvents lblValorTotalOperador As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    ' Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    ' Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblValorTotalArrecadadoOpera As System.Windows.Forms.Label
    Friend WithEvents chkValorDiario As System.Windows.Forms.CheckBox
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lblValorContestados As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents lblPrevisaoRemessa As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents lblValoCriticados As System.Windows.Forms.Label
    Friend WithEvents lblValorMensal As System.Windows.Forms.Label
    Friend WithEvents lblvalorTotalRealizado As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lblvalorTotalInclusoes As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents lblValorSemCriticas As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents lblValorExclusoes As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents btnPrevisao As Button
    Friend WithEvents dtProximaRemessa As DateTimePicker
    Friend WithEvents Label13 As Label
    Friend WithEvents dtUltimaRemessa As DateTimePicker
    Friend WithEvents Label19 As Label
    Friend WithEvents lblNuncContestadosOi As Label
    Friend WithEvents label37 As Label
    Friend WithEvents lblNuncCriticadosOi As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents lblNuncTotal As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents lblNuncExisteRemessa As Label
    'Friend WithEvents ShapeContainer2 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    'Friend WithEvents LineShape2 As Microsoft.VisualBasic.PowerPacks.LineShape
End Class
