﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text
Imports Microsoft.Office.Interop


Public Class FrmRecuperacao


#Region "Enum"

    Private Enum ColunasGridListaTelefonica
        DDD = 0
        Telefone = 1
        nome_cliente = 2
        valor = 3
        categoria = 4
    End Enum

    Private Enum ColunasGridHistorico
        DDD = 0
        Telefone = 1
        Valor = 2
        categoria = 3
        mesRef = 4
        Situacao = 5
        MotivoSituacao = 6
        Dt_Situacao = 7
    End Enum

    Enum xlsOption
        xlsOpen
    End Enum

#End Region

#Region "variaveis"

    Public telefone As String
    Private lstContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstContribuinteSelecionada As New List(Of CRetorno)
    Private v_ambiente As String
    Private rnContribuinte As RNContribuinte = New RNContribuinte
    Private rnRetorno As RNRetorno = New RNRetorno
    Private contribuinte As CContribuinte = New CContribuinte
    Private lstContribuintesOi As New SortableBindingList(Of CRetorno)
    Private lstListaTelefonica As New SortableBindingList(Of CListaTelefonica)
    Private lstContribuinteSelecionadaOi As New List(Of CGenerico)
    Public mNivelAcesso As Integer
    Public mUsuario As String
    Public mCodUsuario As Integer
    Public atualizou As Boolean
    Public bindingSourceContribuite As BindingSource = New BindingSource


    Public Property NivelAcesso() As Integer
        Get
            Return mNivelAcesso
        End Get
        Set(ByVal value As Integer)
            mNivelAcesso = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return mUsuario
        End Get
        Set(ByVal value As String)
            mUsuario = value
        End Set
    End Property

    Public Property CodUsuario() As Integer
        Get
            Return mCodUsuario
        End Get
        Set(ByVal value As Integer)
            mCodUsuario = value
        End Set
    End Property

#End Region



#Region "Metodos"



    Private Sub carregaGrid()

        Dim objContribuintes As CContribuinte = New CContribuinte()

        Dim bindingSourceApolice As BindingSource = New BindingSource

        Try



            lstContribuintes = New SortableBindingList(Of CContribuinte)(rnContribuinte.CarregarGridOi(objContribuintes, "Remessa"))

            If (lstContribuintes.Count > 0) Then

                bindingSourceContribuite.DataSource = lstContribuintes

                grdListaTelefonica.DataSource = bindingSourceContribuite.DataSource


            End If


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CriarColunasGrid()

        Try

            grdListaTelefonica.DataSource = Nothing
            grdListaTelefonica.Columns.Clear()

            grdListaTelefonica.AutoGenerateColumns = False

            grdListaTelefonica.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            'Dim checkboxColumn As New DataGridViewCheckBoxColumn
            'checkboxColumn.Width = 32
            'checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StDDDMeioAcessoCobranca"
            column.HeaderText = "DDD"
            column.Width = 60
            column.ReadOnly = True
            grdListaTelefonica.Columns.Add(column)


            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StNumMeioAcessoCobranca"
            column.HeaderText = "Telefone"
            column.Width = 80
            column.ReadOnly = True
            grdListaTelefonica.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DVlrServico"
            column.HeaderText = "Valor"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            grdListaTelefonica.Columns.Add(column)

            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DdDataServico"
            column.HeaderText = "Data do Serviço"
            column.Width = 100
            column.ReadOnly = False
            column.Visible = True
            grdListaTelefonica.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StDescReferencia"
            column.HeaderText = "Código do Cliente"
            column.Width = 100
            column.ReadOnly = False
            column.Visible = True
            grdListaTelefonica.Columns.Add(column)

            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UsSitFaturamento"
            column.HeaderText = "Situação do Faturamento"
            column.Width = 100
            column.ReadOnly = False
            column.Visible = True
            grdListaTelefonica.Columns.Add(column)

            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UiMotivoSituacao"
            column.HeaderText = "Motivo do Faturamento"
            column.Width = 100
            column.ReadOnly = False
            column.Visible = True
            grdListaTelefonica.Columns.Add(column)


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub



#End Region

    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""

        Try



            Me.Cursor = Cursors.WaitCursor


            CarregaTelefoneRecuperacao()



            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try

    End Sub


    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Close()
    End Sub

    Private Sub FrmConsultaContribuinte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim cod_cliente As Integer = 0
        Dim contribuinteCarregado As CContribuinte = New CContribuinte
        Dim objContribuinte As CContribuinte = New CContribuinte

        Try


            CriarColunasGrid()
            CarregaTelefoneRecuperacao()

            atualizou = True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub



    Private Sub CarregaTelefoneRecuperacao()
        Dim rn As RNRetorno = New RNRetorno
        Dim valor As Single
        Dim quantidade As Integer
        Dim strValor As String
        Dim ds As DataSet = New DataSet



        Try

            grdListaTelefonica.DataSource = rn.BuscaTelefonesRecuperacao()

            ds = rn.BuscaTotaisTelefonesRecuperacao()

            If (ds.Tables(1).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(1).Rows(0).Item(0)) Then
                    quantidade = ds.Tables(1).Rows(0).Item(0)
                Else
                    quantidade = 0
                End If
            End If

            If (ds.Tables(2).Rows.Count > 0) Then
                If Not IsDBNull(ds.Tables(2).Rows(0).Item(0)) Then
                    valor = ds.Tables(2).Rows(0).Item(0)
                Else
                    valor = 0
                End If
            End If

            lblTotalFichas.Text = quantidade

            strValor = valor.ToString("C")
            lblValor.Text = valor.ToString("R$ #,###.00")


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub



    Private Sub txtNome_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBuscaNome.TextChanged
        Dim texto As String = Nothing
        Dim linhaVazia As Integer = 0
        Dim contador As Integer = 0

        If txtBuscaNome.Text <> String.Empty Then

            linhaVazia = grdListaTelefonica.Rows.Count - 1

            'percorre cada linha do DataGridView
            For Each linha As DataGridViewRow In grdListaTelefonica.Rows

                'percorre cada célula da linha
                For Each celula As DataGridViewCell In grdListaTelefonica.Rows(linha.Index).Cells
                    If Not contador = linhaVazia Then
                        'se a coluna for a coluna 2 (Nome) então verifica o criterio
                        If celula.ColumnIndex = 2 Then

                            If celula.Value.ToString <> "" Then
                                texto = IIf(celula.Value.ToString = Nothing, "", celula.Value.ToString)
                                'se o texto informado estiver contido na célula então seleciona toda linha
                                If texto.Contains(txtBuscaNome.Text) Then
                                    'seleciona a linha
                                    grdListaTelefonica.CurrentCell = celula
                                    grdListaTelefonica.CurrentCell = grdListaTelefonica.Rows(celula.RowIndex).Cells(2)
                                    Exit Sub
                                End If
                            End If
                            contador = contador + 1
                        End If
                    End If

                Next

            Next

        End If

    End Sub

    Private Sub txtTelefone_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtBuscaTelefone.TextChanged
        Dim texto As String = Nothing
        Dim linhaVazia As Integer = 0
        Dim contador As Integer = 0

        If txtBuscaTelefone.Text <> String.Empty Then

            linhaVazia = grdListaTelefonica.Rows.Count - 1

            'percorre cada linha do DataGridView
            For Each linha As DataGridViewRow In grdListaTelefonica.Rows

                'percorre cada célula da linha
                For Each celula As DataGridViewCell In grdListaTelefonica.Rows(linha.Index).Cells
                    If Not contador = linhaVazia Then
                        'se a coluna for a coluna 1 (telefone) então verifica o criterio
                        If celula.ColumnIndex = 1 Then

                            If celula.Value.ToString <> "" Then
                                texto = IIf(celula.Value.ToString = Nothing, "", celula.Value.ToString)
                                'se o texto informado estiver contido na célula então seleciona toda linha
                                If texto.Contains(txtBuscaTelefone.Text) Then
                                    'seleciona a linha
                                    grdListaTelefonica.CurrentCell = celula
                                    grdListaTelefonica.CurrentCell = grdListaTelefonica.Rows(celula.RowIndex).Cells(2)
                                    Exit Sub
                                End If
                            End If
                            contador = contador + 1
                        End If
                    End If

                Next

            Next

        End If
    End Sub

    Private Sub grdListaTelefonica_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdListaTelefonica.CellContentClick

        Dim DDDSelecionado As String = ""
        Dim TelefoneSelecionado As String = ""
        Dim rn As RNGenerico = New RNGenerico
        Dim lista As CListaTelefonica = New CListaTelefonica
        Dim ds As DataSet

        Try

            DDDSelecionado = CType(grdListaTelefonica.Rows(e.RowIndex).Cells(ColunasGridHistorico.DDD).Value, String)
            TelefoneSelecionado = CType(grdListaTelefonica.Rows(e.RowIndex).Cells(ColunasGridHistorico.Telefone).Value, String)



            For Each ContribuinteInconsistencia As CListaTelefonica In lstListaTelefonica
                If ContribuinteInconsistencia.Telefone = TelefoneSelecionado Then

                End If
            Next


            If (e.RowIndex > -1) Then

                lstListaTelefonica = New SortableBindingList(Of CListaTelefonica)(rn.Retorna_HistoricoSimplificado(TelefoneSelecionado, DDDSelecionado))

                If (lstListaTelefonica.Count > 0) Then

                    bindingSourceContribuite.DataSource = lstListaTelefonica

                    GrdHistorico.DataSource = bindingSourceContribuite.DataSource

                Else

                    GrdHistorico.DataSource = Nothing

                End If


                ds = rn.Busca_Informações_Lista_Telefonica(TelefoneSelecionado, DDDSelecionado)

                If ds.Tables(0).Rows.Count > 0 Then
                    For Each linha As DataRow In ds.Tables(0).Rows
                        txtCod_Ean.Text = IIf(IsDBNull(linha("cod_ean")), "", linha("cod_ean"))
                        txtNome.Text = IIf(IsDBNull(linha("nome_cliente")), "", linha("nome_cliente"))
                        txtDDD.Text = IIf(IsDBNull(linha("DDD")), "", linha("DDD"))
                        txtTelefone.Text = IIf(IsDBNull(linha("telefone")), "", linha("telefone"))
                        txtValor.Text = IIf(IsDBNull(linha("valor")), "", linha("valor"))


                    Next

                End If


            End If


            contribuinte = rnContribuinte.PesquisarContribuintePorTelefoneSimplificado(telefone, DDDSelecionado, 1)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub btnSalvar_Click(sender As System.Object, e As System.EventArgs) Handles btnSalvar.Click
        Dim rn As RNGenerico = New RNGenerico
        Dim lista As CListaTelefonica = New CListaTelefonica

        Try

            'ATRIBUI OS VALORES AO OBJETO
            lista.Cod_cliente = txtCod_Ean.Text
            lista.Nome_cliente = txtNome.Text
            lista.DDD = txtDDD.Text
            lista.Telefone = txtTelefone.Text
            lista.Valor = txtValor.Text
            lista.Categoria = cboCategoria.SelectedValue
            lista.Cod_cidade = cboCidade.SelectedValue


            If (rn.Atualiza_Lista_Telefonica(lista)) = True Then
                MsgBox("Informações Atualizadas com sucesso!")
                CarregaTelefoneRecuperacao()
            Else
                MsgBox("Ocorreu um erro ao atualizar as informações")
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub


End Class