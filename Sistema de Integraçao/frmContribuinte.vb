﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001

Public Class frmContribuinte

    Dim v_ambiente As String
    Dim rnContribuinte As RNContribuinte = New RNContribuinte
    Dim contribuinte As CContribuinte = New CContribuinte


#Region "Variáveis"

    Private MessageHelper As Util.MessageHelper = New Util.MessageHelper

    Private mCod_Ean_Cliente As String
    Private mCod_cliente As Integer
    Private mNomeCliente As String
    Private mTelefone As String
    Private mSelecionado As List(Of CRetorno)
    Private mFormNome As String
    Private mUserName As String
    Private mMesRef As String
    Private mCodCategoria As Integer
    Private mOperador As Integer
    Private mValor As String


#End Region

#Region "Propriedades"

    Public Property Cod_Ean_Cliente() As String
        Get
            Return mCod_Ean_Cliente
        End Get
        Set(ByVal value As String)
            mCod_Ean_Cliente = value
        End Set
    End Property

    Public Property Cod_Cliente() As Integer
        Get
            Return mCod_cliente
        End Get
        Set(ByVal value As Integer)
            mCod_cliente = value
        End Set
    End Property

    Public Property Telefone() As String
        Get
            Return mTelefone
        End Get
        Set(ByVal value As String)
            mTelefone = value
        End Set
    End Property

    Public Property NomeCliente() As String
        Get
            Return mNomeCliente
        End Get
        Set(ByVal value As String)
            mNomeCliente = value
        End Set
    End Property

    Public Property Categoria() As Integer
        Get
            Return mCodCategoria
        End Get
        Set(ByVal value As Integer)
            mCodCategoria = value
        End Set
    End Property

    Public Property Operador() As Integer
        Get
            Return mOperador
        End Get
        Set(ByVal value As Integer)
            mOperador = value
        End Set
    End Property

    Public Property Valor() As String
        Get
            Return mValor
        End Get
        Set(ByVal value As String)
            mValor = value
        End Set
    End Property


    Public Property MesRef() As String
        Get
            Return mMesRef
        End Get
        Set(ByVal value As String)
            mMesRef = value
        End Set
    End Property

    Public Property FormNome() As String
        Get
            Return mFormNome
        End Get
        Set(ByVal value As String)
            mFormNome = value
        End Set
    End Property

    Public Property Selecionado() As List(Of CRetorno)
        Get
            Return mSelecionado
        End Get
        Set(ByVal value As List(Of CRetorno))
            mSelecionado = value
        End Set
    End Property

    Public Property UserName() As String
        Get
            Return mUserName
        End Get
        Set(ByVal value As String)
            mUserName = value
        End Set
    End Property

    Public Property CodCategoria() As String
        Get
            Return mCodCategoria
        End Get
        Set(ByVal value As String)
            mCodCategoria = value
        End Set
    End Property


#End Region

    Private Sub frmContribuinte_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim cod_cliente As Integer = 0
        Dim contribuinteCarregado As CContribuinte = New CContribuinte
        Dim objContribuinte As CContribuinte = New CContribuinte

        Try

        
        instanciaVariaviesGlobais()
            If Not IsNothing(Selecionado) Then



                If Selecionado.Count > 0 Then

                    contribuinteCarregado = rnContribuinte.ConsultaContribuinteOI(Selecionado.Item(0).Cod_ean_cliente)

                    If Not contribuinteCarregado Is Nothing Then

                        cod_cliente = contribuinteCarregado.CodCliente

                        If cboNome.SelectedIndex <> 0 Then


                            PesquisaContribuinte(cod_cliente)

                            CarregaComboCidades(contribuinteCarregado.Cod_Cidade)
                            CarregaComboCategorias(contribuinteCarregado.Cod_Categoria)
                            If CodCategoria <> 0 Then
                                CarregaComboCategorias(CodCategoria)
                            End If

                            CarregaComboOperador(contribuinteCarregado.Cod_Operador)
                            CarregaComboEspecie(contribuinteCarregado.Cod_Especie)
                            CarregaComboGrupo(contribuinteCarregado.Cod_grupo)
                            CarregaComboBairros(contribuinteCarregado.Cod_Bairro)
                            CarregaSituacaoOi(contribuinteCarregado.Telefone1)


                        End If
                    Else
                        txtNome.Text = NomeCliente
                        txtNome2.Text = NomeCliente
                        txtTelefone1.Text = Telefone
                        txtTelefone2.Text = Telefone
                        txtValDoacao.Text = Valor
                        CarregaComboOperador(Operador)
                        CarregaComboCategorias(Categoria)

                        ' MsgBox("Não foi possível retornar as informações sobre o contribuinte, verificar o cadastro")


                    End If
                Else

                    CarregaComboCidades(0)
                    CarregaComboBairros(0)
                    CarregaComboCategorias(0)
                    CarregaComboOperador(0)
                    CarregaComboEspecie(0)
                    CarregaComboGrupo(0)
                    CarregaComboContribuinte()

                End If


            Else

                CarregaComboCidades(0)
                CarregaComboBairros(0)
                CarregaComboCategorias(0)
                CarregaComboOperador(0)
                CarregaComboEspecie(0)
                CarregaComboGrupo(0)
                CarregaComboContribuinte()

            End If
        Catch ex As Exception

        End Try


    End Sub


    Private Sub instanciaVariaviesGlobais()

        Dim mensagem As New Util.ConnectionHelper(System.Reflection.Assembly.GetExecutingAssembly)
        Dim v_ambiente As String

        v_ambiente = mensagem.RetornaMenssage()

    End Sub

    Private Sub CarregaComboCidades(ByVal cod_cidade As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

        cboCidade.DataSource = Nothing
        cboCidade.Items.Clear()
        cboCidade.DisplayMember = "Nome_cidade"
        cboCidade.ValueMember = "Cod_Cidade"
            cboCidade.DataSource = rn_generico.BuscarCidades(0)

        If cboCidade.Items.Count = 0 Then
            cboCidade.SelectedIndex = 0
            ElseIf cboCidade.Items.Count > 0 And cod_cidade <> 0 Then
              
                For i As Integer = 0 To cboCidade.Items.Count - 1
                    For Each linha As CGenerico In cboCidade.DataSource
                        If linha.Cod_Cidade = cod_cidade Then
                            cboCidade.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next

        Else

                cboCidade.SelectedIndex = 0
        End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CarregaComboBairros(ByVal cod_bairro As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try



            cboBairro.DataSource = Nothing
            cboBairro.Items.Clear()
            cboBairro.DisplayMember = "Nome_Bairro"
            cboBairro.ValueMember = "Cod_Bairro"
            cboBairro.DataSource = rn_generico.BuscarBairros(0)

            If cboBairro.Items.Count > 0 And cod_bairro = 0 Then
                cboBairro.SelectedIndex = 0

            ElseIf cboBairro.Items.Count > 0 And cod_bairro <> 0 Then
                For i As Integer = 0 To cboBairro.Items.Count - 1
                    For Each linha As CGenerico In cboBairro.DataSource
                        If linha.Cod_Bairro = cod_bairro Then
                            cboBairro.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboBairro.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboCategorias(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCategoria.DataSource = Nothing
            cboCategoria.Items.Clear()
            cboCategoria.DisplayMember = "Nome_Categoria"
            cboCategoria.ValueMember = "Cod_Categoria"
            cboCategoria.DataSource = rn_generico.BuscarCategoria(0)

            If cboCategoria.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoria.SelectedIndex = 0
            ElseIf cboCategoria.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoria.Items.Count - 1
                    For Each linha As CGenerico In cboCategoria.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoria.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoria.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboOperador(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboOperador.DataSource = Nothing
            cboOperador.Items.Clear()
            cboOperador.DisplayMember = "Nome_Operador"
            cboOperador.ValueMember = "Cod_Operador"
            cboOperador.DataSource = rn_generico.BuscarOperador(0)

            If cboOperador.Items.Count > 0 And cod_operador = 0 Then
                cboOperador.SelectedIndex = 0
            ElseIf cboOperador.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperador.Items.Count - 1
                    For Each linha As CGenerico In cboOperador.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperador.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperador.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboEspecie(ByVal cod_especie As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try


            cboEspecie.DataSource = Nothing
            cboEspecie.Items.Clear()
            cboEspecie.DisplayMember = "Nome_Especie"
            cboEspecie.ValueMember = "Cod_Especie"
            cboEspecie.DataSource = rn_generico.BuscarEspecie(0)

            If cboEspecie.Items.Count > 0 And cod_especie = 0 Then
                cboEspecie.SelectedIndex = 0
            ElseIf cboEspecie.Items.Count > 0 And cod_especie <> 0 Then
                For i As Integer = 0 To cboEspecie.Items.Count - 1
                    For Each linha As CGenerico In cboEspecie.DataSource
                        If linha.Cod_Especie = cod_especie Then
                            cboEspecie.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboEspecie.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try



    End Sub

    Private Sub CarregaComboGrupo(ByVal cod_grupo As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboGrupo.DataSource = Nothing
            cboGrupo.Items.Clear()
            cboGrupo.DisplayMember = "Nome_Grupo"
            cboGrupo.ValueMember = "Cod_Grupo"
            cboGrupo.DataSource = rn_generico.BuscarGrupo(0)

            If cboGrupo.Items.Count > 0 And cod_grupo = 0 Then
                cboGrupo.SelectedIndex = 0
            ElseIf cboGrupo.Items.Count > 0 And cod_grupo <> 0 Then
                For i As Integer = 0 To cboGrupo.Items.Count - 1
                    For Each linha As CGenerico In cboGrupo.DataSource
                        If linha.Cod_Grupo = cod_grupo Then
                            cboGrupo.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboGrupo.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboContribuinte()
        Dim rn_contribuinte As RNContribuinte = New RNContribuinte
        Dim filtro As CContribuinte = New CContribuinte

        cboNome.DataSource = Nothing
        cboNome.Items.Clear()
        cboNome.DisplayMember = "NomeCliente1"
        cboNome.ValueMember = "Cod_cliente"
        cboNome.DataSource = rn_contribuinte.CarregarGrid(filtro, "Contribuinte")

        If cboNome.Items.Count > 0 Then
            cboNome.SelectedIndex = 0
        End If


    End Sub

    Private Sub CarregaSituacaoOi(ByVal telefone As String)
        Dim rn_retorno As RNRetorno = New RNRetorno


        lblDataSituacao.Text = rn_retorno.RetornaUltimaDataSituacao(telefone)
        lblUltimaSituacao.Text = rn_retorno.RetornaUltimaSituacao(telefone)



    End Sub






    Private Sub PesquisaContribuinte(ByVal cod_cliente)

        Try

            contribuinte = rnContribuinte.BuscarContribuinte(cod_cliente)


            'popula a tela
            txtCodigo.Text = contribuinte.CodCliente
            txtNome.Text = contribuinte.NomeCliente1
            txtNome2.Text = contribuinte.NomeCliente2
            txtCodBarra.Text = contribuinte.Cod_EAN_Cliente
            txtCnpjCpf.Text = contribuinte.CNPJ_CPF
            txtRg.Text = contribuinte.IE_CI
            txtEmail.Text = contribuinte.Email
            txtTelefone1.Text = contribuinte.Telefone1
            txtTelefone2.Text = contribuinte.Telefone2
            txtEndereco.Text = contribuinte.Endereco
            txtUF.Text = contribuinte.UF
            txtCEP.Text = contribuinte.CEP

            txtRegiao.Text = contribuinte.Cod_Regiao

            txtDtNc.Text = contribuinte.DataNascimento
            txtDtNcMp.Text = contribuinte.DT_NC_MP
            txtDtCadastro.Text = contribuinte.DataCadastro
            txtReferencia.Text = contribuinte.Referencia
            txtDtReajuste.Text = contribuinte.DT_Reajuste

            txtObs.Text = contribuinte.Observacao

            txtMelhorDia.Text = contribuinte.DiaLimite
            txtValDoacao.Text = Convert.ToDouble(contribuinte.Valor).ToString("R$ #,###.00")
            txtDDD.Text = contribuinte.DDD


            If contribuinte.Descontinuado = "S" Then
                chkInativo.Checked = True
            Else
                chkInativo.Checked = False
            End If

            If contribuinte.Nao_pedir_extra = "S" Then
                chkNaoPedirExtra.Checked = True
            Else
                chkNaoPedirExtra.Checked = False
            End If

            If contribuinte.Nao_Pedir_Mensal = "S" Then
                chkNaoPedirMensal.Checked = True
            Else
                chkNaoPedirMensal.Checked = False
            End If

            If contribuinte.Tel_desligado = "S" Then
                chkTelDesl.Checked = True
            Else
                chkTelDesl.Checked = False
            End If

            If contribuinte.ClienteOi = "S" Then
                chkClienteOi.Checked = True
            Else
                chkClienteOi.Checked = False
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub


    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Dim isEnviado As Boolean = False
        Dim dataRemessa As String = ""
        Dim dataServico As String = ""
        Dim rnRetorno As RNRetorno = New RNRetorno

        Try



            If txtTel1.Text <> "" Then
                contribuinte = rnContribuinte.PesquisarContribuintePorTelefoneSimplificado(txtTel1.Text, txtDDDPesquisa.Text, 1)
            ElseIf (txtTel2.Text <> "") Then
                contribuinte = rnContribuinte.PesquisarContribuintePorTelefoneSimplificado(txtTel2.Text, txtDDDPesquisa.Text, 2)
            ElseIf (txtCB.Text <> "") Then
                contribuinte = rnContribuinte.PesquisarPorCodBarra(txtCB.Text)
            Else
                contribuinte = rnContribuinte.BuscarContribuinte(1)
            End If

            If contribuinte.Telefone1 = "" Then
                contribuinte = rnContribuinte.PesquisarTabelaRetidoPorTelefoneSimplificado(txtTel1.Text, txtDDDPesquisa.Text)
            End If

            If Not contribuinte.Telefone1 Is Nothing Then
                'popula a tela
                txtCodigo.Text = contribuinte.CodCliente
                txtNome.Text = contribuinte.NomeCliente1
                txtNome2.Text = contribuinte.NomeCliente2
                txtCodBarra.Text = contribuinte.Cod_EAN_Cliente
                txtCnpjCpf.Text = contribuinte.CNPJ_CPF
                txtRg.Text = contribuinte.IE_CI
                txtEmail.Text = contribuinte.Email
                txtTelefone1.Text = contribuinte.Telefone1
                txtTelefone2.Text = contribuinte.Telefone2
                txtEndereco.Text = contribuinte.Endereco
                txtUF.Text = contribuinte.UF
                txtCEP.Text = contribuinte.CEP

                txtRegiao.Text = contribuinte.Cod_Regiao

                txtDtNc.Text = contribuinte.DataNascimento
                txtDtNcMp.Text = contribuinte.DT_NC_MP
                txtDtCadastro.Text = contribuinte.DataCadastro
                txtReferencia.Text = contribuinte.Referencia
                txtDtReajuste.Text = contribuinte.DT_Reajuste

                txtObs.Text = contribuinte.Observacao

                txtMelhorDia.Text = contribuinte.DiaLimite
                txtValDoacao.Text = Convert.ToDouble(contribuinte.Valor).ToString("R$ #,###.00")
                txtDDD.Text = contribuinte.DDD


                If contribuinte.Descontinuado = "S" Then
                    chkInativo.Checked = True
                Else
                    chkInativo.Checked = False
                End If

                If contribuinte.Nao_pedir_extra = "S" Then
                    chkNaoPedirExtra.Checked = True
                Else
                    chkNaoPedirExtra.Checked = False
                End If

                If contribuinte.Nao_Pedir_Mensal = "S" Then
                    chkNaoPedirMensal.Checked = True
                Else
                    chkNaoPedirMensal.Checked = False
                End If

                If contribuinte.Tel_desligado = "S" Then
                    chkTelDesl.Checked = True
                Else
                    chkTelDesl.Checked = False
                End If

                If contribuinte.ClienteOi = "S" Then
                    chkClienteOi.Checked = True
                Else
                    chkClienteOi.Checked = False
                End If


                dataRemessa = Date.Now
                dataServico = "07" + dataRemessa.Substring(2, 8)
                dataRemessa = Replace(dataRemessa, "/", "")
                dataRemessa = dataRemessa.Substring(4, 4) + dataRemessa.Substring(2, 2)


                isEnviado = RNRetorno.ConsultaTelefoneEnviadoRemessa(txtDDD.Text, txtTel1.Text, dataRemessa)

                If isEnviado = True Then

                    MsgBox("TELEFONE ENVIADO NA REMESSA DE " & dataServico & "!", MsgBoxStyle.Exclamation)

                Else
                    MsgBox("TELEFONE NÃO ENVIADO NA REMESSA", MsgBoxStyle.Exclamation)
                End If

            Else
                MsgBox("contribuinte não encontrado!", MsgBoxStyle.Exclamation)
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub


    Private Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalvar.Click
        Dim contribuinte As CContribuinte = New CContribuinte

        Try



            contribuinte.CodCliente = txtCodigo.Text
            contribuinte.NomeCliente1 = txtNome.Text
            contribuinte.NomeCliente2 = IIf(txtNome2.Text = "", txtNome.Text, txtNome2.Text)
            contribuinte.Cod_EAN_Cliente = txtCodBarra.Text
            contribuinte.CNPJ_CPF = IIf(txtCnpjCpf.Text = "", "1", txtCnpjCpf.Text)
            contribuinte.IE_CI = IIf(txtRg.Text = "", "1", txtRg.Text)
            contribuinte.Email = IIf(txtEmail.Text = "", "1", txtEmail.Text)
            contribuinte.Telefone1 = IIf(txtTelefone1.Text = "", "1", txtTelefone1.Text)
            contribuinte.Telefone2 = IIf(txtTelefone2.Text = "", txtTelefone1.Text, txtTelefone2.Text)
            contribuinte.Endereco = IIf(txtEndereco.Text = "", "1", txtEndereco.Text)
            contribuinte.UF = IIf(txtUF.Text = "", "MG", txtUF.Text)
            contribuinte.CEP = IIf(txtCEP.Text = "", "1", txtCEP.Text)

            If cboCidade.SelectedValue <= 0 Then
                contribuinte.Cod_Cidade = 1
            Else
                contribuinte.Cod_Cidade = cboCidade.SelectedValue
            End If

            If cboCidade.Text = "Selecione..." Then
                contribuinte.Cidade = "JUIZ DE FORA"
            Else
                contribuinte.Cidade = cboCidade.Text
            End If

            If cboBairro.SelectedValue <= 0 Then
                contribuinte.Cod_Bairro = 1
            Else
                contribuinte.Cod_Bairro = cboBairro.SelectedValue
            End If

            If cboBairro.Text = "Selecione..." Then
                contribuinte.Bairro = "CENTRO"
            Else
                contribuinte.Bairro = cboBairro.Text
            End If


            If cboOperador.SelectedValue <= 0 Then
                contribuinte.Cod_Operador = 1
            Else
                contribuinte.Cod_Operador = cboOperador.SelectedValue
            End If

            If cboOperador.Text = "Selecione..." Then
                contribuinte.Operador = "INSTITUIÇÃO"
            Else
                contribuinte.Operador = cboOperador.Text
            End If


            If cboOperador.SelectedValue <= 0 Then
                contribuinte.Cod_Usuario = 1
            Else
                contribuinte.Cod_Usuario = cboOperador.SelectedValue
            End If


            If cboCategoria.SelectedValue <= 0 Then
                contribuinte.Cod_Categoria = 3
            Else
                contribuinte.Cod_Categoria = cboCategoria.SelectedValue
            End If

            If cboEspecie.SelectedValue <= 0 Then
                contribuinte.Cod_Especie = 1
            Else
                contribuinte.Cod_Especie = cboEspecie.SelectedValue
            End If


            contribuinte.Cod_empresa = 1

            contribuinte.Cod_Regiao = IIf(txtRegiao.Text = "", 0, txtRegiao.Text)

            contribuinte.DataNascimento = IIf(txtDtNc.Text = "", "01/01/1900", txtDtNc.Text)
            contribuinte.DT_NC_MP = IIf(txtDtNcMp.Text = "", "01/01/1900", txtDtNcMp.Text)
            contribuinte.DataCadastro = IIf(txtDtCadastro.Text = "", "01/01/1900", txtDtCadastro.Text)
            contribuinte.Referencia = IIf(txtReferencia.Text = "", "1", txtReferencia.Text)
            contribuinte.DT_Reajuste = IIf(txtDtReajuste.Text = "", "01/01/1900", txtDtReajuste.Text)

            contribuinte.Observacao = IIf(txtObs.Text = "", "1", txtObs.Text)

            If txtMelhorDia.Text = "" Then
                contribuinte.DiaLimite = 1
            Else
                contribuinte.DiaLimite = txtMelhorDia.Text
            End If

            If txtValDoacao.Text = "" Then
                contribuinte.Valor = 10
            Else
                contribuinte.Valor = txtValDoacao.Text
            End If

            contribuinte.DDD = IIf(txtDDD.Text = "", "32", txtDDD.Text)

            If cboGrupo.SelectedValue <= 0 Then
                cboGrupo.SelectedValue = 1
            Else
                contribuinte.Cod_grupo = cboGrupo.SelectedValue
            End If

            If chkTelDesl.Checked = True Then
                contribuinte.Tel_desligado = "S"
            Else
                contribuinte.Tel_desligado = "N"
            End If

            If chkInativo.Checked = True Then
                contribuinte.Descontinuado = "S"
            Else
                contribuinte.Descontinuado = "N"
            End If

            If chkNaoPedirExtra.Checked = True Then
                contribuinte.Nao_pedir_extra = "S"
            Else
                contribuinte.Nao_pedir_extra = "N"
            End If

            If chkNaoPedirMensal.Checked = True Then
                contribuinte.Nao_Pedir_Mensal = "S"
            Else
                contribuinte.Nao_Pedir_Mensal = "N"
            End If

            If chkClienteOi.Checked = True Then
                contribuinte.ClienteOi = "S"
            Else
                contribuinte.ClienteOi = "N"
            End If

            If rnContribuinte.VerificaExistenciaContribuinte(contribuinte) = True Then
                rnContribuinte.AtualizaContribuinte(contribuinte)
                MsgBox("Dados Salvos com sucesso!")
            Else
                rnContribuinte.Salvar(contribuinte)
                MsgBox("Dados Salvos com sucesso!")
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub cboCidade_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCidade.SelectedIndexChanged

        If Not (cboCidade.SelectedIndex) = 0 Then
            If Not (cboCidade.SelectedIndex) = -1 Then
                CarregaComboBairros(cboCidade.SelectedValue)
                'txtUF.Text = cboCidade.SelectedValue(0)
            Else
                ' cboBairro.Enabled = False
            End If
        End If

    End Sub


    Private Sub cboNome_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboNome.SelectedValueChanged
        Dim objContribuinte As CContribuinte = New CContribuinte
        Try



            Dim cod_cliente As Integer = 0
            objContribuinte = Me.cboNome.SelectedValue

            If Not IsNothing(objContribuinte) Then
                cod_cliente = objContribuinte.CodCliente

                If cboNome.SelectedIndex <> 0 Then


                    PesquisaContribuinte(cod_cliente)

                    CarregaComboCidades(objContribuinte.Cod_Cidade)
                    CarregaComboCategorias(objContribuinte.Cod_Categoria)
                    CarregaComboOperador(objContribuinte.Cod_Operador)
                    CarregaComboEspecie(objContribuinte.Cod_Especie)
                    CarregaComboGrupo(objContribuinte.Cod_grupo)
                    CarregaComboBairros(objContribuinte.Cod_Bairro)
                    CarregaSituacaoOi(objContribuinte.Telefone1)


                End If
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Close()
    End Sub

    Private Sub btnNovo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNovo.Click

        limparTela()

        Dim new_cod As String
        new_cod = rnContribuinte.RetornaNovoCodigo(True)
        txtCodigo.Text = new_cod
        new_cod = "*" + new_cod + "-191*"
        txtCodBarra.Text = new_cod

       
        txtCnpjCpf.Text = "1"
        txtRg.Text = "1"
        txtEmail.Text = "1"
        txtTelefone1.Text = ""
        txtTelefone2.Text = ""
        txtEndereco.Text = "1"
        txtUF.Text = "MG"
        txtCEP.Text = "36000-100"
        txtRegiao.Text = "99"
        txtDtNc.Text = "11-11-1911"
        txtDtNcMp.Text = "11-11-1911"
        txtDtCadastro.Text = "11-11-1911"
        txtReferencia.Text = "1"
        txtDtReajuste.Text = "11-11-1911"
        txtObs.Text = "1"
        txtMelhorDia.Text = "7"
        txtValDoacao.Text = "R$10,00"
        txtDDD.Text = ""



        CarregaComboCidades(0)
        CarregaComboBairros(0)
        CarregaComboCategorias(0)
        CarregaComboOperador(0)
        CarregaComboEspecie(0)
        CarregaComboGrupo(0)
        CarregaComboContribuinte()
    End Sub

    Private Sub btnRemessa_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemessa.Click
        FrmGerarRemessa.Show()
    End Sub

    Private Sub BtnConfirmar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirmar.Click
        Dim frm As New FrmInclusaoManual

        Try

            frm.pTelefone = txtTelefone1.Text
            frm.pCod_ean = txtCodBarra.Text
            frm.pMesAnoRef = MesRef
            frm.psituacao = "S"
            frm.pCodUsuario = UserName
            frm.CodCategoria = CodCategoria

            Me.Visible = False

            frm.Show()



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnRecusar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRecusar.Click
        Dim frm As New FrmInclusaoManual

        Try

            frm.pTelefone = txtTelefone1.Text
            frm.pCod_ean = txtCodBarra.Text
            frm.pMesAnoRef = MesRef
            frm.psituacao = "N"
            frm.pCodUsuario = UserName
            frm.CodCategoria = CodCategoria

            Me.Visible = False

            frm.Show()

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnExcluir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcluir.Click
        ' Dim contribuinte As CContribuinte = New CContribuinte

        Try
            contribuinte.CodCliente = txtCodigo.Text
            If contribuinte.Telefone1 = "" Then
                contribuinte.Telefone1 = txtTel1.Text
                contribuinte.Telefone2 = txtTel2.Text
                contribuinte.DDD = txtDDD.Text
            End If

            Dim result = MessageBox.Show("Deseja realmente excluir o Contribuinte, Deseja continuar?", "Atenção", _
                               MessageBoxButtons.YesNo, _
                               MessageBoxIcon.Question)

         

            If (result = DialogResult.Yes) Then
                If rnContribuinte.VerificaExistenciaContribuinte(contribuinte) = True Then
                    rnContribuinte.Excluir(contribuinte.Cod_EAN_Cliente, contribuinte.DDD, contribuinte.Telefone1, "", "", "")
                    MsgBox("Contribuinte excluido com sucesso!")
                Else
                    Dim result2 = MessageBox.Show("Não foi encontrado o contribuinte pelo paramêtro informado, deseja inclui-lo na lista de rejeição?", "Atenção", _
                            MessageBoxButtons.YesNo, _
                            MessageBoxIcon.Question)

                    If (result2 = DialogResult.Yes) Then
                        rnContribuinte.Excluir(contribuinte.Cod_EAN_Cliente, contribuinte.DDD, contribuinte.Telefone1, "", "", "")
                        MsgBox("Contribuinte excluido com sucesso!")
                    Else
                        MsgBox("Não foi possivel encontrar o contribuinte selecionado")
                    End If
                End If

            End If

            limparTela()

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnAlterar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAlterar.Click
        Dim contribuinte As CContribuinte = New CContribuinte

        contribuinte.CodCliente = txtCodigo.Text
        contribuinte.NomeCliente1 = txtNome.Text
        contribuinte.NomeCliente2 = IIf(txtNome2.Text = "", txtNome.Text, txtNome2.Text)
        contribuinte.Cod_EAN_Cliente = txtCodBarra.Text
        contribuinte.CNPJ_CPF = IIf(txtCnpjCpf.Text = "", "1", txtCnpjCpf.Text)
        contribuinte.IE_CI = IIf(txtRg.Text = "", "1", txtRg.Text)
        contribuinte.Email = IIf(txtEmail.Text = "", "1", txtEmail.Text)
        contribuinte.Telefone1 = IIf(txtTelefone1.Text = "", "1", txtTelefone1.Text)
        contribuinte.Telefone2 = IIf(txtTelefone2.Text = "", txtTelefone1.Text, txtTelefone2.Text)
        contribuinte.Endereco = IIf(txtEndereco.Text = "", "1", txtEndereco.Text)
        contribuinte.UF = IIf(txtUF.Text = "", "MG", txtUF.Text)
        contribuinte.CEP = IIf(txtCEP.Text = "", "1", txtCEP.Text)

        If cboCidade.SelectedValue <= 0 Then
            contribuinte.Cod_Cidade = 1
        Else
            contribuinte.Cod_Cidade = cboCidade.SelectedValue
        End If

        If cboCidade.Text = "Selecione..." Then
            contribuinte.Cidade = "JUIZ DE FORA"
        Else
            contribuinte.Cidade = cboCidade.Text
        End If

        If cboBairro.SelectedValue <= 0 Then
            contribuinte.Cod_Bairro = 1
        Else
            contribuinte.Cod_Bairro = cboBairro.SelectedValue
        End If

        If cboBairro.Text = "Selecione..." Then
            contribuinte.Bairro = "CENTRO"
        Else
            contribuinte.Bairro = cboBairro.Text
        End If


        If cboOperador.SelectedValue <= 0 Then
            contribuinte.Cod_Operador = 1
        Else
            contribuinte.Cod_Operador = cboOperador.SelectedValue
        End If

        If cboOperador.Text = "Selecione..." Then
            contribuinte.Operador = "INSTITUIÇÃO"
        Else
            contribuinte.Operador = cboOperador.Text
        End If


        If cboOperador.SelectedValue <= 0 Then
            contribuinte.Cod_Usuario = 1
        Else
            contribuinte.Cod_Usuario = cboOperador.SelectedValue
        End If


        If cboCategoria.SelectedValue <= 0 Then
            contribuinte.Cod_Categoria = 3
        Else
            contribuinte.Cod_Categoria = cboCategoria.SelectedValue
        End If

        If cboEspecie.SelectedValue <= 0 Then
            contribuinte.Cod_Especie = 1
        Else
            contribuinte.Cod_Especie = cboEspecie.SelectedValue
        End If


        contribuinte.Cod_empresa = 1

        contribuinte.Cod_Regiao = IIf(txtRegiao.Text = "", 0, txtRegiao.Text)

        contribuinte.DataNascimento = IIf(txtDtNc.Text = "", "01/01/1900", txtDtNc.Text)
        contribuinte.DT_NC_MP = IIf(txtDtNcMp.Text = "", "01/01/1900", txtDtNcMp.Text)
        contribuinte.DataCadastro = IIf(txtDtCadastro.Text = "", "01/01/1900", txtDtCadastro.Text)
        contribuinte.Referencia = txtReferencia.Text
        contribuinte.DT_Reajuste = IIf(txtDtReajuste.Text = "", "01/01/1900", txtDtReajuste.Text)

        contribuinte.Observacao = txtObs.Text

        If txtMelhorDia.Text = "" Then
            contribuinte.DiaLimite = 1
        Else
            contribuinte.DiaLimite = txtMelhorDia.Text
        End If

        If txtValDoacao.Text = "" Then
            contribuinte.Valor = 10
        Else
            contribuinte.Valor = txtValDoacao.Text
        End If

        contribuinte.DDD = txtDDD.Text

        If cboGrupo.SelectedValue <= 0 Then
            cboGrupo.SelectedValue = 1
        Else
            contribuinte.Cod_grupo = cboGrupo.SelectedValue
        End If

        If chkTelDesl.Checked = True Then
            contribuinte.Tel_desligado = "S"
        Else
            contribuinte.Tel_desligado = "N"
        End If

        If chkInativo.Checked = True Then
            contribuinte.Descontinuado = "S"
        Else
            contribuinte.Descontinuado = "N"
        End If

        If chkNaoPedirExtra.Checked = True Then
            contribuinte.Nao_pedir_extra = "S"
        Else
            contribuinte.Nao_pedir_extra = "N"
        End If

        If chkNaoPedirMensal.Checked = True Then
            contribuinte.Nao_Pedir_Mensal = "S"
        Else
            contribuinte.Nao_Pedir_Mensal = "N"
        End If

        If chkClienteOi.Checked = True Then
            contribuinte.ClienteOi = "S"
        Else
            contribuinte.ClienteOi = "N"
        End If

        If rnContribuinte.VerificaExistenciaContribuinte(contribuinte) = True Then
            If rnContribuinte.AtualizaContribuinte(contribuinte) = True Then
                MsgBox("Dados Salvos com sucesso!")
            Else
                MsgBox("Ocorreu um erro ao realizar a operação, tente novamente!")
            End If
        Else
            If rnContribuinte.Salvar(contribuinte) = True Then
                MsgBox("Dados Salvos com sucesso!")
            Else
                MsgBox("Ocorreu um erro ao realizar a operação, tente novamente!")
            End If
        End If
    End Sub

    Public Sub limparTela()
        txtCodigo.Text = ""
        txtNome.Text = ""
        txtNome2.Text = ""
        txtCodBarra.Text = ""
        txtCnpjCpf.Text = ""
        txtRg.Text = ""
        txtEmail.Text = ""
        txtTelefone1.Text = ""
        txtTelefone2.Text = ""
        txtEndereco.Text = ""
        txtUF.Text = ""
        txtCEP.Text = ""
        txtRegiao.Text = ""
        txtDtNc.Text = ""
        txtDtNcMp.Text = ""
        txtDtCadastro.Text = ""
        txtReferencia.Text = ""
        txtDtReajuste.Text = ""
        txtObs.Text = ""
        txtMelhorDia.Text = ""
        txtValDoacao.Text = ""
        txtDDD.Text = ""
    End Sub
End Class