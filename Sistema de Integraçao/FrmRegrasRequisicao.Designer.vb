﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRegrasRequisicao
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Dados = New System.Windows.Forms.GroupBox()
        Me.rbCategoria = New System.Windows.Forms.RadioButton()
        Me.rbOperadora = New System.Windows.Forms.RadioButton()
        Me.cboCategoria = New System.Windows.Forms.ComboBox()
        Me.lblCategoria = New System.Windows.Forms.Label()
        Me.cboOperador = New System.Windows.Forms.ComboBox()
        Me.lblOperadora = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkSimRecuperaRetido = New System.Windows.Forms.CheckBox()
        Me.chkSimAvulsoRetido = New System.Windows.Forms.CheckBox()
        Me.chkSimMensalRetido = New System.Windows.Forms.CheckBox()
        Me.ChkSimPedirMensal = New System.Windows.Forms.CheckBox()
        Me.chkSimListaB = New System.Windows.Forms.CheckBox()
        Me.chkSimListaNova = New System.Windows.Forms.CheckBox()
        Me.chkSimRecuperacao = New System.Windows.Forms.CheckBox()
        Me.chkSimInativo = New System.Windows.Forms.CheckBox()
        Me.chkSimAvulsoB = New System.Windows.Forms.CheckBox()
        Me.chkSimMensalPrimeira = New System.Windows.Forms.CheckBox()
        Me.chkSimAvulso = New System.Windows.Forms.CheckBox()
        Me.chkSimEspecial = New System.Windows.Forms.CheckBox()
        Me.chkSimMensal = New System.Windows.Forms.CheckBox()
        Me.chkSimLista = New System.Windows.Forms.CheckBox()
        Me.chkSimDoacao = New System.Windows.Forms.CheckBox()
        Me.btnSair = New System.Windows.Forms.Button()
        Me.btnSalvar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnGrava = New System.Windows.Forms.Button()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.txtDiasRecusa = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.txtDiasAceite = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.chkNaoRecuperaRetido = New System.Windows.Forms.CheckBox()
        Me.chkNaoAvulsoRetido = New System.Windows.Forms.CheckBox()
        Me.chkNaoMensalRetido = New System.Windows.Forms.CheckBox()
        Me.ChkNaoPedirMensal = New System.Windows.Forms.CheckBox()
        Me.chkNaoListaB = New System.Windows.Forms.CheckBox()
        Me.chkNaoListaNova = New System.Windows.Forms.CheckBox()
        Me.chkNaoRecuperacao = New System.Windows.Forms.CheckBox()
        Me.chkNaoInativo = New System.Windows.Forms.CheckBox()
        Me.chkNaoAvulsoB = New System.Windows.Forms.CheckBox()
        Me.chkNaoMensalPrimeira = New System.Windows.Forms.CheckBox()
        Me.chkNaoAvulso = New System.Windows.Forms.CheckBox()
        Me.chkNaoEspecial = New System.Windows.Forms.CheckBox()
        Me.chkNaoMensal = New System.Windows.Forms.CheckBox()
        Me.chkNaoLista = New System.Windows.Forms.CheckBox()
        Me.chkNaoDoacao = New System.Windows.Forms.CheckBox()
        Me.Dados.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Dados
        '
        Me.Dados.Controls.Add(Me.rbCategoria)
        Me.Dados.Controls.Add(Me.rbOperadora)
        Me.Dados.Controls.Add(Me.cboCategoria)
        Me.Dados.Controls.Add(Me.lblCategoria)
        Me.Dados.Controls.Add(Me.cboOperador)
        Me.Dados.Controls.Add(Me.lblOperadora)
        Me.Dados.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Dados.Location = New System.Drawing.Point(12, 12)
        Me.Dados.Name = "Dados"
        Me.Dados.Size = New System.Drawing.Size(380, 181)
        Me.Dados.TabIndex = 8
        Me.Dados.TabStop = False
        Me.Dados.Text = "Definição :"
        '
        'rbCategoria
        '
        Me.rbCategoria.AutoSize = True
        Me.rbCategoria.Location = New System.Drawing.Point(217, 27)
        Me.rbCategoria.Name = "rbCategoria"
        Me.rbCategoria.Size = New System.Drawing.Size(122, 17)
        Me.rbCategoria.TabIndex = 78
        Me.rbCategoria.Text = "Define por Categoria"
        Me.rbCategoria.UseVisualStyleBackColor = True
        '
        'rbOperadora
        '
        Me.rbOperadora.AutoSize = True
        Me.rbOperadora.Checked = True
        Me.rbOperadora.Location = New System.Drawing.Point(20, 27)
        Me.rbOperadora.Name = "rbOperadora"
        Me.rbOperadora.Size = New System.Drawing.Size(127, 17)
        Me.rbOperadora.TabIndex = 77
        Me.rbOperadora.TabStop = True
        Me.rbOperadora.Text = "Define por Operadora"
        Me.rbOperadora.UseVisualStyleBackColor = True
        '
        'cboCategoria
        '
        Me.cboCategoria.FormattingEnabled = True
        Me.cboCategoria.Location = New System.Drawing.Point(100, 97)
        Me.cboCategoria.Name = "cboCategoria"
        Me.cboCategoria.Size = New System.Drawing.Size(181, 21)
        Me.cboCategoria.TabIndex = 75
        '
        'lblCategoria
        '
        Me.lblCategoria.AutoSize = True
        Me.lblCategoria.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCategoria.Location = New System.Drawing.Point(24, 97)
        Me.lblCategoria.Name = "lblCategoria"
        Me.lblCategoria.Size = New System.Drawing.Size(70, 16)
        Me.lblCategoria.TabIndex = 76
        Me.lblCategoria.Text = "Categoria:"
        '
        'cboOperador
        '
        Me.cboOperador.FormattingEnabled = True
        Me.cboOperador.Location = New System.Drawing.Point(100, 61)
        Me.cboOperador.Name = "cboOperador"
        Me.cboOperador.Size = New System.Drawing.Size(239, 21)
        Me.cboOperador.TabIndex = 1
        '
        'lblOperadora
        '
        Me.lblOperadora.AutoSize = True
        Me.lblOperadora.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOperadora.Location = New System.Drawing.Point(17, 62)
        Me.lblOperadora.Name = "lblOperadora"
        Me.lblOperadora.Size = New System.Drawing.Size(77, 16)
        Me.lblOperadora.TabIndex = 57
        Me.lblOperadora.Text = "Operadora:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkSimRecuperaRetido)
        Me.GroupBox1.Controls.Add(Me.chkSimAvulsoRetido)
        Me.GroupBox1.Controls.Add(Me.chkSimMensalRetido)
        Me.GroupBox1.Controls.Add(Me.ChkSimPedirMensal)
        Me.GroupBox1.Controls.Add(Me.chkSimListaB)
        Me.GroupBox1.Controls.Add(Me.chkSimListaNova)
        Me.GroupBox1.Controls.Add(Me.chkSimRecuperacao)
        Me.GroupBox1.Controls.Add(Me.chkSimInativo)
        Me.GroupBox1.Controls.Add(Me.chkSimAvulsoB)
        Me.GroupBox1.Controls.Add(Me.chkSimMensalPrimeira)
        Me.GroupBox1.Controls.Add(Me.chkSimAvulso)
        Me.GroupBox1.Controls.Add(Me.chkSimEspecial)
        Me.GroupBox1.Controls.Add(Me.chkSimMensal)
        Me.GroupBox1.Controls.Add(Me.chkSimLista)
        Me.GroupBox1.Controls.Add(Me.chkSimDoacao)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox1.Location = New System.Drawing.Point(423, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(241, 380)
        Me.GroupBox1.TabIndex = 58
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Contribuiu"
        '
        'chkSimRecuperaRetido
        '
        Me.chkSimRecuperaRetido.AutoSize = True
        Me.chkSimRecuperaRetido.Location = New System.Drawing.Point(15, 350)
        Me.chkSimRecuperaRetido.Name = "chkSimRecuperaRetido"
        Me.chkSimRecuperaRetido.Size = New System.Drawing.Size(129, 17)
        Me.chkSimRecuperaRetido.TabIndex = 14
        Me.chkSimRecuperaRetido.Text = "RECUPERA RETIDO"
        Me.chkSimRecuperaRetido.UseVisualStyleBackColor = True
        '
        'chkSimAvulsoRetido
        '
        Me.chkSimAvulsoRetido.AutoSize = True
        Me.chkSimAvulsoRetido.Location = New System.Drawing.Point(15, 327)
        Me.chkSimAvulsoRetido.Name = "chkSimAvulsoRetido"
        Me.chkSimAvulsoRetido.Size = New System.Drawing.Size(113, 17)
        Me.chkSimAvulsoRetido.TabIndex = 13
        Me.chkSimAvulsoRetido.Text = "AVULSO RETIDO"
        Me.chkSimAvulsoRetido.UseVisualStyleBackColor = True
        '
        'chkSimMensalRetido
        '
        Me.chkSimMensalRetido.AutoSize = True
        Me.chkSimMensalRetido.Location = New System.Drawing.Point(15, 304)
        Me.chkSimMensalRetido.Name = "chkSimMensalRetido"
        Me.chkSimMensalRetido.Size = New System.Drawing.Size(114, 17)
        Me.chkSimMensalRetido.TabIndex = 12
        Me.chkSimMensalRetido.Text = "MENSAL RETIDO"
        Me.chkSimMensalRetido.UseVisualStyleBackColor = True
        '
        'ChkSimPedirMensal
        '
        Me.ChkSimPedirMensal.AutoSize = True
        Me.ChkSimPedirMensal.Location = New System.Drawing.Point(15, 281)
        Me.ChkSimPedirMensal.Name = "ChkSimPedirMensal"
        Me.ChkSimPedirMensal.Size = New System.Drawing.Size(106, 17)
        Me.ChkSimPedirMensal.TabIndex = 11
        Me.ChkSimPedirMensal.Text = "PEDIR MENSAL"
        Me.ChkSimPedirMensal.UseVisualStyleBackColor = True
        '
        'chkSimListaB
        '
        Me.chkSimListaB.AutoSize = True
        Me.chkSimListaB.Location = New System.Drawing.Point(15, 258)
        Me.chkSimListaB.Name = "chkSimListaB"
        Me.chkSimListaB.Size = New System.Drawing.Size(66, 17)
        Me.chkSimListaB.TabIndex = 10
        Me.chkSimListaB.Text = "LISTA B"
        Me.chkSimListaB.UseVisualStyleBackColor = True
        '
        'chkSimListaNova
        '
        Me.chkSimListaNova.AutoSize = True
        Me.chkSimListaNova.Location = New System.Drawing.Point(15, 235)
        Me.chkSimListaNova.Name = "chkSimListaNova"
        Me.chkSimListaNova.Size = New System.Drawing.Size(89, 17)
        Me.chkSimListaNova.TabIndex = 9
        Me.chkSimListaNova.Text = "LISTA NOVA"
        Me.chkSimListaNova.UseVisualStyleBackColor = True
        '
        'chkSimRecuperacao
        '
        Me.chkSimRecuperacao.AutoSize = True
        Me.chkSimRecuperacao.Location = New System.Drawing.Point(15, 212)
        Me.chkSimRecuperacao.Name = "chkSimRecuperacao"
        Me.chkSimRecuperacao.Size = New System.Drawing.Size(107, 17)
        Me.chkSimRecuperacao.TabIndex = 8
        Me.chkSimRecuperacao.Text = "RECUPERAÇÃO"
        Me.chkSimRecuperacao.UseVisualStyleBackColor = True
        '
        'chkSimInativo
        '
        Me.chkSimInativo.AutoSize = True
        Me.chkSimInativo.Location = New System.Drawing.Point(15, 189)
        Me.chkSimInativo.Name = "chkSimInativo"
        Me.chkSimInativo.Size = New System.Drawing.Size(69, 17)
        Me.chkSimInativo.TabIndex = 7
        Me.chkSimInativo.Text = "INATIVO"
        Me.chkSimInativo.UseVisualStyleBackColor = True
        '
        'chkSimAvulsoB
        '
        Me.chkSimAvulsoB.AutoSize = True
        Me.chkSimAvulsoB.Location = New System.Drawing.Point(15, 166)
        Me.chkSimAvulsoB.Name = "chkSimAvulsoB"
        Me.chkSimAvulsoB.Size = New System.Drawing.Size(79, 17)
        Me.chkSimAvulsoB.TabIndex = 6
        Me.chkSimAvulsoB.Text = "AVULSO B"
        Me.chkSimAvulsoB.UseVisualStyleBackColor = True
        '
        'chkSimMensalPrimeira
        '
        Me.chkSimMensalPrimeira.AutoSize = True
        Me.chkSimMensalPrimeira.Location = New System.Drawing.Point(15, 143)
        Me.chkSimMensalPrimeira.Name = "chkSimMensalPrimeira"
        Me.chkSimMensalPrimeira.Size = New System.Drawing.Size(125, 17)
        Me.chkSimMensalPrimeira.TabIndex = 5
        Me.chkSimMensalPrimeira.Text = "MENSAL PRIMEIRA"
        Me.chkSimMensalPrimeira.UseVisualStyleBackColor = True
        '
        'chkSimAvulso
        '
        Me.chkSimAvulso.AutoSize = True
        Me.chkSimAvulso.Location = New System.Drawing.Point(15, 120)
        Me.chkSimAvulso.Name = "chkSimAvulso"
        Me.chkSimAvulso.Size = New System.Drawing.Size(69, 17)
        Me.chkSimAvulso.TabIndex = 4
        Me.chkSimAvulso.Text = "AVULSO"
        Me.chkSimAvulso.UseVisualStyleBackColor = True
        '
        'chkSimEspecial
        '
        Me.chkSimEspecial.AutoSize = True
        Me.chkSimEspecial.Location = New System.Drawing.Point(15, 97)
        Me.chkSimEspecial.Name = "chkSimEspecial"
        Me.chkSimEspecial.Size = New System.Drawing.Size(77, 17)
        Me.chkSimEspecial.TabIndex = 3
        Me.chkSimEspecial.Text = "ESPECIAL"
        Me.chkSimEspecial.UseVisualStyleBackColor = True
        '
        'chkSimMensal
        '
        Me.chkSimMensal.AutoSize = True
        Me.chkSimMensal.Location = New System.Drawing.Point(15, 74)
        Me.chkSimMensal.Name = "chkSimMensal"
        Me.chkSimMensal.Size = New System.Drawing.Size(70, 17)
        Me.chkSimMensal.TabIndex = 2
        Me.chkSimMensal.Text = "MENSAL"
        Me.chkSimMensal.UseVisualStyleBackColor = True
        '
        'chkSimLista
        '
        Me.chkSimLista.AutoSize = True
        Me.chkSimLista.Location = New System.Drawing.Point(15, 51)
        Me.chkSimLista.Name = "chkSimLista"
        Me.chkSimLista.Size = New System.Drawing.Size(56, 17)
        Me.chkSimLista.TabIndex = 1
        Me.chkSimLista.Text = "LISTA"
        Me.chkSimLista.UseVisualStyleBackColor = True
        '
        'chkSimDoacao
        '
        Me.chkSimDoacao.AutoSize = True
        Me.chkSimDoacao.Location = New System.Drawing.Point(15, 28)
        Me.chkSimDoacao.Name = "chkSimDoacao"
        Me.chkSimDoacao.Size = New System.Drawing.Size(71, 17)
        Me.chkSimDoacao.TabIndex = 0
        Me.chkSimDoacao.Text = "DOAÇÃO"
        Me.chkSimDoacao.UseVisualStyleBackColor = True
        '
        'btnSair
        '
        Me.btnSair.Location = New System.Drawing.Point(852, 416)
        Me.btnSair.Name = "btnSair"
        Me.btnSair.Size = New System.Drawing.Size(75, 23)
        Me.btnSair.TabIndex = 62
        Me.btnSair.Text = "Sair"
        Me.btnSair.UseVisualStyleBackColor = True
        '
        'btnSalvar
        '
        Me.btnSalvar.Location = New System.Drawing.Point(742, 416)
        Me.btnSalvar.Name = "btnSalvar"
        Me.btnSalvar.Size = New System.Drawing.Size(75, 23)
        Me.btnSalvar.TabIndex = 59
        Me.btnSalvar.Text = "Salvar"
        Me.btnSalvar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnGrava)
        Me.GroupBox2.Controls.Add(Me.GroupBox5)
        Me.GroupBox2.Controls.Add(Me.GroupBox4)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox2.Location = New System.Drawing.Point(12, 199)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(380, 157)
        Me.GroupBox2.TabIndex = 59
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Especial"
        '
        'btnGrava
        '
        Me.btnGrava.Location = New System.Drawing.Point(234, 123)
        Me.btnGrava.Name = "btnGrava"
        Me.btnGrava.Size = New System.Drawing.Size(105, 23)
        Me.btnGrava.TabIndex = 60
        Me.btnGrava.Text = "Grava Regra"
        Me.btnGrava.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.txtDiasRecusa)
        Me.GroupBox5.Controls.Add(Me.Label3)
        Me.GroupBox5.Location = New System.Drawing.Point(9, 98)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(191, 56)
        Me.GroupBox5.TabIndex = 4
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Não Contribuiu"
        '
        'txtDiasRecusa
        '
        Me.txtDiasRecusa.Location = New System.Drawing.Point(110, 22)
        Me.txtDiasRecusa.Name = "txtDiasRecusa"
        Me.txtDiasRecusa.Size = New System.Drawing.Size(59, 20)
        Me.txtDiasRecusa.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(15, 25)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(95, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Intervalo em Dias :"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.txtDiasAceite)
        Me.GroupBox4.Controls.Add(Me.Label2)
        Me.GroupBox4.Location = New System.Drawing.Point(9, 36)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(191, 56)
        Me.GroupBox4.TabIndex = 3
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Contribuiu"
        '
        'txtDiasAceite
        '
        Me.txtDiasAceite.Location = New System.Drawing.Point(110, 22)
        Me.txtDiasAceite.Name = "txtDiasAceite"
        Me.txtDiasAceite.Size = New System.Drawing.Size(59, 20)
        Me.txtDiasAceite.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(15, 25)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(95, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Intervalo em Dias :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(217, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "*Define o intervalo da requisição do especial"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.chkNaoRecuperaRetido)
        Me.GroupBox3.Controls.Add(Me.chkNaoAvulsoRetido)
        Me.GroupBox3.Controls.Add(Me.chkNaoMensalRetido)
        Me.GroupBox3.Controls.Add(Me.ChkNaoPedirMensal)
        Me.GroupBox3.Controls.Add(Me.chkNaoListaB)
        Me.GroupBox3.Controls.Add(Me.chkNaoListaNova)
        Me.GroupBox3.Controls.Add(Me.chkNaoRecuperacao)
        Me.GroupBox3.Controls.Add(Me.chkNaoInativo)
        Me.GroupBox3.Controls.Add(Me.chkNaoAvulsoB)
        Me.GroupBox3.Controls.Add(Me.chkNaoMensalPrimeira)
        Me.GroupBox3.Controls.Add(Me.chkNaoAvulso)
        Me.GroupBox3.Controls.Add(Me.chkNaoEspecial)
        Me.GroupBox3.Controls.Add(Me.chkNaoMensal)
        Me.GroupBox3.Controls.Add(Me.chkNaoLista)
        Me.GroupBox3.Controls.Add(Me.chkNaoDoacao)
        Me.GroupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox3.Location = New System.Drawing.Point(686, 12)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(241, 380)
        Me.GroupBox3.TabIndex = 63
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Não Contribuiu"
        '
        'chkNaoRecuperaRetido
        '
        Me.chkNaoRecuperaRetido.AutoSize = True
        Me.chkNaoRecuperaRetido.Location = New System.Drawing.Point(15, 350)
        Me.chkNaoRecuperaRetido.Name = "chkNaoRecuperaRetido"
        Me.chkNaoRecuperaRetido.Size = New System.Drawing.Size(129, 17)
        Me.chkNaoRecuperaRetido.TabIndex = 14
        Me.chkNaoRecuperaRetido.Text = "RECUPERA RETIDO"
        Me.chkNaoRecuperaRetido.UseVisualStyleBackColor = True
        '
        'chkNaoAvulsoRetido
        '
        Me.chkNaoAvulsoRetido.AutoSize = True
        Me.chkNaoAvulsoRetido.Location = New System.Drawing.Point(15, 327)
        Me.chkNaoAvulsoRetido.Name = "chkNaoAvulsoRetido"
        Me.chkNaoAvulsoRetido.Size = New System.Drawing.Size(113, 17)
        Me.chkNaoAvulsoRetido.TabIndex = 13
        Me.chkNaoAvulsoRetido.Text = "AVULSO RETIDO"
        Me.chkNaoAvulsoRetido.UseVisualStyleBackColor = True
        '
        'chkNaoMensalRetido
        '
        Me.chkNaoMensalRetido.AutoSize = True
        Me.chkNaoMensalRetido.Location = New System.Drawing.Point(15, 304)
        Me.chkNaoMensalRetido.Name = "chkNaoMensalRetido"
        Me.chkNaoMensalRetido.Size = New System.Drawing.Size(114, 17)
        Me.chkNaoMensalRetido.TabIndex = 12
        Me.chkNaoMensalRetido.Text = "MENSAL RETIDO"
        Me.chkNaoMensalRetido.UseVisualStyleBackColor = True
        '
        'ChkNaoPedirMensal
        '
        Me.ChkNaoPedirMensal.AutoSize = True
        Me.ChkNaoPedirMensal.Location = New System.Drawing.Point(15, 281)
        Me.ChkNaoPedirMensal.Name = "ChkNaoPedirMensal"
        Me.ChkNaoPedirMensal.Size = New System.Drawing.Size(106, 17)
        Me.ChkNaoPedirMensal.TabIndex = 11
        Me.ChkNaoPedirMensal.Text = "PEDIR MENSAL"
        Me.ChkNaoPedirMensal.UseVisualStyleBackColor = True
        '
        'chkNaoListaB
        '
        Me.chkNaoListaB.AutoSize = True
        Me.chkNaoListaB.Location = New System.Drawing.Point(15, 258)
        Me.chkNaoListaB.Name = "chkNaoListaB"
        Me.chkNaoListaB.Size = New System.Drawing.Size(66, 17)
        Me.chkNaoListaB.TabIndex = 10
        Me.chkNaoListaB.Text = "LISTA B"
        Me.chkNaoListaB.UseVisualStyleBackColor = True
        '
        'chkNaoListaNova
        '
        Me.chkNaoListaNova.AutoSize = True
        Me.chkNaoListaNova.Location = New System.Drawing.Point(15, 235)
        Me.chkNaoListaNova.Name = "chkNaoListaNova"
        Me.chkNaoListaNova.Size = New System.Drawing.Size(89, 17)
        Me.chkNaoListaNova.TabIndex = 9
        Me.chkNaoListaNova.Text = "LISTA NOVA"
        Me.chkNaoListaNova.UseVisualStyleBackColor = True
        '
        'chkNaoRecuperacao
        '
        Me.chkNaoRecuperacao.AutoSize = True
        Me.chkNaoRecuperacao.Location = New System.Drawing.Point(15, 212)
        Me.chkNaoRecuperacao.Name = "chkNaoRecuperacao"
        Me.chkNaoRecuperacao.Size = New System.Drawing.Size(107, 17)
        Me.chkNaoRecuperacao.TabIndex = 8
        Me.chkNaoRecuperacao.Text = "RECUPERAÇÃO"
        Me.chkNaoRecuperacao.UseVisualStyleBackColor = True
        '
        'chkNaoInativo
        '
        Me.chkNaoInativo.AutoSize = True
        Me.chkNaoInativo.Location = New System.Drawing.Point(15, 189)
        Me.chkNaoInativo.Name = "chkNaoInativo"
        Me.chkNaoInativo.Size = New System.Drawing.Size(69, 17)
        Me.chkNaoInativo.TabIndex = 7
        Me.chkNaoInativo.Text = "INATIVO"
        Me.chkNaoInativo.UseVisualStyleBackColor = True
        '
        'chkNaoAvulsoB
        '
        Me.chkNaoAvulsoB.AutoSize = True
        Me.chkNaoAvulsoB.Location = New System.Drawing.Point(15, 166)
        Me.chkNaoAvulsoB.Name = "chkNaoAvulsoB"
        Me.chkNaoAvulsoB.Size = New System.Drawing.Size(79, 17)
        Me.chkNaoAvulsoB.TabIndex = 6
        Me.chkNaoAvulsoB.Text = "AVULSO B"
        Me.chkNaoAvulsoB.UseVisualStyleBackColor = True
        '
        'chkNaoMensalPrimeira
        '
        Me.chkNaoMensalPrimeira.AutoSize = True
        Me.chkNaoMensalPrimeira.Location = New System.Drawing.Point(15, 143)
        Me.chkNaoMensalPrimeira.Name = "chkNaoMensalPrimeira"
        Me.chkNaoMensalPrimeira.Size = New System.Drawing.Size(125, 17)
        Me.chkNaoMensalPrimeira.TabIndex = 5
        Me.chkNaoMensalPrimeira.Text = "MENSAL PRIMEIRA"
        Me.chkNaoMensalPrimeira.UseVisualStyleBackColor = True
        '
        'chkNaoAvulso
        '
        Me.chkNaoAvulso.AutoSize = True
        Me.chkNaoAvulso.Location = New System.Drawing.Point(15, 120)
        Me.chkNaoAvulso.Name = "chkNaoAvulso"
        Me.chkNaoAvulso.Size = New System.Drawing.Size(69, 17)
        Me.chkNaoAvulso.TabIndex = 4
        Me.chkNaoAvulso.Text = "AVULSO"
        Me.chkNaoAvulso.UseVisualStyleBackColor = True
        '
        'chkNaoEspecial
        '
        Me.chkNaoEspecial.AutoSize = True
        Me.chkNaoEspecial.Location = New System.Drawing.Point(15, 97)
        Me.chkNaoEspecial.Name = "chkNaoEspecial"
        Me.chkNaoEspecial.Size = New System.Drawing.Size(77, 17)
        Me.chkNaoEspecial.TabIndex = 3
        Me.chkNaoEspecial.Text = "ESPECIAL"
        Me.chkNaoEspecial.UseVisualStyleBackColor = True
        '
        'chkNaoMensal
        '
        Me.chkNaoMensal.AutoSize = True
        Me.chkNaoMensal.Location = New System.Drawing.Point(15, 74)
        Me.chkNaoMensal.Name = "chkNaoMensal"
        Me.chkNaoMensal.Size = New System.Drawing.Size(70, 17)
        Me.chkNaoMensal.TabIndex = 2
        Me.chkNaoMensal.Text = "MENSAL"
        Me.chkNaoMensal.UseVisualStyleBackColor = True
        '
        'chkNaoLista
        '
        Me.chkNaoLista.AutoSize = True
        Me.chkNaoLista.Location = New System.Drawing.Point(15, 51)
        Me.chkNaoLista.Name = "chkNaoLista"
        Me.chkNaoLista.Size = New System.Drawing.Size(56, 17)
        Me.chkNaoLista.TabIndex = 1
        Me.chkNaoLista.Text = "LISTA"
        Me.chkNaoLista.UseVisualStyleBackColor = True
        '
        'chkNaoDoacao
        '
        Me.chkNaoDoacao.AutoSize = True
        Me.chkNaoDoacao.Location = New System.Drawing.Point(15, 28)
        Me.chkNaoDoacao.Name = "chkNaoDoacao"
        Me.chkNaoDoacao.Size = New System.Drawing.Size(71, 17)
        Me.chkNaoDoacao.TabIndex = 0
        Me.chkNaoDoacao.Text = "DOAÇÃO"
        Me.chkNaoDoacao.UseVisualStyleBackColor = True
        '
        'FrmRegrasRequisicao
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(946, 451)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnSair)
        Me.Controls.Add(Me.btnSalvar)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Dados)
        Me.Name = "FrmRegrasRequisicao"
        Me.Text = "Regras da Requisição"
        Me.Dados.ResumeLayout(False)
        Me.Dados.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Dados As System.Windows.Forms.GroupBox
    Friend WithEvents cboOperador As System.Windows.Forms.ComboBox
    Friend WithEvents lblOperadora As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkSimRecuperaRetido As System.Windows.Forms.CheckBox
    Friend WithEvents chkSimAvulsoRetido As System.Windows.Forms.CheckBox
    Friend WithEvents chkSimMensalRetido As System.Windows.Forms.CheckBox
    Friend WithEvents ChkSimPedirMensal As System.Windows.Forms.CheckBox
    Friend WithEvents chkSimListaB As System.Windows.Forms.CheckBox
    Friend WithEvents chkSimListaNova As System.Windows.Forms.CheckBox
    Friend WithEvents chkSimRecuperacao As System.Windows.Forms.CheckBox
    Friend WithEvents chkSimInativo As System.Windows.Forms.CheckBox
    Friend WithEvents chkSimAvulsoB As System.Windows.Forms.CheckBox
    Friend WithEvents chkSimMensalPrimeira As System.Windows.Forms.CheckBox
    Friend WithEvents chkSimAvulso As System.Windows.Forms.CheckBox
    Friend WithEvents chkSimEspecial As System.Windows.Forms.CheckBox
    Friend WithEvents chkSimMensal As System.Windows.Forms.CheckBox
    Friend WithEvents chkSimLista As System.Windows.Forms.CheckBox
    Friend WithEvents chkSimDoacao As System.Windows.Forms.CheckBox
    Friend WithEvents btnSair As System.Windows.Forms.Button
    Friend WithEvents btnSalvar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents rbCategoria As System.Windows.Forms.RadioButton
    Friend WithEvents rbOperadora As System.Windows.Forms.RadioButton
    Friend WithEvents cboCategoria As System.Windows.Forms.ComboBox
    Friend WithEvents lblCategoria As System.Windows.Forms.Label
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDiasRecusa As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents txtDiasAceite As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents chkNaoRecuperaRetido As System.Windows.Forms.CheckBox
    Friend WithEvents chkNaoAvulsoRetido As System.Windows.Forms.CheckBox
    Friend WithEvents chkNaoMensalRetido As System.Windows.Forms.CheckBox
    Friend WithEvents ChkNaoPedirMensal As System.Windows.Forms.CheckBox
    Friend WithEvents chkNaoListaB As System.Windows.Forms.CheckBox
    Friend WithEvents chkNaoListaNova As System.Windows.Forms.CheckBox
    Friend WithEvents chkNaoRecuperacao As System.Windows.Forms.CheckBox
    Friend WithEvents chkNaoInativo As System.Windows.Forms.CheckBox
    Friend WithEvents chkNaoAvulsoB As System.Windows.Forms.CheckBox
    Friend WithEvents chkNaoMensalPrimeira As System.Windows.Forms.CheckBox
    Friend WithEvents chkNaoAvulso As System.Windows.Forms.CheckBox
    Friend WithEvents chkNaoEspecial As System.Windows.Forms.CheckBox
    Friend WithEvents chkNaoMensal As System.Windows.Forms.CheckBox
    Friend WithEvents chkNaoLista As System.Windows.Forms.CheckBox
    Friend WithEvents chkNaoDoacao As System.Windows.Forms.CheckBox
    Friend WithEvents btnGrava As System.Windows.Forms.Button
End Class
