﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text

Public Class FrmAviso


#Region "variaveis"

    Public pCod_ean As String
    Public pNome As String
    Public pTelefone As String
    Public psituacao As String
    Public pMesAnoRef As String
    Public pValor As Decimal
    Public MesAtuacao As String
    Public NextMonth As Date
    Public Consulta As Boolean = False
    Public pCodUsuario As Integer
    Public pUsuario As String
    Public pCodCategoria As String
    Public pCpf As String
    Public pAutorizante As String
    Public pObs As String
    Public isOk As Boolean = False
    Public Analisecontribuinte As New CContribuinte
    Public contribuinte As New CContribuinte
    Public pAceite As String
    Public pNaoConforme As Boolean

#End Region

#Region "Propriedades"

    Public Property Cod_Ean_Cliente() As String
        Get
            Return pCod_ean
        End Get
        Set(ByVal value As String)
            pCod_ean = value
        End Set
    End Property

    Public Property Telefone() As String
        Get
            Return pTelefone
        End Get
        Set(ByVal value As String)
            pTelefone = value
        End Set
    End Property

    Public Property Nome() As String
        Get
            Return pNome
        End Get
        Set(ByVal value As String)
            pNome = value
        End Set
    End Property

    Public Property Valor() As String
        Get
            Return pValor
        End Get
        Set(ByVal value As String)
            pValor = value
        End Set
    End Property

    Public Property CodCategoria() As String
        Get
            Return pCodCategoria
        End Get
        Set(ByVal value As String)
            pCodCategoria = value
        End Set
    End Property



#End Region


    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click

        Dim rnContribuinte As New RNContribuinte
        Dim mesSeguinte As String
        mesSeguinte = DateAdd(DateInterval.Month, 1, CDate(MesAtuacao))
        mesSeguinte = mesSeguinte.Substring(3)


        Dim mensagemAnaliseRequisicao = MessageBox.Show("Já existe um registro com o telefone " & Analisecontribuinte.Telefone1 & _
                                                          ", que foi incluso no dia " & CDate(Analisecontribuinte.Data_mov).ToString("dd/MM/yyyy") & _
                                                          " pela operadora " & Analisecontribuinte.Operador & _
                                                          " com o valor de R$" & Analisecontribuinte.Valor & _
                                                           " O aceite para esse telefone será informado à diretoria, não constando como um novo registro da Oi! " & _
                                                          " Deseja continuar?", "Atenção", _
                                         MessageBoxButtons.YesNo, _
                                         MessageBoxIcon.Question)



        If (mensagemAnaliseRequisicao = DialogResult.Yes) Then
            If rnContribuinte.Inclui_Numeros_Oi(contribuinte, pAceite, MesAtuacao, mesSeguinte, pNome) = True Then
                rnContribuinte.Atualiza_Informacoes_Oi(contribuinte, MesAtuacao, mesSeguinte, pCpf, pAutorizante, pObs, "0")
                rnContribuinte.Atualiza_UsuarioRegistro(contribuinte, MesAtuacao, mesSeguinte, pCpf, pAutorizante, pObs)
                MsgBox("Registro salvo com sucesso!", MsgBoxStyle.Information, "Atenção")
                FrmConsultaUltimaMovimentacao.atualizou = False
            Else
                MsgBox("Ocorreu um erro na inclusão do registro", MsgBoxStyle.Information, "Atenção")
            End If
        End If

        Close()

    End Sub

    Private Sub FrmAviso_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


    End Sub



End Class