﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text
Imports Microsoft.Office.Interop


Public Class FrmConsultaContribuintePorSituacao

#Region "Enum"

    Private Enum ColunasGridRemessa

        DDD = 0
        Telefone = 1
        Nome = 2
        Valor = 3
        Cod_EAN_CLIENTE = 4
    End Enum

    Private Enum ColunasGridHistorico
        DDD = 0
        Telefone = 1
        Valor = 2
        categoria = 3
        mesRef = 4
        Situacao = 5
        MotivoSituacao = 6
        Data_Situacao = 7
    End Enum

    Enum xlsOption
        xlsOpen
    End Enum

#End Region

#Region "variaveis"

    Public telefone As String
    Private lstContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstContribuinteSelecionada As New List(Of CRetorno)
    Private v_ambiente As String
    Private rnContribuinte As RNContribuinte = New RNContribuinte
    Private rnRetorno As RNRetorno = New RNRetorno
    Private contribuinte As CContribuinte = New CContribuinte
    Private lstContribuintesOi As New SortableBindingList(Of CRetorno)
    Private lstListaTelefonica As New SortableBindingList(Of CListaTelefonica)
    Private lstContribuinteSelecionadaOi As New List(Of CRetorno)
    Public mNivelAcesso As Integer
    Public mUsuario As String
    Public mCodUsuario As Integer
    Public atualizou As Boolean
    Public bindingSourceContribuite As BindingSource = New BindingSource
    Public mesSeguinte As String
    Public telefoneErrado As String


    Public Property NivelAcesso() As Integer
        Get
            Return mNivelAcesso
        End Get
        Set(ByVal value As Integer)
            mNivelAcesso = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return mUsuario
        End Get
        Set(ByVal value As String)
            mUsuario = value
        End Set
    End Property

    Public Property CodUsuario() As Integer
        Get
            Return mCodUsuario
        End Get
        Set(ByVal value As Integer)
            mCodUsuario = value
        End Set
    End Property

#End Region



#Region "Metodos"



    Private Sub carregaGrid()

        Dim objContribuintes As CContribuinte = New CContribuinte()

        Dim bindingSourceApolice As BindingSource = New BindingSource

        Try



            lstContribuintes = New SortableBindingList(Of CContribuinte)(rnContribuinte.CarregarGridOi(objContribuintes, "Remessa"))

            If (lstContribuintes.Count > 0) Then

                bindingSourceApolice.DataSource = lstContribuintes

                grdRemessa.DataSource = bindingSourceApolice.DataSource
                'grdRemessa.Sort(grdRemessa.Columns(ColunasGridRemessa.Nome), ListSortDirection.Ascending)
                'lblNumRegistros.Text = lstApolices.Count


            End If


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CriarColunasGrid()

        Try

            grdRemessa.DataSource = Nothing
            grdRemessa.Columns.Clear()

            grdRemessa.AutoGenerateColumns = False

            grdRemessa.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            'Dim checkboxColumn As New DataGridViewCheckBoxColumn
            'checkboxColumn.Width = 32
            'checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StDDDMeioAcessoCobranca"
            column.HeaderText = "DDD"
            column.Width = 30
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)


            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StNumMeioAcessoCobranca"
            column.HeaderText = "Telefone"
            column.Width = 80
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "nome_cliente"
            column.HeaderText = "Nome"
            column.Width = 300
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DVlrServico"
            column.HeaderText = "Valor"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            grdRemessa.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "cod_ean_cliente"
            column.HeaderText = "Cod. EAN Cliente"
            column.Width = 100
            column.ReadOnly = False
            column.Visible = True
            grdRemessa.Columns.Add(column)

            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UsSitFaturamento"
            column.HeaderText = "Situacao Faturamento"
            column.Width = 160
            column.ReadOnly = False
            column.Visible = True
            grdRemessa.Columns.Add(column)


            '7
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UiMotivoSituacao"
            column.HeaderText = "Motivo Situação"
            column.Visible = True
            column.Width = 150
            column.ReadOnly = False
            grdRemessa.Columns.Add(column)




        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CriarColunasGridHistorico()

        Try

            GrdHistorico.DataSource = Nothing
            GrdHistorico.Columns.Clear()

            GrdHistorico.AutoGenerateColumns = False

            GrdHistorico.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            'Dim checkboxColumn As New DataGridViewCheckBoxColumn
            'checkboxColumn.Width = 32
            'checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DDD"
            column.HeaderText = "DDD"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Telefone"
            column.HeaderText = "Telefone"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "valor"
            column.HeaderText = "Valor"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            GrdHistorico.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "categoria"
            column.HeaderText = "Categoria"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            GrdHistorico.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "mesRef"
            column.HeaderText = "Mês Referência"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            GrdHistorico.Columns.Add(column)

            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "situacao"
            column.HeaderText = "Situacao Faturamento"
            column.Width = 160
            column.ReadOnly = False
            column.Visible = True
            GrdHistorico.Columns.Add(column)

            '7
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "motivoSituacao"
            column.HeaderText = "Motivo Situação"
            column.Visible = True
            column.Width = 150
            column.ReadOnly = False
            GrdHistorico.Columns.Add(column)

            '8
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "dt_situacao"
            column.HeaderText = "Data Situação"
            column.Visible = True
            column.Width = 150
            column.ReadOnly = False
            GrdHistorico.Columns.Add(column)



        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

#End Region

    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""

        Try

            If cboSituacao.SelectedIndex = 0 Or cboSituacao.Text = "Selecione..." Then
                MsgBox("Opção invalida, selecionar a situação!", MsgBoxStyle.Critical, "Atenção!")
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor


            carregaFeedGridCategoriaContribuinte(cboSituacao.Text, CodUsuario)


            Me.Cursor = Cursors.Default

        Catch ex As Exception

            Me.Cursor = Cursors.Default
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try

    End Sub

    Private Sub carregaFeedGridCategoriaContribuinte(ByVal situacao As String, ByVal codUsuario As Integer)

        Dim objRetorno As CRetorno = New CRetorno()

        Dim drvl As Decimal = 0
        Dim ds As DataSet = New DataSet
        Dim valor As Single
        Dim strValor As String

        Try

            lstContribuintesOi = New SortableBindingList(Of CRetorno)(rnRetorno.CarregarFeedContribuintesPelaSituacao(situacao, "Consulta"))

            If (lstContribuintesOi.Count > 0) Then

                bindingSourceContribuite.DataSource = lstContribuintesOi

                grdRemessa.DataSource = bindingSourceContribuite.DataSource

             
                If grdRemessa.Rows.Count > 0 Then

                    For Each linha As DataGridViewRow In grdRemessa.Rows
                        drvl = linha.Cells.Item(3).Value
                        valor = valor + drvl
                    Next


                End If

                strValor = valor.ToString("C")
                lblValor.Text = valor.ToString("R$ #,###.00")
                lblTotalFichas.Text = grdRemessa.Rows.Count

            Else
                grdRemessa.DataSource = Nothing
                MsgBox("Não existe registros a serem processados, verifique se houve a liberação das fichas!")

            End If

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub
    Private Sub btnRetorno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRetorno.Click
        Try

            FrmRetorno.Show()

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Close()
    End Sub

    Private Sub FrmConsultaContribuinte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        CriarColunasGrid()
        CarregaComboCategorias(0)
        CarregaComboSituacao(0)

        DtMesAnoPesquisa.Text = Date.Now
        txtMesRef.Text = Date.Now
        atualizou = True

    End Sub


    Private Sub CarregaComboSituacao(ByVal cod_situacao As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboSituacao.DataSource = Nothing
            cboSituacao.Items.Clear()
            cboSituacao.DisplayMember = "situacao"
            cboSituacao.ValueMember = "situacao_faturamento_id"
            cboSituacao.DataSource = rn_generico.BuscarSituacao(0)

            If cboSituacao.Items.Count = 0 Then
                cboSituacao.SelectedIndex = 0
            ElseIf cboSituacao.Items.Count > 0 And cod_situacao <> 0 Then

                For i As Integer = 0 To cboSituacao.Items.Count - 1
                    For Each linha As CGenerico In cboSituacao.DataSource
                        If linha.Situacao = cod_situacao Then
                            cboSituacao.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next

            Else

                cboSituacao.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CarregaComboCategorias(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboSituacao.DataSource = Nothing
            cboSituacao.Items.Clear()
            cboSituacao.DisplayMember = "Nome_Categoria"
            cboSituacao.ValueMember = "Cod_Categoria"
            cboSituacao.DataSource = rn_generico.BuscarCategoriaPorOperador(CodUsuario)

            If cboSituacao.Items.Count > 0 And cod_categoria = 0 Then
                cboSituacao.SelectedIndex = 0
            ElseIf cboSituacao.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboSituacao.Items.Count - 1
                    For Each linha As CGenerico In cboSituacao.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboSituacao.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboSituacao.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboContribuinte()
        Dim rn_contribuinte As RNContribuinte = New RNContribuinte
        Dim filtro As CContribuinte = New CContribuinte

        cboNome.DataSource = Nothing
        cboNome.Items.Clear()
        cboNome.DisplayMember = "NomeCliente1"
        cboNome.ValueMember = "Cod_cliente"
        cboNome.DataSource = rn_contribuinte.CarregarGrid(filtro, "Contribuinte")

        If cboNome.Items.Count > 0 Then
            cboNome.SelectedIndex = 0
        End If


    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click

        ExportarParaExcel(grdRemessa, "teste")
    End Sub

    Public Sub ExportarParaExcel(ByVal dgvName As DataGridView, Optional ByVal fileName As String = "")
        ' Criar uma nova instância do Excel 
        Dim objExcelApp As New Excel.Application()
        Dim objExcelBook As Excel.Workbook
        Dim objExcelSheet As Excel.Worksheet
        Try

            ' Altera o tipo/localização para Inglês. Existe incompatibilidade  
            ' entre algumas versões de Excel vs Sistema Operativo 
            Dim oldCI As CultureInfo = CurrentThread.CurrentCulture
            CurrentThread.CurrentCulture = New CultureInfo("en-US")
            ' Adiciona um workbook e activa a worksheet actual 
            objExcelBook = objExcelApp.Workbooks.Add
            objExcelSheet = CType(objExcelBook.Worksheets(1), Excel.Worksheet)
            ' Ciclo nos cabeçalhos para escrever os títulos a bold/negrito 
            Dim dgvColumnIndex As Int16 = 1
            For Each col As DataGridViewColumn In dgvName.Columns
                objExcelSheet.Cells(1, dgvColumnIndex) = col.HeaderText
                objExcelSheet.Cells(1, dgvColumnIndex).Font.Bold = True
                dgvColumnIndex += 1
            Next
            ' Ciclo nas linhas/células 
            Dim dgvRowIndex As Integer = 2

            For Each row As DataGridViewRow In dgvName.Rows
                Dim dgvCellIndex As Integer = 1

                For Each cell As DataGridViewCell In row.Cells
                    objExcelSheet.Cells(dgvRowIndex, dgvCellIndex) = cell.Value
                    dgvCellIndex += 1
                Next
                dgvRowIndex += 1
            Next
            ' Ajusta o largura das colunas automaticamente 
            objExcelSheet.Columns.AutoFit()
            ' Caso a opção seja gravar (xlsSaveAs) grava o ficheiro e fecha 
            ' o Workbook/Excel. Caso contrário (xlsOpen) abre o Excel 

            objExcelBook.SaveAs("C:\AACI\teste.xlsx")
            objExcelBook.Close()
            objExcelApp.Quit()
            MessageBox.Show("Ficheiro exportado com sucesso para: " & fileName)


            ' Altera a tipo/localização para actual 
            CurrentThread.CurrentCulture = oldCI
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        Finally
            objExcelSheet = Nothing
            objExcelBook = Nothing
            objExcelApp = Nothing
            ' O GC(garbage collector) recolhe a memória não usada pelo sistema.  
            ' O método Collect() força a recolha e a opção WaitForPendingFinalizers  
            ' espera até estar completo. Desta forma o EXCEL.EXE não fica no  
            ' Task Manager(gestor tarefas) ocupando memória desnecessariamente 
            ' (devem ser chamados duas vezes para maior garantia) 
            GC.Collect()
            GC.WaitForPendingFinalizers()
            GC.Collect()
            GC.WaitForPendingFinalizers()
        End Try


    End Sub

    Private Sub FrmConsultaUltimaMovimentacao_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""

        Try
            If atualizou = False Then
                If cboSituacao.SelectedIndex = 0 Or cboSituacao.Text = "Selecione..." Then
                    Exit Sub
                End If

                Me.Cursor = Cursors.WaitCursor

                carregaFeedGridCategoriaContribuinte(cboSituacao.SelectedValue, CodUsuario)


                Me.Cursor = Cursors.Default
                atualizou = True
            End If

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub



    Private Sub txtNome_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNome.TextChanged
        Dim texto As String = Nothing
        Dim linhaVazia As Integer = 0
        Dim contador As Integer = 0

        If txtNome.Text <> String.Empty Then

            linhaVazia = grdRemessa.Rows.Count - 1

            'percorre cada linha do DataGridView
            For Each linha As DataGridViewRow In grdRemessa.Rows

                'percorre cada célula da linha
                For Each celula As DataGridViewCell In grdRemessa.Rows(linha.Index).Cells
                    If Not contador = linhaVazia Then
                        'se a coluna for a coluna 2 (Nome) então verifica o criterio
                        If celula.ColumnIndex = 2 Then

                            If celula.Value.ToString <> "" Then
                                texto = IIf(celula.Value.ToString = Nothing, "", celula.Value.ToString)
                                'se o texto informado estiver contido na célula então seleciona toda linha
                                If texto.Contains(txtNome.Text) Then
                                    'seleciona a linha
                                    grdRemessa.CurrentCell = celula
                                    grdRemessa.CurrentCell = grdRemessa.Rows(celula.RowIndex).Cells(2)
                                    Exit Sub
                                End If
                            End If
                            contador = contador + 1
                        End If
                    End If

                Next

            Next

        End If

    End Sub

    Private Sub txtTelefone_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTelefone.TextChanged
        Dim texto As String = Nothing
        Dim linhaVazia As Integer = 0
        Dim contador As Integer = 0

        If txtTelefone.Text <> String.Empty Then

            linhaVazia = grdRemessa.Rows.Count - 1

            'percorre cada linha do DataGridView
            For Each linha As DataGridViewRow In grdRemessa.Rows

                'percorre cada célula da linha
                For Each celula As DataGridViewCell In grdRemessa.Rows(linha.Index).Cells
                    If Not contador = linhaVazia Then
                        'se a coluna for a coluna 1 (telefone) então verifica o criterio
                        If celula.ColumnIndex = 1 Then

                            If celula.Value.ToString <> "" Then
                                texto = IIf(celula.Value.ToString = Nothing, "", celula.Value.ToString)
                                'se o texto informado estiver contido na célula então seleciona toda linha
                                If texto.Contains(txtTelefone.Text) Then
                                    'seleciona a linha
                                    grdRemessa.CurrentCell = celula
                                    grdRemessa.CurrentCell = grdRemessa.Rows(celula.RowIndex).Cells(2)
                                    Exit Sub
                                End If
                            End If
                            contador = contador + 1
                        End If
                    End If

                Next

            Next

        End If
    End Sub

    Private Sub grdRemessa_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdRemessa.CellContentClick

        Dim ClienteClicado As String
        Dim DDDSelecionado As String = ""
        Dim TelefoneSelecionado As String = ""
        Dim valorEspecial As Decimal = 0
        Dim rn As RNGenerico = New RNGenerico
        Dim rnContribuinte As RNContribuinte = New RNContribuinte

        Try

            ClienteClicado = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.Cod_EAN_CLIENTE).Value, String)

            DDDSelecionado = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.DDD).Value, String)

            TelefoneSelecionado = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.Telefone).Value, String)

            valorEspecial = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.Valor).Value, Decimal)



            For Each ContribuinteInconsistencia As CRetorno In lstContribuintesOi
                If ContribuinteInconsistencia.Cod_ean_cliente = ClienteClicado Then

                End If
            Next


            If (e.RowIndex > -1) Then

                lstListaTelefonica = New SortableBindingList(Of CListaTelefonica)(rn.Retorna_Historico(TelefoneSelecionado, DDDSelecionado, ClienteClicado))

                If (lstListaTelefonica.Count > 0) Then

                    bindingSourceContribuite.DataSource = lstListaTelefonica

                    GrdHistorico.DataSource = bindingSourceContribuite.DataSource

                Else

                    GrdHistorico.DataSource = Nothing

                End If


                contribuinte = rnContribuinte.CarregarContribuintePorTelefone(TelefoneSelecionado, DDDSelecionado, ClienteClicado, "ConsultaUltimaMovimentacao")

                If (contribuinte.Cod_EAN_Cliente = Nothing Or contribuinte.Cod_EAN_Cliente = "") Then

                    contribuinte = rnContribuinte.PesquisarTabelaRetidoPorTelefone(TelefoneSelecionado, DDDSelecionado, ClienteClicado)
                End If

                'BUSCA AS INFORMAÇÕES NA TABELA DE LISTA TELEFONICA SE ACHA COMPLETA COM OS DADOS DO CLIENTE
                If (contribuinte.Cod_EAN_Cliente = Nothing) Then
                    contribuinte = rnContribuinte.BuscaListaTelefonica(TelefoneSelecionado, DDDSelecionado, ClienteClicado)
                    If (contribuinte.Cod_EAN_Cliente <> Nothing) Then
                        contribuinte = rnContribuinte.PesquisarPorCodBarra(contribuinte.Cod_EAN_Cliente)
                        MsgBox("As informações contidas na requisição está diferente das informações do cliente, antes de finalizar a contribuição verifique os dados inseridos", MsgBoxStyle.Critical, "Atenção!!")
                        telefoneErrado = TelefoneSelecionado
                    End If
                End If


                If Not contribuinte Is Nothing And Not contribuinte.Telefone1 Is Nothing Then
                    'popula a tela
                    txtCodigo.Text = contribuinte.CodCliente
                    txtNomeCliente1.Text = contribuinte.NomeCliente1
                    txtNomeCliente2.Text = contribuinte.NomeCliente2
                    txtCodBarra.Text = contribuinte.Cod_EAN_Cliente
                    txtCnpjCpf.Text = contribuinte.CNPJ_CPF
                    txtRg.Text = contribuinte.IE_CI
                    txtEmail.Text = contribuinte.Email
                    txtTelefone1.Text = contribuinte.Telefone1
                    txtTelefone2.Text = contribuinte.Telefone2
                    txtEndereco.Text = contribuinte.Endereco
                    txtUF.Text = contribuinte.UF
                    txtCEP.Text = contribuinte.CEP

                    txtRegiao.Text = contribuinte.Cod_Regiao

                    txtDtNc.Text = contribuinte.DataNascimento
                    txtDtNcMp.Text = contribuinte.DT_NC_MP
                    txtDtCadastro.Text = contribuinte.DataCadastro
                    txtReferencia.Text = contribuinte.Referencia

                    txtObs.Text = contribuinte.Observacao

                    txtMelhorDia.Text = contribuinte.DiaLimite
                    txtValDoacao.Text = Convert.ToDouble(contribuinte.Valor).ToString("R$ #,###.00")
                    txtDDD.Text = contribuinte.DDD

                    CarregaComboCidades(contribuinte.Cod_Cidade)
                    CarregaComboCategoriasDetalhes(contribuinte.Cod_Categoria)
                    CarregaComboOperador(CodUsuario)
                    CarregaComboOperadorDetalhe(contribuinte.Cod_Operador)
                    CarregaComboBairros(contribuinte.Cod_Bairro)
                    CarregaComboEspecie(contribuinte.Cod_Especie)



                    If contribuinte.Descontinuado = "S" Then
                        chkInativo.Checked = True
                    Else
                        chkInativo.Checked = False
                    End If

                    If contribuinte.Nao_pedir_extra = "S" Then
                        chkNaoPedirExtra.Checked = True
                    Else
                        chkNaoPedirExtra.Checked = False
                    End If

                    If contribuinte.Nao_Pedir_Mensal = "S" Then
                        chkNaoPedirMensal.Checked = True
                    Else
                        chkNaoPedirMensal.Checked = False
                    End If

                    If contribuinte.Tel_desligado = "S" Then
                        chkTelDesl.Checked = True
                    Else
                        chkTelDesl.Checked = False
                    End If


                    'TELA DE CONCLUSAO
                    txtCodEanConcluir.Text = contribuinte.Cod_EAN_Cliente
                    txtNomeConcluir.Text = contribuinte.NomeCliente1
                    txtDDDConcluir.Text = contribuinte.DDD
                    txtTelefoneConcluir.Text = contribuinte.Telefone1
                    CarregaComboCategoriasDetalhes(contribuinte.Cod_Categoria)
                    CarregaComboCategoriasConcluir(contribuinte.Cod_Categoria)
                    If cboCategoriaConcluir.Text <> "ESPECIAL" Then
                        txtValorConcluir.Text = Convert.ToDouble(contribuinte.Valor).ToString("#,###.00")
                    Else
                        txtValorConcluir.Text = Convert.ToDouble(valorEspecial).ToString("#,###.00")
                    End If

                    If contribuinte.Cod_Categoria = 3 Or contribuinte.Cod_Categoria = 4 Then
                        btnCancelarMensal.Visible = True
                    Else
                        btnCancelarMensal.Visible = False
                    End If

                End If

            End If


            ' contribuinte = rnContribuinte.PesquisarContribuintePorTelefone(telefone, 1)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CarregaComboCidades(ByVal cod_cidade As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCidade.DataSource = Nothing
            cboCidade.Items.Clear()
            cboCidade.DisplayMember = "Nome_cidade"
            cboCidade.ValueMember = "Cod_Cidade"
            cboCidade.DataSource = rn_generico.BuscarCidades(0)

            If cboCidade.Items.Count = 0 Then
                cboCidade.SelectedIndex = 0
            ElseIf cboCidade.Items.Count > 0 And cod_cidade <> 0 Then

                For i As Integer = 0 To cboCidade.Items.Count - 1
                    For Each linha As CGenerico In cboCidade.DataSource
                        If linha.Cod_Cidade = cod_cidade Then
                            cboCidade.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next

            Else

                cboCidade.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CarregaComboBairros(ByVal cod_bairro As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try



            cboBairroDetalhe.DataSource = Nothing
            cboBairroDetalhe.Items.Clear()
            cboBairroDetalhe.DisplayMember = "Nome_Bairro"
            cboBairroDetalhe.ValueMember = "Cod_Bairro"
            cboBairroDetalhe.DataSource = rn_generico.BuscarBairros(0)

            If cboBairroDetalhe.Items.Count > 0 And cod_bairro = 0 Then
                cboBairroDetalhe.SelectedIndex = 0

            ElseIf cboBairroDetalhe.Items.Count > 0 And cod_bairro <> 0 Then
                For i As Integer = 0 To cboBairroDetalhe.Items.Count - 1
                    For Each linha As CGenerico In cboBairroDetalhe.DataSource
                        If linha.Cod_Bairro = cod_bairro Then
                            cboBairroDetalhe.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboBairroDetalhe.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboOperador(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboOperadorConcluir.DataSource = Nothing
            cboOperadorConcluir.Items.Clear()
            cboOperadorConcluir.DisplayMember = "Nome_Operador"
            cboOperadorConcluir.ValueMember = "Cod_Operador"
            cboOperadorConcluir.DataSource = rn_generico.BuscarOperador(0)

            If cboOperadorConcluir.Items.Count > 0 And cod_operador = 0 Then
                cboOperadorConcluir.SelectedIndex = 0
            ElseIf cboOperadorConcluir.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperadorConcluir.Items.Count - 1
                    For Each linha As CGenerico In cboOperadorConcluir.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperadorConcluir.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperadorConcluir.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboOperadorDetalhe(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboOperadorDetalhe.DataSource = Nothing
            cboOperadorDetalhe.Items.Clear()
            cboOperadorDetalhe.DisplayMember = "Nome_Operador"
            cboOperadorDetalhe.ValueMember = "Cod_Operador"
            cboOperadorDetalhe.DataSource = rn_generico.BuscarOperador(0)

            If cboOperadorDetalhe.Items.Count > 0 And cod_operador = 0 Then
                cboOperadorDetalhe.SelectedIndex = 0
            ElseIf cboOperadorDetalhe.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperadorDetalhe.Items.Count - 1
                    For Each linha As CGenerico In cboOperadorDetalhe.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperadorDetalhe.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperadorDetalhe.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboEspecie(ByVal cod_especie As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try


            cboEspecieDetalhe.DataSource = Nothing
            cboEspecieDetalhe.Items.Clear()
            cboEspecieDetalhe.DisplayMember = "Nome_Especie"
            cboEspecieDetalhe.ValueMember = "Cod_Especie"
            cboEspecieDetalhe.DataSource = rn_generico.BuscarEspecie(0)

            If cboEspecieDetalhe.Items.Count > 0 And cod_especie = 0 Then
                cboEspecieDetalhe.SelectedIndex = 0
            ElseIf cboEspecieDetalhe.Items.Count > 0 And cod_especie <> 0 Then
                For i As Integer = 0 To cboEspecieDetalhe.Items.Count - 1
                    For Each linha As CGenerico In cboEspecieDetalhe.DataSource
                        If linha.Cod_Especie = cod_especie Then
                            cboEspecieDetalhe.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboEspecieDetalhe.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try



    End Sub


    Private Sub CarregaComboCategoriasDetalhes(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCategoriaDetalhe.DataSource = Nothing
            cboCategoriaDetalhe.Items.Clear()
            cboCategoriaDetalhe.DisplayMember = "Nome_Categoria"
            cboCategoriaDetalhe.ValueMember = "Cod_Categoria"
            cboCategoriaDetalhe.DataSource = rn_generico.BuscarCategoria(0)

            If cboCategoriaDetalhe.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoriaDetalhe.SelectedIndex = 0
            ElseIf cboCategoriaDetalhe.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoriaDetalhe.Items.Count - 1
                    For Each linha As CGenerico In cboCategoriaDetalhe.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoriaDetalhe.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoriaDetalhe.SelectedIndex = 1
            End If



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub


    Private Sub CarregaComboCategoriasConcluir(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try
            cboCategoriaConcluir.DataSource = Nothing
            cboCategoriaConcluir.Items.Clear()
            cboCategoriaConcluir.DisplayMember = "Nome_Categoria"
            cboCategoriaConcluir.ValueMember = "Cod_Categoria"
            cboCategoriaConcluir.DataSource = rn_generico.BuscarCategoria(0)

            If cboCategoriaConcluir.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoriaConcluir.SelectedIndex = 0
            ElseIf cboCategoriaConcluir.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoriaConcluir.Items.Count - 1
                    For Each linha As CGenerico In cboCategoriaConcluir.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoriaConcluir.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoriaConcluir.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim rn As RNGenerico = New RNGenerico
        Dim lista As CListaTelefonica = New CListaTelefonica

        Try

            'ATRIBUI OS VALORES AO OBJETO
            lista.Cod_cliente = txtCodEanConcluir.Text
            lista.Nome_cliente = txtNomeConcluir.Text
            lista.DDD = txtDDDConcluir.Text
            lista.Telefone = txtTelefoneConcluir.Text
            lista.Valor = txtValorConcluir.Text
            lista.Categoria = cboCategoriaConcluir.SelectedValue
            lista.Cod_cidade = cboCidade.SelectedValue


            If (rn.Atualiza_Lista_Telefonica(lista)) = True Then
                MsgBox("Informações da Lista Telefônica foram atualizadas com sucesso!")
            Else
                MsgBox("Ocorreu um erro ao atualizar as informações")
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub




    Private Sub btnSalvar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalvar.Click
        Dim contribuinte As CContribuinte = New CContribuinte

        Try

            contribuinte.CodCliente = txtCodigo.Text
            contribuinte.NomeCliente1 = txtNome.Text
            contribuinte.Cod_EAN_Cliente = txtCodBarra.Text
            contribuinte.CNPJ_CPF = txtCnpjCpf.Text
            contribuinte.IE_CI = txtRg.Text
            contribuinte.Email = txtEmail.Text
            contribuinte.Telefone1 = txtTelefone1.Text
            contribuinte.Telefone2 = txtTelefone2.Text
            contribuinte.Endereco = txtEndereco.Text
            contribuinte.UF = txtUF.Text
            contribuinte.CEP = txtCEP.Text

            contribuinte.Cod_Cidade = cboCidade.SelectedValue
            contribuinte.Cidade = cboCidade.Text

            contribuinte.Cod_Bairro = cboBairroDetalhe.SelectedValue
            contribuinte.Bairro = cboBairroDetalhe.Text

            contribuinte.Cod_Operador = cboOperadorConcluir.SelectedValue
            contribuinte.Operador = cboOperadorConcluir.Text
            contribuinte.Cod_Usuario = cboOperadorConcluir.SelectedValue

            contribuinte.Cod_Categoria = cboSituacao.SelectedValue

            contribuinte.Cod_Especie = cboEspecieDetalhe.SelectedValue

            contribuinte.Cod_empresa = 1

            contribuinte.Cod_Regiao = txtRegiao.Text

            contribuinte.DataNascimento = txtDtNc.Text
            contribuinte.DT_NC_MP = txtDtNcMp.Text
            contribuinte.DataCadastro = txtDtCadastro.Text
            contribuinte.Referencia = txtReferencia.Text

            contribuinte.Observacao = txtObs.Text

            contribuinte.DiaLimite = txtMelhorDia.Text
            contribuinte.Valor = txtValDoacao.Text
            contribuinte.DDD = txtDDD.Text

            If chkTelDesl.Checked = True Then
                contribuinte.Tel_desligado = "S"
            Else
                contribuinte.Tel_desligado = "N"
            End If

            If chkInativo.Checked = True Then
                contribuinte.Descontinuado = "S"
            Else
                contribuinte.Descontinuado = "N"
            End If

            If chkNaoPedirExtra.Checked = True Then
                contribuinte.Nao_pedir_extra = "S"
            Else
                contribuinte.Nao_pedir_extra = "N"
            End If

            If chkNaoPedirMensal.Checked = True Then
                contribuinte.Nao_Pedir_Mensal = "S"
            Else
                contribuinte.Nao_Pedir_Mensal = "N"
            End If

            If rnContribuinte.VerificaExistenciaContribuinte(contribuinte) = True Then
                rnContribuinte.AtualizaContribuinte(contribuinte)
                MsgBox("Dados Salvos com sucesso!")
            Else
                rnContribuinte.Salvar(contribuinte)
                MsgBox("Dados Salvos com sucesso!")
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub


    Private Sub btnSalvarConcluir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContribuir.Click
        Dim contribuinte As New CContribuinte
        Dim rnContribuinte As New RNContribuinte
        Dim rnRequisicao As New RNRequisicao
        Dim analiseAceite As String()

        Try
            If cboOperadorConcluir.SelectedIndex = 0 Then
                MsgBox("Por favor selecione um operador")
                Exit Sub
            End If

            If cboCategoriaConcluir.SelectedIndex = 0 Then
                MsgBox("Por favor selecione uma categoria")
                Exit Sub
            End If

            If txtCodEanConcluir.Text = "*     -   *" Then
                MsgBox("Por favor digite o código do contribuinte")
                Exit Sub
            End If

            If txtDDDConcluir.Text = "" Then
                MsgBox("Por favor digite o DDD do contribuinte")
                Exit Sub
            End If


            If txtTelefoneConcluir.Text = "" Then
                MsgBox("Por favor digite o telefone do contribuinte")
                Exit Sub
            End If

            If txtValorConcluir.Text = "" Then
                MsgBox("Por favor digite o valor da contribuição")
                Exit Sub
            End If


            Dim valorConcluir As Decimal = 0
            valorConcluir = CDec(txtValorConcluir.Text)

            If valorConcluir > 100 Then
                Dim result = MessageBox.Show("O valor informado é superior a 100 reais, confirma o valor informado?", "Atenção", _
                           MessageBoxButtons.YesNo, _
                           MessageBoxIcon.Question)

                If (result = DialogResult.No) Then
                    Exit Sub
                End If
            End If

            Me.Cursor = Cursors.WaitCursor


            contribuinte.NomeCliente1 = txtNomeConcluir.Text
            contribuinte.Telefone1 = txtTelefoneConcluir.Text
            contribuinte.DDD = txtDDDConcluir.Text
            contribuinte.Valor = txtValorConcluir.Text
            contribuinte.Cod_EAN_Cliente = txtCodEanConcluir.Text
            contribuinte.Cod_Operador = cboOperadorConcluir.SelectedValue
            contribuinte.Operador = cboOperadorConcluir.Text
            contribuinte.Cod_Categoria = cboSituacao.SelectedValue

            If chkNaoConforme.Checked = True Then
                contribuinte.Nao_Conforme = 1
            Else
                contribuinte.Nao_Conforme = 0
            End If

            mesSeguinte = DateAdd(DateInterval.Month, 1, CDate(txtMesRef.Text))
            mesSeguinte = mesSeguinte.Substring(3)

            analiseAceite = rnContribuinte.Analisar_Aceite_Oi(contribuinte, valorConcluir)

            If analiseAceite(1) > 30 Then

                Dim result = MessageBox.Show("A media de aceite para o telefone é de R$" & analiseAceite(0) & " O atual aceite esta com o valor de R$" & valorConcluir & ", a diferença é de " & analiseAceite(1) & "%. Confirma o aceite?", "Atenção", _
                                             MessageBoxButtons.YesNo, _
                                             MessageBoxIcon.Question)


                If (result = DialogResult.Yes) Then
                    If rnContribuinte.Inclui_Numeros_Oi(contribuinte, "S", mesSeguinte, txtMesRef.Text, txtNome.Text) = True Then
                        rnContribuinte.Atualiza_Informacoes_Oi(contribuinte, mesSeguinte, txtMesRef.Text, txtCpf.Text, txtAutorizante.Text, rtxtObs.Text, contribuinte.Valor)
                        If (telefoneErrado <> "") Then
                            rnRequisicao.AtualizaDadosListaTelefonica(contribuinte)
                            rnRequisicao.ApagaRegistroErradosRequisicao(telefoneErrado)
                        End If
                        MsgBox("Registro salvo com sucesso!", MsgBoxStyle.Information, "Atenção")

                    Else
                        MsgBox("Ocorreu um erro na inclusão do registro", MsgBoxStyle.Information, "Atenção")
                    End If
                Else
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If

            Else

                If rnContribuinte.Inclui_Numeros_Oi(contribuinte, "S", mesSeguinte, txtMesRef.Text, txtNome.Text) = True Then
                    rnContribuinte.Atualiza_Informacoes_Oi(contribuinte, mesSeguinte, txtMesRef.Text, txtCpf.Text, txtAutorizante.Text, rtxtObs.Text, contribuinte.Valor)
                    If (telefoneErrado <> "") Then
                        rnRequisicao.AtualizaDadosListaTelefonica(contribuinte)
                        rnRequisicao.ApagaRegistroErradosRequisicao(telefoneErrado)
                    End If
                    MsgBox("Registro salvo com sucesso!", MsgBoxStyle.Information, "Atenção")

                Else
                    MsgBox("Ocorreu um erro na inclusão do registro", MsgBoxStyle.Information, "Atenção")
                End If

            End If

            carregaFeedGridCategoriaContribuinte(cboSituacao.SelectedValue, CodUsuario)

            LimpaTela()

            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try

    End Sub

    Private Sub btnNaoContribuir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNaoContribuir.Click

        Dim contribuinte As New CContribuinte
        Dim rnContribuinte As New RNContribuinte
        Dim RNRequisicao As New RNRequisicao

        Try
            If cboOperadorConcluir.SelectedIndex = 0 Then
                MsgBox("Por favor selecione um operador")
                Exit Sub
            End If

            If cboCategoriaConcluir.SelectedIndex = 0 Then
                MsgBox("Por favor selecione uma categoria")
                Exit Sub
            End If

            If txtCodEanConcluir.Text = "*     -   *" Then
                MsgBox("Por favor digite o código do contribuinte")
                Exit Sub
            End If

            If txtDDDConcluir.Text = "" Then
                MsgBox("Por favor digite o DDD do contribuinte")
                Exit Sub
            End If


            If txtTelefoneConcluir.Text = "" Then
                MsgBox("Por favor digite o telefone do contribuinte")
                Exit Sub
            End If

            If txtValorConcluir.Text = "" Then
                MsgBox("Por favor digite o valor da contribuição")
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor

            contribuinte.NomeCliente1 = txtNomeConcluir.Text
            contribuinte.Telefone1 = txtTelefoneConcluir.Text
            contribuinte.DDD = txtDDDConcluir.Text
            contribuinte.Valor = txtValorConcluir.Text
            contribuinte.Cod_EAN_Cliente = txtCodEanConcluir.Text
            contribuinte.Cod_Operador = cboOperadorConcluir.SelectedValue
            contribuinte.Operador = cboOperadorConcluir.Text
            contribuinte.Cod_Categoria = cboSituacao.SelectedValue


            If rnContribuinte.Inclui_Numeros_Oi(contribuinte, "N", txtMesRef.Text, txtMesRef.Text, txtNome.Text) = True Then
                rnContribuinte.Atualiza_Informacoes_Oi(contribuinte, txtMesRef.Text, txtMesRef.Text, txtCpf.Text, txtAutorizante.Text, rtxtObs.Text, contribuinte.Valor)
                If (telefoneErrado <> "") Then
                    RNRequisicao.AtualizaDadosListaTelefonica(contribuinte)
                    RNRequisicao.ApagaRegistroErradosRequisicao(telefoneErrado)
                End If
                MsgBox("Registro salvo com sucesso!", MsgBoxStyle.Information, "Atenção")

            Else
                MsgBox("Ocorreu um erro na inclusão do registro", MsgBoxStyle.Information, "Atenção")
            End If

            carregaFeedGridCategoriaContribuinte(cboSituacao.SelectedValue, CodUsuario)

            LimpaTela()

            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub


    Private Sub LimpaTela()

        Try



            txtCodBarra.Text = ""
            txtCodigo.Text = ""
            txtNomeCliente1.Text = ""
            txtNomeCliente2.Text = ""
            txtDDD.Text = ""
            txtTelefone.Text = ""
            txtTelefone2.Text = ""
            txtCnpjCpf.Text = ""
            txtRg.Text = ""
            txtMelhorDia.Text = ""
            txtValorConcluir.Text = ""
            txtValDoacao.Text = ""
            txtDtNc.Text = ""
            txtDtNcMp.Text = ""
            txtReferencia.Text = ""
            txtRegiao.Text = ""
            txtObs.Text = ""
            rtxtObs.Text = ""
            txtCEP.Text = ""
            txtUF.Text = ""
            txtEndereco.Text = ""
            txtNome.Text = ""
            txtTelefone1.Text = ""
            txtEmail.Text = ""
            txtCodEanConcluir.Text = ""
            txtNomeConcluir.Text = ""
            txtDDDConcluir.Text = ""
            txtTelefoneConcluir.Text = ""
            txtValorConcluir.Text = ""
            txtCpf.Text = ""
            txtAutorizante.Text = ""
            rtxtObs.Text = ""
            txtDtCadastro.Text = ""

            cboBairroDetalhe.SelectedIndex = 0
            cboCategoriaConcluir.SelectedIndex = 0
            cboCategoriaDetalhe.SelectedIndex = 0
            cboCidade.SelectedIndex = 0
            cboEspecieDetalhe.SelectedIndex = 0
            cboOperadorConcluir.SelectedIndex = 0
            cboOperadorDetalhe.SelectedIndex = 0

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub btnEnviarCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnviarCancelar.Click
        Dim Daogenerico As GenericoDAO = New GenericoDAO
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable
        Try

            Dim result = MessageBox.Show("Deseja enviar o número " & txtTelefoneConcluir.Text & " para cancelamento permanente?, Deseja continuar?", "Atenção", _
                           MessageBoxButtons.YesNo, _
                           MessageBoxIcon.Question)

            Me.Cursor = Cursors.WaitCursor


            If (result = DialogResult.Yes) Then
                ds = Daogenerico.BuscarOperador(CodUsuario)

                If (ds.Tables(0).Rows.Count > 0) Then
                    If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                        dt = ds.Tables(0)
                    End If
                End If

                contribuinte.Cod_Usuario = CodUsuario
                contribuinte.Cod_opera = dt.Rows(0).Item(0)
                contribuinte.Operador = dt.Rows(0).Item(1)


                'PEGA OS VALORES ATUALIZADOS NA TELA
                contribuinte.Telefone1 = txtTelefoneConcluir.Text
                contribuinte.Nome_contribuinte = txtNomeConcluir.Text
                contribuinte.NomeCliente1 = txtNomeConcluir.Text
                contribuinte.DDD = txtDDD.Text
                contribuinte.Telefone1 = txtTelefoneConcluir.Text
                contribuinte.Valor = txtValorConcluir.Text
                contribuinte.Observacao = txtObs.Text



                rnContribuinte.EnviaContribuinteParaCancelamento(contribuinte, CodUsuario)

            End If

            Me.Cursor = Cursors.Default


        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub


    Private Sub btnCancelarMensal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelarMensal.Click
        Dim Daogenerico As GenericoDAO = New GenericoDAO
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable
        Try

            Dim result = MessageBox.Show("Deseja cancelar a contribuição mensal do telefone" & txtTelefoneConcluir.Text & "?, Deseja continuar?", "Atenção", _
                           MessageBoxButtons.YesNo, _
                           MessageBoxIcon.Question)

            Me.Cursor = Cursors.WaitCursor


            If (result = DialogResult.Yes) Then
                ds = Daogenerico.BuscarOperador(CodUsuario)

                If (ds.Tables(0).Rows.Count > 0) Then
                    If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                        dt = ds.Tables(0)
                    End If
                End If

                contribuinte.Cod_Usuario = CodUsuario
                contribuinte.Cod_opera = dt.Rows(0).Item(0)
                contribuinte.Operador = dt.Rows(0).Item(1)


                'PEGA OS VALORES ATUALIZADOS NA TELA
                contribuinte.Telefone1 = txtTelefoneConcluir.Text
                contribuinte.Nome_contribuinte = txtNomeConcluir.Text
                contribuinte.NomeCliente1 = txtNomeConcluir.Text
                contribuinte.DDD = txtDDD.Text
                contribuinte.Telefone1 = txtTelefoneConcluir.Text
                contribuinte.Valor = txtValorConcluir.Text
                contribuinte.Observacao = txtObs.Text



                rnContribuinte.EnviaContribuinteParaCancelamentoMensal(contribuinte)

            End If

            carregaFeedGridCategoriaContribuinte(cboSituacao.SelectedValue, CodUsuario)

            Me.Cursor = Cursors.Default


        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub
End Class