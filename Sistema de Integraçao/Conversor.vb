﻿Imports System.IO
Imports System.Text
Public Class Conversor
    Public NomeArquivo As String

    Public Sub New(ByVal NomeArquivo As String)
        Me.NomeArquivo = NomeArquivo
    End Sub

    Public Function getColunas(ByVal nomeColunas As Boolean) As String()
        Try
            Dim leitorArquivo As New StreamReader(NomeArquivo)
            Dim linha As String = leitorArquivo.ReadLine
            leitorArquivo.Close()
            Dim Colunas() As String = linha.Split(";")
            If nomeColunas Then
                Return Colunas
            End If
            Dim i As Integer = 1
            Dim c As Integer = 0
            Dim nome_Colunas(Colunas.Length - 1) As String
            For Each column As String In Colunas
                nome_Colunas(c) = "coluna" & i
                i += 1
                c += 1
            Next
            Return nome_Colunas
        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function

    Public Function ConverteCSVParaDataTable(ByVal nomeColunas As Boolean) As DataTable
        Try
            Dim dt As New DataTable
            For Each nome_Coluna In getColunas(nomeColunas)
                dt.Columns.Add(nome_Coluna)
            Next
            Dim leitorArquivo As New StreamReader(NomeArquivo)
            If nomeColunas Then
                leitorArquivo.ReadLine()
            End If
            Dim linha As String = leitorArquivo.ReadLine
            While Not IsNothing(linha)
                linha = linha.Replace(Chr(34), "")
                dt.Rows.Add(linha.Split(";"))
                linha = leitorArquivo.ReadLine
            End While
            leitorArquivo.Close()
            Return dt
        Catch ex As Exception
            Throw ex
        End Try
        Return Nothing
    End Function

    Public Function ConverteTabelaParaCSV(ByVal _tabela As DataTable, ByVal _nomeArquivo As String,
ByVal sepChar As String) As Boolean

        Dim writer As StreamWriter = Nothing
        Try
            writer = New StreamWriter(_nomeArquivo)
            ' primeiro escreva a linha com o nome das colunas
            Dim separador As String = ""
            Dim builder As New StringBuilder
            For Each col As DataColumn In _tabela.Columns
                builder.Append(separador).Append(col.ColumnName)
                separador = sepChar
            Next
            writer.WriteLine(builder.ToString())
            ' escrever todas as linhas
            For Each row As DataRow In _tabela.Rows
                separador = ""
                builder = New StringBuilder
                For Each col As DataColumn In _tabela.Columns
                    builder.Append(separador).Append(row(col.ColumnName))
                    separador = sepChar
                Next
                writer.WriteLine(builder.ToString())
            Next
            Return True
        Catch
            Return False
        Finally
            If Not writer Is Nothing Then writer.Close()
        End Try
    End Function
End Class