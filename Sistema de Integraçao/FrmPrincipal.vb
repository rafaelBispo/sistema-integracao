﻿Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports FTP_Upload_Download
Imports CrystalDecisions.Shared
Imports CrystalDecisions.CrystalReports.Engine

Public Class FrmPrincipal

#Region "Variaveis"

    Dim v_ambiente As String
    Dim mNivelAcesso As Integer
    Dim mUsuario As String
    Dim mCodUsuario As Integer
    Dim lstRequisicao As New SortableBindingList(Of CGenerico)
    Dim exportacao_automatica As Boolean = False
    Public NomeArquivo As String


    Public Property NivelAcesso() As Integer
        Get
            Return mNivelAcesso
        End Get
        Set(ByVal value As Integer)
            mNivelAcesso = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return mUsuario
        End Get
        Set(ByVal value As String)
            mUsuario = value
        End Set
    End Property

    Public Property CodUsuario() As Integer
        Get
            Return mCodUsuario
        End Get
        Set(ByVal value As Integer)
            mCodUsuario = value
        End Set
    End Property

#End Region

    Private Sub instanciaVariaviesGlobais()

        Dim mensagem As New Util.ConnectionHelper(System.Reflection.Assembly.GetExecutingAssembly)
        Dim v_ambiente As String

        v_ambiente = mensagem.RetornaMenssage()

    End Sub

    Private Sub FrmPrincipal_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

        If exportacao_automatica = False Then
            If Date.Now.Hour = 16 And Date.Now.Minute = 10 And Date.Now.Day Mod 2 = 0 Then
                If GeraArquivoExportar() = True Then
                    EnviaEmail()
                    exportacao_automatica = True
                End If
            End If
        End If


    End Sub

#Region "Exportar arquivo"
    Public Function GeraArquivoExportar() As Boolean
        Dim dsArquivo As DataSet = New DataSet
        Dim rnGenerico As New RNGenerico
        Dim tipoArquivo As String = ""
        Dim objeto As String = ""
        Dim diaAtual As Date
        Dim diaAnterior As Date


        Try
            Me.Cursor = Cursors.WaitCursor


            diaAtual = Date.Today
            diaAnterior = diaAtual.AddDays(-1)

            tipoArquivo = "ROI_"
            dsArquivo = rnGenerico.CriaArquivoRequisicaoOiExportar(diaAnterior, diaAtual)

            If dsArquivo.Tables(0).Rows.Count > 0 Then

                NomeArquivo = "C:\Digital\" + tipoArquivo + diaAtual + "_ate_" + diaAnterior + ".txt"


                Dim SW As New StreamWriter(NomeArquivo) ' Cria o arquivo de texto


                For Each row In dsArquivo.Tables(0).Rows 'select do detalhe
                    Dim linha As String = ""
                    For Each obj In row.ItemArray

                        objeto = obj.ToString()
                        'objeto = objeto.Replace(ControlChars.Back, "").ToString
                        'objeto = objeto.Replace(ControlChars.Cr, "").ToString
                        'objeto = objeto.Replace(ControlChars.CrLf, "").ToString
                        'objeto = objeto.Replace(ControlChars.Lf, "").ToString
                        objeto = objeto.Replace(Chr(10), "")
                        objeto = objeto.Replace(Chr(13), "")

                        If linha = "" Then
                            linha = "|" + obj.ToString()
                        Else
                            linha = linha + ";" + obj.ToString()
                        End If

                    Next

                    SW.WriteLine(linha) ' Grava o detalhe

                Next

                SW.Close() 'Fecha o arquivo de texto

                SW.Dispose() 'Libera a memória utilizada

            Else
                Me.Cursor = Cursors.Default

                Return False
            End If

            Me.Cursor = Cursors.Default

            Return True

        Catch ex As Exception

            'daoContribuinte.RollBackTransaction()
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Me.Cursor = Cursors.Default
            Return False

        End Try


    End Function

    Private Sub EnviaEmail()
        Try

            'create the mail message
            Dim SmtpServer As New SmtpClient()
            Dim mail As New MailMessage()
            SmtpServer.Credentials = New Net.NetworkCredential("aaci.jf.mg@gmail.com", "kwcmuyymqekemvtz")
            SmtpServer.Host = "smtp.gmail.com"
            SmtpServer.Port = 587 'server para gmail
            SmtpServer.EnableSsl = True
            mail.From = New MailAddress("aaci.jf.mg@gmail.com")
            mail.To.Add("aaci.jf.mg@gmail.com") 'Email que recebera a Mensagem
            mail.Subject = "Exportação Requisicao"
            mail.Body = ""

            'Este codigo PlainView e caso o email não suporte Html, nisto ele irá informar este erro

            Dim plainView As AlternateView = AlternateView.CreateAlternateViewFromString("Segue em anexo o arquivo de exportação.", Nothing, "text/plain")

            'crie um corpo de texto caso deseje assim você poderá colocar um texto no meio de duas imagens
            Dim linha1 As String = ""
            If Date.Now.Hour < 12 Then
                linha1 = "Bom dia,"
            Else
                linha1 = "Boa tarde,"
            End If

            Dim linha2 As String = "como solicitado, segue a planilha com as ligações efetuadas no periodo solicitado."
            Dim linha3 As String = "Atenciosamente."

            Dim htmlView As AlternateView = AlternateView.CreateAlternateViewFromString("<p> " + linha1 + "</p> <p>&nbsp;</p> <p>" + linha2 + "</p> <p>&nbsp;</p> <img src=cid:companylogo> <p>&nbsp;</p> <p>" + linha3 + "</p> <p><br /> <br /> --</p> <p>&nbsp;</p> <p>att..</p> <p>&nbsp;</p> <p><strong>AACI - ASSOCIA&Ccedil;&Atilde;O DE APOIO AS CRIAN&Ccedil;AS E IDOSOS</strong></p>   ", Nothing, "text/html")


            mail.Attachments.Add(New Attachment(NomeArquivo)) 'Adiciona o anexo a enviar

            With mail
                .AlternateViews.Add(plainView)
                .AlternateViews.Add(htmlView)

            End With

            mail.IsBodyHtml = True

            SmtpServer.Send(mail)
            MsgBox("E-mail enviado com sucesso", vbInformation, "Ok")





        Catch ex As Exception  'caso haja algum erro crie uma msg de aviso.
            MsgBox(ex)
        Finally
            '  File.Delete("")
        End Try
    End Sub
#End Region

   


    Private Sub FrmPrincipal_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim mensagem As New Util.ConnectionHelper(System.Reflection.Assembly.GetExecutingAssembly)
        Dim rn As RNContribuinte = New RNContribuinte

        If NivelAcesso = 6 Then
            btnRetorno.Visible = False
            btnRemessa.Visible = False
            btnGerencial.Visible = False
            SuporteToolStripMenuItem.Visible = False
            RelatôriosToolStripMenuItem.Visible = False
            btnConsultaRequisicoes.Visible = False
            ListaTelefonicaToolStripMenuItem.Visible = False
            UsuarioToolStripMenuItem.Visible = False
            btnRecuperacao.Visible = False
            btnConsulta.Visible = False
            AuditoriaOiToolStripMenuItem.Visible = False
            FTPToolStripMenuItem.Visible = False
            RequisiçãoToolStripMenuItem.Visible = False
            GerêncialToolStripMenuItem.Visible = False
            RemessaToolStripMenuItem.Visible = False
            ExportarDadosToolStripMenuItem.Visible = False
        ElseIf NivelAcesso = 1 Then
            btnRetorno.Visible = True
            btnRemessa.Visible = True
            btnGerencial.Visible = True
            SuporteToolStripMenuItem.Visible = True
            RelatôriosToolStripMenuItem.Visible = True
            UsuarioToolStripMenuItem.Visible = True
            btnConsultaRequisicoes.Visible = True
            ListaTelefonicaToolStripMenuItem.Visible = True
            btnRecuperacao.Visible = True
            AuditoriaOiToolStripMenuItem.Visible = True
            FTPToolStripMenuItem.Visible = True
            RequisiçãoToolStripMenuItem.Visible = True
            GerêncialToolStripMenuItem.Visible = True
            RemessaToolStripMenuItem.Visible = True
            ExportarDadosToolStripMenuItem.Visible = True
        End If


        stripUsuario.Text = "USUARIO: " + mUsuario

        instanciaVariaviesGlobais()
        CarregaComboCategorias(0)

        '  carregaValoreAtuais()

        '    rn.AtualizaTabelaContribuinte()

    End Sub

    Private Sub btnContribuinte_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmContribuinte.Show()

    End Sub

    Private Sub btnRemessa_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemessa.Click
        FrmGerarRemessa.Show()


    End Sub

    Private Sub btnRetorno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRetorno.Click
        FrmRetorno.Show()
    End Sub

    Private Sub btnConsulta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsulta.Click
        FrmConsultaRecuperacao.Show()
    End Sub


    Private Sub btnIncluirManual_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIncluirManual.Click
        FrmInclusaoManual.Show()
    End Sub

    Private Sub btnConsCat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConsCat.Click
        FrmConsultaUltimaMovimentacao.Usuario = Usuario
        FrmConsultaUltimaMovimentacao.CodUsuario = CodUsuario
        FrmConsultaUltimaMovimentacao.Show()
    End Sub

    Private Sub btnGerencial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGerencial.Click
        FrmControleUsuario.Show()
    End Sub

    Private Sub FrmPrincipal_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Application.Exit()
    End Sub

    Private Sub btnConsultaRequisicoes_Click(sender As System.Object, e As System.EventArgs) Handles btnConsultaRequisicoes.Click
        Dim Frm = New FrmConsultaRequisicaoRealizada


        Frm.Usuario = Usuario
        Frm.CodUsuario = CodUsuario
        Frm.Show()
    End Sub

    Private Sub btnRecuperacao_Click(sender As System.Object, e As System.EventArgs) Handles btnRecuperacao.Click
        FrmRecuperacao.Show()
    End Sub

    Private Sub btnCaixa_Click(sender As System.Object, e As System.EventArgs)
        FrmAcertoCaixa.Show()
    End Sub

    Private Sub CadastroContribuinteToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles CadastroContribuinteToolStripMenuItem.Click
        frmContribuinte.Show()
    End Sub

    Private Sub TrocarDeUsuárioToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles TrocarDeUsuárioToolStripMenuItem.Click
        LoginForm.Show()
        Me.Close()
    End Sub

    Private Sub SuporteToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SuporteToolStripMenuItem.Click
        Dim resultado As Boolean
        Dim rn As RNContribuinte = New RNContribuinte
        Try

            MsgBox("O processamento requerido pode ocasionar inconsistência no banco de dados, precisamente na tabela de clientes, deseja continuar?", MsgBoxStyle.YesNo, "Atenção")
            If Windows.Forms.DialogResult.Yes Then
                resultado = rn.AtualizaTabelaCliente()
            End If

            If resultado = True Then

                MsgBox("Base atualizada com sucesso!")

            Else

                MsgBox("Ocorreu um erro na atualização da base, entre em contato com o responsável!")

            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub RelatôriosToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles RelatôriosToolStripMenuItem.Click
        'FrmFiltroRelatorios.Show()
    End Sub

    Private Sub ListaTelefonicaToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ListaTelefonicaToolStripMenuItem.Click
        FrmListaTelefonica.Show()
    End Sub

    Private Sub ImprimirFichasManualToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ImprimirFichasManualToolStripMenuItem.Click

        'Dim frm As FrmRelatorios = New FrmRelatorios

        'Try
        '    frm.NomeRelatorio = "FichasOperador"
        '    frm.Show()

        'Catch ex As Exception
        '    MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        'End Try
    End Sub

    Private Sub ExcluirContribuinteToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ExcluirContribuinteToolStripMenuItem.Click
        frmTelefoneExclusao.Show()
    End Sub

    Private Sub UsuarioToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UsuarioToolStripMenuItem.Click
        Dim frm As FrmCadUsuario = New FrmCadUsuario()
        frm.Show()
    End Sub

    Private Sub AnaliseExclusãoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AnaliseExclusãoToolStripMenuItem.Click
        FrmAnaliseExclusaoContribuinte.Show()
    End Sub


#Region "Resumo"


    Private Sub CarregaComboCategorias(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCategoria.DataSource = Nothing
            cboCategoria.Items.Clear()
            cboCategoria.DisplayMember = "Nome_Categoria"
            cboCategoria.ValueMember = "Cod_Categoria"
            cboCategoria.DataSource = rn_generico.BuscarCategoriaPorOperador(CodUsuario)

            If cboCategoria.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoria.SelectedIndex = 0
            ElseIf cboCategoria.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoria.Items.Count - 1
                    For Each linha As CGenerico In cboCategoria.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoria.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoria.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub


    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""
        Dim tipoPesquisa As Integer = 0

        Try
            Me.Cursor = Cursors.WaitCursor

            carregaGridRequisicoesPorPeriodo(dtInicial.Text, dtFinal.Text, 0, cboCategoria.SelectedValue)


            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub carregaGridRequisicoesPorPeriodo(ByVal dtInicial As Date, ByVal dtFinal As Date, ByVal tipoAceite As Integer, ByVal categoria As Integer)

        Dim objGenerico As CGenerico = New CGenerico()
        Dim totalValorFichas As Integer
        Dim valor As Single
        Dim strValor As String
        Dim rnGenerico As New RNGenerico
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable

        Try

            ds = rnGenerico.CarregarRequisicaoPorPeriodoDataCategoria(dtInicial, dtFinal, tipoAceite, categoria, CodUsuario)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If

            For Each row As DataRow In dt.Rows

                totalValorFichas = row.Item(0)
                lblSolicitacaoAceita.Text = row.Item(1)
                lblSolicitacaoFila.Text = row.Item(2)


            Next

            strValor = valor.ToString("C")
            valor = totalValorFichas
            lblValor.Text = valor.ToString("R$ #,###.00")



        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub carregaValoreAtuais()

        Dim objGenerico As CGenerico = New CGenerico()
        Dim totalValorFichasMensal As Decimal
        Dim totalValorFichasDiario As Decimal
        Dim valor As Single
        Dim strValor As String
        Dim rnGenerico As New RNGenerico
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable

        Try

            'CARREGA OS VALORES MENSAIS

            ds = rnGenerico.CarregarValoresAtuaisMes(CodUsuario)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If

            For Each row As DataRow In dt.Rows

                totalValorFichasMensal = row.Item(0)
                lblSolicitacaoAceitaMensal.Text = row.Item(1)
                lblSolicitacaoFilaMensal.Text = row.Item(2)


            Next

            strValor = valor.ToString("C")
            valor = totalValorFichasMensal
            lblValorMensal.Text = valor.ToString("R$ #,###.00")

            ds.Clear()
            dt.Clear()

            'CARREGA OS VALORES DIARIOS

            ds = rnGenerico.CarregarValoresAtuaisDia(CodUsuario)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If

            For Each row As DataRow In dt.Rows

                totalValorFichasDiario = row.Item(0)
                lblSolicitacaoAceitaDiario.Text = row.Item(1)
                lblSolicitacaoFilaDiario.Text = row.Item(2)


            Next

            strValor = valor.ToString("C")
            valor = totalValorFichasDiario
            lblValorDiario.Text = valor.ToString("R$ #,###.00")



        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

  
    Private Sub btnAtualizar_Click(sender As System.Object, e As System.EventArgs) Handles btnAtualizar.Click
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""
        Dim tipoPesquisa As Integer = 0

        Try
            Me.Cursor = Cursors.WaitCursor

            carregaValoreAtuais()


            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

#End Region

    Private Sub AuditoriaOiToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles AuditoriaOiToolStripMenuItem.Click
        FrmAuditoriaOi.Show()
    End Sub

    Private Sub RelatórioOperadorasToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles RelatórioOperadorasToolStripMenuItem.Click
        FrmRelatorioOperador.Show()
    End Sub

    Private Sub AmostraSemanalToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AmostraSemanalToolStripMenuItem.Click
        'Dim frm As Form1 = New Form1()
        'frm.Show()
    End Sub

    Private Sub LigaçõesContestadosToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LigaçõesContestadosToolStripMenuItem.Click
        'Dim frm As FrmUploadLigacoes = New FrmUploadLigacoes()
        'frm.Show()
    End Sub

    Private Sub ListasNovasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListasNovasToolStripMenuItem.Click
        Dim frm As FrmListaNovaOi = New FrmListaNovaOi()
        frm.Show()
    End Sub

    Private Sub ConsultaRemessaEnviadaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ConsultaRemessaEnviadaToolStripMenuItem.Click
        Dim frm As frmArquivoRemessa = New frmArquivoRemessa()
        frm.Show()
    End Sub

    Private Sub PrevisãoRemessaToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PrevisãoRemessaToolStripMenuItem.Click
        Dim frm As FrmPrevisaoRemessa = New FrmPrevisaoRemessa()
        frm.Show()
    End Sub

    Private Sub PlanilhaOperadorasToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PlanilhaOperadorasToolStripMenuItem.Click
        FrmGerarExcel.Show()
    End Sub

    Private Sub ExportarImportarInformaçõesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExportarImportarInformaçõesToolStripMenuItem.Click
        Dim frm As FrmExportar = New FrmExportar()
        frm.Show()
    End Sub

    Private Sub ConsultaCriticadosEContestadosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConsultaCriticadosEContestadosToolStripMenuItem.Click
        Dim frm As FrmConsultaRetornoOi = New FrmConsultaRetornoOi()
        frm.Show()
    End Sub
End Class