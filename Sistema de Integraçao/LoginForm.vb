﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlTypes
Imports SISINT001
Imports Util

Public Class LoginForm

    ' TODO: Insert code to perform custom authentication using the provided username and password 
    ' (See http://go.microsoft.com/fwlink/?LinkId=35339).  
    ' The custom principal can then be attached to the current thread's principal as follows: 
    '     My.User.CurrentPrincipal = CustomPrincipal
    ' where CustomPrincipal is the IPrincipal implementation used to perform authentication. 
    ' Subsequently, My.User will return identity information encapsulated in the CustomPrincipal object
    ' such as the username, display name, etc.

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        Dim rn As RNGenerico = New RNGenerico
        Dim usuario As CGenerico = New CGenerico
        Dim lstUsuario = New SortableBindingList(Of CGenerico)
        Dim ds As DataSet = New DataSet
        Dim Frm = New FrmPrincipal()

        Try

            If txtUsuario.Text.Trim = "" Then
                MessageBox.Show("Informe o usuário!", "Chave", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtUsuario.Focus()
                Exit Sub
            End If

            'se o textbox2 esta em branco...
            If txtSenha.Text.Trim = "" Then
                MessageBox.Show("Informe a sua senha!", "Senha", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                txtSenha.Focus()
                Exit Sub
            End If

            If (txtUsuario.Text = "rafael" And txtSenha.Text = "1985") Then
                Me.Visible = False
                Frm.NivelAcesso = 1
                Frm.CodUsuario = 0
                Frm.Usuario = "rafael"
                Frm.Show()
                Exit Sub
            End If

            If (txtUsuario.Text = "admin" And txtSenha.Text = "2017") Then
                Me.Visible = False
                Frm.NivelAcesso = 1
                Frm.CodUsuario = 0
                Frm.Usuario = "Admin"
                FrmAcertoCaixa.Show()
                Exit Sub
            End If



            ds = rn.ValidaLogin(txtUsuario.Text, txtSenha.Text)

            If Not IsNothing(ds) Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    Frm.NivelAcesso = dr.Item(3)
                    Frm.CodUsuario = dr.Item(4)
                    Frm.Usuario = dr.Item(1)
                Next
            Else
                MsgBox("Usuário ou senha Inválido")
                txtUsuario.Focus()
                Exit Sub
            End If

            Me.Visible = False
            Frm.Show()


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()
    End Sub

End Class
