﻿Imports System.IO
Imports SISINT001

Public Class FrmRetorno

#Region "Variaveis Globais"

    Dim nome_arquivo As String
    Dim jaGravou As Boolean

#End Region

#Region "Enum"

    Private Enum ColunasGridRemessa

        UsTipoRegistroD = 0
        UiCodServicoArbor = 1
        UiCodLocalidadeCobranca = 2
        StDDDMeioAcessoCobranca = 3
        StNumMeioAcessoCobranca = 4
        DVlrServico = 5
        DdDataServico = 6
        UsNumParcela = 7
        UsQteParcela = 8
        UiDataInicioCobranca = 9
        UcSinal = 10
        UiAnoMesConta = 11
        StDescReferencia = 12
        DNumNotaFiscal = 13
        DdDataVencimento = 14
        DdDataSituacao = 15
        DVlrFaturado = 16
        StCodigoMovimentacao = 17
        UsSitFaturamento = 18
        UiMotivoSituacao = 19
        UiAnoMesContaMulta = 20
        StNumAssinanteMovel = 21
        SistemaFaturamento = 22
        Ciclo = 23
        UsTipoRegistroT = 24
        QuantidadeDeRegistro = 25
        ValorArquivo = 26

    End Enum

    Enum xlsOption
        xlsOpen
    End Enum

#End Region


    Private Sub btnProcessar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcessar.Click
        'Declara uma variável do tipo StreamWriter
        Dim Leitura As StreamReader
        Dim rnRetorno As RNRetorno = New RNRetorno
        Dim retornoList As List(Of CRetorno) = New List(Of CRetorno)
        Dim HeaderList As List(Of CRetorno) = New List(Of CRetorno)
        Dim TraillerList As List(Of CRetorno) = New List(Of CRetorno)
        Dim GravarList As List(Of CRetorno) = New List(Of CRetorno)
        Dim usuario As String = ""
        Dim id_arq As Integer = 0
        Dim nome_arq As String = ""

        Try

        
            If chkProcessarTodos.Checked = False Then

                'Verifica se já existe um arquivo com este nome
                If File.Exists(txtCaminho.Text) = False Then
                    MsgBox("Este arquivo não existe")
                    Exit Sub
                End If

                Me.Cursor = Cursors.WaitCursor



                'Cria um novo arquivo de acordo com o nome digitado e passa para o objeto StremWriter
                Leitura = File.OpenText(txtCaminho.Text)


                nome_arq = System.IO.Path.GetFileName(OpenFileDialog.FileName)


                'Declara as variáveis dos campos
                Dim strLinha As String

                'retorna id do arquivo
                id_arq = rnRetorno.RetornaIdArquivo()

                'Faz um loop dentro das linhas do arquivo
                While (Leitura.Peek() > -1)

                    'Parra toda a linha atual para uma variável 
                    strLinha = Leitura.ReadLine

                    If (strLinha.Substring(0, 1) = "1") Then
                        HeaderList.Add(rnRetorno.LeituraArquivo(strLinha))
                    ElseIf (strLinha.Substring(0, 1) = "3") Then
                        TraillerList.Add(rnRetorno.LeituraArquivo(strLinha))
                    Else
                        retornoList.Add(rnRetorno.LeituraArquivo(strLinha))
                    End If

                    If jaGravou = False Then
                        'preenche a lista com todas as informações do arquivo
                        GravarList.Add(rnRetorno.LeituraArquivo(strLinha))
                    End If

                End While

                Leitura.Dispose()
                Leitura.Close()

                gridRetorno.DataSource = retornoList
                lblDataArquivo.Text = HeaderList.Item(0).DdDataGeracao
                lblTotalArquivo.Text = TraillerList.Item(0).ValorArquivo
                lblTotalReg.Text = TraillerList.Item(0).QuantidadeDeRegistro


                For Each reg As CRetorno In GravarList
                    rnRetorno.GravaDadosArquivo(reg, nome_arquivo, usuario, id_arq)
                Next


                MsgBox("Base atualizada com sucesso")
                lblStatusArquivo.Text = "Arquivo Processado"

                Me.Cursor = Cursors.Default


                'ATUALIZAR OS VALORES DO RESUMO
                'CarregaFaturamento(rnRetorno.DataRemessa)
                CarregaFaturamento(DtMesAnoPesquisa.Text)

                'CRIA O DIRETORIO PARA SALVAR OS ARQUIVOS PROCESSADOS
                My.Computer.FileSystem.CreateDirectory(
              "C:\Processados")

                'MOVE OS ARQUIVOS PROCESSADOS
                '  My.Computer.FileSystem.MoveFile(txtCaminho.Text,
                '"C:\Processados" + "\" + nome_arq)

                Dim arquivoADeletar As String = ""
                arquivoADeletar = txtCaminho.Text
                Leitura.Dispose()
                Leitura.Close()
                FileClose(1)
                'Leitura = File.OpenText("C:\Sistema de Integraçao\Arquivos\Processar\ArquivoMorto")
                'nome_arq = System.IO.Path.GetFileName("C:\Sistema de Integraçao\Arquivos\Processar\ArquivoMorto")
                Leitura.Dispose()
                Leitura.Close()
                FileClose(1)
                My.Computer.FileSystem.DeleteFile(arquivoADeletar)


            Else
                'processar arquivo em lote

                Me.Cursor = Cursors.WaitCursor

                Dim di As New IO.DirectoryInfo(txtCaminho.Text)

                Dim diar1 As IO.FileInfo() = di.GetFiles()

                Dim dra As IO.FileInfo
                Dim count As Integer = 0

                ProgressBar.Value = 0
                ProgressBar.Minimum = 0
                ProgressBar.Maximum = diar1.Length

                For Each dra In diar1

                    lblArquivosLote.Text = "Processando arquivo: " + count.ToString() + " de " + diar1.Length.ToString() + ""

                    If rnRetorno.IsArquivoProcessado(dra.ToString()) = True Then
                        'lblStatusArquivo.Text = "Arquivo processado"
                        'lblStatusArquivo.ForeColor = Color.Red
                        'jaGravou = True

                        'MsgBox("Arquivo já processado, o reprocessamento pode ocasionar, em inconsistências nos registros, Deseja continuar?", MsgBoxStyle.YesNo, "Atenção")


                        My.Computer.FileSystem.DeleteFile(txtCaminho.Text + "\" + dra.ToString())

                    Else


                        'Cria um novo arquivo de acordo com o nome digitado e passa para o objeto StremWriter
                        Leitura = File.OpenText(txtCaminho.Text + "\" + dra.ToString())


                        nome_arq = dra.ToString()


                        'Declara as variáveis dos campos
                        Dim strLinha As String

                        'retorna id do arquivo
                        id_arq = rnRetorno.RetornaIdArquivo()

                        'Faz um loop dentro das linhas do arquivo
                        While (Leitura.Peek() > -1)

                            'Parra toda a linha atual para uma variável 
                            strLinha = Leitura.ReadLine

                            If (strLinha.Substring(0, 1) = "1") Then
                                HeaderList.Add(rnRetorno.LeituraArquivo(strLinha))
                            ElseIf (strLinha.Substring(0, 1) = "3") Then
                                TraillerList.Add(rnRetorno.LeituraArquivo(strLinha))
                            Else
                                retornoList.Add(rnRetorno.LeituraArquivo(strLinha))
                            End If


                            'preenche a lista com todas as informações do arquivo
                            GravarList.Add(rnRetorno.LeituraArquivo(strLinha))


                        End While

                        Leitura.Dispose()

                        gridRetorno.DataSource = retornoList

                        If retornoList.Count > 0 Then

                            lblDataArquivo.Text = HeaderList.Item(0).DdDataGeracao
                            lblTotalArquivo.Text = TraillerList.Item(0).ValorArquivo
                            lblTotalReg.Text = TraillerList.Item(0).QuantidadeDeRegistro

                            nome_arquivo = nome_arq

                            For Each reg As CRetorno In GravarList
                                rnRetorno.GravaDadosArquivo(reg, nome_arquivo, usuario, id_arq)
                            Next
                            GravarList.Clear()

                            ' MsgBox("Base atualizada com sucesso")
                            lblStatusArquivo.Text = "Arquivo Processado"

                        End If

                        Me.Cursor = Cursors.Default

                        count = count + 1

                        ProgressBar.Value = count


                        'CRIA O DIRETORIO PARA SALVAR OS ARQUIVOS PROCESSADOS
                        My.Computer.FileSystem.CreateDirectory("C:\Processados")

                        'MOVE OS ARQUIVOS PROCESSADOS
                        'My.Computer.FileSystem.MoveFile(txtCaminho.Text + "\" + dra.ToString(), "C:\Processados" + "\" + dra.ToString())
                        'System.IO.File.Delete(txtCaminho.Text + "\" + dra.ToString())
                        My.Computer.FileSystem.DeleteFile(txtCaminho.Text + "\" + dra.ToString())

                    End If

                Next

                MsgBox("Arquivos processados com sucesso!!")
                Me.Cursor = Cursors.Default

                'ATUALIZAR OS VALORES DO RESUMO
                'CarregaFaturamento(rnRetorno.DataRemessa)
                CarregaFaturamento(DtMesAnoPesquisa.Text)
            End If



        Catch ex As Exception
            ' Show the exception's message.
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click

        FrmConsultaArquivo.Show()

    End Sub

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click

        Dim rnRetorno As RNRetorno = New RNRetorno


        Try
            btnProcessar.Enabled = True

            If chkProcessarTodos.Checked = False Then

                If OpenFileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                    Dim sr As New System.IO.StreamReader(OpenFileDialog.FileName)
                    txtCaminho.Text = OpenFileDialog.FileName.ToString()

                    If OpenFileDialog.SafeFileName.Substring(10, 6) = "MOVCD2" Then
                        lblArquivo.Text = "Curso Normal"
                    Else
                        lblArquivo.Text = "Criticas"
                    End If

                    nome_arquivo = OpenFileDialog.SafeFileName.ToString()


                    If rnRetorno.IsArquivoProcessado(nome_arquivo) = True Then
                        lblStatusArquivo.Text = "Arquivo processado"
                        lblStatusArquivo.ForeColor = Color.Red
                        jaGravou = True

                        Dim result = MessageBox.Show("Arquivo já processado, Deseja reprocessar o arquivo?", "Atenção", _
                                MessageBoxButtons.YesNo, _
                                MessageBoxIcon.Question)

                        ' MsgBox("Arquivo já processado, Deseja reprocessar o arquivo?", MsgBoxStyle.YesNo, "Atenção")

                        If (result = DialogResult.Yes) Then
                            btnProcessar.Enabled = True

                            rnRetorno.ApagaArquivoProcessado(nome_arquivo)


                        Else
                            lblStatusArquivo.Text = "Arquivo processado"
                            lblStatusArquivo.ForeColor = Color.Red
                            jaGravou = False

                            sr.Close()
                            sr.Close()

                            'CRIA O DIRETORIO PARA SALVAR OS ARQUIVOS PROCESSADOS
                            My.Computer.FileSystem.CreateDirectory(
                          "C:\Processados")
                            sr.Close()

                            'MOVE OS ARQUIVOS PROCESSADOS
                            My.Computer.FileSystem.MoveFile(txtCaminho.Text,
                          "C:\Processados" + "\" + nome_arquivo)

                        End If
                        lblStatusArquivo.Text = "Arquivo processado"
                        lblStatusArquivo.ForeColor = Color.Blue
                        jaGravou = False
                    End If
                End If

            Else




                If FolderBrowserDialog1.ShowDialog() = DialogResult.OK Then
                    'Dim sr As New System.IO.StreamReade(FolderBrowserDialog1.SelectedPath)
                    txtCaminho.Text = FolderBrowserDialog1.SelectedPath.ToString()
                End If

            End If


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub



    Private Sub CriarColunasGrid()

        Try



            gridRetorno.DataSource = Nothing
            gridRetorno.Columns.Clear()

            gridRetorno.AutoGenerateColumns = False

            gridRetorno.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            '0

            Dim checkboxColumn As New DataGridViewCheckBoxColumn
            checkboxColumn.Width = 32
            checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            gridRetorno.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UsTipoRegistroD"
            column.HeaderText = "Tipo Registro"
            column.Width = 60
            column.ReadOnly = True
            column.Visible = False
            gridRetorno.Columns.Add(column)


            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UiCodServicoArbor"
            column.HeaderText = "Cod Serviço Arbor"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = False
            gridRetorno.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UiCodLocalidadeCobranca"
            column.HeaderText = "Cod Localidade Cobrança"
            column.Width = 80
            column.ReadOnly = True
            column.Visible = False
            gridRetorno.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StDDDMeioAcessoCobranca"
            column.HeaderText = "DDD"
            column.Width = 50
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StNumMeioAcessoCobranca"
            column.HeaderText = "Telefone Cobrança"
            column.Width = 100
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DVlrServico"
            column.HeaderText = "Valor Serviço"
            column.Visible = True
            column.DefaultCellStyle.Format = "0.00"
            column.Width = 80
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '7
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DdDataServico"
            column.HeaderText = "Data Servico"
            column.Width = 100
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)


            '8
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UsNumParcela"
            column.HeaderText = " Num Parcela"
            column.Visible = False
            column.Width = 50
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)


            '8
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UsQteParcela"
            column.HeaderText = "UsQteParcela"
            column.Width = 40
            column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            column.Visible = False
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)


            '9
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UiDataInicioCobranca"
            column.HeaderText = "UiDataInicioCobranca"
            column.Visible = False
            column.Width = 100
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '10
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UcSinal"
            column.HeaderText = "UcSinal"
            column.Visible = False
            column.Width = 60
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '11
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UiAnoMesConta"
            column.HeaderText = "Ano Mes Conta"
            column.Width = 100
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '12
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StDescReferencia"
            column.HeaderText = "Descrição Cliente"
            column.Width = 150
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)


            '13
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DNumNotaFiscal"
            column.HeaderText = "Num Nota Fiscal"
            column.Visible = True
            column.Width = 100
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '14
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DdDataVencimento"
            column.HeaderText = "DdDataVencimento"
            column.Visible = True
            column.Width = 100
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '15
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DdDataSituacao"
            column.HeaderText = "Data Situacao"
            column.Visible = True
            column.Width = 100
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)


            '16
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DVlrFaturado"
            column.HeaderText = "Valor Faturado"
            column.Visible = True
            column.Width = 70
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)


            '17
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StCodigoMovimentacao"
            column.HeaderText = "Código Movimentação"
            column.Visible = True
            column.Width = 70
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)


            '18
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UsSitFaturamento"
            column.HeaderText = "Situação Faturamento"
            column.Visible = True
            column.Width = 100
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '19
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UiMotivoSituacao"
            column.HeaderText = "Motivo Situação"
            column.Visible = True
            column.Width = 150
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '20
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UiAnoMesContaMulta"
            column.HeaderText = "Ano Mês Conta Multa"
            column.Visible = True
            column.Width = 100
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)


            '21
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StNumAssinanteMovel"
            column.HeaderText = "StNumAssinanteMovel"
            column.Visible = True
            column.Width = 70
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '22
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "SistemaFaturamento"
            column.HeaderText = "Sistema Faturamento"
            column.Visible = True
            column.Width = 70
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '23
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Ciclo"
            column.HeaderText = "Ciclo"
            column.Visible = True
            column.Width = 70
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub FrmRetorno_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim rnretorno As New RNRetorno

        Try


            CriarColunasGrid()
            'CarregaFaturamento(Date.Now.Month + "/" + Date.Now.Year)
            'CarregaFaturamento(rnretorno.DataRemessa)
            CarregaFaturamento(DtMesAnoPesquisa.Text)

            DtMesAnoPesquisa.Value = Date.Now

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Close()
    End Sub

    Private Sub CarregaFaturamento(ByVal MesAnoRef As String)

        Dim rnRetorno As RNRetorno = New RNRetorno
        Dim valor As Single
        Dim strValor As String
        Dim ds As DataSet

        Try

            If MesAnoRef = "" Then
                MesAnoRef = Date.Now
            End If

            ds = rnRetorno.retorna_valores_atuais(MesAnoRef)

            If ((ds.Tables(0).Rows.Count > 0) Or (ds.Tables(1).Rows.Count > 0) Or
                (ds.Tables(2).Rows.Count > 0) Or (ds.Tables(3).Rows.Count > 0) Or
                (ds.Tables(4).Rows.Count > 0) Or (ds.Tables(5).Rows.Count > 0) Or
                (ds.Tables(6).Rows.Count > 0) Or (ds.Tables(7).Rows.Count > 0) Or
                (ds.Tables(8).Rows.Count > 0)) Then


                'Afaturar
                strValor = valor.ToString("C")
                valor = IIf(IsDBNull(ds.Tables(0).Rows(0).Item(0)), 0, ds.Tables(0).Rows(0).Item(0))
                lblAfaturar.Text = valor.ToString("R$ #,###.00")

                'Faturado
                strValor = valor.ToString("C")
                valor = IIf(IsDBNull(ds.Tables(1).Rows(0).Item(0)), 0, ds.Tables(1).Rows(0).Item(0))
                lblFaturado.Text = valor.ToString("R$ #,###.00")

                'Criticado
                strValor = valor.ToString("C")
                valor = IIf(IsDBNull(ds.Tables(2).Rows(0).Item(0)), 0, ds.Tables(2).Rows(0).Item(0))
                lblCriticado.Text = valor.ToString("R$ #,###.00")

                'Arrecadado
                strValor = valor.ToString("C")
                valor = IIf(IsDBNull(ds.Tables(3).Rows(0).Item(0)), 0, ds.Tables(3).Rows(0).Item(0))
                lblArrecadado.Text = valor.ToString("R$ #,###.00")

                'Alt.conta
                strValor = valor.ToString("C")
                valor = IIf(IsDBNull(ds.Tables(4).Rows(0).Item(0)), 0, ds.Tables(4).Rows(0).Item(0))
                lblAltConta.Text = valor.ToString("R$ #,###.00")


                'Contestado
                strValor = valor.ToString("C")
                valor = IIf(IsDBNull(ds.Tables(5).Rows(0).Item(0)), 0, ds.Tables(5).Rows(0).Item(0))
                lblContestado.Text = valor.ToString("R$ #,###.00")


            End If

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try


    End Sub

    Private Sub DtMesAnoPesquisa_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DtMesAnoPesquisa.ValueChanged

        CarregaFaturamento(DtMesAnoPesquisa.Text)

    End Sub

    Private Sub txtCaminho_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCaminho.TextChanged

    End Sub

    Private Sub btnCarregar_Click(sender As Object, e As EventArgs) Handles btnCarregar.Click
        Dim rnRetorno As RNRetorno = New RNRetorno
        Dim valor As Single
        Dim strValor As String
        Dim ds As DataSet
        Dim dt As DataTable
        Dim data As String = ""

        Try

            data = DtMesAnoPesquisa.Value

            ds = rnRetorno.retorna_Arrecadado(data)
            dt = ds.Tables(0)


            strValor = valor.ToString("C")
            valor = IIf(IsDBNull(dt.Rows(0).Item(0)), 0, dt.Rows(0).Item(0))
            lblArrecadadoAtual.Text = valor.ToString("R$ #,###.00")


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub
End Class


