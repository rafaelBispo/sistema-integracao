﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
<Global.System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726")> _
Partial Class FrmPortal
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub
    Friend WithEvents LogoPictureBox As System.Windows.Forms.PictureBox

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPortal))
        Me.LogoPictureBox = New System.Windows.Forms.PictureBox()
        Me.btnOi = New System.Windows.Forms.Button()
        Me.btnMKT = New System.Windows.Forms.Button()
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LogoPictureBox
        '
        Me.LogoPictureBox.Image = CType(resources.GetObject("LogoPictureBox.Image"), System.Drawing.Image)
        Me.LogoPictureBox.Location = New System.Drawing.Point(0, 0)
        Me.LogoPictureBox.Name = "LogoPictureBox"
        Me.LogoPictureBox.Size = New System.Drawing.Size(490, 226)
        Me.LogoPictureBox.TabIndex = 0
        Me.LogoPictureBox.TabStop = False
        '
        'btnOi
        '
        Me.btnOi.Location = New System.Drawing.Point(12, 232)
        Me.btnOi.Name = "btnOi"
        Me.btnOi.Size = New System.Drawing.Size(211, 143)
        Me.btnOi.TabIndex = 1
        Me.btnOi.Text = "Sistema Oi"
        Me.btnOi.UseVisualStyleBackColor = True
        '
        'btnMKT
        '
        Me.btnMKT.Image = CType(resources.GetObject("btnMKT.Image"), System.Drawing.Image)
        Me.btnMKT.Location = New System.Drawing.Point(266, 232)
        Me.btnMKT.Name = "btnMKT"
        Me.btnMKT.Size = New System.Drawing.Size(211, 143)
        Me.btnMKT.TabIndex = 2
        Me.btnMKT.Text = "Sistema Telemarketing"
        Me.btnMKT.UseVisualStyleBackColor = True
        '
        'FrmPortal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(489, 387)
        Me.Controls.Add(Me.btnMKT)
        Me.Controls.Add(Me.btnOi)
        Me.Controls.Add(Me.LogoPictureBox)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "FrmPortal"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Login"
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnOi As System.Windows.Forms.Button
    Friend WithEvents btnMKT As System.Windows.Forms.Button

End Class
