﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmEnviaEmail
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtDestino = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.lstAnexos = New System.Windows.Forms.ListBox()
        Me.txtAssunto = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.btnIncluiAnexo = New System.Windows.Forms.Button()
        Me.btnExcluiAnexo = New System.Windows.Forms.Button()
        Me.btnEnvia = New System.Windows.Forms.Button()
        Me.chkFormato = New System.Windows.Forms.CheckBox()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.txtMensagem = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txtDestino
        '
        Me.txtDestino.Location = New System.Drawing.Point(84, 34)
        Me.txtDestino.Name = "txtDestino"
        Me.txtDestino.Size = New System.Drawing.Size(464, 20)
        Me.txtDestino.TabIndex = 1
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(84, 69)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(464, 20)
        Me.TextBox3.TabIndex = 2
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(84, 104)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(464, 20)
        Me.TextBox4.TabIndex = 3
        '
        'lstAnexos
        '
        Me.lstAnexos.FormattingEnabled = True
        Me.lstAnexos.Location = New System.Drawing.Point(84, 139)
        Me.lstAnexos.Name = "lstAnexos"
        Me.lstAnexos.Size = New System.Drawing.Size(464, 82)
        Me.lstAnexos.TabIndex = 4
        '
        'txtAssunto
        '
        Me.txtAssunto.Location = New System.Drawing.Point(84, 238)
        Me.txtAssunto.Name = "txtAssunto"
        Me.txtAssunto.Size = New System.Drawing.Size(464, 20)
        Me.txtAssunto.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(39, 37)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(32, 13)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Para:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(39, 72)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 13)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Bcc:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(39, 107)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(24, 13)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "CC:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(39, 141)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(45, 13)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Anexos:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(31, 241)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(48, 13)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = "Assunto:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(17, 277)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(62, 13)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "Mensagem:"
        '
        'btnIncluiAnexo
        '
        Me.btnIncluiAnexo.Location = New System.Drawing.Point(555, 139)
        Me.btnIncluiAnexo.Name = "btnIncluiAnexo"
        Me.btnIncluiAnexo.Size = New System.Drawing.Size(97, 23)
        Me.btnIncluiAnexo.TabIndex = 14
        Me.btnIncluiAnexo.Text = "Incluir Anexo"
        Me.btnIncluiAnexo.UseVisualStyleBackColor = True
        '
        'btnExcluiAnexo
        '
        Me.btnExcluiAnexo.Location = New System.Drawing.Point(555, 198)
        Me.btnExcluiAnexo.Name = "btnExcluiAnexo"
        Me.btnExcluiAnexo.Size = New System.Drawing.Size(97, 23)
        Me.btnExcluiAnexo.TabIndex = 15
        Me.btnExcluiAnexo.Text = "Excluir Anexo"
        Me.btnExcluiAnexo.UseVisualStyleBackColor = True
        '
        'btnEnvia
        '
        Me.btnEnvia.Location = New System.Drawing.Point(683, 63)
        Me.btnEnvia.Name = "btnEnvia"
        Me.btnEnvia.Size = New System.Drawing.Size(112, 57)
        Me.btnEnvia.TabIndex = 16
        Me.btnEnvia.Text = "Enviar Email"
        Me.btnEnvia.UseVisualStyleBackColor = True
        '
        'chkFormato
        '
        Me.chkFormato.AutoSize = True
        Me.chkFormato.Location = New System.Drawing.Point(683, 33)
        Me.chkFormato.Name = "chkFormato"
        Me.chkFormato.Size = New System.Drawing.Size(118, 17)
        Me.chkFormato.TabIndex = 18
        Me.chkFormato.Text = "Enviar como HTML"
        Me.chkFormato.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'txtMensagem
        '
        Me.txtMensagem.Location = New System.Drawing.Point(84, 276)
        Me.txtMensagem.Multiline = True
        Me.txtMensagem.Name = "txtMensagem"
        Me.txtMensagem.Size = New System.Drawing.Size(681, 367)
        Me.txtMensagem.TabIndex = 19
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(683, 141)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 20
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'FrmEnviaEmail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(816, 677)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.txtMensagem)
        Me.Controls.Add(Me.chkFormato)
        Me.Controls.Add(Me.btnEnvia)
        Me.Controls.Add(Me.btnExcluiAnexo)
        Me.Controls.Add(Me.btnIncluiAnexo)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtAssunto)
        Me.Controls.Add(Me.lstAnexos)
        Me.Controls.Add(Me.TextBox4)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.txtDestino)
        Me.Name = "FrmEnviaEmail"
        Me.Text = "FrmEnviaEmail"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtDestino As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents lstAnexos As System.Windows.Forms.ListBox
    Friend WithEvents txtAssunto As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnIncluiAnexo As System.Windows.Forms.Button
    Friend WithEvents btnExcluiAnexo As System.Windows.Forms.Button
    Friend WithEvents btnEnvia As System.Windows.Forms.Button
    Friend WithEvents chkFormato As System.Windows.Forms.CheckBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txtMensagem As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
