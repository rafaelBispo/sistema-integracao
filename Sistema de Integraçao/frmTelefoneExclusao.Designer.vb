﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTelefoneExclusao
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnExcluir = New System.Windows.Forms.Button()
        Me.btnSair = New System.Windows.Forms.Button()
        Me.txtDDDPesquisa = New System.Windows.Forms.TextBox()
        Me.txtTelefonePesquisa = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TabInformacoes = New System.Windows.Forms.TabControl()
        Me.tbInformacoes = New System.Windows.Forms.TabPage()
        Me.txtValDoacao = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txtMelhorDia = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtDtCadastro = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtObs = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtReferencia = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtDtNcMp = New System.Windows.Forms.TextBox()
        Me.txtDtNc = New System.Windows.Forms.TextBox()
        Me.txtRegiao = New System.Windows.Forms.TextBox()
        Me.txtCEP = New System.Windows.Forms.TextBox()
        Me.txtUF = New System.Windows.Forms.TextBox()
        Me.cboBairroDetalhe = New System.Windows.Forms.ComboBox()
        Me.TxtDDD = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.cboCategoriaDetalhe = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cboEspecieDetalhe = New System.Windows.Forms.ComboBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.cboOperadorDetalhe = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.cboCidade = New System.Windows.Forms.ComboBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.txtTelefone2 = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtTelefone1 = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.txtRg = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.txtCnpjCpf = New System.Windows.Forms.TextBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.txtEndereco = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.txtNomeCliente2 = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.txtNomeCliente1 = New System.Windows.Forms.TextBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.txtCodBarra = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.tabHistorico = New System.Windows.Forms.TabPage()
        Me.GrdHistorico = New System.Windows.Forms.DataGridView()
        Me.btnPesquisar = New System.Windows.Forms.Button()
        Me.cboSolicitante = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.cboMotivo = New System.Windows.Forms.ComboBox()
        Me.Pesquisar = New System.Windows.Forms.GroupBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtObsDetalhe = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnCapSalva = New System.Windows.Forms.Button()
        Me.btnCapExibe = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lblMensagem = New System.Windows.Forms.Label()
        Me.TabInformacoes.SuspendLayout()
        Me.tbInformacoes.SuspendLayout()
        Me.tabHistorico.SuspendLayout()
        CType(Me.GrdHistorico, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Pesquisar.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(44, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 25)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "ATENÇÃO"
        '
        'btnExcluir
        '
        Me.btnExcluir.Enabled = False
        Me.btnExcluir.Location = New System.Drawing.Point(973, 526)
        Me.btnExcluir.Name = "btnExcluir"
        Me.btnExcluir.Size = New System.Drawing.Size(75, 43)
        Me.btnExcluir.TabIndex = 3
        Me.btnExcluir.Text = "Excluir"
        Me.btnExcluir.UseVisualStyleBackColor = True
        '
        'btnSair
        '
        Me.btnSair.Location = New System.Drawing.Point(973, 582)
        Me.btnSair.Name = "btnSair"
        Me.btnSair.Size = New System.Drawing.Size(75, 23)
        Me.btnSair.TabIndex = 4
        Me.btnSair.Text = "Sair"
        Me.btnSair.UseVisualStyleBackColor = True
        '
        'txtDDDPesquisa
        '
        Me.txtDDDPesquisa.Location = New System.Drawing.Point(47, 29)
        Me.txtDDDPesquisa.Name = "txtDDDPesquisa"
        Me.txtDDDPesquisa.Size = New System.Drawing.Size(53, 20)
        Me.txtDDDPesquisa.TabIndex = 5
        '
        'txtTelefonePesquisa
        '
        Me.txtTelefonePesquisa.Location = New System.Drawing.Point(168, 29)
        Me.txtTelefonePesquisa.Name = "txtTelefonePesquisa"
        Me.txtTelefonePesquisa.Size = New System.Drawing.Size(117, 20)
        Me.txtTelefonePesquisa.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(13, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(31, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "DDD"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(116, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(49, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Telefone"
        '
        'TabInformacoes
        '
        Me.TabInformacoes.Controls.Add(Me.tbInformacoes)
        Me.TabInformacoes.Controls.Add(Me.tabHistorico)
        Me.TabInformacoes.Location = New System.Drawing.Point(21, 7)
        Me.TabInformacoes.Name = "TabInformacoes"
        Me.TabInformacoes.SelectedIndex = 0
        Me.TabInformacoes.Size = New System.Drawing.Size(931, 348)
        Me.TabInformacoes.TabIndex = 84
        '
        'tbInformacoes
        '
        Me.tbInformacoes.Controls.Add(Me.txtValDoacao)
        Me.tbInformacoes.Controls.Add(Me.Label29)
        Me.tbInformacoes.Controls.Add(Me.txtMelhorDia)
        Me.tbInformacoes.Controls.Add(Me.Label6)
        Me.tbInformacoes.Controls.Add(Me.txtDtCadastro)
        Me.tbInformacoes.Controls.Add(Me.Label30)
        Me.tbInformacoes.Controls.Add(Me.txtObs)
        Me.tbInformacoes.Controls.Add(Me.Label25)
        Me.tbInformacoes.Controls.Add(Me.txtReferencia)
        Me.tbInformacoes.Controls.Add(Me.Label24)
        Me.tbInformacoes.Controls.Add(Me.txtDtNcMp)
        Me.tbInformacoes.Controls.Add(Me.txtDtNc)
        Me.tbInformacoes.Controls.Add(Me.txtRegiao)
        Me.tbInformacoes.Controls.Add(Me.txtCEP)
        Me.tbInformacoes.Controls.Add(Me.txtUF)
        Me.tbInformacoes.Controls.Add(Me.cboBairroDetalhe)
        Me.tbInformacoes.Controls.Add(Me.TxtDDD)
        Me.tbInformacoes.Controls.Add(Me.Label34)
        Me.tbInformacoes.Controls.Add(Me.cboCategoriaDetalhe)
        Me.tbInformacoes.Controls.Add(Me.Label7)
        Me.tbInformacoes.Controls.Add(Me.cboEspecieDetalhe)
        Me.tbInformacoes.Controls.Add(Me.Label27)
        Me.tbInformacoes.Controls.Add(Me.cboOperadorDetalhe)
        Me.tbInformacoes.Controls.Add(Me.Label8)
        Me.tbInformacoes.Controls.Add(Me.Label9)
        Me.tbInformacoes.Controls.Add(Me.Label10)
        Me.tbInformacoes.Controls.Add(Me.Label11)
        Me.tbInformacoes.Controls.Add(Me.Label15)
        Me.tbInformacoes.Controls.Add(Me.Label23)
        Me.tbInformacoes.Controls.Add(Me.Label31)
        Me.tbInformacoes.Controls.Add(Me.cboCidade)
        Me.tbInformacoes.Controls.Add(Me.Label32)
        Me.tbInformacoes.Controls.Add(Me.txtTelefone2)
        Me.tbInformacoes.Controls.Add(Me.Label33)
        Me.tbInformacoes.Controls.Add(Me.txtTelefone1)
        Me.tbInformacoes.Controls.Add(Me.Label35)
        Me.tbInformacoes.Controls.Add(Me.txtRg)
        Me.tbInformacoes.Controls.Add(Me.Label36)
        Me.tbInformacoes.Controls.Add(Me.txtCnpjCpf)
        Me.tbInformacoes.Controls.Add(Me.Label37)
        Me.tbInformacoes.Controls.Add(Me.txtEndereco)
        Me.tbInformacoes.Controls.Add(Me.Label38)
        Me.tbInformacoes.Controls.Add(Me.txtEmail)
        Me.tbInformacoes.Controls.Add(Me.Label39)
        Me.tbInformacoes.Controls.Add(Me.txtNomeCliente2)
        Me.tbInformacoes.Controls.Add(Me.Label40)
        Me.tbInformacoes.Controls.Add(Me.txtNomeCliente1)
        Me.tbInformacoes.Controls.Add(Me.Label41)
        Me.tbInformacoes.Controls.Add(Me.txtCodBarra)
        Me.tbInformacoes.Controls.Add(Me.Label42)
        Me.tbInformacoes.Controls.Add(Me.txtCodigo)
        Me.tbInformacoes.Controls.Add(Me.Label43)
        Me.tbInformacoes.Location = New System.Drawing.Point(4, 22)
        Me.tbInformacoes.Name = "tbInformacoes"
        Me.tbInformacoes.Padding = New System.Windows.Forms.Padding(3)
        Me.tbInformacoes.Size = New System.Drawing.Size(923, 322)
        Me.tbInformacoes.TabIndex = 1
        Me.tbInformacoes.Text = "Informações"
        Me.tbInformacoes.UseVisualStyleBackColor = True
        '
        'txtValDoacao
        '
        Me.txtValDoacao.Location = New System.Drawing.Point(842, 147)
        Me.txtValDoacao.Name = "txtValDoacao"
        Me.txtValDoacao.Size = New System.Drawing.Size(73, 20)
        Me.txtValDoacao.TabIndex = 182
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(839, 128)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(43, 16)
        Me.Label29.TabIndex = 181
        Me.Label29.Text = "Valor:"
        '
        'txtMelhorDia
        '
        Me.txtMelhorDia.Location = New System.Drawing.Point(750, 147)
        Me.txtMelhorDia.Name = "txtMelhorDia"
        Me.txtMelhorDia.Size = New System.Drawing.Size(73, 20)
        Me.txtMelhorDia.TabIndex = 180
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(747, 128)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(76, 16)
        Me.Label6.TabIndex = 179
        Me.Label6.Text = "Melhor Dia:"
        '
        'txtDtCadastro
        '
        Me.txtDtCadastro.Location = New System.Drawing.Point(705, 190)
        Me.txtDtCadastro.Name = "txtDtCadastro"
        Me.txtDtCadastro.Size = New System.Drawing.Size(105, 20)
        Me.txtDtCadastro.TabIndex = 174
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(702, 171)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(98, 16)
        Me.Label30.TabIndex = 173
        Me.Label30.Text = "Data Cadastro:"
        '
        'txtObs
        '
        Me.txtObs.Location = New System.Drawing.Point(109, 257)
        Me.txtObs.Multiline = True
        Me.txtObs.Name = "txtObs"
        Me.txtObs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtObs.Size = New System.Drawing.Size(657, 44)
        Me.txtObs.TabIndex = 172
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(17, 257)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(86, 16)
        Me.Label25.TabIndex = 171
        Me.Label25.Text = "Observação:"
        '
        'txtReferencia
        '
        Me.txtReferencia.Location = New System.Drawing.Point(111, 222)
        Me.txtReferencia.Name = "txtReferencia"
        Me.txtReferencia.Size = New System.Drawing.Size(655, 20)
        Me.txtReferencia.TabIndex = 170
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(17, 223)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(77, 16)
        Me.Label24.TabIndex = 169
        Me.Label24.Text = "Referência:"
        '
        'txtDtNcMp
        '
        Me.txtDtNcMp.Location = New System.Drawing.Point(591, 191)
        Me.txtDtNcMp.Name = "txtDtNcMp"
        Me.txtDtNcMp.Size = New System.Drawing.Size(105, 20)
        Me.txtDtNcMp.TabIndex = 168
        '
        'txtDtNc
        '
        Me.txtDtNc.Location = New System.Drawing.Point(476, 191)
        Me.txtDtNc.Name = "txtDtNc"
        Me.txtDtNc.Size = New System.Drawing.Size(105, 20)
        Me.txtDtNc.TabIndex = 167
        '
        'txtRegiao
        '
        Me.txtRegiao.Location = New System.Drawing.Point(414, 191)
        Me.txtRegiao.Name = "txtRegiao"
        Me.txtRegiao.Size = New System.Drawing.Size(53, 20)
        Me.txtRegiao.TabIndex = 166
        '
        'txtCEP
        '
        Me.txtCEP.Location = New System.Drawing.Point(319, 191)
        Me.txtCEP.Name = "txtCEP"
        Me.txtCEP.Size = New System.Drawing.Size(88, 20)
        Me.txtCEP.TabIndex = 165
        '
        'txtUF
        '
        Me.txtUF.Location = New System.Drawing.Point(268, 191)
        Me.txtUF.Name = "txtUF"
        Me.txtUF.Size = New System.Drawing.Size(41, 20)
        Me.txtUF.TabIndex = 164
        '
        'cboBairroDetalhe
        '
        Me.cboBairroDetalhe.FormattingEnabled = True
        Me.cboBairroDetalhe.Location = New System.Drawing.Point(20, 191)
        Me.cboBairroDetalhe.Name = "cboBairroDetalhe"
        Me.cboBairroDetalhe.Size = New System.Drawing.Size(239, 21)
        Me.cboBairroDetalhe.TabIndex = 163
        '
        'TxtDDD
        '
        Me.TxtDDD.Location = New System.Drawing.Point(379, 105)
        Me.TxtDDD.Name = "TxtDDD"
        Me.TxtDDD.Size = New System.Drawing.Size(56, 20)
        Me.TxtDDD.TabIndex = 162
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(376, 86)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(41, 16)
        Me.Label34.TabIndex = 161
        Me.Label34.Text = "DDD:"
        '
        'cboCategoriaDetalhe
        '
        Me.cboCategoriaDetalhe.FormattingEnabled = True
        Me.cboCategoriaDetalhe.Location = New System.Drawing.Point(747, 63)
        Me.cboCategoriaDetalhe.Name = "cboCategoriaDetalhe"
        Me.cboCategoriaDetalhe.Size = New System.Drawing.Size(168, 21)
        Me.cboCategoriaDetalhe.TabIndex = 160
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(744, 44)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(70, 16)
        Me.Label7.TabIndex = 159
        Me.Label7.Text = "Categoria:"
        '
        'cboEspecieDetalhe
        '
        Me.cboEspecieDetalhe.FormattingEnabled = True
        Me.cboEspecieDetalhe.Location = New System.Drawing.Point(747, 104)
        Me.cboEspecieDetalhe.Name = "cboEspecieDetalhe"
        Me.cboEspecieDetalhe.Size = New System.Drawing.Size(168, 21)
        Me.cboEspecieDetalhe.TabIndex = 158
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(744, 85)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(61, 16)
        Me.Label27.TabIndex = 157
        Me.Label27.Text = "Espécie:"
        '
        'cboOperadorDetalhe
        '
        Me.cboOperadorDetalhe.FormattingEnabled = True
        Me.cboOperadorDetalhe.Location = New System.Drawing.Point(747, 20)
        Me.cboOperadorDetalhe.Name = "cboOperadorDetalhe"
        Me.cboOperadorDetalhe.Size = New System.Drawing.Size(168, 21)
        Me.cboOperadorDetalhe.TabIndex = 156
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(744, 2)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(69, 16)
        Me.Label8.TabIndex = 155
        Me.Label8.Text = "Operador:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(586, 170)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(85, 16)
        Me.Label9.TabIndex = 154
        Me.Label9.Text = "Data NC MP:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(471, 170)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(62, 16)
        Me.Label10.TabIndex = 153
        Me.Label10.Text = "Data NC:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(409, 170)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(56, 16)
        Me.Label11.TabIndex = 152
        Me.Label11.Text = "Região:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(314, 170)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(38, 16)
        Me.Label15.TabIndex = 151
        Me.Label15.Text = "CEP:"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(263, 170)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(29, 16)
        Me.Label23.TabIndex = 150
        Me.Label23.Text = "UF:"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(15, 170)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(47, 16)
        Me.Label31.TabIndex = 149
        Me.Label31.Text = "Bairro:"
        '
        'cboCidade
        '
        Me.cboCidade.FormattingEnabled = True
        Me.cboCidade.Location = New System.Drawing.Point(592, 147)
        Me.cboCidade.Name = "cboCidade"
        Me.cboCidade.Size = New System.Drawing.Size(145, 21)
        Me.cboCidade.TabIndex = 148
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(589, 128)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(55, 16)
        Me.Label32.TabIndex = 147
        Me.Label32.Text = "Cidade:"
        '
        'txtTelefone2
        '
        Me.txtTelefone2.Location = New System.Drawing.Point(592, 105)
        Me.txtTelefone2.Name = "txtTelefone2"
        Me.txtTelefone2.Size = New System.Drawing.Size(145, 20)
        Me.txtTelefone2.TabIndex = 146
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(589, 86)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(72, 16)
        Me.Label33.TabIndex = 145
        Me.Label33.Text = "Telefone2:"
        '
        'txtTelefone1
        '
        Me.txtTelefone1.Location = New System.Drawing.Point(441, 105)
        Me.txtTelefone1.Name = "txtTelefone1"
        Me.txtTelefone1.Size = New System.Drawing.Size(140, 20)
        Me.txtTelefone1.TabIndex = 144
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(438, 86)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(72, 16)
        Me.Label35.TabIndex = 143
        Me.Label35.Text = "Telefone1:"
        '
        'txtRg
        '
        Me.txtRg.Location = New System.Drawing.Point(592, 63)
        Me.txtRg.Name = "txtRg"
        Me.txtRg.Size = New System.Drawing.Size(145, 20)
        Me.txtRg.TabIndex = 142
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.Location = New System.Drawing.Point(589, 44)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(90, 16)
        Me.Label36.TabIndex = 141
        Me.Label36.Text = "Insc. Est./ RG:"
        '
        'txtCnpjCpf
        '
        Me.txtCnpjCpf.Location = New System.Drawing.Point(592, 21)
        Me.txtCnpjCpf.Name = "txtCnpjCpf"
        Me.txtCnpjCpf.Size = New System.Drawing.Size(145, 20)
        Me.txtCnpjCpf.TabIndex = 140
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.Location = New System.Drawing.Point(589, 2)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(76, 16)
        Me.Label37.TabIndex = 139
        Me.Label37.Text = "CNPJ/CPF:"
        '
        'txtEndereco
        '
        Me.txtEndereco.Location = New System.Drawing.Point(18, 147)
        Me.txtEndereco.Name = "txtEndereco"
        Me.txtEndereco.Size = New System.Drawing.Size(563, 20)
        Me.txtEndereco.TabIndex = 138
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.Location = New System.Drawing.Point(15, 128)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(70, 16)
        Me.Label38.TabIndex = 137
        Me.Label38.Text = "Endereço:"
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(18, 105)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(352, 20)
        Me.txtEmail.TabIndex = 136
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.Location = New System.Drawing.Point(15, 86)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(49, 16)
        Me.Label39.TabIndex = 135
        Me.Label39.Text = "E-mail:"
        '
        'txtNomeCliente2
        '
        Me.txtNomeCliente2.Location = New System.Drawing.Point(164, 63)
        Me.txtNomeCliente2.Name = "txtNomeCliente2"
        Me.txtNomeCliente2.Size = New System.Drawing.Size(417, 20)
        Me.txtNomeCliente2.TabIndex = 134
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.Location = New System.Drawing.Point(161, 44)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(58, 16)
        Me.Label40.TabIndex = 133
        Me.Label40.Text = "Nome 2:"
        '
        'txtNomeCliente1
        '
        Me.txtNomeCliente1.Location = New System.Drawing.Point(164, 21)
        Me.txtNomeCliente1.Name = "txtNomeCliente1"
        Me.txtNomeCliente1.Size = New System.Drawing.Size(417, 20)
        Me.txtNomeCliente1.TabIndex = 132
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.Location = New System.Drawing.Point(161, 2)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(48, 16)
        Me.Label41.TabIndex = 131
        Me.Label41.Text = "Nome:"
        '
        'txtCodBarra
        '
        Me.txtCodBarra.Location = New System.Drawing.Point(18, 63)
        Me.txtCodBarra.Name = "txtCodBarra"
        Me.txtCodBarra.Size = New System.Drawing.Size(136, 20)
        Me.txtCodBarra.TabIndex = 130
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Location = New System.Drawing.Point(15, 44)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(75, 16)
        Me.Label42.TabIndex = 129
        Me.Label42.Text = "Cód. Barra:"
        '
        'txtCodigo
        '
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.Location = New System.Drawing.Point(18, 21)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(136, 20)
        Me.txtCodigo.TabIndex = 128
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(15, 2)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(55, 16)
        Me.Label43.TabIndex = 127
        Me.Label43.Text = "Código:"
        '
        'tabHistorico
        '
        Me.tabHistorico.Controls.Add(Me.GrdHistorico)
        Me.tabHistorico.Location = New System.Drawing.Point(4, 22)
        Me.tabHistorico.Name = "tabHistorico"
        Me.tabHistorico.Padding = New System.Windows.Forms.Padding(3)
        Me.tabHistorico.Size = New System.Drawing.Size(923, 322)
        Me.tabHistorico.TabIndex = 0
        Me.tabHistorico.Text = "Histórico"
        Me.tabHistorico.UseVisualStyleBackColor = True
        '
        'GrdHistorico
        '
        Me.GrdHistorico.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GrdHistorico.Location = New System.Drawing.Point(6, 6)
        Me.GrdHistorico.Name = "GrdHistorico"
        Me.GrdHistorico.Size = New System.Drawing.Size(908, 295)
        Me.GrdHistorico.TabIndex = 3
        '
        'btnPesquisar
        '
        Me.btnPesquisar.Location = New System.Drawing.Point(298, 27)
        Me.btnPesquisar.Name = "btnPesquisar"
        Me.btnPesquisar.Size = New System.Drawing.Size(75, 23)
        Me.btnPesquisar.TabIndex = 85
        Me.btnPesquisar.Text = "Pesquisar"
        Me.btnPesquisar.UseVisualStyleBackColor = True
        '
        'cboSolicitante
        '
        Me.cboSolicitante.FormattingEnabled = True
        Me.cboSolicitante.Location = New System.Drawing.Point(16, 35)
        Me.cboSolicitante.Name = "cboSolicitante"
        Me.cboSolicitante.Size = New System.Drawing.Size(372, 21)
        Me.cboSolicitante.TabIndex = 157
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(13, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 16)
        Me.Label4.TabIndex = 158
        Me.Label4.Text = "Solicitante:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(13, 59)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(51, 16)
        Me.Label5.TabIndex = 160
        Me.Label5.Text = "Motivo:"
        '
        'cboMotivo
        '
        Me.cboMotivo.FormattingEnabled = True
        Me.cboMotivo.Location = New System.Drawing.Point(16, 78)
        Me.cboMotivo.Name = "cboMotivo"
        Me.cboMotivo.Size = New System.Drawing.Size(595, 21)
        Me.cboMotivo.TabIndex = 159
        '
        'Pesquisar
        '
        Me.Pesquisar.Controls.Add(Me.txtTelefonePesquisa)
        Me.Pesquisar.Controls.Add(Me.txtDDDPesquisa)
        Me.Pesquisar.Controls.Add(Me.Label2)
        Me.Pesquisar.Controls.Add(Me.Label3)
        Me.Pesquisar.Controls.Add(Me.btnPesquisar)
        Me.Pesquisar.Location = New System.Drawing.Point(14, 33)
        Me.Pesquisar.Name = "Pesquisar"
        Me.Pesquisar.Size = New System.Drawing.Size(389, 62)
        Me.Pesquisar.TabIndex = 161
        Me.Pesquisar.TabStop = False
        Me.Pesquisar.Text = "Pesquisar"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtObsDetalhe)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.cboSolicitante)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.cboMotivo)
        Me.GroupBox1.Location = New System.Drawing.Point(409, 5)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(617, 218)
        Me.GroupBox1.TabIndex = 162
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Detalhes"
        '
        'txtObsDetalhe
        '
        Me.txtObsDetalhe.Location = New System.Drawing.Point(17, 126)
        Me.txtObsDetalhe.Multiline = True
        Me.txtObsDetalhe.Name = "txtObsDetalhe"
        Me.txtObsDetalhe.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtObsDetalhe.Size = New System.Drawing.Size(594, 80)
        Me.txtObsDetalhe.TabIndex = 174
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(12, 107)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(86, 16)
        Me.Label12.TabIndex = 173
        Me.Label12.Text = "Observação:"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblMensagem)
        Me.Panel1.Controls.Add(Me.Pesquisar)
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1041, 232)
        Me.Panel1.TabIndex = 163
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.TabInformacoes)
        Me.Panel2.Location = New System.Drawing.Point(2, 250)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(965, 365)
        Me.Panel2.TabIndex = 164
        '
        'btnCapSalva
        '
        Me.btnCapSalva.Location = New System.Drawing.Point(973, 338)
        Me.btnCapSalva.Name = "btnCapSalva"
        Me.btnCapSalva.Size = New System.Drawing.Size(75, 51)
        Me.btnCapSalva.TabIndex = 165
        Me.btnCapSalva.Text = "Capturar e Salvar Tela"
        Me.btnCapSalva.UseVisualStyleBackColor = True
        Me.btnCapSalva.Visible = False
        '
        'btnCapExibe
        '
        Me.btnCapExibe.Location = New System.Drawing.Point(973, 406)
        Me.btnCapExibe.Name = "btnCapExibe"
        Me.btnCapExibe.Size = New System.Drawing.Size(75, 51)
        Me.btnCapExibe.TabIndex = 166
        Me.btnCapExibe.Text = "Capturar e ExibeTela"
        Me.btnCapExibe.UseVisualStyleBackColor = True
        Me.btnCapExibe.Visible = False
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(973, 469)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 51)
        Me.Button1.TabIndex = 167
        Me.Button1.Text = "Enviar Email"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'lblMensagem
        '
        Me.lblMensagem.AutoSize = True
        Me.lblMensagem.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMensagem.ForeColor = System.Drawing.Color.Red
        Me.lblMensagem.Location = New System.Drawing.Point(44, 149)
        Me.lblMensagem.Name = "lblMensagem"
        Me.lblMensagem.Size = New System.Drawing.Size(300, 25)
        Me.lblMensagem.TabIndex = 163
        Me.lblMensagem.Text = "CONTRIBUINTE EXCLUIDO"
        Me.lblMensagem.Visible = False
        '
        'frmTelefoneExclusao
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1060, 617)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnCapExibe)
        Me.Controls.Add(Me.btnCapSalva)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btnSair)
        Me.Controls.Add(Me.btnExcluir)
        Me.Name = "frmTelefoneExclusao"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Telefones para Exclusão"
        Me.TabInformacoes.ResumeLayout(False)
        Me.tbInformacoes.ResumeLayout(False)
        Me.tbInformacoes.PerformLayout()
        Me.tabHistorico.ResumeLayout(False)
        CType(Me.GrdHistorico, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Pesquisar.ResumeLayout(False)
        Me.Pesquisar.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnExcluir As System.Windows.Forms.Button
    Friend WithEvents btnSair As System.Windows.Forms.Button
    Friend WithEvents txtDDDPesquisa As System.Windows.Forms.TextBox
    Friend WithEvents txtTelefonePesquisa As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TabInformacoes As System.Windows.Forms.TabControl
    Friend WithEvents tabHistorico As System.Windows.Forms.TabPage
    Friend WithEvents GrdHistorico As System.Windows.Forms.DataGridView
    Friend WithEvents tbInformacoes As System.Windows.Forms.TabPage
    Friend WithEvents txtValDoacao As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents txtMelhorDia As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtDtCadastro As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents txtObs As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txtReferencia As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtDtNcMp As System.Windows.Forms.TextBox
    Friend WithEvents txtDtNc As System.Windows.Forms.TextBox
    Friend WithEvents txtRegiao As System.Windows.Forms.TextBox
    Friend WithEvents txtCEP As System.Windows.Forms.TextBox
    Friend WithEvents txtUF As System.Windows.Forms.TextBox
    Friend WithEvents cboBairroDetalhe As System.Windows.Forms.ComboBox
    Friend WithEvents TxtDDD As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents cboCategoriaDetalhe As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cboEspecieDetalhe As System.Windows.Forms.ComboBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents cboOperadorDetalhe As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents cboCidade As System.Windows.Forms.ComboBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents txtTelefone2 As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents txtTelefone1 As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents txtRg As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtCnpjCpf As System.Windows.Forms.TextBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents txtEndereco As System.Windows.Forms.TextBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents txtNomeCliente2 As System.Windows.Forms.TextBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents txtNomeCliente1 As System.Windows.Forms.TextBox
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents txtCodBarra As System.Windows.Forms.TextBox
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents btnPesquisar As System.Windows.Forms.Button
    Friend WithEvents cboSolicitante As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cboMotivo As System.Windows.Forms.ComboBox
    Friend WithEvents Pesquisar As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtObsDetalhe As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnCapSalva As System.Windows.Forms.Button
    Friend WithEvents btnCapExibe As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents lblMensagem As System.Windows.Forms.Label
End Class
