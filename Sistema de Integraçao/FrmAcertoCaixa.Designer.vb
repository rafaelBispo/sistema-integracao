﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmAcertoCaixa
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rdBordero = New System.Windows.Forms.RadioButton()
        Me.rdCanceladoNP = New System.Windows.Forms.RadioButton()
        Me.rdRecebido = New System.Windows.Forms.RadioButton()
        Me.rdProxDia = New System.Windows.Forms.RadioButton()
        Me.rdCancelado = New System.Windows.Forms.RadioButton()
        Me.rdExtravio = New System.Windows.Forms.RadioButton()
        Me.rdPredatado = New System.Windows.Forms.RadioButton()
        Me.rdConfirmar = New System.Windows.Forms.RadioButton()
        Me.txtCodMovimentacao = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtData = New System.Windows.Forms.MaskedTextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cboFuncionario = New System.Windows.Forms.ComboBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.btnPesquisar = New System.Windows.Forms.Button()
        Me.grdFaturamento = New System.Windows.Forms.DataGridView()
        Me.btnSair = New System.Windows.Forms.Button()
        Me.lblTotalFichasSelecionado = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblValorSelecionado = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.gridMovimentacao = New System.Windows.Forms.DataGridView()
        Me.txtDataInicio = New System.Windows.Forms.MaskedTextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtDataFim = New System.Windows.Forms.MaskedTextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnPesquisarMov = New System.Windows.Forms.Button()
        Me.chkHeader = New System.Windows.Forms.CheckBox()
        Me.btnAtualizar = New System.Windows.Forms.Button()
        Me.lblValor = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblTotalFichas = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btnProcessar = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.grdFaturamento, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridMovimentacao, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.txtCodMovimentacao)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.txtData)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.cboFuncionario)
        Me.Panel1.Controls.Add(Me.Label26)
        Me.Panel1.Controls.Add(Me.btnPesquisar)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(740, 173)
        Me.Panel1.TabIndex = 1
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rdBordero)
        Me.GroupBox1.Controls.Add(Me.rdCanceladoNP)
        Me.GroupBox1.Controls.Add(Me.rdRecebido)
        Me.GroupBox1.Controls.Add(Me.rdProxDia)
        Me.GroupBox1.Controls.Add(Me.rdCancelado)
        Me.GroupBox1.Controls.Add(Me.rdExtravio)
        Me.GroupBox1.Controls.Add(Me.rdPredatado)
        Me.GroupBox1.Controls.Add(Me.rdConfirmar)
        Me.GroupBox1.Location = New System.Drawing.Point(9, 51)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(256, 117)
        Me.GroupBox1.TabIndex = 80
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Situações"
        '
        'rdBordero
        '
        Me.rdBordero.AutoSize = True
        Me.rdBordero.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdBordero.Location = New System.Drawing.Point(126, 88)
        Me.rdBordero.Name = "rdBordero"
        Me.rdBordero.Size = New System.Drawing.Size(75, 20)
        Me.rdBordero.TabIndex = 7
        Me.rdBordero.TabStop = True
        Me.rdBordero.Text = "Borderô"
        Me.rdBordero.UseVisualStyleBackColor = True
        '
        'rdCanceladoNP
        '
        Me.rdCanceladoNP.AutoSize = True
        Me.rdCanceladoNP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdCanceladoNP.Location = New System.Drawing.Point(126, 65)
        Me.rdCanceladoNP.Name = "rdCanceladoNP"
        Me.rdCanceladoNP.Size = New System.Drawing.Size(114, 20)
        Me.rdCanceladoNP.TabIndex = 6
        Me.rdCanceladoNP.TabStop = True
        Me.rdCanceladoNP.Text = "Cancelado NP"
        Me.rdCanceladoNP.UseVisualStyleBackColor = True
        '
        'rdRecebido
        '
        Me.rdRecebido.AutoSize = True
        Me.rdRecebido.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdRecebido.Location = New System.Drawing.Point(12, 88)
        Me.rdRecebido.Name = "rdRecebido"
        Me.rdRecebido.Size = New System.Drawing.Size(86, 20)
        Me.rdRecebido.TabIndex = 5
        Me.rdRecebido.TabStop = True
        Me.rdRecebido.Text = "Recebido"
        Me.rdRecebido.UseVisualStyleBackColor = True
        '
        'rdProxDia
        '
        Me.rdProxDia.AutoSize = True
        Me.rdProxDia.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdProxDia.Location = New System.Drawing.Point(12, 65)
        Me.rdProxDia.Name = "rdProxDia"
        Me.rdProxDia.Size = New System.Drawing.Size(99, 20)
        Me.rdProxDia.TabIndex = 4
        Me.rdProxDia.TabStop = True
        Me.rdProxDia.Text = "Próximo Dia"
        Me.rdProxDia.UseVisualStyleBackColor = True
        '
        'rdCancelado
        '
        Me.rdCancelado.AutoSize = True
        Me.rdCancelado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdCancelado.Location = New System.Drawing.Point(126, 42)
        Me.rdCancelado.Name = "rdCancelado"
        Me.rdCancelado.Size = New System.Drawing.Size(92, 20)
        Me.rdCancelado.TabIndex = 3
        Me.rdCancelado.TabStop = True
        Me.rdCancelado.Text = "Cancelado"
        Me.rdCancelado.UseVisualStyleBackColor = True
        '
        'rdExtravio
        '
        Me.rdExtravio.AutoSize = True
        Me.rdExtravio.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdExtravio.Location = New System.Drawing.Point(126, 19)
        Me.rdExtravio.Name = "rdExtravio"
        Me.rdExtravio.Size = New System.Drawing.Size(74, 20)
        Me.rdExtravio.TabIndex = 2
        Me.rdExtravio.TabStop = True
        Me.rdExtravio.Text = "Extravio"
        Me.rdExtravio.UseVisualStyleBackColor = True
        '
        'rdPredatado
        '
        Me.rdPredatado.AutoSize = True
        Me.rdPredatado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdPredatado.Location = New System.Drawing.Point(12, 42)
        Me.rdPredatado.Name = "rdPredatado"
        Me.rdPredatado.Size = New System.Drawing.Size(96, 20)
        Me.rdPredatado.TabIndex = 1
        Me.rdPredatado.TabStop = True
        Me.rdPredatado.Text = "Pré-Datado"
        Me.rdPredatado.UseVisualStyleBackColor = True
        '
        'rdConfirmar
        '
        Me.rdConfirmar.AutoSize = True
        Me.rdConfirmar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdConfirmar.Location = New System.Drawing.Point(12, 19)
        Me.rdConfirmar.Name = "rdConfirmar"
        Me.rdConfirmar.Size = New System.Drawing.Size(83, 20)
        Me.rdConfirmar.TabIndex = 0
        Me.rdConfirmar.TabStop = True
        Me.rdConfirmar.Text = "Confirmar"
        Me.rdConfirmar.UseVisualStyleBackColor = True
        '
        'txtCodMovimentacao
        '
        Me.txtCodMovimentacao.Location = New System.Drawing.Point(414, 46)
        Me.txtCodMovimentacao.Name = "txtCodMovimentacao"
        Me.txtCodMovimentacao.Size = New System.Drawing.Size(121, 20)
        Me.txtCodMovimentacao.TabIndex = 79
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(271, 46)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(137, 16)
        Me.Label12.TabIndex = 78
        Me.Label12.Text = "Cod. Movimentação : "
        '
        'txtData
        '
        Me.txtData.Location = New System.Drawing.Point(414, 14)
        Me.txtData.Mask = "##/##/####"
        Me.txtData.Name = "txtData"
        Me.txtData.Size = New System.Drawing.Size(121, 20)
        Me.txtData.TabIndex = 77
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(299, 18)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(109, 16)
        Me.Label4.TabIndex = 76
        Me.Label4.Text = "Data Movimento:"
        '
        'cboFuncionario
        '
        Me.cboFuncionario.FormattingEnabled = True
        Me.cboFuncionario.Location = New System.Drawing.Point(79, 17)
        Me.cboFuncionario.Name = "cboFuncionario"
        Me.cboFuncionario.Size = New System.Drawing.Size(197, 21)
        Me.cboFuncionario.TabIndex = 69
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(6, 18)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(71, 16)
        Me.Label26.TabIndex = 68
        Me.Label26.Text = "Cobrador: "
        '
        'btnPesquisar
        '
        Me.btnPesquisar.Location = New System.Drawing.Point(578, 11)
        Me.btnPesquisar.Name = "btnPesquisar"
        Me.btnPesquisar.Size = New System.Drawing.Size(75, 23)
        Me.btnPesquisar.TabIndex = 67
        Me.btnPesquisar.Text = "Pesquisar"
        Me.btnPesquisar.UseVisualStyleBackColor = True
        '
        'grdFaturamento
        '
        Me.grdFaturamento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdFaturamento.Location = New System.Drawing.Point(12, 191)
        Me.grdFaturamento.Name = "grdFaturamento"
        Me.grdFaturamento.Size = New System.Drawing.Size(740, 360)
        Me.grdFaturamento.TabIndex = 2
        '
        'btnSair
        '
        Me.btnSair.Location = New System.Drawing.Point(1030, 528)
        Me.btnSair.Name = "btnSair"
        Me.btnSair.Size = New System.Drawing.Size(75, 23)
        Me.btnSair.TabIndex = 3
        Me.btnSair.Text = "Sair"
        Me.btnSair.UseVisualStyleBackColor = True
        '
        'lblTotalFichasSelecionado
        '
        Me.lblTotalFichasSelecionado.AutoSize = True
        Me.lblTotalFichasSelecionado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalFichasSelecionado.Location = New System.Drawing.Point(762, 348)
        Me.lblTotalFichasSelecionado.Name = "lblTotalFichasSelecionado"
        Me.lblTotalFichasSelecionado.Size = New System.Drawing.Size(16, 16)
        Me.lblTotalFichasSelecionado.TabIndex = 80
        Me.lblTotalFichasSelecionado.Text = "0"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(758, 320)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(218, 16)
        Me.Label2.TabIndex = 79
        Me.Label2.Text = "Total de Fichas selecionadas:"
        '
        'lblValorSelecionado
        '
        Me.lblValorSelecionado.AutoSize = True
        Me.lblValorSelecionado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorSelecionado.Location = New System.Drawing.Point(762, 417)
        Me.lblValorSelecionado.Name = "lblValorSelecionado"
        Me.lblValorSelecionado.Size = New System.Drawing.Size(16, 16)
        Me.lblValorSelecionado.TabIndex = 82
        Me.lblValorSelecionado.Text = "0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(762, 380)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(183, 16)
        Me.Label5.TabIndex = 81
        Me.Label5.Text = "Valor Total selecionado :"
        '
        'gridMovimentacao
        '
        Me.gridMovimentacao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridMovimentacao.Location = New System.Drawing.Point(769, 12)
        Me.gridMovimentacao.Name = "gridMovimentacao"
        Me.gridMovimentacao.Size = New System.Drawing.Size(336, 224)
        Me.gridMovimentacao.TabIndex = 84
        '
        'txtDataInicio
        '
        Me.txtDataInicio.Location = New System.Drawing.Point(845, 244)
        Me.txtDataInicio.Mask = "##/##/####"
        Me.txtDataInicio.Name = "txtDataInicio"
        Me.txtDataInicio.Size = New System.Drawing.Size(85, 20)
        Me.txtDataInicio.TabIndex = 86
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(770, 248)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 16)
        Me.Label1.TabIndex = 85
        Me.Label1.Text = "Data Início"
        '
        'txtDataFim
        '
        Me.txtDataFim.Location = New System.Drawing.Point(845, 273)
        Me.txtDataFim.Mask = "##/##/####"
        Me.txtDataFim.Name = "txtDataFim"
        Me.txtDataFim.Size = New System.Drawing.Size(85, 20)
        Me.txtDataFim.TabIndex = 88
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(770, 277)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 16)
        Me.Label3.TabIndex = 87
        Me.Label3.Text = "Data Fim"
        '
        'btnPesquisarMov
        '
        Me.btnPesquisarMov.Location = New System.Drawing.Point(959, 244)
        Me.btnPesquisarMov.Name = "btnPesquisarMov"
        Me.btnPesquisarMov.Size = New System.Drawing.Size(75, 23)
        Me.btnPesquisarMov.TabIndex = 89
        Me.btnPesquisarMov.Text = "Pesquisar"
        Me.btnPesquisarMov.UseVisualStyleBackColor = True
        '
        'chkHeader
        '
        Me.chkHeader.AutoSize = True
        Me.chkHeader.Location = New System.Drawing.Point(63, 210)
        Me.chkHeader.Name = "chkHeader"
        Me.chkHeader.Size = New System.Drawing.Size(15, 14)
        Me.chkHeader.TabIndex = 90
        Me.chkHeader.UseVisualStyleBackColor = True
        '
        'btnAtualizar
        '
        Me.btnAtualizar.Location = New System.Drawing.Point(761, 458)
        Me.btnAtualizar.Name = "btnAtualizar"
        Me.btnAtualizar.Size = New System.Drawing.Size(75, 23)
        Me.btnAtualizar.TabIndex = 91
        Me.btnAtualizar.Text = "Atualizar"
        Me.btnAtualizar.UseVisualStyleBackColor = True
        '
        'lblValor
        '
        Me.lblValor.AutoSize = True
        Me.lblValor.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValor.Location = New System.Drawing.Point(989, 417)
        Me.lblValor.Name = "lblValor"
        Me.lblValor.Size = New System.Drawing.Size(16, 16)
        Me.lblValor.TabIndex = 95
        Me.lblValor.Text = "0"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(989, 380)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(93, 16)
        Me.Label7.TabIndex = 94
        Me.Label7.Text = "Valor Total :"
        '
        'lblTotalFichas
        '
        Me.lblTotalFichas.AutoSize = True
        Me.lblTotalFichas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalFichas.Location = New System.Drawing.Point(989, 348)
        Me.lblTotalFichas.Name = "lblTotalFichas"
        Me.lblTotalFichas.Size = New System.Drawing.Size(16, 16)
        Me.lblTotalFichas.TabIndex = 93
        Me.lblTotalFichas.Text = "0"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(985, 320)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(120, 16)
        Me.Label9.TabIndex = 92
        Me.Label9.Text = "Total de Fichas:"
        '
        'btnProcessar
        '
        Me.btnProcessar.Location = New System.Drawing.Point(761, 527)
        Me.btnProcessar.Name = "btnProcessar"
        Me.btnProcessar.Size = New System.Drawing.Size(126, 23)
        Me.btnProcessar.TabIndex = 96
        Me.btnProcessar.Text = "Processar"
        Me.btnProcessar.UseVisualStyleBackColor = True
        '
        'FrmAcertoCaixa
        '
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.ClientSize = New System.Drawing.Size(1117, 562)
        Me.Controls.Add(Me.btnProcessar)
        Me.Controls.Add(Me.lblValor)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.lblTotalFichas)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.btnAtualizar)
        Me.Controls.Add(Me.chkHeader)
        Me.Controls.Add(Me.btnPesquisarMov)
        Me.Controls.Add(Me.txtDataFim)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtDataInicio)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.gridMovimentacao)
        Me.Controls.Add(Me.lblValorSelecionado)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblTotalFichasSelecionado)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.btnSair)
        Me.Controls.Add(Me.grdFaturamento)
        Me.Controls.Add(Me.Panel1)
        Me.MinimizeBox = False
        Me.Name = "FrmAcertoCaixa"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Consulta Requisição"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.grdFaturamento, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridMovimentacao, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnPesquisar As System.Windows.Forms.Button
    Friend WithEvents cboFuncionario As System.Windows.Forms.ComboBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents grdFaturamento As System.Windows.Forms.DataGridView
    Friend WithEvents btnSair As System.Windows.Forms.Button
    Friend WithEvents lblTotalFichasSelecionado As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblValorSelecionado As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtData As System.Windows.Forms.MaskedTextBox
    Friend WithEvents gridMovimentacao As System.Windows.Forms.DataGridView
    Friend WithEvents txtDataInicio As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtDataFim As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnPesquisarMov As System.Windows.Forms.Button
    Friend WithEvents txtCodMovimentacao As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents chkHeader As System.Windows.Forms.CheckBox
    Friend WithEvents btnAtualizar As System.Windows.Forms.Button
    Friend WithEvents lblValor As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblTotalFichas As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rdCanceladoNP As System.Windows.Forms.RadioButton
    Friend WithEvents rdRecebido As System.Windows.Forms.RadioButton
    Friend WithEvents rdProxDia As System.Windows.Forms.RadioButton
    Friend WithEvents rdCancelado As System.Windows.Forms.RadioButton
    Friend WithEvents rdExtravio As System.Windows.Forms.RadioButton
    Friend WithEvents rdPredatado As System.Windows.Forms.RadioButton
    Friend WithEvents rdConfirmar As System.Windows.Forms.RadioButton
    Friend WithEvents btnProcessar As System.Windows.Forms.Button
    Friend WithEvents rdBordero As System.Windows.Forms.RadioButton
End Class
