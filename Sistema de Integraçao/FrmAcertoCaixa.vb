﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text
Imports Microsoft.Office.Interop


Public Class FrmAcertoCaixa

#Region "Enum"

    Private Enum ColunasGridFaturamento

        Selecao = 0
        cod_mov_contribuicao = 1
        tipo_mov = 2
        tipo_cobranca = 3
        cobrador = 4
        cod_categoria = 5
        data_mov = 6
        cod_ean_cliente = 7
        nome_contribuinte = 8
        valor = 9
        cod_recibo = 10
        dia = 11
        cidade = 12
        cod_mov_contribuicao_descr = 13

    End Enum

    Private Enum ColunasGridMovimentacao

        cod_mov_contribuicao = 0
        tipo_mov = 1
        cobrador = 2
        data_mov = 3
        total_fichas = 4
        valor_fichas = 5

    End Enum

    Private Enum ColunasGridHistorico
        DDD = 0
        Telefone = 1
        Valor = 2
        categoria = 3
        mesRef = 4
        Situacao = 5
        MotivoSituacao = 6
        Data_Situacao = 7
    End Enum

    Enum xlsOption
        xlsOpen
    End Enum

#End Region

#Region "variaveis"

    Public telefone As String
    Public ddd As String
    Private lstContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstFaturamentoSelecionado As New List(Of CGenerico)
    Private v_ambiente As String
    Private rnContribuinte As RNContribuinte = New RNContribuinte
    Private rnRetorno As RNRetorno = New RNRetorno
    Private contribuinte As CContribuinte = New CContribuinte
    Private lstFaturamento As New SortableBindingList(Of CGenerico)
    Private lstMovimentacao As New SortableBindingList(Of CGenerico)
    Private lstContribuinteSelecionadaOi As New List(Of CRetorno)
    Public mNivelAcesso As Integer
    Public mUsuario As String
    Public mCodUsuario As Integer
    Public atualizou As Boolean
    Public bindingSourceContribuite As BindingSource = New BindingSource
    Private rnGenerico As RNGenerico = New RNGenerico
    Public tipo_mov As String = ""


    Public Property NivelAcesso() As Integer
        Get
            Return mNivelAcesso
        End Get
        Set(ByVal value As Integer)
            mNivelAcesso = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return mUsuario
        End Get
        Set(ByVal value As String)
            mUsuario = value
        End Set
    End Property

    Public Property CodUsuario() As Integer
        Get
            Return mCodUsuario
        End Get
        Set(ByVal value As Integer)
            mCodUsuario = value
        End Set
    End Property

#End Region



#Region "Metodos"





    Private Sub CriarColunasGrid()

        Try

            grdFaturamento.DataSource = Nothing
            grdFaturamento.Columns.Clear()

            grdFaturamento.AutoGenerateColumns = False

            grdFaturamento.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            Dim column As New DataGridViewTextBoxColumn

            ''0
            Dim checkboxColumn As New DataGridViewCheckBoxColumn
            checkboxColumn.Width = 32
            checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            grdFaturamento.Columns.Add(checkboxColumn)


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "cod_mov_contribuicao"
            column.HeaderText = "código da movimentação"
            column.Width = 60
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)


            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "tipo_mov"
            column.HeaderText = "Tipo movimentação"
            column.Width = 100
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "tipo_cobranca"
            column.HeaderText = "Tipo cobranca"
            column.Width = 100
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "cobrador"
            column.HeaderText = "Cobrador"
            column.Width = 100
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "cod_categoria"
            column.HeaderText = "código categoria"
            column.Width = 60
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)


            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "data_mov"
            column.HeaderText = "Data movimentação"
            column.Width = 80
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)

            '7
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "cod_ean_cliente"
            column.HeaderText = "código ean cliente"
            column.Width = 70
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)

            '8
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "nome_contribuinte"
            column.HeaderText = "Nome contribuinte"
            column.Width = 200
            column.ReadOnly = True
            grdFaturamento.Columns.Add(column)


            '9
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "valor"
            column.HeaderText = "Valor"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            grdFaturamento.Columns.Add(column)

            '10
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "cod_recibo"
            column.HeaderText = "Código do recibo"
            column.Width = 90
            column.ReadOnly = False
            column.Visible = True
            grdFaturamento.Columns.Add(column)

            '11
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "dia"
            column.HeaderText = "Dia"
            column.Width = 60
            column.ReadOnly = False
            column.Visible = True
            grdFaturamento.Columns.Add(column)


            '12
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "cidade"
            column.HeaderText = "Cidade"
            column.Visible = True
            column.Width = 120
            column.ReadOnly = False
            grdFaturamento.Columns.Add(column)


            '13
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "cod_mov_contribuicao_descr"
            column.HeaderText = "cod_mov_contribuicao_descr"
            column.Visible = False
            column.Width = 60
            column.ReadOnly = False
            grdFaturamento.Columns.Add(column)




        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CriarColunasGridMov()

        Try

            gridMovimentacao.DataSource = Nothing
            gridMovimentacao.Columns.Clear()

            gridMovimentacao.AutoGenerateColumns = False

            gridMovimentacao.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            'Dim checkboxColumn As New DataGridViewCheckBoxColumn
            'checkboxColumn.Width = 32
            'checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'grdFaturamento.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "cod_mov_contribuicao"
            column.HeaderText = "código da movimentação"
            column.Width = 60
            column.ReadOnly = True
            gridMovimentacao.Columns.Add(column)


            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "tipo_mov"
            column.HeaderText = "Tipo movimentação"
            column.Width = 100
            column.ReadOnly = True
            gridMovimentacao.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "cobrador"
            column.HeaderText = "Cobrador"
            column.Width = 100
            column.ReadOnly = True
            gridMovimentacao.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "data_mov"
            column.HeaderText = "Data movimentação"
            column.Width = 80
            column.ReadOnly = True
            gridMovimentacao.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "total_fichas"
            column.HeaderText = "total de fichas"
            column.Width = 70
            column.ReadOnly = True
            gridMovimentacao.Columns.Add(column)


            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "valor_fichas"
            column.HeaderText = "valor das fichas"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            gridMovimentacao.Columns.Add(column)




        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub



#End Region

    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""
        Dim ValorFicha As Decimal = 0
        Dim ParcialValorFicha As Decimal = 0
        Dim Parcialfichas As Integer = 0
        Dim valor As Single
        Dim strValor As String = ""




        Try

            If cboFuncionario.SelectedIndex = 0 Or cboFuncionario.Text = "Selecione..." Then
                MsgBox("Opção invalida, selecionar a situação!", MsgBoxStyle.Critical, "Atenção!")
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor

            If rdConfirmar.Checked = True Then
                tipo_mov = "BAIXADO"
            ElseIf rdPredatado.Checked = True Then
                tipo_mov = "PRE DATADO"
            ElseIf rdProxDia.Checked = True Then
                tipo_mov = "PROX DIA"
            ElseIf rdRecebido.Checked = True Then
                tipo_mov = "BAIXADO"
            ElseIf rdExtravio.Checked = True Then
                tipo_mov = "BORDERO"
            ElseIf rdCancelado.Checked = True Then
                tipo_mov = "CANCELADO"
            ElseIf rdCanceladoNP.Checked = True Then
                tipo_mov = "CANCELADONP"
            ElseIf rdBordero.Checked = True Then
                tipo_mov = "BORDERO"
            End If


            If txtCodMovimentacao.Text <> "" Then

                carregaGridFaturamentoCod(CInt(txtCodMovimentacao.Text))

            Else

                carregaGridFaturamento(tipo_mov, cboFuncionario.Text, CDate(txtData.Text))

            End If


            For i As Integer = 0 To grdFaturamento.RowCount - 1

                ValorFicha = CType(grdFaturamento.Rows(i).Cells(ColunasGridFaturamento.valor).Value, Integer)
                Parcialfichas = Parcialfichas + 1
                ParcialValorFicha = ValorFicha + ParcialValorFicha

            Next

            strValor = valor.ToString("C")
            valor = ParcialValorFicha
            lblValor.Text = valor.ToString("R$ #,###.00")
            lblTotalFichas.Text = Parcialfichas - 1


            lblTotalFichasSelecionado.Text = 0
            lblValorSelecionado.Text = 0

            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try

    End Sub



    Private Sub carregaGridFaturamento(ByVal tipo_mov As String, ByVal funcionario As String, ByVal data As String)
        Try
            Dim objRetorno As CGenerico = New CGenerico()
            Dim ds As DataSet = New DataSet

            lstFaturamento = New SortableBindingList(Of CGenerico)(rnGenerico.CarregarGridFaturamento(tipo_mov, funcionario, data))

            'se ocorrer erro retorna do modo antigo
            'If lstContribuintesOi.Count = 0 Then
            '    lstContribuintesOi = New SortableBindingList(Of CRetorno)(rnRetorno.CarregarFeedContribuintesPelaCategoria(categoria, DtMesAnoPesquisa.Text, codUsuario, "Consulta"))
            'End If


            If (lstFaturamento.Count > 0) Then

                bindingSourceContribuite.DataSource = lstFaturamento

                grdFaturamento.DataSource = bindingSourceContribuite.DataSource
                'grdRemessa.Sort(grdRemessa.Columns(ColunasGridRemessa.Nome), ListSortDirection.Ascending)
                'lblNumRegistros.Text = lstApolices.Count

                '  ds = rnRetorno.CarregarTotaisContribuintesPelaCategoriaNovo(categoria, DtMesAnoPesquisa.Text, codUsuario)

                'If ds.Tables(0).Rows.Count = 0 Then
                '    '   ds = rnRetorno.CarregarTotaisContribuintesPelaCategoria(categoria, DtMesAnoPesquisa.Text, codUsuario)
                'End If

                'If ds.Tables(1).Rows.Count > 0 Then
                '    lblTotalFichasSelecionado.Text = ds.Tables(1).Rows(0).Item(0)
                'End If

                'If ds.Tables(2).Rows.Count > 0 Then

                '    strValor = valor.ToString("C")
                '    valor = IIf(IsDBNull(ds.Tables(2).Rows(0).Item(0)), 0, ds.Tables(2).Rows(0).Item(0))
                '    lblValorSelecionado.Text = valor.ToString("R$ #,###.00")

                'End If

            Else
                grdFaturamento.DataSource = Nothing
                MsgBox("Não existe registros a serem processados, verifique se houve a liberação das fichas!")

            End If

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub carregaGridFaturamentoCod(ByVal cod_movimentacao As Integer)

        Dim objRetorno As CGenerico = New CGenerico()


        Dim ds As DataSet = New DataSet
        Dim valor As Single
        Dim strValor As String

        Try

            lstFaturamento = New SortableBindingList(Of CGenerico)(rnGenerico.CarregarGridFaturamentoCod(cod_movimentacao))

            'se ocorrer erro retorna do modo antigo
            'If lstContribuintesOi.Count = 0 Then
            '    lstContribuintesOi = New SortableBindingList(Of CRetorno)(rnRetorno.CarregarFeedContribuintesPelaCategoria(categoria, DtMesAnoPesquisa.Text, codUsuario, "Consulta"))
            'End If


            If (lstFaturamento.Count > 0) Then

                bindingSourceContribuite.DataSource = lstFaturamento

                grdFaturamento.DataSource = bindingSourceContribuite.DataSource
                'grdRemessa.Sort(grdRemessa.Columns(ColunasGridRemessa.Nome), ListSortDirection.Ascending)
                'lblNumRegistros.Text = lstApolices.Count

                '  ds = rnRetorno.CarregarTotaisContribuintesPelaCategoriaNovo(categoria, DtMesAnoPesquisa.Text, codUsuario)

                If ds.Tables(0).Rows.Count = 0 Then
                    '   ds = rnRetorno.CarregarTotaisContribuintesPelaCategoria(categoria, DtMesAnoPesquisa.Text, codUsuario)
                End If

                If ds.Tables(1).Rows.Count > 0 Then
                    lblTotalFichasSelecionado.Text = ds.Tables(1).Rows(0).Item(0)
                End If

                If ds.Tables(2).Rows.Count > 0 Then

                    strValor = valor.ToString("C")
                    valor = IIf(IsDBNull(ds.Tables(2).Rows(0).Item(0)), 0, ds.Tables(2).Rows(0).Item(0))
                    lblValorSelecionado.Text = valor.ToString("R$ #,###.00")

                End If

            Else
                grdFaturamento.DataSource = Nothing
                MsgBox("Não existe registros a serem processados, verifique se houve a liberação das fichas!")

            End If

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub carregaGridMovimentacao(ByVal dataInicio As String, ByVal dataFim As String)

        Dim objRetorno As CGenerico = New CGenerico()


        Dim ds As DataSet = New DataSet

        Try

            lstMovimentacao = New SortableBindingList(Of CGenerico)(rnGenerico.CarregarGridMovimentacao(dataInicio, dataFim))

            'se ocorrer erro retorna do modo antigo
            'If lstContribuintesOi.Count = 0 Then
            '    lstContribuintesOi = New SortableBindingList(Of CRetorno)(rnRetorno.CarregarFeedContribuintesPelaCategoria(categoria, DtMesAnoPesquisa.Text, codUsuario, "Consulta"))
            'End If


            If (lstMovimentacao.Count > 0) Then

                bindingSourceContribuite.DataSource = lstMovimentacao

                gridMovimentacao.DataSource = bindingSourceContribuite.DataSource
                'grdRemessa.Sort(grdRemessa.Columns(ColunasGridRemessa.Nome), ListSortDirection.Ascending)
                'lblNumRegistros.Text = lstApolices.Count

                '  ds = rnRetorno.CarregarTotaisContribuintesPelaCategoriaNovo(categoria, DtMesAnoPesquisa.Text, codUsuario)

                'If ds.Tables(0).Rows.Count = 0 Then
                '    '   ds = rnRetorno.CarregarTotaisContribuintesPelaCategoria(categoria, DtMesAnoPesquisa.Text, codUsuario)
                'End If

                'If ds.Tables(1).Rows.Count > 0 Then
                '    lblTotalFichasSelecionado.Text = ds.Tables(1).Rows(0).Item(0)
                'End If

                'If ds.Tables(2).Rows.Count > 0 Then

                '    strValor = valor.ToString("C")
                '    valor = IIf(IsDBNull(ds.Tables(2).Rows(0).Item(0)), 0, ds.Tables(2).Rows(0).Item(0))
                '    lblValorSelecionado.Text = valor.ToString("R$ #,###.00")

                'End If

            Else
                grdFaturamento.DataSource = Nothing
                MsgBox("Não existe registros a serem processados!")

            End If


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Close()
    End Sub

    Private Sub FrmAcertoCaixa_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Me.Width = My.Computer.Screen.WorkingArea.Width
        'Me.Height = My.Computer.Screen.WorkingArea.Height

        CarregaComboOperador(0)

        CriarColunasGrid()
        CriarColunasGridMov()

        carregaGridMovimentacao(CDate("01/01/2016"), CDate(Date.Now).ToString())

    End Sub

    Private Sub CarregaComboOperador(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboFuncionario.DataSource = Nothing
            cboFuncionario.Items.Clear()
            cboFuncionario.DisplayMember = "Nome_Operador"
            cboFuncionario.ValueMember = "Cod_Operador"
            cboFuncionario.DataSource = rn_generico.BuscarOperadorDigital(0)

            If cboFuncionario.Items.Count > 0 And cod_operador = 0 Then
                cboFuncionario.SelectedIndex = 0
            ElseIf cboFuncionario.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboFuncionario.Items.Count - 1
                    For Each linha As CGenerico In cboFuncionario.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboFuncionario.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboFuncionario.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub



    Private Sub grdRemessa_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdFaturamento.CellDoubleClick

        Dim ClienteClicado As String

        Try

            ClienteClicado = CType(grdFaturamento.Rows(e.RowIndex).Cells(ColunasGridFaturamento.cod_ean_cliente).Value, String)
            If ClienteClicado = Nothing Then
                ' TelefoneSelecionado = CType(grdFaturamento.Rows(e.RowIndex).Cells(ColunasGridFaturamento.Telefone).Value, Integer)

            End If

            For Each ContribuinteInconsistencia As CGenerico In lstFaturamento
                If ContribuinteInconsistencia.Cod_ean_cliente = ClienteClicado Then

                End If
            Next

            If (e.RowIndex > -1) Then
                Dim frm As frmContribuinte = New frmContribuinte
                frm.FormNome = "Contribuinte"
                frm.Cod_Ean_Cliente = CType(grdFaturamento.Rows(e.RowIndex).Cells(ColunasGridFaturamento.cod_ean_cliente).Value, String)
                frm.CodCategoria = cboFuncionario.SelectedValue
                frm.UserName = CodUsuario

                lstFaturamentoSelecionado = New List(Of CGenerico)
                lstFaturamentoSelecionado.Add(lstFaturamento(e.RowIndex))
                Me.Visible = False
                frm.ShowDialog()

            End If

            contribuinte = rnContribuinte.PesquisarContribuintePorTelefoneSimplificado(telefone, ddd, 1)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub



    Private Sub FrmConsultaUltimaMovimentacao_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""

        Try

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub


    Private Sub btnPesquisarMov_Click(sender As System.Object, e As System.EventArgs) Handles btnPesquisarMov.Click
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""

        Try

            If txtDataInicio.Text = "__\__\____" Or txtDataFim.Text = "__\__\____" Then
                MsgBox("Por favor selecionar as datas para pesquisa!", MsgBoxStyle.Critical, "Atenção!")
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor


            carregaGridMovimentacao(CDate(txtDataInicio.Text), CDate(txtDataFim.Text))


            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub gridMovimentacao_CellContentDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridMovimentacao.CellContentDoubleClick

        Dim FuncionarioClicado As String
        Dim DataSelecionado As String
        Dim codMovimentacaoSelecionado As Long

        Dim ValorFicha As Decimal = 0
        Dim ParcialValorFicha As Decimal = 0
        Dim Parcialfichas As Integer = 0
        Dim valor As Single
        Dim strValor As String

        Try

            FuncionarioClicado = CType(gridMovimentacao.Rows(e.RowIndex).Cells(ColunasGridMovimentacao.cobrador).Value, String)
            If Not FuncionarioClicado = Nothing Then
                DataSelecionado = CType(gridMovimentacao.Rows(e.RowIndex).Cells(ColunasGridMovimentacao.data_mov).Value, String)
                codMovimentacaoSelecionado = CType(gridMovimentacao.Rows(e.RowIndex).Cells(ColunasGridMovimentacao.cod_mov_contribuicao).Value, Integer)

                carregaGridFaturamentoCod(codMovimentacaoSelecionado)



                For i As Integer = 0 To grdFaturamento.RowCount - 1

                    ValorFicha = CType(grdFaturamento.Rows(i).Cells(ColunasGridFaturamento.valor).Value, Integer)
                    Parcialfichas = Parcialfichas + 1
                    ParcialValorFicha = ValorFicha + ParcialValorFicha

                Next

                strValor = valor.ToString("C")
                valor = ParcialValorFicha
                lblValor.Text = valor.ToString("R$ #,###.00")
                lblTotalFichas.Text = Parcialfichas

            End If

            lblValorSelecionado.Text = 0
            lblTotalFichasSelecionado.Text = 0



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub chkHeader_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkHeader.CheckedChanged

        Me.Cursor = Cursors.WaitCursor

        Dim i As Integer
        Dim valorCheckHeader As Boolean

        valorCheckHeader = chkHeader.Checked

        For i = 0 To grdFaturamento.RowCount - 1

            If valorCheckHeader = True Then
                grdFaturamento(ColunasGridFaturamento.Selecao, i).Value = valorCheckHeader
            Else
                grdFaturamento(ColunasGridFaturamento.Selecao, i).Value = valorCheckHeader
            End If
        Next i

        Me.Cursor = Cursors.Default
        grdFaturamento.EndEdit()
    End Sub


    Private Sub btnAtualizar_Click(sender As System.Object, e As System.EventArgs) Handles btnAtualizar.Click
        Dim TelefoneSelecionado As String = ""
        Dim cod_mov_contribuicao As Long = 0
        Dim cod_mov_contribuicao_desr As Long = 0
        Dim ValorFicha As Decimal = 0
        Dim ParcialValorFicha As Decimal = 0
        Dim Parcialfichas As Integer = 0
        Dim valor As Single
        Dim strValor As String

        Try

            For i As Integer = 0 To grdFaturamento.RowCount - 1
                If (Convert.ToBoolean(grdFaturamento.Rows(i).Cells(ColunasGridFaturamento.Selecao).Value)) = True Then

                    ValorFicha = CType(grdFaturamento.Rows(i).Cells(ColunasGridFaturamento.valor).Value, Integer)
                    Parcialfichas = Parcialfichas + 1
                    ParcialValorFicha = ValorFicha + ParcialValorFicha


                End If
            Next

            strValor = valor.ToString("C")
            valor = ParcialValorFicha
            lblValorSelecionado.Text = valor.ToString("R$ #,###.00")
            lblTotalFichasSelecionado.Text = Parcialfichas

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnProcessar_Click(sender As System.Object, e As System.EventArgs) Handles btnProcessar.Click
        Dim cod_mov_contribuicao_descr As Long
        Dim rn As RNGenerico = New RNGenerico
        Dim ValorFicha As Decimal = 0
        Dim ParcialValorFicha As Decimal = 0
        Dim Parcialfichas As Integer = 0
        Dim valor As Single
        Dim strValor As String

        Try

            For i As Integer = 0 To grdFaturamento.RowCount - 1
                If (Convert.ToBoolean(grdFaturamento.Rows(i).Cells(ColunasGridFaturamento.Selecao).Value)) = True Then

                    cod_mov_contribuicao_descr = CType(grdFaturamento.Rows(i).Cells(ColunasGridFaturamento.cod_mov_contribuicao_descr).Value, Integer)

                    If rn.Atualiza_mov_contribuicao_descr(cod_mov_contribuicao_descr) = False Then
                        MsgBox("Ocorreu um erro ao atualizar os registros no código: " & cod_mov_contribuicao_descr & ", tente novamente, ou entre em contato com o administrador")
                        Exit Sub
                    End If

                End If
            Next


            carregaGridFaturamento(tipo_mov, cboFuncionario.Text, CDate(txtData.Text))

            For i As Integer = 0 To grdFaturamento.RowCount - 1

                ValorFicha = CType(grdFaturamento.Rows(i).Cells(ColunasGridFaturamento.valor).Value, Integer)
                Parcialfichas = Parcialfichas + 1
                ParcialValorFicha = ValorFicha + ParcialValorFicha

            Next

            strValor = valor.ToString("C")
            valor = ParcialValorFicha
            lblValor.Text = valor.ToString("R$ #,###.00")
            lblTotalFichas.Text = Parcialfichas - 1


            lblTotalFichasSelecionado.Text = 0
            lblValorSelecionado.Text = 0


            'ATUALIZA OS DADOS DA MOVIMENTACAO
            rn.AtualizaFaturamento(tipo_mov, cboFuncionario.Text, CDate(txtData.Text))

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub Panel1_Paint(sender As System.Object, e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

    End Sub
End Class