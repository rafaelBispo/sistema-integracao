﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmRelatorioOperador
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.lblValorPrevisto = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.lblValor5 = New System.Windows.Forms.Label()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.lblValor4 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.lblValor3 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lblValor2 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblValor1 = New System.Windows.Forms.Label()
        Me.lblPeriodo5 = New System.Windows.Forms.Label()
        Me.lblPeriodo4 = New System.Windows.Forms.Label()
        Me.lblPeriodo3 = New System.Windows.Forms.Label()
        Me.lblPeriodo2 = New System.Windows.Forms.Label()
        Me.lblPeriodo1 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblValorReq = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.gridRequisicao = New System.Windows.Forms.DataGridView()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.chkFiltaDataRep = New System.Windows.Forms.CheckBox()
        Me.gridRepetidos = New System.Windows.Forms.DataGridView()
        Me.btnPequisaRep = New System.Windows.Forms.Button()
        Me.dtRepFinal = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.dtRepInicial = New System.Windows.Forms.DateTimePicker()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.dtInicio = New System.Windows.Forms.DateTimePicker()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.dtFinal = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnPesquisar = New System.Windows.Forms.Button()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblValorTotalRequisicao = New System.Windows.Forms.Label()
        Me.lblValorTotalOperador = New System.Windows.Forms.Label()
        Me.lblValorTotalOutros = New System.Windows.Forms.Label()
        Me.lblValorTotalMensal = New System.Windows.Forms.Label()
        Me.gridResultadoOperadorOutros = New System.Windows.Forms.DataGridView()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.gridResultadoOperadorMensal = New System.Windows.Forms.DataGridView()
        ' Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        ' Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.chkFiltrarData = New System.Windows.Forms.CheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblValorTotalArrecadadoOpera = New System.Windows.Forms.Label()
        Me.chkValorDiario = New System.Windows.Forms.CheckBox()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.gridRequisicao, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.gridRepetidos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel7.SuspendLayout()
        CType(Me.gridResultadoOperadorOutros, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.gridResultadoOperadorMensal, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(12, 59)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(611, 572)
        Me.TabControl1.TabIndex = 3
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.TabPage1.Controls.Add(Me.lblValorPrevisto)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.Panel1)
        Me.TabPage1.Controls.Add(Me.lblValorReq)
        Me.TabPage1.Controls.Add(Me.Label26)
        Me.TabPage1.Controls.Add(Me.gridRequisicao)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(603, 546)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Resumo"
        '
        'lblValorPrevisto
        '
        Me.lblValorPrevisto.AutoSize = True
        Me.lblValorPrevisto.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorPrevisto.Location = New System.Drawing.Point(261, 32)
        Me.lblValorPrevisto.Name = "lblValorPrevisto"
        Me.lblValorPrevisto.Size = New System.Drawing.Size(66, 18)
        Me.lblValorPrevisto.TabIndex = 8
        Me.lblValorPrevisto.Text = "R$ 0,00"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(17, 32)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(228, 18)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Valor total Previsto Operador"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Panel6)
        Me.Panel1.Controls.Add(Me.Panel5)
        Me.Panel1.Controls.Add(Me.Panel4)
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Controls.Add(Me.lblPeriodo5)
        Me.Panel1.Controls.Add(Me.lblPeriodo4)
        Me.Panel1.Controls.Add(Me.lblPeriodo3)
        Me.Panel1.Controls.Add(Me.lblPeriodo2)
        Me.Panel1.Controls.Add(Me.lblPeriodo1)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Location = New System.Drawing.Point(351, 59)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(236, 481)
        Me.Panel1.TabIndex = 6
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.lblValor5)
        Me.Panel6.Location = New System.Drawing.Point(17, 389)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(200, 50)
        Me.Panel6.TabIndex = 15
        '
        'lblValor5
        '
        Me.lblValor5.AutoSize = True
        Me.lblValor5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValor5.Location = New System.Drawing.Point(12, 17)
        Me.lblValor5.Name = "lblValor5"
        Me.lblValor5.Size = New System.Drawing.Size(27, 18)
        Me.lblValor5.TabIndex = 17
        Me.lblValor5.Text = "R$"
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.lblValor4)
        Me.Panel5.Location = New System.Drawing.Point(17, 315)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(200, 50)
        Me.Panel5.TabIndex = 14
        '
        'lblValor4
        '
        Me.lblValor4.AutoSize = True
        Me.lblValor4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValor4.Location = New System.Drawing.Point(12, 15)
        Me.lblValor4.Name = "lblValor4"
        Me.lblValor4.Size = New System.Drawing.Size(27, 18)
        Me.lblValor4.TabIndex = 19
        Me.lblValor4.Text = "R$"
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.lblValor3)
        Me.Panel4.Location = New System.Drawing.Point(17, 241)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(200, 50)
        Me.Panel4.TabIndex = 13
        '
        'lblValor3
        '
        Me.lblValor3.AutoSize = True
        Me.lblValor3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValor3.Location = New System.Drawing.Point(12, 14)
        Me.lblValor3.Name = "lblValor3"
        Me.lblValor3.Size = New System.Drawing.Size(27, 18)
        Me.lblValor3.TabIndex = 18
        Me.lblValor3.Text = "R$"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.lblValor2)
        Me.Panel3.Location = New System.Drawing.Point(17, 167)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(200, 50)
        Me.Panel3.TabIndex = 12
        '
        'lblValor2
        '
        Me.lblValor2.AutoSize = True
        Me.lblValor2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValor2.Location = New System.Drawing.Point(12, 16)
        Me.lblValor2.Name = "lblValor2"
        Me.lblValor2.Size = New System.Drawing.Size(27, 18)
        Me.lblValor2.TabIndex = 17
        Me.lblValor2.Text = "R$"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lblValor1)
        Me.Panel2.Location = New System.Drawing.Point(17, 93)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(200, 50)
        Me.Panel2.TabIndex = 11
        '
        'lblValor1
        '
        Me.lblValor1.AutoSize = True
        Me.lblValor1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValor1.Location = New System.Drawing.Point(12, 15)
        Me.lblValor1.Name = "lblValor1"
        Me.lblValor1.Size = New System.Drawing.Size(27, 18)
        Me.lblValor1.TabIndex = 16
        Me.lblValor1.Text = "R$"
        '
        'lblPeriodo5
        '
        Me.lblPeriodo5.AutoSize = True
        Me.lblPeriodo5.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodo5.Location = New System.Drawing.Point(14, 368)
        Me.lblPeriodo5.Name = "lblPeriodo5"
        Me.lblPeriodo5.Size = New System.Drawing.Size(56, 18)
        Me.lblPeriodo5.TabIndex = 10
        Me.lblPeriodo5.Text = "--/--/----"
        '
        'lblPeriodo4
        '
        Me.lblPeriodo4.AutoSize = True
        Me.lblPeriodo4.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodo4.Location = New System.Drawing.Point(14, 294)
        Me.lblPeriodo4.Name = "lblPeriodo4"
        Me.lblPeriodo4.Size = New System.Drawing.Size(56, 18)
        Me.lblPeriodo4.TabIndex = 9
        Me.lblPeriodo4.Text = "--/--/----"
        '
        'lblPeriodo3
        '
        Me.lblPeriodo3.AutoSize = True
        Me.lblPeriodo3.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodo3.Location = New System.Drawing.Point(14, 220)
        Me.lblPeriodo3.Name = "lblPeriodo3"
        Me.lblPeriodo3.Size = New System.Drawing.Size(56, 18)
        Me.lblPeriodo3.TabIndex = 8
        Me.lblPeriodo3.Text = "--/--/----"
        '
        'lblPeriodo2
        '
        Me.lblPeriodo2.AutoSize = True
        Me.lblPeriodo2.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodo2.Location = New System.Drawing.Point(14, 146)
        Me.lblPeriodo2.Name = "lblPeriodo2"
        Me.lblPeriodo2.Size = New System.Drawing.Size(56, 18)
        Me.lblPeriodo2.TabIndex = 7
        Me.lblPeriodo2.Text = "--/--/----"
        '
        'lblPeriodo1
        '
        Me.lblPeriodo1.AutoSize = True
        Me.lblPeriodo1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeriodo1.Location = New System.Drawing.Point(14, 72)
        Me.lblPeriodo1.Name = "lblPeriodo1"
        Me.lblPeriodo1.Size = New System.Drawing.Size(56, 18)
        Me.lblPeriodo1.TabIndex = 6
        Me.lblPeriodo1.Text = "--/--/----"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(14, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(177, 20)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Resumo por Semana"
        '
        'lblValorReq
        '
        Me.lblValorReq.AutoSize = True
        Me.lblValorReq.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorReq.Location = New System.Drawing.Point(261, 9)
        Me.lblValorReq.Name = "lblValorReq"
        Me.lblValorReq.Size = New System.Drawing.Size(66, 18)
        Me.lblValorReq.TabIndex = 4
        Me.lblValorReq.Text = "R$ 0,00"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(17, 9)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(228, 18)
        Me.Label26.TabIndex = 2
        Me.Label26.Text = "Valor total das fichas no mês"
        '
        'gridRequisicao
        '
        Me.gridRequisicao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridRequisicao.Location = New System.Drawing.Point(6, 59)
        Me.gridRequisicao.Name = "gridRequisicao"
        Me.gridRequisicao.Size = New System.Drawing.Size(332, 481)
        Me.gridRequisicao.TabIndex = 0
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.LightGray
        Me.TabPage2.Controls.Add(Me.chkFiltaDataRep)
        Me.TabPage2.Controls.Add(Me.gridRepetidos)
        Me.TabPage2.Controls.Add(Me.btnPequisaRep)
        Me.TabPage2.Controls.Add(Me.dtRepFinal)
        Me.TabPage2.Controls.Add(Me.Label10)
        Me.TabPage2.Controls.Add(Me.dtRepInicial)
        Me.TabPage2.Controls.Add(Me.Label11)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(603, 546)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Verifica Duplicidade"
        '
        'chkFiltaDataRep
        '
        Me.chkFiltaDataRep.AutoSize = True
        Me.chkFiltaDataRep.Checked = True
        Me.chkFiltaDataRep.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFiltaDataRep.Location = New System.Drawing.Point(13, 38)
        Me.chkFiltaDataRep.Name = "chkFiltaDataRep"
        Me.chkFiltaDataRep.Size = New System.Drawing.Size(93, 17)
        Me.chkFiltaDataRep.TabIndex = 85
        Me.chkFiltaDataRep.Text = "Filtrar por data"
        Me.chkFiltaDataRep.UseVisualStyleBackColor = True
        '
        'gridRepetidos
        '
        Me.gridRepetidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridRepetidos.Location = New System.Drawing.Point(13, 59)
        Me.gridRepetidos.Name = "gridRepetidos"
        Me.gridRepetidos.Size = New System.Drawing.Size(572, 481)
        Me.gridRepetidos.TabIndex = 84
        '
        'btnPequisaRep
        '
        Me.btnPequisaRep.Location = New System.Drawing.Point(525, 14)
        Me.btnPequisaRep.Name = "btnPequisaRep"
        Me.btnPequisaRep.Size = New System.Drawing.Size(75, 23)
        Me.btnPequisaRep.TabIndex = 83
        Me.btnPequisaRep.Text = "Pesquisar"
        Me.btnPequisaRep.UseVisualStyleBackColor = True
        '
        'dtRepFinal
        '
        Me.dtRepFinal.CustomFormat = ""
        Me.dtRepFinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtRepFinal.Location = New System.Drawing.Point(338, 16)
        Me.dtRepFinal.Name = "dtRepFinal"
        Me.dtRepFinal.Size = New System.Drawing.Size(178, 20)
        Me.dtRepFinal.TabIndex = 82
        Me.dtRepFinal.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(272, 19)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(66, 13)
        Me.Label10.TabIndex = 81
        Me.Label10.Text = "Data final:"
        '
        'dtRepInicial
        '
        Me.dtRepInicial.CustomFormat = ""
        Me.dtRepInicial.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtRepInicial.Location = New System.Drawing.Point(87, 16)
        Me.dtRepInicial.Name = "dtRepInicial"
        Me.dtRepInicial.Size = New System.Drawing.Size(178, 20)
        Me.dtRepInicial.TabIndex = 80
        Me.dtRepInicial.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(10, 19)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(76, 13)
        Me.Label11.TabIndex = 79
        Me.Label11.Text = "Data Inicial:"
        '
        'dtInicio
        '
        Me.dtInicio.CustomFormat = ""
        Me.dtInicio.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtInicio.Location = New System.Drawing.Point(91, 12)
        Me.dtInicio.Name = "dtInicio"
        Me.dtInicio.Size = New System.Drawing.Size(178, 20)
        Me.dtInicio.TabIndex = 75
        Me.dtInicio.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(12, 15)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(76, 13)
        Me.Label18.TabIndex = 74
        Me.Label18.Text = "Data Inicial:"
        '
        'dtFinal
        '
        Me.dtFinal.CustomFormat = ""
        Me.dtFinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtFinal.Location = New System.Drawing.Point(353, 12)
        Me.dtFinal.Name = "dtFinal"
        Me.dtFinal.Size = New System.Drawing.Size(178, 20)
        Me.dtFinal.TabIndex = 77
        Me.dtFinal.Value = New Date(2018, 11, 12, 0, 0, 0, 0)
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(274, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(66, 13)
        Me.Label1.TabIndex = 76
        Me.Label1.Text = "Data final:"
        '
        'btnPesquisar
        '
        Me.btnPesquisar.Location = New System.Drawing.Point(544, 10)
        Me.btnPesquisar.Name = "btnPesquisar"
        Me.btnPesquisar.Size = New System.Drawing.Size(75, 23)
        Me.btnPesquisar.TabIndex = 78
        Me.btnPesquisar.Text = "Pesquisar"
        Me.btnPesquisar.UseVisualStyleBackColor = True
        '
        'Panel7
        '
        Me.Panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel7.Controls.Add(Me.Label8)
        Me.Panel7.Controls.Add(Me.Label2)
        Me.Panel7.Controls.Add(Me.lblValorTotalRequisicao)
        Me.Panel7.Controls.Add(Me.lblValorTotalOperador)
        Me.Panel7.Controls.Add(Me.lblValorTotalOutros)
        Me.Panel7.Controls.Add(Me.lblValorTotalMensal)
        Me.Panel7.Controls.Add(Me.gridResultadoOperadorOutros)
        Me.Panel7.Controls.Add(Me.Label7)
        Me.Panel7.Controls.Add(Me.Label6)
        Me.Panel7.Controls.Add(Me.gridResultadoOperadorMensal)
        ' Me.Panel7.Controls.Add(Me.ShapeContainer1)
        Me.Panel7.Location = New System.Drawing.Point(629, 10)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(681, 617)
        Me.Panel7.TabIndex = 79
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(358, 21)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(17, 24)
        Me.Label8.TabIndex = 18
        Me.Label8.Text = "-"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(3, 2)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(186, 20)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Resumo por Operador"
        '
        'lblValorTotalRequisicao
        '
        Me.lblValorTotalRequisicao.AutoSize = True
        Me.lblValorTotalRequisicao.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorTotalRequisicao.ForeColor = System.Drawing.Color.Fuchsia
        Me.lblValorTotalRequisicao.Location = New System.Drawing.Point(375, 27)
        Me.lblValorTotalRequisicao.Name = "lblValorTotalRequisicao"
        Me.lblValorTotalRequisicao.Size = New System.Drawing.Size(164, 13)
        Me.lblValorTotalRequisicao.TabIndex = 16
        Me.lblValorTotalRequisicao.Text = "Valor Total Requisição : R$"
        '
        'lblValorTotalOperador
        '
        Me.lblValorTotalOperador.AutoSize = True
        Me.lblValorTotalOperador.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorTotalOperador.ForeColor = System.Drawing.Color.LimeGreen
        Me.lblValorTotalOperador.Location = New System.Drawing.Point(375, 49)
        Me.lblValorTotalOperador.Name = "lblValorTotalOperador"
        Me.lblValorTotalOperador.Size = New System.Drawing.Size(149, 13)
        Me.lblValorTotalOperador.TabIndex = 15
        Me.lblValorTotalOperador.Text = "Valor Total Operador: R$"
        '
        'lblValorTotalOutros
        '
        Me.lblValorTotalOutros.AutoSize = True
        Me.lblValorTotalOutros.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorTotalOutros.ForeColor = System.Drawing.Color.LimeGreen
        Me.lblValorTotalOutros.Location = New System.Drawing.Point(398, 323)
        Me.lblValorTotalOutros.Name = "lblValorTotalOutros"
        Me.lblValorTotalOutros.Size = New System.Drawing.Size(126, 18)
        Me.lblValorTotalOutros.TabIndex = 14
        Me.lblValorTotalOutros.Text = "Valor Total : R$"
        '
        'lblValorTotalMensal
        '
        Me.lblValorTotalMensal.AutoSize = True
        Me.lblValorTotalMensal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorTotalMensal.ForeColor = System.Drawing.Color.DodgerBlue
        Me.lblValorTotalMensal.Location = New System.Drawing.Point(375, 10)
        Me.lblValorTotalMensal.Name = "lblValorTotalMensal"
        Me.lblValorTotalMensal.Size = New System.Drawing.Size(97, 13)
        Me.lblValorTotalMensal.TabIndex = 13
        Me.lblValorTotalMensal.Text = "Valor Total : R$"
        '
        'gridResultadoOperadorOutros
        '
        Me.gridResultadoOperadorOutros.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridResultadoOperadorOutros.Location = New System.Drawing.Point(3, 348)
        Me.gridResultadoOperadorOutros.Name = "gridResultadoOperadorOutros"
        Me.gridResultadoOperadorOutros.Size = New System.Drawing.Size(673, 262)
        Me.gridResultadoOperadorOutros.TabIndex = 12
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(3, 323)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(290, 18)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Valor Arrecadação Outras Categorias"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(3, 46)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(275, 18)
        Me.Label6.TabIndex = 10
        Me.Label6.Text = "Valor Arrecadação Mensal/Especial"
        '
        'gridResultadoOperadorMensal
        '
        Me.gridResultadoOperadorMensal.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridResultadoOperadorMensal.Location = New System.Drawing.Point(3, 71)
        Me.gridResultadoOperadorMensal.Name = "gridResultadoOperadorMensal"
        Me.gridResultadoOperadorMensal.Size = New System.Drawing.Size(673, 242)
        Me.gridResultadoOperadorMensal.TabIndex = 7
        '
        'ShapeContainer1
        '
        'Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        'Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        'Me.ShapeContainer1.Name = "ShapeContainer1"
        'Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        'Me.ShapeContainer1.Size = New System.Drawing.Size(679, 615)
        'Me.ShapeContainer1.TabIndex = 17
        'Me.ShapeContainer1.TabStop = False
        ''
        ''LineShape1
        ''
        'Me.LineShape1.BorderWidth = 2
        'Me.LineShape1.Name = "LineShape1"
        'Me.LineShape1.X1 = 371
        'Me.LineShape1.X2 = 594
        'Me.LineShape1.Y1 = 44
        'Me.LineShape1.Y2 = 44
        '
        'chkFiltrarData
        '
        Me.chkFiltrarData.AutoSize = True
        Me.chkFiltrarData.Location = New System.Drawing.Point(12, 38)
        Me.chkFiltrarData.Name = "chkFiltrarData"
        Me.chkFiltrarData.Size = New System.Drawing.Size(149, 17)
        Me.chkFiltrarData.TabIndex = 80
        Me.chkFiltrarData.Text = "Filtrar operadores por data"
        Me.chkFiltrarData.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.Red
        Me.Label4.Location = New System.Drawing.Point(19, 634)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(264, 13)
        Me.Label4.TabIndex = 81
        Me.Label4.Text = "* Valores se referindo a (Requisição - Valores Mensais)"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(634, 637)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(279, 18)
        Me.Label9.TabIndex = 82
        Me.Label9.Text = "Valor Arrecadado previsto Operador"
        '
        'lblValorTotalArrecadadoOpera
        '
        Me.lblValorTotalArrecadadoOpera.AutoSize = True
        Me.lblValorTotalArrecadadoOpera.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorTotalArrecadadoOpera.Location = New System.Drawing.Point(911, 637)
        Me.lblValorTotalArrecadadoOpera.Name = "lblValorTotalArrecadadoOpera"
        Me.lblValorTotalArrecadadoOpera.Size = New System.Drawing.Size(29, 18)
        Me.lblValorTotalArrecadadoOpera.TabIndex = 83
        Me.lblValorTotalArrecadadoOpera.Text = "R$"
        '
        'chkValorDiario
        '
        Me.chkValorDiario.AutoSize = True
        Me.chkValorDiario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkValorDiario.Location = New System.Drawing.Point(353, 38)
        Me.chkValorDiario.Name = "chkValorDiario"
        Me.chkValorDiario.Size = New System.Drawing.Size(174, 19)
        Me.chkValorDiario.TabIndex = 84
        Me.chkValorDiario.Text = "Verificar Valores Diario"
        Me.chkValorDiario.UseVisualStyleBackColor = True
        '
        'FrmRelatorioOperador
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1322, 664)
        Me.Controls.Add(Me.chkValorDiario)
        Me.Controls.Add(Me.lblValorTotalArrecadadoOpera)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.chkFiltrarData)
        Me.Controls.Add(Me.Panel7)
        Me.Controls.Add(Me.btnPesquisar)
        Me.Controls.Add(Me.dtFinal)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dtInicio)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "FrmRelatorioOperador"
        Me.Text = "Relatório Operador"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.gridRequisicao, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.gridRepetidos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        CType(Me.gridResultadoOperadorOutros, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.gridResultadoOperadorMensal, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents lblValorReq As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents gridRequisicao As System.Windows.Forms.DataGridView
    Friend WithEvents dtInicio As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents dtFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnPesquisar As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblPeriodo5 As System.Windows.Forms.Label
    Friend WithEvents lblPeriodo4 As System.Windows.Forms.Label
    Friend WithEvents lblPeriodo3 As System.Windows.Forms.Label
    Friend WithEvents lblPeriodo2 As System.Windows.Forms.Label
    Friend WithEvents lblPeriodo1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblValor5 As System.Windows.Forms.Label
    Friend WithEvents lblValor4 As System.Windows.Forms.Label
    Friend WithEvents lblValor3 As System.Windows.Forms.Label
    Friend WithEvents lblValor2 As System.Windows.Forms.Label
    Friend WithEvents lblValor1 As System.Windows.Forms.Label
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents gridResultadoOperadorMensal As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkFiltrarData As System.Windows.Forms.CheckBox
    Friend WithEvents lblValorPrevisto As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents gridResultadoOperadorOutros As System.Windows.Forms.DataGridView
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents lblValorTotalOutros As System.Windows.Forms.Label
    Friend WithEvents lblValorTotalMensal As System.Windows.Forms.Label
    Friend WithEvents lblValorTotalRequisicao As System.Windows.Forms.Label
    Friend WithEvents lblValorTotalOperador As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    'Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    'Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblValorTotalArrecadadoOpera As System.Windows.Forms.Label
    Friend WithEvents chkValorDiario As System.Windows.Forms.CheckBox
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents chkFiltaDataRep As System.Windows.Forms.CheckBox
    Friend WithEvents gridRepetidos As System.Windows.Forms.DataGridView
    Friend WithEvents btnPequisaRep As System.Windows.Forms.Button
    Friend WithEvents dtRepFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents dtRepInicial As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label11 As System.Windows.Forms.Label
End Class
