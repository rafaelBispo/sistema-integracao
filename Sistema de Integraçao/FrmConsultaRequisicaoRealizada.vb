﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text
Imports Microsoft.Office.Interop
Imports System.Threading
Imports System.Windows.Forms.VisualStyles.VisualStyleElement


Public Class FrmConsultaRequisicaoRealizada


#Region "Enum"

    Private Enum ColunasGridRemessa

        DDD = 0
        Telefone = 1
        Nome = 2
        Valor = 3
        Cod_EAN_CLIENTE = 4
        Operador = 5
        Categoria = 7
        Dt_inclusao = 8
        MesRefSeguinte = 9
        Cpf = 10
        Autorizante = 11
        Observacao = 12
        RequisicaoId = 14
    End Enum


    Private Enum ColunasGridHistorico

        Cod_EAN_Cliente = 0
        Nome_contribuinte = 1
        Data_mov = 2
        Tipo_mov = 3
        Tipo_cobranca = 4
        Cod_Categoria = 5
        DDD = 6
        Telefone1 = 7
        Valor = 8
        Cod_recibo = 9
        Cidade = 10

    End Enum

    Enum xlsOption
        xlsOpen
    End Enum

#End Region

#Region "variaveis"

    Public telefone As String
    Private lstContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstContribuinteSelecionada As New List(Of CRetorno)
    Private v_ambiente As String
    Private rnContribuinte As RNContribuinte = New RNContribuinte
    Private rnRetorno As RNRetorno = New RNRetorno
    Private contribuinte As CContribuinte = New CContribuinte
    'Private lstRequisicaoSelecionada As New SortableBindingList(Of CListaTelefonica)
    Private lstRequisicaoSelecionada As New SortableBindingList(Of CGenerico)
    Private lstContribuintesOi As New SortableBindingList(Of CRetorno)
    Private lstContribuinteSelecionadaOi As New List(Of CRetorno)
    Public mNivelAcesso As Integer
    Public mUsuario As String
    Public mCodUsuario As Integer
    Public atualizou As Boolean
    Public bindingSourceContribuite As BindingSource = New BindingSource
    Private rnGenerico As RNGenerico = New RNGenerico
    Private lstRequisicao As New SortableBindingList(Of CGenerico)
    Private lstListaTelefonica As New SortableBindingList(Of CListaTelefonica)
    Private lstOperador As New List(Of CGenerico)
    Public bindingSourceRequisicao As BindingSource = New BindingSource
    Public mOperadorId As Integer
    Public mOperador As String
    Public mMesRef As String
    Public mSelecionado As Integer
    Public RequisicaoIdSelecionado As Long = 0
    Public ClienteClicado As String
    Public DDDSelecionado As String = ""
    Public TelefoneSelecionado As String = ""
    Public PesquisaSelecionado As String = ""
    Public totalValorFichas As Integer = 0
    Public Delegate Sub CarregaValorCriticadoDelegate(dtInicial As Date, dtFinal As Date, operadorId As Integer, ByRef valorTotal As Integer)


    Public Property NivelAcesso() As Integer
        Get
            Return mNivelAcesso
        End Get
        Set(ByVal value As Integer)
            mNivelAcesso = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return mUsuario
        End Get
        Set(ByVal value As String)
            mUsuario = value
        End Set
    End Property

    Public Property CodUsuario() As Integer
        Get
            Return mCodUsuario
        End Get
        Set(ByVal value As Integer)
            mCodUsuario = value
        End Set
    End Property

    Public Property OperadorId() As Integer
        Get
            Return mOperadorId
        End Get
        Set(ByVal value As Integer)
            mOperadorId = value
        End Set
    End Property

    Public Property Operador() As String
        Get
            Return mOperador
        End Get
        Set(ByVal value As String)
            mOperador = value
        End Set
    End Property

    Public Property MesRef() As String
        Get
            Return mMesRef
        End Get
        Set(ByVal value As String)
            mMesRef = value
        End Set
    End Property

    Public Property Selecionado() As Integer
        Get
            Return mSelecionado
        End Get
        Set(ByVal value As Integer)
            mSelecionado = value
        End Set
    End Property

#End Region



#Region "Metodos"



    Private Sub carregaGrid()

        Dim objContribuintes As CContribuinte = New CContribuinte()

        Dim bindingSourceApolice As BindingSource = New BindingSource

        Try



            lstContribuintes = New SortableBindingList(Of CContribuinte)(rnContribuinte.CarregarGridOi(objContribuintes, "Remessa"))

            If (lstContribuintes.Count > 0) Then

                bindingSourceApolice.DataSource = lstContribuintes

                grdRemessa.DataSource = bindingSourceApolice.DataSource
                'grdRemessa.Sort(grdRemessa.Columns(ColunasGridRemessa.Nome), ListSortDirection.Ascending)
                'lblNumRegistros.Text = lstApolices.Count


            End If


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CriarColunasGrid()

        Try

            grdRemessa.DataSource = Nothing
            grdRemessa.Columns.Clear()

            grdRemessa.AutoGenerateColumns = False

            grdRemessa.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            Dim checkboxColumn As New DataGridViewCheckBoxColumn
            checkboxColumn.Width = 32
            checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StDDDMeioAcessoCobranca"
            column.HeaderText = "DDD"
            column.Width = 30
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)


            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StNumMeioAcessoCobranca"
            column.HeaderText = "Telefone"
            column.Width = 80
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "nome_cliente"
            column.HeaderText = "Nome"
            column.Width = 300
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DVlrServico"
            column.HeaderText = "Valor"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            grdRemessa.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "cod_ean_cliente"
            column.HeaderText = "Cod. EAN Cliente"
            column.Width = 100
            column.ReadOnly = False
            column.Visible = True
            grdRemessa.Columns.Add(column)

            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UsSitFaturamento"
            column.HeaderText = "Situacao Faturamento"
            column.Width = 160
            column.ReadOnly = False
            column.Visible = True
            grdRemessa.Columns.Add(column)


            '7
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UiMotivoSituacao"
            column.HeaderText = "Motivo Situação"
            column.Visible = True
            column.Width = 150
            column.ReadOnly = False
            grdRemessa.Columns.Add(column)




        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CriarColunasGridRequisicao()

        Try

            grdRemessa.DataSource = Nothing
            grdRemessa.Columns.Clear()

            grdRemessa.AutoGenerateColumns = False

            grdRemessa.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            'Dim checkboxColumn As New DataGridViewCheckBoxColumn
            'checkboxColumn.Width = 32
            'checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DDD"
            column.HeaderText = "DDD"
            column.Width = 50
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)


            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Telefone"
            column.HeaderText = "Telefone"
            column.Width = 80
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Nome_cliente"
            column.HeaderText = "Nome"
            column.Width = 250
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Valor"
            column.HeaderText = "Valor"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            grdRemessa.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_ean"
            column.HeaderText = "Cod. EAN Cliente"
            column.Width = 80
            column.ReadOnly = False
            column.Visible = True
            grdRemessa.Columns.Add(column)

            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Operador"
            column.HeaderText = "Operador"
            column.Width = 120
            column.ReadOnly = False
            column.Visible = True
            grdRemessa.Columns.Add(column)


            '7
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Aceite"
            column.HeaderText = "Aceite"
            column.Visible = True
            column.Width = 80
            column.ReadOnly = False
            grdRemessa.Columns.Add(column)


            '7
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "hist_Aceite"
            column.HeaderText = "Ultimo Aceite"
            column.Visible = True
            column.Width = 80
            column.ReadOnly = False
            grdRemessa.Columns.Add(column)


            '8
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Categoria"
            column.HeaderText = "Categoria"
            column.Visible = True
            column.Width = 60
            column.ReadOnly = False
            grdRemessa.Columns.Add(column)

            '9
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Dt_inclusao"
            column.HeaderText = "Data inclusão"
            column.Visible = True
            column.Width = 120
            column.ReadOnly = False
            grdRemessa.Columns.Add(column)

            '10
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "MesRefSeguinte"
            column.HeaderText = "Mês Referência"
            column.Visible = True
            column.Width = 80
            column.ReadOnly = False
            grdRemessa.Columns.Add(column)



            '12
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cpf"
            column.HeaderText = "CPF"
            column.Visible = True
            column.Width = 80
            column.ReadOnly = False
            grdRemessa.Columns.Add(column)

            '13
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Autorizante"
            column.HeaderText = "Autorizante"
            column.Visible = True
            column.Width = 80
            column.ReadOnly = False
            grdRemessa.Columns.Add(column)

            '13
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Observacao"
            column.HeaderText = "Observação"
            column.Visible = True
            column.Width = 80
            column.ReadOnly = False
            grdRemessa.Columns.Add(column)

            '14
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "requisicao_id"
            column.HeaderText = "Requisição Id"
            column.Visible = True
            column.Width = 80
            column.ReadOnly = False
            grdRemessa.Columns.Add(column)



        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CriarColunasGridHistorico()

        Try

            GrdHistorico.DataSource = Nothing
            GrdHistorico.Columns.Clear()

            GrdHistorico.AutoGenerateColumns = False

            GrdHistorico.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            'Dim checkboxColumn As New DataGridViewCheckBoxColumn
            'checkboxColumn.Width = 32
            'checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_EAN_Cliente"
            column.HeaderText = "Cód. EAN Cliente"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Nome_contribuinte"
            column.HeaderText = "Nome contribuinte"
            column.Width = 300
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Data_mov"
            column.HeaderText = "Data Movimentação"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Tipo_mov"
            column.HeaderText = "Tipo Movimentação"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Tipo_cobranca"
            column.HeaderText = "Tipo Cobranca"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_Categoria"
            column.HeaderText = "Cód Categoria"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DDD"
            column.HeaderText = "DDD"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Telefone"
            column.HeaderText = "Telefone"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "valor"
            column.HeaderText = "Valor"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            GrdHistorico.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_recibo"
            column.HeaderText = "Cód. Recibo"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            GrdHistorico.Columns.Add(column)

            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cidade"
            column.HeaderText = "Cidade"
            column.Width = 80
            column.ReadOnly = True
            column.Visible = True
            GrdHistorico.Columns.Add(column)




        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

#End Region

    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Try
            Me.Cursor = Cursors.WaitCursor
            Dim situacao As String = ""
            Dim tipoPesquisa As Integer = 0
            Dim verificaGrau As Boolean
            Dim isEnviado As Boolean = False
            Dim dataRemessa As String = ""
            Dim dataServico As String = ""
            Dim rnRetorno As RNRetorno = New RNRetorno

            lblValor.Text = "R$ 0,000.00"
            lblTotalFichas.Text = "0"

            verificaGrau = chkVerificaGrau.CheckState
            PesquisaSelecionado = cboPesquisa.SelectedValue
            If txtTelefone.Text <> String.Empty Then
                carregaGridRequisicoes(txtTelefone.Text, txtDDDPesquisa.Text)

            ElseIf chkPesquisaData.Checked = True And cboPesquisa.SelectedIndex > 0 Then
                If chkTipoPesquisa.Checked = True Then
                    tipoPesquisa = 1
                Else
                    tipoPesquisa = 0
                End If
                carregaGridRequisicoesPorPeriodoComplementar(dtInicial.Text, dtFinal.Text, cboAceite.SelectedIndex, chkNaoConforme.Checked, cboPesquisa.SelectedValue, tipoPesquisa, verificaGrau)

            ElseIf chkPesquisaData.Checked = True Then
                carregaGridRequisicoesPorPeriodo(dtInicial.Text, dtFinal.Text, cboAceite.SelectedIndex, chkNaoConforme.Checked)

            ElseIf cboPesquisa.SelectedIndex > 0 Then
                '0 =  categoria, 1 =  operador

                If chkTipoPesquisa.Checked = True Then
                    tipoPesquisa = 1
                    carregaFeedGridRequisicoes(cboPesquisa.SelectedValue, CodUsuario, 1)
                Else
                    tipoPesquisa = 0
                    carregaFeedGridRequisicoes(cboPesquisa.SelectedValue, CodUsuario, 0)
                End If

            End If

            dataRemessa = Date.Now
            dataServico = "07" + dataRemessa.Substring(2, 8)
            dataRemessa = Replace(dataRemessa, "/", "")
            dataRemessa = dataRemessa.Substring(4, 4) + dataRemessa.Substring(2, 2)


            isEnviado = rnRetorno.ConsultaTelefoneEnviadoRemessa(txtDDDPesquisa.Text, txtTelefone.Text, dataRemessa)

            lblSemCritica.Visible = True

            If isEnviado = True Then

                lblSemCritica.Text = "ENVIADO NA REMESSA DE " & dataServico & "!"
                lblSemCritica.ForeColor = Color.Red

            Else
                lblSemCritica.Text = "TELEFONE NÃO ENVIADO NA REMESSA"
                lblSemCritica.ForeColor = Color.Green
            End If

            LimpaTela()

            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try

    End Sub



    Private Sub carregaFeedGridRequisicoes(ByVal filtro As String, ByVal codUsuario As Integer, ByVal tipoPesquisa As Integer) '0 =  categoria, 1 =  operador

        Dim objRetorno As CRetorno = New CRetorno()


        Dim ds As DataSet = New DataSet

        Try

            lstContribuintesOi = New SortableBindingList(Of CRetorno)(rnRetorno.CarregarFeedRetornaRequisicao(filtro, dtInicial.Text, codUsuario, "Consulta", tipoPesquisa))

            If (lstContribuintesOi.Count > 0) Then

                CriarColunasGrid()

                bindingSourceContribuite.DataSource = lstContribuintesOi

                grdRemessa.DataSource = bindingSourceContribuite.DataSource
                'grdRemessa.Sort(grdRemessa.Columns(ColunasGridRemessa.Nome), ListSortDirection.Ascending)
                'lblNumRegistros.Text = lstApolices.Count

                ds = rnRetorno.CarregarTotaisRetornoRequisicao(filtro, dtInicial.Text, codUsuario, tipoPesquisa)

                If ds.Tables(0).Rows.Count = 0 Then
                    'ds = rnRetorno.CarregarTotaisContribuintesPelaCategoria(filtro, DtMesAnoPesquisa.Text, codUsuario, tipoPesquisa)
                End If

            Else
                grdRemessa.DataSource = Nothing
                MsgBox("Não existe registros a serem processados")

            End If

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub carregaGridRequisicoes(ByVal telefone As String, ByVal DDD As String)
        Try
            Dim objGenerico As CGenerico = New CGenerico()
            Dim valor As Single
            Dim strValor As String
            Dim ds As DataSet = New DataSet
            lstRequisicao = New SortableBindingList(Of CGenerico)(rnGenerico.CarregarRequisicaoPorTelefone(telefone, DDD, "Consulta"))
            If (lstRequisicao.Count > 0) Then
                CriarColunasGridRequisicao()
                bindingSourceRequisicao.DataSource = lstRequisicao
                grdRemessa.DataSource = bindingSourceRequisicao.DataSource
                For Each col As DataGridViewRow In grdRemessa.Rows
                    totalValorFichas = totalValorFichas + col.Cells(3).Value
                Next
                strValor = valor.ToString("C")
                valor = totalValorFichas
                lblValor.Text = valor.ToString("R$ #,###.00")
                lblTotalFichas.Text = grdRemessa.Rows.Count - 1
            Else
                grdRemessa.DataSource = Nothing
                MsgBox("Não existe registros a serem processados")
            End If
        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub carregaGridRequisicoesPorPeriodo(ByVal dtInicial As Date, ByVal dtFinal As Date, ByVal tipoAceite As Integer, ByVal naoConforme As Boolean)
        Try
            Dim objGenerico As New CGenerico
            Dim valor As Single
            Dim strValor As String
            lstRequisicao = New SortableBindingList(Of CGenerico)(rnGenerico.CarregarRequisicaoPorPeriodoData(dtInicial, dtFinal, tipoAceite, naoConforme, "Consulta"))
            If (lstRequisicao.Count > 0) Then
                lblValorCriticado.Text = "CARREGANDO..."
                lblValorComissao.Text = "CARREGANDO..."
                CriarColunasGridRequisicao()
                CarregamentoAssincrono()
                bindingSourceRequisicao.DataSource = lstRequisicao
                grdRemessa.DataSource = bindingSourceRequisicao.DataSource
                For Each col As DataGridViewRow In grdRemessa.Rows
                    totalValorFichas = totalValorFichas + col.Cells(3).Value
                Next
                strValor = valor.ToString("C")
                valor = totalValorFichas
                lblValor.Text = valor.ToString("R$ #,###.00")
                lblTotalFichas.Text = grdRemessa.Rows.Count - 1
            Else
                grdRemessa.DataSource = Nothing
                MsgBox("Não existe registros a serem processados")
            End If

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub carregaGridRequisicoesPorPeriodoComplementar(ByVal dtInicial As Date, ByVal dtFinal As Date, ByVal tipoAceite As Integer, ByVal naoConforme As Boolean, ByVal Pesquisa As Integer, ByVal TipoPesquisa As Integer, ByVal verificaGrau As Boolean)
        Try
            Dim objGenerico As CGenerico = New CGenerico()
            Dim valor As Single
            Dim strValor As String
            Dim valorCriticados As Integer = 0
            Dim theDataSet As New DataSet
            '0 =  categoria, 1 =  operador
            lstRequisicao = New SortableBindingList(Of CGenerico)(rnGenerico.CarregarRequisicaoPorPeriodoDataComplementar(dtInicial, dtFinal, tipoAceite, naoConforme, Pesquisa, TipoPesquisa, verificaGrau, "Consulta"))
            If (lstRequisicao.Count > 0) Then
                CriarColunasGridRequisicao()
                lblValorCriticado.Text = "CARREGANDO..."
                lblValorComissao.Text = "CARREGANDO..."
                CarregamentoAssincrono()
                bindingSourceRequisicao.DataSource = lstRequisicao
                grdRemessa.DataSource = bindingSourceRequisicao.DataSource
                For Each col As DataGridViewRow In grdRemessa.Rows
                    totalValorFichas = totalValorFichas + col.Cells(3).Value
                Next
                strValor = valor.ToString("C")
                valor = totalValorFichas
                lblValor.Text = valor.ToString("R$ #,###.00")
                lblTotalFichas.Text = grdRemessa.Rows.Count - 1
            Else
                lblValorComissao.Text = $"Valor Comissão: R$0.00"
                lblValorCriticado.Text = $"Valor Comissão: R$0.00"
                grdRemessa.DataSource = Nothing
                MsgBox("Não existe registros a serem processados")
            End If
        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub


    Private Sub btnRetorno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            FrmRetorno.Show()

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Close()
    End Sub

    Private Sub FrmConsultaContribuinte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            CriarColunasGrid()
            dtInicial.Text = Date.Now
            dtFinal.Text = Date.Now
            atualizou = True
            Dim primeiroDiaMesAnterior As DateTime = New DateTime(DateTime.Now.Year, DateTime.Now.Month - 1, 1)
            Dim ultimoDiaMesAnterior As DateTime = New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddDays(-1)
            gprBoxAnaliseComissao.Text = $"Valores do periodo de {primeiroDiaMesAnterior.ToString("dd/MM/yyyy")} à {ultimoDiaMesAnterior.ToString("dd/MM/yyyy")}"

            If Not IsNothing(Selecionado) Then

                If Selecionado > 0 Then

                    Dim rn_generico As RNGenerico = New RNGenerico

                    lstOperador = rn_generico.BuscarOperadorPeloNome(Operador)

                    OperadorId = lstOperador.Item(0).Cod_Operador

                    carregaFeedGridRequisicoes(OperadorId, CodUsuario, 1)


                End If
            End If

            cboAceite.Items.Add("Aceite")
            cboAceite.Items.Add("Não Aceite")
            cboAceite.Items.Add("Cancelado")
            cboAceite.Items.Add("Todos")

            cboAceite.SelectedIndex = 0

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CarregaComboCategorias(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboPesquisa.DataSource = Nothing
            cboPesquisa.Items.Clear()
            cboPesquisa.DisplayMember = "Nome_Categoria"
            cboPesquisa.ValueMember = "Cod_Categoria"
            cboPesquisa.DataSource = rn_generico.BuscarCategoria(0)

            If cboPesquisa.Items.Count > 0 And cod_categoria = 0 Then
                cboPesquisa.SelectedIndex = 0
            ElseIf cboPesquisa.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboPesquisa.Items.Count - 1
                    For Each linha As CGenerico In cboPesquisa.DataSource
                        If linha.Cod_Operador = cod_categoria Then
                            cboPesquisa.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                ' cboOperadorConcluir.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub


    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        ExportarParaExcel(grdRemessa, "teste")
    End Sub

    Public Sub ExportarParaExcel(ByVal dgvName As DataGridView, Optional ByVal fileName As String = "")
        ' Criar uma nova instância do Excel 
        Dim objExcelApp As New Excel.Application()
        Dim objExcelBook As Excel.Workbook
        Dim objExcelSheet As Excel.Worksheet
        Try

            ' Altera o tipo/localização para Inglês. Existe incompatibilidade  
            ' entre algumas versões de Excel vs Sistema Operativo 
            Dim oldCI As CultureInfo = CurrentThread.CurrentCulture
            CurrentThread.CurrentCulture = New CultureInfo("en-US")
            ' Adiciona um workbook e activa a worksheet actual 
            objExcelBook = objExcelApp.Workbooks.Add
            objExcelSheet = CType(objExcelBook.Worksheets(1), Excel.Worksheet)
            ' Ciclo nos cabeçalhos para escrever os títulos a bold/negrito 
            Dim dgvColumnIndex As Int16 = 1
            For Each col As DataGridViewColumn In dgvName.Columns
                objExcelSheet.Cells(1, dgvColumnIndex) = col.HeaderText
                objExcelSheet.Cells(1, dgvColumnIndex).Font.Bold = True
                dgvColumnIndex += 1
            Next
            ' Ciclo nas linhas/células 
            Dim dgvRowIndex As Integer = 2

            For Each row As DataGridViewRow In dgvName.Rows
                Dim dgvCellIndex As Integer = 1

                For Each cell As DataGridViewCell In row.Cells
                    objExcelSheet.Cells(dgvRowIndex, dgvCellIndex) = cell.Value
                    dgvCellIndex += 1
                Next
                dgvRowIndex += 1
            Next
            ' Ajusta o largura das colunas automaticamente 
            objExcelSheet.Columns.AutoFit()
            ' Caso a opção seja gravar (xlsSaveAs) grava o ficheiro e fecha 
            ' o Workbook/Excel. Caso contrário (xlsOpen) abre o Excel 

            objExcelBook.SaveAs("C:\AACI\teste.xlsx")
            objExcelBook.Close()
            objExcelApp.Quit()
            MessageBox.Show("Ficheiro exportado com sucesso para: " & fileName)


            ' Altera a tipo/localização para actual 
            CurrentThread.CurrentCulture = oldCI
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        Finally
            objExcelSheet = Nothing
            objExcelBook = Nothing
            objExcelApp = Nothing
            ' O GC(garbage collector) recolhe a memória não usada pelo sistema.  
            ' O método Collect() força a recolha e a opção WaitForPendingFinalizers  
            ' espera até estar completo. Desta forma o EXCEL.EXE não fica no  
            ' Task Manager(gestor tarefas) ocupando memória desnecessariamente 
            ' (devem ser chamados duas vezes para maior garantia) 
            GC.Collect()
            GC.WaitForPendingFinalizers()
            GC.Collect()
            GC.WaitForPendingFinalizers()
        End Try


    End Sub





    Private Sub FrmConsultaUltimaMovimentacao_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""
        Dim TipoPesquisa As Integer = 0

        Try
            If chkTipoPesquisa.Checked = True Then
                TipoPesquisa = 0
            Else
                TipoPesquisa = 1
            End If

            If atualizou = False Then
                If cboPesquisa.SelectedIndex = 0 Or cboPesquisa.Text = "Selecione..." Then
                    Exit Sub
                End If

                Me.Cursor = Cursors.WaitCursor

                CarregaComboCategorias(0)

                carregaFeedGridRequisicoes(cboPesquisa.SelectedValue, CodUsuario, TipoPesquisa)


                Me.Cursor = Cursors.Default
                atualizou = True
            End If

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub



    Private Sub txtNome_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNome.TextChanged
        Dim texto As String = Nothing
        Dim linhaVazia As Integer = 0
        Dim contador As Integer = 0

        If txtNome.Text <> String.Empty Then

            linhaVazia = grdRemessa.Rows.Count - 1

            'percorre cada linha do DataGridView
            For Each linha As DataGridViewRow In grdRemessa.Rows

                'percorre cada célula da linha
                For Each celula As DataGridViewCell In grdRemessa.Rows(linha.Index).Cells
                    If Not contador = linhaVazia Then
                        'se a coluna for a coluna 2 (Nome) então verifica o criterio
                        If celula.ColumnIndex = 2 Then

                            If celula.Value.ToString <> "" Then
                                texto = IIf(celula.Value.ToString = Nothing, "", celula.Value.ToString)
                                'se o texto informado estiver contido na célula então seleciona toda linha
                                If texto.Contains(txtNome.Text) Then
                                    'seleciona a linha
                                    grdRemessa.CurrentCell = celula
                                    grdRemessa.CurrentCell = grdRemessa.Rows(celula.RowIndex).Cells(2)
                                    Exit Sub
                                End If
                            End If
                            contador = contador + 1
                        End If
                    End If

                Next

            Next

        End If

    End Sub



    Private Sub txtTelefone_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtTelefone.TextChanged
        Dim texto As String = Nothing
        Dim linhaVazia As Integer = 0
        Dim contador As Integer = 0

        If grdRemessa.RowCount > 1 Then

            If txtTelefone.Text <> String.Empty Then

                linhaVazia = grdRemessa.Rows.Count - 1

                'percorre cada linha do DataGridView
                For Each linha As DataGridViewRow In grdRemessa.Rows

                    'percorre cada célula da linha
                    For Each celula As DataGridViewCell In grdRemessa.Rows(linha.Index).Cells
                        If Not contador = linhaVazia Then
                            'se a coluna for a coluna 1 (telefone) então verifica o criterio
                            If celula.ColumnIndex = 1 Then

                                If celula.Value.ToString <> "" Then
                                    texto = IIf(celula.Value.ToString = Nothing, "", celula.Value.ToString)
                                    'se o texto informado estiver contido na célula então seleciona toda linha
                                    If texto.Contains(txtTelefone.Text) Then
                                        'seleciona a linha
                                        grdRemessa.CurrentCell = celula
                                        grdRemessa.CurrentCell = grdRemessa.Rows(celula.RowIndex).Cells(2)
                                        Exit Sub
                                    End If
                                End If
                                contador = contador + 1
                            End If
                        End If

                    Next

                Next

            End If

        End If

    End Sub

    Private Sub chkTipoPesquisa_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkTipoPesquisa.CheckedChanged

        Try

            If chkTipoPesquisa.Checked = True Then
                lblTipoPesquisa.Text = "Operador"
                CarregaComboOperadorPesquisa(0)
            Else
                lblTipoPesquisa.Text = "Categoria"
                CarregaComboCategorias(0)
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CarregaComboOperador(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboOperadorConcluir.DataSource = Nothing
            cboOperadorConcluir.Items.Clear()
            cboOperadorConcluir.DisplayMember = "Nome_Operador"
            cboOperadorConcluir.ValueMember = "Cod_Operador"
            cboOperadorConcluir.DataSource = rn_generico.BuscarOperador(0)

            If cboOperadorConcluir.Items.Count > 0 And cod_operador = 0 Then
                cboOperadorConcluir.SelectedIndex = 0
            ElseIf cboOperadorConcluir.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperadorConcluir.Items.Count - 1
                    For Each linha As CGenerico In cboOperadorConcluir.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperadorConcluir.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                ' cboOperadorConcluir.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboOperadorPesquisa(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False
        Dim list As New List(Of CGenerico)
        Dim listCompara As New List(Of CGenerico)
        Dim listFinal As New List(Of CGenerico)
        Dim listFinalBKP As New List(Of CGenerico)
        Dim listExclusao As New List(Of CGenerico)


        Try

            list = rn_generico.BuscarOperador(0)
            listCompara = rn_generico.BuscarOperadorDoMes()

            cboPesquisa.DataSource = Nothing
            cboPesquisa.Items.Clear()
            cboPesquisa.DisplayMember = "Nome_Operador"
            cboPesquisa.ValueMember = "Cod_Operador"

            'COMPARA AS LISTAS (INICIO)

            For Each linha As CGenerico In list
                For Each linhaAux As CGenerico In listCompara
                    If linha.Nome_Operador.Trim = linhaAux.Nome_Operador.Trim Then
                        listFinal.Add(linha)
                        listExclusao.Add(linha)
                    End If
                Next
            Next

            'backup da lista final
            '  listFinalBKP = listFinal

            'For Each t In listExclusao
            '    If (list.Contains(t)) Then
            '        list.Remove(t)
            '    End If
            'Next


            'unifica as listas
            'For Each t In list
            '    listFinal.Add(t)
            'Next


            'COMPARA AS LISTAS (FINAL)
            If listFinal.Count = 1 Then
                cboPesquisa.DataSource = list
            Else
                cboPesquisa.DataSource = listFinal
            End If


            If cboPesquisa.Items.Count > 0 And cod_operador = 0 Then

            ElseIf cboPesquisa.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboPesquisa.Items.Count - 1
                    For Each linha As CGenerico In cboPesquisa.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboPesquisa.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                ' cboOperadorConcluir.SelectedIndex = 0
            End If

            'COLORIR OS NOMES DAS OPERADORAS ATIVAS


            'For Each linha As CGenerico In cboPesquisa.DataSource
            '        For Each linhaAux As CGenerico In listFinalBKP
            '        If linha.Nome_Operador = linhaAux.Nome_Operador And linha.Nome_Operador <> "Selecione..." Then
            '            cboPesquisa.ForeColor = Color.Blue
            '        End If
            '    Next
            '    Next



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub btnLimpar_Click(sender As System.Object, e As System.EventArgs) Handles btnLimpar.Click
        Try

            grdRemessa.DataSource = Nothing

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub


    Private Sub grdRemessa_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdRemessa.CellContentClick

        Try
            Dim valorEspecial As Decimal = 0
            Dim rn As RNGenerico = New RNGenerico
            Dim rnContribuinte As RNContribuinte = New RNContribuinte
            Dim RequisicaoSelecionada As CGenerico = New CGenerico
            ClienteClicado = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.Cod_EAN_CLIENTE).Value, String)
            DDDSelecionado = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.DDD).Value, String)
            TelefoneSelecionado = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.Telefone).Value, String)
            RequisicaoIdSelecionado = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.RequisicaoId).Value, String)
            If (e.RowIndex > -1) Then
                CarregamentoHistoricoAssincrono()
                contribuinte = rnContribuinte.CarregarContribuintePorTelefone(TelefoneSelecionado, DDDSelecionado, ClienteClicado, "ConsultaUltimaMovimentacao")
                If (contribuinte.Cod_EAN_Cliente = Nothing Or contribuinte.Cod_EAN_Cliente = "") Then
                    contribuinte = rnContribuinte.PesquisarTabelaRetidoPorTelefone(TelefoneSelecionado, DDDSelecionado, ClienteClicado)
                End If
                If Not contribuinte Is Nothing And Not contribuinte.Telefone1 Is Nothing Then
                    'popula a tela
                    txtCodigo.Text = contribuinte.CodCliente
                    txtNomeCliente1.Text = contribuinte.NomeCliente1
                    txtNomeCliente2.Text = contribuinte.NomeCliente2
                    txtCodBarra.Text = contribuinte.Cod_EAN_Cliente
                    txtCnpjCpf.Text = contribuinte.CNPJ_CPF
                    txtRg.Text = contribuinte.IE_CI
                    txtEmail.Text = contribuinte.Email
                    txtTelefone1.Text = contribuinte.Telefone1
                    txtTelefone2.Text = contribuinte.Telefone2
                    txtEndereco.Text = contribuinte.Endereco
                    txtUF.Text = contribuinte.UF
                    txtCEP.Text = contribuinte.CEP

                    txtRegiao.Text = contribuinte.Cod_Regiao

                    txtDtNc.Text = contribuinte.DataNascimento
                    txtDtNcMp.Text = contribuinte.DT_NC_MP
                    txtDtCadastro.Text = contribuinte.DataCadastro
                    txtReferencia.Text = contribuinte.Referencia

                    txtObs.Text = contribuinte.Observacao

                    txtMelhorDia.Text = contribuinte.DiaLimite
                    txtValDoacao.Text = Convert.ToDouble(contribuinte.Valor).ToString("R$ #,###.00")
                    txtDDD.Text = contribuinte.DDD

                    CarregaComboCidades(contribuinte.Cod_Cidade)
                    CarregaComboCategoriasDetalhes(contribuinte.Cod_Categoria)
                    CarregaComboOperador(CodUsuario)
                    CarregaComboOperadorDetalhe(contribuinte.Cod_Operador)
                    CarregaComboBairros(contribuinte.Cod_Bairro)
                    CarregaComboEspecie(contribuinte.Cod_Especie)



                    If contribuinte.Descontinuado = "S" Then
                        chkInativo.Checked = True
                    Else
                        chkInativo.Checked = False
                    End If

                    If contribuinte.Nao_pedir_extra = "S" Then
                        chkNaoPedirExtra.Checked = True
                    Else
                        chkNaoPedirExtra.Checked = False
                    End If

                    If contribuinte.Nao_Pedir_Mensal = "S" Then
                        chkNaoPedirMensal.Checked = True
                    Else
                        chkNaoPedirMensal.Checked = False
                    End If

                    If contribuinte.Tel_desligado = "S" Then
                        chkTelDesl.Checked = True
                    Else
                        chkTelDesl.Checked = False
                    End If


                    'TELA DE RESUMO

                    lstRequisicaoSelecionada = New SortableBindingList(Of CGenerico)(rn.RetornaRequisicaoSelecionada(RequisicaoIdSelecionado))

                    If (lstRequisicaoSelecionada.Count > 0) Then

                        For Each RequisicaoSelecionada In lstRequisicaoSelecionada

                            txtCodEanConcluir.Text = RequisicaoSelecionada.Cod_cliente
                            txtNomeConcluir.Text = RequisicaoSelecionada.Nome_cliente
                            If txtNomeConcluir.Text = "" Then
                                txtNomeConcluir.Text = contribuinte.NomeCliente1
                            End If
                            txtDDDConcluir.Text = RequisicaoSelecionada.DDD
                            txtTelefoneConcluir.Text = RequisicaoSelecionada.Telefone
                            CarregaComboCategoriasDetalhes(contribuinte.Cod_Categoria)
                            CarregaComboCategoriasConcluir(RequisicaoSelecionada.Cod_Categoria)
                            CarregaComboOperadorDetalhe(RequisicaoSelecionada.Operador_id)
                            CarregaComboOperador(RequisicaoSelecionada.Operador_id)
                            txtValorConcluir.Text = Convert.ToDouble(RequisicaoSelecionada.Valor).ToString("#,###.00")
                            txtObs.Text = RequisicaoSelecionada.Observacao
                            txtCpf.Text = RequisicaoSelecionada.Cpf
                            txtAutorizante.Text = RequisicaoSelecionada.Autorizante

                            If RequisicaoSelecionada.Situacao = "Aceite" Then
                                cbSituacao.SelectedIndex = 0
                            ElseIf RequisicaoSelecionada.Situacao = "Recusa" Then
                                cbSituacao.SelectedIndex = 1
                            ElseIf RequisicaoSelecionada.Situacao = "Cancelado" Then
                                cbSituacao.SelectedIndex = 2
                            End If

                        Next


                    Else

                        GrdHistorico.DataSource = Nothing

                    End If


                End If


                lstRequisicaoSelecionada = New SortableBindingList(Of CGenerico)(rn.RetornaRequisicaoSelecionada(RequisicaoIdSelecionado))

                If (lstRequisicaoSelecionada.Count > 0) Then

                    For Each RequisicaoSelecionada In lstRequisicaoSelecionada

                        txtCodEanConcluir.Text = RequisicaoSelecionada.Cod_cliente
                        txtNomeConcluir.Text = RequisicaoSelecionada.Nome_cliente
                        If txtNomeConcluir.Text = "" Then
                            txtNomeConcluir.Text = contribuinte.NomeCliente1
                        End If
                        txtDDDConcluir.Text = RequisicaoSelecionada.DDD
                        txtTelefoneConcluir.Text = RequisicaoSelecionada.Telefone
                        CarregaComboCategoriasDetalhes(contribuinte.Cod_Categoria)
                        CarregaComboCategoriasConcluir(RequisicaoSelecionada.Cod_Categoria)
                        CarregaComboOperadorDetalhe(RequisicaoSelecionada.Operador_id)
                        CarregaComboOperador(RequisicaoSelecionada.Operador_id)
                        txtValorConcluir.Text = Convert.ToDouble(RequisicaoSelecionada.Valor).ToString("#,###.00")
                        txtObs.Text = RequisicaoSelecionada.Observacao
                        txtCpf.Text = RequisicaoSelecionada.Cpf
                        txtAutorizante.Text = RequisicaoSelecionada.Autorizante
                        If RequisicaoSelecionada.Situacao = "Aceite" Then
                            cbSituacao.SelectedIndex = 0
                        ElseIf RequisicaoSelecionada.Situacao = "Recusa" Then
                            cbSituacao.SelectedIndex = 1
                        ElseIf RequisicaoSelecionada.Situacao = "Cancelado" Then
                            cbSituacao.SelectedIndex = 2
                        End If
                    Next
                Else
                    GrdHistorico.DataSource = Nothing
                End If
            End If
            cboCategoriaConcluir.Enabled = True
            CarregaTelefoneNuncaCriticadoAssincrono()
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub CarregaHistoricoCriticaContribuinte()
        Try
            Dim rnGen As New RNGenerico
            Dim theDataTable As New DataTable
            Dim valorTotal As Single
            Dim strValorTotal As String
            Dim dataInicial As Date = dtInicial.Text
            Dim dataFinal As Date = dtFinal.Text
            Dim operadorId As Integer = PesquisaSelecionado
            Dim valorCriticado As Integer
            Dim totalValorFichasComissao As Integer = rnGen.RetornaRequisicaoComCriticasComissao(dataInicial, dataFinal, operadorId)
            strValorTotal = valorTotal.ToString("C")
            valorTotal = totalValorFichasComissao
            valorCriticado = valorTotal
            Me.Invoke(Sub()
                          lblValorCriticado.Text = $"Valor Criticado: {valorCriticado.ToString("R$ #,###.00")}"
                          Dim valorComissao As Integer = totalValorFichas - valorCriticado
                          lblValorComissao.Text = $"Valor Comissão: {valorComissao.ToString("R$ #,###.00")}"
                      End Sub)
        Catch ex As Exception
            'MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub CarregaHistorico()
        Try
            Dim rn As New RNGenerico
            lstListaTelefonica = New SortableBindingList(Of CListaTelefonica)(rn.Retorna_Historico(TelefoneSelecionado, DDDSelecionado, ClienteClicado))
            If (lstListaTelefonica.Count > 0) Then
                bindingSourceContribuite.DataSource = lstListaTelefonica
            End If
            Me.Invoke(Sub()
                          If lstListaTelefonica.Count > 0 Then
                              GrdHistorico.DataSource = bindingSourceContribuite.DataSource
                          Else
                              GrdHistorico.DataSource = Nothing
                          End If
                      End Sub)
        Catch ex As Exception
            ' MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub CarregaTelefoneNuncaCriticado()
        Try
            Dim isSemCritica As Boolean = rnGenerico.VerificaTelefoneNuncaCriticado(TelefoneSelecionado)
            Me.Invoke(Sub()
                          If isSemCritica Then
                              lblSemCritica.Visible = True
                          Else
                              lblSemCritica.Visible = False
                          End If
                      End Sub)
        Catch ex As Exception
            'MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub CarregaComboCidades(ByVal cod_cidade As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCidade.DataSource = Nothing
            cboCidade.Items.Clear()
            cboCidade.DisplayMember = "Nome_cidade"
            cboCidade.ValueMember = "Cod_Cidade"
            cboCidade.DataSource = rn_generico.BuscarCidades(0)

            If cboCidade.Items.Count = 0 Then
                cboCidade.SelectedIndex = 0
            ElseIf cboCidade.Items.Count > 0 And cod_cidade <> 0 Then

                For i As Integer = 0 To cboCidade.Items.Count - 1
                    For Each linha As CGenerico In cboCidade.DataSource
                        If linha.Cod_Cidade = cod_cidade Then
                            cboCidade.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next

            Else

                cboCidade.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CarregaComboCategoriasDetalhes(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCategoriaDetalhe.DataSource = Nothing
            cboCategoriaDetalhe.Items.Clear()
            cboCategoriaDetalhe.DisplayMember = "Nome_Categoria"
            cboCategoriaDetalhe.ValueMember = "Cod_Categoria"
            cboCategoriaDetalhe.DataSource = rn_generico.BuscarCategoria(0)

            If cboCategoriaDetalhe.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoriaDetalhe.SelectedIndex = 0
            ElseIf cboCategoriaDetalhe.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoriaDetalhe.Items.Count - 1
                    For Each linha As CGenerico In cboCategoriaDetalhe.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoriaDetalhe.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoriaDetalhe.SelectedIndex = 1
            End If



        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboOperadorDetalhe(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboOperadorDetalhe.DataSource = Nothing
            cboOperadorDetalhe.Items.Clear()
            cboOperadorDetalhe.DisplayMember = "Nome_Operador"
            cboOperadorDetalhe.ValueMember = "Cod_Operador"
            cboOperadorDetalhe.DataSource = rn_generico.BuscarOperador(0)

            If cboOperadorDetalhe.Items.Count > 0 And cod_operador = 0 Then
                cboOperadorDetalhe.SelectedIndex = 0
            ElseIf cboOperadorDetalhe.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperadorDetalhe.Items.Count - 1
                    For Each linha As CGenerico In cboOperadorDetalhe.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperadorDetalhe.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperadorDetalhe.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboEspecie(ByVal cod_especie As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try


            cboEspecieDetalhe.DataSource = Nothing
            cboEspecieDetalhe.Items.Clear()
            cboEspecieDetalhe.DisplayMember = "Nome_Especie"
            cboEspecieDetalhe.ValueMember = "Cod_Especie"
            cboEspecieDetalhe.DataSource = rn_generico.BuscarEspecie(0)

            If cboEspecieDetalhe.Items.Count > 0 And cod_especie = 0 Then
                cboEspecieDetalhe.SelectedIndex = 0
            ElseIf cboEspecieDetalhe.Items.Count > 0 And cod_especie <> 0 Then
                For i As Integer = 0 To cboEspecieDetalhe.Items.Count - 1
                    For Each linha As CGenerico In cboEspecieDetalhe.DataSource
                        If linha.Cod_Especie = cod_especie Then
                            cboEspecieDetalhe.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboEspecieDetalhe.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try



    End Sub

    Private Sub CarregaComboBairros(ByVal cod_bairro As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try



            cboBairroDetalhe.DataSource = Nothing
            cboBairroDetalhe.Items.Clear()
            cboBairroDetalhe.DisplayMember = "Nome_Bairro"
            cboBairroDetalhe.ValueMember = "Cod_Bairro"
            cboBairroDetalhe.DataSource = rn_generico.BuscarBairros(0)

            If cboBairroDetalhe.Items.Count > 0 And cod_bairro = 0 Then
                cboBairroDetalhe.SelectedIndex = 0

            ElseIf cboBairroDetalhe.Items.Count > 0 And cod_bairro <> 0 Then
                For i As Integer = 0 To cboBairroDetalhe.Items.Count - 1
                    For Each linha As CGenerico In cboBairroDetalhe.DataSource
                        If linha.Cod_Bairro = cod_bairro Then
                            cboBairroDetalhe.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboBairroDetalhe.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboCategoriasConcluir(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try
            cboCategoriaConcluir.DataSource = Nothing
            cboCategoriaConcluir.Items.Clear()
            cboCategoriaConcluir.DisplayMember = "Nome_Categoria"
            cboCategoriaConcluir.ValueMember = "Cod_Categoria"
            cboCategoriaConcluir.DataSource = rn_generico.BuscarCategoria(0)

            If cboCategoriaConcluir.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoriaConcluir.SelectedIndex = 0
            ElseIf cboCategoriaConcluir.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoriaConcluir.Items.Count - 1
                    For Each linha As CGenerico In cboCategoriaConcluir.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoriaConcluir.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoriaConcluir.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub btnContribuir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcluirContribuir.Click
        Dim Daogenerico As GenericoDAO = New GenericoDAO
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable
        Try

            Dim result = MessageBox.Show("Deseja cancelar a requisição do número " & txtTelefoneConcluir.Text & " para o próximo envio?, Deseja continuar?", "Atenção",
                           MessageBoxButtons.YesNo,
                           MessageBoxIcon.Question)

            Me.Cursor = Cursors.WaitCursor


            If (result = DialogResult.Yes) Then

                If rnGenerico.ExcluirRequisicaoselecionada(RequisicaoIdSelecionado) Then
                    MsgBox("Registro excluido com sucesso!", MsgBoxStyle.Information, "Atenção!!")
                End If

            End If

            Me.Cursor = Cursors.Default


        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub btnEnviarCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnviarCancelar.Click
        Dim Daogenerico As GenericoDAO = New GenericoDAO
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable
        Try

            Dim result = MessageBox.Show("Deseja enviar o número " & txtTelefoneConcluir.Text & " para cancelamento permanente?, Deseja continuar?", "Atenção",
                           MessageBoxButtons.YesNo,
                           MessageBoxIcon.Question)

            Me.Cursor = Cursors.WaitCursor


            If (result = DialogResult.Yes) Then
                ds = Daogenerico.BuscarOperador(CodUsuario)

                If (ds.Tables(0).Rows.Count > 0) Then
                    If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                        dt = ds.Tables(0)
                    End If
                End If

                contribuinte.Cod_Usuario = CodUsuario
                contribuinte.Cod_opera = dt.Rows(0).Item(0)
                contribuinte.Operador = dt.Rows(0).Item(1)


                'PEGA OS VALORES ATUALIZADOS NA TELA
                contribuinte.Telefone1 = txtTelefoneConcluir.Text
                contribuinte.Nome_contribuinte = txtNomeConcluir.Text
                contribuinte.NomeCliente1 = txtNomeConcluir.Text
                contribuinte.DDD = txtDDD.Text
                contribuinte.Telefone1 = txtTelefoneConcluir.Text
                contribuinte.Valor = txtValorConcluir.Text
                contribuinte.Observacao = txtObs.Text



                rnContribuinte.EnviaContribuinteParaCancelamento(contribuinte, CodUsuario)

            End If

            Me.Cursor = Cursors.Default


        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub btnCancelarMensal_Click(sender As System.Object, e As System.EventArgs) Handles btnCancelarMensal.Click
        Dim Daogenerico As GenericoDAO = New GenericoDAO
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable
        Try

            Dim result = MessageBox.Show("Deseja cancelar a contribuição mensal do telefone" & txtTelefoneConcluir.Text & ", tornando-o um contribuinte avulso?, Deseja continuar?", "Atenção",
                           MessageBoxButtons.YesNo,
                           MessageBoxIcon.Question)

            Me.Cursor = Cursors.WaitCursor


            If (result = DialogResult.Yes) Then
                ds = Daogenerico.BuscarOperador(CodUsuario)

                If (ds.Tables(0).Rows.Count > 0) Then
                    If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                        dt = ds.Tables(0)
                    End If
                End If

                contribuinte.Cod_Usuario = CodUsuario
                contribuinte.Cod_opera = dt.Rows(0).Item(0)
                contribuinte.Operador = dt.Rows(0).Item(1)


                'PEGA OS VALORES ATUALIZADOS NA TELA
                contribuinte.Telefone1 = txtTelefoneConcluir.Text
                contribuinte.Nome_contribuinte = txtNomeConcluir.Text
                contribuinte.NomeCliente1 = txtNomeConcluir.Text
                contribuinte.DDD = txtDDD.Text
                contribuinte.Telefone1 = txtTelefoneConcluir.Text
                contribuinte.Valor = txtValorConcluir.Text
                contribuinte.Observacao = txtObs.Text



                rnContribuinte.EnviaContribuinteParaCancelamentoMensal(contribuinte)

            End If

            Me.Cursor = Cursors.Default


        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub btnAlterarContribuicao_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAlterarContribuicao.Click
        Dim contribuinte As New CContribuinte
        Dim rnContribuinte As New RNContribuinte
        Dim rnRequisicao As New RNRequisicao
        Dim analiseAceite As String()
        Dim mesSeguinte As String = ""
        Dim mesAtual As String = ""
        Dim telefoneErrado As String = ""
        Dim situacao As String = ""

        Try
            If cboOperadorConcluir.SelectedIndex = 0 Then
                MsgBox("Por favor selecione um operador")
                Exit Sub
            End If

            If cboCategoriaConcluir.SelectedIndex = 0 Then
                MsgBox("Por favor selecione uma categoria")
                Exit Sub
            End If

            If txtCodEanConcluir.Text = "*     -   *" Then
                MsgBox("Por favor digite o código do contribuinte")
                Exit Sub
            End If

            If txtDDDConcluir.Text = "" Then
                MsgBox("Por favor digite o DDD do contribuinte")
                Exit Sub
            End If


            If txtTelefoneConcluir.Text = "" Then
                MsgBox("Por favor digite o telefone do contribuinte")
                Exit Sub
            End If

            If txtValorConcluir.Text = "" Then
                MsgBox("Por favor digite o valor da contribuição")
                Exit Sub
            End If


            Dim valorConcluir As Decimal = 0
            valorConcluir = CDec(txtValorConcluir.Text)

            If valorConcluir > 100 Then
                Dim result = MessageBox.Show("O valor informado é superior a 100 reais, confirma o valor informado?", "Atenção",
                           MessageBoxButtons.YesNo,
                           MessageBoxIcon.Question)

                If (result = DialogResult.No) Then
                    Exit Sub
                End If
            End If

            Me.Cursor = Cursors.WaitCursor


            contribuinte.NomeCliente1 = txtNomeConcluir.Text
            contribuinte.Telefone1 = txtTelefoneConcluir.Text
            contribuinte.DDD = txtDDDConcluir.Text
            contribuinte.Valor = txtValorConcluir.Text
            contribuinte.Cod_EAN_Cliente = txtCodEanConcluir.Text
            contribuinte.Cod_Operador = cboOperadorConcluir.SelectedValue
            contribuinte.Operador = cboOperadorConcluir.Text
            contribuinte.Cod_Categoria = cboCategoriaConcluir.SelectedValue


            If cbSituacao.SelectedIndex = 0 Then
                situacao = "S"
            ElseIf cbSituacao.SelectedIndex = 1 Then
                situacao = "N"
            ElseIf cbSituacao.SelectedIndex = 2 Then
                situacao = "C"
            End If


            mesSeguinte = DateAdd(DateInterval.Month, 1, CDate(Date.Now))
            mesSeguinte = mesSeguinte.Substring(3)

            mesAtual = CDate(Date.Now)
            mesAtual = mesAtual.Substring(3)

            analiseAceite = rnContribuinte.Analisar_Aceite_Oi(contribuinte, valorConcluir)

            If analiseAceite(1) > 30 Then

                Dim result = MessageBox.Show("A media de aceite para o telefone é de R$" & analiseAceite(0) & " O atual aceite esta com o valor de R$" & valorConcluir & ", a diferença é de " & analiseAceite(1) & "%. Confirma o aceite?", "Atenção",
                                             MessageBoxButtons.YesNo,
                                             MessageBoxIcon.Question)


                If (result = DialogResult.Yes) Then
                    If rnContribuinte.Atualiza_contribuicao_Oi(contribuinte, situacao, mesAtual.Substring(0, 7), mesSeguinte.Substring(0, 7), txtNomeConcluir.Text, RequisicaoIdSelecionado) = True Then
                        rnContribuinte.Atualiza_Informacoes_Oi(contribuinte, mesAtual.Substring(0, 7), mesSeguinte.Substring(0, 7), txtCpf.Text, txtAutorizante.Text, rtxtObs.Text, contribuinte.Valor)
                        If (telefoneErrado <> "") Then
                            rnRequisicao.AtualizaDadosListaTelefonica(contribuinte)
                            rnRequisicao.ApagaRegistroErradosRequisicao(telefoneErrado)
                        End If
                        MsgBox("Registro salvo com sucesso!", MsgBoxStyle.Information, "Atenção")

                    Else
                        MsgBox("Ocorreu um erro na alteracao do registro", MsgBoxStyle.Information, "Atenção")
                    End If
                Else
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If

            Else

                If rnContribuinte.Atualiza_contribuicao_Oi(contribuinte, situacao, mesAtual.Substring(0, 7), mesSeguinte.Substring(0, 7), txtNomeConcluir.Text, RequisicaoIdSelecionado) = True Then
                    rnContribuinte.Atualiza_Informacoes_Oi(contribuinte, mesAtual.Substring(0, 7), mesSeguinte.Substring(0, 7), txtCpf.Text, txtAutorizante.Text, rtxtObs.Text, contribuinte.Valor)
                    If (telefoneErrado <> "") Then
                        rnRequisicao.AtualizaDadosListaTelefonica(contribuinte)
                        rnRequisicao.ApagaRegistroErradosRequisicao(telefoneErrado)
                    End If
                    MsgBox("Registro salvo com sucesso!", MsgBoxStyle.Information, "Atenção")

                Else
                    MsgBox("Ocorreu um erro na inclusão do registro", MsgBoxStyle.Information, "Atenção")
                End If

            End If

            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try

    End Sub


    Private Sub chkVerificaGrau_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkVerificaGrau.CheckedChanged

        If chkVerificaGrau.Checked = True Then
            chkNaoConforme.Enabled = True
        Else
            chkNaoConforme.Enabled = False
        End If

    End Sub

    Private Sub LimpaTela()

        Try

            txtCodBarra.Text = ""
            txtCodigo.Text = ""
            txtNomeCliente1.Text = ""
            txtNomeCliente2.Text = ""
            txtDDD.Text = ""
            txtTelefone1.Text = ""
            txtTelefone2.Text = ""
            txtCnpjCpf.Text = ""
            txtRg.Text = ""
            txtMelhorDia.Text = ""
            txtValorConcluir.Text = ""
            txtValDoacao.Text = ""
            txtDtNc.Text = ""
            txtDtNcMp.Text = ""
            txtReferencia.Text = ""
            txtRegiao.Text = ""
            txtObs.Text = ""
            rtxtObs.Text = ""
            txtCEP.Text = ""
            txtUF.Text = ""
            txtEndereco.Text = ""
            txtNome.Text = ""
            txtTelefone1.Text = ""
            txtEmail.Text = ""
            txtCodEanConcluir.Text = ""
            txtNomeConcluir.Text = ""
            txtDDDConcluir.Text = ""
            txtTelefoneConcluir.Text = ""
            txtValorConcluir.Text = ""
            txtCpf.Text = ""
            txtAutorizante.Text = ""
            rtxtObs.Text = ""
            txtDtCadastro.Text = ""



            txtCodEanConcluir.Text = ""
            txtNomeConcluir.Text = ""
            txtDDDConcluir.Text = ""
            txtTelefoneConcluir.Text = ""
            cboCategoriaDetalhe.DataSource = Nothing
            cboCategoriaConcluir.DataSource = Nothing
            cboOperadorConcluir.DataSource = Nothing
            cboOperadorDetalhe.DataSource = Nothing
            txtValorConcluir.Text = ""
            txtObs.Text = ""
            txtCpf.Text = ""
            txtAutorizante.Text = ""
            cbSituacao.SelectedIndex = -1

            GrdHistorico.DataSource = Nothing


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub CarregamentoAssincrono()
        Try
            If cboPesquisa.SelectedIndex > 0 Then
                Dim t As New Thread(AddressOf CarregaHistoricoCriticaContribuinte)
                t.IsBackground = True
                t.Start()
            End If
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub CarregamentoHistoricoAssincrono()
        Try
            Dim t As New Thread(AddressOf CarregaHistorico)
            t.IsBackground = True
            t.Start()
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub CarregaTelefoneNuncaCriticadoAssincrono()
        Try
            Dim t As New Thread(AddressOf CarregaTelefoneNuncaCriticado)
            t.IsBackground = True
            t.Start()
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub GrdHistorico_Paint(sender As Object, e As PaintEventArgs) Handles GrdHistorico.Paint
        If GrdHistorico.Rows.Count = 0 Then
            TextRenderer.DrawText(e.Graphics, "Carregando...",
                GrdHistorico.Font, GrdHistorico.ClientRectangle,
                GrdHistorico.ForeColor, GrdHistorico.BackgroundColor,
                TextFormatFlags.HorizontalCenter Or TextFormatFlags.VerticalCenter)
        End If
    End Sub
End Class