﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text
Imports System.Net
'Imports FTP_Upload_Download
Imports Microsoft.Office.Interop
Imports Microsoft.Office.Interop.Excel
Imports System.Net.Mail
Imports System.Windows.Forms.Screen
Imports System.Drawing.Graphics


Public Class FrmAuditoriaOi

#Region "Enum"

    Private Enum ColunasGridRemessa

        SelecaoContribuinte = 0
        COD_CLIENTE = 1
        Cod_EAN_CLIENTE = 2
        Nome = 3
        DDD = 4
        Telefone = 5
        Valor = 6
        bairro = 7
        cidade = 8
        UF = 9
        Data_Cadastro = 10
        Categoria = 11
        Operador = 12
        dia_limite = 13

    End Enum

    Enum xlsOption
        xlsOpen
    End Enum

#End Region

#Region "variaveis"

    Private lstContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstContribuinteSelecionada As New List(Of CContribuinte)
    Private v_ambiente As String
    Private rnContribuinte As RNContribuinte = New RNContribuinte
    Private contribuinte As CContribuinte = New CContribuinte
    Public NomeArquivo As String
    Dim Condicao1 As List(Of String) = New List(Of String)
    Dim Condicao2 As List(Of String) = New List(Of String)

#End Region



#Region "Metodos"

    Private Sub FrmGerarRemessa_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim dataInicio As Date
        Dim dataFim As Date
        Dim data As Date

        'PEGO O NUMERO DO DIA ATUAL
        Dim diaAtual As Integer = Date.Today.DayOfWeek

        'SUBTRAIO O DIA ATUAL E ACRESCENTO UM DIA PARA ENCONTRAR A SEGUNDA FEIRA
        data = Date.Now.AddDays(-diaAtual + 1)

        'SUBTRAIO -7 DIAS PARA ENCONTRAR A SEGUNDA DA SEMANA PASSADA
        data = data.AddDays(-7)

        dataInicio = data
        dataFim = dataInicio.AddDays(5)

        dtInicio.Value = dataInicio
        dtFim.Value = dataFim

        carregaOperadorSemana()

        '  carregaOperadorasTab1()

        CarregaComboOperadorTab1(0)




    End Sub



    Private Sub carregaOperadorasTab1()

        Dim objGenerico As CGenerico = New CGenerico()
        Dim rnGenerico As RNGenerico = New RNGenerico()
        Dim lstGenerico As List(Of CGenerico) = New List(Of CGenerico)
        Dim bindingSource As BindingSource = New BindingSource

        Try
            'limpa o listview
            ListViewTab1.Columns.Clear()

            'define se irá aparecer um checkbox para listar
            ListViewTab1.CheckBoxes = True

            'mostrar as linhas do grid
            ListViewTab1.GridLines = True

            'define para selecionar a linha inteira
            ListViewTab1.FullRowSelect = True

            'define o tipo de listview
            ListViewTab1.View = View.List


            lstGenerico = rnGenerico.BuscarUsuario(0)

            If (lstGenerico.Count > 0) Then

                bindingSource.DataSource = lstGenerico

                For Each linha As CGenerico In bindingSource.DataSource

                    ListViewTab1.Items.Add(linha.Usuario)

                Next

            End If


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub carregaOperadorSemana()

        Dim objGenerico As CGenerico = New CGenerico()
        Dim rnGenerico As RNGenerico = New RNGenerico()
        Dim lstGenerico As List(Of String) = New List(Of String)
        Dim bindingSource As BindingSource = New BindingSource

        Try
            'limpa o listview
            ListViewTab1.Columns.Clear()

            'define se irá aparecer um checkbox para listar
            ListViewTab1.CheckBoxes = True

            'mostrar as linhas do grid
            ListViewTab1.GridLines = True

            'define para selecionar a linha inteira
            ListViewTab1.FullRowSelect = True

            'define o tipo de listview
            ListViewTab1.View = View.List


            lstGenerico = rnGenerico.BuscarOperadorSemana(dtInicio.Value, dtFim.Value)

            If (lstGenerico.Count > 0) Then

                'bindingSource.DataSource = lstGenerico

                For Each linha As String In lstGenerico

                    ListViewTab1.Items.Add(linha)

                Next

            End If


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub carregaOperador()

        Dim objGenerico As CGenerico = New CGenerico()
        Dim rnGenerico As RNGenerico = New RNGenerico()
        Dim lstGenerico As List(Of String) = New List(Of String)
        Dim bindingSource As BindingSource = New BindingSource

        Try
            'limpa o listview
            ListViewTab1.Columns.Clear()

            'define se irá aparecer um checkbox para listar
            ListViewTab1.CheckBoxes = True

            'mostrar as linhas do grid
            ListViewTab1.GridLines = True

            'define para selecionar a linha inteira
            ListViewTab1.FullRowSelect = True

            'define o tipo de listview
            ListViewTab1.View = View.List


            lstGenerico = rnGenerico.BuscarOperador()

            If (lstGenerico.Count > 0) Then

                'bindingSource.DataSource = lstGenerico

                For Each linha As String In lstGenerico

                    ListViewTab1.Items.Add(linha)

                Next

            End If


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboOperadorTab1(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboOperadorTab1Cond1.DataSource = Nothing
            cboOperadorTab1Cond1.Items.Clear()
            cboOperadorTab1Cond1.DisplayMember = "Usuario"
            cboOperadorTab1Cond1.ValueMember = "Cod_Funcionario"
            cboOperadorTab1Cond1.DataSource = rn_generico.BuscarUsuario(0)

            If cboOperadorTab1Cond1.Items.Count > 0 And cod_operador = 0 Then
                cboOperadorTab1Cond1.SelectedIndex = 0
            ElseIf cboOperadorTab1Cond1.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperadorTab1Cond1.Items.Count - 1
                    For Each linha As CGenerico In cboOperadorTab1Cond1.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperadorTab1Cond1.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                ' cboOperadorConcluir.SelectedIndex = 0
            End If




        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

#End Region

    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Dim rnGenerico As New RNGenerico
        Dim ds As DataSet = New DataSet
        Dim dt As System.Data.DataTable = New System.Data.DataTable
        Dim operadoras As String = ""


        Try
            Me.Cursor = Cursors.WaitCursor


            For Each item As ListViewItem In ListViewTab1.Items
                Dim i As Integer = 0
                If item.Checked = True Then
                    operadoras = operadoras + "'" & item.Text & "',"
                End If
                i = i + 1
            Next

            operadoras = operadoras.Remove(operadoras.Length - 1)


            ds = rnGenerico.CriaPlanilhaAuditoriaOi(dtInicio.Value, dtFim.Value, operadoras, Condicao1, Condicao2, chkPlanilhaInterna.Checked)

            dt = ds.Tables(0)

                grdRemessa.DataSource = dt

                lblNumRegistros.Text = dt.Rows.Count

                Me.Cursor = Cursors.Default

                If grdRemessa.RowCount > 0 Then
                    TabControl.SelectedIndex = 1
                End If


        Catch ex As Exception

            ' Show the exception's message.
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try

    End Sub

    Private Sub carregaGridClienteOI()

        Dim objContribuintes As CContribuinte = New CContribuinte()

        Dim bindingSourceApolice As BindingSource = New BindingSource

        Try

            lstContribuintes = New SortableBindingList(Of CContribuinte)(rnContribuinte.CarregarGridOi(objContribuintes, "Remessa"))

            If (lstContribuintes.Count > 0) Then

                bindingSourceApolice.DataSource = lstContribuintes

                grdRemessa.DataSource = bindingSourceApolice.DataSource
                'grdRemessa.Sort(grdRemessa.Columns(ColunasGridRemessa.Nome), ListSortDirection.Ascending)
                'lblNumRegistros.Text = lstApolices.Count


            End If

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub



    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Close()
    End Sub

    Private Sub chkCondicao1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTab1Condicao1.CheckedChanged

        If chkTab1Condicao1.Checked = True Then
            txtTab1Hora1.Enabled = True
            dtTab1Condicao1.Enabled = True
        Else
            cboOperadorTab1Cond1.Enabled = False
            txtTab1Hora1.Enabled = False
            dtTab1Condicao1.Enabled = False
        End If

    End Sub

    Private Sub dtFim_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtFim.Leave

        Dim dataInicio As Date
        Dim dataFim As Date
        Dim qtsDias As Integer

        Try
            dataInicio = CDate(dtInicio.Text)
            dataFim = CDate(dtFim.Text)

            qtsDias = DateDiff(DateInterval.Day, dataInicio, dataFim)

            If qtsDias = 5 Then

                If Not TabCondicao.TabPages.Contains(Tab1) Then
                    TabCondicao.TabPages.Add(Tab1)
                End If

            ElseIf qtsDias > 5 Then

                If Not TabCondicao.TabPages.Contains(Tab1) Then
                    TabCondicao.TabPages.Add(Tab1)
                End If

            End If


        Catch ex As Exception
            MsgBox(ex)
        End Try

    End Sub

    Private Sub btnExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcel.Click
        Dim diaInicial As Integer = 0
        Dim diaFinal As Integer = 0
        Dim MesInicial As String = ""
        Dim MesFinal As String = ""
        Dim NomeArquivo As String = ""

        Try

            Me.Cursor = Cursors.WaitCursor

            If grdRemessa.RowCount = 0 Then
                MessageBox.Show("Não existe dados a serem processados, por favor realize uma nova pesquisa!", "Atenção",
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Error)
                TabControl.SelectedIndex = 0
                Me.Cursor = Cursors.Default
                Exit Sub
            End If


            If txtCaminho.Text = "" Then
                MessageBox.Show("Por favor selecione onde o arquivo deve ser criado", "Atenção",
                         MessageBoxButtons.OK,
                         MessageBoxIcon.Error)
                txtCaminho.Focus()
                Me.Cursor = Cursors.Default
                Exit Sub
            End If



            diaInicial = dtInicio.Value.Day
            diaFinal = dtFim.Value.Day

            MesInicial = dtInicio.Value.Month
            MesFinal = dtFim.Value.Month

            MesInicial = RetornaMesExtenso(MesInicial)
            MesFinal = RetornaMesExtenso(MesFinal)

            If MesInicial.Contains(MesFinal) Then
                NomeArquivo = "Auditoria OI AACI " + diaInicial.ToString() + " a " + diaFinal.ToString() + " de " + MesInicial.ToString() + ".xlsx"
            Else
                NomeArquivo = "Auditoria OI AACI " + diaInicial.ToString() + " de " + MesInicial.ToString() + " a " + diaFinal.ToString() + " de " + MesFinal.ToString() + ".xlsx"
            End If

            NomeArquivo = txtCaminho.Text + "\" + NomeArquivo

            ExportarParaExcel(grdRemessa, NomeArquivo)

            If txtEmail.Text <> "" Then
                Dim result = MessageBox.Show("Deseja enviar um e-mail com a planilha gerada?", "Atenção",
                     MessageBoxButtons.YesNo,
                     MessageBoxIcon.Question)

                If (result = DialogResult.Yes) Then
                    If txtEmail.Text = "" Then
                        MessageBox.Show("Por favor preencha o campo de e-mail!", "Atenção",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Error)
                        txtEmail.Focus()
                        Me.Cursor = Cursors.Default
                        Exit Sub
                    End If
                End If

                If (result = DialogResult.Yes) Then
                    If txtEmail.Text <> "" Then
                        txtCaminho.Text = NomeArquivo
                        EnviaEmail()
                    End If
                End If

            End If

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MessageBox.Show("Erro : " + ex.Message)
        End Try

    End Sub

    Public Function RetornaMesExtenso(ByVal mes As String) As String
        Select Case mes
            Case 1
                mes = "Janeiro"
            Case 2
                mes = "Fevereiro"
            Case 3
                mes = "Março"
            Case 4
                mes = "Abril"
            Case 5
                mes = "Maio"
            Case 6
                mes = "Junho"
            Case 7
                mes = "Julho"
            Case 8
                mes = "Agosto"
            Case 9
                mes = "Setembro"
            Case 10
                mes = "Outubro"
            Case 11
                mes = "Novembro"
            Case 12
                mes = "Dezembro"
            Case Else
                mes = "Mês invalido"

        End Select

        Return mes

    End Function

    Public Sub ExportarParaExcel(ByVal dgvName As DataGridView, Optional ByVal fileName As String = "")
        ' Criar uma nova instância do Excel 
        Dim objExcelApp As New Excel.Application()
        Dim objExcelBook As Excel.Workbook
        Dim objExcelSheet As Excel.Worksheet
        Try

            ' Altera o tipo/localização para Inglês. Existe incompatibilidade  
            ' entre algumas versões de Excel vs Sistema Operativo 
            Dim oldCI As CultureInfo = CurrentThread.CurrentCulture
            CurrentThread.CurrentCulture = New CultureInfo("en-US")
            ' Adiciona um workbook e activa a worksheet actual 
            objExcelBook = objExcelApp.Workbooks.Add
            objExcelSheet = CType(objExcelBook.Worksheets(1), Excel.Worksheet)

            ' Ciclo nos cabeçalhos para escrever os títulos a bold/negrito 
            Dim dgvColumnIndex As Int16 = 1
            For Each col As DataGridViewColumn In dgvName.Columns
                objExcelSheet.Cells(1, 1) = "Código do terceiro "
                objExcelSheet.Cells(1, 2) = "Nome do Cliente "
                objExcelSheet.Cells(1, 3) = "Autorizante "
                objExcelSheet.Cells(1, 4) = "DDD"
                objExcelSheet.Cells(1, 5) = "Telefone"
                objExcelSheet.Cells(1, 6) = "Valor"
                objExcelSheet.Cells(1, 7) = "De"
                objExcelSheet.Cells(1, 8) = "Até"
                objExcelSheet.Cells(1, 9) = "Endereço"
                objExcelSheet.Cells(1, 10) = "Bairro"
                objExcelSheet.Cells(1, 11) = "Cidade"
                objExcelSheet.Cells(1, 12) = "UF"
                objExcelSheet.Cells(1, 13) = "CEP"
                objExcelSheet.Cells(1, 14) = "CPF"
                objExcelSheet.Cells(1, 15) = "SEMANA"

                dgvColumnIndex += 1
            Next

            ' Aqui estou formatando o cabeçalho
            objExcelSheet.Range("A1", "O1").Font.Name = "Verdana"
            objExcelSheet.Range("A1", "O1").Font.Bold = True
            objExcelSheet.Range("A1", "O1").Font.Size = 10
            objExcelSheet.Range("A1", "O1").Font.ColorIndex = 2
            objExcelSheet.Range("A1", "O1").Cells.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter
            objExcelSheet.Range("A1", "O1").Interior.ColorIndex = 25
            objExcelSheet.Range("A1", "O1").Borders.ColorIndex = 1


            ' Ciclo nas linhas/células 
            Dim dgvRowIndex As Integer = 2

            For Each row As DataGridViewRow In dgvName.Rows
                Dim dgvCellIndex As Integer = 1

                For Each cell As DataGridViewCell In row.Cells
                    objExcelSheet.Cells(dgvRowIndex, dgvCellIndex) = cell.Value
                    objExcelSheet.Cells(dgvRowIndex, dgvCellIndex).Borders.ColorIndex = 1
                    dgvCellIndex += 1
                Next
                dgvRowIndex += 1
            Next
            ' Ajusta o largura das colunas automaticamente 
            objExcelSheet.Columns.AutoFit()
            ' Caso a opção seja gravar (xlsSaveAs) grava o ficheiro e fecha 
            ' o Workbook/Excel. Caso contrário (xlsOpen) abre o Excel 

            objExcelBook.SaveAs(fileName)
            objExcelBook.Close()
            objExcelApp.Quit()
            MessageBox.Show("Arquivo gerado com sucesso para: " & fileName)


            ' Altera a tipo/localização para actual 
            CurrentThread.CurrentCulture = oldCI
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        Finally
            objExcelSheet = Nothing
            objExcelBook = Nothing
            objExcelApp = Nothing
            ' O GC(garbage collector) recolhe a memória não usada pelo sistema.  
            ' O método Collect() força a recolha e a opção WaitForPendingFinalizers  
            ' espera até estar completo. Desta forma o EXCEL.EXE não fica no  
            ' Task Manager(gestor tarefas) ocupando memória desnecessariamente 
            ' (devem ser chamados duas vezes para maior garantia) 
            GC.Collect()
            GC.WaitForPendingFinalizers()
            GC.Collect()
            GC.WaitForPendingFinalizers()
        End Try


    End Sub


    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click

        If FolderBrowserDialog1.ShowDialog() = DialogResult.OK Then
            txtCaminho.Text = FolderBrowserDialog1.SelectedPath.ToString()
        End If
    End Sub

    Private Sub EnviaEmail()
        Try

            'create the mail message
            Dim SmtpServer As New SmtpClient()
            Dim mail As New MailMessage()
            SmtpServer.Credentials = New Net.NetworkCredential("aaci.jf.mg@gmail.com", "kwcmuyymqekemvtz")
            SmtpServer.Host = "smtp.gmail.com"
            SmtpServer.Port = 587 'server para gmail
            SmtpServer.EnableSsl = True
            mail.From = New MailAddress("aaci.jf.mg@gmail.com")
            mail.To.Add(txtEmail.Text) 'Email que recebera a Mensagem
            mail.Subject = "Planilha Auditoria AACI"
            mail.Body = ""

            'Este codigo PlainView e caso o email não suporte Html, nisto ele irá informar este erro

            Dim plainView As AlternateView = AlternateView.CreateAlternateViewFromString("Boa tarde,como solicitado, estamos excluindo o seu cadastro do nosso sistema. Desculpe o transtorno, agradeço a compreensão.", Nothing, "text/plain")

            'crie um corpo de texto caso deseje assim você poderá colocar um texto no meio de duas imagens
            Dim linha1 As String = ""
            If Date.Now.Hour < 12 Then
                linha1 = "Bom dia,"
            Else
                linha1 = "Boa tarde,"
            End If

            Dim linha2 As String = "como solicitado, segue a planilha com as ligações efetuadas no periodo solicitado."
            Dim linha3 As String = "Atenciosamente."

            Dim htmlView As AlternateView = AlternateView.CreateAlternateViewFromString("<p> " + linha1 + "</p> <p>&nbsp;</p> <p>" + linha2 + "</p> <p>&nbsp;</p> <img src=cid:companylogo> <p>&nbsp;</p> <p>" + linha3 + "</p> <p><br /> <br /> --</p> <p>&nbsp;</p> <p>att..</p> <p>&nbsp;</p> <p><strong>AACI - ASSOCIA&Ccedil;&Atilde;O DE APOIO AS CRIAN&Ccedil;AS E IDOSOS</strong></p>   ", Nothing, "text/html")


            mail.Attachments.Add(New Attachment(txtCaminho.Text)) 'Adiciona o anexo a enviar

            With mail
                .AlternateViews.Add(plainView)
                .AlternateViews.Add(htmlView)

            End With

            mail.IsBodyHtml = True

            SmtpServer.Send(mail)
            MsgBox("E-mail enviado com sucesso", vbInformation, "Ok")





        Catch ex As Exception  'caso haja algum erro crie uma msg de aviso.
            MsgBox(ex)
        Finally
            '  File.Delete("")
        End Try
    End Sub

    Private Sub EnviaEmailAuditoria(ByVal emailEnviar As String, ByVal caminho As String)
        Try

            'create the mail message
            Dim SmtpServer As New SmtpClient()
            Dim mail As New MailMessage()
            SmtpServer.Credentials = New Net.NetworkCredential("aaci.jf.mg@gmail.com", "kwcmuyymqekemvtz")
            SmtpServer.Host = "smtp.gmail.com"
            SmtpServer.Port = 587 'server para gmail
            SmtpServer.EnableSsl = True
            mail.From = New MailAddress("aaci.jf.mg@gmail.com")
            mail.To.Add(emailEnviar) 'Email que recebera a Mensagem
            mail.Subject = "Planilha Auditoria AACI"
            mail.Body = ""

            'Este codigo PlainView e caso o email não suporte Html, nisto ele irá informar este erro

            Dim plainView As AlternateView = AlternateView.CreateAlternateViewFromString("Boa tarde,como solicitado, estamos excluindo o seu cadastro do nosso sistema. Desculpe o transtorno, agradeço a compreensão.", Nothing, "text/plain")

            'crie um corpo de texto caso deseje assim você poderá colocar um texto no meio de duas imagens
            Dim linha1 As String = ""
            If Date.Now.Hour < 12 Then
                linha1 = "Bom dia,"
            Else
                linha1 = "Boa tarde,"
            End If

            Dim linha2 As String = "como solicitado, segue a planilha com as ligações efetuadas no periodo solicitado."
            Dim linha3 As String = "Atenciosamente."

            Dim htmlView As AlternateView = AlternateView.CreateAlternateViewFromString("<p> " + linha1 + "</p> <p>&nbsp;</p> <p>" + linha2 + "</p> <p>&nbsp;</p> <img src=cid:companylogo> <p>&nbsp;</p> <p>" + linha3 + "</p> <p><br /> <br /> --</p> <p>&nbsp;</p> <p>att..</p> <p>&nbsp;</p> <p><strong>AACI - ASSOCIA&Ccedil;&Atilde;O DE APOIO AS CRIAN&Ccedil;AS E IDOSOS</strong></p>   ", Nothing, "text/html")

            caminho = caminho.Remove(caminho.Length - 3)
            caminho += "xls"


            mail.Attachments.Add(New Attachment(caminho)) 'Adiciona o anexo a enviar

            With mail
                .AlternateViews.Add(plainView)
                .AlternateViews.Add(htmlView)

            End With

            mail.IsBodyHtml = True

            SmtpServer.Send(mail)
            MsgBox("E-mail enviado com sucesso", vbInformation, "Ok")





        Catch ex As Exception  'caso haja algum erro crie uma msg de aviso.
            MsgBox(ex)
        Finally
            '  File.Delete("")
        End Try
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub

    Private Sub btnImportar_Click(sender As Object, e As EventArgs) Handles btnImportar.Click
        Try

            Dim telefones As New List(Of String)
            Dim rn = New RNGenerico()
            Dim DataSet = New DataSet("Planilha")

            If txtCaminhoCVS.Text = "" Then
                MsgBox("Por favor selecione um arquivo valido", vbOKOnly, "Atenção!")
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor

            ' Obtem o texto do arquivo
            Dim arquivoCSV As String = File.ReadAllText(txtCaminhoCVS.Text)
            ' Obtém cada linha do arquivo
            arquivoCSV = arquivoCSV.Replace(ControlChars.Lf, ControlChars.Cr)
            Dim linhas As String() = arquivoCSV.Split(New Char() {ControlChars.Cr}, StringSplitOptions.RemoveEmptyEntries)
            ' Ve quantas linhas e colunas existem
            Dim numeroLinhas As Integer = linhas.Length
            Dim numeroColunas As Integer = linhas(0).Split(";"c).Length
            ' Aloca dados no array
            Dim valores As String(,) = New String(numeroLinhas - 1, numeroColunas - 1) {}

            Dim linhaAUX As String = ""
            Dim DDD As String = ""
            Dim telefone As String = ""


            For vetor As Integer = 0 To numeroLinhas - 1
                Dim linha_vetor As String() = linhas(vetor).Split(";"c)
                For c As Integer = 0 To numeroColunas - 1
                    valores(vetor, c) = linha_vetor(c)

                    If c = 1 And linha_vetor(c) <> "ddd" Then
                        DDD = linha_vetor(c)
                    End If

                    If c = 2 And linha_vetor(c) <> "telefone" Then
                        telefone = linha_vetor(c)
                    End If

                Next

                If DDD <> "" And telefone <> "" Then

                    linhaAUX = "INSERT INTO #T (ddd, telefone) values ('" + DDD + "','" + telefone + "')"

                    telefones.Add(linhaAUX)

                End If

            Next

            DataSet = rn.RetornaLigacoesPlanilhaOi(telefones, dtInicio.Value, dtFim.Value)
            CriarArquivoExcel(DataSet, txtCaminhoCVS.Text)

            If (chkEnviaEmailCSV.Checked = True) Then
                EnviaEmailAuditoria("auditoria.aaci.jf.mg@gmail.com", txtCaminhoCVS.Text)
            End If

            Me.Cursor = Cursors.Default



        Catch ex As Exception  'caso haja algum erro crie uma msg de aviso.
            MsgBox(ex)
            Me.Cursor = Cursors.Default
            'Finally
            '  File.Delete("")
        End Try

    End Sub

    Private Sub CriarArquivoExcel(ByVal dados As DataSet, ByVal local As String)

        Dim misValue As Object = System.Reflection.Missing.Value
        Dim xlApp As New Excel.Application()
        Dim xlWorkBook As Excel.Workbook
        Dim xlWorkSheet As Excel.Worksheet


        Try


            local = local.Remove(local.Length - 3)
            local += "xls"

            xlApp = New Microsoft.Office.Interop.Excel.Application()

            xlWorkBook = xlApp.Workbooks.Add(misValue)

            xlWorkSheet = xlWorkBook.Worksheets(1)

            '//MONTA CABEÇALHO

            xlWorkSheet.Cells(1, 1) = "DDD"
            xlWorkSheet.Cells(1, 2) = "Telefone"
            xlWorkSheet.Cells(1, 3) = "Nome Cliente"
            xlWorkSheet.Cells(1, 4) = "Operador"
            xlWorkSheet.Cells(1, 5) = "data Inclusao"

            Dim n As Integer = 2

            For Each dr As DataRow In dados.Tables(0).Rows

                xlWorkSheet.Cells(n, 1) = dr(0).ToString()
                xlWorkSheet.Cells(n, 2) = dr(1).ToString()
                xlWorkSheet.Cells(n, 3) = dr(2).ToString()
                xlWorkSheet.Cells(n, 4) = dr(3).ToString()
                xlWorkSheet.Cells(n, 5) = dr(4).ToString()
                n = n + 1
            Next

            xlApp.Columns.AutoFit()
            xlWorkBook.SaveAs(local)
            xlWorkBook.Close()
            xlApp.Quit()
            MessageBox.Show("Arquivo gerado com sucesso para: " & local)



        Catch ex As Exception  'caso haja algum erro crie uma msg de aviso.
            MsgBox(ex)
        Finally
            xlWorkSheet = Nothing
            xlWorkBook = Nothing
            xlApp = Nothing
            ' O GC(garbage collector) recolhe a memória não usada pelo sistema.  
            ' O método Collect() força a recolha e a opção WaitForPendingFinalizers  
            ' espera até estar completo. Desta forma o EXCEL.EXE não fica no  
            ' Task Manager(gestor tarefas) ocupando memória desnecessariamente 
            ' (devem ser chamados duas vezes para maior garantia) 
            GC.Collect()
            GC.WaitForPendingFinalizers()
            GC.Collect()
            GC.WaitForPendingFinalizers()
        End Try

    End Sub

    Private Sub btnBuscarCVS_Click(sender As Object, e As EventArgs) Handles btnBuscarCVS.Click
        Dim AbrirComo As OpenFileDialog = New OpenFileDialog()
        Dim caminho As DialogResult


        AbrirComo.Title = "Abrir como"
        AbrirComo.InitialDirectory = "C:\DIGITAL"
        AbrirComo.FileName = "Nome Arquivo"
        AbrirComo.Filter = "Arquivos Textos (*.*)|*.*"
        caminho = AbrirComo.ShowDialog

        NomeArquivo = AbrirComo.SafeFileName

        If NomeArquivo = Nothing Then
            MessageBox.Show("Arquivo Inválido", "Arquivo", MessageBoxButtons.OK)
        Else
            txtCaminhoCVS.Text = AbrirComo.FileName
        End If
    End Sub

    Private Sub cboOperadorTab1Cond1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboOperadorTab1Cond1.SelectedIndexChanged
        If cboOperadorTab1Cond1.SelectedIndex <> 0 Then
            chkTab1Condicao1.Checked = True
        Else
            chkTab1Condicao1.Checked = False
        End If

    End Sub

    Private Sub btnExcluirOperadora_Click(sender As Object, e As EventArgs) Handles btnExcluirOperadora.Click

        Dim condicao As String = ""
        Try


            If chkTab1Condicao1.Checked = True And cboOperadorTab1Cond1.SelectedIndex <> 0 Then
                condicao = "delete from #t" & vbNewLine
                If txtTab1Hora1.Text <> "" Then
                    condicao &= "where datepart(HOUR, hora_inclusao)<= " & txtTab1Hora1.Text & " and dt_inclusao = '" & CDate(dtTab1Condicao1.Value).ToString("yyyyMMdd") & "'"
                Else
                    condicao &= "where dt_inclusao = '" & CDate(dtTab1Condicao1.Value).ToString("yyyyMMdd") & "'"
                End If
                If cboOperadorTab1Cond1.SelectedIndex <> 0 Then
                    condicao &= "and operador = '" & cboOperadorTab1Cond1.Text & "'"
                End If
            End If

            Condicao1.Add(condicao)

        Catch ex As Exception
            MsgBox(ex)
        End Try

    End Sub

    Private Sub btnExcluirTelefone_Click(sender As Object, e As EventArgs) Handles btnExcluirTelefone.Click

        Dim condicao As String = ""
        Try

            If txtDDD.Text <> "" And txtTelefone.Text <> "" Then
                condicao = "insert into #temp (DDD, Telefone) values ('" + txtDDD.Text + "','" + txtTelefone.Text + "' )"
            End If

            Condicao2.Add(condicao)

            MsgBox("Telefone excluido da lista!", vbOKOnly, "Atenção")

            txtDDD.Text = ""
            txtTelefone.Text = ""


        Catch ex As Exception
            MsgBox(ex)
        End Try
    End Sub

    Private Sub CheckBox1_CheckedChanged(sender As Object, e As EventArgs) Handles chkEnviaEmailCSV.CheckedChanged

    End Sub

    Private Sub btnReload_Click(sender As Object, e As EventArgs) Handles btnReload.Click
        ListViewTab1.Clear()
        carregaOperador()
    End Sub
End Class