﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text
Imports Microsoft.Office.Interop


Public Class FrmConsultaContribuinte


#Region "Enum"

    Private Enum ColunasGridRemessa

        DDD = 0
        Telefone = 1
        Nome = 2
        Valor = 3
        Cod_EAN_CLIENTE = 4
    End Enum

    Enum xlsOption
        xlsOpen
    End Enum

#End Region

#Region "variaveis"

    Public telefone As String
    Private lstContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstContribuinteSelecionada As New List(Of CRetorno)
    Private v_ambiente As String
    Private rnContribuinte As RNContribuinte = New RNContribuinte
    Private rnRetorno As RNRetorno = New RNRetorno
    Private contribuinte As CContribuinte = New CContribuinte
    Private lstContribuintesOi As New SortableBindingList(Of CRetorno)
    Private lstContribuinteSelecionadaOi As New List(Of CRetorno)

#End Region



#Region "Metodos"



    Private Sub carregaGrid()

        Dim objContribuintes As CContribuinte = New CContribuinte()

        Dim bindingSourceApolice As BindingSource = New BindingSource

        Try



            lstContribuintes = New SortableBindingList(Of CContribuinte)(rnContribuinte.CarregarGridOi(objContribuintes, "Remessa"))

            If (lstContribuintes.Count > 0) Then

                bindingSourceApolice.DataSource = lstContribuintes

                grdRemessa.DataSource = bindingSourceApolice.DataSource
                'grdRemessa.Sort(grdRemessa.Columns(ColunasGridRemessa.Nome), ListSortDirection.Ascending)
                'lblNumRegistros.Text = lstApolices.Count


            End If


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CriarColunasGrid()

        Try

            grdRemessa.DataSource = Nothing
            grdRemessa.Columns.Clear()

            grdRemessa.AutoGenerateColumns = False

            grdRemessa.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            'Dim checkboxColumn As New DataGridViewCheckBoxColumn
            'checkboxColumn.Width = 32
            'checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StDDDMeioAcessoCobranca"
            column.HeaderText = "DDD"
            column.Width = 30
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)


            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StNumMeioAcessoCobranca"
            column.HeaderText = "Telefone"
            column.Width = 80
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "nome_cliente"
            column.HeaderText = "Nome"
            column.Width = 300
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DVlrServico"
            column.HeaderText = "Valor"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            grdRemessa.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "cod_ean_cliente"
            column.HeaderText = "Cod. EAN Cliente"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = False
            column.Visible = True
            grdRemessa.Columns.Add(column)

            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UsSitFaturamento"
            column.HeaderText = "Situacao Faturamento"
            column.Width = 160
            column.ReadOnly = True
            column.Visible = False
            column.Visible = True
            grdRemessa.Columns.Add(column)


            '7
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UiMotivoSituacao"
            column.HeaderText = "Motivo Situação"
            column.Visible = True
            column.Width = 150
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)




        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

#End Region

    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""

        Try

            If cboSituacao.SelectedIndex = 0 Or cboSituacao.Text = "Selecione..." Then
                MsgBox("Opção invalida, selecionar a situação!", MsgBoxStyle.Critical, "Atenção!")
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor

            carregaGridSituacaoContribuinte(cboSituacao.Text)

            carregaFeedGridSituacaoContribuinte(cboSituacao.Text)


            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try

    End Sub

    Private Sub carregaGridSituacaoContribuinte(ByVal situacao As String)

        Dim objRetorno As CRetorno = New CRetorno()

        Dim bindingSourceApolice As BindingSource = New BindingSource

        Try

            lstContribuintesOi = New SortableBindingList(Of CRetorno)(rnRetorno.CarregarContribuintesPelaSituacao(situacao, DtMesAnoPesquisa.Text, "Consulta"))

            If (lstContribuintesOi.Count > 0) Then

                bindingSourceApolice.DataSource = lstContribuintesOi

                grdRemessa.DataSource = bindingSourceApolice.DataSource
                'grdRemessa.Sort(grdRemessa.Columns(ColunasGridRemessa.Nome), ListSortDirection.Ascending)
                'lblNumRegistros.Text = lstApolices.Count


            End If

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub carregaFeedGridSituacaoContribuinte(ByVal situacao As String)

        Dim objRetorno As CRetorno = New CRetorno()

        Dim bindingSourceApolice As BindingSource = New BindingSource

        Try

            lstContribuintesOi = New SortableBindingList(Of CRetorno)(rnRetorno.CarregarFeedContribuintesPelaSituacao(situacao, "Consulta"))

            If (lstContribuintesOi.Count > 0) Then

                bindingSourceApolice.DataSource = lstContribuintesOi

                grdRemessa.DataSource = bindingSourceApolice.DataSource
                'grdRemessa.Sort(grdRemessa.Columns(ColunasGridRemessa.Nome), ListSortDirection.Ascending)
                'lblNumRegistros.Text = lstApolices.Count


            End If

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub
    Private Sub btnRetorno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRetorno.Click
        Try

            FrmRetorno.Show()

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Close()
    End Sub

    Private Sub FrmConsultaContribuinte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        CriarColunasGrid()
        CarregaComboSituacao(0)
        CarregaComboContribuinte()



    End Sub

    Private Sub CarregaComboSituacao(ByVal cod_situacao As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboSituacao.DataSource = Nothing
            cboSituacao.Items.Clear()
            cboSituacao.DisplayMember = "situacao"
            cboSituacao.ValueMember = "situacao_faturamento_id"
            cboSituacao.DataSource = rn_generico.BuscarSituacao(0)

            If cboSituacao.Items.Count = 0 Then
                cboSituacao.SelectedIndex = 0
            ElseIf cboSituacao.Items.Count > 0 And cod_situacao <> 0 Then

                For i As Integer = 0 To cboSituacao.Items.Count - 1
                    For Each linha As CGenerico In cboSituacao.DataSource
                        If linha.Situacao = cod_situacao Then
                            cboSituacao.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next

            Else

                cboSituacao.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CarregaComboContribuinte()
        Dim rn_contribuinte As RNContribuinte = New RNContribuinte
        Dim filtro As CContribuinte = New CContribuinte

        cboNome.DataSource = Nothing
        cboNome.Items.Clear()
        cboNome.DisplayMember = "NomeCliente1"
        cboNome.ValueMember = "Cod_cliente"
        cboNome.DataSource = rn_contribuinte.CarregarGrid(filtro, "Contribuinte")

        If cboNome.Items.Count > 0 Then
            cboNome.SelectedIndex = 0
        End If


    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExportar.Click

        ExportarParaExcel(grdRemessa, "teste")
    End Sub

    Public Sub ExportarParaExcel(ByVal dgvName As DataGridView, Optional ByVal fileName As String = "")
        ' Criar uma nova instância do Excel 
        Dim objExcelApp As New Excel.Application()
        Dim objExcelBook As Excel.Workbook
        Dim objExcelSheet As Excel.Worksheet
        Try

            ' Altera o tipo/localização para Inglês. Existe incompatibilidade  
            ' entre algumas versões de Excel vs Sistema Operativo 
            Dim oldCI As CultureInfo = CurrentThread.CurrentCulture
            CurrentThread.CurrentCulture = New CultureInfo("en-US")
            ' Adiciona um workbook e activa a worksheet actual 
            objExcelBook = objExcelApp.Workbooks.Add
            objExcelSheet = CType(objExcelBook.Worksheets(1), Excel.Worksheet)
            ' Ciclo nos cabeçalhos para escrever os títulos a bold/negrito 
            Dim dgvColumnIndex As Int16 = 1
            For Each col As DataGridViewColumn In dgvName.Columns
                objExcelSheet.Cells(1, dgvColumnIndex) = col.HeaderText
                objExcelSheet.Cells(1, dgvColumnIndex).Font.Bold = True
                dgvColumnIndex += 1
            Next
            ' Ciclo nas linhas/células 
            Dim dgvRowIndex As Integer = 2

            For Each row As DataGridViewRow In dgvName.Rows
                Dim dgvCellIndex As Integer = 1

                For Each cell As DataGridViewCell In row.Cells
                    objExcelSheet.Cells(dgvRowIndex, dgvCellIndex) = cell.Value
                    dgvCellIndex += 1
                Next
                dgvRowIndex += 1
            Next
            ' Ajusta o largura das colunas automaticamente 
            objExcelSheet.Columns.AutoFit()
            ' Caso a opção seja gravar (xlsSaveAs) grava o ficheiro e fecha 
            ' o Workbook/Excel. Caso contrário (xlsOpen) abre o Excel 

            objExcelBook.SaveAs("C:\AACI\teste.xlsx")
            objExcelBook.Close()
            objExcelApp.Quit()
            MessageBox.Show("Ficheiro exportado com sucesso para: " & fileName)

           
            ' Altera a tipo/localização para actual 
            CurrentThread.CurrentCulture = oldCI
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        Finally
            objExcelSheet = Nothing
            objExcelBook = Nothing
            objExcelApp = Nothing
            ' O GC(garbage collector) recolhe a memória não usada pelo sistema.  
            ' O método Collect() força a recolha e a opção WaitForPendingFinalizers  
            ' espera até estar completo. Desta forma o EXCEL.EXE não fica no  
            ' Task Manager(gestor tarefas) ocupando memória desnecessariamente 
            ' (devem ser chamados duas vezes para maior garantia) 
            GC.Collect()
            GC.WaitForPendingFinalizers()
            GC.Collect()
            GC.WaitForPendingFinalizers()
        End Try


    End Sub

    Private Sub grdRemessa_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdRemessa.CellDoubleClick

        Dim ClienteClicado As String
        Dim DDDSelecionado As String
        Dim TelefoneSelecionado As String

        Try

            ClienteClicado = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.Cod_EAN_CLIENTE).Value, String)
            If ClienteClicado = Nothing Then
                TelefoneSelecionado = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.Telefone).Value, Integer)
            End If

            DDDSelecionado = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.DDD).Value, Integer)

            For Each ContribuinteInconsistencia As CRetorno In lstContribuintesOi
                If ContribuinteInconsistencia.Cod_ean_cliente = ClienteClicado Then

                End If
            Next

            If (e.RowIndex > -1) Then
                Dim frm As frmContribuinte = New frmContribuinte
                frm.FormNome = "Contribuinte"
                frm.Cod_Ean_Cliente = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.Cod_EAN_CLIENTE).Value, String)
                frm.Telefone = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.Telefone).Value, String)
                frm.MesRef = DtMesAnoPesquisa.Text
                frm.UserName = "AACI"

                lstContribuinteSelecionada = New List(Of CRetorno)
                lstContribuinteSelecionada.Add(lstContribuintesOi(e.RowIndex))
                frm.Selecionado = lstContribuinteSelecionada
                frm.ShowDialog()

            End If

            contribuinte = rnContribuinte.PesquisarContribuintePorTelefoneSimplificado(telefone, DDDSelecionado, 1)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

   
   
    Private Sub txtNome_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNome.TextChanged
        Dim texto As String = Nothing
        Dim linhaVazia As Integer = 0
        Dim contador As Integer = 0

        If txtNome.Text <> String.Empty Then

            linhaVazia = grdRemessa.Rows.Count - 1

            'percorre cada linha do DataGridView
            For Each linha As DataGridViewRow In grdRemessa.Rows

                'percorre cada célula da linha
                For Each celula As DataGridViewCell In grdRemessa.Rows(linha.Index).Cells
                    If Not contador = linhaVazia Then
                        'se a coluna for a coluna 2 (Nome) então verifica o criterio
                        If celula.ColumnIndex = 2 Then

                            If celula.Value.ToString <> "" Then
                                texto = IIf(celula.Value.ToString = Nothing, "", celula.Value.ToString)
                                'se o texto informado estiver contido na célula então seleciona toda linha
                                If texto.Contains(txtNome.Text) Then
                                    'seleciona a linha
                                    grdRemessa.CurrentCell = celula
                                    grdRemessa.CurrentCell = grdRemessa.Rows(celula.RowIndex).Cells(2)
                                    Exit Sub
                                End If
                            End If
                            contador = contador + 1
                        End If
                    End If

                Next

            Next

        End If

    End Sub
End Class