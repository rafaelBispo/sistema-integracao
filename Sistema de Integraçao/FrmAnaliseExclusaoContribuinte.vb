﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text
Imports Microsoft.Office.Interop


Public Class FrmAnaliseExclusaoContribuinte


#Region "Enum"

    Private Enum ColunasGridRemessa

        Selecao = 0
        Cod_EAN_CLIENTE = 1
        Telefone = 2
        cod_opera = 3
        operador = 4
        Dt_inclusao = 5

    End Enum



    Enum xlsOption
        xlsOpen
    End Enum

#End Region

#Region "variaveis"

    Public telefone As String
    Private lstContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstRetornoContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstContribuinteSelecionada As New List(Of CRetorno)
    Private v_ambiente As String
    Private rnContribuinte As RNContribuinte = New RNContribuinte
    Private rnRetorno As RNRetorno = New RNRetorno
    Private contribuinte As CContribuinte = New CContribuinte
    Private lstContribuintesOi As New SortableBindingList(Of CRetorno)
    Private lstListaTelefonica As New SortableBindingList(Of CListaTelefonica)
    Private lstContribuinteSelecionadaOi As New List(Of CRetorno)
    Private rnRequisicao As RNRequisicao = New RNRequisicao
    Private lstrequisicao As New SortableBindingList(Of CContribuinte)
    Private lstrequisicaoSelecionada As New List(Of CContribuinte)
    Private lstexclusao As New SortableBindingList(Of CGenerico)
    Public mNivelAcesso As Integer
    Public mUsuario As String
    Public mCodUsuario As Integer
    Public atualizou As Boolean
    Public bindingSourceContribuite As BindingSource = New BindingSource


    Public Property NivelAcesso() As Integer
        Get
            Return mNivelAcesso
        End Get
        Set(ByVal value As Integer)
            mNivelAcesso = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return mUsuario
        End Get
        Set(ByVal value As String)
            mUsuario = value
        End Set
    End Property

    Public Property CodUsuario() As Integer
        Get
            Return mCodUsuario
        End Get
        Set(ByVal value As Integer)
            mCodUsuario = value
        End Set
    End Property

#End Region



#Region "Metodos"



    Private Sub CriarColunasGrid()

        Try

            grdRemessa.DataSource = Nothing
            grdRemessa.Columns.Clear()

            grdRemessa.AutoGenerateColumns = False

            grdRemessa.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            Dim checkboxColumn As New DataGridViewCheckBoxColumn
            checkboxColumn.Width = 32
            checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "cod_ean_cliente"
            column.HeaderText = "Cod. EAN Cliente"
            column.Width = 100
            column.ReadOnly = False
            column.Visible = True
            grdRemessa.Columns.Add(column)



            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Telefone"
            column.HeaderText = "Telefone"
            column.Width = 100
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "cod_opera"
            column.HeaderText = "Cód. Operador"
            column.Width = 100
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "operador"
            column.HeaderText = "Operador"
            column.Width = 100
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)


            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Dt_inclusao"
            column.HeaderText = "Dia Solicitação"
            column.Width = 100
            column.ReadOnly = False
            column.Visible = True
            grdRemessa.Columns.Add(column)



        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub


#End Region

    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""

        Try

            Me.Cursor = Cursors.WaitCursor


            carregaFeedGridCategoriaContribuinte()
            'CarregaPrimeiraLinha()


            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try

    End Sub



    Private Sub carregaFeedGridCategoriaContribuinte()

        Dim objRetorno As CRetorno = New CRetorno()
        Dim rn As RNGenerico = New RNGenerico
        Dim ds As DataSet = New DataSet

        Try

            grdRemessa.DataSource = Nothing

            lstexclusao = New SortableBindingList(Of CGenerico)(rn.RetornaTelefonesAnaliseExclusao("Analise"))

            If (lstexclusao.Count > 0) Then

                bindingSourceContribuite.DataSource = lstexclusao

                grdRemessa.DataSource = bindingSourceContribuite.DataSource

            Else

                MsgBox("Não existe registros a serem processados, verifique se houve a liberação das fichas!")
            End If



        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub
    Private Sub btnRetorno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            FrmRetorno.Show()

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Close()
    End Sub

    Private Sub FrmConsultaContribuinte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load



        Me.Cursor = Cursors.WaitCursor

        CriarColunasGrid()

        Me.Cursor = Cursors.Default



        atualizou = True

    End Sub

#Region "Carrega Combos"


    Private Sub CarregaComboContribuinte()
        Dim rn_contribuinte As RNContribuinte = New RNContribuinte
        Dim filtro As CContribuinte = New CContribuinte

        cboNome.DataSource = Nothing
        cboNome.Items.Clear()
        cboNome.DisplayMember = "NomeCliente1"
        cboNome.ValueMember = "Cod_cliente"
        cboNome.DataSource = rn_contribuinte.CarregarGrid(filtro, "Contribuinte")

        If cboNome.Items.Count > 0 Then
            cboNome.SelectedIndex = 0
        End If


    End Sub

#End Region

    Private Sub FrmConsultaUltimaMovimentacao_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""

        Try

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub



    Private Sub txtNome_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNome.TextChanged
        Dim texto As String = Nothing
        Dim linhaVazia As Integer = 0
        Dim contador As Integer = 0

        If txtNome.Text <> String.Empty Then

            linhaVazia = grdRemessa.Rows.Count - 1

            'percorre cada linha do DataGridView
            For Each linha As DataGridViewRow In grdRemessa.Rows

                'percorre cada célula da linha
                For Each celula As DataGridViewCell In grdRemessa.Rows(linha.Index).Cells
                    If Not contador = linhaVazia Then
                        'se a coluna for a coluna 2 (Nome) então verifica o criterio
                        If celula.ColumnIndex = 2 Then

                            If celula.Value.ToString <> "" Then
                                texto = IIf(celula.Value.ToString = Nothing, "", celula.Value.ToString)
                                'se o texto informado estiver contido na célula então seleciona toda linha
                                If texto.Contains(txtNome.Text) Then
                                    'seleciona a linha
                                    grdRemessa.CurrentCell = celula
                                    grdRemessa.CurrentCell = grdRemessa.Rows(celula.RowIndex).Cells(2)
                                    Exit Sub
                                End If
                            End If
                            contador = contador + 1
                        End If
                    End If

                Next

            Next

        End If

    End Sub

    Private Sub txtTelefone_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTelefone.TextChanged
        Dim texto As String = Nothing
        Dim linhaVazia As Integer = 0
        Dim contador As Integer = 0

        If txtTelefone.Text <> String.Empty Then

            linhaVazia = grdRemessa.Rows.Count - 1

            'percorre cada linha do DataGridView
            For Each linha As DataGridViewRow In grdRemessa.Rows

                'percorre cada célula da linha
                For Each celula As DataGridViewCell In grdRemessa.Rows(linha.Index).Cells
                    If Not contador = linhaVazia Then
                        'se a coluna for a coluna 1 (telefone) então verifica o criterio
                        If celula.ColumnIndex = 1 Then

                            If celula.Value.ToString <> "" Then
                                texto = IIf(celula.Value.ToString = Nothing, "", celula.Value.ToString)
                                'se o texto informado estiver contido na célula então seleciona toda linha
                                If texto.Contains(txtTelefone.Text) Then
                                    'seleciona a linha
                                    grdRemessa.CurrentCell = celula
                                    grdRemessa.CurrentCell = grdRemessa.Rows(celula.RowIndex).Cells(2)
                                    Exit Sub
                                End If
                            End If
                            contador = contador + 1
                        End If
                    End If

                Next

            Next

        End If
    End Sub




    Private Sub btnLibera_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLibera.Click
        Dim rnGenerico As RNGenerico = New RNGenerico
        Dim ClienteSelecionado As String
        Dim TelefoneSelecionado As String
        Dim isErro As Integer = 0

        Try

            Me.Cursor = Cursors.WaitCursor

            For Each linha As DataGridViewRow In grdRemessa.Rows

                ClienteSelecionado = linha.Cells(1).Value

                If (Convert.ToBoolean(linha.Cells(ColunasGridRemessa.Selecao).Value)) = True Then

                    contribuinte = rnContribuinte.PesquisarPorCodBarra(ClienteSelecionado)

                    ClienteSelecionado = CType(linha.Cells(ColunasGridRemessa.Cod_EAN_CLIENTE).Value, String)
                    TelefoneSelecionado = CType(linha.Cells(ColunasGridRemessa.Telefone).Value, String)

                    contribuinte.Telefone1 = TelefoneSelecionado

                    rnContribuinte.Excluir(contribuinte.Cod_EAN_Cliente, contribuinte.DDD, contribuinte.Telefone1, "", "", "")
                    rnGenerico.AtualizaAnaliseExclusao(contribuinte)
                    MsgBox("Registro excluido com sucesso!", MsgBoxStyle.Information, "Atenção")
                Else
                    MsgBox("Ocorreu um erro na exclusão, tente novamente!! ", MsgBoxStyle.Information, "Atenção")
                End If
            Next

            grdRemessa.DataSource = Nothing

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub chkHeader_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkHeader.CheckedChanged

        Me.Cursor = Cursors.WaitCursor

        Dim i As Integer
        Dim valorCheckHeader As Boolean

        valorCheckHeader = chkHeader.Checked

        For i = 0 To grdRemessa.RowCount - 1

            If valorCheckHeader = True Then
                grdRemessa(ColunasGridRemessa.Selecao, i).Value = valorCheckHeader
            Else
                grdRemessa(ColunasGridRemessa.Selecao, i).Value = valorCheckHeader
            End If
        Next i

        Me.Cursor = Cursors.Default
        grdRemessa.EndEdit()
    End Sub


    Private Sub btnRotinaCancelamento_Click(sender As System.Object, e As System.EventArgs) Handles btnRotinaCancelamento.Click
        Dim rnGenerico As RNGenerico = New RNGenerico
        Dim isErro As Integer = 0
        Dim contribuinte As CContribuinte = New CContribuinte
        Dim count As Integer = 0
        Try

            Me.Cursor = Cursors.WaitCursor

            lstrequisicao = New SortableBindingList(Of CContribuinte)(rnContribuinte.CarregaListaTelefoneExclusao())

            ProgressBar1.Minimum = 0
            ProgressBar1.Maximum = lstrequisicao.Count

            lblExlcusao.Text = "Excluindo " & count & " de " & lstrequisicao.Count


            For Each contribuinte In lstrequisicao

                If Not (rnContribuinte.RodarExclusao(contribuinte)) = True Then
                    isErro = isErro + 1
                End If

                ProgressBar1.Increment(1)
                count = count + 1
                lblExlcusao.Text = "Excluindo " & count & " de " & lstrequisicao.Count

            Next

            If isErro = 0 Then
                MsgBox("Operação realizada com sucesso!", MsgBoxStyle.Information, "Atenção")
            Else
                MsgBox("Ocorreu um erro na rotina executada, tente novamente!! ", MsgBoxStyle.Information, "Atenção")
            End If

            lstrequisicao = Nothing

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub
End Class