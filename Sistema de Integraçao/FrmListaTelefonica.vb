﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text
Imports Microsoft.Office.Interop


Public Class FrmListaTelefonica



#Region "Enum"

    Private Enum ColunasGridListaTelefonica
        DDD = 0
        Telefone = 1
        nome_cliente = 2
        valor = 3
        categoria = 4
        lista_telefonica_Id = 5
    End Enum

    Private Enum ColunasGridHistorico
        DDD = 0
        Telefone = 1
        Valor = 2
        categoria = 3
        lista_telefonica_Id = 5
        ' mesRef = 4
        'Situacao = 5
        'MotivoSituacao = 6
        Dt_Situacao = 7


    End Enum

    Enum xlsOption
        xlsOpen
    End Enum

#End Region

#Region "variaveis"

    Public telefone As String
    Private lstContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstContribuinteSelecionada As New List(Of CRetorno)
    Private v_ambiente As String
    Private rnContribuinte As RNContribuinte = New RNContribuinte
    Private rnRetorno As RNRetorno = New RNRetorno
    Private contribuinte As CContribuinte = New CContribuinte
    Private lstContribuintesOi As New SortableBindingList(Of CRetorno)
    Private lstListaTelefonica As New SortableBindingList(Of CListaTelefonica)
    Private lstContribuinteSelecionadaOi As New List(Of CGenerico)
    Public mNivelAcesso As Integer
    Public mUsuario As String
    Public mCodUsuario As Integer
    Public atualizou As Boolean
    Public bindingSourceContribuite As BindingSource = New BindingSource
    Public ListaTelefonicaId As Integer = 0


    Public Property NivelAcesso() As Integer
        Get
            Return mNivelAcesso
        End Get
        Set(ByVal value As Integer)
            mNivelAcesso = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return mUsuario
        End Get
        Set(ByVal value As String)
            mUsuario = value
        End Set
    End Property

    Public Property CodUsuario() As Integer
        Get
            Return mCodUsuario
        End Get
        Set(ByVal value As Integer)
            mCodUsuario = value
        End Set
    End Property

#End Region



#Region "Metodos"



    Private Sub carregaGrid()

        Dim objContribuintes As CContribuinte = New CContribuinte()

        Dim bindingSourceApolice As BindingSource = New BindingSource

        Try



            lstContribuintes = New SortableBindingList(Of CContribuinte)(rnContribuinte.CarregarGridOi(objContribuintes, "Remessa"))

            If (lstContribuintes.Count > 0) Then

                bindingSourceContribuite.DataSource = lstContribuintes

                grdListaTelefonica.DataSource = bindingSourceContribuite.DataSource


            End If


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CriarColunasGrid()

        Try

            grdListaTelefonica.DataSource = Nothing
            grdListaTelefonica.Columns.Clear()

            grdListaTelefonica.AutoGenerateColumns = False

            grdListaTelefonica.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            'Dim checkboxColumn As New DataGridViewCheckBoxColumn
            'checkboxColumn.Width = 32
            'checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DDD"
            column.HeaderText = "DDD"
            column.Width = 60
            column.ReadOnly = True
            grdListaTelefonica.Columns.Add(column)


            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Telefone"
            column.HeaderText = "Telefone"
            column.Width = 80
            column.ReadOnly = True
            grdListaTelefonica.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "nome_cliente"
            column.HeaderText = "Nome"
            column.Width = 300
            column.ReadOnly = True
            grdListaTelefonica.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "valor"
            column.HeaderText = "Valor"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            grdListaTelefonica.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "categoria"
            column.HeaderText = "Categoria"
            column.Width = 100
            column.ReadOnly = False
            column.Visible = True
            grdListaTelefonica.Columns.Add(column)

            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "lista_telefonica_id"
            column.HeaderText = "ID"
            column.Width = 0
            column.ReadOnly = True
            column.Visible = True
            grdListaTelefonica.Columns.Add(column)


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub



#End Region

    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""

        Try

            'If cboBuscaCategoria.SelectedIndex = 0 Or cboBuscaCategoria.Text = "Selecione..." Then
            '    MsgBox("Opção invalida, selecionar a situação!", MsgBoxStyle.Critical, "Atenção!")
            '    Exit Sub
            'End If

            Me.Cursor = Cursors.WaitCursor


            CarregaListaTelefonica(cboBuscaCategoria.SelectedValue)


            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try

    End Sub



    'Private Sub carregaFeedGridCategoriaContribuinte(ByVal categoria As String, ByVal codUsuario As Integer)

    '    Dim objRetorno As CRetorno = New CRetorno()


    '    Dim ds As DataSet = New DataSet
    '    Dim valor As Single
    '    Dim strValor As String

    '    Try

    '        ' lstContribuintesOi = New SortableBindingList(Of CRetorno)(rnRetorno.CarregarFeedContribuintesPelaCategoriaNovo(categoria, codUsuario, codUsuario, "Consulta"))

    '        'se ocorrer erro retorna do modo antigo
    '        'If lstContribuintesOi.Count = 0 Then
    '        '    lstContribuintesOi = New SortableBindingList(Of CRetorno)(rnRetorno.CarregarFeedContribuintesPelaCategoria(categoria, DtMesAnoPesquisa.Text, codUsuario, "Consulta"))
    '        'End If


    '        If (lstContribuintesOi.Count > 0) Then

    '            bindingSourceContribuite.DataSource = lstContribuintesOi

    '            grdListaTelefonica.DataSource = bindingSourceContribuite.DataSource
    '            'grdRemessa.Sort(grdRemessa.Columns(ColunasGridRemessa.Nome), ListSortDirection.Ascending)
    '            'lblNumRegistros.Text = lstApolices.Count

    '            ' ds = rnRetorno.CarregarTotaisContribuintesPelaCategoriaNovo(categoria, DtMesAnoPesquisa.Text, codUsuario)

    '            If ds.Tables(0).Rows.Count = 0 Then
    '                '  ds = rnRetorno.CarregarTotaisContribuintesPelaCategoria(categoria, DtMesAnoPesquisa.Text, codUsuario)
    '            End If

    '            If ds.Tables(1).Rows.Count > 0 Then
    '                lblTotalFichas.Text = ds.Tables(1).Rows(0).Item(0)
    '            End If

    '            If ds.Tables(2).Rows.Count > 0 Then

    '                strValor = valor.ToString("C")
    '                valor = IIf(IsDBNull(ds.Tables(2).Rows(0).Item(0)), 0, ds.Tables(2).Rows(0).Item(0))
    '                lblValor.Text = valor.ToString("R$ #,###.00")

    '            End If

    '        Else
    '            grdListaTelefonica.DataSource = Nothing
    '            MsgBox("Não existe registros a serem processados, verifique se houve a liberação das fichas!")

    '        End If

    '    Catch ex As Exception
    '        ' Show the exception's message.
    '       MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
    '    End Try

    'End Sub
    Private Sub btnRetorno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            FrmRetorno.Show()

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Close()
    End Sub

    Private Sub FrmConsultaContribuinte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim cod_cliente As Integer = 0
        Dim contribuinteCarregado As CContribuinte = New CContribuinte
        Dim objContribuinte As CContribuinte = New CContribuinte

        Try


            CriarColunasGrid()
            CarregaComboBuscaCategorias(0)
            CarregaListaTelefonica(0)


            CarregaComboCidades(contribuinteCarregado.Cod_Cidade)
            CarregaComboCategorias(contribuinteCarregado.Cod_Categoria)


            atualizou = True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CarregaComboCategorias(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCategoria.DataSource = Nothing
            cboCategoria.Items.Clear()
            cboCategoria.DisplayMember = "Nome_Categoria"
            cboCategoria.ValueMember = "Cod_Categoria"
            cboCategoria.DataSource = rn_generico.BuscarCategoria(0)

            If cboCategoria.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoria.SelectedIndex = 0
            ElseIf cboCategoria.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoria.Items.Count - 1
                    For Each linha As CGenerico In cboCategoria.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoria.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoria.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboBuscaCategorias(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboBuscaCategoria.DataSource = Nothing
            cboBuscaCategoria.Items.Clear()
            cboBuscaCategoria.DisplayMember = "Nome_Categoria"
            cboBuscaCategoria.ValueMember = "Cod_Categoria"
            cboBuscaCategoria.DataSource = rn_generico.BuscarCategoria(0)

            If cboBuscaCategoria.Items.Count > 0 And cod_categoria = 0 Then
                cboBuscaCategoria.SelectedIndex = 0
            ElseIf cboBuscaCategoria.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboBuscaCategoria.Items.Count - 1
                    For Each linha As CGenerico In cboBuscaCategoria.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboBuscaCategoria.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboBuscaCategoria.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboContribuinte()
        Dim rn_contribuinte As RNContribuinte = New RNContribuinte
        Dim filtro As CContribuinte = New CContribuinte

        cboNome.DataSource = Nothing
        cboNome.Items.Clear()
        cboNome.DisplayMember = "NomeCliente1"
        cboNome.ValueMember = "Cod_cliente"
        cboNome.DataSource = rn_contribuinte.CarregarGrid(filtro, "Contribuinte")

        If cboNome.Items.Count > 0 Then
            cboNome.SelectedIndex = 0
        End If


    End Sub

    Private Sub CarregaComboCidades(ByVal cod_cidade As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCidade.DataSource = Nothing
            cboCidade.Items.Clear()
            cboCidade.DisplayMember = "Nome_cidade"
            cboCidade.ValueMember = "Cod_Cidade"
            cboCidade.DataSource = rn_generico.BuscarCidades(0)

            If cboCidade.Items.Count = 0 Then
                cboCidade.SelectedIndex = 0
            ElseIf cboCidade.Items.Count > 0 And cod_cidade <> 0 Then

                For i As Integer = 0 To cboCidade.Items.Count - 1
                    For Each linha As CGenerico In cboCidade.DataSource
                        If linha.Cod_Cidade = cod_cidade Then
                            cboCidade.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next

            Else

                cboCidade.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub btnExportar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        ExportarParaExcel(grdListaTelefonica, "teste")
    End Sub

    Public Sub ExportarParaExcel(ByVal dgvName As DataGridView, Optional ByVal fileName As String = "")
        ' Criar uma nova instância do Excel 
        Dim objExcelApp As New Excel.Application()
        Dim objExcelBook As Excel.Workbook
        Dim objExcelSheet As Excel.Worksheet
        Try

            ' Altera o tipo/localização para Inglês. Existe incompatibilidade  
            ' entre algumas versões de Excel vs Sistema Operativo 
            Dim oldCI As CultureInfo = CurrentThread.CurrentCulture
            CurrentThread.CurrentCulture = New CultureInfo("en-US")
            ' Adiciona um workbook e activa a worksheet actual 
            objExcelBook = objExcelApp.Workbooks.Add
            objExcelSheet = CType(objExcelBook.Worksheets(1), Excel.Worksheet)
            ' Ciclo nos cabeçalhos para escrever os títulos a bold/negrito 
            Dim dgvColumnIndex As Int16 = 1
            For Each col As DataGridViewColumn In dgvName.Columns
                objExcelSheet.Cells(1, dgvColumnIndex) = col.HeaderText
                objExcelSheet.Cells(1, dgvColumnIndex).Font.Bold = True
                dgvColumnIndex += 1
            Next
            ' Ciclo nas linhas/células 
            Dim dgvRowIndex As Integer = 2

            For Each row As DataGridViewRow In dgvName.Rows
                Dim dgvCellIndex As Integer = 1

                For Each cell As DataGridViewCell In row.Cells
                    objExcelSheet.Cells(dgvRowIndex, dgvCellIndex) = cell.Value
                    dgvCellIndex += 1
                Next
                dgvRowIndex += 1
            Next
            ' Ajusta o largura das colunas automaticamente 
            objExcelSheet.Columns.AutoFit()
            ' Caso a opção seja gravar (xlsSaveAs) grava o ficheiro e fecha 
            ' o Workbook/Excel. Caso contrário (xlsOpen) abre o Excel 

            objExcelBook.SaveAs("C:\AACI\teste.xlsx")
            objExcelBook.Close()
            objExcelApp.Quit()
            MessageBox.Show("Ficheiro exportado com sucesso para: " & fileName)


            ' Altera a tipo/localização para actual 
            CurrentThread.CurrentCulture = oldCI
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        Finally
            objExcelSheet = Nothing
            objExcelBook = Nothing
            objExcelApp = Nothing
            ' O GC(garbage collector) recolhe a memória não usada pelo sistema.  
            ' O método Collect() força a recolha e a opção WaitForPendingFinalizers  
            ' espera até estar completo. Desta forma o EXCEL.EXE não fica no  
            ' Task Manager(gestor tarefas) ocupando memória desnecessariamente 
            ' (devem ser chamados duas vezes para maior garantia) 
            GC.Collect()
            GC.WaitForPendingFinalizers()
            GC.Collect()
            GC.WaitForPendingFinalizers()
        End Try


    End Sub



    Private Sub CarregaListaTelefonica(ByVal categoria As Integer)
        Dim rn As RNGenerico = New RNGenerico
        Dim valor As Single
        Dim strValor As String

        Try

            If cboBuscaCategoria.SelectedIndex = 0 Then
                grdListaTelefonica.DataSource = rn.BuscaListaTelefonica(0)
                lblTotalFichas.Text = rn.BuscaQuantidadeListaTelefonica(0)

                strValor = valor.ToString("C")
                valor = IIf(IsDBNull(rn.BuscaValorListaTelefonica(categoria)), 0, rn.BuscaValorListaTelefonica(0))
                lblValor.Text = valor.ToString("R$ #,###.00")
            Else
                grdListaTelefonica.DataSource = rn.BuscaListaTelefonica(categoria)
                lblTotalFichas.Text = rn.BuscaQuantidadeListaTelefonica(categoria)

                strValor = valor.ToString("C")
                valor = IIf(IsDBNull(rn.BuscaValorListaTelefonica(categoria)), 0, rn.BuscaValorListaTelefonica(categoria))
                lblValor.Text = valor.ToString("R$ #,###.00")

            End If

        Catch ex As Exception

        End Try


    End Sub


    'Private Sub FrmConsultaUltimaMovimentacao_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
    '    Dim rnretorno As New RNRetorno
    '    Dim situacao As String = ""

    '    Try
    '        If atualizou = False Then
    '            If cboCategoria.SelectedIndex = 0 Or cboCategoria.Text = "Selecione..." Then
    '                Exit Sub
    '            End If

    '            Me.Cursor = Cursors.WaitCursor

    '            carregaFeedGridCategoriaContribuinte(cboCategoria.SelectedValue, CodUsuario)


    '            Me.Cursor = Cursors.Default
    '            atualizou = True
    '        End If

    '    Catch ex As Exception

    '        ' Show the exception's message.
    '       MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

    '    End Try
    'End Sub



    Private Sub txtNome_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtBuscaNome.TextChanged
        Dim texto As String = Nothing
        Dim linhaVazia As Integer = 0
        Dim contador As Integer = 0

        If txtBuscaNome.Text <> String.Empty Then

            linhaVazia = grdListaTelefonica.Rows.Count - 1

            'percorre cada linha do DataGridView
            For Each linha As DataGridViewRow In grdListaTelefonica.Rows

                'percorre cada célula da linha
                For Each celula As DataGridViewCell In grdListaTelefonica.Rows(linha.Index).Cells
                    If Not contador = linhaVazia Then
                        'se a coluna for a coluna 2 (Nome) então verifica o criterio
                        If celula.ColumnIndex = 2 Then

                            If celula.Value.ToString <> "" Then
                                texto = IIf(celula.Value.ToString = Nothing, "", celula.Value.ToString)
                                'se o texto informado estiver contido na célula então seleciona toda linha
                                If texto.Contains(txtBuscaNome.Text) Then
                                    'seleciona a linha
                                    grdListaTelefonica.CurrentCell = celula
                                    grdListaTelefonica.CurrentCell = grdListaTelefonica.Rows(celula.RowIndex).Cells(2)
                                    Exit Sub
                                End If
                            End If
                            contador = contador + 1
                        End If
                    End If

                Next

            Next

        End If

    End Sub

    Private Sub txtTelefone_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtBuscaTelefone.TextChanged
        Dim texto As String = Nothing
        Dim linhaVazia As Integer = 0
        Dim contador As Integer = 0
        Dim DDD As String = ""

        If txtBuscaTelefone.Text <> String.Empty Then

            linhaVazia = grdListaTelefonica.Rows.Count - 1

            'percorre cada linha do DataGridView
            For Each linha As DataGridViewRow In grdListaTelefonica.Rows

                'percorre cada célula da linha
                For Each celula As DataGridViewCell In grdListaTelefonica.Rows(linha.Index).Cells
                    If Not contador = linhaVazia Then

                        If celula.ColumnIndex = 0 Then
                            DDD = celula.Value.ToString()
                        End If
                        'se a coluna for a coluna 1 (telefone) então verifica o criterio
                        If celula.ColumnIndex = 1 Then

                            If celula.Value.ToString <> "" Then
                                texto = IIf(celula.Value.ToString = Nothing, "", celula.Value.ToString)
                                'se o texto informado estiver contido na célula então seleciona toda linha
                                If texto.Contains(txtBuscaTelefone.Text) And DDD.Contains(txtDDDPesquisar.Text) Then
                                    'seleciona a linha
                                    grdListaTelefonica.CurrentCell = celula
                                    grdListaTelefonica.CurrentCell = grdListaTelefonica.Rows(celula.RowIndex).Cells(2)
                                    Exit Sub
                                End If
                            End If
                            contador = contador + 1
                        End If
                    End If

                Next

            Next

        End If
    End Sub

    Private Sub grdListaTelefonica_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdListaTelefonica.CellContentClick

        Dim DDDSelecionado As String = ""
        Dim TelefoneSelecionado As String = ""

        Dim rn As RNGenerico = New RNGenerico
        Dim lista As CListaTelefonica = New CListaTelefonica
        Dim ds As DataSet

        Try

            DDDSelecionado = CType(grdListaTelefonica.Rows(e.RowIndex).Cells(ColunasGridHistorico.DDD).Value, Integer)
            TelefoneSelecionado = CType(grdListaTelefonica.Rows(e.RowIndex).Cells(ColunasGridHistorico.Telefone).Value, Integer)
            ListaTelefonicaId = CType(grdListaTelefonica.Rows(e.RowIndex).Cells(ColunasGridHistorico.lista_telefonica_Id).Value, Integer)


            For Each ContribuinteInconsistencia As CListaTelefonica In lstListaTelefonica
                If ContribuinteInconsistencia.Telefone = TelefoneSelecionado Then

                End If
            Next


            If (e.RowIndex > -1) Then

                lstListaTelefonica = New SortableBindingList(Of CListaTelefonica)(rn.Retorna_HistoricoSimplificado(TelefoneSelecionado, DDDSelecionado))

                If (lstListaTelefonica.Count > 0) Then

                    bindingSourceContribuite.DataSource = lstListaTelefonica

                    GrdHistorico.DataSource = bindingSourceContribuite.DataSource

                Else
                    GrdHistorico.DataSource = Nothing

                End If


                ds = rn.Busca_Informações_Lista_Telefonica(TelefoneSelecionado, DDDSelecionado)

                If ds.Tables(0).Rows.Count > 0 Then
                    For Each linha As DataRow In ds.Tables(0).Rows
                        txtCod_Ean.Text = IIf(IsDBNull(linha("cod_ean")), "", linha("cod_ean"))
                        txtNome.Text = IIf(IsDBNull(linha("nome_cliente")), "", linha("nome_cliente"))
                        txtDDD.Text = IIf(IsDBNull(linha("DDD")), "", linha("DDD"))
                        txtTelefone.Text = IIf(IsDBNull(linha("telefone")), "", linha("telefone"))
                        txtValor.Text = IIf(IsDBNull(linha("valor")), "", linha("valor"))
                        CarregaComboCidades(IIf(IsDBNull(linha("Cod_Cidade")), 0, linha("Cod_Cidade")))
                        CarregaComboCategorias(IIf(IsDBNull(linha("Categoria")), 0, linha("Categoria")))

                    Next

                End If


            End If


            contribuinte = rnContribuinte.PesquisarContribuintePorTelefoneSimplificado(TelefoneSelecionado, DDDSelecionado, 1)


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub btnSalvar_Click(sender As System.Object, e As System.EventArgs) Handles btnSalvar.Click
        Dim rn As RNGenerico = New RNGenerico
        Dim lista As CListaTelefonica = New CListaTelefonica

        Try

            'ATRIBUI OS VALORES AO OBJETO
            lista.Cod_cliente = txtCod_Ean.Text
            lista.Nome_cliente = txtNome.Text
            lista.DDD = txtDDD.Text
            lista.Telefone = txtTelefone.Text
            lista.Valor = txtValor.Text
            lista.Categoria = cboCategoria.SelectedValue
            lista.Cod_cidade = cboCidade.SelectedValue
            lista.Lista_telefonica_id = ListaTelefonicaId


            If (rn.Atualiza_Lista_Telefonica(lista)) = True Then
                MsgBox("Informações Atualizadas com sucesso!")
                CarregaListaTelefonica(0)
            Else
                MsgBox("Ocorreu um erro ao atualizar as informações")
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub



End Class