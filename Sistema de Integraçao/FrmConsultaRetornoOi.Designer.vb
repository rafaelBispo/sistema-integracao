﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConsultaRetornoOi
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboTipoCritica = New System.Windows.Forms.ComboBox()
        Me.dtConsulta = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnPesquisar = New System.Windows.Forms.Button()
        Me.grdHistorioOi = New System.Windows.Forms.DataGridView()
        Me.lblValor = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblTotalFichas = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.TabInformacoes = New System.Windows.Forms.TabControl()
        Me.tabHistorico = New System.Windows.Forms.TabPage()
        Me.GrdHistorico = New System.Windows.Forms.DataGridView()
        Me.tbHistoricoOi = New System.Windows.Forms.TabPage()
        Me.gridHistoricoOi = New System.Windows.Forms.DataGridView()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.grdExclusaoAuto = New System.Windows.Forms.DataGridView()
        Me.lblValorCriticasAuto = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblValorExclusaoManual = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.grdHistorioOi, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabInformacoes.SuspendLayout()
        Me.tabHistorico.SuspendLayout()
        CType(Me.GrdHistorico, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tbHistoricoOi.SuspendLayout()
        CType(Me.gridHistoricoOi, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.grdExclusaoAuto, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.cboTipoCritica)
        Me.Panel1.Controls.Add(Me.dtConsulta)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.btnPesquisar)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1103, 56)
        Me.Panel1.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(183, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(94, 16)
        Me.Label2.TabIndex = 138
        Me.Label2.Text = "Tipo de Critica"
        '
        'cboTipoCritica
        '
        Me.cboTipoCritica.FormattingEnabled = True
        Me.cboTipoCritica.Items.AddRange(New Object() {"Todos", "Automatica", "Manual"})
        Me.cboTipoCritica.Location = New System.Drawing.Point(283, 15)
        Me.cboTipoCritica.Name = "cboTipoCritica"
        Me.cboTipoCritica.Size = New System.Drawing.Size(189, 21)
        Me.cboTipoCritica.TabIndex = 137
        '
        'dtConsulta
        '
        Me.dtConsulta.CustomFormat = "MM/yyyy"
        Me.dtConsulta.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtConsulta.Location = New System.Drawing.Point(59, 15)
        Me.dtConsulta.Name = "dtConsulta"
        Me.dtConsulta.Size = New System.Drawing.Size(108, 20)
        Me.dtConsulta.TabIndex = 72
        Me.dtConsulta.Value = New Date(2017, 12, 18, 0, 0, 0, 0)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(17, 15)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(36, 16)
        Me.Label3.TabIndex = 71
        Me.Label3.Text = "Data"
        '
        'btnPesquisar
        '
        Me.btnPesquisar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPesquisar.Location = New System.Drawing.Point(991, 8)
        Me.btnPesquisar.Name = "btnPesquisar"
        Me.btnPesquisar.Size = New System.Drawing.Size(95, 37)
        Me.btnPesquisar.TabIndex = 67
        Me.btnPesquisar.Text = "Pesquisar"
        Me.btnPesquisar.UseVisualStyleBackColor = True
        '
        'grdHistorioOi
        '
        Me.grdHistorioOi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdHistorioOi.Location = New System.Drawing.Point(5, 6)
        Me.grdHistorioOi.Name = "grdHistorioOi"
        Me.grdHistorioOi.Size = New System.Drawing.Size(1085, 305)
        Me.grdHistorioOi.TabIndex = 88
        '
        'lblValor
        '
        Me.lblValor.AutoSize = True
        Me.lblValor.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValor.Location = New System.Drawing.Point(970, 449)
        Me.lblValor.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblValor.Name = "lblValor"
        Me.lblValor.Size = New System.Drawing.Size(15, 16)
        Me.lblValor.TabIndex = 95
        Me.lblValor.Text = "0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(970, 427)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(92, 16)
        Me.Label5.TabIndex = 94
        Me.Label5.Text = "Valor Total :"
        '
        'lblTotalFichas
        '
        Me.lblTotalFichas.AutoSize = True
        Me.lblTotalFichas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalFichas.Location = New System.Drawing.Point(829, 449)
        Me.lblTotalFichas.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblTotalFichas.Name = "lblTotalFichas"
        Me.lblTotalFichas.Size = New System.Drawing.Size(15, 16)
        Me.lblTotalFichas.TabIndex = 93
        Me.lblTotalFichas.Text = "0"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(826, 427)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(119, 16)
        Me.Label6.TabIndex = 92
        Me.Label6.Text = "Total de Fichas:"
        '
        'TabInformacoes
        '
        Me.TabInformacoes.Controls.Add(Me.tabHistorico)
        Me.TabInformacoes.Controls.Add(Me.tbHistoricoOi)
        Me.TabInformacoes.Location = New System.Drawing.Point(16, 468)
        Me.TabInformacoes.Name = "TabInformacoes"
        Me.TabInformacoes.SelectedIndex = 0
        Me.TabInformacoes.Size = New System.Drawing.Size(929, 333)
        Me.TabInformacoes.TabIndex = 96
        Me.TabInformacoes.Visible = False
        '
        'tabHistorico
        '
        Me.tabHistorico.Controls.Add(Me.GrdHistorico)
        Me.tabHistorico.Location = New System.Drawing.Point(4, 22)
        Me.tabHistorico.Name = "tabHistorico"
        Me.tabHistorico.Padding = New System.Windows.Forms.Padding(3)
        Me.tabHistorico.Size = New System.Drawing.Size(921, 307)
        Me.tabHistorico.TabIndex = 0
        Me.tabHistorico.Text = "Histórico"
        Me.tabHistorico.UseVisualStyleBackColor = True
        '
        'GrdHistorico
        '
        Me.GrdHistorico.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GrdHistorico.Location = New System.Drawing.Point(6, 6)
        Me.GrdHistorico.Name = "GrdHistorico"
        Me.GrdHistorico.Size = New System.Drawing.Size(908, 295)
        Me.GrdHistorico.TabIndex = 3
        '
        'tbHistoricoOi
        '
        Me.tbHistoricoOi.Controls.Add(Me.gridHistoricoOi)
        Me.tbHistoricoOi.Location = New System.Drawing.Point(4, 22)
        Me.tbHistoricoOi.Name = "tbHistoricoOi"
        Me.tbHistoricoOi.Padding = New System.Windows.Forms.Padding(3)
        Me.tbHistoricoOi.Size = New System.Drawing.Size(921, 307)
        Me.tbHistoricoOi.TabIndex = 3
        Me.tbHistoricoOi.Text = "Histórico Oi"
        Me.tbHistoricoOi.UseVisualStyleBackColor = True
        '
        'gridHistoricoOi
        '
        Me.gridHistoricoOi.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridHistoricoOi.Location = New System.Drawing.Point(6, 5)
        Me.gridHistoricoOi.Name = "gridHistoricoOi"
        Me.gridHistoricoOi.Size = New System.Drawing.Size(908, 295)
        Me.gridHistoricoOi.TabIndex = 4
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Location = New System.Drawing.Point(12, 74)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1103, 348)
        Me.TabControl1.TabIndex = 97
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.grdHistorioOi)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1095, 322)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Criticados Automaticos"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.grdExclusaoAuto)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1095, 322)
        Me.TabPage2.TabIndex = 3
        Me.TabPage2.Text = "Exclusões Manual"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'grdExclusaoAuto
        '
        Me.grdExclusaoAuto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdExclusaoAuto.Location = New System.Drawing.Point(4, 6)
        Me.grdExclusaoAuto.Name = "grdExclusaoAuto"
        Me.grdExclusaoAuto.Size = New System.Drawing.Size(1085, 304)
        Me.grdExclusaoAuto.TabIndex = 89
        '
        'lblValorCriticasAuto
        '
        Me.lblValorCriticasAuto.AutoSize = True
        Me.lblValorCriticasAuto.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorCriticasAuto.Location = New System.Drawing.Point(21, 449)
        Me.lblValorCriticasAuto.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblValorCriticasAuto.Name = "lblValorCriticasAuto"
        Me.lblValorCriticasAuto.Size = New System.Drawing.Size(15, 16)
        Me.lblValorCriticasAuto.TabIndex = 99
        Me.lblValorCriticasAuto.Text = "0"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(18, 427)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(214, 16)
        Me.Label4.TabIndex = 98
        Me.Label4.Text = "Total de Criticas Automáticas:"
        '
        'lblValorExclusaoManual
        '
        Me.lblValorExclusaoManual.AutoSize = True
        Me.lblValorExclusaoManual.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorExclusaoManual.Location = New System.Drawing.Point(274, 449)
        Me.lblValorExclusaoManual.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblValorExclusaoManual.Name = "lblValorExclusaoManual"
        Me.lblValorExclusaoManual.Size = New System.Drawing.Size(15, 16)
        Me.lblValorExclusaoManual.TabIndex = 101
        Me.lblValorExclusaoManual.Text = "0"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(271, 427)
        Me.Label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(206, 16)
        Me.Label8.TabIndex = 100
        Me.Label8.Text = "Total de Exclusões Manuais:"
        '
        'FrmConsultaRetornoOi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1116, 489)
        Me.Controls.Add(Me.lblValorExclusaoManual)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.lblValorCriticasAuto)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.TabInformacoes)
        Me.Controls.Add(Me.lblValor)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblTotalFichas)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "FrmConsultaRetornoOi"
        Me.Text = "Consulta Contestado e Criticados Oi"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.grdHistorioOi, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabInformacoes.ResumeLayout(False)
        Me.tabHistorico.ResumeLayout(False)
        CType(Me.GrdHistorico, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tbHistoricoOi.ResumeLayout(False)
        CType(Me.gridHistoricoOi, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        CType(Me.grdExclusaoAuto, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label2 As Label
    Friend WithEvents cboTipoCritica As ComboBox
    Friend WithEvents dtConsulta As DateTimePicker
    Friend WithEvents Label3 As Label
    Friend WithEvents btnPesquisar As Button
    Friend WithEvents grdHistorioOi As DataGridView
    Friend WithEvents lblValor As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents lblTotalFichas As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents TabInformacoes As TabControl
    Friend WithEvents tabHistorico As TabPage
    Friend WithEvents GrdHistorico As DataGridView
    Friend WithEvents tbHistoricoOi As TabPage
    Friend WithEvents gridHistoricoOi As DataGridView
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents grdExclusaoAuto As DataGridView
    Friend WithEvents lblValorCriticasAuto As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents lblValorExclusaoManual As Label
    Friend WithEvents Label8 As Label
End Class
