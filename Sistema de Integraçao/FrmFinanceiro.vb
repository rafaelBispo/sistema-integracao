﻿Imports SISINT001
Imports System.Text
Imports Microsoft.Office.Interop
Imports Util
Imports System.IO
Imports System.Reflection

Public Class FrmFinanceiro

    Private lstResumoRequisicao As New SortableBindingList(Of CGenerico)
    Private lstResumoRequisicaoCategoria As New SortableBindingList(Of CGenerico)
    Private lstOperador As New SortableBindingList(Of CGenerico)
    Private lstOperadorSelecionado As New List(Of CGenerico)
    Public bindingSourceRequisicao As BindingSource = New BindingSource

    Private Enum ColunasGridRequisicao

        Operador = 0
        qtd_fichas_confirmadas = 1
        valor_fichas_confirmadas = 2
        qtd_fichas_recusadas = 3
        valor_fichas_recusadas = 4
    End Enum


    Private Sub FrmFinanceiro_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            btnSeparaFichas.Enabled = False

            CriarColunasGridRequisicao()
            CriarColunasGridRequisicaoCategoria()
            CarregaComboCategorias(0)
            CarregaComboOperador(0)
            DtMesAnoPesquisa.Value = Date.Now
            CarregaComboCidades(0)

            CarregaResumoRequisicao(DtMesAnoPesquisa.Text)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub CriarColunasGridRequisicao()

        Try

            gridRequisicao.DataSource = Nothing
            gridRequisicao.Columns.Clear()

            gridRequisicao.AutoGenerateColumns = False

            gridRequisicao.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            'Dim checkboxColumn As New DataGridViewCheckBoxColumn
            'checkboxColumn.Width = 32
            'checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "operador"
            column.HeaderText = "Operador"
            column.Width = 120
            column.ReadOnly = True
            gridRequisicao.Columns.Add(column)

            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "QuantidadeConfirmado"
            column.HeaderText = "Quantidade de fichas confirmadas"
            column.Width = 120
            column.ReadOnly = True
            gridRequisicao.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "ValorConfirmado"
            column.HeaderText = "Valor das fichas confirmadas"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            gridRequisicao.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "QuantidadeRecusado"
            column.HeaderText = "Quantidade de fichas recusadas"
            column.Width = 120
            column.ReadOnly = True
            column.Visible = True
            gridRequisicao.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "ValorRecusado"
            column.HeaderText = "Valor das fichas recusadas"
            column.Width = 120
            column.ReadOnly = True
            column.Visible = True
            gridRequisicao.Columns.Add(column)


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CriarColunasGridRequisicaoCategoria()

        Try

            gridRequisicaoCategoria.DataSource = Nothing
            gridRequisicaoCategoria.Columns.Clear()

            gridRequisicaoCategoria.AutoGenerateColumns = False

            gridRequisicaoCategoria.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            'Dim checkboxColumn As New DataGridViewCheckBoxColumn
            'checkboxColumn.Width = 32
            'checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Nome_Categoria"
            column.HeaderText = "Categoria"
            column.Width = 120
            column.ReadOnly = True
            gridRequisicaoCategoria.Columns.Add(column)

            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "QuantidadeConfirmado"
            column.HeaderText = "Quantidade de fichas na requisição"
            column.Width = 120
            column.ReadOnly = True
            gridRequisicaoCategoria.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "ValorConfirmado"
            column.HeaderText = "Valor das fichas na requisição"
            column.DefaultCellStyle.Format = "R$0.00"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            gridRequisicaoCategoria.Columns.Add(column)


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub


    Private Sub CarregaComboCategorias(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try



            cboCategoria.DataSource = Nothing
            cboCategoria.Items.Clear()
            cboCategoria.DisplayMember = "Nome_Categoria"
            cboCategoria.ValueMember = "Cod_Categoria"
            cboCategoria.DataSource = rn_generico.BuscarCategoria(0)

            If cboCategoria.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoria.SelectedIndex = 0
            ElseIf cboCategoria.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoria.Items.Count - 1
                    For Each linha As CGenerico In cboCategoria.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoria.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoria.SelectedIndex = 1
            End If

            cboCategoriaResetar.DataSource = Nothing
            cboCategoriaResetar.Items.Clear()
            cboCategoriaResetar.DisplayMember = "Nome_Categoria"
            cboCategoriaResetar.ValueMember = "Cod_Categoria"
            cboCategoriaResetar.DataSource = rn_generico.BuscarCategoria(0)

            If cboCategoriaResetar.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoriaResetar.SelectedIndex = 0
            ElseIf cboCategoriaResetar.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoriaResetar.Items.Count - 1
                    For Each linha As CGenerico In cboCategoriaResetar.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoriaResetar.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoriaResetar.SelectedIndex = 1
            End If

            cbCategoriaTrocar.DataSource = Nothing
            cbCategoriaTrocar.Items.Clear()
            cbCategoriaTrocar.DisplayMember = "Nome_Categoria"
            cbCategoriaTrocar.ValueMember = "Cod_Categoria"
            cbCategoriaTrocar.DataSource = rn_generico.BuscarCategoria(0)

            If cbCategoriaTrocar.Items.Count > 0 And cod_categoria = 0 Then
                cbCategoriaTrocar.SelectedIndex = 0
            ElseIf cbCategoriaTrocar.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cbCategoriaTrocar.Items.Count - 1
                    For Each linha As CGenerico In cbCategoriaTrocar.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cbCategoriaTrocar.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cbCategoriaTrocar.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Try
            Dim rnretorno As New RNRetorno
            Dim situacao As String = ""
            If cboCategoria.SelectedIndex = 0 Then
                MsgBox("Opção invalida, selecionar a situação!", MsgBoxStyle.Critical, "Atenção!")
                Exit Sub
            End If

            If (cboCategoria.Text.Equals("LISTA NOVA")) Then

                If cboCidade.SelectedIndex = 0 Or cboCidade.Text.Equals("Selecione...") Then
                    MsgBox("Opção invalida, selecionar a cidade!", MsgBoxStyle.Critical, "Atenção!")
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If

            End If

            Me.Cursor = Cursors.WaitCursor

            If (cboCategoria.Text.Equals("LISTA NOVA")) Then
                carregaFeedGridCategoriaContribuinteListaNovaOi(cboCategoria.SelectedValue, DtMesAnoPesquisa.Text, cboCidade.Text)
            Else
                carregaFeedGridCategoriaContribuinte(cboCategoria.SelectedValue, DtMesAnoPesquisa.Text)
            End If

            CarregaResumoRequisicao(DtMesAnoPesquisa.Text)

            btnSeparaFichas.Enabled = True

            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub carregaFeedGridCategoriaContribuinte(ByVal categoria As String, ByVal mesRef As String)

        Dim objRetorno As CRetorno = New CRetorno()
        Dim rn As RNRetorno = New RNRetorno()
        Dim bindingSourceApolice As BindingSource = New BindingSource
        Dim ds As DataSet = New DataSet
        Dim valor As Single
        Dim strValor As String

        Try

            ds = rn.RetornaDadosPorCategoria(categoria, DtMesAnoPesquisa.Text)

            If ds.Tables(0).Rows.Count > 0 Then
                lblCategoria.Text = ds.Tables(0).Rows(0).Item(0)
            End If

            If ds.Tables(1).Rows.Count > 0 Then
                lblTotalFichas.Text = ds.Tables(1).Rows(0).Item(0)
                txtQtdFichas.Text = ds.Tables(1).Rows(0).Item(0)
            End If

            If ds.Tables(2).Rows.Count > 0 Then
                strValor = valor.ToString("C")
                valor = IIf(IsDBNull(ds.Tables(2).Rows(0).Item(0)), 0, ds.Tables(2).Rows(0).Item(0))
                lblValorTotal.Text = valor.ToString("R$ #,###.00")
            End If

            If ds.Tables(3).Rows.Count > 0 Then
                lblFichaConfirmada.Text = ds.Tables(3).Rows(0).Item(0)
            End If

            If ds.Tables(4).Rows.Count > 0 Then
                strValor = valor.ToString("C")
                valor = IIf(IsDBNull(ds.Tables(4).Rows(0).Item(0)), 0, ds.Tables(4).Rows(0).Item(0))
                lblValorConfirmado.Text = valor.ToString("R$ #,###.00")
            End If

            If ds.Tables(5).Rows.Count > 0 Then
                lblFichaRecusada.Text = ds.Tables(5).Rows(0).Item(0)
            End If

            If ds.Tables(6).Rows.Count > 0 Then
                strValor = valor.ToString("C")
                valor = IIf(IsDBNull(ds.Tables(6).Rows(0).Item(0)), 0, ds.Tables(6).Rows(0).Item(0))
                lblValorRecusado.Text = valor.ToString("R$ #,###.00")
            End If

            If ds.Tables(7).Rows.Count > 0 Then
                lblFichaAberto.Text = ds.Tables(7).Rows(0).Item(0)
            End If

            If ds.Tables(8).Rows.Count > 0 Then
                strValor = valor.ToString("C")
                valor = IIf(IsDBNull(ds.Tables(8).Rows(0).Item(0)), 0, ds.Tables(8).Rows(0).Item(0))
                lblValorAberto.Text = valor.ToString("R$ #,###.00")
            End If

            txtFichasDisponiveis.Text = rn.RetornaFichasDisponiveis(categoria)
            txtFichasBloqueadas.Text = rn.RetornaFichasBloqueadas(categoria)
        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub carregaFeedGridCategoriaContribuinteListaNovaOi(ByVal categoria As String, ByVal mesRef As String, ByVal Cidade As String)

        Dim objRetorno As CRetorno = New CRetorno()
        Dim rn As RNRetorno = New RNRetorno()
        Dim bindingSourceApolice As BindingSource = New BindingSource
        Dim ds As DataSet = New DataSet
        Dim valor As Single
        Dim strValor As String

        Try

            ds = rn.RetornaDadosPorCategoriaListaNovaOi(categoria, mesRef, Cidade)

            If ds.Tables(0).Rows.Count > 0 Then
                lblCategoria.Text = ds.Tables(0).Rows(0).Item(0)
            End If

            If ds.Tables(1).Rows.Count > 0 Then
                lblTotalFichas.Text = ds.Tables(1).Rows(0).Item(0)
                txtQtdFichas.Text = ds.Tables(1).Rows(0).Item(0)
            End If

            If ds.Tables(2).Rows.Count > 0 Then
                strValor = valor.ToString("C")
                valor = IIf(IsDBNull(ds.Tables(2).Rows(0).Item(0)), 0, ds.Tables(2).Rows(0).Item(0))
                lblValorTotal.Text = valor.ToString("R$ #,###.00")
            End If

            If ds.Tables(3).Rows.Count > 0 Then
                lblFichaConfirmada.Text = ds.Tables(3).Rows(0).Item(0)
            End If

            If ds.Tables(4).Rows.Count > 0 Then
                strValor = valor.ToString("C")
                valor = IIf(IsDBNull(ds.Tables(4).Rows(0).Item(0)), 0, ds.Tables(4).Rows(0).Item(0))
                lblValorConfirmado.Text = valor.ToString("R$ #,###.00")
            End If

            If ds.Tables(5).Rows.Count > 0 Then
                lblFichaRecusada.Text = ds.Tables(5).Rows(0).Item(0)
            End If

            If ds.Tables(6).Rows.Count > 0 Then
                strValor = valor.ToString("C")
                valor = IIf(IsDBNull(ds.Tables(6).Rows(0).Item(0)), 0, ds.Tables(6).Rows(0).Item(0))
                lblValorRecusado.Text = valor.ToString("R$ #,###.00")
            End If

            If ds.Tables(7).Rows.Count > 0 Then
                lblFichaAberto.Text = ds.Tables(7).Rows(0).Item(0)
            End If

            If ds.Tables(8).Rows.Count > 0 Then
                strValor = valor.ToString("C")
                valor = IIf(IsDBNull(ds.Tables(8).Rows(0).Item(0)), 0, ds.Tables(8).Rows(0).Item(0))
                lblValorAberto.Text = valor.ToString("R$ #,###.00")
            End If

            txtFichasDisponiveis.Text = rn.RetornaFichasDisponiveisListaNovaOi(categoria, Cidade)


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub


    Private Sub CarregaResumoRequisicao(ByVal mesRef As String)

        Dim objRetorno As CRetorno = New CRetorno()
        Dim rn As RNGenerico = New RNGenerico()
        Dim bindingSourceApolice As BindingSource = New BindingSource
        Dim ds As DataSet = New DataSet

        Dim valor As Single
        Dim strValor As String

        Try

            ds = rn.RetornaRequisicao()

            If ds.Tables(0).Rows.Count > 0 Then

                lblQuantidadeReq.Text = ds.Tables(0).Rows(0).Item(0)

                strValor = valor.ToString("C")
                valor = IIf(IsDBNull(ds.Tables(0).Rows(0).Item(1)), 0, ds.Tables(0).Rows(0).Item(1))
                lblValorReq.Text = valor.ToString("R$ #,###.00")

            End If


            lstResumoRequisicaoCategoria = New SortableBindingList(Of CGenerico)(rn.RetornaRequisicaoPorCategoria())

            If (lstResumoRequisicaoCategoria.Count > 0) Then

                bindingSourceRequisicao.DataSource = lstResumoRequisicaoCategoria

                gridRequisicaoCategoria.DataSource = bindingSourceRequisicao.DataSource

            End If


            lstResumoRequisicao = New SortableBindingList(Of CGenerico)(rn.RetornaResumoRequisicao(mesRef))

            If (lstResumoRequisicao.Count > 0) Then

                bindingSourceRequisicao.DataSource = lstResumoRequisicao

                gridRequisicao.DataSource = bindingSourceRequisicao.DataSource

            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub


    Private Sub GerarArquivoCSV(categoria As String, cod_categoria As Integer)
        Try
            Dim rn As RNRetorno = New RNRetorno()
            Dim ds As DataSet = rn.RetornaDadosCSVOi(cod_categoria)
            Dim textoarquivo As String = String.Empty
            If ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    textoarquivo += $"{dr.Item(0).ToString}{vbCrLf}"
                Next
                GeraCriarArquivo(textoarquivo, categoria)
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub GeraCriarArquivo(textoarquivo As String, categoria As String)
        Try
            Dim strNomeArquivo As String = $"Arquivo_CSV_{categoria}.txt"
            Dim caminhoExe As String = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
            Dim caminhoArquivo As String = Path.Combine(caminhoExe, strNomeArquivo)
            Dim rn As RNRetorno = New RNRetorno()
            'verifica se o arquivo existe
            If Not File.Exists(caminhoArquivo) Then
                ' Cria um arquivo para escrita
                Using sw As FileStream = File.Create(caminhoArquivo)
                    Dim texto As Byte() = New UTF8Encoding(True).GetBytes(textoarquivo)
                    sw.Write(texto, 0, texto.Length)
                End Using
                MsgBox($"Arquivo gerado com sucesso no caminho{caminhoArquivo}")
            Else
                MessageBox.Show("Ja existe um arquivo com esse nome ", "Informe o nome do arquivo", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If
            rn.AtualizaImpressaoFichas()
        Catch ex As Exception

        End Try
    End Sub


    Private Sub btnCalcular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim rn As RNGenerico = New RNGenerico()
        Dim ds As DataSet
        Dim valorOp1 As Decimal = 0
        Dim valorOp2 As Decimal = 0
        Dim valorOp3 As Decimal = 0
        Dim valorOp4 As Decimal = 0
        Dim valor As Single
        Dim strValor As String

        Try

            'PARA OS MENSAIS QUE TIVERAM A RECUSA DO ESPECIAL DEVE SE CONSIDERAR O VALOR INTEIRO
            ds = rn.NomeOperadorPorCategoria(3) '3 = mensal

            If ds.Tables(0).Rows.Count > 0 Then
                For Each linha As DataRow In ds.Tables(0).Rows

                    If lblOperador1.Text = linha.Item(0).ToString Then
                        valorOp1 = (CInt(lblOp1ValorConf.Text) + CInt(lblOp1ValorRecusada.Text)) * CInt(txtCoeficiente.Text) \ 100
                        strValor = valor.ToString("C")
                        valor = valorOp1
                        lblValComissaoOp1.Text = valor.ToString("R$ #,###.00")
                    End If
                    If lblOperador2.Text = linha.Item(0).ToString Then
                        valorOp2 = (CInt(lblOp2ValorConf.Text) + CInt(lblOp2ValorRecusada.Text)) * CInt(txtCoeficiente.Text) \ 100
                        strValor = valor.ToString("C")
                        valor = valorOp2
                        lblValComissaoOp2.Text = valor.ToString("R$ #,###.00")
                    End If
                    If lblOperador3.Text = linha.Item(0).ToString Then
                        valorOp3 = (CInt(lblOp3ValorConf.Text) + CInt(lblOp3ValorRecusada.Text)) * CInt(txtCoeficiente.Text) \ 100
                        strValor = valor.ToString("C")
                        valor = valorOp3
                        lblValComissaoOp3.Text = valor.ToString("R$ #,###.00")
                    End If
                    If lblOperador4.Text = linha.Item(0).ToString Then
                        valorOp4 = (CInt(lblOp4ValorConf.Text) + CInt(lblOp4ValorRecusada.Text)) * CInt(txtCoeficiente.Text) \ 100
                        strValor = valor.ToString("C")
                        valor = valorOp4
                        lblValComissaoOp4.Text = valor.ToString("R$ #,###.00")
                    End If

                Next

            End If

            If valorOp1 = 0 Then
                valorOp1 = CInt(lblOp1ValorConf.Text) * CInt(txtCoeficiente.Text) \ 100
                strValor = valor.ToString("C")
                valor = valorOp1
                lblValComissaoOp1.Text = valor.ToString("R$ #,###.00")
            End If

            If valorOp2 = 0 Then
                valorOp2 = CInt(lblOp2ValorConf.Text) * CInt(txtCoeficiente.Text) \ 100
                strValor = valor.ToString("C")
                valor = valorOp2
                lblValComissaoOp2.Text = valor.ToString("R$ #,###.00")
            End If

            If valorOp3 = 0 Then
                valorOp3 = CInt(lblOp3ValorConf.Text) * CInt(txtCoeficiente.Text) \ 100
                strValor = valor.ToString("C")
                valor = valorOp3
                lblValComissaoOp3.Text = valor.ToString("R$ #,###.00")
            End If

            If valorOp4 = 0 Then
                valorOp4 = CInt(lblOp4ValorConf.Text) * CInt(txtCoeficiente.Text) \ 100
                strValor = valor.ToString("C")
                valor = valorOp4
                lblValComissaoOp4.Text = valor.ToString("R$ #,###.00")
            End If






        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub




    Private Sub btnSeparaFichas_Click(sender As System.Object, e As System.EventArgs) Handles btnSeparaFichas.Click
        Dim rnGenerico As New RNGenerico
        Dim operador_id As Integer = 0
        Dim qdtFichas As Integer = 0
        Dim parte As Integer = 0
        Dim restaUm As Integer = 0
        'Dim dsOperador As DataSet

        Try
            Me.Cursor = Cursors.WaitCursor

            btnSeparaFichas.Enabled = False

            If (chkZera.Checked = False) Then

                If (cboCategoria.Text.Equals("LISTA NOVA")) Then

                    If cboCidade.SelectedIndex = 0 Or cboCidade.Text = "Selecione..." Then
                        MsgBox("Opção invalida, selecionar a cidade!", MsgBoxStyle.Critical, "Atenção!")
                        Me.Cursor = Cursors.Default
                        Exit Sub
                    End If

                End If

                If cboOperador.SelectedValue <= 0 Then
                    MsgBox("Por favor informe o operador.")
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If

                qdtFichas = CInt(txtFichasDisponiveis.Text)
                operador_id = cboOperador.SelectedValue

                If (cboCategoria.Text.Equals("LISTA NOVA")) Then
                    rnGenerico.SeparaFichasListaNovaOi(operador_id, cboCategoria.SelectedValue, qdtFichas, cboCidade.Text)
                Else
                    rnGenerico.SeparaFichas(operador_id, cboCategoria.SelectedValue, qdtFichas)
                End If



                MsgBox("Operação realizada com suceso!")
                Me.Cursor = Cursors.Default

            Else

                rnGenerico.ResetaFichasOi(cboCategoria.SelectedValue)
                MsgBox("Operação realizada com suceso!")
                Me.Cursor = Cursors.Default

            End If
            If Not chkZera.Checked Then
                'gera arquivo csv
                Try
                    Me.Cursor = Cursors.WaitCursor
                    MsgBox("O arquivo CSV, vai ser gerado, por favor aguarde a conclusão")
                    GeraCSV()
                    Me.Cursor = Cursors.Default

                Catch ex As Exception
                    MsgBox("Erro ao gerar Arquivo CSV. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
                    Me.Cursor = Cursors.Default
                End Try

            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboOperador(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False


        Try



            cboOperador.DataSource = Nothing
            cboOperador.Items.Clear()
            cboOperador.DisplayMember = "Nome_Operador"
            cboOperador.ValueMember = "Cod_Operador"
            cboOperador.DataSource = rn_generico.BuscarOperador(0)

            If cboOperador.Items.Count > 0 And cod_operador = 0 Then
                cboOperador.SelectedIndex = 0
            ElseIf cboOperador.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperador.Items.Count - 1
                    For Each linha As CGenerico In cboOperador.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperador.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperador.SelectedIndex = 1
            End If


            cboOperadorResetar.DataSource = Nothing
            cboOperadorResetar.Items.Clear()
            cboOperadorResetar.DisplayMember = "Nome_Operador"
            cboOperadorResetar.ValueMember = "Cod_Operador"
            cboOperadorResetar.DataSource = rn_generico.BuscarOperador(0)

            If cboOperadorResetar.Items.Count > 0 And cod_operador = 0 Then
                cboOperadorResetar.SelectedIndex = 0
            ElseIf cboOperadorResetar.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperadorResetar.Items.Count - 1
                    For Each linha As CGenerico In cboOperadorResetar.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperadorResetar.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperadorResetar.SelectedIndex = 1
            End If

            cbDeOperadorTrocar.DataSource = Nothing
            cbDeOperadorTrocar.Items.Clear()
            cbDeOperadorTrocar.DisplayMember = "Nome_Operador"
            cbDeOperadorTrocar.ValueMember = "Cod_Operador"
            cbDeOperadorTrocar.DataSource = rn_generico.BuscarOperador(0)

            If cbDeOperadorTrocar.Items.Count > 0 And cod_operador = 0 Then
                cbDeOperadorTrocar.SelectedIndex = 0
            ElseIf cbDeOperadorTrocar.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cbDeOperadorTrocar.Items.Count - 1
                    For Each linha As CGenerico In cbDeOperadorTrocar.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cbDeOperadorTrocar.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cbDeOperadorTrocar.SelectedIndex = 1
            End If

            cbParaOperadorTrocar.DataSource = Nothing
            cbParaOperadorTrocar.Items.Clear()
            cbParaOperadorTrocar.DisplayMember = "Nome_Operador"
            cbParaOperadorTrocar.ValueMember = "Cod_Operador"
            cbParaOperadorTrocar.DataSource = rn_generico.BuscarOperador(0)

            If cbParaOperadorTrocar.Items.Count > 0 And cod_operador = 0 Then
                cbParaOperadorTrocar.SelectedIndex = 0
            ElseIf cbParaOperadorTrocar.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cbParaOperadorTrocar.Items.Count - 1
                    For Each linha As CGenerico In cbParaOperadorTrocar.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cbParaOperadorTrocar.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cbParaOperadorTrocar.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub
   

    Private Sub gridRequisicao_CellDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridRequisicao.CellDoubleClick
        Dim OperadorClicado As String

        Try

            OperadorClicado = CType(gridRequisicao.Rows(e.RowIndex).Cells(ColunasGridRequisicao.Operador).Value, String)


            If (e.RowIndex > -1) Then
                Dim frm As FrmConsultaRequisicaoRealizada = New FrmConsultaRequisicaoRealizada
                frm.MesRef = DtMesAnoPesquisa.Value
                frm.Operador = OperadorClicado

                If frm.Operador <> "" Then
                    frm.Selecionado = 1
                    Me.Visible = True
                    frm.ShowDialog()
                End If
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub CarregaComboCidades(ByVal cod_cidade As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCidade.DataSource = Nothing
            cboCidade.Items.Clear()
            cboCidade.DisplayMember = "Nome_cidade"
            cboCidade.ValueMember = "Cod_Cidade"
            cboCidade.DataSource = rn_generico.BuscarCidades(0)

            If cboCidade.Items.Count = 0 Then
                cboCidade.SelectedIndex = 0
            ElseIf cboCidade.Items.Count > 0 And cod_cidade <> 0 Then

                For i As Integer = 0 To cboCidade.Items.Count - 1
                    For Each linha As CGenerico In cboCidade.DataSource
                        If linha.Cod_Cidade = cod_cidade Then
                            cboCidade.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next

            Else

                cboCidade.SelectedIndex = 0
            End If

            cboCidadeResetar.DataSource = Nothing
            cboCidadeResetar.Items.Clear()
            cboCidadeResetar.DisplayMember = "Nome_cidade"
            cboCidadeResetar.ValueMember = "Cod_Cidade"
            cboCidadeResetar.DataSource = rn_generico.BuscarCidades(0)

            If cboCidadeResetar.Items.Count = 0 Then
                cboCidadeResetar.SelectedIndex = 0
            ElseIf cboCidadeResetar.Items.Count > 0 And cod_cidade <> 0 Then

                For i As Integer = 0 To cboCidadeResetar.Items.Count - 1
                    For Each linha As CGenerico In cboCidadeResetar.DataSource
                        If linha.Cod_Cidade = cod_cidade Then
                            cboCidadeResetar.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next

            Else

                cboCidadeResetar.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub


    Private Sub cboCategoria_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboCategoria.SelectedIndexChanged

        If cboCategoria.Text.Equals("LISTA NOVA") Then
            cboCidade.Visible = True
            lblCidade.Visible = True
        Else
            cboCidade.Visible = False
            lblCidade.Visible = False
        End If
    End Sub

    Private Sub btnGerarExcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FrmGerarExcel.Show()
    End Sub

    Private Sub btnResetaPersonalizado_Click(sender As Object, e As EventArgs) Handles btnResetaPersonalizado.Click

        Dim rnGenerico As New RNGenerico
        Dim operador_id As String = ""
        Dim operador As String = ""
        Dim qdtFichas As String = ""


        Try
            Me.Cursor = Cursors.WaitCursor

            'If cboCidadeResetar.SelectedIndex = 0 And cboCategoriaResetar.SelectedIndex = 0 And cboOperadorResetar.SelectedIndex = 0 Then
            '    MsgBox("Por favor selecione um dos requisitos acima")
            '    Me.Cursor = Cursors.Default
            '    Exit Sub
            'End If

            operador_id = cboOperadorResetar.SelectedValue
            operador = cboOperadorResetar.Text
            qdtFichas = txtqtdFichasResetar.Text

            If txtqtdFichasResetar.Text = "" Then
                txtqtdFichasResetar.Text = "0"
            End If

            If rdManual.Checked = True Then
                If rnGenerico.ResetaFichasPersonalizadoManual(operador_id, cboCategoriaResetar.SelectedValue, qdtFichas, cboCidadeResetar.Text) = True Then
                    MsgBox("Operação realizada com suceso!")
                Else
                    MsgBox("Ocorreu um erro!")

                End If

            Else
                If rnGenerico.ResetaFichasPersonalizadoOi(operador_id, operador, cboCategoriaResetar.SelectedValue, qdtFichas) = True Then

                    MsgBox("Operação realizada com suceso!")
                Else
                    MsgBox("Ocorreu um erro!")

                End If
            End If



            Me.Cursor = Cursors.Default

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub rdManual_CheckedChanged(sender As Object, e As EventArgs) Handles rdManual.CheckedChanged
        If rdManual.Checked = True Then
            rdOi.Checked = False
            cboCidadeResetar.Enabled = True
        End If
    End Sub

    Private Sub rdOi_CheckedChanged(sender As Object, e As EventArgs) Handles rdOi.CheckedChanged
        If rdOi.Checked = True Then
            rdManual.Checked = False
            cboCidadeResetar.Enabled = False
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnTrocarFichas.Click


        Dim rnGenerico As New RNGenerico
        Dim de_operador_id As Integer = 0
        Dim de_operador As String = ""
        Dim para_operador_id As Integer = 0
        Dim para_operador As String = ""
        Dim qdtFichas As Integer = 0


        Try
            Me.Cursor = Cursors.WaitCursor

            If cbDeOperadorTrocar.SelectedIndex = 0 And cbCategoriaTrocar.SelectedIndex = 0 Then
                MsgBox("Por favor selecione o operador e categoria para realizar a troca de fichas")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If

            If cbParaOperadorTrocar.SelectedIndex = 0 Then
                MsgBox("Por favor selecione o operador e categoria para enviar as fichas")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If

            de_operador_id = cbDeOperadorTrocar.SelectedValue
            de_operador = cbDeOperadorTrocar.Text
            para_operador_id = cbParaOperadorTrocar.SelectedValue
            para_operador = cbParaOperadorTrocar.Text
            qdtFichas = txtQtdFichasTrocar.Text


            If rnGenerico.TrocarFichasPersonalizadoOi(de_operador_id, de_operador, para_operador_id, para_operador, cbCategoriaTrocar.SelectedValue, qdtFichas) = True Then
                MsgBox("Operação realizada com suceso!")

            Else
                MsgBox("Ocorreu um erro no processo")

            End If

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Me.Cursor = Cursors.Default
        End Try


    End Sub

    Private Sub GeraCSV()
        Try
            Dim rnretorno As New RNRetorno
            Dim situacao As String = ""
            If txtFichasDisponiveis.Text = "" OrElse
                txtFichasDisponiveis.Text = "0" Then
                MsgBox("Não existe fichas disponiveis gerar o arquivo CSV", MsgBoxStyle.Critical, "Atenção!")
                Exit Sub
            End If
            If (cboCategoria.Text.Equals("LISTA NOVA")) Then
                If cboCidade.SelectedIndex = 0 Or cboCidade.Text.Equals("Selecione...") Then
                    MsgBox("Opção invalida, selecionar a cidade!", MsgBoxStyle.Critical, "Atenção!")
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If
            End If
            Me.Cursor = Cursors.WaitCursor
            GerarArquivoCSV(cboCategoria.Text, cboCategoria.SelectedValue)
            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub
End Class