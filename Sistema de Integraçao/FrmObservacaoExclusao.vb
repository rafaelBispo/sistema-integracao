﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports System.Windows.Forms.Screen
Imports System.Drawing.Graphics
Imports System
Imports System.Net
Imports System.Net.Mail

Public Class FrmObservacaoExclusao

#Region "variaveis"

    Public telefone As String
    Public DDD As String
    Public motivo As String = ""
    Public observacao As String = ""
    Public solicitante As String = ""
    Public dtExclusao As String = ""
    Public isExcluido As Boolean = True
    Public caminhoArquivoSalvo As String = ""
    Public email As String = ""

#End Region





    Private Sub FrmObservacaoExclusao_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        Try
            lblData.Text = dtExclusao
            lblMotivo.Text = motivo
            lblObservacao.Text = observacao
            lblSolicitante.Text = solicitante
            lblTelefone.Text = "Telefone (" + DDD + ") " + telefone


            If isExcluido = True Then
                'lblStatus.BackColor = Color.Yellow
                lblStatus.ForeColor = Color.Red
                lblStatus.Text = "CONTRIBUINTE EXCLUIDO!"
            Else
                lblStatus.ForeColor = Color.Green
                lblStatus.Text = "CONTRIBUINTE ATIVO!"
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Me.Close()
    End Sub

    Public Sub IniciaCaptura()

        Dim Frm = New FrmPrint

        Try

            Frm.PictureBox1.Image = CapturaTela()
            Frm.isExcluido = True
            Frm.local = "C:\teste\" & DDD + " " + telefone & ".png"
            caminhoArquivoSalvo = "C:\teste\" & DDD + " " + telefone & ".png"
            Frm.Show()

            Dim result = MessageBox.Show("Deseja enviar um e-mail para o contribuinte com a tela comprobatória?", "Atenção", _
                        MessageBoxButtons.YesNo, _
                        MessageBoxIcon.Question)

            If (result = DialogResult.Yes) Then
                Dim frmEmail As New FrmEmailEnviar
                frmEmail.caminhoArquivoSalvo = caminhoArquivoSalvo
                frmEmail.Show()
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Public Function CapturaTela() As Bitmap
        Try
            Dim BMP As New Bitmap(Me.Width, Me.Height, Imaging.PixelFormat.Format32bppArgb)
            Dim GFX As Graphics = FromImage(BMP)

            Dim screenWidth As Integer = Screen.PrimaryScreen.Bounds.Width
            Dim screenHeight As Integer = Screen.PrimaryScreen.Bounds.Height


            GFX.CopyFromScreen(Me.Location.X, Me.Location.Y, 0, 0, Me.Size, CopyPixelOperation.SourceCopy)

            Return BMP
        Catch ex As Exception
            MsgBox("Erro : " & ex.Message)
        End Try
    End Function

    Public Sub EnviaEmail()
        Try

            'create the mail message
            Dim SmtpServer As New SmtpClient()
            Dim mail As New MailMessage()
            SmtpServer.Credentials = New Net.NetworkCredential("aaci.jf.mg@gmail.com", "instituicao2022")
            SmtpServer.Host = "smtp.gmail.com"
            SmtpServer.Port = 465 'server para gmail
            SmtpServer.EnableSsl = True
            mail.From = New MailAddress("aaci.jf.mg@gmail.com")
            mail.To.Add(email) 'Email que recebera a Mensagem
            mail.Subject = "Exclusão Cadastro AACI"
            mail.Body = ""

            'Este codigo PlainView e caso o email não suporte Html, nisto ele irá informar este erro

            Dim plainView As AlternateView = AlternateView.CreateAlternateViewFromString("Boa tarde,como solicitado, estamos excluindo o seu cadastro do nosso sistema. Desculpe o transtorno, agradeço a compreensão.", Nothing, "text/plain")

            'crie um corpo de texto caso deseje assim você poderá colocar um texto no meio de duas imagens
            Dim linha1 As String = ""
            If Date.Now.Hour < 12 Then
                linha1 = "Bom dia,"
            Else
                linha1 = "Boa tarde,"
            End If

            Dim linha2 As String = "O seu cadastro já consta como excluido em nosso sistema."
            Dim linha3 As String = "desculpe o&nbsp;transtorno, agrade&ccedil;o a compreens&atilde;o."

            'Vamos Iniciar a Parte das Imagens
            ' as imagens ficam no prefixo 'cid' eles irão pegar os seus ContentId e irão destinar as imagens 
            ' você pode criar varias imagens colocando diferentes ContentId 
            ' Veja: <img src='cid:companylogo'> o LinkedResource ( que veremos logo a baixo ) irá destinar a imagem que tenha o  ContentId com o nome de  'companylogo'

            ' aqui você cria o corpo da mensagem com ele você pode criar e desenvolver mais


            'Dim htmlView As AlternateView = AlternateView.CreateAlternateViewFromString("<img src=cid:companylogo> <p> <class='body'>  " + body + "</div>  <img src=cid:Comlog>   ", Nothing, "text/html")
            Dim htmlView As AlternateView = AlternateView.CreateAlternateViewFromString("<p> " + linha1 + "</p> <p>&nbsp;</p> <p>" + linha2 + "</p> <p>&nbsp;</p> <img src=cid:companylogo> <p>&nbsp;</p> <p>" + linha3 + "</p> <p><br /> <br /> --</p> <p>&nbsp;</p> <p>att..</p> <p>&nbsp;</p> <p><strong>AACI - ASSOCIA&Ccedil;&Atilde;O DE APOIO AS CRIAN&Ccedil;AS E IDOSOS</strong></p>   ", Nothing, "text/html")


            'você irá dizer aonde estão as suas imagens e elas irão para Logo e Logo1

            Dim logo As New LinkedResource(caminhoArquivoSalvo)

            ' Lembra do ContentId então aqui que você irá dizer aonde fica as imagens
            ' Olhe ali o htmlview ele tem o ' <img scr=cid:companylogo> ' ele que diz a que a imagem que tenha a propriedade com o nome 'Companylogo' irá ficar aqui
            ' e é nesta parte aonde você diz o nome delas.

            logo.ContentId = "companylogo"

            ' ok agora o Sistema sabe que a ContentId da imagem 'logo' tem o nome de 'companylogo' mas ele não sabe aonde você quer adicionar, 
            'por isto você deve informar. 
            ' como temos mais de uma imagem você deve adicionar mais de uma linha conforme visto a baixo.
            htmlView.LinkedResources.Add(logo)

            'ok, agora só criar o email, digitando o Plainview aonde ele irá criar um segundo plano caso o email não aceite html e irá colocar a imagem caso aceite 

            With mail
                .AlternateViews.Add(plainView)
                .AlternateViews.Add(htmlView)

            End With

            mail.IsBodyHtml = True

            SmtpServer.Send(mail)
            MsgBox("E-mail enviado com sucesso", vbInformation, "Ok")




        Catch ex As Exception  'caso haja algum erro crie uma msg de aviso.
            MsgBox(ex)
        Finally
            '  File.Delete("")
        End Try
    End Sub

End Class