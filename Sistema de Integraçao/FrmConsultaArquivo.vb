﻿Imports System.IO
Imports SISINT001

Public Class FrmConsultaArquivo

#Region "Variaveis Globais"

    Dim nome_arquivo As String
    Dim jaGravou As Boolean

#End Region

#Region "Enum"

    Private Enum ColunasGridRemessa

        UsTipoRegistroD = 0
        UiCodServicoArbor = 1
        UiCodLocalidadeCobranca = 2
        StDDDMeioAcessoCobranca = 3
        StNumMeioAcessoCobranca = 4
        DVlrServico = 5
        DdDataServico = 6
        UsNumParcela = 7
        UsQteParcela = 8
        UiDataInicioCobranca = 9
        UcSinal = 10
        UiAnoMesConta = 11
        StDescReferencia = 12
        DNumNotaFiscal = 13
        DdDataVencimento = 14
        DdDataSituacao = 15
        DVlrFaturado = 16
        StCodigoMovimentacao = 17
        UsSitFaturamento = 18
        UiMotivoSituacao = 19
        UiAnoMesContaMulta = 20
        StNumAssinanteMovel = 21
        SistemaFaturamento = 22
        Ciclo = 23
        UsTipoRegistroT = 24
        QuantidadeDeRegistro = 25
        ValorArquivo = 26

    End Enum

    Enum xlsOption
        xlsOpen
    End Enum

#End Region


    

    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        'Declara uma variável do tipo StreamWriter
        Dim rnRetorno As RNRetorno = New RNRetorno
        Dim retornoList As List(Of CRetorno) = New List(Of CRetorno)
        Dim HeaderList As List(Of CRetorno) = New List(Of CRetorno)
        Dim TraillerList As List(Of CRetorno) = New List(Of CRetorno)
        Dim GravarList As List(Of CRetorno) = New List(Of CRetorno)
        Dim usuario As String = ""
        Dim id_arq As Integer = 0
        Dim nome_arq As String = ""

        Try

            gridRetorno.DataSource = rnRetorno.ConsultaArquivo(cboArquivo.Text)


            'ATUALIZAR OS VALORES DO RESUMO
            CarregaFaturamento(rnRetorno.DataRemessa)



        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub CarregaComboArquivo()
        Dim rn_retorno As RNRetorno = New RNRetorno
        Dim retornoList As List(Of CRetorno) = New List(Of CRetorno)

        Try



            cboArquivo.DataSource = Nothing
            cboArquivo.Items.Clear()
            cboArquivo.DisplayMember = "nome_arquivo"
            cboArquivo.ValueMember = "id_arq"
            cboArquivo.DataSource = rn_retorno.BuscarArquivos()


            If cboArquivo.Items.Count > 0 Then
                cboArquivo.SelectedIndex = 0
            End If

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try

    End Sub



    Private Sub CriarColunasGrid()

        Try



            gridRetorno.DataSource = Nothing
            gridRetorno.Columns.Clear()

            gridRetorno.AutoGenerateColumns = False

            gridRetorno.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            '0

            Dim checkboxColumn As New DataGridViewCheckBoxColumn
            checkboxColumn.Width = 32
            checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            gridRetorno.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UsTipoRegistroD"
            column.HeaderText = "Tipo Registro"
            column.Width = 60
            column.ReadOnly = True
            column.Visible = False
            gridRetorno.Columns.Add(column)


            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UiCodServicoArbor"
            column.HeaderText = "Cod Serviço Arbor"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = False
            gridRetorno.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UiCodLocalidadeCobranca"
            column.HeaderText = "Cod Localidade Cobrança"
            column.Width = 80
            column.ReadOnly = True
            column.Visible = False
            gridRetorno.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StDDDMeioAcessoCobranca"
            column.HeaderText = "DDD"
            column.Width = 50
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StNumMeioAcessoCobranca"
            column.HeaderText = "Telefone Cobrança"
            column.Width = 100
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DVlrServico"
            column.HeaderText = "Valor Serviço"
            column.Visible = True
            column.DefaultCellStyle.Format = "0.00"
            column.Width = 80
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '7
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DdDataServico"
            column.HeaderText = "Data Servico"
            column.Width = 100
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)


            '8
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UsNumParcela"
            column.HeaderText = " Num Parcela"
            column.Visible = False
            column.Width = 50
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)


            '8
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UsQteParcela"
            column.HeaderText = "UsQteParcela"
            column.Width = 40
            column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            column.Visible = False
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)


            '9
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UiDataInicioCobranca"
            column.HeaderText = "UiDataInicioCobranca"
            column.Visible = False
            column.Width = 100
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '10
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UcSinal"
            column.HeaderText = "UcSinal"
            column.Visible = False
            column.Width = 60
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '11
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UiAnoMesConta"
            column.HeaderText = "Ano Mes Conta"
            column.Width = 100
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '12
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StDescReferencia"
            column.HeaderText = "Descrição Cliente"
            column.Width = 150
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)


            '13
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DNumNotaFiscal"
            column.HeaderText = "Num Nota Fiscal"
            column.Visible = True
            column.Width = 100
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '14
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DdDataVencimento"
            column.HeaderText = "DdDataVencimento"
            column.Visible = True
            column.Width = 100
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '15
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DdDataSituacao"
            column.HeaderText = "Data Situacao"
            column.Visible = True
            column.Width = 100
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)


            '16
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DVlrFaturado"
            column.HeaderText = "Valor Faturado"
            column.Visible = True
            column.Width = 70
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)


            '17
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StCodigoMovimentacao"
            column.HeaderText = "Código Movimentação"
            column.Visible = True
            column.Width = 70
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)


            '18
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UsSitFaturamento"
            column.HeaderText = "Situação Faturamento"
            column.Visible = True
            column.Width = 100
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '19
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UiMotivoSituacao"
            column.HeaderText = "Motivo Situação"
            column.Visible = True
            column.Width = 150
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '20
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UiAnoMesContaMulta"
            column.HeaderText = "Ano Mês Conta Multa"
            column.Visible = True
            column.Width = 100
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)


            '21
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "StNumAssinanteMovel"
            column.HeaderText = "StNumAssinanteMovel"
            column.Visible = True
            column.Width = 70
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '22
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "SistemaFaturamento"
            column.HeaderText = "Sistema Faturamento"
            column.Visible = True
            column.Width = 70
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)

            '23
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Ciclo"
            column.HeaderText = "Ciclo"
            column.Visible = True
            column.Width = 70
            column.ReadOnly = True
            gridRetorno.Columns.Add(column)


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub FrmConsultaArquivo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim rnretorno As New RNRetorno

        Try


            CriarColunasGrid()
            CarregaComboArquivo()
            CarregaFaturamento(rnretorno.DataRemessa)

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Close()
    End Sub

    Private Sub CarregaFaturamento(ByVal MesAnoRef As String)

        Dim rnRetorno As RNRetorno = New RNRetorno
        Dim valor As Single
        Dim strValor As String

        Try
            'Afaturar
            strValor = valor.ToString("C")
            valor = rnRetorno.RetornaAFaturar(MesAnoRef)
            lblAfaturar.Text = valor.ToString("R$ #,###.00")

            'Faturado
            strValor = valor.ToString("C")
            valor = rnRetorno.RetornaFaturado(MesAnoRef)
            lblFaturado.Text = valor.ToString("R$ #,###.00")

            'Criticado
            strValor = valor.ToString("C")
            valor = rnRetorno.RetornaCriticado(MesAnoRef)
            lblCriticado.Text = valor.ToString("R$ #,###.00")

            'Arrecadado
            strValor = valor.ToString("C")
            valor = rnRetorno.RetornaArrecadado(MesAnoRef)
            lblArrecadado.Text = valor.ToString("R$ #,###.00")

            'Alt.conta
            strValor = valor.ToString("C")
            valor = rnRetorno.RetornaAltConta(MesAnoRef)
            lblAltConta.Text = valor.ToString("R$ #,###.00")


            'Contestado
            strValor = valor.ToString("C")
            valor = rnRetorno.RetornaContestado(MesAnoRef)
            lblContestado.Text = valor.ToString("R$ #,###.00")


        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try


    End Sub

    Private Sub Label3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label3.Click

    End Sub
End Class


