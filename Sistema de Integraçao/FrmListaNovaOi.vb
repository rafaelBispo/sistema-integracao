﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text
Imports Microsoft.Office.Interop
Imports System.Reflection


Public Class FrmListaNovaOi

#Region "Enum"

    Private Enum ColunasGridRemessa

        Selecao = 0
        Cod_EAN_CLIENTE = 1
        Nome = 2
        DDD = 3
        Telefone = 4
        Valor = 5
        Observacao = 6

    End Enum

    Private Enum ColunasGridHistorico

        Cod_EAN_Cliente = 0
        Nome_contribuinte = 1
        Data_mov = 2
        Tipo_mov = 3
        Tipo_cobranca = 4
        Cod_Categoria = 5
        DDD = 6
        Telefone1 = 7
        Valor = 8
        Cod_recibo = 9
        Cidade = 10

    End Enum

    Enum xlsOption
        xlsOpen
    End Enum

#End Region

#Region "variaveis"

    Public telefone As String
    Private lstContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstRetornoContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstContribuinteSelecionada As New List(Of CRetorno)
    Private v_ambiente As String
    Private rnContribuinte As RNContribuinte = New RNContribuinte
    Private rnRetorno As RNRetorno = New RNRetorno
    Private contribuinte As CContribuinte = New CContribuinte
    Private lstContribuintesOi As New SortableBindingList(Of CRetorno)
    Private lstListaTelefonica As New SortableBindingList(Of CListaTelefonica)
    Private lstContribuinteSelecionadaOi As New List(Of CRetorno)
    Private rnRequisicao As RNRequisicao = New RNRequisicao
    Private lstrequisicao As New SortableBindingList(Of CContribuinte)
    Private lstrequisicaoSelecionada As New List(Of CContribuinte)
    Public mNivelAcesso As Integer
    Public mUsuario As String
    Public mCodUsuario As Integer
    Public atualizou As Boolean
    Public bindingSourceContribuite As BindingSource = New BindingSource
    Public cidadeEscolhida As String


    Public Property NivelAcesso() As Integer
        Get
            Return mNivelAcesso
        End Get
        Set(ByVal value As Integer)
            mNivelAcesso = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return mUsuario
        End Get
        Set(ByVal value As String)
            mUsuario = value
        End Set
    End Property

    Public Property CodUsuario() As Integer
        Get
            Return mCodUsuario
        End Get
        Set(ByVal value As Integer)
            mCodUsuario = value
        End Set
    End Property

#End Region



#Region "Metodos"



    Private Sub CriarColunasGrid()

        Try

            grdRemessa.DataSource = Nothing
            grdRemessa.Columns.Clear()

            grdRemessa.AutoGenerateColumns = False

            grdRemessa.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            Dim checkboxColumn As New DataGridViewCheckBoxColumn
            checkboxColumn.Width = 32
            checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_contribuinte"
            column.HeaderText = "Cod. Contribuinte"
            column.Width = 100
            column.ReadOnly = False
            column.Visible = True
            grdRemessa.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Nome_contribuinte"
            column.HeaderText = "Nome Contribuinte"
            column.Width = 300
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Endereco"
            column.HeaderText = "Endereco"
            column.Width = 250
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Bairro"
            column.HeaderText = "Bairro"
            column.Width = 100
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cidade"
            column.HeaderText = "Cidade"
            column.Width = 100
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UF"
            column.HeaderText = "UF"
            column.Width = 50
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "CEP"
            column.HeaderText = "CEP"
            column.Width = 70
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Telefone1"
            column.HeaderText = "Telefone"
            column.Width = 80
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_empresa"
            column.HeaderText = "Cod. empresa"
            column.Width = 50
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_Cidade"
            column.HeaderText = "Cod. Cidade"
            column.Width = 50
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)


            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_Bairro"
            column.HeaderText = "Cod. Bairro"
            column.Width = 50
            column.ReadOnly = False
            column.Visible = True
            grdRemessa.Columns.Add(column)

            '7
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "clienteOi"
            column.HeaderText = "Cliente Oi"
            column.Width = 50
            column.ReadOnly = False
            column.Visible = True
            grdRemessa.Columns.Add(column)



        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

#End Region

    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""

        Try

            Me.Cursor = Cursors.WaitCursor


            carregaFeedGridCategoriaContribuinte()
            'CarregaPrimeiraLinha()

            btnConfirmarCadastro.Enabled = True
            btnEnriquecimento.Enabled = True
            btnBuscar.Enabled = True

            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try

    End Sub



    Private Sub carregaFeedGridCategoriaContribuinte()

        Dim objRetorno As CRetorno = New CRetorno()
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim ds As DataSet = New DataSet

        Try
            Dim uf As String = cboUF.Text
            grdRemessa.DataSource = Nothing

            lstrequisicao = New SortableBindingList(Of CContribuinte)(rnRequisicao.CarregaFichasListaNovaOi(uf))


            If (lstrequisicao.Count > 0) Then

                bindingSourceContribuite.DataSource = lstrequisicao

                grdRemessa.DataSource = bindingSourceContribuite.DataSource


                lblTotalFichas.Text = grdRemessa.Rows.Count - 1



            Else
                grdRemessa.DataSource = Nothing
                MsgBox("Não existe registros a serem processados, verifique se houve a liberação das fichas!")

            End If



        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub
    Private Sub btnRetorno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            FrmRetorno.Show()

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Close()
    End Sub

    Private Sub FrmConsultaContribuinte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try



            Me.Cursor = Cursors.WaitCursor
            cboUF.SelectedIndex = 0

            CriarColunasGrid()
            CarregaComboCidades(0)
            'CarregaComboCidadesLote(0)

            'carregaFeedGridCategoriaContribuinte()
            'CarregaPrimeiraLinha()

            Me.Cursor = Cursors.Default



            atualizou = True
        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro no load do form:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

#Region "Carrega Combos"



    Private Sub CarregaComboCategoriasDetalhe(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCategoriaDetalhe.DataSource = Nothing
            cboCategoriaDetalhe.Items.Clear()
            cboCategoriaDetalhe.DisplayMember = "Nome_Categoria"
            cboCategoriaDetalhe.ValueMember = "Cod_Categoria"
            cboCategoriaDetalhe.DataSource = rn_generico.BuscarCategoriaPorOperador(CodUsuario)

            If cboCategoriaDetalhe.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoriaDetalhe.SelectedIndex = 0
            ElseIf cboCategoriaDetalhe.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoriaDetalhe.Items.Count - 1
                    For Each linha As CGenerico In cboCategoriaDetalhe.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoriaDetalhe.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoriaDetalhe.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboContribuinte()
        Dim rn_contribuinte As RNContribuinte = New RNContribuinte
        Dim filtro As CContribuinte = New CContribuinte

        'cboNome.DataSource = Nothing
        'cboNome.Items.Clear()
        'cboNome.DisplayMember = "NomeCliente1"
        'cboNome.ValueMember = "Cod_cliente"
        'cboNome.DataSource = rn_contribuinte.CarregarGrid(filtro, "Contribuinte")

        'If cboNome.Items.Count > 0 Then
        '    cboNome.SelectedIndex = 0
        'End If


    End Sub

    Private Sub CarregaComboBairros(ByVal cod_bairro As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try



            cboBairro.DataSource = Nothing
            cboBairro.Items.Clear()
            cboBairro.DisplayMember = "Nome_Bairro"
            cboBairro.ValueMember = "Cod_Bairro"
            cboBairro.DataSource = rn_generico.BuscarBairros(0)

            If cboBairro.Items.Count > 0 And cod_bairro = 0 Then
                cboBairro.SelectedIndex = 0

            ElseIf cboBairro.Items.Count > 0 And cod_bairro <> 0 Then
                For i As Integer = 0 To cboBairro.Items.Count - 1
                    For Each linha As CGenerico In cboBairro.DataSource
                        If linha.Cod_Bairro = cod_bairro Then
                            cboBairro.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboBairro.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboOperador(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboOperador.DataSource = Nothing
            cboOperador.Items.Clear()
            cboOperador.DisplayMember = "Nome_Operador"
            cboOperador.ValueMember = "Cod_Operador"
            cboOperador.DataSource = rn_generico.BuscarOperador(0)

            If cboOperador.Items.Count > 0 And cod_operador = 0 Then
                cboOperador.SelectedIndex = 0
            ElseIf cboOperador.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperador.Items.Count - 1
                    For Each linha As CGenerico In cboOperador.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperador.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperador.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboEspecie(ByVal cod_especie As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try


            cboEspecie.DataSource = Nothing
            cboEspecie.Items.Clear()
            cboEspecie.DisplayMember = "Nome_Especie"
            cboEspecie.ValueMember = "Cod_Especie"
            cboEspecie.DataSource = rn_generico.BuscarEspecie(0)

            If cboEspecie.Items.Count > 0 And cod_especie = 0 Then
                cboEspecie.SelectedIndex = 0
            ElseIf cboEspecie.Items.Count > 0 And cod_especie <> 0 Then
                For i As Integer = 0 To cboEspecie.Items.Count - 1
                    For Each linha As CGenerico In cboEspecie.DataSource
                        If linha.Cod_Especie = cod_especie Then
                            cboEspecie.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboEspecie.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try



    End Sub

#End Region

    Private Sub FrmConsultaUltimaMovimentacao_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""

        Try

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub



    'Private Sub txtNome_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim texto As String = Nothing
    '    Dim linhaVazia As Integer = 0
    '    Dim contador As Integer = 0

    '    If txtNome.Text <> String.Empty Then

    '        linhaVazia = grdRemessa.Rows.Count - 1

    '        'percorre cada linha do DataGridView
    '        For Each linha As DataGridViewRow In grdRemessa.Rows

    '            'percorre cada célula da linha
    '            For Each celula As DataGridViewCell In grdRemessa.Rows(linha.Index).Cells
    '                If Not contador = linhaVazia Then
    '                    'se a coluna for a coluna 2 (Nome) então verifica o criterio
    '                    If celula.ColumnIndex = 2 Then

    '                        If celula.Value.ToString <> "" Then
    '                            texto = IIf(celula.Value.ToString = Nothing, "", celula.Value.ToString)
    '                            'se o texto informado estiver contido na célula então seleciona toda linha
    '                            If texto.Contains(txtNome.Text) Then
    '                                'seleciona a linha
    '                                grdRemessa.CurrentCell = celula
    '                                grdRemessa.CurrentCell = grdRemessa.Rows(celula.RowIndex).Cells(2)
    '                                Exit Sub
    '                            End If
    '                        End If
    '                        contador = contador + 1
    '                    End If
    '                End If

    '            Next

    '        Next

    '    End If

    'End Sub

    'Private Sub txtTelefone_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Dim texto As String = Nothing
    '    Dim linhaVazia As Integer = 0
    '    Dim contador As Integer = 0

    '    If txtTelefone.Text <> String.Empty Then

    '        linhaVazia = grdRemessa.Rows.Count - 1

    '        'percorre cada linha do DataGridView
    '        For Each linha As DataGridViewRow In grdRemessa.Rows

    '            'percorre cada célula da linha
    '            For Each celula As DataGridViewCell In grdRemessa.Rows(linha.Index).Cells
    '                If Not contador = linhaVazia Then
    '                    'se a coluna for a coluna 1 (telefone) então verifica o criterio
    '                    If celula.ColumnIndex = 1 Then

    '                        If celula.Value.ToString <> "" Then
    '                            texto = IIf(celula.Value.ToString = Nothing, "", celula.Value.ToString)
    '                            'se o texto informado estiver contido na célula então seleciona toda linha
    '                            If texto.Contains(txtTelefone.Text) Then
    '                                'seleciona a linha
    '                                grdRemessa.CurrentCell = celula
    '                                grdRemessa.CurrentCell = grdRemessa.Rows(celula.RowIndex).Cells(2)
    '                                Exit Sub
    '                            End If
    '                        End If
    '                        contador = contador + 1
    '                    End If
    '                End If

    '            Next

    '        Next

    '    End If
    'End Sub



    Private Sub CarregaComboCidades(ByVal cod_cidade As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCidade.DataSource = Nothing
            cboCidade.Items.Clear()
            cboCidade.DisplayMember = "Nome_cidade"
            cboCidade.ValueMember = "Cod_Cidade"
            cboCidade.DataSource = rn_generico.BuscarCidades(0)

            If cboCidade.Items.Count = 0 Then
                cboCidade.SelectedIndex = 0
            ElseIf cboCidade.Items.Count > 0 And cod_cidade <> 0 Then

                For i As Integer = 0 To cboCidade.Items.Count - 1
                    For Each linha As CGenerico In cboCidade.DataSource
                        If linha.Cod_Cidade = cod_cidade Then
                            cboCidade.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next

            Else

                cboCidade.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    'Private Sub CarregaComboCidadesLote(ByVal cod_cidade As Integer)
    '    Dim rn_generico As RNGenerico = New RNGenerico
    '    Dim index As Integer = 0
    '    Dim achou As Boolean = False

    '    Try

    '        cboCidadeLote.DataSource = Nothing
    '        cboCidadeLote.Items.Clear()
    '        cboCidadeLote.DisplayMember = "Nome_cidade"
    '        cboCidadeLote.ValueMember = "Cod_Cidade"
    '        cboCidadeLote.DataSource = rn_generico.BuscarCidades(0)

    '        If cboCidadeLote.Items.Count = 0 Then
    '            cboCidadeLote.SelectedIndex = 0
    '        ElseIf cboCidadeLote.Items.Count > 0 And cod_cidade <> 0 Then

    '            For i As Integer = 0 To cboCidadeLote.Items.Count - 1
    '                For Each linha As CGenerico In cboCidadeLote.DataSource
    '                    If linha.Cod_Cidade = cod_cidade Then
    '                        cboCidadeLote.SelectedIndex = index
    '                        achou = True
    '                        Exit For
    '                    End If
    '                    index = index + 1
    '                Next
    '                If achou = True Then
    '                    Exit For
    '                End If
    '            Next

    '        Else

    '            cboCidadeLote.SelectedIndex = 0
    '        End If

    '    Catch ex As Exception
    '        MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
    '    End Try

    'End Sub

    Private Sub btnLibera_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConfirmarCadastro.Click
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim ClienteSelecionado As String
        Dim isErro As Integer = 0
        Dim new_cod As String
        Dim contatorInclusao As Integer = 0

        Try

            Me.Cursor = Cursors.WaitCursor
            Dim mensagem = MessageBox.Show("Vão ser incluidos " & lstrequisicao.Count & ", deseja continuar?", "Atenção", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
            If (mensagem = DialogResult.No) Then
                Exit Sub
            End If

            If (lstrequisicao.Count > 0) Then
                For Each contribuinte In lstrequisicao
                    Dim cod_cliente_existente As String = String.Empty
                    Dim cod_ean_existente As String = String.Empty
                    Dim numeroInstalacao As String = String.Empty
                    contribuinte.DataCadastro = Format(Date.Now, "yyyy-MM-dd")
                    contribuinte.Cod_Categoria = 10
                    contribuinte.Valor = 10
                    contribuinte.Cod_Especie = 1
                    contribuinte.NomeCliente1 = Replace(contribuinte.NomeCliente1, "'", "")
                    contribuinte.NomeCliente2 = Replace(contribuinte.NomeCliente2, "'", "")
                    contribuinte.Endereco = Replace(contribuinte.Endereco, "'", "")
                    contribuinte.Telefone1 = contribuinte.Telefone1.Replace("-", "")
                    contribuinte.Cod_Cidade = rnRequisicao.RetornaCodCidade(contribuinte.Cidade)
                    'If Not rnRequisicao.VerificaInclusaoCliente(contribuinte.DDD, contribuinte.Telefone1) Then
                    '    Continue For
                    'End If
                    'contribuinte.ClienteOi = "S"

                    If contribuinte.ClienteOi = "S" Then
                        new_cod = rnContribuinte.RetornaNovoCodigo(True)
                        contribuinte.Cod_contribuinte = new_cod
                        contribuinte.CodCliente = new_cod
                        contribuinte.Cod_EAN_Cliente = "*" + new_cod + "-224*"
                        If Not rnRequisicao.VerificaInclusaoCliente(contribuinte.DDD,
                                                                contribuinte.Telefone1,
                                                                cod_ean_existente,
                                                                cod_cliente_existente,
                                                                True) Then
                            contribuinte.Cod_EAN_Cliente = cod_ean_existente
                            contribuinte.CodCliente = cod_cliente_existente
                            contribuinte.Cod_contribuinte = cod_cliente_existente
                        Else
                            rnContribuinte.Salvar(contribuinte)
                        End If
                        If rnRequisicao.VerificaInclusaoRequisicaoLista(contribuinte.Cod_EAN_Cliente, "Oi") AndAlso
                            rnRequisicao.IncluiRegistroListaNovaRequisicaoOi(contribuinte) Then
                            contatorInclusao = contatorInclusao + 1
                        Else
                            isErro = isErro + 1
                        End If
                    ElseIf contribuinte.ClienteOi = "N" Then
                        new_cod = rnContribuinte.RetornaNovoCodigo(False)
                        contribuinte.Cod_contribuinte = new_cod
                        contribuinte.CodCliente = new_cod
                        contribuinte.Cod_EAN_Cliente = "*" + new_cod + "-224*"
                        If Not rnRequisicao.VerificaInclusaoCliente(contribuinte.DDD,
                                                                contribuinte.Telefone1,
                                                                cod_ean_existente,
                                                                cod_cliente_existente,
                                                                False) Then
                            contribuinte.Cod_EAN_Cliente = cod_ean_existente
                            contribuinte.CodCliente = cod_cliente_existente
                            contribuinte.Cod_contribuinte = cod_cliente_existente
                        Else
                            rnContribuinte.SalvarCemig(contribuinte)
                        End If
                        If rnRequisicao.VerificaInclusaoRequisicaoLista(contribuinte.Cod_EAN_Cliente, "Cemig") AndAlso
                            rnRequisicao.IncluiRegistroListaNovaRequisicaoCemig(contribuinte, contribuinte.Cobrador) Then
                            contatorInclusao = contatorInclusao + 1
                        Else
                            isErro = isErro + 1
                        End If
                    End If
                Next
            End If

            If isErro = 0 Then
                MsgBox("Foram incluidos com sucesso " & contatorInclusao & " registros no cadastro de cliente e na requisição!", MsgBoxStyle.Information, "Atenção")
            Else
                MsgBox("Ocorreu um erro na confirmação de " + isErro + " fichas, verifique novamente para liberar!! ", MsgBoxStyle.Information, "Atenção")
            End If


            lblValor.Text = ""
            lblTotalFichas.Text = 0

            carregaFeedGridCategoriaContribuinte()

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            ' MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub chkHeader_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkHeader.CheckedChanged

        Me.Cursor = Cursors.WaitCursor

        Dim i As Integer
        Dim valorCheckHeader As Boolean

        valorCheckHeader = chkHeader.Checked

        For i = 0 To grdRemessa.RowCount - 1

            If valorCheckHeader = True Then
                grdRemessa(ColunasGridRemessa.Selecao, i).Value = valorCheckHeader
            Else
                grdRemessa(ColunasGridRemessa.Selecao, i).Value = valorCheckHeader
            End If
        Next i

        Me.Cursor = Cursors.Default
        grdRemessa.EndEdit()
    End Sub

    'Private Sub grdRemessa_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdRemessa.CellContentClick
    '    Dim ClienteClicado As String
    '    Dim TelefoneSelecionado As String = ""
    '    Dim rn As RNRequisicao = New RNRequisicao
    '    Dim rnContribuinte As RNContribuinte = New RNContribuinte

    '    Try

    '        ClienteClicado = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.Cod_EAN_CLIENTE).Value, String)


    '        If (e.RowIndex > -1) Then

    '            lstrequisicao = New SortableBindingList(Of CContribuinte)(rnContribuinte.CarregaFichasListaNova(cboCidadeLote.Text, ClienteClicado))

    '            If (lstrequisicao.Count > 0) Then


    '                For Each contribuinte In lstrequisicao


    '                    'popula a tela
    '                    txtCodigo.Text = contribuinte.CodCliente
    '                    txtNomeCliente1.Text = contribuinte.Nome_contribuinte
    '                    txtNomeCliente2.Text = contribuinte.Nome_contribuinte
    '                    txtCodBarra.Text = contribuinte.Cod_EAN_Cliente
    '                    txtCnpjCpf.Text = contribuinte.CNPJ_CPF
    '                    txtRg.Text = contribuinte.IE_CI
    '                    txtEmail.Text = contribuinte.Email
    '                    txtTelefone1.Text = contribuinte.Telefone1
    '                    txtTelefone2.Text = contribuinte.Telefone2
    '                    txtEndereco.Text = contribuinte.Endereco
    '                    txtUF.Text = contribuinte.UF
    '                    txtCEP.Text = contribuinte.CEP

    '                    txtRegiao.Text = contribuinte.Cod_Regiao

    '                    txtDtNc.Text = contribuinte.DataNascimento
    '                    txtDtNcMp.Text = contribuinte.DT_NC_MP
    '                    txtDtCadastro.Text = contribuinte.DataCadastro
    '                    txtReferencia.Text = contribuinte.Referencia

    '                    txtObs.Text = contribuinte.Observacao

    '                    txtMelhorDia.Text = contribuinte.DiaLimite
    '                    txtValDoacao.Text = Convert.ToDouble(contribuinte.Valor).ToString("R$ #,###.00")
    '                    txtDDD.Text = contribuinte.DDD

    '                    CarregaComboCidades(contribuinte.Cod_Cidade)
    '                    CarregaComboCategoriasDetalhe(contribuinte.Cod_Categoria)
    '                    CarregaComboOperador(contribuinte.Cod_Operador)
    '                    CarregaComboBairros(contribuinte.Cod_Bairro)
    '                    CarregaComboEspecie(contribuinte.Cod_Especie)



    '                    If contribuinte.Descontinuado = "S" Then
    '                        chkInativo.Checked = True
    '                    Else
    '                        chkInativo.Checked = False
    '                    End If

    '                    If contribuinte.Nao_pedir_extra = "S" Then
    '                        chkNaoPedirExtra.Checked = True
    '                    Else
    '                        chkNaoPedirExtra.Checked = False
    '                    End If

    '                    If contribuinte.Nao_Pedir_Mensal = "S" Then
    '                        chkNaoPedirMensal.Checked = True
    '                    Else
    '                        chkNaoPedirMensal.Checked = False
    '                    End If

    '                    If contribuinte.Tel_desligado = "S" Then
    '                        chkTelDesl.Checked = True
    '                    Else
    '                        chkTelDesl.Checked = False
    '                    End If

    '                Next

    '            End If

    '        End If

    '    Catch ex As Exception
    '        MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
    '    End Try
    'End Sub

    Private Sub btnBuscar_Click(sender As Object, e As EventArgs) Handles btnBuscar.Click

        Dim rnRetorno As RNRetorno = New RNRetorno

        Try
            btnEnriquecimento.Enabled = True

            If OpenFileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                Dim sr As New System.IO.StreamReader(OpenFileDialog.FileName)
                txtcaminho.Text = OpenFileDialog.FileName.ToString()

            End If

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub btnEnriquecimento_Click(sender As Object, e As EventArgs) Handles btnEnriquecimento.Click
        Dim Leitura As StreamReader
        Dim clienteOi As String = ""
        Dim DDD As String = ""
        Dim Telefone As String = ""
        Dim separador() As Char
        Dim strLinha As String
        Dim rnRequisicao As RNRequisicao = New RNRequisicao

        Try
            'Verifica se já existe um arquivo com este nome
            If File.Exists(txtcaminho.Text) = False Then
                MsgBox("Este arquivo não existe")
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor

            Leitura = File.OpenText(txtcaminho.Text)


            While (Leitura.Peek() > -1)

                'Parra toda a linha atual para uma variável 
                strLinha = Leitura.ReadLine
                separador = New Char() {";"}
                ' Separa string baseado no delimitador
                Dim palavras As String() = strLinha.Split(separador)
                Dim palavra As String

                For Each palavra In palavras
                    clienteOi = palavra(0)
                    DDD = palavra(3)
                    Telefone = palavra(4)
                    rnRequisicao.IncluiRegistroListaNovaRequisicaoOi(contribuinte)

                Next

            End While

            Leitura.Dispose()
            Leitura.Close()


            Me.Cursor = Cursors.Default

        Catch ex As Exception
            ' Show the exception's message.
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles btnGeraArquivoEnriquecimento.Click
        Try
            Dim Leitura As StreamReader
            Dim clienteOi As String = ""
            Dim DDD As String = ""
            Dim Telefone As String = ""
            Dim separador() As Char
            Dim strLinha As String
            Dim rnRequisicao As RNRequisicao = New RNRequisicao
            Dim listaTelefones As New List(Of String)
            Dim conteudoArquivo As String
            'Verifica se já existe um arquivo com este nome
            If File.Exists(txtcaminho.Text) = False Then
                MsgBox("Este arquivo não existe")
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor

            Leitura = File.OpenText(txtcaminho.Text)

            While (Leitura.Peek() > -1)

                'Parra toda a linha atual para uma variável 
                strLinha = Leitura.ReadLine
                separador = New Char() {";"}
                ' Separa string baseado no delimitador
                Dim palavras As String() = strLinha.Split(separador)

                If palavras(1).Equals("""F�sica""") OrElse
                    palavras(1).Equals("""Física""") OrElse
                    palavras(1).Equals("""Fisica""") Then
                    DDD = palavras(4)
                    Telefone = (Replace(palavras(5), "-", String.Empty))
                    listaTelefones.Add($"{Replace(DDD, """", "")}{vbTab}{Replace(Telefone, """", "")}")
                End If
            End While
            conteudoArquivo = ConcatenateWithNewLines(listaTelefones)

            Leitura.Dispose()
            Leitura.Close()

            CriarArquivo(conteudoArquivo, "lista Bahia")


            Me.Cursor = Cursors.Default

        Catch ex As Exception
            ' Show the exception's message.
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Public Function ConcatenateWithNewLines(values As IEnumerable(Of String)) As String
        Try
            ' Verifica se a lista de valores não é nula ou vazia
            If values Is Nothing OrElse Not values.Any() Then
                Return String.Empty
            End If
            ' Concatena os valores com quebras de linha no final de cada linha
            Dim concatenatedString As String = String.Join(vbCrLf, values)
            Return concatenatedString
        Catch ex As Exception
            ' Show the exception's message.
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return String.Empty
        End Try
    End Function

    Private Sub CriarArquivo(textoarquivo As String, nome_arquivo As String)
        Try
            Dim strNomeArquivo As String = $"{nome_arquivo}.txt"
            Dim caminhoExe As String = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
            Dim caminhoArquivo As String = Path.Combine(caminhoExe, strNomeArquivo)
            Dim rn As RNRetorno = New RNRetorno()
            'verifica se o arquivo existe
            If Not File.Exists(caminhoArquivo) Then
                ' Cria um arquivo para escrita
                Using sw As FileStream = File.Create(caminhoArquivo)
                    Dim texto As Byte() = New UTF8Encoding(True).GetBytes(textoarquivo)
                    sw.Write(texto, 0, texto.Length)
                End Using
                MsgBox($"Arquivo gerado com sucesso no caminho{caminhoArquivo}")
            Else
                MessageBox.Show("Ja existe um arquivo com esse nome ", "Informe o nome do arquivo", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If
        Catch ex As Exception

        End Try
    End Sub
End Class