﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmControleUsuario
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Dados = New System.Windows.Forms.GroupBox()
        Me.cboOperador = New System.Windows.Forms.ComboBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtNome = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtTelefone = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkRecuperaRetido = New System.Windows.Forms.CheckBox()
        Me.chkAvulsoRetido = New System.Windows.Forms.CheckBox()
        Me.chkMensalRetido = New System.Windows.Forms.CheckBox()
        Me.ChkPedirMensal = New System.Windows.Forms.CheckBox()
        Me.chkListaB = New System.Windows.Forms.CheckBox()
        Me.chkListaNova = New System.Windows.Forms.CheckBox()
        Me.chkRecuperacao = New System.Windows.Forms.CheckBox()
        Me.chkInativo = New System.Windows.Forms.CheckBox()
        Me.chkAvulsoB = New System.Windows.Forms.CheckBox()
        Me.chkMensalPrimeira = New System.Windows.Forms.CheckBox()
        Me.chkAvulso = New System.Windows.Forms.CheckBox()
        Me.chkEspecial = New System.Windows.Forms.CheckBox()
        Me.chkMensal = New System.Windows.Forms.CheckBox()
        Me.chkLista = New System.Windows.Forms.CheckBox()
        Me.chkDoacao = New System.Windows.Forms.CheckBox()
        Me.btnSair = New System.Windows.Forms.Button()
        Me.btnSalvar = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkGerencial = New System.Windows.Forms.CheckBox()
        Me.chkFinanceiro = New System.Windows.Forms.CheckBox()
        Me.btnFinanceiro = New System.Windows.Forms.Button()
        Me.btnRegras = New System.Windows.Forms.Button()
        Me.btnGerarRequisicao = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.DtMesAnoPesquisa = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Dados.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Dados
        '
        Me.Dados.Controls.Add(Me.cboOperador)
        Me.Dados.Controls.Add(Me.Label26)
        Me.Dados.Controls.Add(Me.Label5)
        Me.Dados.Controls.Add(Me.txtNome)
        Me.Dados.Controls.Add(Me.Label4)
        Me.Dados.Controls.Add(Me.txtTelefone)
        Me.Dados.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.Dados.Location = New System.Drawing.Point(12, 12)
        Me.Dados.Name = "Dados"
        Me.Dados.Size = New System.Drawing.Size(380, 181)
        Me.Dados.TabIndex = 8
        Me.Dados.TabStop = False
        Me.Dados.Text = "Dados"
        '
        'cboOperador
        '
        Me.cboOperador.FormattingEnabled = True
        Me.cboOperador.Location = New System.Drawing.Point(100, 28)
        Me.cboOperador.Name = "cboOperador"
        Me.cboOperador.Size = New System.Drawing.Size(239, 21)
        Me.cboOperador.TabIndex = 1
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(17, 29)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(77, 16)
        Me.Label26.TabIndex = 57
        Me.Label26.Text = "Operadora:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(46, 66)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 16)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Nome:"
        '
        'txtNome
        '
        Me.txtNome.Location = New System.Drawing.Point(100, 65)
        Me.txtNome.Name = "txtNome"
        Me.txtNome.ReadOnly = True
        Me.txtNome.Size = New System.Drawing.Size(239, 20)
        Me.txtNome.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(44, 102)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(50, 16)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Senha:"
        '
        'txtTelefone
        '
        Me.txtTelefone.Location = New System.Drawing.Point(100, 101)
        Me.txtTelefone.Name = "txtTelefone"
        Me.txtTelefone.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtTelefone.Size = New System.Drawing.Size(239, 20)
        Me.txtTelefone.TabIndex = 4
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkRecuperaRetido)
        Me.GroupBox1.Controls.Add(Me.chkAvulsoRetido)
        Me.GroupBox1.Controls.Add(Me.chkMensalRetido)
        Me.GroupBox1.Controls.Add(Me.ChkPedirMensal)
        Me.GroupBox1.Controls.Add(Me.chkListaB)
        Me.GroupBox1.Controls.Add(Me.chkListaNova)
        Me.GroupBox1.Controls.Add(Me.chkRecuperacao)
        Me.GroupBox1.Controls.Add(Me.chkInativo)
        Me.GroupBox1.Controls.Add(Me.chkAvulsoB)
        Me.GroupBox1.Controls.Add(Me.chkMensalPrimeira)
        Me.GroupBox1.Controls.Add(Me.chkAvulso)
        Me.GroupBox1.Controls.Add(Me.chkEspecial)
        Me.GroupBox1.Controls.Add(Me.chkMensal)
        Me.GroupBox1.Controls.Add(Me.chkLista)
        Me.GroupBox1.Controls.Add(Me.chkDoacao)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox1.Location = New System.Drawing.Point(414, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(520, 181)
        Me.GroupBox1.TabIndex = 58
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Categorias"
        '
        'chkRecuperaRetido
        '
        Me.chkRecuperaRetido.AutoSize = True
        Me.chkRecuperaRetido.Location = New System.Drawing.Point(328, 74)
        Me.chkRecuperaRetido.Name = "chkRecuperaRetido"
        Me.chkRecuperaRetido.Size = New System.Drawing.Size(129, 17)
        Me.chkRecuperaRetido.TabIndex = 14
        Me.chkRecuperaRetido.Text = "RECUPERA RETIDO"
        Me.chkRecuperaRetido.UseVisualStyleBackColor = True
        '
        'chkAvulsoRetido
        '
        Me.chkAvulsoRetido.AutoSize = True
        Me.chkAvulsoRetido.Location = New System.Drawing.Point(328, 51)
        Me.chkAvulsoRetido.Name = "chkAvulsoRetido"
        Me.chkAvulsoRetido.Size = New System.Drawing.Size(113, 17)
        Me.chkAvulsoRetido.TabIndex = 13
        Me.chkAvulsoRetido.Text = "AVULSO RETIDO"
        Me.chkAvulsoRetido.UseVisualStyleBackColor = True
        '
        'chkMensalRetido
        '
        Me.chkMensalRetido.AutoSize = True
        Me.chkMensalRetido.Location = New System.Drawing.Point(328, 28)
        Me.chkMensalRetido.Name = "chkMensalRetido"
        Me.chkMensalRetido.Size = New System.Drawing.Size(114, 17)
        Me.chkMensalRetido.TabIndex = 12
        Me.chkMensalRetido.Text = "MENSAL RETIDO"
        Me.chkMensalRetido.UseVisualStyleBackColor = True
        '
        'ChkPedirMensal
        '
        Me.ChkPedirMensal.AutoSize = True
        Me.ChkPedirMensal.Location = New System.Drawing.Point(170, 143)
        Me.ChkPedirMensal.Name = "ChkPedirMensal"
        Me.ChkPedirMensal.Size = New System.Drawing.Size(106, 17)
        Me.ChkPedirMensal.TabIndex = 11
        Me.ChkPedirMensal.Text = "PEDIR MENSAL"
        Me.ChkPedirMensal.UseVisualStyleBackColor = True
        '
        'chkListaB
        '
        Me.chkListaB.AutoSize = True
        Me.chkListaB.Location = New System.Drawing.Point(170, 120)
        Me.chkListaB.Name = "chkListaB"
        Me.chkListaB.Size = New System.Drawing.Size(66, 17)
        Me.chkListaB.TabIndex = 10
        Me.chkListaB.Text = "LISTA B"
        Me.chkListaB.UseVisualStyleBackColor = True
        '
        'chkListaNova
        '
        Me.chkListaNova.AutoSize = True
        Me.chkListaNova.Location = New System.Drawing.Point(170, 97)
        Me.chkListaNova.Name = "chkListaNova"
        Me.chkListaNova.Size = New System.Drawing.Size(89, 17)
        Me.chkListaNova.TabIndex = 9
        Me.chkListaNova.Text = "LISTA NOVA"
        Me.chkListaNova.UseVisualStyleBackColor = True
        '
        'chkRecuperacao
        '
        Me.chkRecuperacao.AutoSize = True
        Me.chkRecuperacao.Location = New System.Drawing.Point(170, 74)
        Me.chkRecuperacao.Name = "chkRecuperacao"
        Me.chkRecuperacao.Size = New System.Drawing.Size(107, 17)
        Me.chkRecuperacao.TabIndex = 8
        Me.chkRecuperacao.Text = "RECUPERAÇÃO"
        Me.chkRecuperacao.UseVisualStyleBackColor = True
        '
        'chkInativo
        '
        Me.chkInativo.AutoSize = True
        Me.chkInativo.Location = New System.Drawing.Point(170, 51)
        Me.chkInativo.Name = "chkInativo"
        Me.chkInativo.Size = New System.Drawing.Size(69, 17)
        Me.chkInativo.TabIndex = 7
        Me.chkInativo.Text = "INATIVO"
        Me.chkInativo.UseVisualStyleBackColor = True
        '
        'chkAvulsoB
        '
        Me.chkAvulsoB.AutoSize = True
        Me.chkAvulsoB.Location = New System.Drawing.Point(170, 28)
        Me.chkAvulsoB.Name = "chkAvulsoB"
        Me.chkAvulsoB.Size = New System.Drawing.Size(79, 17)
        Me.chkAvulsoB.TabIndex = 6
        Me.chkAvulsoB.Text = "AVULSO B"
        Me.chkAvulsoB.UseVisualStyleBackColor = True
        '
        'chkMensalPrimeira
        '
        Me.chkMensalPrimeira.AutoSize = True
        Me.chkMensalPrimeira.Location = New System.Drawing.Point(15, 143)
        Me.chkMensalPrimeira.Name = "chkMensalPrimeira"
        Me.chkMensalPrimeira.Size = New System.Drawing.Size(125, 17)
        Me.chkMensalPrimeira.TabIndex = 5
        Me.chkMensalPrimeira.Text = "MENSAL PRIMEIRA"
        Me.chkMensalPrimeira.UseVisualStyleBackColor = True
        '
        'chkAvulso
        '
        Me.chkAvulso.AutoSize = True
        Me.chkAvulso.Location = New System.Drawing.Point(15, 120)
        Me.chkAvulso.Name = "chkAvulso"
        Me.chkAvulso.Size = New System.Drawing.Size(69, 17)
        Me.chkAvulso.TabIndex = 4
        Me.chkAvulso.Text = "AVULSO"
        Me.chkAvulso.UseVisualStyleBackColor = True
        '
        'chkEspecial
        '
        Me.chkEspecial.AutoSize = True
        Me.chkEspecial.Location = New System.Drawing.Point(15, 97)
        Me.chkEspecial.Name = "chkEspecial"
        Me.chkEspecial.Size = New System.Drawing.Size(77, 17)
        Me.chkEspecial.TabIndex = 3
        Me.chkEspecial.Text = "ESPECIAL"
        Me.chkEspecial.UseVisualStyleBackColor = True
        '
        'chkMensal
        '
        Me.chkMensal.AutoSize = True
        Me.chkMensal.Location = New System.Drawing.Point(15, 74)
        Me.chkMensal.Name = "chkMensal"
        Me.chkMensal.Size = New System.Drawing.Size(70, 17)
        Me.chkMensal.TabIndex = 2
        Me.chkMensal.Text = "MENSAL"
        Me.chkMensal.UseVisualStyleBackColor = True
        '
        'chkLista
        '
        Me.chkLista.AutoSize = True
        Me.chkLista.Location = New System.Drawing.Point(15, 51)
        Me.chkLista.Name = "chkLista"
        Me.chkLista.Size = New System.Drawing.Size(56, 17)
        Me.chkLista.TabIndex = 1
        Me.chkLista.Text = "LISTA"
        Me.chkLista.UseVisualStyleBackColor = True
        '
        'chkDoacao
        '
        Me.chkDoacao.AutoSize = True
        Me.chkDoacao.Location = New System.Drawing.Point(15, 28)
        Me.chkDoacao.Name = "chkDoacao"
        Me.chkDoacao.Size = New System.Drawing.Size(71, 17)
        Me.chkDoacao.TabIndex = 0
        Me.chkDoacao.Text = "DOAÇÃO"
        Me.chkDoacao.UseVisualStyleBackColor = True
        '
        'btnSair
        '
        Me.btnSair.Location = New System.Drawing.Point(852, 271)
        Me.btnSair.Name = "btnSair"
        Me.btnSair.Size = New System.Drawing.Size(75, 23)
        Me.btnSair.TabIndex = 62
        Me.btnSair.Text = "Sair"
        Me.btnSair.UseVisualStyleBackColor = True
        '
        'btnSalvar
        '
        Me.btnSalvar.Location = New System.Drawing.Point(769, 271)
        Me.btnSalvar.Name = "btnSalvar"
        Me.btnSalvar.Size = New System.Drawing.Size(75, 23)
        Me.btnSalvar.TabIndex = 59
        Me.btnSalvar.Text = "Salvar"
        Me.btnSalvar.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkGerencial)
        Me.GroupBox2.Controls.Add(Me.chkFinanceiro)
        Me.GroupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.GroupBox2.Location = New System.Drawing.Point(12, 199)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(117, 100)
        Me.GroupBox2.TabIndex = 59
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Acesso"
        '
        'chkGerencial
        '
        Me.chkGerencial.AutoSize = True
        Me.chkGerencial.Location = New System.Drawing.Point(15, 51)
        Me.chkGerencial.Name = "chkGerencial"
        Me.chkGerencial.Size = New System.Drawing.Size(87, 17)
        Me.chkGerencial.TabIndex = 1
        Me.chkGerencial.Text = "GERÊNCIAL"
        Me.chkGerencial.UseVisualStyleBackColor = True
        '
        'chkFinanceiro
        '
        Me.chkFinanceiro.AutoSize = True
        Me.chkFinanceiro.Location = New System.Drawing.Point(15, 28)
        Me.chkFinanceiro.Name = "chkFinanceiro"
        Me.chkFinanceiro.Size = New System.Drawing.Size(91, 17)
        Me.chkFinanceiro.TabIndex = 0
        Me.chkFinanceiro.Text = "FINANCEIRO"
        Me.chkFinanceiro.UseVisualStyleBackColor = True
        '
        'btnFinanceiro
        '
        Me.btnFinanceiro.Location = New System.Drawing.Point(646, 271)
        Me.btnFinanceiro.Name = "btnFinanceiro"
        Me.btnFinanceiro.Size = New System.Drawing.Size(75, 23)
        Me.btnFinanceiro.TabIndex = 63
        Me.btnFinanceiro.Text = "Financeiro"
        Me.btnFinanceiro.UseVisualStyleBackColor = True
        '
        'btnRegras
        '
        Me.btnRegras.Location = New System.Drawing.Point(23, 23)
        Me.btnRegras.Name = "btnRegras"
        Me.btnRegras.Size = New System.Drawing.Size(113, 23)
        Me.btnRegras.TabIndex = 64
        Me.btnRegras.Text = "Regras Requisição"
        Me.btnRegras.UseVisualStyleBackColor = True
        '
        'btnGerarRequisicao
        '
        Me.btnGerarRequisicao.Location = New System.Drawing.Point(23, 57)
        Me.btnGerarRequisicao.Name = "btnGerarRequisicao"
        Me.btnGerarRequisicao.Size = New System.Drawing.Size(113, 37)
        Me.btnGerarRequisicao.TabIndex = 65
        Me.btnGerarRequisicao.Text = "Gerar Requisição"
        Me.btnGerarRequisicao.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.DtMesAnoPesquisa)
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.btnRegras)
        Me.GroupBox3.Controls.Add(Me.btnGerarRequisicao)
        Me.GroupBox3.Location = New System.Drawing.Point(135, 199)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(257, 100)
        Me.GroupBox3.TabIndex = 66
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Requisição"
        '
        'DtMesAnoPesquisa
        '
        Me.DtMesAnoPesquisa.CustomFormat = "MM/yyyy"
        Me.DtMesAnoPesquisa.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DtMesAnoPesquisa.Location = New System.Drawing.Point(159, 71)
        Me.DtMesAnoPesquisa.Name = "DtMesAnoPesquisa"
        Me.DtMesAnoPesquisa.Size = New System.Drawing.Size(83, 20)
        Me.DtMesAnoPesquisa.TabIndex = 74
        Me.DtMesAnoPesquisa.Value = New Date(2017, 3, 1, 0, 0, 0, 0)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(151, 51)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(91, 16)
        Me.Label3.TabIndex = 73
        Me.Label3.Text = "Ano Mês Ref.:"
        '
        'FrmControleUsuario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(946, 308)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.btnFinanceiro)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnSair)
        Me.Controls.Add(Me.btnSalvar)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Dados)
        Me.Name = "FrmControleUsuario"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Controle de Usuario"
        Me.Dados.ResumeLayout(False)
        Me.Dados.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Dados As System.Windows.Forms.GroupBox
    Friend WithEvents cboOperador As System.Windows.Forms.ComboBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtNome As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtTelefone As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkRecuperaRetido As System.Windows.Forms.CheckBox
    Friend WithEvents chkAvulsoRetido As System.Windows.Forms.CheckBox
    Friend WithEvents chkMensalRetido As System.Windows.Forms.CheckBox
    Friend WithEvents ChkPedirMensal As System.Windows.Forms.CheckBox
    Friend WithEvents chkListaB As System.Windows.Forms.CheckBox
    Friend WithEvents chkListaNova As System.Windows.Forms.CheckBox
    Friend WithEvents chkRecuperacao As System.Windows.Forms.CheckBox
    Friend WithEvents chkInativo As System.Windows.Forms.CheckBox
    Friend WithEvents chkAvulsoB As System.Windows.Forms.CheckBox
    Friend WithEvents chkMensalPrimeira As System.Windows.Forms.CheckBox
    Friend WithEvents chkAvulso As System.Windows.Forms.CheckBox
    Friend WithEvents chkEspecial As System.Windows.Forms.CheckBox
    Friend WithEvents chkMensal As System.Windows.Forms.CheckBox
    Friend WithEvents chkLista As System.Windows.Forms.CheckBox
    Friend WithEvents chkDoacao As System.Windows.Forms.CheckBox
    Friend WithEvents btnSair As System.Windows.Forms.Button
    Friend WithEvents btnSalvar As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents chkGerencial As System.Windows.Forms.CheckBox
    Friend WithEvents chkFinanceiro As System.Windows.Forms.CheckBox
    Friend WithEvents btnFinanceiro As System.Windows.Forms.Button
    Friend WithEvents btnRegras As System.Windows.Forms.Button
    Friend WithEvents btnGerarRequisicao As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents DtMesAnoPesquisa As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
End Class
