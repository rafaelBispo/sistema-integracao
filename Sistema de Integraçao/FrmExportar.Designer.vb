﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmExportar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExportar = New System.Windows.Forms.Button()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.chkEnviaEmail = New System.Windows.Forms.CheckBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.dtFim = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dtInicio = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.chkReqOi = New System.Windows.Forms.CheckBox()
        Me.lstDados = New System.Windows.Forms.ListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnArquivo = New System.Windows.Forms.Button()
        Me.txtArquivo = New System.Windows.Forms.TextBox()
        Me.btnLog = New System.Windows.Forms.Button()
        Me.btnImportar = New System.Windows.Forms.Button()
        Me.ofd1 = New System.Windows.Forms.OpenFileDialog()
        Me.btnSair = New System.Windows.Forms.Button()
        Me.chkRequisicao_tb = New System.Windows.Forms.CheckBox()
        Me.chkLista = New System.Windows.Forms.CheckBox()
        Me.chkTelefoneExclusao = New System.Windows.Forms.CheckBox()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnExportar
        '
        Me.btnExportar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnExportar.Location = New System.Drawing.Point(280, 256)
        Me.btnExportar.Name = "btnExportar"
        Me.btnExportar.Size = New System.Drawing.Size(101, 31)
        Me.btnExportar.TabIndex = 0
        Me.btnExportar.Text = "Exportar"
        Me.btnExportar.UseVisualStyleBackColor = True
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Location = New System.Drawing.Point(12, 12)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.SplitContainer1.Panel1.Controls.Add(Me.chkTelefoneExclusao)
        Me.SplitContainer1.Panel1.Controls.Add(Me.chkLista)
        Me.SplitContainer1.Panel1.Controls.Add(Me.chkRequisicao_tb)
        Me.SplitContainer1.Panel1.Controls.Add(Me.chkEnviaEmail)
        Me.SplitContainer1.Panel1.Controls.Add(Me.GroupBox1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.chkReqOi)
        Me.SplitContainer1.Panel1.Controls.Add(Me.btnExportar)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.SplitContainer1.Panel2.Controls.Add(Me.lstDados)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnArquivo)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtArquivo)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnLog)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnImportar)
        Me.SplitContainer1.Size = New System.Drawing.Size(929, 303)
        Me.SplitContainer1.SplitterDistance = 399
        Me.SplitContainer1.TabIndex = 1
        '
        'chkEnviaEmail
        '
        Me.chkEnviaEmail.AutoSize = True
        Me.chkEnviaEmail.Checked = True
        Me.chkEnviaEmail.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkEnviaEmail.Location = New System.Drawing.Point(166, 264)
        Me.chkEnviaEmail.Name = "chkEnviaEmail"
        Me.chkEnviaEmail.Size = New System.Drawing.Size(104, 17)
        Me.chkEnviaEmail.TabIndex = 3
        Me.chkEnviaEmail.Text = "Enviar por e-mail"
        Me.chkEnviaEmail.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dtFim)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.dtInicio)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Location = New System.Drawing.Point(157, 11)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(224, 143)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Periodo:"
        '
        'dtFim
        '
        Me.dtFim.CustomFormat = "dd/MM/yyyy"
        Me.dtFim.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtFim.Location = New System.Drawing.Point(9, 102)
        Me.dtFim.Name = "dtFim"
        Me.dtFim.Size = New System.Drawing.Size(200, 20)
        Me.dtFim.TabIndex = 78
        Me.dtFim.Value = New Date(2018, 9, 18, 0, 0, 0, 0)
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 83)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 16)
        Me.Label2.TabIndex = 77
        Me.Label2.Text = "Data Fim:"
        '
        'dtInicio
        '
        Me.dtInicio.CustomFormat = "dd/MM/yyyy"
        Me.dtInicio.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtInicio.Location = New System.Drawing.Point(9, 53)
        Me.dtInicio.Name = "dtInicio"
        Me.dtInicio.Size = New System.Drawing.Size(200, 20)
        Me.dtInicio.TabIndex = 76
        Me.dtInicio.Value = New Date(2018, 9, 18, 0, 0, 0, 0)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 32)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(74, 16)
        Me.Label3.TabIndex = 75
        Me.Label3.Text = "Data Inicio:"
        '
        'chkReqOi
        '
        Me.chkReqOi.AutoSize = True
        Me.chkReqOi.Checked = True
        Me.chkReqOi.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkReqOi.Location = New System.Drawing.Point(18, 11)
        Me.chkReqOi.Name = "chkReqOi"
        Me.chkReqOi.Size = New System.Drawing.Size(92, 17)
        Me.chkReqOi.TabIndex = 1
        Me.chkReqOi.Text = "Requisição Oi"
        Me.chkReqOi.UseVisualStyleBackColor = True
        '
        'lstDados
        '
        Me.lstDados.FormattingEnabled = True
        Me.lstDados.Location = New System.Drawing.Point(9, 49)
        Me.lstDados.Name = "lstDados"
        Me.lstDados.Size = New System.Drawing.Size(508, 147)
        Me.lstDados.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Caminho"
        '
        'btnArquivo
        '
        Me.btnArquivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnArquivo.Location = New System.Drawing.Point(365, 21)
        Me.btnArquivo.Name = "btnArquivo"
        Me.btnArquivo.Size = New System.Drawing.Size(35, 23)
        Me.btnArquivo.TabIndex = 4
        Me.btnArquivo.Text = "..."
        Me.btnArquivo.UseVisualStyleBackColor = True
        '
        'txtArquivo
        '
        Me.txtArquivo.Location = New System.Drawing.Point(9, 23)
        Me.txtArquivo.Name = "txtArquivo"
        Me.txtArquivo.Size = New System.Drawing.Size(350, 20)
        Me.txtArquivo.TabIndex = 3
        '
        'btnLog
        '
        Me.btnLog.Location = New System.Drawing.Point(9, 260)
        Me.btnLog.Name = "btnLog"
        Me.btnLog.Size = New System.Drawing.Size(75, 23)
        Me.btnLog.TabIndex = 2
        Me.btnLog.Text = "Log"
        Me.btnLog.UseVisualStyleBackColor = True
        '
        'btnImportar
        '
        Me.btnImportar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImportar.Location = New System.Drawing.Point(211, 256)
        Me.btnImportar.Name = "btnImportar"
        Me.btnImportar.Size = New System.Drawing.Size(101, 31)
        Me.btnImportar.TabIndex = 1
        Me.btnImportar.Text = "Importar"
        Me.btnImportar.UseVisualStyleBackColor = True
        '
        'ofd1
        '
        Me.ofd1.FileName = "OpenFileDialog1"
        '
        'btnSair
        '
        Me.btnSair.Location = New System.Drawing.Point(865, 332)
        Me.btnSair.Name = "btnSair"
        Me.btnSair.Size = New System.Drawing.Size(75, 23)
        Me.btnSair.TabIndex = 3
        Me.btnSair.Text = "Sair"
        Me.btnSair.UseVisualStyleBackColor = True
        '
        'chkRequisicao_tb
        '
        Me.chkRequisicao_tb.AutoSize = True
        Me.chkRequisicao_tb.Location = New System.Drawing.Point(18, 34)
        Me.chkRequisicao_tb.Name = "chkRequisicao_tb"
        Me.chkRequisicao_tb.Size = New System.Drawing.Size(79, 17)
        Me.chkRequisicao_tb.TabIndex = 4
        Me.chkRequisicao_tb.Text = "Requisição"
        Me.chkRequisicao_tb.UseVisualStyleBackColor = True
        '
        'chkLista
        '
        Me.chkLista.AutoSize = True
        Me.chkLista.Location = New System.Drawing.Point(18, 57)
        Me.chkLista.Name = "chkLista"
        Me.chkLista.Size = New System.Drawing.Size(101, 17)
        Me.chkLista.TabIndex = 5
        Me.chkLista.Text = "Lista Telefonica"
        Me.chkLista.UseVisualStyleBackColor = True
        '
        'chkTelefoneExclusao
        '
        Me.chkTelefoneExclusao.AutoSize = True
        Me.chkTelefoneExclusao.Location = New System.Drawing.Point(18, 80)
        Me.chkTelefoneExclusao.Name = "chkTelefoneExclusao"
        Me.chkTelefoneExclusao.Size = New System.Drawing.Size(114, 17)
        Me.chkTelefoneExclusao.TabIndex = 6
        Me.chkTelefoneExclusao.Text = "Telefone Exclusão"
        Me.chkTelefoneExclusao.UseVisualStyleBackColor = True
        '
        'FrmExportar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(952, 367)
        Me.Controls.Add(Me.btnSair)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Name = "FrmExportar"
        Me.Text = "FrmExportar"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnExportar As System.Windows.Forms.Button
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents btnImportar As System.Windows.Forms.Button
    Friend WithEvents btnLog As System.Windows.Forms.Button
    Friend WithEvents ofd1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnArquivo As System.Windows.Forms.Button
    Friend WithEvents txtArquivo As System.Windows.Forms.TextBox
    Friend WithEvents lstDados As System.Windows.Forms.ListBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkReqOi As System.Windows.Forms.CheckBox
    Friend WithEvents btnSair As System.Windows.Forms.Button
    Friend WithEvents dtFim As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dtInicio As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents chkEnviaEmail As System.Windows.Forms.CheckBox
    Friend WithEvents chkTelefoneExclusao As System.Windows.Forms.CheckBox
    Friend WithEvents chkLista As System.Windows.Forms.CheckBox
    Friend WithEvents chkRequisicao_tb As System.Windows.Forms.CheckBox
End Class
