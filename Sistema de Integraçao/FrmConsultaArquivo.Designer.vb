﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmConsultaArquivo
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnPesquisar = New System.Windows.Forms.Button()
        Me.gridRetorno = New System.Windows.Forms.DataGridView()
        Me.OpenFileDialog = New System.Windows.Forms.OpenFileDialog()
        Me.lblTipoArq = New System.Windows.Forms.Label()
        Me.lblArquivo = New System.Windows.Forms.Label()
        Me.lblTotalReg = New System.Windows.Forms.Label()
        Me.lblTotalArquivo = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lblStatusArquivo = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblDataArquivo = New System.Windows.Forms.Label()
        Me.btnSair = New System.Windows.Forms.Button()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblContestado = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.lblAltConta = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.lblArrecadado = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.lblCriticado = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.lblFaturado = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblAfaturar = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cboArquivo = New System.Windows.Forms.ComboBox()
        CType(Me.gridRetorno, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(28, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(46, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Arquivo:"
        '
        'btnPesquisar
        '
        Me.btnPesquisar.Location = New System.Drawing.Point(598, 26)
        Me.btnPesquisar.Name = "btnPesquisar"
        Me.btnPesquisar.Size = New System.Drawing.Size(133, 23)
        Me.btnPesquisar.TabIndex = 2
        Me.btnPesquisar.Text = "Pesquisar"
        Me.btnPesquisar.UseVisualStyleBackColor = True
        '
        'gridRetorno
        '
        Me.gridRetorno.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridRetorno.Location = New System.Drawing.Point(31, 64)
        Me.gridRetorno.Name = "gridRetorno"
        Me.gridRetorno.Size = New System.Drawing.Size(868, 523)
        Me.gridRetorno.TabIndex = 3
        '
        'OpenFileDialog
        '
        Me.OpenFileDialog.FileName = "OpenFileDialog"
        '
        'lblTipoArq
        '
        Me.lblTipoArq.AutoSize = True
        Me.lblTipoArq.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoArq.Location = New System.Drawing.Point(20, 31)
        Me.lblTipoArq.Name = "lblTipoArq"
        Me.lblTipoArq.Size = New System.Drawing.Size(104, 16)
        Me.lblTipoArq.TabIndex = 6
        Me.lblTipoArq.Text = "Tipo de Arquivo"
        '
        'lblArquivo
        '
        Me.lblArquivo.AutoSize = True
        Me.lblArquivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblArquivo.Location = New System.Drawing.Point(19, 59)
        Me.lblArquivo.Name = "lblArquivo"
        Me.lblArquivo.Size = New System.Drawing.Size(0, 16)
        Me.lblArquivo.TabIndex = 7
        '
        'lblTotalReg
        '
        Me.lblTotalReg.AutoSize = True
        Me.lblTotalReg.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalReg.Location = New System.Drawing.Point(174, 58)
        Me.lblTotalReg.Name = "lblTotalReg"
        Me.lblTotalReg.Size = New System.Drawing.Size(16, 16)
        Me.lblTotalReg.TabIndex = 8
        Me.lblTotalReg.Text = "0"
        '
        'lblTotalArquivo
        '
        Me.lblTotalArquivo.AutoSize = True
        Me.lblTotalArquivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalArquivo.Location = New System.Drawing.Point(20, 111)
        Me.lblTotalArquivo.Name = "lblTotalArquivo"
        Me.lblTotalArquivo.Size = New System.Drawing.Size(16, 16)
        Me.lblTotalArquivo.TabIndex = 9
        Me.lblTotalArquivo.Text = "0"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(173, 31)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(181, 16)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Qtd. de Registros  no arquivo"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(20, 84)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(136, 16)
        Me.Label5.TabIndex = 11
        Me.Label5.Text = "Valor total do Aqruivo"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.lblStatusArquivo)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.lblArquivo)
        Me.GroupBox1.Controls.Add(Me.lblDataArquivo)
        Me.GroupBox1.Controls.Add(Me.lblTipoArq)
        Me.GroupBox1.Controls.Add(Me.lblTotalReg)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.lblTotalArquivo)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Location = New System.Drawing.Point(905, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(360, 210)
        Me.GroupBox1.TabIndex = 12
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Sobre o arquivo"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(20, 142)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(94, 16)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Status Arquivo"
        '
        'lblStatusArquivo
        '
        Me.lblStatusArquivo.AutoSize = True
        Me.lblStatusArquivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStatusArquivo.Location = New System.Drawing.Point(20, 167)
        Me.lblStatusArquivo.Name = "lblStatusArquivo"
        Me.lblStatusArquivo.Size = New System.Drawing.Size(0, 16)
        Me.lblStatusArquivo.TabIndex = 14
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(174, 84)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(105, 16)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "Data do Arquivo"
        '
        'lblDataArquivo
        '
        Me.lblDataArquivo.AutoSize = True
        Me.lblDataArquivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDataArquivo.Location = New System.Drawing.Point(174, 109)
        Me.lblDataArquivo.Name = "lblDataArquivo"
        Me.lblDataArquivo.Size = New System.Drawing.Size(0, 16)
        Me.lblDataArquivo.TabIndex = 12
        '
        'btnSair
        '
        Me.btnSair.Location = New System.Drawing.Point(766, 603)
        Me.btnSair.Name = "btnSair"
        Me.btnSair.Size = New System.Drawing.Size(133, 23)
        Me.btnSair.TabIndex = 13
        Me.btnSair.Text = "Sair"
        Me.btnSair.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblContestado)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.lblAltConta)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.lblArrecadado)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.lblCriticado)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.lblFaturado)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.lblAfaturar)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Location = New System.Drawing.Point(905, 236)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(360, 356)
        Me.GroupBox2.TabIndex = 14
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Resumo Mensal"
        '
        'lblContestado
        '
        Me.lblContestado.AutoSize = True
        Me.lblContestado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContestado.Location = New System.Drawing.Point(21, 331)
        Me.lblContestado.Name = "lblContestado"
        Me.lblContestado.Size = New System.Drawing.Size(59, 16)
        Me.lblContestado.TabIndex = 21
        Me.lblContestado.Text = "R$ 0,00"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(20, 304)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(77, 16)
        Me.Label10.TabIndex = 22
        Me.Label10.Text = "Contestado"
        '
        'lblAltConta
        '
        Me.lblAltConta.AutoSize = True
        Me.lblAltConta.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAltConta.Location = New System.Drawing.Point(18, 271)
        Me.lblAltConta.Name = "lblAltConta"
        Me.lblAltConta.Size = New System.Drawing.Size(59, 16)
        Me.lblAltConta.TabIndex = 19
        Me.lblAltConta.Text = "R$ 0,00"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(17, 244)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(124, 16)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "Alteração Validade"
        '
        'lblArrecadado
        '
        Me.lblArrecadado.AutoSize = True
        Me.lblArrecadado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblArrecadado.Location = New System.Drawing.Point(18, 216)
        Me.lblArrecadado.Name = "lblArrecadado"
        Me.lblArrecadado.Size = New System.Drawing.Size(59, 16)
        Me.lblArrecadado.TabIndex = 17
        Me.lblArrecadado.Text = "R$ 0,00"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(17, 189)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(80, 16)
        Me.Label13.TabIndex = 18
        Me.Label13.Text = "Arrecadado"
        '
        'lblCriticado
        '
        Me.lblCriticado.AutoSize = True
        Me.lblCriticado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCriticado.Location = New System.Drawing.Point(20, 160)
        Me.lblCriticado.Name = "lblCriticado"
        Me.lblCriticado.Size = New System.Drawing.Size(59, 16)
        Me.lblCriticado.TabIndex = 15
        Me.lblCriticado.Text = "R$ 0,00"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(19, 133)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(61, 16)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "Criticado"
        '
        'lblFaturado
        '
        Me.lblFaturado.AutoSize = True
        Me.lblFaturado.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFaturado.Location = New System.Drawing.Point(18, 105)
        Me.lblFaturado.Name = "lblFaturado"
        Me.lblFaturado.Size = New System.Drawing.Size(59, 16)
        Me.lblFaturado.TabIndex = 13
        Me.lblFaturado.Text = "R$ 0,00"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(17, 78)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(62, 16)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "Faturado"
        '
        'lblAfaturar
        '
        Me.lblAfaturar.AutoSize = True
        Me.lblAfaturar.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAfaturar.Location = New System.Drawing.Point(18, 52)
        Me.lblAfaturar.Name = "lblAfaturar"
        Me.lblAfaturar.Size = New System.Drawing.Size(59, 16)
        Me.lblAfaturar.TabIndex = 11
        Me.lblAfaturar.Text = "R$ 0,00"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(17, 25)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(62, 16)
        Me.Label7.TabIndex = 12
        Me.Label7.Text = "A Faturar"
        '
        'cboArquivo
        '
        Me.cboArquivo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboArquivo.FormattingEnabled = True
        Me.cboArquivo.Location = New System.Drawing.Point(80, 28)
        Me.cboArquivo.Name = "cboArquivo"
        Me.cboArquivo.Size = New System.Drawing.Size(496, 21)
        Me.cboArquivo.TabIndex = 15
        '
        'FrmConsultaArquivo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1277, 640)
        Me.Controls.Add(Me.cboArquivo)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.btnSair)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.gridRetorno)
        Me.Controls.Add(Me.btnPesquisar)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.Name = "FrmConsultaArquivo"
        Me.Text = "Retorno"
        CType(Me.gridRetorno, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnPesquisar As System.Windows.Forms.Button
    Friend WithEvents gridRetorno As System.Windows.Forms.DataGridView
    Friend WithEvents OpenFileDialog As System.Windows.Forms.OpenFileDialog
    Friend WithEvents lblTipoArq As System.Windows.Forms.Label
    Friend WithEvents lblArquivo As System.Windows.Forms.Label
    Friend WithEvents lblTotalReg As System.Windows.Forms.Label
    Friend WithEvents lblTotalArquivo As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblDataArquivo As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblStatusArquivo As System.Windows.Forms.Label
    Friend WithEvents btnSair As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblArrecadado As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lblCriticado As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents lblFaturado As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblAfaturar As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblAltConta As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents lblContestado As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cboArquivo As System.Windows.Forms.ComboBox
End Class
