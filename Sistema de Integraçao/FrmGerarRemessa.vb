﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text


Public Class FrmGerarRemessa

#Region "Enum"

    Private Enum ColunasGridRemessa

        SelecaoContribuinte = 0
        COD_CLIENTE = 1
        Cod_EAN_CLIENTE = 2
        Nome = 3
        DDD = 4
        Telefone = 5
        Valor = 6
        bairro = 7
        cidade = 8
        UF = 9
        Data_Cadastro = 10
        Categoria = 11
        Operador = 12
        dia_limite = 13

    End Enum

    Enum xlsOption
        xlsOpen
    End Enum

#End Region

#Region "variaveis"

    Private lstContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstContribuinteSelecionada As New List(Of CContribuinte)
    Private v_ambiente As String
    Private rnContribuinte As RNContribuinte = New RNContribuinte
    Private contribuinte As CContribuinte = New CContribuinte

#End Region



#Region "Metodos"

    Private Sub FrmGerarRemessa_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        CriarColunasGrid()

        carregaGrid()



    End Sub

    Private Sub carregaGrid()

        Dim objContribuintes As CContribuinte = New CContribuinte()

        Dim bindingSourceApolice As BindingSource = New BindingSource

        Try

        

        lstContribuintes = New SortableBindingList(Of CContribuinte)(rnContribuinte.CarregarGridOi(objContribuintes, "Remessa"))

        If (lstContribuintes.Count > 0) Then

            bindingSourceApolice.DataSource = lstContribuintes

            grdRemessa.DataSource = bindingSourceApolice.DataSource
            'grdRemessa.Sort(grdRemessa.Columns(ColunasGridRemessa.Nome), ListSortDirection.Ascending)
            'lblNumRegistros.Text = lstApolices.Count


            End If


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CriarColunasGrid()

        Try

            grdRemessa.DataSource = Nothing
            grdRemessa.Columns.Clear()

            grdRemessa.AutoGenerateColumns = False

            grdRemessa.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            '0

            Dim checkboxColumn As New DataGridViewCheckBoxColumn
            checkboxColumn.Width = 32
            checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Codcliente"
            column.HeaderText = "Código do Cliente"
            column.Width = 60
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)


            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "COD_EAN_CLIENTE"
            column.HeaderText = "Código de Barras"
            column.Width = 80
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "NomeCliente1"
            column.HeaderText = "Nome"
            column.Width = 300
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DDD"
            column.HeaderText = "DDD"
            column.Width = 50
            column.ReadOnly = True
            column.Visible = False
            column.Visible = True
            grdRemessa.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Telefone1"
            column.HeaderText = "Telefone"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = False
            column.Visible = True
            grdRemessa.Columns.Add(column)

            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "VALOR"
            column.HeaderText = "Valor"
            column.Visible = True
            column.DefaultCellStyle.Format = "0.00"
            column.Width = 80
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '7
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "BAIRRO"
            column.HeaderText = "bairro"
            column.Width = 160
            column.ReadOnly = True
            column.Visible = False
            column.Visible = True
            grdRemessa.Columns.Add(column)


            '8
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "CIDADE"
            column.HeaderText = "cidade"
            column.Visible = True
            column.Width = 150
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)


            '8
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "UF"
            column.HeaderText = "UF"
            column.Width = 40
            column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            column.Visible = True
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)


            '9
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DataCadastro"
            column.HeaderText = "Data Cadastro"
            column.Visible = True
            column.Width = 100
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '10
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "COD_CATEGORIA"
            column.HeaderText = "Categoria"
            column.Visible = True
            column.Width = 60
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '11
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "categoria"
            column.HeaderText = "Categoria"
            column.Width = 110
            column.ReadOnly = True
            column.Visible = False
            column.Visible = True
            grdRemessa.Columns.Add(column)

            '12
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "OPERADOR"
            column.HeaderText = "Operador"
            column.Width = 110
            column.ReadOnly = True
            column.Visible = False
            column.Visible = True
            grdRemessa.Columns.Add(column)


            '13
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DiaLimite"
            column.HeaderText = "Dia Limite"
            column.Visible = True
            column.Width = 50
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

#End Region

    Private Sub btnGerarRemessa_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGerarRemessa.Click
        Try
            Dim objContribuinte As CContribuinte = New CContribuinte

            Me.Cursor = Cursors.WaitCursor


            ''gera o arquivo com as pessoas selecionadas na grid
            If Executar() = True Then
                MsgBox("Arquivo Gerado com sucesso!!")
            End If

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            ' Show the exception's message.
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Public Function AProcessar(ByVal cliente As CContribuinte) As Boolean

        Dim mesAnoRef As String = ""

        Try

            mesAnoRef = dtMesAnoRef.Value
            mesAnoRef = Replace(mesAnoRef.Remove(0, 2), "/", "")


            rnContribuinte.cargaContribuinte(cliente, mesAnoRef)

            Return True

        Catch ex As Exception
            Return False
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try

    End Function

    Public Function Executar() As Boolean

        Try



            rnContribuinte.CargaArquivo(contribuinte.Operador, 1)
            rnContribuinte.CargaArquivo(contribuinte.Operador, 2)
            rnContribuinte.CargaArquivo(contribuinte.Operador, 3)
            Return True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try

    End Function




    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click

    End Sub

    Private Sub carregaGridClienteOI()

        Dim objContribuintes As CContribuinte = New CContribuinte()

        Dim bindingSourceApolice As BindingSource = New BindingSource

        Try

            lstContribuintes = New SortableBindingList(Of CContribuinte)(rnContribuinte.CarregarGridOi(objContribuintes, "Remessa"))

            If (lstContribuintes.Count > 0) Then

                bindingSourceApolice.DataSource = lstContribuintes

                grdRemessa.DataSource = bindingSourceApolice.DataSource
                'grdRemessa.Sort(grdRemessa.Columns(ColunasGridRemessa.Nome), ListSortDirection.Ascending)
                'lblNumRegistros.Text = lstApolices.Count


            End If

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub


    Private Sub btnRetorno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRetorno.Click
        Try

            FrmRetorno.Show()

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Close()
    End Sub

    Private Sub btnIncluirManual_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIncluirManual.Click
        FrmInclusaoManual.Show()
    End Sub
End Class