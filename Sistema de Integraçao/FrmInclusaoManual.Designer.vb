﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmInclusaoManual
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtDDD = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtValor = New System.Windows.Forms.TextBox()
        Me.Dados = New System.Windows.Forms.GroupBox()
        Me.txtValorFicha = New System.Windows.Forms.TextBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.chkNaoConforme = New System.Windows.Forms.CheckBox()
        Me.chkAceite = New System.Windows.Forms.CheckBox()
        Me.cboCategoria = New System.Windows.Forms.ComboBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.txtMesRef = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.cboOperador = New System.Windows.Forms.ComboBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txtCodEan = New System.Windows.Forms.MaskedTextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtNome = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtTelefone = New System.Windows.Forms.TextBox()
        Me.btnSair = New System.Windows.Forms.Button()
        Me.btnExcluir = New System.Windows.Forms.Button()
        Me.btnAlterar = New System.Windows.Forms.Button()
        Me.btnSalvar = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.txtEmailContato = New System.Windows.Forms.TextBox()
        Me.txtWhatsApp = New System.Windows.Forms.TextBox()
        Me.txtCpf = New System.Windows.Forms.MaskedTextBox()
        Me.rtxtObs = New System.Windows.Forms.RichTextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtAutorizante = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.btnNaoContribuiu = New System.Windows.Forms.Button()
        Me.btnComercial = New System.Windows.Forms.Button()
        Me.Dados.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(30, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Código EAN"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(56, 109)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(31, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "DDD"
        '
        'txtDDD
        '
        Me.txtDDD.Location = New System.Drawing.Point(101, 106)
        Me.txtDDD.Name = "txtDDD"
        Me.txtDDD.Size = New System.Drawing.Size(45, 20)
        Me.txtDDD.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(56, 143)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(31, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Valor"
        '
        'txtValor
        '
        Me.txtValor.Location = New System.Drawing.Point(101, 140)
        Me.txtValor.Name = "txtValor"
        Me.txtValor.Size = New System.Drawing.Size(95, 20)
        Me.txtValor.TabIndex = 6
        '
        'Dados
        '
        Me.Dados.Controls.Add(Me.txtValorFicha)
        Me.Dados.Controls.Add(Me.Label44)
        Me.Dados.Controls.Add(Me.chkNaoConforme)
        Me.Dados.Controls.Add(Me.chkAceite)
        Me.Dados.Controls.Add(Me.cboCategoria)
        Me.Dados.Controls.Add(Me.Label28)
        Me.Dados.Controls.Add(Me.txtMesRef)
        Me.Dados.Controls.Add(Me.Label6)
        Me.Dados.Controls.Add(Me.cboOperador)
        Me.Dados.Controls.Add(Me.Label26)
        Me.Dados.Controls.Add(Me.txtCodEan)
        Me.Dados.Controls.Add(Me.Label5)
        Me.Dados.Controls.Add(Me.txtNome)
        Me.Dados.Controls.Add(Me.Label4)
        Me.Dados.Controls.Add(Me.txtTelefone)
        Me.Dados.Controls.Add(Me.Label1)
        Me.Dados.Controls.Add(Me.Label3)
        Me.Dados.Controls.Add(Me.txtValor)
        Me.Dados.Controls.Add(Me.Label2)
        Me.Dados.Controls.Add(Me.txtDDD)
        Me.Dados.Location = New System.Drawing.Point(12, 12)
        Me.Dados.Name = "Dados"
        Me.Dados.Size = New System.Drawing.Size(624, 226)
        Me.Dados.TabIndex = 7
        Me.Dados.TabStop = False
        Me.Dados.Text = "Dados"
        '
        'txtValorFicha
        '
        Me.txtValorFicha.Location = New System.Drawing.Point(518, 177)
        Me.txtValorFicha.Name = "txtValorFicha"
        Me.txtValorFicha.Size = New System.Drawing.Size(69, 20)
        Me.txtValorFicha.TabIndex = 146
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.Location = New System.Drawing.Point(422, 180)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(101, 13)
        Me.Label44.TabIndex = 145
        Me.Label44.Text = "Valor da Ficha : "
        '
        'chkNaoConforme
        '
        Me.chkNaoConforme.AutoSize = True
        Me.chkNaoConforme.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNaoConforme.Location = New System.Drawing.Point(100, 179)
        Me.chkNaoConforme.Margin = New System.Windows.Forms.Padding(2)
        Me.chkNaoConforme.Name = "chkNaoConforme"
        Me.chkNaoConforme.Size = New System.Drawing.Size(221, 17)
        Me.chkNaoConforme.TabIndex = 135
        Me.chkNaoConforme.Text = "Grau de parentesco Não conforme"
        Me.chkNaoConforme.UseVisualStyleBackColor = True
        '
        'chkAceite
        '
        Me.chkAceite.AutoSize = True
        Me.chkAceite.Location = New System.Drawing.Point(419, 143)
        Me.chkAceite.Name = "chkAceite"
        Me.chkAceite.Size = New System.Drawing.Size(56, 17)
        Me.chkAceite.TabIndex = 75
        Me.chkAceite.Text = "Aceite"
        Me.chkAceite.UseVisualStyleBackColor = True
        '
        'cboCategoria
        '
        Me.cboCategoria.FormattingEnabled = True
        Me.cboCategoria.Location = New System.Drawing.Point(404, 107)
        Me.cboCategoria.Name = "cboCategoria"
        Me.cboCategoria.Size = New System.Drawing.Size(181, 21)
        Me.cboCategoria.TabIndex = 5
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(401, 88)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(69, 16)
        Me.Label28.TabIndex = 74
        Me.Label28.Text = "Categoria:"
        '
        'txtMesRef
        '
        Me.txtMesRef.CustomFormat = "MM/yyyy"
        Me.txtMesRef.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.txtMesRef.Location = New System.Drawing.Point(297, 140)
        Me.txtMesRef.Name = "txtMesRef"
        Me.txtMesRef.Size = New System.Drawing.Size(92, 20)
        Me.txtMesRef.TabIndex = 7
        Me.txtMesRef.Value = New Date(2016, 12, 1, 0, 0, 0, 0)
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(209, 143)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(82, 13)
        Me.Label6.TabIndex = 60
        Me.Label6.Text = "Mês Referência"
        '
        'cboOperador
        '
        Me.cboOperador.FormattingEnabled = True
        Me.cboOperador.Location = New System.Drawing.Point(346, 28)
        Me.cboOperador.Name = "cboOperador"
        Me.cboOperador.Size = New System.Drawing.Size(239, 21)
        Me.cboOperador.TabIndex = 1
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(263, 29)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(76, 16)
        Me.Label26.TabIndex = 57
        Me.Label26.Text = "Operadora:"
        '
        'txtCodEan
        '
        Me.txtCodEan.Location = New System.Drawing.Point(101, 28)
        Me.txtCodEan.Name = "txtCodEan"
        Me.txtCodEan.Size = New System.Drawing.Size(141, 20)
        Me.txtCodEan.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(56, 68)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(38, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Nome "
        '
        'txtNome
        '
        Me.txtNome.Location = New System.Drawing.Point(100, 65)
        Me.txtNome.Name = "txtNome"
        Me.txtNome.Size = New System.Drawing.Size(485, 20)
        Me.txtNome.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(166, 109)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = "Telefone"
        '
        'txtTelefone
        '
        Me.txtTelefone.Location = New System.Drawing.Point(221, 106)
        Me.txtTelefone.Name = "txtTelefone"
        Me.txtTelefone.Size = New System.Drawing.Size(168, 20)
        Me.txtTelefone.TabIndex = 4
        '
        'btnSair
        '
        Me.btnSair.Location = New System.Drawing.Point(565, 549)
        Me.btnSair.Name = "btnSair"
        Me.btnSair.Size = New System.Drawing.Size(75, 23)
        Me.btnSair.TabIndex = 11
        Me.btnSair.Text = "Sair"
        Me.btnSair.UseVisualStyleBackColor = True
        '
        'btnExcluir
        '
        Me.btnExcluir.Location = New System.Drawing.Point(460, 549)
        Me.btnExcluir.Name = "btnExcluir"
        Me.btnExcluir.Size = New System.Drawing.Size(75, 23)
        Me.btnExcluir.TabIndex = 10
        Me.btnExcluir.Text = "Excluir"
        Me.btnExcluir.UseVisualStyleBackColor = True
        '
        'btnAlterar
        '
        Me.btnAlterar.Enabled = False
        Me.btnAlterar.Location = New System.Drawing.Point(360, 549)
        Me.btnAlterar.Name = "btnAlterar"
        Me.btnAlterar.Size = New System.Drawing.Size(75, 23)
        Me.btnAlterar.TabIndex = 9
        Me.btnAlterar.Text = "Alterar"
        Me.btnAlterar.UseVisualStyleBackColor = True
        '
        'btnSalvar
        '
        Me.btnSalvar.Location = New System.Drawing.Point(254, 549)
        Me.btnSalvar.Name = "btnSalvar"
        Me.btnSalvar.Size = New System.Drawing.Size(75, 23)
        Me.btnSalvar.TabIndex = 8
        Me.btnSalvar.Text = "Salvar"
        Me.btnSalvar.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.txtCpf)
        Me.GroupBox1.Controls.Add(Me.rtxtObs)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtAutorizante)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 244)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(624, 283)
        Me.GroupBox1.TabIndex = 12
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Informações complementares"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label45)
        Me.GroupBox2.Controls.Add(Me.Label46)
        Me.GroupBox2.Controls.Add(Me.txtEmailContato)
        Me.GroupBox2.Controls.Add(Me.txtWhatsApp)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.Blue
        Me.GroupBox2.Location = New System.Drawing.Point(100, 189)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(320, 85)
        Me.GroupBox2.TabIndex = 151
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Contatos"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.Location = New System.Drawing.Point(21, 20)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(42, 13)
        Me.Label45.TabIndex = 147
        Me.Label45.Text = "E-Mail"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.Location = New System.Drawing.Point(2, 49)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(65, 13)
        Me.Label46.TabIndex = 149
        Me.Label46.Text = "WhatsApp"
        '
        'txtEmailContato
        '
        Me.txtEmailContato.Location = New System.Drawing.Point(69, 14)
        Me.txtEmailContato.Name = "txtEmailContato"
        Me.txtEmailContato.Size = New System.Drawing.Size(235, 20)
        Me.txtEmailContato.TabIndex = 146
        '
        'txtWhatsApp
        '
        Me.txtWhatsApp.Location = New System.Drawing.Point(69, 46)
        Me.txtWhatsApp.Name = "txtWhatsApp"
        Me.txtWhatsApp.Size = New System.Drawing.Size(170, 20)
        Me.txtWhatsApp.TabIndex = 148
        '
        'txtCpf
        '
        Me.txtCpf.Location = New System.Drawing.Point(100, 64)
        Me.txtCpf.Name = "txtCpf"
        Me.txtCpf.Size = New System.Drawing.Size(202, 20)
        Me.txtCpf.TabIndex = 17
        '
        'rtxtObs
        '
        Me.rtxtObs.Location = New System.Drawing.Point(100, 102)
        Me.rtxtObs.Name = "rtxtObs"
        Me.rtxtObs.Size = New System.Drawing.Size(486, 81)
        Me.rtxtObs.TabIndex = 18
        Me.rtxtObs.Text = ""
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(23, 105)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(74, 13)
        Me.Label9.TabIndex = 16
        Me.Label9.Text = "Observação : "
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(52, 30)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(42, 13)
        Me.Label7.TabIndex = 14
        Me.Label7.Text = "Titular :"
        '
        'txtAutorizante
        '
        Me.txtAutorizante.Location = New System.Drawing.Point(101, 27)
        Me.txtAutorizante.Name = "txtAutorizante"
        Me.txtAutorizante.Size = New System.Drawing.Size(485, 20)
        Me.txtAutorizante.TabIndex = 11
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(61, 67)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(36, 13)
        Me.Label8.TabIndex = 13
        Me.Label8.Text = "CPF : "
        '
        'btnNaoContribuiu
        '
        Me.btnNaoContribuiu.Location = New System.Drawing.Point(23, 549)
        Me.btnNaoContribuiu.Name = "btnNaoContribuiu"
        Me.btnNaoContribuiu.Size = New System.Drawing.Size(101, 23)
        Me.btnNaoContribuiu.TabIndex = 152
        Me.btnNaoContribuiu.Text = "Não Contribuiu"
        Me.btnNaoContribuiu.UseVisualStyleBackColor = True
        '
        'btnComercial
        '
        Me.btnComercial.Location = New System.Drawing.Point(152, 549)
        Me.btnComercial.Name = "btnComercial"
        Me.btnComercial.Size = New System.Drawing.Size(75, 23)
        Me.btnComercial.TabIndex = 153
        Me.btnComercial.Text = "Comercial"
        Me.btnComercial.UseVisualStyleBackColor = True
        '
        'FrmInclusaoManual
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(656, 584)
        Me.Controls.Add(Me.btnComercial)
        Me.Controls.Add(Me.btnNaoContribuiu)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnSair)
        Me.Controls.Add(Me.btnExcluir)
        Me.Controls.Add(Me.btnAlterar)
        Me.Controls.Add(Me.btnSalvar)
        Me.Controls.Add(Me.Dados)
        Me.Name = "FrmInclusaoManual"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Inclusão Manual"
        Me.Dados.ResumeLayout(False)
        Me.Dados.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtDDD As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtValor As System.Windows.Forms.TextBox
    Friend WithEvents Dados As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtNome As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtTelefone As System.Windows.Forms.TextBox
    Friend WithEvents txtCodEan As System.Windows.Forms.MaskedTextBox
    Friend WithEvents cboOperador As System.Windows.Forms.ComboBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents btnSair As System.Windows.Forms.Button
    Friend WithEvents btnExcluir As System.Windows.Forms.Button
    Friend WithEvents btnAlterar As System.Windows.Forms.Button
    Friend WithEvents btnSalvar As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtMesRef As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboCategoria As System.Windows.Forms.ComboBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents chkAceite As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents txtAutorizante As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents rtxtObs As System.Windows.Forms.RichTextBox
    Friend WithEvents txtCpf As System.Windows.Forms.MaskedTextBox
    Friend WithEvents chkNaoConforme As System.Windows.Forms.CheckBox
    Friend WithEvents txtValorFicha As System.Windows.Forms.TextBox
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents txtEmailContato As System.Windows.Forms.TextBox
    Friend WithEvents txtWhatsApp As System.Windows.Forms.TextBox
    Friend WithEvents btnNaoContribuiu As Button
    Friend WithEvents btnComercial As Button
End Class
