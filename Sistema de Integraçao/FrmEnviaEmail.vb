﻿Imports System
Imports System.Net
Imports System.Net.Mail

Public Class FrmEnviaEmail

    Dim Attachment As System.Net.Mail.Attachment
    Dim Mailmsg As New System.Net.Mail.MailMessage()
    Dim ServidorSMTP As String = ""
    Dim EmailOrigem As String = ""

    Private Sub FrmEnviaEmail_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ServidorSMTP = "smtp.gmail.com"
        EmailOrigem = "aaci.jf.mg@gmail.com"

    End Sub

    Private Sub btnEnvia_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnvia.Click
        Dim i As Integer = 0


        'validação dos dados
        If ServidorSMTP = "" Then
            MsgBox("Informe o nome do servidor SMTP ...!!!", MsgBoxStyle.Information, "Envia Email")
            Exit Sub
        End If

        If EmailOrigem = "" Then
            MsgBox("Informe o endereço de Origem ...!!!", MsgBoxStyle.Information, "Envia Email")
            Exit Sub
        End If

        If txtDestino.Text = "" Then
            MsgBox("Informe o endereço de destino ...!!!", MsgBoxStyle.Information, "Envia Email")
            Exit Sub
        End If

        If txtAssunto.Text = "" Then
            MsgBox("Informe o assunto do email ...!!!", MsgBoxStyle.Information, "Envia Email")
            Exit Sub
        End If


        Dim SmtpServer As New SmtpClient()
        Dim mail As New MailMessage()
        SmtpServer.Credentials = New Net.NetworkCredential("aaci.jf.mg@gmail.com", "kwcmuyymqekemvtz")
        SmtpServer.Host = "smtp.gmail.com"
        SmtpServer.Port = 587 'server para gmail
        SmtpServer.EnableSsl = True
        mail.From = New MailAddress("aaci.jf.mg@gmail.com")
        mail.To.Add(txtDestino.Text) 'Email que recebera a Mensagem
        mail.Subject = txtAssunto.Text
        mail.Body = txtMensagem.Text

        'Anexa os arquivos um por um

        For i = 0 To lstAnexos.Items.Count - 1

            mail.Attachments.Add(New Attachment(lstAnexos.Items(i)))

        Next

        Try

            SmtpServer.Send(mail)
            MsgBox("E-mail enviado com sucesso", vbInformation, "Ok")


        Catch ex As Exception
            MsgBox(ex.Message)
        End Try





    End Sub

    Private Sub btnIncluiAnexo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIncluiAnexo.Click

        'Mostra caixa de dialogo para selecionar anexos

        Dim Conta As Integer


        OpenFileDialog1.CheckFileExists = True

        OpenFileDialog1.Title = "Selecione um arquivo para anexar"

        OpenFileDialog1.ShowDialog()


        For Conta = 0 To UBound(OpenFileDialog1.FileNames)

            lstAnexos.Items.Add(OpenFileDialog1.FileNames(Conta))

        Next
    End Sub

    Private Sub btnExcluiAnexo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcluiAnexo.Click
        'Remove os anexos

        If lstAnexos.SelectedIndex > -1 Then

            lstAnexos.Items.RemoveAt(lstAnexos.SelectedIndex)

        End If
    End Sub

  
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try


            'create the mail message
            Dim SmtpServer As New SmtpClient()
            Dim mail As New MailMessage()
            SmtpServer.Credentials = New Net.NetworkCredential("aaci.jf.mg@gmail.com", "kwcmuyymqekemvtz")
            SmtpServer.Host = "smtp.gmail.com"
            SmtpServer.Port = 587 'server para gmail
            SmtpServer.EnableSsl = True
            mail.From = New MailAddress("aaci.jf.mg@gmail.com")
            mail.To.Add(txtDestino.Text) 'Email que recebera a Mensagem
            mail.Subject = txtAssunto.Text
            mail.Body = txtMensagem.Text

            'Este codigo PlainView e caso o email não suporte Html, nisto ele irá informar este erro

            Dim plainView As AlternateView = AlternateView.CreateAlternateViewFromString("This is my plain text content, viewable by those clients that don't support html", Nothing, "text/plain")

            'crie um corpo de texto caso deseje assim você poderá colocar um texto no meio de duas imagens
            Dim linha1 As String = ""
            If Date.Now.Hour < 12 Then
                linha1 = "Bom dia,"
            Else
                linha1 = "Boa tarde,"
            End If

            Dim linha2 As String = "como solicitado, estamos excluindo o seu cadastro do nosso sistema."
            Dim linha3 As String = "desculpe o&nbsp;transtorno, agrade&ccedil;o a compreens&atilde;o."

            'Vamos Iniciar a Parte das Imagens
            ' as imagens ficam no prefixo 'cid' eles irão pegar os seus ContentId e irão destinar as imagens 
            ' você pode criar varias imagens colocando diferentes ContentId 
            ' Veja: <img src='cid:companylogo'> o LinkedResource ( que veremos logo a baixo ) irá destinar a imagem que tenha o  ContentId com o nome de  'companylogo'

            ' aqui você cria o corpo da mensagem com ele você pode criar e desenvolver mais


            'Dim htmlView As AlternateView = AlternateView.CreateAlternateViewFromString("<img src=cid:companylogo> <p> <class='body'>  " + body + "</div>  <img src=cid:Comlog>   ", Nothing, "text/html")
            Dim htmlView As AlternateView = AlternateView.CreateAlternateViewFromString("<p> " + linha1 + "</p> <p>&nbsp;</p> <p>" + linha2 + "</p> <p>&nbsp;</p> <img src=cid:companylogo> <p>&nbsp;</p> <p>" + linha3 + "</p> <p><br /> <br /> --</p> <p>&nbsp;</p> <p>att..</p> <p>&nbsp;</p> <p><strong>AACI - ASSOCIA&Ccedil;&Atilde;O DE APOIO AS CRIAN&Ccedil;AS E IDOSOS</strong></p>   ", Nothing, "text/html")


            'você irá dizer aonde estão as suas imagens e elas irão para Logo e Logo1

            Dim logo As New LinkedResource("C:\teste\44.JPG")

            Dim logo1 As New LinkedResource("C:\teste\44.JPG")

            ' Lembra do ContentId então aqui que você irá dizer aonde fica as imagens
            ' Olhe ali o htmlview ele tem o ' <img scr=cid:companylogo> ' ele que diz a que a imagem que tenha a propriedade com o nome 'Companylogo' irá ficar aqui
            ' e é nesta parte aonde você diz o nome delas.

            logo.ContentId = "companylogo"
            logo1.ContentId = "Comlog"

            ' ok agora o Sistema sabe que a ContentId da imagem 'logo' tem o nome de 'companylogo' mas ele não sabe aonde você quer adicionar, 
            'por isto você deve informar. 
            ' como temos mais de uma imagem você deve adicionar mais de uma linha conforme visto a baixo.
            htmlView.LinkedResources.Add(logo)
            htmlView.LinkedResources.Add(logo1)

            'ok, agora só criar o email, digitando o Plainview aonde ele irá criar um segundo plano caso o email não aceite html e irá colocar a imagem caso aceite 

            With mail
                .AlternateViews.Add(plainView)
                .AlternateViews.Add(htmlView)

            End With

            mail.IsBodyHtml = True

            SmtpServer.Send(mail)
            MsgBox("E-mail enviado com sucesso", vbInformation, "Ok")



        Catch ex As Exception  'caso haja algum erro crie uma msg de aviso.
            MsgBox(ex)
        Finally
            '  File.Delete("")
        End Try

    End Sub
End Class