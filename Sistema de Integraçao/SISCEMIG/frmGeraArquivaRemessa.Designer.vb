﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmGeraArquivaRemessa
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.grdRemessa = New System.Windows.Forms.DataGridView()
        Me.cbMovimento = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BtnGerarArquivo = New System.Windows.Forms.Button()
        Me.btnSair = New System.Windows.Forms.Button()
        Me.chkHeader = New System.Windows.Forms.CheckBox()
        Me.chkNovasIncluoes = New System.Windows.Forms.CheckBox()
        Me.dtMesAnoRef = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        CType(Me.grdRemessa, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'grdRemessa
        '
        Me.grdRemessa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdRemessa.Location = New System.Drawing.Point(12, 55)
        Me.grdRemessa.Name = "grdRemessa"
        Me.grdRemessa.RowHeadersWidth = 51
        Me.grdRemessa.Size = New System.Drawing.Size(995, 480)
        Me.grdRemessa.TabIndex = 3
        '
        'cbMovimento
        '
        Me.cbMovimento.FormattingEnabled = True
        Me.cbMovimento.Items.AddRange(New Object() {"Selecionar...", "INCLUSÃO", "EXCLUSÃO"})
        Me.cbMovimento.Location = New System.Drawing.Point(12, 24)
        Me.cbMovimento.Name = "cbMovimento"
        Me.cbMovimento.Size = New System.Drawing.Size(129, 21)
        Me.cbMovimento.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 16)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Movimento"
        '
        'BtnGerarArquivo
        '
        Me.BtnGerarArquivo.Location = New System.Drawing.Point(1013, 55)
        Me.BtnGerarArquivo.Name = "BtnGerarArquivo"
        Me.BtnGerarArquivo.Size = New System.Drawing.Size(105, 42)
        Me.BtnGerarArquivo.TabIndex = 6
        Me.BtnGerarArquivo.Text = "Gerar Arquivo"
        Me.BtnGerarArquivo.UseVisualStyleBackColor = True
        '
        'btnSair
        '
        Me.btnSair.Location = New System.Drawing.Point(1013, 103)
        Me.btnSair.Name = "btnSair"
        Me.btnSair.Size = New System.Drawing.Size(105, 46)
        Me.btnSair.TabIndex = 7
        Me.btnSair.Text = "Sair"
        Me.btnSair.UseVisualStyleBackColor = True
        '
        'chkHeader
        '
        Me.chkHeader.AutoSize = True
        Me.chkHeader.Location = New System.Drawing.Point(43, 70)
        Me.chkHeader.Name = "chkHeader"
        Me.chkHeader.Size = New System.Drawing.Size(15, 14)
        Me.chkHeader.TabIndex = 92
        Me.chkHeader.UseVisualStyleBackColor = True
        '
        'chkNovasIncluoes
        '
        Me.chkNovasIncluoes.AutoSize = True
        Me.chkNovasIncluoes.Location = New System.Drawing.Point(147, 28)
        Me.chkNovasIncluoes.Name = "chkNovasIncluoes"
        Me.chkNovasIncluoes.Size = New System.Drawing.Size(150, 17)
        Me.chkNovasIncluoes.TabIndex = 93
        Me.chkNovasIncluoes.Text = "Somente Novas Inclusões"
        Me.chkNovasIncluoes.UseVisualStyleBackColor = True
        '
        'dtMesAnoRef
        '
        Me.dtMesAnoRef.CustomFormat = "dd/MM/yyyy"
        Me.dtMesAnoRef.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtMesAnoRef.Location = New System.Drawing.Point(528, 25)
        Me.dtMesAnoRef.Name = "dtMesAnoRef"
        Me.dtMesAnoRef.Size = New System.Drawing.Size(114, 20)
        Me.dtMesAnoRef.TabIndex = 95
        Me.dtMesAnoRef.Value = New Date(2018, 11, 5, 0, 0, 0, 0)
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(321, 27)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(201, 16)
        Me.Label4.TabIndex = 94
        Me.Label4.Text = "Data para Inicio de Faturamento:"
        '
        'frmGeraArquivaRemessa
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1130, 547)
        Me.Controls.Add(Me.dtMesAnoRef)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.chkNovasIncluoes)
        Me.Controls.Add(Me.chkHeader)
        Me.Controls.Add(Me.btnSair)
        Me.Controls.Add(Me.BtnGerarArquivo)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbMovimento)
        Me.Controls.Add(Me.grdRemessa)
        Me.Name = "frmGeraArquivaRemessa"
        Me.Text = "frmGeraArquivaRemessa"
        CType(Me.grdRemessa, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents grdRemessa As DataGridView
    Friend WithEvents cbMovimento As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents BtnGerarArquivo As Button
    Friend WithEvents btnSair As Button
    Friend WithEvents chkHeader As CheckBox
    Friend WithEvents chkNovasIncluoes As CheckBox
    Friend WithEvents dtMesAnoRef As DateTimePicker
    Friend WithEvents Label4 As Label
End Class
