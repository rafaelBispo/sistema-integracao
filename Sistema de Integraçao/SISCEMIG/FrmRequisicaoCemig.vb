﻿Imports SISINT001
Imports System.Text
Imports Microsoft.Office.Interop
Imports Util
Imports System.IO
Imports System.Reflection

Public Class FrmRequisicaoCemig


    Private lstResumoRequisicao As New SortableBindingList(Of CGenerico)
    Private lstResumoRequisicaoCategoria As New SortableBindingList(Of CGenerico)
    Private lstOperador As New SortableBindingList(Of CGenerico)
    Private lstOperadorSelecionado As New List(Of CGenerico)
    Public bindingSourceRequisicao As BindingSource = New BindingSource

    Private Enum ColunasGridRequisicao

        Operador = 0
        qtd_fichas_confirmadas = 1
        valor_fichas_confirmadas = 2
        qtd_fichas_recusadas = 3
        valor_fichas_recusadas = 4
    End Enum


    Private Sub FrmFinanceiro_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

            CriarColunasGridRequisicao()
            CriarColunasGridRequisicaoCategoria()
            DtMesAnoPesquisa.Value = Date.Now
            CarregaComboCategorias(0)
            CarregaComboCidades(0)
            CarregaComboOperador(0)

            CarregaResumoRequisicao(DtMesAnoPesquisa.Text)

            btnSeparaFichas.Enabled = False


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub CriarColunasGridRequisicao()

        Try

            gridRequisicao.DataSource = Nothing
            gridRequisicao.Columns.Clear()

            gridRequisicao.AutoGenerateColumns = False

            gridRequisicao.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            'Dim checkboxColumn As New DataGridViewCheckBoxColumn
            'checkboxColumn.Width = 32
            'checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "operador"
            column.HeaderText = "Operador"
            column.Width = 120
            column.ReadOnly = True
            gridRequisicao.Columns.Add(column)

            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "QuantidadeConfirmado"
            column.HeaderText = "Quantidade de fichas confirmadas"
            column.Width = 120
            column.ReadOnly = True
            gridRequisicao.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "ValorConfirmado"
            column.HeaderText = "Valor das fichas confirmadas"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            gridRequisicao.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "QuantidadeRecusado"
            column.HeaderText = "Quantidade de fichas recusadas"
            column.Width = 120
            column.ReadOnly = True
            column.Visible = True
            gridRequisicao.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "ValorRecusado"
            column.HeaderText = "Valor das fichas recusadas"
            column.Width = 120
            column.ReadOnly = True
            column.Visible = True
            gridRequisicao.Columns.Add(column)


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CriarColunasGridRequisicaoCategoria()

        Try

            gridRequisicaoCategoria.DataSource = Nothing
            gridRequisicaoCategoria.Columns.Clear()

            gridRequisicaoCategoria.AutoGenerateColumns = False

            gridRequisicaoCategoria.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            'Dim checkboxColumn As New DataGridViewCheckBoxColumn
            'checkboxColumn.Width = 32
            'checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Nome_Categoria"
            column.HeaderText = "Categoria"
            column.Width = 120
            column.ReadOnly = True
            gridRequisicaoCategoria.Columns.Add(column)

            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "QuantidadeConfirmado"
            column.HeaderText = "Quantidade de fichas na requisição"
            column.Width = 120
            column.ReadOnly = True
            gridRequisicaoCategoria.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "ValorConfirmado"
            column.HeaderText = "Valor das fichas na requisição"
            column.DefaultCellStyle.Format = "R$0.00"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            gridRequisicaoCategoria.Columns.Add(column)


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CarregaResumoRequisicao(ByVal mesRef As String)

        Dim objRetorno As CRetorno = New CRetorno()
        Dim rn As RNGenerico = New RNGenerico()
        Dim bindingSourceApolice As BindingSource = New BindingSource
        Dim ds As DataSet = New DataSet

        Dim valor As Single
        Dim strValor As String

        Try

            ds = rn.RetornaRequisicaoCemig()

            If ds.Tables(0).Rows.Count > 0 Then

                lblQuantidadeReq.Text = ds.Tables(0).Rows(0).Item(0)

                strValor = valor.ToString("C")
                valor = IIf(IsDBNull(ds.Tables(0).Rows(0).Item(1)), 0, ds.Tables(0).Rows(0).Item(1))
                lblValorReq.Text = valor.ToString("R$ #,###.00")

            End If


            lstResumoRequisicaoCategoria = New SortableBindingList(Of CGenerico)(rn.RetornaRequisicaoPorCategoriaCemig())

            If (lstResumoRequisicaoCategoria.Count > 0) Then

                bindingSourceRequisicao.DataSource = lstResumoRequisicaoCategoria

                gridRequisicaoCategoria.DataSource = bindingSourceRequisicao.DataSource

            End If


            lstResumoRequisicao = New SortableBindingList(Of CGenerico)(rn.RetornaResumoRequisicaoManual(mesRef))

            If (lstResumoRequisicao.Count > 0) Then

                bindingSourceRequisicao.DataSource = lstResumoRequisicao

                gridRequisicao.DataSource = bindingSourceRequisicao.DataSource

            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub carregaFichasFaturamento(ByVal cidade As String, ByVal dia As Integer, ByVal categoria_id As Integer)
        Dim objRetorno As CRetorno = New CRetorno()
        Dim dt As DataTable = New DataTable
        Dim ds As DataSet = New DataSet
        Dim dr As DataRow
        Dim valor As Single = 0
        Dim strValor As String = ""
        Dim totalValorFichas As Decimal = 0
        Dim rnRequisicao As RNRequisicao = New RNRequisicao


        Try

            gridResultado.DataSource = Nothing

            ds = rnRequisicao.CarregaFichasRequisicaoCemigPorCidadeDia(cidade, dia, categoria_id)

            If Not ds Is Nothing Then
                If (ds.Tables(0).Rows.Count > 0) Then
                    If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                        dt = ds.Tables(0)
                    End If
                End If

                gridResultado.DataSource = dt


                If (gridResultado.Rows.Count = 0) Then

                    gridResultado.DataSource = Nothing
                    ' MsgBox("Não existe registros a serem processados!")

                Else

                End If
            Else
                gridResultado.DataSource = Nothing
                ' MsgBox("Não existe registros a serem processados!")
            End If

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub


    Private Sub CarregaComboCidades(ByVal cod_cidade As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCidade.DataSource = Nothing
            cboCidade.Items.Clear()
            cboCidade.DisplayMember = "Nome_cidade"
            cboCidade.ValueMember = "Cod_Cidade"
            cboCidade.DataSource = rn_generico.BuscarCidades(0)

            If cboCidade.Items.Count = 0 Then
                cboCidade.SelectedIndex = 0
            ElseIf cboCidade.Items.Count > 0 And cod_cidade <> 0 Then

                For i As Integer = 0 To cboCidade.Items.Count - 1
                    For Each linha As CGenerico In cboCidade.DataSource
                        If linha.Cod_Cidade = cod_cidade Then
                            cboCidade.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next

            Else

                cboCidade.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub


    Private Sub btnCalcular_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim rn As RNGenerico = New RNGenerico()
        Dim ds As DataSet
        Dim valorOp1 As Decimal = 0
        Dim valorOp2 As Decimal = 0
        Dim valorOp3 As Decimal = 0
        Dim valorOp4 As Decimal = 0
        Dim valor As Single
        Dim strValor As String

        Try

            'PARA OS MENSAIS QUE TIVERAM A RECUSA DO ESPECIAL DEVE SE CONSIDERAR O VALOR INTEIRO
            ds = rn.NomeOperadorPorCategoria(3) '3 = mensal

            If ds.Tables(0).Rows.Count > 0 Then
                For Each linha As DataRow In ds.Tables(0).Rows

                    If lblOperador1.Text = linha.Item(0).ToString Then
                        valorOp1 = (CInt(lblOp1ValorConf.Text) + CInt(lblOp1ValorRecusada.Text)) * CInt(txtCoeficiente.Text) \ 100
                        strValor = valor.ToString("C")
                        valor = valorOp1
                        lblValComissaoOp1.Text = valor.ToString("R$ #,###.00")
                    End If
                    If lblOperador2.Text = linha.Item(0).ToString Then
                        valorOp2 = (CInt(lblOp2ValorConf.Text) + CInt(lblOp2ValorRecusada.Text)) * CInt(txtCoeficiente.Text) \ 100
                        strValor = valor.ToString("C")
                        valor = valorOp2
                        lblValComissaoOp2.Text = valor.ToString("R$ #,###.00")
                    End If
                    If lblOperador3.Text = linha.Item(0).ToString Then
                        valorOp3 = (CInt(lblOp3ValorConf.Text) + CInt(lblOp3ValorRecusada.Text)) * CInt(txtCoeficiente.Text) \ 100
                        strValor = valor.ToString("C")
                        valor = valorOp3
                        lblValComissaoOp3.Text = valor.ToString("R$ #,###.00")
                    End If
                    If lblOperador4.Text = linha.Item(0).ToString Then
                        valorOp4 = (CInt(lblOp4ValorConf.Text) + CInt(lblOp4ValorRecusada.Text)) * CInt(txtCoeficiente.Text) \ 100
                        strValor = valor.ToString("C")
                        valor = valorOp4
                        lblValComissaoOp4.Text = valor.ToString("R$ #,###.00")
                    End If

                Next

            End If

            If valorOp1 = 0 Then
                valorOp1 = CInt(lblOp1ValorConf.Text) * CInt(txtCoeficiente.Text) \ 100
                strValor = valor.ToString("C")
                valor = valorOp1
                lblValComissaoOp1.Text = valor.ToString("R$ #,###.00")
            End If

            If valorOp2 = 0 Then
                valorOp2 = CInt(lblOp2ValorConf.Text) * CInt(txtCoeficiente.Text) \ 100
                strValor = valor.ToString("C")
                valor = valorOp2
                lblValComissaoOp2.Text = valor.ToString("R$ #,###.00")
            End If

            If valorOp3 = 0 Then
                valorOp3 = CInt(lblOp3ValorConf.Text) * CInt(txtCoeficiente.Text) \ 100
                strValor = valor.ToString("C")
                valor = valorOp3
                lblValComissaoOp3.Text = valor.ToString("R$ #,###.00")
            End If

            If valorOp4 = 0 Then
                valorOp4 = CInt(lblOp4ValorConf.Text) * CInt(txtCoeficiente.Text) \ 100
                strValor = valor.ToString("C")
                valor = valorOp4
                lblValComissaoOp4.Text = valor.ToString("R$ #,###.00")
            End If






        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub


    Private Sub gridRequisicao_CellDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridRequisicao.CellDoubleClick
        Dim OperadorClicado As String

        Try

            OperadorClicado = CType(gridRequisicao.Rows(e.RowIndex).Cells(ColunasGridRequisicao.Operador).Value, String)


            If (e.RowIndex > -1) Then
                Dim frm As FrmConsultaRequisicaoRealizada = New FrmConsultaRequisicaoRealizada
                frm.MesRef = DtMesAnoPesquisa.Value
                frm.Operador = OperadorClicado

                If frm.Operador <> "" Then
                    frm.Selecionado = 1
                    Me.Visible = True
                    frm.ShowDialog()
                End If
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnGerarRequisicao_Click(sender As System.Object, e As System.EventArgs)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim mesSeguinte As String
        Dim NextMonth As Date
        Dim gerou As Boolean = False

        Try

            Me.Cursor = Cursors.WaitCursor

            NextMonth = DateAdd(DateInterval.Month, 1, CDate(DtMesAnoPesquisa.Text))
            mesSeguinte = NextMonth.Month & "/" & NextMonth.Year

            Dim result = MessageBox.Show("Deseja realizar o processamento da requisição, Deseja continuar?", "Atenção",
                              MessageBoxButtons.YesNo,
                              MessageBoxIcon.Question)

            If (result = DialogResult.Yes) Then
                If rn_generico.ValidaRemessaManual(DtMesAnoPesquisa.Text) = True Then

                    Dim result2 = MessageBox.Show("O processamento para o mês " & DtMesAnoPesquisa.Text & " já foi realizado, deseja reprocessar?", "Atenção",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question)

                    If (result2 = DialogResult.Yes) Then
                        If (rn_generico.GerarRequisicaoManual(DtMesAnoPesquisa.Text, mesSeguinte, "AACI")) = True Then

                            MsgBox("Operação realizada com sucesso!!", MsgBoxStyle.OkOnly, "Atenção")
                            gerou = True
                        Else
                            MsgBox("Ocorreu um erro ao gerar a requisição, verifique com o responsável", MsgBoxStyle.OkOnly, "Atenção")
                            gerou = True
                        End If
                    End If

                End If

                If gerou = False Then
                    If (rn_generico.GerarRequisicaoManual(DtMesAnoPesquisa.Text, mesSeguinte, "AACI")) = True Then

                        MsgBox("Operação realizada com sucesso!!", MsgBoxStyle.OkOnly, "Atenção")
                        gerou = True
                    Else
                        MsgBox("Ocorreu um erro ao gerar a requisição, verifique com o responsável", MsgBoxStyle.OkOnly, "Atenção")
                        gerou = True
                    End If
                End If
            End If

            CarregaResumoRequisicao(DtMesAnoPesquisa.Text)

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub


    Private Sub btnSeparaFichas_Click(sender As System.Object, e As System.EventArgs) Handles btnSeparaFichas.Click
        Dim rnGenerico As New RNGenerico
        Dim operador_id As Integer = 0
        Dim qdtFichas As Integer = 0
        Dim parte As Integer = 0
        Dim restaUm As Integer = 0
        Dim lstOperador As New List(Of CGenerico)
        'Dim dsOperador As DataSet

        Try

            If chkZera.Checked = False Then
                If cboOperador.Text = "" Then
                    MsgBox("Por favor informe o operador.")
                    Exit Sub
                End If

                qdtFichas = CInt(txtFichasDisponiveis.Text)
                If cboOperador.SelectedValue = -1 Then
                    MsgBox("Por favor selecione um operador!")
                    Exit Sub
                End If
                operador_id = cboOperador.SelectedValue

                'lstOperador = rnGenerico.BuscarOperadorNome(cboOperador.Text)

                If lstOperador.Count > 0 Then
                    operador_id = CInt(lstOperador.Item(0).ToString)
                End If


                rnGenerico.SeparaFichasCemig(operador_id, cboCategoria.SelectedValue, qdtFichas, cboCidade.Text)

            Else

                rnGenerico.ResetaFichasCemig(cboCategoria.SelectedValue)

            End If

            If Not chkZera.Checked Then
                'gera arquivo csv
                Try
                    Me.Cursor = Cursors.WaitCursor
                    MsgBox("O arquivo CSV, vai ser gerado, por favor aguarde a conclusão")
                    GeraCSV()
                    Me.Cursor = Cursors.Default

                Catch ex As Exception
                    MsgBox("Erro ao gerar Arquivo CSV. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
                    Me.Cursor = Cursors.Default
                End Try

            End If

            btnSeparaFichas.Enabled = False


            MsgBox("Operação realizada com suceso!")

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnPesquisar_Click(sender As System.Object, e As System.EventArgs) Handles btnPesquisar.Click
        Try

            Dim rnretorno As New RNRetorno
            Dim situacao As String = ""
            If cboCategoria.SelectedIndex = 0 Then
                MsgBox("Opção invalida, selecionar a situação!", MsgBoxStyle.Critical, "Atenção!")
                Exit Sub
            End If

            If (cboCategoria.Text.Equals("LISTA NOVA")) Then

                If cboCidade.SelectedIndex = 0 Or cboCidade.Text.Equals("Selecione...") Then
                    MsgBox("Opção invalida, selecionar a cidade!", MsgBoxStyle.Critical, "Atenção!")
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If

            End If

            Me.Cursor = Cursors.WaitCursor

            carregaFeedGridCategoriaContribuinte(cboCategoria.SelectedValue, DtMesAnoPesquisa.Text)

            CarregaResumoRequisicao(DtMesAnoPesquisa.Text)

            btnSeparaFichas.Enabled = True

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub carregaFeedGridCategoriaContribuinte(ByVal categoria As String, ByVal mesRef As String)

        Dim objRetorno As CRetorno = New CRetorno()
        Dim rn As RNRetorno = New RNRetorno()
        Dim bindingSourceApolice As BindingSource = New BindingSource
        Dim ds As DataSet = New DataSet
        Dim valor As Single
        Dim strValor As String

        Try

            ds = rn.RetornaDadosPorCategoriaCemig(categoria, DtMesAnoPesquisa.Text)

            If ds.Tables(0).Rows.Count > 0 Then
                lblCategoria.Text = ds.Tables(0).Rows(0).Item(0)
            End If

            If ds.Tables(1).Rows.Count > 0 Then
                lblTotalFichas.Text = ds.Tables(1).Rows(0).Item(0)
                txtQtdFichas.Text = ds.Tables(1).Rows(0).Item(0)
            End If

            If ds.Tables(2).Rows.Count > 0 Then
                strValor = valor.ToString("C")
                valor = IIf(IsDBNull(ds.Tables(2).Rows(0).Item(0)), 0, ds.Tables(2).Rows(0).Item(0))
                lblValorTotal.Text = valor.ToString("R$ #,###.00")
            End If

            If ds.Tables(3).Rows.Count > 0 Then
                lblFichaConfirmada.Text = ds.Tables(3).Rows(0).Item(0)
            End If

            If ds.Tables(4).Rows.Count > 0 Then
                strValor = valor.ToString("C")
                valor = IIf(IsDBNull(ds.Tables(4).Rows(0).Item(0)), 0, ds.Tables(4).Rows(0).Item(0))
                lblValorConfirmado.Text = valor.ToString("R$ #,###.00")
            End If

            If ds.Tables(5).Rows.Count > 0 Then
                lblFichaRecusada.Text = ds.Tables(5).Rows(0).Item(0)
            End If

            If ds.Tables(6).Rows.Count > 0 Then
                strValor = valor.ToString("C")
                valor = IIf(IsDBNull(ds.Tables(6).Rows(0).Item(0)), 0, ds.Tables(6).Rows(0).Item(0))
                lblValorRecusado.Text = valor.ToString("R$ #,###.00")
            End If

            If ds.Tables(7).Rows.Count > 0 Then
                lblFichaAberto.Text = ds.Tables(7).Rows(0).Item(0)
            End If

            If ds.Tables(8).Rows.Count > 0 Then
                strValor = valor.ToString("C")
                valor = IIf(IsDBNull(ds.Tables(8).Rows(0).Item(0)), 0, ds.Tables(8).Rows(0).Item(0))
                lblValorAberto.Text = valor.ToString("R$ #,###.00")
            End If

            txtFichasDisponiveis.Text = rn.RetornaFichasDisponiveisCemig(categoria)
            txtFichasBloqueadas.Text = rn.RetornaFichasBloqueadasCemig(categoria)
        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub



    Private Sub CarregaComboOperador(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboOperador.DataSource = Nothing
            cboOperador.Items.Clear()
            cboOperador.DisplayMember = "Nome_Operador"
            cboOperador.ValueMember = "Cod_Operador"
            cboOperador.DataSource = rn_generico.BuscarOperador(0)

            If cboOperador.Items.Count > 0 And cod_operador = 0 Then
                cboOperador.SelectedIndex = 0
            ElseIf cboOperador.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperador.Items.Count - 1
                    For Each linha As CGenerico In cboOperador.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperador.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperador.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboCategorias(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCategoria.DataSource = Nothing
            cboCategoria.Items.Clear()
            cboCategoria.DisplayMember = "Nome_Categoria"
            cboCategoria.ValueMember = "Cod_Categoria"
            cboCategoria.DataSource = rn_generico.BuscarCategoria(0)

            If cboCategoria.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoria.SelectedIndex = 0
            ElseIf cboCategoria.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoria.Items.Count - 1
                    For Each linha As CGenerico In cboCategoria.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoria.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoria.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub btnImprimiFichas_Click(sender As System.Object, e As System.EventArgs)
        FrmImprimiFichasManual.Show()
    End Sub

    Private Sub btnListaNova_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        FrmListaNova.Show()
    End Sub

    Private Sub btnRetirarOperadora_Click(sender As Object, e As EventArgs)
        Dim rnGenerico As New RNGenerico
        Dim lstOperador As New List(Of CGenerico)
        Dim operador_id As Integer = 0

        Try
            operador_id = cboOperador.SelectedValue
            If lstOperador.Count > 0 Then
                operador_id = CInt(lstOperador.Item(0).ToString)
            End If
            rnGenerico.RetirarOperadoraCemig(cboCidade.Text, operador_id)

            MsgBox("Operação realizada com suceso!")
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub cboCategoria_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboCategoria.SelectedIndexChanged
        If cboCategoria.Text.Equals("LISTA NOVA") Then
            cboCidade.Visible = True
            lblCidade.Visible = True
        Else
            cboCidade.Visible = False
            lblCidade.Visible = False
        End If
    End Sub

    Private Sub GeraCSV()
        Try
            Dim rnretorno As New RNRetorno
            Dim situacao As String = ""
            If txtFichasDisponiveis.Text = "" OrElse
                txtFichasDisponiveis.Text = "0" Then
                MsgBox("Não existe fichas disponiveis gerar o arquivo CSV", MsgBoxStyle.Critical, "Atenção!")
                Exit Sub
            End If
            If (cboCategoria.Text.Equals("LISTA NOVA")) Then
                If cboCidade.SelectedIndex = 0 Or cboCidade.Text.Equals("Selecione...") Then
                    MsgBox("Opção invalida, selecionar a cidade!", MsgBoxStyle.Critical, "Atenção!")
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If
            End If
            Me.Cursor = Cursors.WaitCursor
            GerarArquivoCSV(cboCategoria.Text, cboCategoria.SelectedValue)
            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub GerarArquivoCSV(categoria As String, cod_categoria As Integer)
        Try
            Dim rn As RNRetorno = New RNRetorno()
            Dim ds As DataSet = rn.RetornaDadosCSVCemig(cod_categoria)
            Dim textoarquivo As String = String.Empty
            If ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    textoarquivo += $"{dr.Item(0).ToString}{vbCrLf}"
                Next
                GeraCriarArquivo(textoarquivo, categoria)
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub GeraCriarArquivo(textoarquivo As String, categoria As String)
        Try
            Dim strNomeArquivo As String = $"Arquivo_CSV_{categoria}.txt"
            Dim caminhoExe As String = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)
            Dim caminhoArquivo As String = Path.Combine(caminhoExe, strNomeArquivo)
            Dim rn As RNRetorno = New RNRetorno()
            'verifica se o arquivo existe
            If Not File.Exists(caminhoArquivo) Then
                ' Cria um arquivo para escrita
                Using sw As FileStream = File.Create(caminhoArquivo)
                    Dim texto As Byte() = New UTF8Encoding(True).GetBytes(textoarquivo)
                    sw.Write(texto, 0, texto.Length)
                End Using
                MsgBox($"Arquivo gerado com sucesso no caminho{caminhoArquivo}")
            Else
                MessageBox.Show("Ja existe um arquivo com esse nome ", "Informe o nome do arquivo", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If
            rn.AtualizaImpressaoFichas()
        Catch ex As Exception

        End Try
    End Sub


End Class