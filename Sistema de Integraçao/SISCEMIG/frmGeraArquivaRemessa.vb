﻿Imports System.IO
Imports SISINT001
Imports Util

Public Class frmGeraArquivaRemessa

    Public bindingSourceRemessa As BindingSource = New BindingSource
    Public ProcessarIDs As New List(Of Integer)

    Private inicializando As Boolean

    Private Enum ColunasGridRemessa

        Selecao = 0
        ID = 1
        Cod_EAN_CLIENTE = 2
        DDD = 3
        Telefone = 4
        Nome = 5
        CPF = 6
        Autorizante = 7
        Valor = 8
        Numero_Instalacao = 9
        mesRef = 10

    End Enum

    Private Sub cbMovimento_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbMovimento.SelectedIndexChanged
        Try
            CarregaRemessa()
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub CarregaRemessa()
        Try
            If cbMovimento.SelectedIndex = 0 Then
                'MsgBox("Por favor selecione o movimento a ser processado")
                Exit Sub
            End If
            Dim movimento As String = String.Empty
            If cbMovimento.Text.Equals("INCLUSÃO") Then
                movimento = "S"
                chkNovasIncluoes.Enabled = True
            ElseIf cbMovimento.Text.Equals("EXCLUSÃO") Then
                movimento = "C"
                chkNovasIncluoes.Enabled = False
            End If
            Dim rn As New RNRequisicao()
            Dim textoarquivo As String = String.Empty
            Dim lstrequisicao As New List(Of RemessaCemig)
            Dim novaInclusao As String
            If chkNovasIncluoes.Checked Then
                novaInclusao = "S"
            Else
                novaInclusao = "N"
            End If
            grdRemessa.DataSource = Nothing
            grdRemessa.DataSource = rn.RetornaMovimentos(movimento, novaInclusao)
            If grdRemessa.Rows.Count = 0 Then
                MsgBox("Não existe registros a serem processados, verifique se houve a liberação das fichas!")
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub chkHeader_CheckedChanged(sender As Object, e As EventArgs) Handles chkHeader.CheckedChanged
        Try
            Me.Cursor = Cursors.WaitCursor
            Dim i As Integer
            Dim valorCheckHeader As Boolean
            valorCheckHeader = chkHeader.Checked
            For i = 0 To grdRemessa.RowCount - 1
                If valorCheckHeader = True Then
                    grdRemessa(ColunasGridRemessa.Selecao, i).Value = valorCheckHeader
                Else
                    grdRemessa(ColunasGridRemessa.Selecao, i).Value = valorCheckHeader
                End If
            Next i
            Me.Cursor = Cursors.Default
            grdRemessa.EndEdit()
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub BtnGerarArquivo_Click(sender As Object, e As EventArgs) Handles BtnGerarArquivo.Click
        Try
            For Each linha As DataGridViewRow In grdRemessa.Rows
                If linha.Cells(0).Value = True Then
                    ProcessarIDs.Add(linha.Cells(1).Value)
                End If
            Next
            Dim dataProcessamento As String = Format(dtMesAnoRef.Value, "ddMMyyyy")
            Dim movimento As String = String.Empty
            If cbMovimento.SelectedIndex = 1 Then
                movimento = "02"
            ElseIf cbMovimento.SelectedIndex = 2 Then
                movimento = "01"
            End If
            If movimento <> String.Empty Then
                ProcessaArquivo(ProcessarIDs, movimento, dataProcessamento)
            End If
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub ProcessaArquivo(listIds As List(Of Integer), movimento As String, dataProcessamento As String)
        Try
            'movimento Deve ser = “01”- Exclusão de Autorização de Débito
            '“02” - Inclusão de Autorização de Débito
            '“03” - Alteração de Autorização de Débito
            Dim rn As New RNRequisicao
            Dim qtsLinhas As String = listIds.Count * 3 + 2
            Dim numeroRemessa As Integer = rn.RetornaNumeroRemessa()
            Dim nomeArquivo As String = "COB1.txt"
            Dim SW As New StreamWriter("C:\Digital\" + nomeArquivo + "") ' Cria o arquivo de texto
            SW.WriteLine(rn.GeraHeader(numeroRemessa)) ' Grava o Header
            For Each id As String In listIds
                SW.WriteLine(rn.GeraAutorizacaoDebito(id, movimento))
                SW.WriteLine(rn.GeraItemDebito(id, dataProcessamento, movimento))
                SW.WriteLine(rn.GeraGrupoParcelas(id, movimento))
            Next
            SW.WriteLine(rn.GeraTrailler(qtsLinhas))
            SW.Close() 'Fecha o arquivo de texto
            SW.Dispose() 'Libera a memória utilizada
            AtualizaDadosProcessamento(listIds, movimento, numeroRemessa, qtsLinhas)

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Function AtualizaDadosProcessamento(listIds As List(Of Integer),
                                                movimento As String,
                                                versao As Integer,
                                                qtdLinhas As Integer) As Boolean
        Try
            Dim rn As New RNRequisicao

            For Each id As String In listIds
                If Not rn.AtualizaIDProcessado(id) Then
                    Return False
                End If
            Next
            rn.InsereRemessaProcessada(movimento, versao, listIds.Count, qtdLinhas)
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Return False
        End Try
        Return True
    End Function

    Private Sub frmGeraArquivaRemessa_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            cbMovimento.SelectedIndex = 0
            CriarColunasGrid()
            dtMesAnoRef.Value = DateAdd(DateInterval.Month, 1, Now())
        Catch ex As Exception

        End Try
    End Sub


    Private Sub CriarColunasGrid()

        Try
            grdRemessa.DataSource = Nothing
            grdRemessa.Columns.Clear()
            grdRemessa.AutoGenerateColumns = False
            grdRemessa.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            '0

            Dim checkboxColumn As New DataGridViewCheckBoxColumn
            checkboxColumn.Width = 32
            checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "ID"
            column.HeaderText = "ID"
            column.Width = 60
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)


            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "COD_EAN_CLIENTE"
            column.HeaderText = "Código de Barras"
            column.Width = 80
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "NomeCliente"
            column.HeaderText = "Nome"
            column.Width = 300
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DDD"
            column.HeaderText = "DDD"
            column.Width = 50
            column.ReadOnly = True
            column.Visible = False
            column.Visible = True
            grdRemessa.Columns.Add(column)

            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Telefone"
            column.HeaderText = "Telefone"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = False
            column.Visible = True
            grdRemessa.Columns.Add(column)

            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "VALOR"
            column.HeaderText = "Valor"
            column.Visible = True
            column.DefaultCellStyle.Format = "0.00"
            column.Width = 80
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '7
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "CPF"
            column.HeaderText = "CPF"
            column.Width = 160
            column.ReadOnly = True
            column.Visible = False
            column.Visible = True
            grdRemessa.Columns.Add(column)


            '8
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Autorizante"
            column.HeaderText = "AUTORIZANTE"
            column.Visible = True
            column.Width = 150
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)


            '8
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "NumeroInstalacao"
            column.HeaderText = "Número de Instalação"
            column.Width = 100
            column.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft
            column.Visible = True
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)


            '9
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "MesEnvio"
            column.HeaderText = "Mês de envio"
            column.Visible = True
            column.Width = 100
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub
End Class