﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text

Public Class FrmInclusaoManualCemig


    Dim objContribuintes As CContribuinte = New CContribuinte()
    Dim bindingSourceApolice As BindingSource = New BindingSource
    Private lstContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstContribuinteSe As New List(Of CContribuinte)


#Region "variaveis"

    Public pCod_ean As String
    Public pNome As String
    Public pTelefone As String
    Public psituacao As String
    Public pMesAnoRef As String
    Public pValor As Decimal
    Public MesAtuacao As String
    Public NextMonth As Date
    Public Consulta As Boolean = False
    Public pCodUsuario As Integer
    Public pUsuario As String
    Public pCodCategoria As String
    Public DDDOriginal As String

#End Region

#Region "Propriedades"

    Public Property Cod_Ean_Cliente() As String
        Get
            Return pCod_ean
        End Get
        Set(ByVal value As String)
            pCod_ean = value
        End Set
    End Property

    Public Property Telefone() As String
        Get
            Return pTelefone
        End Get
        Set(ByVal value As String)
            pTelefone = value
        End Set
    End Property

    Public Property Nome() As String
        Get
            Return pNome
        End Get
        Set(ByVal value As String)
            pNome = value
        End Set
    End Property

    Public Property Valor() As String
        Get
            Return pValor
        End Get
        Set(ByVal value As String)
            pValor = value
        End Set
    End Property

    Public Property CodCategoria() As String
        Get
            Return pCodCategoria
        End Get
        Set(ByVal value As String)
            pCodCategoria = value
        End Set
    End Property



#End Region



    Private Sub txtCodEan_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCodEan.KeyPress
        Dim rncontribuinte As New RNContribuinte
        Dim contribuinte As New CContribuinte
        lstContribuintes = New SortableBindingList(Of CContribuinte)

        Try
            'Nota : Ordenar o tabindex das TextBox : O,1,2, etc..
            If e.KeyChar = Chr(Keys.Enter) Then 'Detecta se tecla Enter foi pressionada
                CarregaDados()
            End If
        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub


    Private Sub txtCodEan_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodEan.Leave
        Try
            carregaDados()
        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub CarregaDados()
        Try
            Dim rncontribuinte As New RNContribuinte
            lstContribuintes = New SortableBindingList(Of CContribuinte)
            If txtCodEan.Text <> "" And txtCodEan.Text <> Nothing And txtCodEan.Text <> "*     -   *" Then
                lstContribuintes = New SortableBindingList(Of CContribuinte)(rncontribuinte.CarregarInclusaoManualCemig(txtCodEan.Text, "Remessa"))
                If (lstContribuintes.Count > 0) Then
                    If lstContribuintes.Item(0).Telefone1.Contains("-") Then
                        Dim telefoneArr As String
                        telefoneArr = lstContribuintes.Item(0).Telefone1.Replace("(", "").Replace(")", "").Replace("-", "")
                        DDDOriginal = telefoneArr.Substring(0, 2)
                        txtDDD.Text = telefoneArr.Substring(0, 2)
                        txtTelefone.Text = telefoneArr.Substring(2, 8)
                    Else
                        txtTelefone.Text = LTrim(RTrim(lstContribuintes.Item(0).Telefone1))
                        txtDDD.Text = LTrim(RTrim(lstContribuintes.Item(0).DDD))
                    End If
                    txtNome.Text = lstContribuintes.Item(0).NomeCliente1
                    txtValor.Text = FormatNumber(LTrim(RTrim(lstContribuintes.Item(0).Valor)), 2)
                    txtNumInstalacao.Text = LTrim(RTrim(lstContribuintes.Item(0).Numero_Instalacao))
                    txtEndereco.Text = lstContribuintes.Item(0).Endereco
                    txtCpf.Text = LTrim(RTrim(lstContribuintes.Item(0).CNPJ_CPF))
                Else
                    txtTelefone.Text = ""
                    txtNome.Text = ""
                    txtDDD.Text = ""
                    txtValor.Text = ""
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnSalvar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalvar.Click
        Dim contribuinte As New CContribuinte
        Dim rnContribuinte As New RNContribuinte
        Dim Analisecontribuinte As New CContribuinte
        Dim rnRequisicao As New RNRequisicao

        Try
            If cboOperador.SelectedIndex = 0 Then
                MsgBox("Por favor selecione um operador")
                Exit Sub
            End If

            If cboCategoria.SelectedIndex = 0 Then
                MsgBox("Por favor selecione uma categoria")
                Exit Sub
            End If

            If txtCodEan.Text = "*     -   *" Then
                MsgBox("Por favor digite o código do contribuinte")
                Exit Sub
            End If

            If txtDDD.Text = "" Then
                MsgBox("Por favor digite o DDD do contribuinte")
                Exit Sub
            End If


            If txtTelefone.Text = "" Then
                MsgBox("Por favor digite o telefone do contribuinte")
                Exit Sub
            End If

            If txtValor.Text = "" Then
                MsgBox("Por favor digite o valor da contribuição")
                Exit Sub
            End If

            If txtValorFicha.Text = "" Then
                MsgBox("Por favor digite o valor a ser cadastrado na Ficha")
                Exit Sub
            End If

            If LTrim(RTrim(txtDDD.Text.Length)) > 8 Then
                MsgBox("Por favor verifique o DDD informado, está fora do padrão")
                Exit Sub
            End If

            If txtNumInstalacao.Text = "" Then
                MsgBox("Por favor digite o número da instalação Cemig")
                Exit Sub
            End If

            Dim result = MessageBox.Show("Deseja realmente incluir o valor de R$" & txtValor.Text & " para o contribuinte?. Deseja continuar?", "Atenção",
                              MessageBoxButtons.YesNo,
                              MessageBoxIcon.Question)

            If (result = DialogResult.No) Then
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor

            contribuinte.Telefone1 = txtTelefone.Text
            contribuinte.DDD = txtDDD.Text
            contribuinte.Valor = txtValor.Text
            contribuinte.Cod_EAN_Cliente = txtCodEan.Text
            contribuinte.Cod_Operador = cboOperador.SelectedValue
            contribuinte.Operador = cboOperador.Text
            contribuinte.Cod_Categoria = cboCategoria.SelectedValue
            If IsNothing(contribuinte.NomeCliente1) Then
                contribuinte.NomeCliente1 = txtNome.Text
            End If

            If chkNaoConforme.Checked = True Then
                contribuinte.Nao_Conforme = 1
            Else
                contribuinte.Nao_Conforme = 0
            End If

            'verifica se houve aceite ou recusa
            If psituacao = Nothing Then
                If chkAceite.Checked = True Then
                    psituacao = "S"
                Else
                    psituacao = "N"
                End If
            End If

            'Analisecontribuinte = rnContribuinte.AnalisaInclusaoRegistroOiRepetido(contribuinte, "Inclusao Manual")

            'If Not IsNothing(Analisecontribuinte.Telefone1) And Analisecontribuinte.Telefone1 <> "" Then

            '    FrmAviso.MesAtuacao = txtMesRef.Text
            '    FrmAviso.pNome = txtNome.Text
            '    FrmAviso.pCpf = txtCpf.Text
            '    FrmAviso.pAutorizante = txtAutorizante.Text
            '    FrmAviso.pObs = rtxtObs.Text
            '    FrmAviso.Analisecontribuinte = Analisecontribuinte
            '    FrmAviso.contribuinte = contribuinte
            '    If chkNaoConforme.Checked = True Then
            '        FrmAviso.contribuinte.Nao_Conforme = 1
            '    Else
            '        FrmAviso.contribuinte.Nao_Conforme = 0
            '    End If
            '    If chkAceite.Checked = True Then
            '        FrmAviso.pAceite = "S"
            '    End If

            '    FrmAviso.Show()


            'contribuinte.Valor = txtValorFicha.Text
            'rnRequisicao.AtualizaDadosListaTelefonicaCemig(contribuinte)
            ''rnContribuinte.Inclui_Contatos(contribuinte, txtMesRef.Text, txtEmailContato.Text, txtWhatsApp.Text)


            'LimpaTela()
            '    FrmConsultaUltimaMovimentacao.atualizou = False
            'Else
            '    MsgBox("Ocorreu um erro na inclusão do registro", MsgBoxStyle.Information, "Atenção")
            'End If
            '    End If

            'If (mensagemAnaliseRequisicao = DialogResult.Yes) Then
            '    If rnContribuinte.Inclui_Numeros_Oi(contribuinte, psituacao, "JaExiteNaOi", txtMesRef.Text, txtNome.Text) = True Then
            '        rnContribuinte.Atualiza_Informacoes_Oi(contribuinte, MesAtuacao, txtMesRef.Text, txtCpf.Text, txtAutorizante.Text, rtxtObs.Text)
            '        MsgBox("Registro salvo com sucesso!", MsgBoxStyle.Information, "Atenção")
            '        LimpaTela()
            '        FrmConsultaUltimaMovimentacao.atualizou = False
            '    Else
            '        MsgBox("Ocorreu um erro na inclusão do registro", MsgBoxStyle.Information, "Atenção")
            '    End If
            'End If

            'Else
            If rnContribuinte.Inclui_Numeros_Cemig(contribuinte, psituacao, "InManual", txtMesRef.Text, txtNome.Text, txtNumInstalacao.Text) = True Then
                rnContribuinte.Atualiza_Informacoes_Cemig(contribuinte, MesAtuacao, txtMesRef.Text, txtCpf.Text, txtAutorizante.Text, rtxtObs.Text, contribuinte.Valor)
                contribuinte.Valor = txtValorFicha.Text
                rnRequisicao.AtualizaDadosListaTelefonicaCemig(contribuinte)
                'rnContribuinte.Inclui_Contatos(contribuinte, txtMesRef.Text, txtEmailContato.Text, txtWhatsApp.Text)
                MsgBox("Registro salvo com sucesso!", MsgBoxStyle.Information, "Atenção")
                LimpaTela()
                FrmConsultaUltimaMovimentacao.atualizou = False
            Else
                MsgBox("Ocorreu um erro na inclusão do registro", MsgBoxStyle.Information, "Atenção")
                End If
            'End If



            Me.Cursor = Cursors.Default
            Me.Visible = False
            If Not FrmConsultaUltimaMovimentacao.CodUsuario = Nothing Then
                FrmConsultaUltimaMovimentacao.Visible = True
            End If

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try

    End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        FrmConsultaUltimaMovimentacao.Enabled = True
        Close()
    End Sub

    Private Sub btnAlterar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAlterar.Click
        Dim contribuinte As New CContribuinte
        Dim rnContribuinte As New RNContribuinte

        Try
            If cboOperador.SelectedIndex = -1 Then
                MsgBox("Por favor selecione um operador")
                Exit Sub
            End If


            Me.Cursor = Cursors.WaitCursor

            contribuinte.Telefone1 = txtTelefone.Text
            contribuinte.DDD = txtDDD.Text
            contribuinte.Valor = txtValor.Text
            contribuinte.Cod_EAN_Cliente = txtCodEan.Text
            contribuinte.Cod_Operador = cboOperador.SelectedValue
            contribuinte.Operador = cboOperador.Text
            contribuinte.Cod_Categoria = cboCategoria.SelectedValue

            If rnContribuinte.AtualizaRequisicaoOi(contribuinte, psituacao, MesAtuacao, txtMesRef.Text, txtNome.Text, 0) = True Then
                MsgBox("Registro alterado com sucesso!", MsgBoxStyle.Information, "Atenção")
                LimpaTela()
            Else
                MsgBox("Ocorreu um erro na inclusão do registro", MsgBoxStyle.Information, "Atenção")
            End If

            Me.Cursor = Cursors.Default
            FrmConsultaUltimaMovimentacao.Enabled = True


        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)


        End Try
    End Sub

    Private Sub btnExcluir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExcluir.Click
        Dim contribuinte As New CContribuinte
        Dim rnContribuinte As New RNContribuinte

        Try

            If cboOperador.SelectedIndex = -1 Then
                MsgBox("Por favor selecione um operador")
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor

            contribuinte.Telefone1 = txtTelefone.Text
            contribuinte.DDD = txtDDD.Text
            contribuinte.Valor = txtValor.Text
            contribuinte.Cod_EAN_Cliente = txtCodEan.Text

            If rnContribuinte.Excluir_Requisicao_Cemig(contribuinte, txtMesRef.Text) = True Then
                MsgBox("Registro excluido com sucesso!", MsgBoxStyle.Information, "Atenção")
                LimpaTela()
            Else
                MsgBox("Ocorreu um erro na inclusão do registro", MsgBoxStyle.Information, "Atenção")
            End If

            Me.Cursor = Cursors.Default
            FrmConsultaUltimaMovimentacao.Enabled = True

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub FrmInclusaoManual_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim rncontribuinte As New RNContribuinte
        Dim new_cod As String
        lstContribuintes = New SortableBindingList(Of CContribuinte)


        Try

            If Consulta = True Then
                lstContribuintes = New SortableBindingList(Of CContribuinte)(rncontribuinte.CarregarDadosRequisicaoOi(Telefone, pMesAnoRef, "Remessa"))

                If lstContribuintes.Count = 0 Then
                    lstContribuintes = New SortableBindingList(Of CContribuinte)(rncontribuinte.CarregarDadosRequisicaoPorTelefone(pTelefone, "Remessa"))
                End If

                If (lstContribuintes.Count > 0) Then
                    CarregaComboCategorias(lstContribuintes.Item(0).Cod_Categoria)
                    'CarregaComboOperador(lstContribuintes.Item(0).Cod_Operador)
                    CarregaComboOperador(pCodUsuario)
                    txtCodEan.Text = IIf(lstContribuintes.Item(0).Cod_EAN_Cliente = "", Cod_Ean_Cliente, lstContribuintes.Item(0).Cod_EAN_Cliente)
                    txtTelefone.Text = IIf(lstContribuintes.Item(0).Telefone1 = "", pTelefone, lstContribuintes.Item(0).Telefone1)
                    txtNome.Text = IIf(lstContribuintes.Item(0).NomeCliente1 = "", pNome, lstContribuintes.Item(0).NomeCliente1)
                    txtDDD.Text = IIf(lstContribuintes.Item(0).DDD = "", "", lstContribuintes.Item(0).DDD)
                    txtValor.Text = IIf(lstContribuintes.Item(0).Valor = 0, 10, FormatNumber(lstContribuintes.Item(0).Valor, 2))
                    txtMesRef.Text = pMesAnoRef
                    btnSalvar.Enabled = False

                End If
                'reset na variavel
                Consulta = False

            Else

                If pCod_ean <> "" Or pCod_ean <> Nothing Then

                    lstContribuintes = New SortableBindingList(Of CContribuinte)(rncontribuinte.CarregarInclusaoManual(pCod_ean, "Remessa"))

                    If lstContribuintes.Count = 0 Then
                        lstContribuintes = New SortableBindingList(Of CContribuinte)(rncontribuinte.CarregarDadosRequisicaoPorTelefone(pTelefone, "Remessa"))
                    End If

                    If (lstContribuintes.Count > 0) Then
                        CarregaComboCategorias(lstContribuintes.Item(0).Cod_Categoria)
                        'CarregaComboOperador(lstContribuintes.Item(0).Cod_Operador)
                        CarregaComboOperador(pCodUsuario)
                        txtCodEan.Text = IIf(lstContribuintes.Item(0).Cod_EAN_Cliente = "", Cod_Ean_Cliente, lstContribuintes.Item(0).Cod_EAN_Cliente)
                        txtTelefone.Text = IIf(lstContribuintes.Item(0).Telefone1 = "", pTelefone, lstContribuintes.Item(0).Telefone1)
                        txtNome.Text = IIf(lstContribuintes.Item(0).NomeCliente1 = "", pNome, lstContribuintes.Item(0).NomeCliente1)
                        txtDDD.Text = IIf(lstContribuintes.Item(0).DDD = "", "", lstContribuintes.Item(0).DDD)
                        txtValor.Text = IIf(lstContribuintes.Item(0).Valor = 0, 10, FormatNumber(lstContribuintes.Item(0).Valor, 2))
                        MesAtuacao = pMesAnoRef
                        NextMonth = DateAdd(DateInterval.Month, 1, CDate(pMesAnoRef))
                        txtMesRef.Text = NextMonth.Month & "/" & NextMonth.Year

                    ElseIf Telefone <> "" Then
                        txtTelefone.Text = Telefone
                        NextMonth = DateAdd(DateInterval.Month, 1, CDate(pMesAnoRef))
                        txtMesRef.Text = NextMonth.Month & "/" & NextMonth.Year
                    Else
                        txtTelefone.Text = ""
                        txtNome.Text = ""
                        txtDDD.Text = ""
                        txtValor.Text = ""
                        NextMonth = DateAdd(DateInterval.Month, 1, CDate(pMesAnoRef))
                        txtMesRef.Text = NextMonth.Month & "/" & NextMonth.Year

                    End If

                ElseIf pTelefone <> "" Or pTelefone <> Nothing Then

                    lstContribuintes = New SortableBindingList(Of CContribuinte)(rncontribuinte.CarregarInclusaoManualPorTelefoneSimplificado(pTelefone, txtDDD.Text, "Remessa"))

                    If lstContribuintes.Count = 0 Then
                        lstContribuintes = New SortableBindingList(Of CContribuinte)(rncontribuinte.CarregarDadosRequisicaoPorTelefone(pTelefone, "Remessa"))
                    End If

                    If (lstContribuintes.Count > 0) Then
                        txtCodEan.Text = IIf(lstContribuintes.Item(0).Cod_EAN_Cliente = "", Cod_Ean_Cliente, lstContribuintes.Item(0).Cod_EAN_Cliente)
                        txtTelefone.Text = IIf(lstContribuintes.Item(0).Telefone1 = "", pTelefone, lstContribuintes.Item(0).Telefone1)
                        txtNome.Text = IIf(lstContribuintes.Item(0).NomeCliente1 = "", pNome, lstContribuintes.Item(0).NomeCliente1)
                        txtDDD.Text = IIf(lstContribuintes.Item(0).DDD = "", "", lstContribuintes.Item(0).DDD)
                        txtValor.Text = IIf(lstContribuintes.Item(0).Valor = 0, 10, FormatNumber(lstContribuintes.Item(0).Valor, 2))
                        NextMonth = DateAdd(DateInterval.Month, 1, CDate(pMesAnoRef))
                        txtMesRef.Text = NextMonth.Month & "/" & NextMonth.Year
                        CarregaComboCategorias(CodCategoria)
                        CarregaComboOperador(pCodUsuario)

                    ElseIf Telefone <> "" Then
                        txtTelefone.Text = Telefone
                        NextMonth = DateAdd(DateInterval.Month, 1, CDate(pMesAnoRef))
                        txtMesRef.Text = NextMonth.Month & "/" & NextMonth.Year
                        new_cod = rncontribuinte.RetornaNovoCodigo(True)
                        new_cod = "*" + new_cod + "-171*"
                        txtCodEan.Text = new_cod
                    Else
                        txtTelefone.Text = ""
                        txtNome.Text = ""
                        txtDDD.Text = ""
                        txtValor.Text = ""
                        NextMonth = DateAdd(DateInterval.Month, 1, CDate(pMesAnoRef))
                        txtMesRef.Text = NextMonth.Month & "/" & NextMonth.Year
                        new_cod = rncontribuinte.RetornaNovoCodigo(True)
                        new_cod = "*" + new_cod + "-171*"
                        txtCodEan.Text = new_cod
                    End If

                Else
                    CarregaComboCategorias(0)
                    CarregaComboOperador(0)
                    NextMonth = DateAdd(DateInterval.Month, 1, CDate(Now.Date))
                    txtMesRef.Text = NextMonth.Month & "/" & NextMonth.Year
                End If

            End If

            'bloqueia
            'If CodCategoria <> 3 And CodCategoria <> 4 And CodCategoria <> 6 Then
            '    cboCategoria.Enabled = False
            '    CarregaComboCategorias(CodCategoria)
            '    txtValor.Enabled = False
            'Else
            '    cboCategoria.Enabled = False
            '    CarregaComboCategorias(CodCategoria)
            '    txtValor.Enabled = True
            'End If

        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub CarregaComboOperador(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboOperador.DataSource = Nothing
            cboOperador.Items.Clear()
            cboOperador.DisplayMember = "Nome_Operador"
            cboOperador.ValueMember = "Cod_Operador"
            cboOperador.DataSource = rn_generico.BuscarOperador(0)

            If cboOperador.Items.Count > 0 And cod_operador = 0 Then
                cboOperador.SelectedIndex = 0
            ElseIf cboOperador.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperador.Items.Count - 1
                    For Each linha As CGenerico In cboOperador.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperador.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperador.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboCategorias(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCategoria.DataSource = Nothing
            cboCategoria.Items.Clear()
            cboCategoria.DisplayMember = "Nome_Categoria"
            cboCategoria.ValueMember = "Cod_Categoria"
            cboCategoria.DataSource = rn_generico.BuscarCategoria(0)

            If cboCategoria.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoria.SelectedIndex = 0
            ElseIf cboCategoria.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoria.Items.Count - 1
                    For Each linha As CGenerico In cboCategoria.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoria.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoria.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub LimpaTela()
        txtCodEan.Text = ""
        txtDDD.Text = ""
        txtMesRef.Text = ""
        txtNome.Text = ""
        txtTelefone.Text = ""
        txtValor.Text = ""
        txtCpf.Text = ""
        txtAutorizante.Text = ""
        rtxtObs.Text = ""
        txtCodEan.Focus()
    End Sub

    Private Sub txtDDD_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtDDD.KeyPress
        If Not (Char.IsDigit(e.KeyChar) OrElse Char.IsControl(e.KeyChar)) Then

            e.Handled = True

        End If
    End Sub

    Private Sub txtTelefone_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtTelefone.KeyPress
        If Not (Char.IsDigit(e.KeyChar) OrElse Char.IsControl(e.KeyChar)) Then

            e.Handled = True

        End If

    End Sub


    Private Sub txtCpf_Leave(sender As System.Object, e As System.EventArgs) Handles txtCpf.Leave
        Dim cpf1 As New Recursos

        cpf1.cpf = txtCpf.Text.ToString()

        If Not cpf1.isCpfValido() Then
            MsgBox("CPF Inválido, por favor confirmar os dados")
        End If
    End Sub


    Private Sub txtCpf_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtCpf.KeyPress

        Try

            'permite apenas números a serem digitados
            If Not (Char.IsDigit(e.KeyChar) OrElse Char.IsControl(e.KeyChar)) Then

                e.Handled = True

            End If

            'formata o textbox para o cpf
            If IsNumeric(e.KeyChar) AndAlso txtCpf.TextLength < txtCpf.MaxLength Then
                txtCpf.Text &= e.KeyChar
                txtCpf.SelectionStart = txtCpf.TextLength
                Call formatacpf(txtCpf)
            End If
            e.Handled = True

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub formatacpf(ByVal txtTexto As Object)
        If Len(txtTexto.Text) = 3 Then
            txtTexto.Text = txtTexto.Text & "."
            txtTexto.SelectionStart = Len(txtTexto.Text) + 1
        ElseIf Len(txtTexto.Text) = 7 Then
            txtTexto.Text = txtTexto.Text & "."
            txtTexto.SelectionStart = Len(txtTexto.Text) + 1
        ElseIf Len(txtTexto.Text) = 11 Then
            txtTexto.Text = txtTexto.Text & "-"
            txtTexto.SelectionStart = Len(txtTexto.Text) + 1
        End If
    End Sub

    Private Sub txtValor_KeyPress(sender As System.Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtValor.KeyPress
        If Not (Char.IsDigit(e.KeyChar) OrElse Char.IsControl(e.KeyChar)) Then

            e.Handled = True

        End If
    End Sub

    Private Sub txtDDD_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDDD.Leave

        If RTrim(LTrim(DDDOriginal)) <> RTrim(LTrim(txtDDD.Text)) Then
            MsgBox("Por favor certifique-se que o DDD informado é o correto, pois pode ocorrer cobrança para um contribuinte diferente do esperado", MsgBoxStyle.Exclamation, "Atenção")
        End If

    End Sub

    Private Sub btnComercial_Click(sender As Object, e As EventArgs) Handles btnComercial.Click
        Try
            Dim contribuinte As New CContribuinte
            Dim rnContribuinte As New RNContribuinte

            If cboOperador.SelectedIndex = 0 Then
                MsgBox("Por favor selecione um operador")
                Exit Sub
            End If

            If cboCategoria.SelectedIndex = 0 Then
                MsgBox("Por favor selecione uma categoria")
                Exit Sub
            End If

            If txtCodEan.Text = "*     -   *" Then
                MsgBox("Por favor digite o código do contribuinte")
                Exit Sub
            End If

            If txtDDD.Text = "" Then
                MsgBox("Por favor digite o DDD do contribuinte")
                Exit Sub
            End If


            If txtTelefone.Text = "" Then
                MsgBox("Por favor digite o telefone do contribuinte")
                Exit Sub
            End If

            If LTrim(RTrim(txtDDD.Text.Length)) > 8 Then
                MsgBox("Por favor verifique o DDD informado, está fora do padrão")
                Exit Sub
            End If


            Me.Cursor = Cursors.WaitCursor

            contribuinte.Telefone1 = txtTelefone.Text
            contribuinte.DDD = txtDDD.Text
            contribuinte.Valor = txtValor.Text
            contribuinte.Cod_EAN_Cliente = txtCodEan.Text
            contribuinte.Cod_Operador = cboOperador.SelectedValue
            contribuinte.Operador = cboOperador.Text
            contribuinte.Cod_Categoria = cboCategoria.SelectedValue

            If rnContribuinte.Inclui_Comercial_Nao_ContribuiuCemig(contribuinte, psituacao, "comercial", txtMesRef.Text) = True Then
                MsgBox("Registro Incluido como número comercial!", MsgBoxStyle.Information, "Atenção")
                LimpaTela()
            End If
            Me.Cursor = Cursors.Default
            FrmConsultaUltimaMovimentacao.Enabled = True
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnNaoContribuiu_Click(sender As Object, e As EventArgs) Handles btnNaoContribuiu.Click
        Try
            Dim contribuinte As New CContribuinte
            Dim rnContribuinte As New RNContribuinte

            If cboOperador.SelectedIndex = 0 Then
                MsgBox("Por favor selecione um operador")
                Exit Sub
            End If

            If cboCategoria.SelectedIndex = 0 Then
                MsgBox("Por favor selecione uma categoria")
                Exit Sub
            End If

            If txtCodEan.Text = "*     -   *" Then
                MsgBox("Por favor digite o código do contribuinte")
                Exit Sub
            End If

            If txtDDD.Text = "" Then
                MsgBox("Por favor digite o DDD do contribuinte")
                Exit Sub
            End If


            If txtTelefone.Text = "" Then
                MsgBox("Por favor digite o telefone do contribuinte")
                Exit Sub
            End If

            If LTrim(RTrim(txtDDD.Text.Length)) > 8 Then
                MsgBox("Por favor verifique o DDD informado, está fora do padrão")
                Exit Sub
            End If


            Me.Cursor = Cursors.WaitCursor

            contribuinte.Telefone1 = txtTelefone.Text
            contribuinte.DDD = txtDDD.Text
            contribuinte.Valor = txtValor.Text
            contribuinte.Cod_EAN_Cliente = txtCodEan.Text
            contribuinte.Cod_Operador = cboOperador.SelectedValue
            contribuinte.Operador = cboOperador.Text
            contribuinte.Cod_Categoria = cboCategoria.SelectedValue

            If rnContribuinte.Inclui_Comercial_Nao_ContribuiuCemig(contribuinte, psituacao, "NaoContribuiu", txtMesRef.Text) = True Then
                MsgBox("Registro Incluido como Não Contribuiu!", MsgBoxStyle.Information, "Atenção")
                LimpaTela()
            End If
            Me.Cursor = Cursors.Default
            FrmConsultaUltimaMovimentacao.Enabled = True
        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub
End Class