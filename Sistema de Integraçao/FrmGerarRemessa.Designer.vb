﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmGerarRemessa
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.DtMesAnoPesquisa = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtAnoMesRef = New System.Windows.Forms.TextBox()
        Me.cboOperadora = New System.Windows.Forms.ComboBox()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.btnPesquisar = New System.Windows.Forms.Button()
        Me.txtTel1 = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboNome = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.grdRemessa = New System.Windows.Forms.DataGridView()
        Me.btnSair = New System.Windows.Forms.Button()
        Me.btnGerarRemessa = New System.Windows.Forms.Button()
        Me.dtMesAnoRef = New System.Windows.Forms.DateTimePicker()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.btnRetorno = New System.Windows.Forms.Button()
        Me.Filtros = New System.Windows.Forms.GroupBox()
        Me.chkContestado = New System.Windows.Forms.CheckBox()
        Me.chkCriticado = New System.Windows.Forms.CheckBox()
        Me.btnIncluirManual = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        CType(Me.grdRemessa, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Filtros.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.DtMesAnoPesquisa)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.txtAnoMesRef)
        Me.Panel1.Controls.Add(Me.cboOperadora)
        Me.Panel1.Controls.Add(Me.Label26)
        Me.Panel1.Controls.Add(Me.btnPesquisar)
        Me.Panel1.Controls.Add(Me.txtTel1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.cboNome)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1009, 87)
        Me.Panel1.TabIndex = 1
        '
        'DtMesAnoPesquisa
        '
        Me.DtMesAnoPesquisa.CustomFormat = "MM/yyyy"
        Me.DtMesAnoPesquisa.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.DtMesAnoPesquisa.Location = New System.Drawing.Point(515, 49)
        Me.DtMesAnoPesquisa.Name = "DtMesAnoPesquisa"
        Me.DtMesAnoPesquisa.Size = New System.Drawing.Size(200, 20)
        Me.DtMesAnoPesquisa.TabIndex = 72
        Me.DtMesAnoPesquisa.Value = New Date(2016, 12, 1, 0, 0, 0, 0)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(408, 49)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(91, 16)
        Me.Label3.TabIndex = 71
        Me.Label3.Text = "Ano Mês Ref.:"
        '
        'txtAnoMesRef
        '
        Me.txtAnoMesRef.Location = New System.Drawing.Point(515, 48)
        Me.txtAnoMesRef.Name = "txtAnoMesRef"
        Me.txtAnoMesRef.Size = New System.Drawing.Size(104, 20)
        Me.txtAnoMesRef.TabIndex = 70
        '
        'cboOperadora
        '
        Me.cboOperadora.FormattingEnabled = True
        Me.cboOperadora.Location = New System.Drawing.Point(128, 48)
        Me.cboOperadora.Name = "cboOperadora"
        Me.cboOperadora.Size = New System.Drawing.Size(263, 21)
        Me.cboOperadora.TabIndex = 69
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(14, 49)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(108, 16)
        Me.Label26.TabIndex = 68
        Me.Label26.Text = "Tipo Operadora:"
        '
        'btnPesquisar
        '
        Me.btnPesquisar.Location = New System.Drawing.Point(918, 12)
        Me.btnPesquisar.Name = "btnPesquisar"
        Me.btnPesquisar.Size = New System.Drawing.Size(75, 23)
        Me.btnPesquisar.TabIndex = 67
        Me.btnPesquisar.Text = "Pesquisar"
        Me.btnPesquisar.UseVisualStyleBackColor = True
        '
        'txtTel1
        '
        Me.txtTel1.Location = New System.Drawing.Point(515, 15)
        Me.txtTel1.Name = "txtTel1"
        Me.txtTel1.Size = New System.Drawing.Size(104, 20)
        Me.txtTel1.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(408, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(101, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Num Remessa:"
        '
        'cboNome
        '
        Me.cboNome.FormattingEnabled = True
        Me.cboNome.Location = New System.Drawing.Point(64, 14)
        Me.cboNome.Name = "cboNome"
        Me.cboNome.Size = New System.Drawing.Size(327, 21)
        Me.cboNome.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(14, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(48, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nome:"
        '
        'grdRemessa
        '
        Me.grdRemessa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdRemessa.Location = New System.Drawing.Point(12, 122)
        Me.grdRemessa.Name = "grdRemessa"
        Me.grdRemessa.Size = New System.Drawing.Size(800, 411)
        Me.grdRemessa.TabIndex = 2
        '
        'btnSair
        '
        Me.btnSair.Location = New System.Drawing.Point(946, 553)
        Me.btnSair.Name = "btnSair"
        Me.btnSair.Size = New System.Drawing.Size(75, 23)
        Me.btnSair.TabIndex = 3
        Me.btnSair.Text = "Sair"
        Me.btnSair.UseVisualStyleBackColor = True
        '
        'btnGerarRemessa
        '
        Me.btnGerarRemessa.Location = New System.Drawing.Point(315, 552)
        Me.btnGerarRemessa.Name = "btnGerarRemessa"
        Me.btnGerarRemessa.Size = New System.Drawing.Size(99, 23)
        Me.btnGerarRemessa.TabIndex = 4
        Me.btnGerarRemessa.Text = "Gerar Remessa"
        Me.btnGerarRemessa.UseVisualStyleBackColor = True
        '
        'dtMesAnoRef
        '
        Me.dtMesAnoRef.CustomFormat = "MM/yyyy"
        Me.dtMesAnoRef.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtMesAnoRef.Location = New System.Drawing.Point(109, 552)
        Me.dtMesAnoRef.Name = "dtMesAnoRef"
        Me.dtMesAnoRef.Size = New System.Drawing.Size(200, 20)
        Me.dtMesAnoRef.TabIndex = 74
        Me.dtMesAnoRef.Value = New Date(2018, 11, 5, 0, 0, 0, 0)
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(12, 553)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(91, 16)
        Me.Label4.TabIndex = 73
        Me.Label4.Text = "Ano Mês Ref.:"
        '
        'btnRetorno
        '
        Me.btnRetorno.Location = New System.Drawing.Point(824, 553)
        Me.btnRetorno.Name = "btnRetorno"
        Me.btnRetorno.Size = New System.Drawing.Size(99, 23)
        Me.btnRetorno.TabIndex = 75
        Me.btnRetorno.Text = "Retorno"
        Me.btnRetorno.UseVisualStyleBackColor = True
        '
        'Filtros
        '
        Me.Filtros.Controls.Add(Me.chkContestado)
        Me.Filtros.Controls.Add(Me.chkCriticado)
        Me.Filtros.Location = New System.Drawing.Point(824, 122)
        Me.Filtros.Name = "Filtros"
        Me.Filtros.Size = New System.Drawing.Size(200, 299)
        Me.Filtros.TabIndex = 76
        Me.Filtros.TabStop = False
        Me.Filtros.Text = "Não Incluir"
        '
        'chkContestado
        '
        Me.chkContestado.AutoSize = True
        Me.chkContestado.Checked = True
        Me.chkContestado.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkContestado.Location = New System.Drawing.Point(18, 42)
        Me.chkContestado.Name = "chkContestado"
        Me.chkContestado.Size = New System.Drawing.Size(80, 17)
        Me.chkContestado.TabIndex = 1
        Me.chkContestado.Text = "Contestado"
        Me.chkContestado.UseVisualStyleBackColor = True
        '
        'chkCriticado
        '
        Me.chkCriticado.AutoSize = True
        Me.chkCriticado.Checked = True
        Me.chkCriticado.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkCriticado.Location = New System.Drawing.Point(18, 19)
        Me.chkCriticado.Name = "chkCriticado"
        Me.chkCriticado.Size = New System.Drawing.Size(72, 17)
        Me.chkCriticado.TabIndex = 0
        Me.chkCriticado.Text = "Criticados"
        Me.chkCriticado.UseVisualStyleBackColor = True
        '
        'btnIncluirManual
        '
        Me.btnIncluirManual.Location = New System.Drawing.Point(424, 552)
        Me.btnIncluirManual.Name = "btnIncluirManual"
        Me.btnIncluirManual.Size = New System.Drawing.Size(106, 23)
        Me.btnIncluirManual.TabIndex = 77
        Me.btnIncluirManual.Text = "Incluir Manual"
        Me.btnIncluirManual.UseVisualStyleBackColor = True
        '
        'FrmGerarRemessa
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1036, 610)
        Me.Controls.Add(Me.btnIncluirManual)
        Me.Controls.Add(Me.Filtros)
        Me.Controls.Add(Me.btnRetorno)
        Me.Controls.Add(Me.dtMesAnoRef)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.btnGerarRemessa)
        Me.Controls.Add(Me.btnSair)
        Me.Controls.Add(Me.grdRemessa)
        Me.Controls.Add(Me.Panel1)
        Me.MinimizeBox = False
        Me.Name = "FrmGerarRemessa"
        Me.Text = "Gerar Remessa"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.grdRemessa, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Filtros.ResumeLayout(False)
        Me.Filtros.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnPesquisar As System.Windows.Forms.Button
    Friend WithEvents txtTel1 As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboNome As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboOperadora As System.Windows.Forms.ComboBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents grdRemessa As System.Windows.Forms.DataGridView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtAnoMesRef As System.Windows.Forms.TextBox
    Friend WithEvents btnSair As System.Windows.Forms.Button
    Friend WithEvents btnGerarRemessa As System.Windows.Forms.Button
    Friend WithEvents DtMesAnoPesquisa As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtMesAnoRef As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnRetorno As System.Windows.Forms.Button
    Friend WithEvents Filtros As System.Windows.Forms.GroupBox
    Friend WithEvents chkContestado As System.Windows.Forms.CheckBox
    Friend WithEvents chkCriticado As System.Windows.Forms.CheckBox
    Friend WithEvents btnIncluirManual As System.Windows.Forms.Button
End Class
