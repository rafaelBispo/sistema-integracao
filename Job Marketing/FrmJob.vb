﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text
Imports System.Net.Mail
Imports Microsoft.Win32

Public Class frmJob

    Dim caminho As String = "C:\GIT\executavel\JOB\Job Marketing.exe"

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Timer1.Start()



    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim HorarioAtual As DateTime = DateTime.Now
        Dim rn As New RNContribuinte
        Dim ds As DataSet = New DataSet
        Dim email As String

        If HorarioAtual.Hour = 11 And HorarioAtual.Minute = 35 Then
            ds = rn.RetornaEmailsEnviar()

            For Each item As DataTable In ds.Tables
                email = item.Rows.Item(0).Item(0)
                EnviaEmail(email)
            Next

            If Date.Now.Day Mod 2 = 0 Then
                GeraArquivoExportar()
            End If

            MsgBox("E-mail enviado com sucesso", vbInformation, "Ok")
        End If

    End Sub


    Public Function GeraArquivoExportar() As Boolean
        Dim dsArquivo As DataSet = New DataSet
        Dim rnGenerico As New RNGenerico
        Dim tipoArquivo As String = ""
        Dim objeto As String = ""
        Dim diaAtual As Date
        Dim diaAnterior As Date
        Dim NomeArquivo As String = ""


        Try
            Me.Cursor = Cursors.WaitCursor


            diaAtual = Date.Today
            diaAnterior = diaAtual.AddDays(-1)

            tipoArquivo = "ROI_"
            dsArquivo = rnGenerico.CriaArquivoRequisicaoOiExportar(diaAnterior, diaAtual)

            If dsArquivo.Tables(0).Rows.Count > 0 Then

                NomeArquivo = "C:\Digital\" + tipoArquivo + diaAtual + "_ate_" + diaAnterior + ".txt"


                Dim SW As New StreamWriter(NomeArquivo) ' Cria o arquivo de texto


                For Each row In dsArquivo.Tables(0).Rows 'select do detalhe
                    Dim linha As String = ""
                    For Each obj In row.ItemArray

                        objeto = obj.ToString()
                        objeto = objeto.Replace(Chr(10), "")
                        objeto = objeto.Replace(Chr(13), "")

                        If linha = "" Then
                            linha = "|" + obj.ToString()
                        Else
                            linha = linha + ";" + obj.ToString()
                        End If

                    Next

                    SW.WriteLine(linha) ' Grava o detalhe

                Next

                SW.Close() 'Fecha o arquivo de texto

                SW.Dispose() 'Libera a memória utilizada

            Else
                Me.Cursor = Cursors.Default

                Return False
            End If

            Me.Cursor = Cursors.Default

            Return True

        Catch ex As Exception

            'daoContribuinte.RollBackTransaction()
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
            Me.Cursor = Cursors.Default
            Return False

        End Try


    End Function

    Private Sub EnviaEmail(ByVal email As String)
        Try

            'create the mail message
            Dim SmtpServer As New SmtpClient()
            Dim mail As New MailMessage()
            SmtpServer.Credentials = New Net.NetworkCredential("aaci.jf.mg@gmail.com", "kwcmuyymqekemvtz")
            SmtpServer.Host = "smtp.gmail.com"
            SmtpServer.Port = 587 'server para gmail
            SmtpServer.EnableSsl = True
            mail.From = New MailAddress("aaci.jf.mg@gmail.com")
            mail.To.Add(email) 'Email que recebera a Mensagem
            mail.Subject = "Agradecimento AACI"
            mail.Body = ""

            'Este codigo PlainView e caso o email não suporte Html, nisto ele irá informar este erro

            Dim plainView As AlternateView = AlternateView.CreateAlternateViewFromString("Obrigado por fazer parte da nossa familia, nossas crianças agradecem", Nothing, "text/plain")

            'crie um corpo de texto caso deseje assim você poderá colocar um texto no meio de duas imagens
            Dim linha1 As String = ""
            If Date.Now.Hour < 12 Then
                linha1 = "Bom dia,"
            Else
                linha1 = "Boa tarde,"
            End If

            Dim linha2 As String = "Obrigado por fazer parte da nossa familia, nossas crianças agradecem"
            Dim linha3 As String = "Atenciosamente."

            Dim htmlView As AlternateView = AlternateView.CreateAlternateViewFromString("<p> " + linha1 + "</p> <p>&nbsp;</p> <p>" + linha2 + "</p> <p>&nbsp;</p> <img src=cid:companylogo> <p>&nbsp;</p> <p>" + linha3 + "</p> <p><br /> <br /> --</p> <p>&nbsp;</p> <p>att..</p> <p>&nbsp;</p> <p><strong>AACI - ASSOCIA&Ccedil;&Atilde;O DE APOIO AS CRIAN&Ccedil;AS E IDOSOS</strong></p>   ", Nothing, "text/html")


            mail.Attachments.Add(New Attachment("C:\Digital\Imagens\aaci3.png")) 'Adiciona o anexo a enviar

            With mail
                .AlternateViews.Add(plainView)
                .AlternateViews.Add(htmlView)

            End With

            mail.IsBodyHtml = True

            SmtpServer.Send(mail)
            'MsgBox("E-mail enviado com sucesso", vbInformation, "Ok")





        Catch ex As Exception  'caso haja algum erro crie uma msg de aviso.
            MsgBox(ex)
        Finally
            '  File.Delete("")
        End Try
    End Sub

    Private Sub btnExecuta_Click(sender As Object, e As EventArgs) Handles btnExecuta.Click
        Dim rn As New RNContribuinte
        Dim ds As DataSet = New DataSet
        Dim email As String

        ds = rn.RetornaEmailsEnviar()

        For Each item As DataTable In ds.Tables
            email = item.Rows.Item(0).Item(0)
            EnviaEmail(email)
        Next
    End Sub

    Private Sub btnInicia_Click(sender As Object, e As EventArgs) Handles btnInicia.Click
        Try
            AdicionarAplicacaoAoIniciar()
        Catch ex As Exception
            MessageBox.Show("Erro " + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.[Error])
        End Try
    End Sub

    Private Sub btnRemove_Click(sender As Object, e As EventArgs) Handles btnRemove.Click
        Try
            RemoverAplicacaoAoIniciar()
        Catch ex As Exception
            MessageBox.Show("Erro " + ex.Message, "Erro", MessageBoxButtons.OK, MessageBoxIcon.[Error])
        End Try
    End Sub


    Public Sub RemoverAplicacaoAoIniciar()
        Try
            Using key As RegistryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True)
                key.DeleteValue(caminho, False)
            End Using
        Catch ex As Exception  'caso haja algum erro crie uma msg de aviso.
            MsgBox(ex)
        Finally
            '  File.Delete("")
        End Try
    End Sub

    Public Sub AdicionarAplicacaoAoIniciar()
        Try
            Using key As RegistryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\Microsoft\Windows\CurrentVersion\Run", True)
                key.SetValue(caminho, """" + Application.ExecutablePath + """")
            End Using
        Catch ex As Exception  'caso haja algum erro crie uma msg de aviso.
            MsgBox(ex)
        Finally
            '  File.Delete("")
        End Try
    End Sub
End Class
