﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmJob
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnExecuta = New System.Windows.Forms.Button()
        Me.lblHoraAtual = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblHoraExecução = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.btnInicia = New System.Windows.Forms.Button()
        Me.btnRemove = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnExecuta
        '
        Me.btnExecuta.Location = New System.Drawing.Point(133, 105)
        Me.btnExecuta.Name = "btnExecuta"
        Me.btnExecuta.Size = New System.Drawing.Size(148, 23)
        Me.btnExecuta.TabIndex = 0
        Me.btnExecuta.Text = "Executar Manualmente"
        Me.btnExecuta.UseVisualStyleBackColor = True
        '
        'lblHoraAtual
        '
        Me.lblHoraAtual.AutoSize = True
        Me.lblHoraAtual.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHoraAtual.Location = New System.Drawing.Point(129, 18)
        Me.lblHoraAtual.Name = "lblHoraAtual"
        Me.lblHoraAtual.Size = New System.Drawing.Size(88, 24)
        Me.lblHoraAtual.TabIndex = 1
        Me.lblHoraAtual.Text = "00:00:00"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(114, 24)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Hora Atual:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(12, 51)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(191, 24)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Hora de Execução:"
        '
        'lblHoraExecução
        '
        Me.lblHoraExecução.AutoSize = True
        Me.lblHoraExecução.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHoraExecução.Location = New System.Drawing.Point(209, 51)
        Me.lblHoraExecução.Name = "lblHoraExecução"
        Me.lblHoraExecução.Size = New System.Drawing.Size(88, 24)
        Me.lblHoraExecução.TabIndex = 4
        Me.lblHoraExecução.Text = "00:00:00"
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 1000
        '
        'btnInicia
        '
        Me.btnInicia.Location = New System.Drawing.Point(16, 144)
        Me.btnInicia.Name = "btnInicia"
        Me.btnInicia.Size = New System.Drawing.Size(148, 37)
        Me.btnInicia.TabIndex = 5
        Me.btnInicia.Text = "Iniciar Apliceção junto do Windows"
        Me.btnInicia.UseVisualStyleBackColor = True
        '
        'btnRemove
        '
        Me.btnRemove.Location = New System.Drawing.Point(251, 144)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(148, 37)
        Me.btnRemove.TabIndex = 6
        Me.btnRemove.Text = "Remover Inicialização"
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'frmJob
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(421, 198)
        Me.Controls.Add(Me.btnRemove)
        Me.Controls.Add(Me.btnInicia)
        Me.Controls.Add(Me.lblHoraExecução)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblHoraAtual)
        Me.Controls.Add(Me.btnExecuta)
        Me.Name = "frmJob"
        Me.Text = "Job"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExecuta As System.Windows.Forms.Button
    Friend WithEvents lblHoraAtual As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblHoraExecução As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents btnInicia As Button
    Friend WithEvents btnRemove As Button
End Class
