
Imports System.Windows.Forms

''' <summary>
''' Classe auxiliar com m�todos simplificados para exibi��es de mensagens no sistema
''' </summary>
''' <flow>3273725</flow>
''' <analyst>
''' Bruno Nunes Trassante
''' </analyst>
''' <date>8/7/2010</date>
''' <remarks></remarks>
Public Class MessageHelper

#Region "Vari�veis"


#End Region

#Region "M�todos"

    ''' <summary>
    ''' Exibe uma message box de erro 
    ''' </summary>
    ''' <flow>3273725</flow>
    ''' <analyst>
    ''' Bruno Nunes Trassante
    ''' </analyst>
    ''' <date>8/7/2010</date>
    ''' <param name="message"></param>
    ''' <param name="titulo"></param>
    ''' <remarks></remarks>
    Public Sub ShowErrorMessage(ByVal message As String, ByVal titulo As String)

        MessageBox.Show(message, titulo, MessageBoxButtons.OK, MessageBoxIcon.Error)

    End Sub

    ''' <summary>
    ''' Exibe uma message box de erro 
    ''' </summary>
    ''' <flow>3273725</flow>
    ''' <analyst>
    ''' Bruno Nunes Trassante
    ''' </analyst>
    ''' <date>8/7/2010</date>
    ''' <param name="exception"></param>
    ''' <remarks></remarks>
    Public Sub ShowErrorMessage(ByVal exception As Exception)

        ShowErrorMessage(exception.Message, My.Settings.ErrorTitle)

    End Sub


    ''' <summary>
    ''' Exibe uma message box de erro 
    ''' </summary>
    ''' <flow>3273725</flow>
    ''' <analyst>
    ''' Bruno Nunes Trassante
    ''' </analyst>
    ''' <date>8/7/2010</date>
    ''' <param name="mensagem"></param>
    ''' <remarks></remarks>
    Public Sub ShowErrorMessage(ByVal mensagem As String)

        ShowErrorMessage(mensagem, My.Settings.ErrorTitle)

    End Sub

    ''' <summary>
    ''' Exibe uma message box de erro 
    ''' </summary>
    ''' <flow>3273725</flow>
    ''' <analyst>
    ''' Bruno Nunes Trassante
    ''' </analyst>
    ''' <date>8/7/2010</date>
    ''' <remarks></remarks>
    Public Function ShowErrorInfoMessage(ByVal message As String, ByVal titulo As String) As Windows.Forms.DialogResult

        Return MessageBox.Show(message, titulo, MessageBoxButtons.OK, MessageBoxIcon.Warning)

    End Function



    ''' <summary>
    ''' Exibe uma message box de confirma��o 
    ''' </summary>
    ''' <flow>3273725</flow>
    ''' <analyst>
    ''' Lucas Cassaniga Duarte
    ''' </analyst>
    ''' <date>24/7/2010</date>
    ''' <param name="message"></param>
    ''' <param name="titulo"></param>
    ''' <remarks></remarks>
    Public Function ShowConfirmMessage(ByVal message As String, ByVal titulo As String) As Windows.Forms.DialogResult
        Return MessageBox.Show(message, titulo, MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
    End Function

    ''' <summary>
    ''' Exibe uma message box de alerta 
    ''' </summary>
    ''' <flow>3273725</flow>
    ''' <analyst>
    ''' Lucas Cassaniga Duarte
    ''' </analyst>
    ''' <date>8/7/2010</date>
    ''' <param name="menssagem"></param>
    ''' <param name="titulo"></param>
    ''' <remarks></remarks>
    Public Sub ShowAlertMessage(ByVal menssagem As String, ByVal titulo As String)

        MessageBox.Show(menssagem, titulo, MessageBoxButtons.OK, MessageBoxIcon.Warning)

    End Sub

    ''' <summary>
    ''' Exibe uma message box de informa��o 
    ''' </summary>
    ''' <flow>3273725</flow>
    ''' <analyst>
    ''' Lucas Cassaniga Duarte
    ''' </analyst>
    ''' <date>8/7/2010</date>
    ''' <param name="menssagem"></param>
    ''' <param name="titulo"></param>
    ''' <remarks></remarks>
    Public Sub ShowInformationMessage(ByVal menssagem As String, ByVal titulo As String)

        MessageBox.Show(menssagem, titulo, MessageBoxButtons.OK, MessageBoxIcon.Information)

    End Sub


    ''' <summary>
    ''' Exibe uma message box de confirma��o sim ou n�o
    ''' </summary>
    ''' <flow>3273725</flow>
    ''' <analyst>
    ''' Lucas Cassaniga Duarte
    ''' </analyst>
    ''' <date>24/7/2010</date>
    ''' <param name="message"></param>
    ''' <param name="titulo"></param>
    ''' <remarks></remarks>
    Public Function ShowConfirmYesNoMessage(ByVal message As String, ByVal titulo As String) As Windows.Forms.DialogResult
        Return MessageBox.Show(message, titulo, MessageBoxButtons.YesNo, MessageBoxIcon.Information)
    End Function


#End Region

End Class
