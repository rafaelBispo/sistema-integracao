Imports System.Collections.Generic
Imports System.Drawing

''' <remarks>
''' Cont�m m�todos auxili�res para 
''' </remarks>
Public NotInheritable Class ExcelGenerator
    Private Sub New()
    End Sub
    ''' <summary>
    ''' Cria um documento do Excel (.xls) baseado na estrutura de dados (Proposta - Pasta - Agrupador - Campo).
    ''' </summary>
    ''' <example>
    ''' <code>
    ''' List(KeyValuePair(string, string)) data = new List(KeyValuePair(string, string))();
    ''' 
    ''' data.Add(new KeyValuePair(string, string)("Dados T�cnicos", "1"));
    ''' data.Add(new KeyValuePair(string, string)("Dados T�cnicos.Question�rio da Proposta", "1"));
    ''' data.Add(new KeyValuePair(string, string)("1.Dados T�cnicos.Question�rio da Proposta.Campo1", "Valor1"));
    ''' data.Add(new KeyValuePair(string, string)("1.Dados T�cnicos.Question�rio da Proposta.Campo2", "Valor2"));
    ''' data.Add(new KeyValuePair(string, string)("1.Dados T�cnicos.Question�rio da Proposta.Campo3", "Valor3"));
    ''' 
    ''' // Adiciona um grid com 2 colunas e 2 linhas
    ''' data.Add(new KeyValuePair(string, string)("1.Dados T�cnicos.Question�rio da Proposta.Campo4", "primeiroValor.segundoValor"));
    ''' data.Add(new KeyValuePair(string, string)("1.Dados T�cnicos.Question�rio da Proposta.Campo5", "primeiroValor.segundoValor"));
    ''' 
    ''' Export.XLSHelper.CreateExcel(data, @"c:\1.xls");
    ''' </code>
    ''' </example>
    ''' <param name="data">Dados que serao exportados.</param>
    ''' <param name="filePath">Caminho onde o arquivo sera salvo.</param>
    Public Shared Sub CreateExcel(ByVal data As List(Of KeyValuePair(Of String, String)), ByVal filePath As String)

        Dim folders As List(Of KeyValuePair(Of String, String)) = New List(Of KeyValuePair(Of String, String))
        Dim groups As List(Of KeyValuePair(Of String, String)) = New List(Of KeyValuePair(Of String, String))
        Dim fieldValues As List(Of KeyValuePair(Of String, String)) = New List(Of KeyValuePair(Of String, String))

        ' Separa os dados de pastas, agrupamentos e campos
        For Each item As KeyValuePair(Of String, String) In data

            Select Case item.Key.Split(".").Length
                Case 1
                    folders.Add(item)
                Case 2
                    groups.Add(item)
                Case 4
                    fieldValues.Add(item)
            End Select

        Next item

        CreateExcel(folders, groups, fieldValues, filePath)
    End Sub

    ''' <summary>
    ''' Cria um documento do Excel (.xls) baseado na estrutura de dados (Proposta - Pasta - Agrupador - Campo).
    ''' </summary>
    ''' <example>
    ''' <code>
    ''' List(KeyValuePair(string, string)) data = new List(KeyValuePair(string, string))();
    ''' 
    ''' data.Add(new KeyValuePair(string, string)("Dados T�cnicos", "1"));
    ''' data.Add(new KeyValuePair(string, string)("Dados T�cnicos.Question�rio da Proposta", "1"));
    ''' data.Add(new KeyValuePair(string, string)("1.Dados T�cnicos.Question�rio da Proposta.Campo1", "Valor1"));
    ''' data.Add(new KeyValuePair(string, string)("1.Dados T�cnicos.Question�rio da Proposta.Campo2", "Valor2"));
    ''' data.Add(new KeyValuePair(string, string)("1.Dados T�cnicos.Question�rio da Proposta.Campo3", "Valor3"));
    ''' 
    ''' // Adiciona um grid com 2 colunas e 2 linhas
    ''' data.Add(new KeyValuePair(string, string)("1.Dados T�cnicos.Question�rio da Proposta.Campo4", "primeiroValor.segundoValor"));
    ''' data.Add(new KeyValuePair(string, string)("1.Dados T�cnicos.Question�rio da Proposta.Campo5", "primeiroValor.segundoValor"));
    ''' 
    ''' Export.XLSHelper.CreateExcel(data, @"c:\1.xls");
    ''' </code>
    ''' </example>
    ''' <param name="folders"></param>
    ''' <param name="groups"></param>
    ''' <param name="fieldValues"></param>
    Private Shared Sub CreateExcel(ByVal folders As List(Of KeyValuePair(Of String, String)), ByVal groups As List(Of KeyValuePair(Of String, String)), ByVal fieldValues As List(Of KeyValuePair(Of String, String)), ByVal filePath As String)

        'Enum do excel
        Const XlWBATemplate_xlWBATWorksheet = -4167
        Const XlFileFormat_xlWorkbookNormal = -4143
        Const XlSaveAsAccessMode_xlShared = 2

        Dim xls As Object = CreateObject("Excel.Application")
        Dim wb As Object = xls.Workbooks.Add(XlWBATemplate_xlWBATWorksheet)
        Dim ws As Object = wb.Worksheets(1)
        Dim initialRange As Object, initialGroupRange As Object, finalRange As Object, entryInitialRange As Object
        Dim rowIndex As Integer = 1, columnIndex As Integer = 1, newEntryInitialRow As Integer = 1
        Dim currentEntry As String = String.Empty
        Dim primeiro = True

        Try
            ' Desabilita alertas do excel para o usu�rio
            xls.DisplayAlerts = False

            ' Guarda o range inicial da pasta corrente.
            entryInitialRange = ws.Cells(rowIndex, columnIndex)

            ' xls.Visible = True


            ' Para cada pasta, de uma proposta
            For Each folder As KeyValuePair(Of String, String) In folders

                ' Se for uma nova pasta
                If currentEntry <> folder.Value Then


                    If (currentEntry <> String.Empty) Then
                        primeiro = False
                    End If

                    ' Se j� existe uma pasta criada anteriormente, faz merge entre o range inicial e final das celulas utilizadas pela mesma
                    'If Not String.IsNullOrEmpty(currentEntry) Then
                    '    MergeCenter(ws.Cells.Range(entryInitialRange, ws.Cells(entryInitialRange.Row, columnIndex - 2)))
                    'End If

                    ' seta posi��o onde os dados da nova entrada (Proposta) inciar�o
                    rowIndex = newEntryInitialRow

                    If Not primeiro Then
                        rowIndex -= 3
                    End If

                    columnIndex = 1

                    ' Seta o nome para entrada correne e escreve no arquivo
                    currentEntry = folder.Value
                    'ws.Cells(rowIndex, columnIndex) = folder.Value
                    'ws.Columns.AutoFit()

                    ' Guarda o range inicial onde a nova pasta come�ou a ser escrita
                    entryInitialRange = ws.Cells(rowIndex, columnIndex)



                    ' Seta o estilo para nova entrada (Proposta)
                    'SetEntryStyle(entryInitialRange)



                End If

                'If (rowIndex < 1) Then
                rowIndex += 1
                ' End If



                'ws.Cells(rowIndex, columnIndex) = folder.Key
                'ws.Columns.AutoFit()

                initialRange = ws.Cells(rowIndex, columnIndex)

                finalRange = initialRange

                'SetFolderStyle(initialRange)

                Dim tempNext = 0
                tempNext = System.Math.Max(System.Threading.Interlocked.Increment(rowIndex), rowIndex - 1)
                If newEntryInitialRow < tempNext Then
                    newEntryInitialRow = tempNext
                End If
                Dim folderGroups As List(Of KeyValuePair(Of String, String)) = New List(Of KeyValuePair(Of String, String))

                ' Filtra os grupos buscando os registroos referentes a pasta corrente
                For Each item As KeyValuePair(Of String, String) In groups
                    If item.Key.Split(".")(0).Equals(folder.Key) AndAlso item.Value.Equals(folder.Value) Then
                        folderGroups.Add(item)
                    End If
                Next item

                ' Para cada grupo da pasta corrente
                For Each group As KeyValuePair(Of String, String) In folderGroups
                    'ws.Cells(rowIndex, columnIndex) = group.Key.Split(".")(1)
                    'ws.Columns.AutoFit()

                    initialGroupRange = ws.Cells(rowIndex, columnIndex)
                    'SetGroupStyle(initialGroupRange)

                    tempNext = System.Math.Max(System.Threading.Interlocked.Increment(rowIndex), rowIndex - 1)

                    If newEntryInitialRow < tempNext Then
                        newEntryInitialRow = tempNext
                    End If

                    Dim groupsFieldValues As List(Of KeyValuePair(Of String, String)) = New List(Of KeyValuePair(Of String, String))

                    ' Filtra os campos buscando os registros do grupo corrente
                    For Each item As KeyValuePair(Of String, String) In fieldValues
                        If item.Key.Split(".")(0).Equals(group.Value) AndAlso item.Key.Split(".")(1).Equals(folder.Key.Split(".")(0)) AndAlso item.Key.Split(".")(2).Equals(group.Key.Split(".")(1)) Then
                            groupsFieldValues.Add(item)
                        End If
                    Next item

                    Dim increment As Integer = 0
                    ' Para cada campo do grupo corrente
                    For Each fieldValue As KeyValuePair(Of String, String) In groupsFieldValues
                        If (primeiro) Then
                            ws.Cells(rowIndex, columnIndex) = fieldValue.Key.Split(".")(3)
                            ws.Columns.AutoFit()
                            SetFieldStyle(ws.Cells(rowIndex, columnIndex))


                        End If


                        tempNext = System.Math.Max(System.Threading.Interlocked.Increment(rowIndex), rowIndex + increment - 1)

                        If newEntryInitialRow < tempNext Then
                            newEntryInitialRow = tempNext
                        End If

                        'Garante o funcionamento caso o valor do campos venha NULL
                        Dim values As String()
                        If Not String.IsNullOrEmpty(fieldValue.Value) Then
                            values = fieldValue.Value.Split(New Char() {"#"c, "�"c}, StringSplitOptions.RemoveEmptyEntries)

                            If values.Length > 0 Then
                                If values.Length = 1 Then
                                    ws.Cells(rowIndex, columnIndex) = fieldValue.Value
                                    ws.Columns.AutoFit()

                                    SetValueStyle(ws.Cells(rowIndex, columnIndex))

                                Else
                                    For i As Integer = 0 To values.Length - 1
                                        ws.Cells(rowIndex, columnIndex) = values(i)
                                        ws.Columns.AutoFit()

                                        SetValueStyle(ws.Cells(rowIndex, columnIndex))

                                        'newEntryInitialRow += 1
                                        rowIndex += 1
                                    Next

                                    increment = values.Length
                                    rowIndex -= values.Length
                                End If
                            End If
                        Else
                            ws.Cells(rowIndex, columnIndex) = String.Empty
                            SetValueStyle(ws.Cells(rowIndex, columnIndex))
                        End If

                        'Conta o ultimo valor de coluna utilizado para fazer o espa�amento entre os agrupamentos
                        finalRange = ws.Cells(rowIndex, columnIndex)
                        'Voltar uma celula acima, escrevendo valor ou n�o
                        rowIndex -= 1

                        'Linha separadora de pastas
                        columnIndex += 1
                    Next

                    rowIndex -= 1


                    'MergeCenter(ws.Cells.Range(initialGroupRange, ws.Cells(initialGroupRange.Row, finalRange.Column)))

                    'Coluna a mais no final do arquivo
                    'columnIndex += 1

                    Dim r As Object = ws.Cells.Range(ws.Cells(finalRange.Row, finalRange.Column + 1), ws.Cells(finalRange.Row, finalRange.Column + 1))
                    r.ColumnWidth = 0.75
                Next

                rowIndex -= 2
                'newEntryInitialRow += 2

                'MergeCenter(ws.Cells.Range(initialRange, ws.Cells(initialRange.Row, finalRange.Column)))

            Next


            'MergeCenter(ws.Cells.Range(entryInitialRange, ws.Cells(entryInitialRange.Row, columnIndex - 2)))

            ' Salva o arquivo
            wb.SaveAs(filePath, XlFileFormat_xlWorkbookNormal, Type.Missing, Type.Missing, Type.Missing, Type.Missing, _
             XlSaveAsAccessMode_xlShared, Type.Missing, Type.Missing, Type.Missing, Type.Missing)
        Catch ex As Exception
            Throw ex
        Finally
            xls.Application.Quit()
            System.Runtime.InteropServices.Marshal.ReleaseComObject(xls)
        End Try
    End Sub

    '''' <summary>
    '''' Une um determinado range de colunas e centraliza o valor.
    '''' </summary>
    '''' <param name="range">Range que sera feito o merge e tera seu valor centralizado.</param>
    'Private Shared Sub MergeCenter(ByVal range As Object)
    '    range.MergeCells = True
    '    range.HorizontalAlignment = XlHAlign.xlHAlignCenter
    'End Sub

    ''' <summary>
    ''' Seta o estilo para celulas pasta.
    ''' </summary>
    ''' <param name="entryRange">Range que contenha celulas do agrupador maior (Proposta).</param>
    Private Shared Sub SetEntryStyle(ByVal entryRange As Object)
        entryRange.Font.Italic = True
        entryRange.Font.Bold = True
        entryRange.Font.Size = 13
        entryRange.Font.Color = ColorTranslator.ToOle(Color.White)

        entryRange.Interior.Color = ColorTranslator.ToOle(Color.Black)
    End Sub

    ''' <summary>
    ''' Seta o estilo para celulas pasta.
    ''' </summary>
    ''' <param name="folderRange">Range que contenha celulas de pasta.</param>
    Private Shared Sub SetFolderStyle(ByVal folderRange As Object)
        folderRange.Font.Italic = True
        folderRange.Font.Bold = True
        folderRange.Font.Size = 12
        folderRange.Font.Color = ColorTranslator.ToOle(Color.White)

        folderRange.Interior.Color = ColorTranslator.ToOle(Color.Gray)
    End Sub

    ''' <summary>
    ''' Seta o estilo para celulas grupo.
    ''' </summary>
    ''' <param name="groupRange">Range que contenha celulas de grupo.</param>
    Private Shared Sub SetGroupStyle(ByVal groupRange As Object)
        groupRange.Font.Bold = True
        groupRange.Font.Size = 11
        groupRange.Font.Color = ColorTranslator.ToOle(Color.Gray)

        groupRange.Interior.Color = ColorTranslator.ToOle(Color.LightGray)
    End Sub

    ''' <summary>
    ''' Seta o estilo para celulas campos.
    ''' </summary>
    ''' <param name="fieldRange">Range que contenha celulas de campos.</param>
    Private Shared Sub SetFieldStyle(ByVal fieldRange As Object)
        fieldRange.Font.Bold = True
        fieldRange.Font.Size = 10
        fieldRange.Font.Color = ColorTranslator.ToOle(Color.Black)

        'fieldRange.Interior.Color = ColorTranslator.ToOle(Color.White)
    End Sub

    ''' <summary>
    ''' Seta o estilo para celulas valores.
    ''' </summary>
    ''' <param name="valueRange">Range que contenha celulas de valor.</param>
    Private Shared Sub SetValueStyle(ByVal valueRange As Object)
        valueRange.Font.Size = 10
        valueRange.Font.Color = ColorTranslator.ToOle(Color.Black)

        'valueRange.Interior.Color = ColorTranslator.ToOle(Color.White)
    End Sub
End Class

