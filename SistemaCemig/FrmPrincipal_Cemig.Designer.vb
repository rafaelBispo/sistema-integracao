﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPrincipal_Cemig
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPrincipal_Cemig))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnGerencial = New System.Windows.Forms.Button()
        Me.btnAbrirChamado = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ArquivoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TrocarDeUsuárioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SuporteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.SairToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RequisiçãoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CadastroContribuinteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExcluirContribuinteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AnaliseExclusãoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GerêncialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AbrirChamadoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LiberaFaturamentoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BorderôToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BaixaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.GerêncialToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RelatôriosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.GerênciaisToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HistóricoOperadoraSintéticoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HistóricoOperadorAnaliticoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FichasEmAbertoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AnaliticoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AnaliticoPorCidadeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SinteticoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FichasEmAbertoToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AnaliticoToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AnaliticoPorCidadeToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.SintêticoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OperadorMensageiroToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HistóricoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OperadorSintéticoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FichasNãoImpressasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusbarPrincipal = New System.Windows.Forms.StatusStrip()
        Me.stripUsuario = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnAtualizar = New System.Windows.Forms.Button()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.lblSolicitacaoFila = New System.Windows.Forms.Label()
        Me.lblValor = New System.Windows.Forms.Label()
        Me.btnPesquisar = New System.Windows.Forms.Button()
        Me.lblSolicitacaoAceita = New System.Windows.Forms.Label()
        Me.dtFinal = New System.Windows.Forms.DateTimePicker()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.cboCategoria = New System.Windows.Forms.ComboBox()
        Me.dtInicial = New System.Windows.Forms.DateTimePicker()
        Me.lblTipoPesquisa = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.lblValorMensal = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.lblSolicitacaoAceitaMensal = New System.Windows.Forms.Label()
        Me.lblSolicitacaoFilaMensal = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.lblValorDiario = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.lblSolicitacaoAceitaDiario = New System.Windows.Forms.Label()
        Me.lblSolicitacaoFilaDiario = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnRequisicoesRealizadas = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.StatusbarPrincipal.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.AutoSize = True
        Me.Panel1.Controls.Add(Me.btnRequisicoesRealizadas)
        Me.Panel1.Controls.Add(Me.btnGerencial)
        Me.Panel1.Controls.Add(Me.btnAbrirChamado)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 24)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(963, 62)
        Me.Panel1.TabIndex = 3
        '
        'btnGerencial
        '
        Me.btnGerencial.Location = New System.Drawing.Point(84, 3)
        Me.btnGerencial.Name = "btnGerencial"
        Me.btnGerencial.Size = New System.Drawing.Size(75, 56)
        Me.btnGerencial.TabIndex = 8
        Me.btnGerencial.Text = "Gerêncial"
        Me.btnGerencial.UseVisualStyleBackColor = True
        '
        'btnAbrirChamado
        '
        Me.btnAbrirChamado.Location = New System.Drawing.Point(3, 3)
        Me.btnAbrirChamado.Name = "btnAbrirChamado"
        Me.btnAbrirChamado.Size = New System.Drawing.Size(75, 56)
        Me.btnAbrirChamado.TabIndex = 9
        Me.btnAbrirChamado.Text = "Captação Cemig"
        Me.btnAbrirChamado.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ArquivoToolStripMenuItem, Me.RequisiçãoToolStripMenuItem, Me.GerêncialToolStripMenuItem, Me.RelatôriosToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(963, 24)
        Me.MenuStrip1.TabIndex = 5
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ArquivoToolStripMenuItem
        '
        Me.ArquivoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TrocarDeUsuárioToolStripMenuItem, Me.SuporteToolStripMenuItem, Me.ToolStripSeparator1, Me.SairToolStripMenuItem1})
        Me.ArquivoToolStripMenuItem.Name = "ArquivoToolStripMenuItem"
        Me.ArquivoToolStripMenuItem.Size = New System.Drawing.Size(61, 20)
        Me.ArquivoToolStripMenuItem.Text = "&Arquivo"
        '
        'TrocarDeUsuárioToolStripMenuItem
        '
        Me.TrocarDeUsuárioToolStripMenuItem.Name = "TrocarDeUsuárioToolStripMenuItem"
        Me.TrocarDeUsuárioToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.TrocarDeUsuárioToolStripMenuItem.Text = "Trocar de Usuário"
        '
        'SuporteToolStripMenuItem
        '
        Me.SuporteToolStripMenuItem.Name = "SuporteToolStripMenuItem"
        Me.SuporteToolStripMenuItem.Size = New System.Drawing.Size(165, 22)
        Me.SuporteToolStripMenuItem.Text = "Suporte"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(162, 6)
        '
        'SairToolStripMenuItem1
        '
        Me.SairToolStripMenuItem1.Name = "SairToolStripMenuItem1"
        Me.SairToolStripMenuItem1.Size = New System.Drawing.Size(165, 22)
        Me.SairToolStripMenuItem1.Text = "Sair"
        '
        'RequisiçãoToolStripMenuItem
        '
        Me.RequisiçãoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CadastroContribuinteToolStripMenuItem, Me.UsuarioToolStripMenuItem, Me.ExcluirContribuinteToolStripMenuItem, Me.AnaliseExclusãoToolStripMenuItem})
        Me.RequisiçãoToolStripMenuItem.Name = "RequisiçãoToolStripMenuItem"
        Me.RequisiçãoToolStripMenuItem.Size = New System.Drawing.Size(66, 20)
        Me.RequisiçãoToolStripMenuItem.Text = "&Cadastro"
        Me.RequisiçãoToolStripMenuItem.Visible = False
        '
        'CadastroContribuinteToolStripMenuItem
        '
        Me.CadastroContribuinteToolStripMenuItem.Name = "CadastroContribuinteToolStripMenuItem"
        Me.CadastroContribuinteToolStripMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.CadastroContribuinteToolStripMenuItem.Text = "&Contribuinte"
        '
        'UsuarioToolStripMenuItem
        '
        Me.UsuarioToolStripMenuItem.Name = "UsuarioToolStripMenuItem"
        Me.UsuarioToolStripMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.UsuarioToolStripMenuItem.Text = "&Usuario"
        '
        'ExcluirContribuinteToolStripMenuItem
        '
        Me.ExcluirContribuinteToolStripMenuItem.Name = "ExcluirContribuinteToolStripMenuItem"
        Me.ExcluirContribuinteToolStripMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.ExcluirContribuinteToolStripMenuItem.Text = "Excluir Contribuinte"
        '
        'AnaliseExclusãoToolStripMenuItem
        '
        Me.AnaliseExclusãoToolStripMenuItem.Name = "AnaliseExclusãoToolStripMenuItem"
        Me.AnaliseExclusãoToolStripMenuItem.Size = New System.Drawing.Size(179, 22)
        Me.AnaliseExclusãoToolStripMenuItem.Text = "Analise Exclusão"
        '
        'GerêncialToolStripMenuItem
        '
        Me.GerêncialToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AbrirChamadoToolStripMenuItem, Me.LiberaFaturamentoToolStripMenuItem, Me.BorderôToolStripMenuItem, Me.BaixaToolStripMenuItem, Me.ToolStripSeparator2, Me.GerêncialToolStripMenuItem1})
        Me.GerêncialToolStripMenuItem.Name = "GerêncialToolStripMenuItem"
        Me.GerêncialToolStripMenuItem.Size = New System.Drawing.Size(68, 20)
        Me.GerêncialToolStripMenuItem.Text = "Gerêncial"
        Me.GerêncialToolStripMenuItem.Visible = False
        '
        'AbrirChamadoToolStripMenuItem
        '
        Me.AbrirChamadoToolStripMenuItem.Name = "AbrirChamadoToolStripMenuItem"
        Me.AbrirChamadoToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.AbrirChamadoToolStripMenuItem.Text = "Abrir Chamado"
        '
        'LiberaFaturamentoToolStripMenuItem
        '
        Me.LiberaFaturamentoToolStripMenuItem.Name = "LiberaFaturamentoToolStripMenuItem"
        Me.LiberaFaturamentoToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.LiberaFaturamentoToolStripMenuItem.Text = "Libera Faturamento"
        '
        'BorderôToolStripMenuItem
        '
        Me.BorderôToolStripMenuItem.Name = "BorderôToolStripMenuItem"
        Me.BorderôToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.BorderôToolStripMenuItem.Text = "Borderô"
        '
        'BaixaToolStripMenuItem
        '
        Me.BaixaToolStripMenuItem.Name = "BaixaToolStripMenuItem"
        Me.BaixaToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.BaixaToolStripMenuItem.Text = "Baixa"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(174, 6)
        '
        'GerêncialToolStripMenuItem1
        '
        Me.GerêncialToolStripMenuItem1.Name = "GerêncialToolStripMenuItem1"
        Me.GerêncialToolStripMenuItem1.Size = New System.Drawing.Size(177, 22)
        Me.GerêncialToolStripMenuItem1.Text = "Gerêncial"
        '
        'RelatôriosToolStripMenuItem
        '
        Me.RelatôriosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.GerênciaisToolStripMenuItem, Me.OperadorMensageiroToolStripMenuItem, Me.FichasNãoImpressasToolStripMenuItem})
        Me.RelatôriosToolStripMenuItem.Name = "RelatôriosToolStripMenuItem"
        Me.RelatôriosToolStripMenuItem.Size = New System.Drawing.Size(71, 20)
        Me.RelatôriosToolStripMenuItem.Text = "Relatorios"
        Me.RelatôriosToolStripMenuItem.Visible = False
        '
        'GerênciaisToolStripMenuItem
        '
        Me.GerênciaisToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HistóricoOperadoraSintéticoToolStripMenuItem, Me.HistóricoOperadorAnaliticoToolStripMenuItem, Me.FichasEmAbertoToolStripMenuItem, Me.FichasEmAbertoToolStripMenuItem1})
        Me.GerênciaisToolStripMenuItem.Name = "GerênciaisToolStripMenuItem"
        Me.GerênciaisToolStripMenuItem.Size = New System.Drawing.Size(197, 22)
        Me.GerênciaisToolStripMenuItem.Text = "Gerênciais"
        '
        'HistóricoOperadoraSintéticoToolStripMenuItem
        '
        Me.HistóricoOperadoraSintéticoToolStripMenuItem.Name = "HistóricoOperadoraSintéticoToolStripMenuItem"
        Me.HistóricoOperadoraSintéticoToolStripMenuItem.Size = New System.Drawing.Size(225, 22)
        Me.HistóricoOperadoraSintéticoToolStripMenuItem.Text = "Histórico Operador Sintético"
        '
        'HistóricoOperadorAnaliticoToolStripMenuItem
        '
        Me.HistóricoOperadorAnaliticoToolStripMenuItem.Name = "HistóricoOperadorAnaliticoToolStripMenuItem"
        Me.HistóricoOperadorAnaliticoToolStripMenuItem.Size = New System.Drawing.Size(225, 22)
        Me.HistóricoOperadorAnaliticoToolStripMenuItem.Text = "Histórico Operador Analitico"
        '
        'FichasEmAbertoToolStripMenuItem
        '
        Me.FichasEmAbertoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AnaliticoToolStripMenuItem, Me.AnaliticoPorCidadeToolStripMenuItem, Me.SinteticoToolStripMenuItem})
        Me.FichasEmAbertoToolStripMenuItem.Name = "FichasEmAbertoToolStripMenuItem"
        Me.FichasEmAbertoToolStripMenuItem.Size = New System.Drawing.Size(225, 22)
        Me.FichasEmAbertoToolStripMenuItem.Text = "Fichas não impressas"
        '
        'AnaliticoToolStripMenuItem
        '
        Me.AnaliticoToolStripMenuItem.Name = "AnaliticoToolStripMenuItem"
        Me.AnaliticoToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.AnaliticoToolStripMenuItem.Text = "Analitico"
        '
        'AnaliticoPorCidadeToolStripMenuItem
        '
        Me.AnaliticoPorCidadeToolStripMenuItem.Name = "AnaliticoPorCidadeToolStripMenuItem"
        Me.AnaliticoPorCidadeToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.AnaliticoPorCidadeToolStripMenuItem.Text = "Analitico por Cidade"
        '
        'SinteticoToolStripMenuItem
        '
        Me.SinteticoToolStripMenuItem.Name = "SinteticoToolStripMenuItem"
        Me.SinteticoToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.SinteticoToolStripMenuItem.Text = "Sintetico"
        '
        'FichasEmAbertoToolStripMenuItem1
        '
        Me.FichasEmAbertoToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AnaliticoToolStripMenuItem1, Me.AnaliticoPorCidadeToolStripMenuItem1, Me.SintêticoToolStripMenuItem})
        Me.FichasEmAbertoToolStripMenuItem1.Name = "FichasEmAbertoToolStripMenuItem1"
        Me.FichasEmAbertoToolStripMenuItem1.Size = New System.Drawing.Size(225, 22)
        Me.FichasEmAbertoToolStripMenuItem1.Text = "Fichas em aberto"
        '
        'AnaliticoToolStripMenuItem1
        '
        Me.AnaliticoToolStripMenuItem1.Name = "AnaliticoToolStripMenuItem1"
        Me.AnaliticoToolStripMenuItem1.Size = New System.Drawing.Size(182, 22)
        Me.AnaliticoToolStripMenuItem1.Text = "Analitico"
        '
        'AnaliticoPorCidadeToolStripMenuItem1
        '
        Me.AnaliticoPorCidadeToolStripMenuItem1.Name = "AnaliticoPorCidadeToolStripMenuItem1"
        Me.AnaliticoPorCidadeToolStripMenuItem1.Size = New System.Drawing.Size(182, 22)
        Me.AnaliticoPorCidadeToolStripMenuItem1.Text = "Analitico por Cidade"
        '
        'SintêticoToolStripMenuItem
        '
        Me.SintêticoToolStripMenuItem.Name = "SintêticoToolStripMenuItem"
        Me.SintêticoToolStripMenuItem.Size = New System.Drawing.Size(182, 22)
        Me.SintêticoToolStripMenuItem.Text = "Sintêtico"
        '
        'OperadorMensageiroToolStripMenuItem
        '
        Me.OperadorMensageiroToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HistóricoToolStripMenuItem})
        Me.OperadorMensageiroToolStripMenuItem.Name = "OperadorMensageiroToolStripMenuItem"
        Me.OperadorMensageiroToolStripMenuItem.Size = New System.Drawing.Size(197, 22)
        Me.OperadorMensageiroToolStripMenuItem.Text = "Operador / Mensageiro"
        '
        'HistóricoToolStripMenuItem
        '
        Me.HistóricoToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.OperadorSintéticoToolStripMenuItem})
        Me.HistóricoToolStripMenuItem.Name = "HistóricoToolStripMenuItem"
        Me.HistóricoToolStripMenuItem.Size = New System.Drawing.Size(122, 22)
        Me.HistóricoToolStripMenuItem.Text = "Histórico"
        '
        'OperadorSintéticoToolStripMenuItem
        '
        Me.OperadorSintéticoToolStripMenuItem.Name = "OperadorSintéticoToolStripMenuItem"
        Me.OperadorSintéticoToolStripMenuItem.Size = New System.Drawing.Size(173, 22)
        Me.OperadorSintéticoToolStripMenuItem.Text = "Operador Sintético"
        '
        'FichasNãoImpressasToolStripMenuItem
        '
        Me.FichasNãoImpressasToolStripMenuItem.Name = "FichasNãoImpressasToolStripMenuItem"
        Me.FichasNãoImpressasToolStripMenuItem.Size = New System.Drawing.Size(197, 22)
        Me.FichasNãoImpressasToolStripMenuItem.Text = "Fichas Não Impressas"
        '
        'StatusbarPrincipal
        '
        Me.StatusbarPrincipal.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.StatusbarPrincipal.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.stripUsuario})
        Me.StatusbarPrincipal.Location = New System.Drawing.Point(0, 434)
        Me.StatusbarPrincipal.Name = "StatusbarPrincipal"
        Me.StatusbarPrincipal.Size = New System.Drawing.Size(963, 22)
        Me.StatusbarPrincipal.TabIndex = 17
        Me.StatusbarPrincipal.Text = "StatusStrip1"
        '
        'stripUsuario
        '
        Me.stripUsuario.Name = "stripUsuario"
        Me.stripUsuario.Size = New System.Drawing.Size(119, 17)
        Me.stripUsuario.Text = "ToolStripStatusLabel1"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnAtualizar)
        Me.Panel2.Controls.Add(Me.Panel5)
        Me.Panel2.Controls.Add(Me.Panel4)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Controls.Add(Me.Label1)
        Me.Panel2.Location = New System.Drawing.Point(9, 90)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(760, 275)
        Me.Panel2.TabIndex = 20
        '
        'btnAtualizar
        '
        Me.btnAtualizar.Location = New System.Drawing.Point(15, 151)
        Me.btnAtualizar.Name = "btnAtualizar"
        Me.btnAtualizar.Size = New System.Drawing.Size(75, 23)
        Me.btnAtualizar.TabIndex = 106
        Me.btnAtualizar.Text = "Atualizar"
        Me.btnAtualizar.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel5.Controls.Add(Me.Label22)
        Me.Panel5.Controls.Add(Me.Label3)
        Me.Panel5.Controls.Add(Me.Label23)
        Me.Panel5.Controls.Add(Me.lblSolicitacaoFila)
        Me.Panel5.Controls.Add(Me.lblValor)
        Me.Panel5.Controls.Add(Me.btnPesquisar)
        Me.Panel5.Controls.Add(Me.lblSolicitacaoAceita)
        Me.Panel5.Controls.Add(Me.dtFinal)
        Me.Panel5.Controls.Add(Me.Label6)
        Me.Panel5.Controls.Add(Me.Label45)
        Me.Panel5.Controls.Add(Me.cboCategoria)
        Me.Panel5.Controls.Add(Me.dtInicial)
        Me.Panel5.Controls.Add(Me.lblTipoPesquisa)
        Me.Panel5.Controls.Add(Me.Label4)
        Me.Panel5.Location = New System.Drawing.Point(460, 37)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(283, 220)
        Me.Panel5.TabIndex = 105
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(13, 5)
        Me.Label22.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(184, 18)
        Me.Label22.TabIndex = 98
        Me.Label22.Text = "Resumo Personalizado"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(54, 57)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(118, 15)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Solicitações aceitas:"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(13, 36)
        Me.Label23.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(159, 15)
        Me.Label23.TabIndex = 1
        Me.Label23.Text = "Total de Solicitações na fila:"
        '
        'lblSolicitacaoFila
        '
        Me.lblSolicitacaoFila.AutoSize = True
        Me.lblSolicitacaoFila.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSolicitacaoFila.Location = New System.Drawing.Point(176, 37)
        Me.lblSolicitacaoFila.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblSolicitacaoFila.Name = "lblSolicitacaoFila"
        Me.lblSolicitacaoFila.Size = New System.Drawing.Size(14, 15)
        Me.lblSolicitacaoFila.TabIndex = 89
        Me.lblSolicitacaoFila.Text = "0"
        '
        'lblValor
        '
        Me.lblValor.AutoSize = True
        Me.lblValor.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValor.Location = New System.Drawing.Point(176, 79)
        Me.lblValor.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblValor.Name = "lblValor"
        Me.lblValor.Size = New System.Drawing.Size(14, 15)
        Me.lblValor.TabIndex = 92
        Me.lblValor.Text = "0"
        '
        'btnPesquisar
        '
        Me.btnPesquisar.Location = New System.Drawing.Point(16, 182)
        Me.btnPesquisar.Name = "btnPesquisar"
        Me.btnPesquisar.Size = New System.Drawing.Size(75, 23)
        Me.btnPesquisar.TabIndex = 87
        Me.btnPesquisar.Text = "Pesquisar"
        Me.btnPesquisar.UseVisualStyleBackColor = True
        '
        'lblSolicitacaoAceita
        '
        Me.lblSolicitacaoAceita.AutoSize = True
        Me.lblSolicitacaoAceita.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSolicitacaoAceita.Location = New System.Drawing.Point(176, 58)
        Me.lblSolicitacaoAceita.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblSolicitacaoAceita.Name = "lblSolicitacaoAceita"
        Me.lblSolicitacaoAceita.Size = New System.Drawing.Size(14, 15)
        Me.lblSolicitacaoAceita.TabIndex = 90
        Me.lblSolicitacaoAceita.Text = "0"
        '
        'dtFinal
        '
        Me.dtFinal.CustomFormat = "dd/MM/yyyy"
        Me.dtFinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtFinal.Location = New System.Drawing.Point(104, 158)
        Me.dtFinal.Name = "dtFinal"
        Me.dtFinal.Size = New System.Drawing.Size(108, 20)
        Me.dtFinal.TabIndex = 86
        Me.dtFinal.Value = New Date(2017, 12, 18, 0, 0, 0, 0)
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(75, 78)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(97, 15)
        Me.Label6.TabIndex = 91
        Me.Label6.Text = "Valor Realizado:"
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.Location = New System.Drawing.Point(22, 160)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(71, 16)
        Me.Label45.TabIndex = 85
        Me.Label45.Text = "Data Final:"
        '
        'cboCategoria
        '
        Me.cboCategoria.FormattingEnabled = True
        Me.cboCategoria.Location = New System.Drawing.Point(85, 102)
        Me.cboCategoria.Name = "cboCategoria"
        Me.cboCategoria.Size = New System.Drawing.Size(189, 21)
        Me.cboCategoria.TabIndex = 81
        '
        'dtInicial
        '
        Me.dtInicial.CustomFormat = "dd/MM/yyyy"
        Me.dtInicial.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtInicial.Location = New System.Drawing.Point(104, 131)
        Me.dtInicial.Name = "dtInicial"
        Me.dtInicial.Size = New System.Drawing.Size(108, 20)
        Me.dtInicial.TabIndex = 83
        Me.dtInicial.Value = New Date(2017, 12, 18, 0, 0, 0, 0)
        '
        'lblTipoPesquisa
        '
        Me.lblTipoPesquisa.AutoSize = True
        Me.lblTipoPesquisa.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoPesquisa.Location = New System.Drawing.Point(14, 103)
        Me.lblTipoPesquisa.Name = "lblTipoPesquisa"
        Me.lblTipoPesquisa.Size = New System.Drawing.Size(72, 16)
        Me.lblTipoPesquisa.TabIndex = 80
        Me.lblTipoPesquisa.Text = "Categoria: "
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(17, 133)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(76, 16)
        Me.Label4.TabIndex = 82
        Me.Label4.Text = "Data Inicial:"
        '
        'Panel4
        '
        Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel4.Controls.Add(Me.lblValorMensal)
        Me.Panel4.Controls.Add(Me.Label15)
        Me.Panel4.Controls.Add(Me.lblSolicitacaoAceitaMensal)
        Me.Panel4.Controls.Add(Me.lblSolicitacaoFilaMensal)
        Me.Panel4.Controls.Add(Me.Label18)
        Me.Panel4.Controls.Add(Me.Label19)
        Me.Panel4.Controls.Add(Me.Label20)
        Me.Panel4.Controls.Add(Me.Label21)
        Me.Panel4.Location = New System.Drawing.Point(238, 37)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(204, 108)
        Me.Panel4.TabIndex = 104
        '
        'lblValorMensal
        '
        Me.lblValorMensal.AutoSize = True
        Me.lblValorMensal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorMensal.Location = New System.Drawing.Point(133, 78)
        Me.lblValorMensal.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblValorMensal.Name = "lblValorMensal"
        Me.lblValorMensal.Size = New System.Drawing.Size(14, 15)
        Me.lblValorMensal.TabIndex = 103
        Me.lblValorMensal.Text = "0"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(36, 78)
        Me.Label15.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(97, 15)
        Me.Label15.TabIndex = 102
        Me.Label15.Text = "Valor Realizado:"
        '
        'lblSolicitacaoAceitaMensal
        '
        Me.lblSolicitacaoAceitaMensal.AutoSize = True
        Me.lblSolicitacaoAceitaMensal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSolicitacaoAceitaMensal.Location = New System.Drawing.Point(133, 57)
        Me.lblSolicitacaoAceitaMensal.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblSolicitacaoAceitaMensal.Name = "lblSolicitacaoAceitaMensal"
        Me.lblSolicitacaoAceitaMensal.Size = New System.Drawing.Size(14, 15)
        Me.lblSolicitacaoAceitaMensal.TabIndex = 101
        Me.lblSolicitacaoAceitaMensal.Text = "0"
        '
        'lblSolicitacaoFilaMensal
        '
        Me.lblSolicitacaoFilaMensal.AutoSize = True
        Me.lblSolicitacaoFilaMensal.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSolicitacaoFilaMensal.Location = New System.Drawing.Point(133, 36)
        Me.lblSolicitacaoFilaMensal.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblSolicitacaoFilaMensal.Name = "lblSolicitacaoFilaMensal"
        Me.lblSolicitacaoFilaMensal.Size = New System.Drawing.Size(14, 15)
        Me.lblSolicitacaoFilaMensal.TabIndex = 100
        Me.lblSolicitacaoFilaMensal.Text = "0"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(17, 57)
        Me.Label18.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(118, 15)
        Me.Label18.TabIndex = 99
        Me.Label18.Text = "Solicitações aceitas:"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(23, 36)
        Me.Label19.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(112, 15)
        Me.Label19.TabIndex = 98
        Me.Label19.Text = "Solicitações na fila:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(21, 36)
        Me.Label20.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(112, 15)
        Me.Label20.TabIndex = 98
        Me.Label20.Text = "Solicitações na fila:"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(17, 5)
        Me.Label21.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(130, 18)
        Me.Label21.TabIndex = 97
        Me.Label21.Text = "Resumo Mensal"
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.lblValorDiario)
        Me.Panel3.Controls.Add(Me.Label7)
        Me.Panel3.Controls.Add(Me.lblSolicitacaoAceitaDiario)
        Me.Panel3.Controls.Add(Me.lblSolicitacaoFilaDiario)
        Me.Panel3.Controls.Add(Me.Label11)
        Me.Panel3.Controls.Add(Me.Label13)
        Me.Panel3.Controls.Add(Me.Label12)
        Me.Panel3.Controls.Add(Me.Label10)
        Me.Panel3.Location = New System.Drawing.Point(15, 37)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(204, 108)
        Me.Panel3.TabIndex = 97
        '
        'lblValorDiario
        '
        Me.lblValorDiario.AutoSize = True
        Me.lblValorDiario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValorDiario.Location = New System.Drawing.Point(133, 78)
        Me.lblValorDiario.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblValorDiario.Name = "lblValorDiario"
        Me.lblValorDiario.Size = New System.Drawing.Size(14, 15)
        Me.lblValorDiario.TabIndex = 103
        Me.lblValorDiario.Text = "0"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(36, 78)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(97, 15)
        Me.Label7.TabIndex = 102
        Me.Label7.Text = "Valor Realizado:"
        '
        'lblSolicitacaoAceitaDiario
        '
        Me.lblSolicitacaoAceitaDiario.AutoSize = True
        Me.lblSolicitacaoAceitaDiario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSolicitacaoAceitaDiario.Location = New System.Drawing.Point(133, 57)
        Me.lblSolicitacaoAceitaDiario.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblSolicitacaoAceitaDiario.Name = "lblSolicitacaoAceitaDiario"
        Me.lblSolicitacaoAceitaDiario.Size = New System.Drawing.Size(14, 15)
        Me.lblSolicitacaoAceitaDiario.TabIndex = 101
        Me.lblSolicitacaoAceitaDiario.Text = "0"
        '
        'lblSolicitacaoFilaDiario
        '
        Me.lblSolicitacaoFilaDiario.AutoSize = True
        Me.lblSolicitacaoFilaDiario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSolicitacaoFilaDiario.Location = New System.Drawing.Point(133, 36)
        Me.lblSolicitacaoFilaDiario.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblSolicitacaoFilaDiario.Name = "lblSolicitacaoFilaDiario"
        Me.lblSolicitacaoFilaDiario.Size = New System.Drawing.Size(14, 15)
        Me.lblSolicitacaoFilaDiario.TabIndex = 100
        Me.lblSolicitacaoFilaDiario.Text = "0"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(17, 57)
        Me.Label11.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(118, 15)
        Me.Label11.TabIndex = 99
        Me.Label11.Text = "Solicitações aceitas:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(23, 36)
        Me.Label13.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(112, 15)
        Me.Label13.TabIndex = 98
        Me.Label13.Text = "Solicitações na fila:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(21, 36)
        Me.Label12.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(112, 15)
        Me.Label12.TabIndex = 98
        Me.Label12.Text = "Solicitações na fila:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(17, 5)
        Me.Label10.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(125, 18)
        Me.Label10.TabIndex = 97
        Me.Label10.Text = "Resumo do Dia"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(11, 11)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(91, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "RESUMO:"
        '
        'btnRequisicoesRealizadas
        '
        Me.btnRequisicoesRealizadas.Location = New System.Drawing.Point(165, 3)
        Me.btnRequisicoesRealizadas.Name = "btnRequisicoesRealizadas"
        Me.btnRequisicoesRealizadas.Size = New System.Drawing.Size(88, 56)
        Me.btnRequisicoesRealizadas.TabIndex = 10
        Me.btnRequisicoesRealizadas.Text = "Consulta Requisições Realizadas"
        Me.btnRequisicoesRealizadas.UseVisualStyleBackColor = True
        '
        'FrmPrincipal_Cemig
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(963, 456)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.StatusbarPrincipal)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "FrmPrincipal_Cemig"
        Me.Text = "Principal"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.StatusbarPrincipal.ResumeLayout(False)
        Me.StatusbarPrincipal.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnGerencial As System.Windows.Forms.Button
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ArquivoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RequisiçãoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GerêncialToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents RelatôriosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TrocarDeUsuárioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SuporteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SairToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CadastroContribuinteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnAbrirChamado As System.Windows.Forms.Button
    Friend WithEvents UsuarioToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AbrirChamadoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusbarPrincipal As System.Windows.Forms.StatusStrip
    Friend WithEvents LiberaFaturamentoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BorderôToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BaixaToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents GerêncialToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents stripUsuario As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents OperadorMensageiroToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HistóricoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OperadorSintéticoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GerênciaisToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HistóricoOperadoraSintéticoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HistóricoOperadorAnaliticoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FichasEmAbertoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AnaliticoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SinteticoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AnaliticoPorCidadeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExcluirContribuinteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FichasEmAbertoToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AnaliticoToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SintêticoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AnaliticoPorCidadeToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AnaliseExclusãoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnAtualizar As System.Windows.Forms.Button
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents lblSolicitacaoFila As System.Windows.Forms.Label
    Friend WithEvents lblValor As System.Windows.Forms.Label
    Friend WithEvents btnPesquisar As System.Windows.Forms.Button
    Friend WithEvents lblSolicitacaoAceita As System.Windows.Forms.Label
    Friend WithEvents dtFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents cboCategoria As System.Windows.Forms.ComboBox
    Friend WithEvents dtInicial As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblTipoPesquisa As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents lblValorMensal As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents lblSolicitacaoAceitaMensal As System.Windows.Forms.Label
    Friend WithEvents lblSolicitacaoFilaMensal As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents lblValorDiario As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lblSolicitacaoAceitaDiario As System.Windows.Forms.Label
    Friend WithEvents lblSolicitacaoFilaDiario As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents FichasNãoImpressasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnRequisicoesRealizadas As Button
End Class
