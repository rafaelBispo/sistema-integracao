﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FrmConsultaRequisicaoRealizadaCemig
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtDDDPesquisa = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.chkVerificaGrau = New System.Windows.Forms.CheckBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboAceite = New System.Windows.Forms.ComboBox()
        Me.chkPesquisaData = New System.Windows.Forms.CheckBox()
        Me.chkNaoConforme = New System.Windows.Forms.CheckBox()
        Me.dtFinal = New System.Windows.Forms.DateTimePicker()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.btnLimpar = New System.Windows.Forms.Button()
        Me.chkTipoPesquisa = New System.Windows.Forms.CheckBox()
        Me.txtTelefone = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtNome = New System.Windows.Forms.TextBox()
        Me.dtInicial = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboPesquisa = New System.Windows.Forms.ComboBox()
        Me.lblTipoPesquisa = New System.Windows.Forms.Label()
        Me.btnPesquisar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.grdRemessa = New System.Windows.Forms.DataGridView()
        Me.btnSair = New System.Windows.Forms.Button()
        Me.TabInformacoes = New System.Windows.Forms.TabControl()
        Me.tabHistorico = New System.Windows.Forms.TabPage()
        Me.GrdHistorico = New System.Windows.Forms.DataGridView()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btnSalvar = New System.Windows.Forms.Button()
        Me.txtValDoacao = New System.Windows.Forms.TextBox()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.txtMelhorDia = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.chkInativo = New System.Windows.Forms.CheckBox()
        Me.chkNaoPedirMensal = New System.Windows.Forms.CheckBox()
        Me.chkNaoPedirExtra = New System.Windows.Forms.CheckBox()
        Me.chkTelDesl = New System.Windows.Forms.CheckBox()
        Me.txtDtCadastro = New System.Windows.Forms.TextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.txtObs = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.txtReferencia = New System.Windows.Forms.TextBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.txtDtNcMp = New System.Windows.Forms.TextBox()
        Me.txtDtNc = New System.Windows.Forms.TextBox()
        Me.txtRegiao = New System.Windows.Forms.TextBox()
        Me.txtCEP = New System.Windows.Forms.TextBox()
        Me.txtUF = New System.Windows.Forms.TextBox()
        Me.cboBairroDetalhe = New System.Windows.Forms.ComboBox()
        Me.txtDDD = New System.Windows.Forms.TextBox()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.cboCategoriaDetalhe = New System.Windows.Forms.ComboBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cboEspecieDetalhe = New System.Windows.Forms.ComboBox()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.cboOperadorDetalhe = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.cboCidade = New System.Windows.Forms.ComboBox()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.txtTelefone2 = New System.Windows.Forms.TextBox()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtTelefone1 = New System.Windows.Forms.TextBox()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.txtRg = New System.Windows.Forms.TextBox()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.txtCnpjCpf = New System.Windows.Forms.TextBox()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.txtEndereco = New System.Windows.Forms.TextBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.txtNomeCliente2 = New System.Windows.Forms.TextBox()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.txtNomeCliente1 = New System.Windows.Forms.TextBox()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.txtCodBarra = New System.Windows.Forms.TextBox()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.txtCodigo = New System.Windows.Forms.TextBox()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.tbConcluir = New System.Windows.Forms.TabPage()
        Me.Dados = New System.Windows.Forms.GroupBox()
        Me.cbSituacao = New System.Windows.Forms.ComboBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.btnAlterarContribuicao = New System.Windows.Forms.Button()
        Me.btnCancelarMensal = New System.Windows.Forms.Button()
        Me.btnEnviarCancelar = New System.Windows.Forms.Button()
        Me.btnExcluirContribuir = New System.Windows.Forms.Button()
        Me.rtxtObs = New System.Windows.Forms.RichTextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtCpf = New System.Windows.Forms.MaskedTextBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.txtAutorizante = New System.Windows.Forms.TextBox()
        Me.cboCategoriaConcluir = New System.Windows.Forms.ComboBox()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.cboOperadorConcluir = New System.Windows.Forms.ComboBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtCodEanConcluir = New System.Windows.Forms.MaskedTextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtNomeConcluir = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.txtTelefoneConcluir = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.txtValorConcluir = New System.Windows.Forms.TextBox()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.txtDDDConcluir = New System.Windows.Forms.TextBox()
        Me.lblValor = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblTotalFichas = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.lblSemCritica = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.grdRemessa, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabInformacoes.SuspendLayout()
        Me.tabHistorico.SuspendLayout()
        CType(Me.GrdHistorico, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        Me.tbConcluir.SuspendLayout()
        Me.Dados.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.txtDDDPesquisa)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.chkVerificaGrau)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.cboAceite)
        Me.Panel1.Controls.Add(Me.chkPesquisaData)
        Me.Panel1.Controls.Add(Me.chkNaoConforme)
        Me.Panel1.Controls.Add(Me.dtFinal)
        Me.Panel1.Controls.Add(Me.Label45)
        Me.Panel1.Controls.Add(Me.btnLimpar)
        Me.Panel1.Controls.Add(Me.chkTipoPesquisa)
        Me.Panel1.Controls.Add(Me.txtTelefone)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.txtNome)
        Me.Panel1.Controls.Add(Me.dtInicial)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.cboPesquisa)
        Me.Panel1.Controls.Add(Me.lblTipoPesquisa)
        Me.Panel1.Controls.Add(Me.btnPesquisar)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1089, 104)
        Me.Panel1.TabIndex = 1
        '
        'txtDDDPesquisa
        '
        Me.txtDDDPesquisa.Location = New System.Drawing.Point(404, 15)
        Me.txtDDDPesquisa.Name = "txtDDDPesquisa"
        Me.txtDDDPesquisa.Size = New System.Drawing.Size(34, 20)
        Me.txtDDDPesquisa.TabIndex = 141
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(366, 17)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(40, 16)
        Me.Label8.TabIndex = 140
        Me.Label8.Text = "DDD:"
        '
        'chkVerificaGrau
        '
        Me.chkVerificaGrau.AutoSize = True
        Me.chkVerificaGrau.Location = New System.Drawing.Point(691, 55)
        Me.chkVerificaGrau.Name = "chkVerificaGrau"
        Me.chkVerificaGrau.Size = New System.Drawing.Size(180, 17)
        Me.chkVerificaGrau.TabIndex = 139
        Me.chkVerificaGrau.Text = "Verificar por Grau de Parentesco"
        Me.chkVerificaGrau.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(645, 17)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(123, 16)
        Me.Label2.TabIndex = 138
        Me.Label2.Text = "Situação do Aceite:"
        '
        'cboAceite
        '
        Me.cboAceite.FormattingEnabled = True
        Me.cboAceite.Location = New System.Drawing.Point(771, 15)
        Me.cboAceite.Name = "cboAceite"
        Me.cboAceite.Size = New System.Drawing.Size(189, 21)
        Me.cboAceite.TabIndex = 137
        '
        'chkPesquisaData
        '
        Me.chkPesquisaData.AutoSize = True
        Me.chkPesquisaData.Checked = True
        Me.chkPesquisaData.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkPesquisaData.Location = New System.Drawing.Point(548, 49)
        Me.chkPesquisaData.Name = "chkPesquisaData"
        Me.chkPesquisaData.Size = New System.Drawing.Size(113, 17)
        Me.chkPesquisaData.TabIndex = 80
        Me.chkPesquisaData.Text = "Pesquisa por Data"
        Me.chkPesquisaData.UseVisualStyleBackColor = True
        '
        'chkNaoConforme
        '
        Me.chkNaoConforme.AutoSize = True
        Me.chkNaoConforme.Enabled = False
        Me.chkNaoConforme.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkNaoConforme.Location = New System.Drawing.Point(691, 76)
        Me.chkNaoConforme.Margin = New System.Windows.Forms.Padding(2)
        Me.chkNaoConforme.Name = "chkNaoConforme"
        Me.chkNaoConforme.Size = New System.Drawing.Size(276, 17)
        Me.chkNaoConforme.TabIndex = 136
        Me.chkNaoConforme.Text = "Pesquisa Grau de parentesco Não conforme"
        Me.chkNaoConforme.UseVisualStyleBackColor = True
        '
        'dtFinal
        '
        Me.dtFinal.CustomFormat = "dd/MM/yyyy"
        Me.dtFinal.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtFinal.Location = New System.Drawing.Point(435, 74)
        Me.dtFinal.Name = "dtFinal"
        Me.dtFinal.Size = New System.Drawing.Size(108, 20)
        Me.dtFinal.TabIndex = 79
        Me.dtFinal.Value = New Date(2017, 12, 18, 0, 0, 0, 0)
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.Location = New System.Drawing.Point(353, 76)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(71, 16)
        Me.Label45.TabIndex = 78
        Me.Label45.Text = "Data Final:"
        '
        'btnLimpar
        '
        Me.btnLimpar.Location = New System.Drawing.Point(982, 51)
        Me.btnLimpar.Name = "btnLimpar"
        Me.btnLimpar.Size = New System.Drawing.Size(102, 23)
        Me.btnLimpar.TabIndex = 77
        Me.btnLimpar.Text = "Limpar Pesquisa"
        Me.btnLimpar.UseVisualStyleBackColor = True
        '
        'chkTipoPesquisa
        '
        Me.chkTipoPesquisa.AutoSize = True
        Me.chkTipoPesquisa.Checked = True
        Me.chkTipoPesquisa.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTipoPesquisa.Location = New System.Drawing.Point(19, 77)
        Me.chkTipoPesquisa.Name = "chkTipoPesquisa"
        Me.chkTipoPesquisa.Size = New System.Drawing.Size(140, 17)
        Me.chkTipoPesquisa.TabIndex = 76
        Me.chkTipoPesquisa.Text = "Pesquisa por Operadora"
        Me.chkTipoPesquisa.UseVisualStyleBackColor = True
        '
        'txtTelefone
        '
        Me.txtTelefone.Location = New System.Drawing.Point(518, 15)
        Me.txtTelefone.Name = "txtTelefone"
        Me.txtTelefone.Size = New System.Drawing.Size(110, 20)
        Me.txtTelefone.TabIndex = 75
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(448, 17)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 16)
        Me.Label4.TabIndex = 74
        Me.Label4.Text = "Telefone:"
        '
        'txtNome
        '
        Me.txtNome.Location = New System.Drawing.Point(75, 15)
        Me.txtNome.Name = "txtNome"
        Me.txtNome.Size = New System.Drawing.Size(285, 20)
        Me.txtNome.TabIndex = 73
        '
        'dtInicial
        '
        Me.dtInicial.CustomFormat = "dd/MM/yyyy"
        Me.dtInicial.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtInicial.Location = New System.Drawing.Point(435, 47)
        Me.dtInicial.Name = "dtInicial"
        Me.dtInicial.Size = New System.Drawing.Size(108, 20)
        Me.dtInicial.TabIndex = 72
        Me.dtInicial.Value = New Date(2017, 12, 18, 0, 0, 0, 0)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(348, 49)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 16)
        Me.Label3.TabIndex = 71
        Me.Label3.Text = "Data Inicial:"
        '
        'cboPesquisa
        '
        Me.cboPesquisa.FormattingEnabled = True
        Me.cboPesquisa.Location = New System.Drawing.Point(75, 48)
        Me.cboPesquisa.Name = "cboPesquisa"
        Me.cboPesquisa.Size = New System.Drawing.Size(189, 21)
        Me.cboPesquisa.TabIndex = 69
        '
        'lblTipoPesquisa
        '
        Me.lblTipoPesquisa.AutoSize = True
        Me.lblTipoPesquisa.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipoPesquisa.Location = New System.Drawing.Point(4, 49)
        Me.lblTipoPesquisa.Name = "lblTipoPesquisa"
        Me.lblTipoPesquisa.Size = New System.Drawing.Size(71, 16)
        Me.lblTipoPesquisa.TabIndex = 68
        Me.lblTipoPesquisa.Text = "Operador: "
        '
        'btnPesquisar
        '
        Me.btnPesquisar.Location = New System.Drawing.Point(1009, 13)
        Me.btnPesquisar.Name = "btnPesquisar"
        Me.btnPesquisar.Size = New System.Drawing.Size(75, 23)
        Me.btnPesquisar.TabIndex = 67
        Me.btnPesquisar.Text = "Pesquisar"
        Me.btnPesquisar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(27, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nome:"
        '
        'grdRemessa
        '
        Me.grdRemessa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdRemessa.Location = New System.Drawing.Point(12, 122)
        Me.grdRemessa.Name = "grdRemessa"
        Me.grdRemessa.Size = New System.Drawing.Size(1089, 208)
        Me.grdRemessa.TabIndex = 2
        '
        'btnSair
        '
        Me.btnSair.Location = New System.Drawing.Point(946, 553)
        Me.btnSair.Name = "btnSair"
        Me.btnSair.Size = New System.Drawing.Size(75, 23)
        Me.btnSair.TabIndex = 3
        Me.btnSair.Text = "Sair"
        Me.btnSair.UseVisualStyleBackColor = True
        '
        'TabInformacoes
        '
        Me.TabInformacoes.Controls.Add(Me.tabHistorico)
        Me.TabInformacoes.Controls.Add(Me.TabPage2)
        Me.TabInformacoes.Controls.Add(Me.tbConcluir)
        Me.TabInformacoes.Location = New System.Drawing.Point(2, 336)
        Me.TabInformacoes.Margin = New System.Windows.Forms.Padding(2)
        Me.TabInformacoes.Name = "TabInformacoes"
        Me.TabInformacoes.SelectedIndex = 0
        Me.TabInformacoes.Size = New System.Drawing.Size(788, 271)
        Me.TabInformacoes.TabIndex = 87
        '
        'tabHistorico
        '
        Me.tabHistorico.Controls.Add(Me.GrdHistorico)
        Me.tabHistorico.Location = New System.Drawing.Point(4, 22)
        Me.tabHistorico.Margin = New System.Windows.Forms.Padding(2)
        Me.tabHistorico.Name = "tabHistorico"
        Me.tabHistorico.Padding = New System.Windows.Forms.Padding(2)
        Me.tabHistorico.Size = New System.Drawing.Size(780, 245)
        Me.tabHistorico.TabIndex = 0
        Me.tabHistorico.Text = "Histórico"
        Me.tabHistorico.UseVisualStyleBackColor = True
        '
        'GrdHistorico
        '
        Me.GrdHistorico.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GrdHistorico.Location = New System.Drawing.Point(4, 5)
        Me.GrdHistorico.Margin = New System.Windows.Forms.Padding(2)
        Me.GrdHistorico.Name = "GrdHistorico"
        Me.GrdHistorico.Size = New System.Drawing.Size(681, 240)
        Me.GrdHistorico.TabIndex = 3
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.btnSalvar)
        Me.TabPage2.Controls.Add(Me.txtValDoacao)
        Me.TabPage2.Controls.Add(Me.Label29)
        Me.TabPage2.Controls.Add(Me.txtMelhorDia)
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Controls.Add(Me.chkInativo)
        Me.TabPage2.Controls.Add(Me.chkNaoPedirMensal)
        Me.TabPage2.Controls.Add(Me.chkNaoPedirExtra)
        Me.TabPage2.Controls.Add(Me.chkTelDesl)
        Me.TabPage2.Controls.Add(Me.txtDtCadastro)
        Me.TabPage2.Controls.Add(Me.Label30)
        Me.TabPage2.Controls.Add(Me.txtObs)
        Me.TabPage2.Controls.Add(Me.Label25)
        Me.TabPage2.Controls.Add(Me.txtReferencia)
        Me.TabPage2.Controls.Add(Me.Label24)
        Me.TabPage2.Controls.Add(Me.txtDtNcMp)
        Me.TabPage2.Controls.Add(Me.txtDtNc)
        Me.TabPage2.Controls.Add(Me.txtRegiao)
        Me.TabPage2.Controls.Add(Me.txtCEP)
        Me.TabPage2.Controls.Add(Me.txtUF)
        Me.TabPage2.Controls.Add(Me.cboBairroDetalhe)
        Me.TabPage2.Controls.Add(Me.txtDDD)
        Me.TabPage2.Controls.Add(Me.Label34)
        Me.TabPage2.Controls.Add(Me.cboCategoriaDetalhe)
        Me.TabPage2.Controls.Add(Me.Label9)
        Me.TabPage2.Controls.Add(Me.cboEspecieDetalhe)
        Me.TabPage2.Controls.Add(Me.Label27)
        Me.TabPage2.Controls.Add(Me.cboOperadorDetalhe)
        Me.TabPage2.Controls.Add(Me.Label10)
        Me.TabPage2.Controls.Add(Me.Label11)
        Me.TabPage2.Controls.Add(Me.Label12)
        Me.TabPage2.Controls.Add(Me.Label13)
        Me.TabPage2.Controls.Add(Me.Label15)
        Me.TabPage2.Controls.Add(Me.Label23)
        Me.TabPage2.Controls.Add(Me.Label31)
        Me.TabPage2.Controls.Add(Me.cboCidade)
        Me.TabPage2.Controls.Add(Me.Label32)
        Me.TabPage2.Controls.Add(Me.txtTelefone2)
        Me.TabPage2.Controls.Add(Me.Label33)
        Me.TabPage2.Controls.Add(Me.txtTelefone1)
        Me.TabPage2.Controls.Add(Me.Label35)
        Me.TabPage2.Controls.Add(Me.txtRg)
        Me.TabPage2.Controls.Add(Me.Label36)
        Me.TabPage2.Controls.Add(Me.txtCnpjCpf)
        Me.TabPage2.Controls.Add(Me.Label37)
        Me.TabPage2.Controls.Add(Me.txtEndereco)
        Me.TabPage2.Controls.Add(Me.Label38)
        Me.TabPage2.Controls.Add(Me.txtEmail)
        Me.TabPage2.Controls.Add(Me.Label39)
        Me.TabPage2.Controls.Add(Me.txtNomeCliente2)
        Me.TabPage2.Controls.Add(Me.Label40)
        Me.TabPage2.Controls.Add(Me.txtNomeCliente1)
        Me.TabPage2.Controls.Add(Me.Label41)
        Me.TabPage2.Controls.Add(Me.txtCodBarra)
        Me.TabPage2.Controls.Add(Me.Label42)
        Me.TabPage2.Controls.Add(Me.txtCodigo)
        Me.TabPage2.Controls.Add(Me.Label43)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Margin = New System.Windows.Forms.Padding(2)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(2)
        Me.TabPage2.Size = New System.Drawing.Size(780, 245)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Informações"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'btnSalvar
        '
        Me.btnSalvar.Location = New System.Drawing.Point(689, 226)
        Me.btnSalvar.Margin = New System.Windows.Forms.Padding(2)
        Me.btnSalvar.Name = "btnSalvar"
        Me.btnSalvar.Size = New System.Drawing.Size(56, 19)
        Me.btnSalvar.TabIndex = 183
        Me.btnSalvar.Text = "Salvar"
        Me.btnSalvar.UseVisualStyleBackColor = True
        '
        'txtValDoacao
        '
        Me.txtValDoacao.Location = New System.Drawing.Point(632, 119)
        Me.txtValDoacao.Margin = New System.Windows.Forms.Padding(2)
        Me.txtValDoacao.Name = "txtValDoacao"
        Me.txtValDoacao.ReadOnly = True
        Me.txtValDoacao.Size = New System.Drawing.Size(56, 20)
        Me.txtValDoacao.TabIndex = 182
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(629, 104)
        Me.Label29.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(42, 16)
        Me.Label29.TabIndex = 181
        Me.Label29.Text = "Valor:"
        '
        'txtMelhorDia
        '
        Me.txtMelhorDia.Location = New System.Drawing.Point(562, 119)
        Me.txtMelhorDia.Margin = New System.Windows.Forms.Padding(2)
        Me.txtMelhorDia.Name = "txtMelhorDia"
        Me.txtMelhorDia.Size = New System.Drawing.Size(56, 20)
        Me.txtMelhorDia.TabIndex = 180
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(560, 104)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(75, 16)
        Me.Label7.TabIndex = 179
        Me.Label7.Text = "Melhor Dia:"
        '
        'chkInativo
        '
        Me.chkInativo.AutoSize = True
        Me.chkInativo.Location = New System.Drawing.Point(590, 177)
        Me.chkInativo.Margin = New System.Windows.Forms.Padding(2)
        Me.chkInativo.Name = "chkInativo"
        Me.chkInativo.Size = New System.Drawing.Size(58, 17)
        Me.chkInativo.TabIndex = 178
        Me.chkInativo.Text = "Inativo"
        Me.chkInativo.UseVisualStyleBackColor = True
        '
        'chkNaoPedirMensal
        '
        Me.chkNaoPedirMensal.AutoSize = True
        Me.chkNaoPedirMensal.Location = New System.Drawing.Point(590, 214)
        Me.chkNaoPedirMensal.Margin = New System.Windows.Forms.Padding(2)
        Me.chkNaoPedirMensal.Name = "chkNaoPedirMensal"
        Me.chkNaoPedirMensal.Size = New System.Drawing.Size(109, 17)
        Me.chkNaoPedirMensal.TabIndex = 177
        Me.chkNaoPedirMensal.Text = "Não pedir Mensal"
        Me.chkNaoPedirMensal.UseVisualStyleBackColor = True
        '
        'chkNaoPedirExtra
        '
        Me.chkNaoPedirExtra.AutoSize = True
        Me.chkNaoPedirExtra.Location = New System.Drawing.Point(590, 195)
        Me.chkNaoPedirExtra.Margin = New System.Windows.Forms.Padding(2)
        Me.chkNaoPedirExtra.Name = "chkNaoPedirExtra"
        Me.chkNaoPedirExtra.Size = New System.Drawing.Size(98, 17)
        Me.chkNaoPedirExtra.TabIndex = 176
        Me.chkNaoPedirExtra.Text = "Não pedir extra"
        Me.chkNaoPedirExtra.UseVisualStyleBackColor = True
        '
        'chkTelDesl
        '
        Me.chkTelDesl.AutoSize = True
        Me.chkTelDesl.Location = New System.Drawing.Point(590, 233)
        Me.chkTelDesl.Margin = New System.Windows.Forms.Padding(2)
        Me.chkTelDesl.Name = "chkTelDesl"
        Me.chkTelDesl.Size = New System.Drawing.Size(91, 17)
        Me.chkTelDesl.TabIndex = 175
        Me.chkTelDesl.Text = "Tel Desligado"
        Me.chkTelDesl.UseVisualStyleBackColor = True
        '
        'txtDtCadastro
        '
        Me.txtDtCadastro.Location = New System.Drawing.Point(529, 154)
        Me.txtDtCadastro.Margin = New System.Windows.Forms.Padding(2)
        Me.txtDtCadastro.Name = "txtDtCadastro"
        Me.txtDtCadastro.Size = New System.Drawing.Size(80, 20)
        Me.txtDtCadastro.TabIndex = 174
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(526, 139)
        Me.Label30.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(97, 16)
        Me.Label30.TabIndex = 173
        Me.Label30.Text = "Data Cadastro:"
        '
        'txtObs
        '
        Me.txtObs.Location = New System.Drawing.Point(82, 209)
        Me.txtObs.Margin = New System.Windows.Forms.Padding(2)
        Me.txtObs.Multiline = True
        Me.txtObs.Name = "txtObs"
        Me.txtObs.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtObs.Size = New System.Drawing.Size(494, 36)
        Me.txtObs.TabIndex = 172
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(13, 209)
        Me.Label25.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(85, 16)
        Me.Label25.TabIndex = 171
        Me.Label25.Text = "Observação:"
        '
        'txtReferencia
        '
        Me.txtReferencia.Location = New System.Drawing.Point(83, 180)
        Me.txtReferencia.Margin = New System.Windows.Forms.Padding(2)
        Me.txtReferencia.Name = "txtReferencia"
        Me.txtReferencia.Size = New System.Drawing.Size(492, 20)
        Me.txtReferencia.TabIndex = 170
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(13, 181)
        Me.Label24.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(76, 16)
        Me.Label24.TabIndex = 169
        Me.Label24.Text = "Referência:"
        '
        'txtDtNcMp
        '
        Me.txtDtNcMp.Location = New System.Drawing.Point(443, 155)
        Me.txtDtNcMp.Margin = New System.Windows.Forms.Padding(2)
        Me.txtDtNcMp.Name = "txtDtNcMp"
        Me.txtDtNcMp.Size = New System.Drawing.Size(80, 20)
        Me.txtDtNcMp.TabIndex = 168
        '
        'txtDtNc
        '
        Me.txtDtNc.Location = New System.Drawing.Point(357, 155)
        Me.txtDtNc.Margin = New System.Windows.Forms.Padding(2)
        Me.txtDtNc.Name = "txtDtNc"
        Me.txtDtNc.Size = New System.Drawing.Size(80, 20)
        Me.txtDtNc.TabIndex = 167
        '
        'txtRegiao
        '
        Me.txtRegiao.Location = New System.Drawing.Point(310, 155)
        Me.txtRegiao.Margin = New System.Windows.Forms.Padding(2)
        Me.txtRegiao.Name = "txtRegiao"
        Me.txtRegiao.Size = New System.Drawing.Size(41, 20)
        Me.txtRegiao.TabIndex = 166
        '
        'txtCEP
        '
        Me.txtCEP.Location = New System.Drawing.Point(239, 155)
        Me.txtCEP.Margin = New System.Windows.Forms.Padding(2)
        Me.txtCEP.Name = "txtCEP"
        Me.txtCEP.Size = New System.Drawing.Size(67, 20)
        Me.txtCEP.TabIndex = 165
        '
        'txtUF
        '
        Me.txtUF.Location = New System.Drawing.Point(201, 155)
        Me.txtUF.Margin = New System.Windows.Forms.Padding(2)
        Me.txtUF.Name = "txtUF"
        Me.txtUF.Size = New System.Drawing.Size(32, 20)
        Me.txtUF.TabIndex = 164
        '
        'cboBairroDetalhe
        '
        Me.cboBairroDetalhe.FormattingEnabled = True
        Me.cboBairroDetalhe.Location = New System.Drawing.Point(15, 155)
        Me.cboBairroDetalhe.Margin = New System.Windows.Forms.Padding(2)
        Me.cboBairroDetalhe.Name = "cboBairroDetalhe"
        Me.cboBairroDetalhe.Size = New System.Drawing.Size(180, 21)
        Me.cboBairroDetalhe.TabIndex = 163
        '
        'txtDDD
        '
        Me.txtDDD.Location = New System.Drawing.Point(284, 85)
        Me.txtDDD.Margin = New System.Windows.Forms.Padding(2)
        Me.txtDDD.Name = "txtDDD"
        Me.txtDDD.Size = New System.Drawing.Size(43, 20)
        Me.txtDDD.TabIndex = 162
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.Location = New System.Drawing.Point(282, 70)
        Me.Label34.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(40, 16)
        Me.Label34.TabIndex = 161
        Me.Label34.Text = "DDD:"
        '
        'cboCategoriaDetalhe
        '
        Me.cboCategoriaDetalhe.FormattingEnabled = True
        Me.cboCategoriaDetalhe.Location = New System.Drawing.Point(560, 51)
        Me.cboCategoriaDetalhe.Margin = New System.Windows.Forms.Padding(2)
        Me.cboCategoriaDetalhe.Name = "cboCategoriaDetalhe"
        Me.cboCategoriaDetalhe.Size = New System.Drawing.Size(180, 21)
        Me.cboCategoriaDetalhe.TabIndex = 160
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(558, 36)
        Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(69, 16)
        Me.Label9.TabIndex = 159
        Me.Label9.Text = "Categoria:"
        '
        'cboEspecieDetalhe
        '
        Me.cboEspecieDetalhe.FormattingEnabled = True
        Me.cboEspecieDetalhe.Location = New System.Drawing.Point(560, 84)
        Me.cboEspecieDetalhe.Margin = New System.Windows.Forms.Padding(2)
        Me.cboEspecieDetalhe.Name = "cboEspecieDetalhe"
        Me.cboEspecieDetalhe.Size = New System.Drawing.Size(180, 21)
        Me.cboEspecieDetalhe.TabIndex = 158
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(558, 69)
        Me.Label27.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(60, 16)
        Me.Label27.TabIndex = 157
        Me.Label27.Text = "Espécie:"
        '
        'cboOperadorDetalhe
        '
        Me.cboOperadorDetalhe.FormattingEnabled = True
        Me.cboOperadorDetalhe.Location = New System.Drawing.Point(560, 16)
        Me.cboOperadorDetalhe.Margin = New System.Windows.Forms.Padding(2)
        Me.cboOperadorDetalhe.Name = "cboOperadorDetalhe"
        Me.cboOperadorDetalhe.Size = New System.Drawing.Size(180, 21)
        Me.cboOperadorDetalhe.TabIndex = 156
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(558, 2)
        Me.Label10.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(68, 16)
        Me.Label10.TabIndex = 155
        Me.Label10.Text = "Operador:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(440, 138)
        Me.Label11.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(84, 16)
        Me.Label11.TabIndex = 154
        Me.Label11.Text = "Data NC MP:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(353, 138)
        Me.Label12.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(61, 16)
        Me.Label12.TabIndex = 153
        Me.Label12.Text = "Data NC:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(307, 138)
        Me.Label13.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(55, 16)
        Me.Label13.TabIndex = 152
        Me.Label13.Text = "Região:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(236, 138)
        Me.Label15.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(37, 16)
        Me.Label15.TabIndex = 151
        Me.Label15.Text = "CEP:"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(197, 138)
        Me.Label23.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(28, 16)
        Me.Label23.TabIndex = 150
        Me.Label23.Text = "UF:"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(11, 138)
        Me.Label31.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(46, 16)
        Me.Label31.TabIndex = 149
        Me.Label31.Text = "Bairro:"
        '
        'cboCidade
        '
        Me.cboCidade.FormattingEnabled = True
        Me.cboCidade.Location = New System.Drawing.Point(444, 119)
        Me.cboCidade.Margin = New System.Windows.Forms.Padding(2)
        Me.cboCidade.Name = "cboCidade"
        Me.cboCidade.Size = New System.Drawing.Size(110, 21)
        Me.cboCidade.TabIndex = 148
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(442, 104)
        Me.Label32.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(54, 16)
        Me.Label32.TabIndex = 147
        Me.Label32.Text = "Cidade:"
        '
        'txtTelefone2
        '
        Me.txtTelefone2.Location = New System.Drawing.Point(444, 85)
        Me.txtTelefone2.Margin = New System.Windows.Forms.Padding(2)
        Me.txtTelefone2.Name = "txtTelefone2"
        Me.txtTelefone2.Size = New System.Drawing.Size(110, 20)
        Me.txtTelefone2.TabIndex = 146
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(442, 70)
        Me.Label33.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(71, 16)
        Me.Label33.TabIndex = 145
        Me.Label33.Text = "Telefone2:"
        '
        'txtTelefone1
        '
        Me.txtTelefone1.Location = New System.Drawing.Point(331, 85)
        Me.txtTelefone1.Margin = New System.Windows.Forms.Padding(2)
        Me.txtTelefone1.Name = "txtTelefone1"
        Me.txtTelefone1.Size = New System.Drawing.Size(106, 20)
        Me.txtTelefone1.TabIndex = 144
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.Location = New System.Drawing.Point(328, 70)
        Me.Label35.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(71, 16)
        Me.Label35.TabIndex = 143
        Me.Label35.Text = "Telefone1:"
        '
        'txtRg
        '
        Me.txtRg.Location = New System.Drawing.Point(444, 51)
        Me.txtRg.Margin = New System.Windows.Forms.Padding(2)
        Me.txtRg.Name = "txtRg"
        Me.txtRg.Size = New System.Drawing.Size(110, 20)
        Me.txtRg.TabIndex = 142
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.Location = New System.Drawing.Point(442, 36)
        Me.Label36.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(89, 16)
        Me.Label36.TabIndex = 141
        Me.Label36.Text = "Insc. Est./ RG:"
        '
        'txtCnpjCpf
        '
        Me.txtCnpjCpf.Location = New System.Drawing.Point(444, 17)
        Me.txtCnpjCpf.Margin = New System.Windows.Forms.Padding(2)
        Me.txtCnpjCpf.Name = "txtCnpjCpf"
        Me.txtCnpjCpf.Size = New System.Drawing.Size(110, 20)
        Me.txtCnpjCpf.TabIndex = 140
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.Location = New System.Drawing.Point(442, 2)
        Me.Label37.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(75, 16)
        Me.Label37.TabIndex = 139
        Me.Label37.Text = "CNPJ/CPF:"
        '
        'txtEndereco
        '
        Me.txtEndereco.Location = New System.Drawing.Point(14, 119)
        Me.txtEndereco.Margin = New System.Windows.Forms.Padding(2)
        Me.txtEndereco.Name = "txtEndereco"
        Me.txtEndereco.Size = New System.Drawing.Size(423, 20)
        Me.txtEndereco.TabIndex = 138
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.Location = New System.Drawing.Point(11, 104)
        Me.Label38.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(69, 16)
        Me.Label38.TabIndex = 137
        Me.Label38.Text = "Endereço:"
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(14, 85)
        Me.txtEmail.Margin = New System.Windows.Forms.Padding(2)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(265, 20)
        Me.txtEmail.TabIndex = 136
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.Location = New System.Drawing.Point(11, 70)
        Me.Label39.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(48, 16)
        Me.Label39.TabIndex = 135
        Me.Label39.Text = "E-mail:"
        '
        'txtNomeCliente2
        '
        Me.txtNomeCliente2.Location = New System.Drawing.Point(123, 51)
        Me.txtNomeCliente2.Margin = New System.Windows.Forms.Padding(2)
        Me.txtNomeCliente2.Name = "txtNomeCliente2"
        Me.txtNomeCliente2.Size = New System.Drawing.Size(314, 20)
        Me.txtNomeCliente2.TabIndex = 134
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.Location = New System.Drawing.Point(121, 36)
        Me.Label40.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(57, 16)
        Me.Label40.TabIndex = 133
        Me.Label40.Text = "Nome 2:"
        '
        'txtNomeCliente1
        '
        Me.txtNomeCliente1.Location = New System.Drawing.Point(123, 17)
        Me.txtNomeCliente1.Margin = New System.Windows.Forms.Padding(2)
        Me.txtNomeCliente1.Name = "txtNomeCliente1"
        Me.txtNomeCliente1.Size = New System.Drawing.Size(314, 20)
        Me.txtNomeCliente1.TabIndex = 132
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.Location = New System.Drawing.Point(121, 2)
        Me.Label41.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(47, 16)
        Me.Label41.TabIndex = 131
        Me.Label41.Text = "Nome:"
        '
        'txtCodBarra
        '
        Me.txtCodBarra.Location = New System.Drawing.Point(14, 51)
        Me.txtCodBarra.Margin = New System.Windows.Forms.Padding(2)
        Me.txtCodBarra.Name = "txtCodBarra"
        Me.txtCodBarra.Size = New System.Drawing.Size(103, 20)
        Me.txtCodBarra.TabIndex = 130
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Location = New System.Drawing.Point(11, 36)
        Me.Label42.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(74, 16)
        Me.Label42.TabIndex = 129
        Me.Label42.Text = "Cód. Barra:"
        '
        'txtCodigo
        '
        Me.txtCodigo.Enabled = False
        Me.txtCodigo.Location = New System.Drawing.Point(14, 17)
        Me.txtCodigo.Margin = New System.Windows.Forms.Padding(2)
        Me.txtCodigo.Name = "txtCodigo"
        Me.txtCodigo.Size = New System.Drawing.Size(103, 20)
        Me.txtCodigo.TabIndex = 128
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(11, 2)
        Me.Label43.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(54, 16)
        Me.Label43.TabIndex = 127
        Me.Label43.Text = "Código:"
        '
        'tbConcluir
        '
        Me.tbConcluir.Controls.Add(Me.Dados)
        Me.tbConcluir.Location = New System.Drawing.Point(4, 22)
        Me.tbConcluir.Margin = New System.Windows.Forms.Padding(2)
        Me.tbConcluir.Name = "tbConcluir"
        Me.tbConcluir.Padding = New System.Windows.Forms.Padding(2)
        Me.tbConcluir.Size = New System.Drawing.Size(780, 245)
        Me.tbConcluir.TabIndex = 2
        Me.tbConcluir.Text = "Resumo"
        Me.tbConcluir.UseVisualStyleBackColor = True
        '
        'Dados
        '
        Me.Dados.Controls.Add(Me.cbSituacao)
        Me.Dados.Controls.Add(Me.Label14)
        Me.Dados.Controls.Add(Me.btnAlterarContribuicao)
        Me.Dados.Controls.Add(Me.btnCancelarMensal)
        Me.Dados.Controls.Add(Me.btnEnviarCancelar)
        Me.Dados.Controls.Add(Me.btnExcluirContribuir)
        Me.Dados.Controls.Add(Me.rtxtObs)
        Me.Dados.Controls.Add(Me.Label20)
        Me.Dados.Controls.Add(Me.txtCpf)
        Me.Dados.Controls.Add(Me.Label22)
        Me.Dados.Controls.Add(Me.Label21)
        Me.Dados.Controls.Add(Me.txtAutorizante)
        Me.Dados.Controls.Add(Me.cboCategoriaConcluir)
        Me.Dados.Controls.Add(Me.Label28)
        Me.Dados.Controls.Add(Me.cboOperadorConcluir)
        Me.Dados.Controls.Add(Me.Label16)
        Me.Dados.Controls.Add(Me.txtCodEanConcluir)
        Me.Dados.Controls.Add(Me.Label17)
        Me.Dados.Controls.Add(Me.txtNomeConcluir)
        Me.Dados.Controls.Add(Me.Label18)
        Me.Dados.Controls.Add(Me.txtTelefoneConcluir)
        Me.Dados.Controls.Add(Me.Label19)
        Me.Dados.Controls.Add(Me.Label26)
        Me.Dados.Controls.Add(Me.txtValorConcluir)
        Me.Dados.Controls.Add(Me.Label44)
        Me.Dados.Controls.Add(Me.txtDDDConcluir)
        Me.Dados.Location = New System.Drawing.Point(4, 0)
        Me.Dados.Margin = New System.Windows.Forms.Padding(2)
        Me.Dados.Name = "Dados"
        Me.Dados.Padding = New System.Windows.Forms.Padding(2)
        Me.Dados.Size = New System.Drawing.Size(776, 245)
        Me.Dados.TabIndex = 8
        Me.Dados.TabStop = False
        Me.Dados.Text = "Dados"
        '
        'cbSituacao
        '
        Me.cbSituacao.FormattingEnabled = True
        Me.cbSituacao.Items.AddRange(New Object() {"ACEITE", "RECUSA", "CANCELADO"})
        Me.cbSituacao.Location = New System.Drawing.Point(64, 187)
        Me.cbSituacao.Margin = New System.Windows.Forms.Padding(2)
        Me.cbSituacao.Name = "cbSituacao"
        Me.cbSituacao.Size = New System.Drawing.Size(144, 21)
        Me.cbSituacao.TabIndex = 136
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(0, 190)
        Me.Label14.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(63, 16)
        Me.Label14.TabIndex = 137
        Me.Label14.Text = "Situação:"
        '
        'btnAlterarContribuicao
        '
        Me.btnAlterarContribuicao.Location = New System.Drawing.Point(319, 190)
        Me.btnAlterarContribuicao.Margin = New System.Windows.Forms.Padding(2)
        Me.btnAlterarContribuicao.Name = "btnAlterarContribuicao"
        Me.btnAlterarContribuicao.Size = New System.Drawing.Size(93, 41)
        Me.btnAlterarContribuicao.TabIndex = 135
        Me.btnAlterarContribuicao.Text = "Alterar Contribuição"
        Me.btnAlterarContribuicao.UseVisualStyleBackColor = True
        '
        'btnCancelarMensal
        '
        Me.btnCancelarMensal.Location = New System.Drawing.Point(532, 190)
        Me.btnCancelarMensal.Margin = New System.Windows.Forms.Padding(2)
        Me.btnCancelarMensal.Name = "btnCancelarMensal"
        Me.btnCancelarMensal.Size = New System.Drawing.Size(93, 41)
        Me.btnCancelarMensal.TabIndex = 134
        Me.btnCancelarMensal.Text = "Cancelar Mensal"
        Me.btnCancelarMensal.UseVisualStyleBackColor = True
        '
        'btnEnviarCancelar
        '
        Me.btnEnviarCancelar.Location = New System.Drawing.Point(641, 190)
        Me.btnEnviarCancelar.Margin = New System.Windows.Forms.Padding(2)
        Me.btnEnviarCancelar.Name = "btnEnviarCancelar"
        Me.btnEnviarCancelar.Size = New System.Drawing.Size(90, 41)
        Me.btnEnviarCancelar.TabIndex = 133
        Me.btnEnviarCancelar.Text = "Enviar para exclusão"
        Me.btnEnviarCancelar.UseVisualStyleBackColor = True
        '
        'btnExcluirContribuir
        '
        Me.btnExcluirContribuir.Location = New System.Drawing.Point(423, 190)
        Me.btnExcluirContribuir.Margin = New System.Windows.Forms.Padding(2)
        Me.btnExcluirContribuir.Name = "btnExcluirContribuir"
        Me.btnExcluirContribuir.Size = New System.Drawing.Size(93, 41)
        Me.btnExcluirContribuir.TabIndex = 84
        Me.btnExcluirContribuir.Text = "Cancelar Contribuição"
        Me.btnExcluirContribuir.UseVisualStyleBackColor = True
        '
        'rtxtObs
        '
        Me.rtxtObs.Location = New System.Drawing.Point(491, 31)
        Me.rtxtObs.Margin = New System.Windows.Forms.Padding(2)
        Me.rtxtObs.Name = "rtxtObs"
        Me.rtxtObs.Size = New System.Drawing.Size(241, 149)
        Me.rtxtObs.TabIndex = 83
        Me.rtxtObs.Text = ""
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(489, 16)
        Me.Label20.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(91, 16)
        Me.Label20.TabIndex = 82
        Me.Label20.Text = "Observação : "
        '
        'txtCpf
        '
        Me.txtCpf.Location = New System.Drawing.Point(56, 159)
        Me.txtCpf.Margin = New System.Windows.Forms.Padding(2)
        Me.txtCpf.Name = "txtCpf"
        Me.txtCpf.Size = New System.Drawing.Size(152, 20)
        Me.txtCpf.TabIndex = 81
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(19, 162)
        Me.Label22.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(36, 13)
        Me.Label22.TabIndex = 80
        Me.Label22.Text = "CPF : "
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(13, 133)
        Me.Label21.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(42, 13)
        Me.Label21.TabIndex = 79
        Me.Label21.Text = "Titular :"
        '
        'txtAutorizante
        '
        Me.txtAutorizante.Location = New System.Drawing.Point(56, 130)
        Me.txtAutorizante.Margin = New System.Windows.Forms.Padding(2)
        Me.txtAutorizante.Name = "txtAutorizante"
        Me.txtAutorizante.Size = New System.Drawing.Size(365, 20)
        Me.txtAutorizante.TabIndex = 78
        '
        'cboCategoriaConcluir
        '
        Me.cboCategoriaConcluir.FormattingEnabled = True
        Me.cboCategoriaConcluir.Location = New System.Drawing.Point(259, 101)
        Me.cboCategoriaConcluir.Margin = New System.Windows.Forms.Padding(2)
        Me.cboCategoriaConcluir.Name = "cboCategoriaConcluir"
        Me.cboCategoriaConcluir.Size = New System.Drawing.Size(137, 21)
        Me.cboCategoriaConcluir.TabIndex = 5
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.Location = New System.Drawing.Point(185, 104)
        Me.Label28.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(69, 16)
        Me.Label28.TabIndex = 74
        Me.Label28.Text = "Categoria:"
        '
        'cboOperadorConcluir
        '
        Me.cboOperadorConcluir.FormattingEnabled = True
        Me.cboOperadorConcluir.Location = New System.Drawing.Point(241, 15)
        Me.cboOperadorConcluir.Margin = New System.Windows.Forms.Padding(2)
        Me.cboOperadorConcluir.Name = "cboOperadorConcluir"
        Me.cboOperadorConcluir.Size = New System.Drawing.Size(180, 21)
        Me.cboOperadorConcluir.TabIndex = 1
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(178, 16)
        Me.Label16.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(76, 16)
        Me.Label16.TabIndex = 57
        Me.Label16.Text = "Operadora:"
        '
        'txtCodEanConcluir
        '
        Me.txtCodEanConcluir.Location = New System.Drawing.Point(70, 15)
        Me.txtCodEanConcluir.Margin = New System.Windows.Forms.Padding(2)
        Me.txtCodEanConcluir.Mask = "*#####-###*"
        Me.txtCodEanConcluir.Name = "txtCodEanConcluir"
        Me.txtCodEanConcluir.Size = New System.Drawing.Size(107, 20)
        Me.txtCodEanConcluir.TabIndex = 0
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(19, 48)
        Me.Label17.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(38, 13)
        Me.Label17.TabIndex = 10
        Me.Label17.Text = "Nome "
        '
        'txtNomeConcluir
        '
        Me.txtNomeConcluir.Location = New System.Drawing.Point(58, 46)
        Me.txtNomeConcluir.Margin = New System.Windows.Forms.Padding(2)
        Me.txtNomeConcluir.Name = "txtNomeConcluir"
        Me.txtNomeConcluir.Size = New System.Drawing.Size(282, 20)
        Me.txtNomeConcluir.TabIndex = 2
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(104, 78)
        Me.Label18.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(49, 13)
        Me.Label18.TabIndex = 8
        Me.Label18.Text = "Telefone"
        '
        'txtTelefoneConcluir
        '
        Me.txtTelefoneConcluir.Location = New System.Drawing.Point(154, 77)
        Me.txtTelefoneConcluir.Margin = New System.Windows.Forms.Padding(2)
        Me.txtTelefoneConcluir.Name = "txtTelefoneConcluir"
        Me.txtTelefoneConcluir.Size = New System.Drawing.Size(127, 20)
        Me.txtTelefoneConcluir.TabIndex = 4
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(4, 18)
        Me.Label19.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(65, 13)
        Me.Label19.TabIndex = 2
        Me.Label19.Text = "Código EAN"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(23, 106)
        Me.Label26.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(31, 13)
        Me.Label26.TabIndex = 6
        Me.Label26.Text = "Valor"
        '
        'txtValorConcluir
        '
        Me.txtValorConcluir.Location = New System.Drawing.Point(57, 103)
        Me.txtValorConcluir.Margin = New System.Windows.Forms.Padding(2)
        Me.txtValorConcluir.Name = "txtValorConcluir"
        Me.txtValorConcluir.Size = New System.Drawing.Size(72, 20)
        Me.txtValorConcluir.TabIndex = 6
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.Location = New System.Drawing.Point(23, 78)
        Me.Label44.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(31, 13)
        Me.Label44.TabIndex = 4
        Me.Label44.Text = "DDD"
        '
        'txtDDDConcluir
        '
        Me.txtDDDConcluir.Location = New System.Drawing.Point(57, 76)
        Me.txtDDDConcluir.Margin = New System.Windows.Forms.Padding(2)
        Me.txtDDDConcluir.Name = "txtDDDConcluir"
        Me.txtDDDConcluir.Size = New System.Drawing.Size(35, 20)
        Me.txtDDDConcluir.TabIndex = 3
        '
        'lblValor
        '
        Me.lblValor.AutoSize = True
        Me.lblValor.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblValor.Location = New System.Drawing.Point(817, 436)
        Me.lblValor.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblValor.Name = "lblValor"
        Me.lblValor.Size = New System.Drawing.Size(15, 16)
        Me.lblValor.TabIndex = 91
        Me.lblValor.Text = "0"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(817, 405)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(92, 16)
        Me.Label5.TabIndex = 90
        Me.Label5.Text = "Valor Total :"
        '
        'lblTotalFichas
        '
        Me.lblTotalFichas.AutoSize = True
        Me.lblTotalFichas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTotalFichas.Location = New System.Drawing.Point(817, 379)
        Me.lblTotalFichas.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblTotalFichas.Name = "lblTotalFichas"
        Me.lblTotalFichas.Size = New System.Drawing.Size(15, 16)
        Me.lblTotalFichas.TabIndex = 89
        Me.lblTotalFichas.Text = "0"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(814, 357)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(119, 16)
        Me.Label6.TabIndex = 88
        Me.Label6.Text = "Total de Fichas:"
        '
        'lblSemCritica
        '
        Me.lblSemCritica.AutoSize = True
        Me.lblSemCritica.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSemCritica.ForeColor = System.Drawing.Color.LightSeaGreen
        Me.lblSemCritica.Location = New System.Drawing.Point(793, 480)
        Me.lblSemCritica.Name = "lblSemCritica"
        Me.lblSemCritica.Size = New System.Drawing.Size(165, 18)
        Me.lblSemCritica.TabIndex = 92
        Me.lblSemCritica.Text = "Não existem Criticas"
        Me.lblSemCritica.Visible = False
        '
        'FrmConsultaRequisicaoRealizadaCemig
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1113, 612)
        Me.Controls.Add(Me.lblSemCritica)
        Me.Controls.Add(Me.lblValor)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblTotalFichas)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.TabInformacoes)
        Me.Controls.Add(Me.btnSair)
        Me.Controls.Add(Me.grdRemessa)
        Me.Controls.Add(Me.Panel1)
        Me.MinimizeBox = False
        Me.Name = "FrmConsultaRequisicaoRealizadaCemig"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Consulta Requisição Efetuada Cemig"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.grdRemessa, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabInformacoes.ResumeLayout(False)
        Me.tabHistorico.ResumeLayout(False)
        CType(Me.GrdHistorico, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.tbConcluir.ResumeLayout(False)
        Me.Dados.ResumeLayout(False)
        Me.Dados.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnPesquisar As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboPesquisa As System.Windows.Forms.ComboBox
    Friend WithEvents lblTipoPesquisa As System.Windows.Forms.Label
    Friend WithEvents grdRemessa As System.Windows.Forms.DataGridView
    Friend WithEvents btnSair As System.Windows.Forms.Button
    Friend WithEvents dtInicial As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtNome As System.Windows.Forms.TextBox
    Friend WithEvents txtTelefone As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents chkTipoPesquisa As System.Windows.Forms.CheckBox
    Friend WithEvents btnLimpar As System.Windows.Forms.Button
    Friend WithEvents TabInformacoes As System.Windows.Forms.TabControl
    Friend WithEvents tabHistorico As System.Windows.Forms.TabPage
    Friend WithEvents GrdHistorico As System.Windows.Forms.DataGridView
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents btnSalvar As System.Windows.Forms.Button
    Friend WithEvents txtValDoacao As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents txtMelhorDia As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents chkInativo As System.Windows.Forms.CheckBox
    Friend WithEvents chkNaoPedirMensal As System.Windows.Forms.CheckBox
    Friend WithEvents chkNaoPedirExtra As System.Windows.Forms.CheckBox
    Friend WithEvents chkTelDesl As System.Windows.Forms.CheckBox
    Friend WithEvents txtDtCadastro As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents txtObs As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents txtReferencia As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents txtDtNcMp As System.Windows.Forms.TextBox
    Friend WithEvents txtDtNc As System.Windows.Forms.TextBox
    Friend WithEvents txtRegiao As System.Windows.Forms.TextBox
    Friend WithEvents txtCEP As System.Windows.Forms.TextBox
    Friend WithEvents txtUF As System.Windows.Forms.TextBox
    Friend WithEvents cboBairroDetalhe As System.Windows.Forms.ComboBox
    Friend WithEvents txtDDD As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents cboCategoriaDetalhe As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cboEspecieDetalhe As System.Windows.Forms.ComboBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents cboOperadorDetalhe As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents cboCidade As System.Windows.Forms.ComboBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents txtTelefone2 As System.Windows.Forms.TextBox
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents txtTelefone1 As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents txtRg As System.Windows.Forms.TextBox
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents txtCnpjCpf As System.Windows.Forms.TextBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents txtEndereco As System.Windows.Forms.TextBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents txtEmail As System.Windows.Forms.TextBox
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents txtNomeCliente2 As System.Windows.Forms.TextBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents txtNomeCliente1 As System.Windows.Forms.TextBox
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents txtCodBarra As System.Windows.Forms.TextBox
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents txtCodigo As System.Windows.Forms.TextBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents tbConcluir As System.Windows.Forms.TabPage
    Friend WithEvents Dados As System.Windows.Forms.GroupBox
    Friend WithEvents btnEnviarCancelar As System.Windows.Forms.Button
    Friend WithEvents btnExcluirContribuir As System.Windows.Forms.Button
    Friend WithEvents rtxtObs As System.Windows.Forms.RichTextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtCpf As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents txtAutorizante As System.Windows.Forms.TextBox
    Friend WithEvents cboCategoriaConcluir As System.Windows.Forms.ComboBox
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents cboOperadorConcluir As System.Windows.Forms.ComboBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents txtCodEanConcluir As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents txtNomeConcluir As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents txtTelefoneConcluir As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents txtValorConcluir As System.Windows.Forms.TextBox
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents txtDDDConcluir As System.Windows.Forms.TextBox
    Friend WithEvents dtFinal As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents chkPesquisaData As System.Windows.Forms.CheckBox
    Friend WithEvents btnCancelarMensal As System.Windows.Forms.Button
    Friend WithEvents btnAlterarContribuicao As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboAceite As System.Windows.Forms.ComboBox
    Friend WithEvents chkNaoConforme As System.Windows.Forms.CheckBox
    Friend WithEvents lblValor As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblTotalFichas As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents chkVerificaGrau As System.Windows.Forms.CheckBox
    Friend WithEvents lblSemCritica As System.Windows.Forms.Label
    Friend WithEvents txtDDDPesquisa As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cbSituacao As System.Windows.Forms.ComboBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
End Class
