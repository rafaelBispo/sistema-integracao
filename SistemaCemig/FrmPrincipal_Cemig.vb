﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001

Public Class FrmPrincipal_Cemig

#Region "Variaveis"

    Dim v_ambiente As String
    Dim mNivelAcesso As Integer
    Dim mUsuario As String
    Dim mCodUsuario As Integer


    Public Property NivelAcesso() As Integer
        Get
            Return mNivelAcesso
        End Get
        Set(ByVal value As Integer)
            mNivelAcesso = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return mUsuario
        End Get
        Set(ByVal value As String)
            mUsuario = value
        End Set
    End Property

    Public Property CodUsuario() As Integer
        Get
            Return mCodUsuario
        End Get
        Set(ByVal value As Integer)
            mCodUsuario = value
        End Set
    End Property

#End Region

    Private Sub instanciaVariaviesGlobais()

        Dim mensagem As New Util.ConnectionHelper(System.Reflection.Assembly.GetExecutingAssembly)
        Dim v_ambiente As String

        v_ambiente = mensagem.RetornaMenssage()

    End Sub


    Private Sub FrmPrincipal_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim mensagem As New Util.ConnectionHelper(System.Reflection.Assembly.GetExecutingAssembly)
        Dim rn As RNContribuinte = New RNContribuinte

        If NivelAcesso = 6 Then
            btnGerencial.Visible = False
            SuporteToolStripMenuItem.Visible = False
            RelatôriosToolStripMenuItem.Visible = False
            AbrirChamadoToolStripMenuItem.Visible = False
            UsuarioToolStripMenuItem.Visible = False
            GerêncialToolStripMenuItem.Visible = False
            btnGerencial.Visible = False
            btnRequisicoesRealizadas.Visible = False
        ElseIf NivelAcesso = 1 Then
            btnGerencial.Visible = True
            SuporteToolStripMenuItem.Visible = True
            RelatôriosToolStripMenuItem.Visible = True
            UsuarioToolStripMenuItem.Visible = True
            AbrirChamadoToolStripMenuItem.Visible = True
            GerêncialToolStripMenuItem.Visible = True
            btnGerencial.Visible = True
            btnRequisicoesRealizadas.Visible = True
        End If


        stripUsuario.Text = "USUARIO: " + mUsuario


        instanciaVariaviesGlobais()
        CarregaComboCategorias(0)
        ' carregaValoreAtuais()


        '    rn.AtualizaTabelaContribuinte()

    End Sub


    Private Sub btnGerencial_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGerencial.Click
        FrmRequisicaoCemig.Show()
    End Sub

    Private Sub FrmPrincipal_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Application.Exit()
    End Sub

    Private Sub btnAbrirChamado_Click(sender As System.Object, e As System.EventArgs) Handles btnAbrirChamado.Click
        Dim Frm = New FrmAbrirChamadoCemig
        Frm.Usuario = Usuario
        Frm.CodUsuario = CodUsuario
        Frm.Show()
    End Sub

    Private Sub SuporteToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SuporteToolStripMenuItem.Click
        Dim resultado As Boolean
        Dim rn As RNContribuinte = New RNContribuinte
        Try

            MsgBox("O processamento requerido pode ocasionar inconsistência no banco de dados, precisamente na tabela de clientes, deseja continuar?", MsgBoxStyle.YesNo, "Atenção")
            If Windows.Forms.DialogResult.Yes Then
                resultado = rn.AtualizaTabelaCliente()
            End If

            If resultado = True Then

                MsgBox("Base atualizada com sucesso!")

            Else

                MsgBox("Ocorreu um erro na atualização da base, entre em contato com o responsável!")

            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

#Region "Resumo"

    Private Sub CarregaComboCategorias(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCategoria.DataSource = Nothing
            cboCategoria.Items.Clear()
            cboCategoria.DisplayMember = "Nome_Categoria"
            cboCategoria.ValueMember = "Cod_Categoria"
            cboCategoria.DataSource = rn_generico.BuscarCategoriaPorOperador(CodUsuario)

            If cboCategoria.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoria.SelectedIndex = 0
            ElseIf cboCategoria.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoria.Items.Count - 1
                    For Each linha As CGenerico In cboCategoria.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoria.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoria.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""
        Dim tipoPesquisa As Integer = 0

        Try
            Me.Cursor = Cursors.WaitCursor

            carregaGridRequisicoesManualPorPeriodo(dtInicial.Text, dtFinal.Text, 0, cboCategoria.SelectedValue)


            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub carregaGridRequisicoesManualPorPeriodo(ByVal dtInicial As Date, ByVal dtFinal As Date, ByVal tipoAceite As Integer, ByVal categoria As Integer)

        Dim objGenerico As CGenerico = New CGenerico()
        Dim totalValorFichas As Integer
        Dim valor As Single
        Dim strValor As String
        Dim rnGenerico As New RNGenerico
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable

        Try

            ds = rnGenerico.CarregarRequisicaoManualPorPeriodoDataCategoria(dtInicial, dtFinal, tipoAceite, categoria, CodUsuario)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If

            For Each row As DataRow In dt.Rows

                totalValorFichas = row.Item(0)
                lblSolicitacaoAceita.Text = row.Item(1)
                lblSolicitacaoFila.Text = row.Item(2)


            Next

            strValor = valor.ToString("C")
            valor = totalValorFichas
            lblValor.Text = valor.ToString("R$ #,###.00")



        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub btnAtualizar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAtualizar.Click
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""
        Dim tipoPesquisa As Integer = 0

        Try
            Me.Cursor = Cursors.WaitCursor

            carregaValoreAtuais()


            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub carregaValoreAtuais()

        Dim objGenerico As CGenerico = New CGenerico()
        Dim totalValorFichasMensal As Decimal
        Dim totalValorFichasDiario As Decimal
        Dim valor As Single
        Dim strValor As String
        Dim rnGenerico As New RNGenerico
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable

        Try

            'CARREGA OS VALORES MENSAIS

            ds = rnGenerico.CarregarManualValoresAtuaisMes(CodUsuario)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If

            For Each row As DataRow In dt.Rows

                totalValorFichasMensal = row.Item(0)
                lblSolicitacaoAceitaMensal.Text = row.Item(1)
                lblSolicitacaoFilaMensal.Text = row.Item(2)


            Next

            strValor = valor.ToString("C")
            valor = totalValorFichasMensal
            lblValorMensal.Text = valor.ToString("R$ #,###.00")

            ds.Clear()
            dt.Clear()

            'CARREGA OS VALORES DIARIOS

            ds = rnGenerico.CarregarManualValoresAtuaisDia(CodUsuario)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If

            For Each row As DataRow In dt.Rows

                totalValorFichasDiario = row.Item(0)
                lblSolicitacaoAceitaDiario.Text = row.Item(1)
                lblSolicitacaoFilaDiario.Text = row.Item(2)


            Next

            strValor = valor.ToString("C")
            valor = totalValorFichasDiario
            lblValorDiario.Text = valor.ToString("R$ #,###.00")



        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub


#End Region

    Private Sub btnRequisicoesRealizadas_Click(sender As Object, e As EventArgs) Handles btnRequisicoesRealizadas.Click
        Dim frm As FrmConsultaRequisicaoRealizadaCemig = New FrmConsultaRequisicaoRealizadaCemig()
        frm.Show()
    End Sub
End Class
