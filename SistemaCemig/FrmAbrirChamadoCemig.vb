﻿Imports System.Text.RegularExpressions
Imports System.ComponentModel
Imports Util
Imports System.IO
Imports System.Threading.Thread
Imports System.Globalization
Imports SISINT001
Imports System.Text
Imports Microsoft.Office.Interop


Public Class FrmAbrirChamadoCemig
#Region "Enum"

    Private Enum ColunasGridRemessa

        Cod_EAN_CLIENTE = 0
        Nome = 1
        DDD = 2
        Telefone = 3
        Valor = 4
        Observacao = 5
        numeroLinha = 7

    End Enum

    Private Enum ColunasGridHistorico

        Cod_EAN_Cliente = 0
        Nome_contribuinte = 1
        Data_mov = 2
        Tipo_mov = 3
        Tipo_cobranca = 4
        Cod_Categoria = 5
        DDD = 6
        Telefone1 = 7
        Valor = 8
        Cod_recibo = 9
        Cidade = 10
        cod_empresa = 11

    End Enum

    Enum xlsOption
        xlsOpen
    End Enum

#End Region

#Region "variaveis"

    Public telefone As String
    Private lstContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstRetornoContribuintes As New SortableBindingList(Of CContribuinte)
    Private lstContribuinteSelecionada As New List(Of CRetorno)
    Private v_ambiente As String
    Private rnContribuinte As RNContribuinte = New RNContribuinte
    Private rnRetorno As RNRetorno = New RNRetorno
    Private contribuinte As CContribuinte = New CContribuinte
    Private lstContribuintesOi As New SortableBindingList(Of CRetorno)
    Private lstListaTelefonica As New SortableBindingList(Of CListaTelefonica)
    Private lstContribuinteSelecionadaOi As New List(Of CRetorno)
    Private rnRequisicao As RNRequisicao = New RNRequisicao
    Private lstrequisicao As New SortableBindingList(Of CContribuinte)
    Private lstrequisicaoSelecionada As New List(Of CContribuinte)
    Public mNivelAcesso As Integer
    Public mUsuario As String
    Public mCodUsuario As Integer
    Public atualizou As Boolean
    Public bindingSourceContribuite As BindingSource = New BindingSource
    Public PosicaoGrid As Integer
    Public psituacao As String
    Public NextMonth As Date
    Public pMesAnoRef As String
    Public MesAtuacao As String


    Public Property NivelAcesso() As Integer
        Get
            Return mNivelAcesso
        End Get
        Set(ByVal value As Integer)
            mNivelAcesso = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return mUsuario
        End Get
        Set(ByVal value As String)
            mUsuario = value
        End Set
    End Property

    Public Property CodUsuario() As Integer
        Get
            Return mCodUsuario
        End Get
        Set(ByVal value As Integer)
            mCodUsuario = value
        End Set
    End Property

#End Region



#Region "Metodos"



    Private Sub CriarColunasGrid()

        Try

            grdRemessa.DataSource = Nothing
            grdRemessa.Columns.Clear()

            grdRemessa.AutoGenerateColumns = False

            grdRemessa.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            'Dim checkboxColumn As New DataGridViewCheckBoxColumn
            'checkboxColumn.Width = 32
            'checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "cod_ean_cliente"
            column.HeaderText = "Cod. EAN Cliente"
            column.Width = 100
            column.ReadOnly = False
            column.Visible = True
            grdRemessa.Columns.Add(column)

            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "nome_contribuinte"
            column.HeaderText = "Nome Contribuinte"
            column.Width = 300
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DDD"
            column.HeaderText = "DDD"
            column.Width = 30
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Telefone1"
            column.HeaderText = "Telefone"
            column.Width = 80
            column.ReadOnly = True
            grdRemessa.Columns.Add(column)


            '5
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Valor"
            column.HeaderText = "Valor"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            grdRemessa.Columns.Add(column)

            '6
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Observacao"
            column.HeaderText = "Observação"
            column.Width = 160
            column.ReadOnly = False
            column.Visible = True
            grdRemessa.Columns.Add(column)

            '7
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "cod_empresa"
            column.HeaderText = "Empresa"
            column.Width = 1
            column.ReadOnly = True
            column.Visible = True
            grdRemessa.Columns.Add(column)


        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CriarColunasGridHistorico()

        Try

            GrdHistorico.DataSource = Nothing
            GrdHistorico.Columns.Clear()

            GrdHistorico.AutoGenerateColumns = False

            GrdHistorico.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

            ''0

            'Dim checkboxColumn As New DataGridViewCheckBoxColumn
            'checkboxColumn.Width = 32
            'checkboxColumn.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            'grdRemessa.Columns.Add(checkboxColumn)


            Dim column As New DataGridViewTextBoxColumn


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_EAN_Cliente"
            column.HeaderText = "Cód. EAN Cliente"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)


            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Nome_contribuinte"
            column.HeaderText = "Nome contribuinte"
            column.Width = 300
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Data_mov"
            column.HeaderText = "Data Movimentação"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Tipo_mov"
            column.HeaderText = "Tipo Movimentação"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Tipo_cobranca"
            column.HeaderText = "Tipo Cobranca"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_Categoria"
            column.HeaderText = "Cód Categoria"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '1
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "DDD"
            column.HeaderText = "DDD"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '2
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Telefone"
            column.HeaderText = "Telefone"
            column.Width = 80
            column.ReadOnly = True
            GrdHistorico.Columns.Add(column)

            '3
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "valor"
            column.HeaderText = "Valor"
            column.Width = 100
            column.DefaultCellStyle.Format = "0.00"
            column.ReadOnly = True
            column.Visible = True
            GrdHistorico.Columns.Add(column)


            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cod_recibo"
            column.HeaderText = "Cód. Recibo"
            column.Width = 100
            column.ReadOnly = True
            column.Visible = True
            GrdHistorico.Columns.Add(column)

            '4
            column = New DataGridViewTextBoxColumn
            column.DataPropertyName = "Cidade"
            column.HeaderText = "Cidade"
            column.Width = 80
            column.ReadOnly = True
            column.Visible = True
            GrdHistorico.Columns.Add(column)




        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

#End Region

    Private Sub btnPesquisar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPesquisar.Click
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""

        Try

            If cboCategoria.SelectedIndex = 0 Or cboCategoria.Text = "Selecione..." Then
                MsgBox("Opção invalida, selecionar a situação!", MsgBoxStyle.Critical, "Atenção!")
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor


            carregaFeedGridCategoriaContribuinte(cboCategoria.SelectedValue, cboCidadeLote.Text, CodUsuario)


            Me.Cursor = Cursors.Default

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try

    End Sub



    Private Sub carregaFeedGridCategoriaContribuinte(ByVal categoria As String, ByVal cidade As String, ByVal codUsuario As Integer)

        Try
            Dim objRetorno As CRetorno = New CRetorno()
            Dim totalValorFichas As Decimal
            Dim ds As DataSet = New DataSet
            Dim valor As Single
            Dim strValor As String
            Dim numLinhas As Integer
            Dim coluna As DataGridViewColumn = grdRemessa.Columns("id")
            If coluna IsNot Nothing Then
                grdRemessa.Columns.Remove(coluna)
            End If
            grdRemessa.DataSource = Nothing
            lstrequisicao = New SortableBindingList(Of CContribuinte)(rnRequisicao.CarregaFichasCemigOperador(categoria, cidade, codUsuario))
            If (lstrequisicao.Count > 0) Then
                bindingSourceContribuite.DataSource = lstrequisicao
                grdRemessa.DataSource = bindingSourceContribuite.DataSource
                For Each col As DataGridViewRow In grdRemessa.Rows
                    totalValorFichas = totalValorFichas + col.Cells(4).Value
                    If col.Cells(6).Value = 2 Then
                        'coloco cor na linha
                        col.DefaultCellStyle.BackColor = Color.Yellow
                        col.DefaultCellStyle.ForeColor = Color.Black
                    End If
                Next
                grdRemessa.Columns.Add("id", "ID")
                For Each linha In grdRemessa.Rows
                    linha.Cells(7).Value = numLinhas
                    numLinhas = numLinhas + 1
                Next
                grdRemessa.Columns(7).Width = 0
                strValor = valor.ToString("C")
                valor = totalValorFichas
                lblValor.Text = valor.ToString("R$ #,###.00")
                lblTotalFichas.Text = grdRemessa.Rows.Count - 1
            Else
                grdRemessa.DataSource = Nothing
                MsgBox("Não existe registros a serem processados, verifique se houve a liberação das fichas!")
            End If
        Catch ex As Exception
            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub
    'Private Sub btnRetorno_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try

    '        FrmRetorno.Show()

    '    Catch ex As Exception
    '        MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
    '    End Try
    'End Sub

    Private Sub btnSair_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSair.Click
        Close()
    End Sub

    Private Sub FrmConsultaContribuinte_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Me.Width = My.Computer.Screen.WorkingArea.Width
        'Me.Height = My.Computer.Screen.WorkingArea.Height


        CriarColunasGrid()
        CriarColunasGridHistorico()
        CarregaComboCategorias(0)
        CarregaComboCidades(0)
        CarregaComboCidadeLote(0)

        CarregaComboCategoriasConcluir(0)
        CarregaComboOperadorConcluir(0)

        pMesAnoRef = Date.Now
        MesAtuacao = pMesAnoRef
        NextMonth = DateAdd(DateInterval.Month, 1, CDate(pMesAnoRef))
        txtMesRef.Text = NextMonth.Month & "/" & NextMonth.Year

        atualizou = True

    End Sub

#Region "Carrega Combos"

    Private Sub CarregaComboCategorias(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCategoria.DataSource = Nothing
            cboCategoria.Items.Clear()
            cboCategoria.DisplayMember = "Nome_Categoria"
            cboCategoria.ValueMember = "Cod_Categoria"
            cboCategoria.DataSource = rn_generico.BuscarCategoriasPorRequisicaoCemigOperador(CodUsuario)

            If cboCategoria.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoria.SelectedIndex = 0
            ElseIf cboCategoria.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoria.Items.Count - 1
                    For Each linha As CGenerico In cboCategoria.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoria.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoria.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboCategoriasConcluir(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCategoriaConcluir.DataSource = Nothing
            cboCategoriaConcluir.Items.Clear()
            cboCategoriaConcluir.DisplayMember = "Nome_Categoria"
            cboCategoriaConcluir.ValueMember = "Cod_Categoria"
            cboCategoriaConcluir.DataSource = rn_generico.BuscarCategoria(0)

            If cboCategoriaConcluir.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoriaConcluir.SelectedIndex = 0
            ElseIf cboCategoriaConcluir.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoriaConcluir.Items.Count - 1
                    For Each linha As CGenerico In cboCategoriaConcluir.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoriaConcluir.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoriaConcluir.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboCategoriasDetalhe(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCategoriaDetalhe.DataSource = Nothing
            cboCategoriaDetalhe.Items.Clear()
            cboCategoriaDetalhe.DisplayMember = "Nome_Categoria"
            cboCategoriaDetalhe.ValueMember = "Cod_Categoria"
            cboCategoriaDetalhe.DataSource = rn_generico.BuscarCategoriaPorOperador(CodUsuario)

            If cboCategoriaDetalhe.Items.Count > 0 And cod_categoria = 0 Then
                cboCategoriaDetalhe.SelectedIndex = 0
            ElseIf cboCategoriaDetalhe.Items.Count > 0 And cod_categoria <> 0 Then
                For i As Integer = 0 To cboCategoriaDetalhe.Items.Count - 1
                    For Each linha As CGenerico In cboCategoriaDetalhe.DataSource
                        If linha.Cod_Categoria = cod_categoria Then
                            cboCategoriaDetalhe.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboCategoriaDetalhe.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboContribuinte()
        Dim rn_contribuinte As RNContribuinte = New RNContribuinte
        Dim filtro As CContribuinte = New CContribuinte

        cboNome.DataSource = Nothing
        cboNome.Items.Clear()
        cboNome.DisplayMember = "NomeCliente1"
        cboNome.ValueMember = "Cod_cliente"
        cboNome.DataSource = rn_contribuinte.CarregarGrid(filtro, "Contribuinte")

        If cboNome.Items.Count > 0 Then
            cboNome.SelectedIndex = 0
        End If


    End Sub

    Private Sub CarregaComboBairros(ByVal cod_bairro As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try



            cboBairro.DataSource = Nothing
            cboBairro.Items.Clear()
            cboBairro.DisplayMember = "Nome_Bairro"
            cboBairro.ValueMember = "Cod_Bairro"
            cboBairro.DataSource = rn_generico.BuscarBairros(0)

            If cboBairro.Items.Count > 0 And cod_bairro = 0 Then
                cboBairro.SelectedIndex = 0

            ElseIf cboBairro.Items.Count > 0 And cod_bairro <> 0 Then
                For i As Integer = 0 To cboBairro.Items.Count - 1
                    For Each linha As CGenerico In cboBairro.DataSource
                        If linha.Cod_Bairro = cod_bairro Then
                            cboBairro.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboBairro.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboOperador(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboOperador.DataSource = Nothing
            cboOperador.Items.Clear()
            cboOperador.DisplayMember = "Nome_Operador"
            cboOperador.ValueMember = "Cod_Operador"
            cboOperador.DataSource = rn_generico.BuscarOperador(0)

            If cboOperador.Items.Count > 0 And cod_operador = 0 Then
                cboOperador.SelectedIndex = 0
            ElseIf cboOperador.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperador.Items.Count - 1
                    For Each linha As CGenerico In cboOperador.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperador.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperador.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboOperadorConcluir(ByVal cod_operador As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboOperadorConcluir.DataSource = Nothing
            cboOperadorConcluir.Items.Clear()
            cboOperadorConcluir.DisplayMember = "Nome_Operador"
            cboOperadorConcluir.ValueMember = "Cod_Operador"
            cboOperadorConcluir.DataSource = rn_generico.BuscarOperador(CodUsuario)

            If cboOperadorConcluir.Items.Count > 0 And cod_operador = 0 Then
                cboOperadorConcluir.SelectedIndex = 0
            ElseIf cboOperadorConcluir.Items.Count > 0 And cod_operador <> 0 Then
                For i As Integer = 0 To cboOperadorConcluir.Items.Count - 1
                    For Each linha As CGenerico In cboOperadorConcluir.DataSource
                        If linha.Cod_Operador = cod_operador Then
                            cboOperadorConcluir.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboOperadorConcluir.SelectedIndex = 1
            End If

            'seleciona a operadora que está logada
            cboOperadorConcluir.SelectedIndex = 1

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub CarregaComboEspecie(ByVal cod_especie As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try


            cboEspecie.DataSource = Nothing
            cboEspecie.Items.Clear()
            cboEspecie.DisplayMember = "Nome_Especie"
            cboEspecie.ValueMember = "Cod_Especie"
            cboEspecie.DataSource = rn_generico.BuscarEspecie(0)

            If cboEspecie.Items.Count > 0 And cod_especie = 0 Then
                cboEspecie.SelectedIndex = 0
            ElseIf cboEspecie.Items.Count > 0 And cod_especie <> 0 Then
                For i As Integer = 0 To cboEspecie.Items.Count - 1
                    For Each linha As CGenerico In cboEspecie.DataSource
                        If linha.Cod_Especie = cod_especie Then
                            cboEspecie.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next
            Else
                cboEspecie.SelectedIndex = 1
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try



    End Sub

#End Region

    Private Sub FrmConsultaUltimaMovimentacao_Activated(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        Dim rnretorno As New RNRetorno
        Dim situacao As String = ""

        Try
            If atualizou = False Then
                If cboCategoria.SelectedIndex = 0 Or cboCategoria.Text = "Selecione..." Then
                    Exit Sub
                End If

                Me.Cursor = Cursors.WaitCursor

                carregaFeedGridCategoriaContribuinte(cboCategoria.SelectedValue, cboCidadeLote.Text, CodUsuario)


                Me.Cursor = Cursors.Default
                atualizou = True
            End If

        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub



    Private Sub txtNome_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNome.TextChanged
        Dim texto As String = Nothing
        Dim linhaVazia As Integer = 0
        Dim contador As Integer = 0

        If txtNome.Text <> String.Empty Then

            linhaVazia = grdRemessa.Rows.Count - 1

            'percorre cada linha do DataGridView
            For Each linha As DataGridViewRow In grdRemessa.Rows

                'percorre cada célula da linha
                For Each celula As DataGridViewCell In grdRemessa.Rows(linha.Index).Cells
                    If Not contador = linhaVazia Then
                        'se a coluna for a coluna 2 (Nome) então verifica o criterio
                        If celula.ColumnIndex = 2 Then

                            If celula.Value.ToString <> "" Then
                                texto = IIf(celula.Value.ToString = Nothing, "", celula.Value.ToString)
                                'se o texto informado estiver contido na célula então seleciona toda linha
                                If texto.Contains(txtNome.Text) Then
                                    'seleciona a linha
                                    grdRemessa.CurrentCell = celula
                                    grdRemessa.CurrentCell = grdRemessa.Rows(celula.RowIndex).Cells(2)
                                    Exit Sub
                                End If
                            End If
                            contador = contador + 1
                        End If
                    End If

                Next

            Next

        End If

    End Sub

    Private Sub txtTelefone_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtTelefone.TextChanged
        Dim texto As String = Nothing
        Dim linhaVazia As Integer = 0
        Dim contador As Integer = 0

        If txtTelefone.Text <> String.Empty Then

            linhaVazia = grdRemessa.Rows.Count - 1

            'percorre cada linha do DataGridView
            For Each linha As DataGridViewRow In grdRemessa.Rows

                'percorre cada célula da linha
                For Each celula As DataGridViewCell In grdRemessa.Rows(linha.Index).Cells
                    If Not contador = linhaVazia Then
                        'se a coluna for a coluna 1 (telefone) então verifica o criterio
                        If celula.ColumnIndex = 1 Then

                            If celula.Value.ToString <> "" Then
                                texto = IIf(celula.Value.ToString = Nothing, "", celula.Value.ToString)
                                'se o texto informado estiver contido na célula então seleciona toda linha
                                If texto.Contains(txtTelefone.Text) Then
                                    'seleciona a linha
                                    grdRemessa.CurrentCell = celula
                                    grdRemessa.CurrentCell = grdRemessa.Rows(celula.RowIndex).Cells(2)
                                    Exit Sub
                                End If
                            End If
                            contador = contador + 1
                        End If
                    End If

                Next

            Next

        End If
    End Sub

    Private Sub grdRemessa_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdRemessa.CellContentClick

        Dim ClienteClicado As String
        Dim TelefoneSelecionado As String = ""
        Dim valorEspecial As Decimal = 0
        Dim rn As RNRequisicao = New RNRequisicao
        Dim rnContribuinte As RNContribuinte = New RNContribuinte
        Dim dsMarketing As DataSet = New DataSet()

        Try

            Me.Cursor = Cursors.WaitCursor

            ClienteClicado = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.Cod_EAN_CLIENTE).Value, String)

            TelefoneSelecionado = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.Telefone).Value, String)

            valorEspecial = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.Valor).Value, Decimal)

            PosicaoGrid = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.numeroLinha).Value, Integer)



            For Each ContribuinteRequisicao As CContribuinte In lstContribuintes
                If ContribuinteRequisicao.Cod_EAN_Cliente = ClienteClicado Then

                End If
            Next


            If (e.RowIndex > -1) Then

                lstRetornoContribuintes = New SortableBindingList(Of CContribuinte)(rn.CarregaHistoricoContribuinte(ClienteClicado))

                If (lstRetornoContribuintes.Count > 0) Then

                    bindingSourceContribuite.DataSource = lstRetornoContribuintes

                    GrdHistorico.DataSource = bindingSourceContribuite.DataSource

                Else

                    GrdHistorico.DataSource = Nothing

                End If


                contribuinte = rnContribuinte.PesquisarPorCodBarra(ClienteClicado)

                If (contribuinte.Cod_EAN_Cliente = Nothing) Then
                    contribuinte = rnContribuinte.PesquisaClienteTabelaRetido(ClienteClicado)
                End If


                If Not contribuinte Is Nothing Then
                    'popula a tela
                    txtCodigo.Text = contribuinte.CodCliente
                    txtNomeCliente1.Text = contribuinte.NomeCliente1
                    txtNomeCliente2.Text = contribuinte.NomeCliente2
                    txtCodBarra.Text = contribuinte.Cod_EAN_Cliente
                    txtCnpjCpf.Text = contribuinte.CNPJ_CPF
                    txtRg.Text = contribuinte.IE_CI
                    txtEmail.Text = contribuinte.Email
                    txtTelefone1.Text = contribuinte.Telefone1
                    txtTelefone2.Text = contribuinte.Telefone2
                    txtEndereco.Text = contribuinte.Endereco
                    txtUF.Text = contribuinte.UF
                    txtCEP.Text = contribuinte.CEP

                    txtRegiao.Text = contribuinte.Cod_Regiao

                    txtDtNc.Text = contribuinte.DataNascimento
                    txtDtNcMp.Text = contribuinte.DT_NC_MP
                    txtDtCadastro.Text = contribuinte.DataCadastro
                    txtReferencia.Text = contribuinte.Referencia

                    txtObs.Text = contribuinte.Observacao

                    txtMelhorDia.Text = contribuinte.DiaLimite
                    txtValDoacao.Text = Convert.ToDouble(contribuinte.Valor).ToString("R$ #,###.00")
                    txtDDD.Text = contribuinte.DDD

                    CarregaComboCidades(contribuinte.Cod_Cidade)
                    CarregaComboCategoriasDetalhe(contribuinte.Cod_Categoria)
                    CarregaComboOperador(contribuinte.Cod_Operador)
                    CarregaComboBairros(contribuinte.Cod_Bairro)
                    CarregaComboEspecie(contribuinte.Cod_Especie)

                    'CARREGA DADOS DE CONTATO DO TELEMARKETING
                    dsMarketing = rnContribuinte.RetornaDadosMarketing(contribuinte.DDD, contribuinte.Telefone1)

                    If (dsMarketing.Tables(0).Rows.Count > 0) Then
                        If Not IsDBNull(dsMarketing.Tables(0).Rows(0).Item(0)) Then
                            txtEmailContato.Text = dsMarketing.Tables(0).Rows(0).Item(0).ToString()
                        End If
                        If Not IsDBNull(dsMarketing.Tables(0).Rows(0).Item(1)) Then
                            txtWhatsApp.Text = dsMarketing.Tables(0).Rows(0).Item(1).ToString()
                        End If

                    End If


                    If contribuinte.Descontinuado = "S" Then
                        chkInativo.Checked = True
                    Else
                        chkInativo.Checked = False
                    End If

                    If contribuinte.Nao_pedir_extra = "S" Then
                        chkNaoPedirExtra.Checked = True
                    Else
                        chkNaoPedirExtra.Checked = False
                    End If

                    If contribuinte.Nao_Pedir_Mensal = "S" Then
                        chkNaoPedirMensal.Checked = True
                    Else
                        chkNaoPedirMensal.Checked = False
                    End If

                    If contribuinte.Tel_desligado = "S" Then
                        chkTelDesl.Checked = True
                    Else
                        chkTelDesl.Checked = False
                    End If

                    txtNomClienteConcluir.Text = contribuinte.NomeCliente1
                    txtDiaConcluir.Text = contribuinte.DiaLimite
                    txtDDDConcluir.Text = contribuinte.DDD
                    txtTelefoneConcluir.Text = contribuinte.Telefone1
                    If cboCategoria.Text <> "ESPECIAL" Then
                        txtValorConcluir.Text = Convert.ToDouble(contribuinte.Valor).ToString("#,###.00")
                    Else
                        txtValorConcluir.Text = Convert.ToDouble(valorEspecial).ToString("#,###.00")
                    End If

                End If



            End If

            'VERIFICA SE A CATEGORIA FOR ESPECIAL OU MENSAL, HABILITA O CAMPO VALOR
            If contribuinte.Cod_Categoria = 3 Or contribuinte.Cod_Categoria = 4 Or contribuinte.Cod_Categoria = 13 Or contribuinte.Cod_Categoria = 14 Or contribuinte.Cod_Categoria = 15 Or contribuinte.Cod_Categoria = 10 Then

                txtValorConcluir.ReadOnly = False
                txtObsConcluir.ReadOnly = False
                txtDDDConcluir.ReadOnly = False
                txtTelefoneConcluir.ReadOnly = False
                txtNomClienteConcluir.ReadOnly = False

            Else

                txtObsConcluir.ReadOnly = True
                txtDDDConcluir.ReadOnly = True
                txtTelefoneConcluir.ReadOnly = True
                txtNomClienteConcluir.ReadOnly = True
                txtValorConcluir.ReadOnly = True


            End If

            Me.Cursor = Cursors.Default
            'contribuinte = rnContribuinte.PesquisarContribuintePorTelefone(telefone, 1)

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CarregaComboCidades(ByVal cod_cidade As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCidade.DataSource = Nothing
            cboCidade.Items.Clear()
            cboCidade.DisplayMember = "Nome_cidade"
            cboCidade.ValueMember = "Cod_Cidade"
            cboCidade.DataSource = rn_generico.BuscarCidadesCemig(0)

            If cboCidade.Items.Count = 0 Then
                cboCidade.SelectedIndex = 0
            ElseIf cboCidade.Items.Count > 0 And cod_cidade <> 0 Then

                For i As Integer = 0 To cboCidade.Items.Count - 1
                    For Each linha As CGenerico In cboCidade.DataSource
                        If linha.Cod_Cidade = cod_cidade Then
                            cboCidade.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next

            Else

                cboCidade.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CarregaComboCidadeLote(ByVal cod_cidade As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            cboCidadeLote.DataSource = Nothing
            cboCidadeLote.Items.Clear()
            cboCidadeLote.DisplayMember = "Nome_cidade"
            cboCidadeLote.ValueMember = "Cod_Cidade"
            cboCidadeLote.DataSource = rn_generico.BuscarCidadesCemig(0)

            If cboCidadeLote.Items.Count = 0 Then
                cboCidadeLote.SelectedIndex = 0
            ElseIf cboCidadeLote.Items.Count > 0 And cod_cidade <> 0 Then

                For i As Integer = 0 To cboCidadeLote.Items.Count - 1
                    For Each linha As CGenerico In cboCidadeLote.DataSource
                        If linha.Cod_Cidade = cod_cidade Then
                            cboCidadeLote.SelectedIndex = index
                            achou = True
                            Exit For
                        End If
                        index = index + 1
                    Next
                    If achou = True Then
                        Exit For
                    End If
                Next

            Else

                cboCidadeLote.SelectedIndex = 0
            End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub CarregaComboCategoriasInformacoes(ByVal cod_categoria As Integer)
        Dim rn_generico As RNGenerico = New RNGenerico
        Dim index As Integer = 0
        Dim achou As Boolean = False

        Try

            'cboCategoria2.DataSource = Nothing
            'cboCategoria2.Items.Clear()
            'cboCategoria2.DisplayMember = "Nome_Categoria"
            'cboCategoria2.ValueMember = "Cod_Categoria"
            'cboCategoria2.DataSource = rn_generico.BuscarCategoria(0)

            'If cboCategoria2.Items.Count > 0 And cod_categoria = 0 Then
            '    cboCategoria2.SelectedIndex = 0
            'ElseIf cboCategoria2.Items.Count > 0 And cod_categoria <> 0 Then
            '    For i As Integer = 0 To cboCategoria2.Items.Count - 1
            '        For Each linha As CGenerico In cboCategoria2.DataSource
            '            If linha.Cod_Categoria = cod_categoria Then
            '                cboCategoria2.SelectedIndex = index
            '                achou = True
            '                Exit For
            '            End If
            '            index = index + 1
            '        Next
            '        If achou = True Then
            '            Exit For
            '        End If
            '    Next
            'Else
            '    cboCategoria2.SelectedIndex = 1
            'End If

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try


    End Sub

    Private Sub btnSalvar_Click(sender As System.Object, e As System.EventArgs)
        Dim rn As RNGenerico = New RNGenerico
        Dim lista As CListaTelefonica = New CListaTelefonica

        Try

            ''ATRIBUI OS VALORES AO OBJETO
            'lista.Cod_cliente = txtCod_Ean.Text
            'lista.Nome_cliente = txtNome2.Text
            'lista.DDD = txtDDD.Text
            'lista.Telefone = txtTelefone2.Text
            'lista.Valor = txtValor.Text
            'lista.Categoria = cboCategoria.SelectedValue
            'lista.Cod_cidade = cboCidade.SelectedValue


            'If (rn.Atualiza_Lista_Telefonica(lista)) = True Then
            '    MsgBox("Informações da Lista Telefônica foram atualizadas com sucesso!")
            'Else
            '    MsgBox("Ocorreu um erro ao atualizar as informações")
            'End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnProxDia_Click(sender As System.Object, e As System.EventArgs) Handles btnProxDia.Click
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim Daogenerico As GenericoDAO = New GenericoDAO
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable

        Try

            Me.Cursor = Cursors.WaitCursor

            ds = Daogenerico.BuscarOperador(CodUsuario)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If

            contribuinte.Cod_Usuario = CodUsuario
            contribuinte.Cod_opera = dt.Rows(0).Item(0)
            contribuinte.Operador = dt.Rows(0).Item(1)

            If txtObsConcluir.Text = "" Then
                MsgBox("Por favor justifique a ação", MsgBoxStyle.Exclamation)
                txtObsConcluir.Focus()
                Exit Sub
            End If


            If rnRequisicao.AgendaProxDiaCemig(txtObsConcluir.Text, txtCodBarra.Text) = True Then
                MsgBox("Registro salvo com sucesso!", MsgBoxStyle.Information, "Atenção")
            Else
                MsgBox("Ocorreu um erro na inclusão do registro", MsgBoxStyle.Information, "Atenção")
            End If

            carregaFeedGridCategoriaContribuinte(cboCategoria.SelectedValue, cboCidadeLote.Text, CodUsuario)

            LimpaTela()

            'reposiciona a grid para a linha visualizada
            RetornaALinhaSelecionada()

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub

    Private Sub btnContribuir_Click(sender As System.Object, e As System.EventArgs)
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim Daogenerico As GenericoDAO = New GenericoDAO
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable

        Try

            Me.Cursor = Cursors.WaitCursor

            ds = Daogenerico.BuscarOperador(CodUsuario)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If

            contribuinte.Cod_Usuario = CodUsuario
            contribuinte.Cod_opera = dt.Rows(0).Item(0)
            contribuinte.Operador = dt.Rows(0).Item(1)


            'PEGA OS VALORES ATUALIZADOS NA TELA
            contribuinte.Telefone1 = txtTelefoneConcluir.Text
            contribuinte.Nome_contribuinte = txtNomClienteConcluir.Text
            contribuinte.NomeCliente1 = txtNomClienteConcluir.Text
            contribuinte.DDD = txtDDD.Text
            contribuinte.Telefone1 = txtTelefoneConcluir.Text
            contribuinte.Valor = txtValorConcluir.Text
            contribuinte.Observacao = txtObsConcluir.Text

            If rnRequisicao.ConfirmarContribuicao(contribuinte, "S") = True Then
                rnContribuinte.Inclui_Contatos(contribuinte, txtMesRef.Text, txtEmailContato.Text, txtWhatsApp.Text)
                MsgBox("Registro salvo com sucesso!", MsgBoxStyle.Information, "Atenção")
            Else
                MsgBox("Ocorreu um erro na inclusão do registro", MsgBoxStyle.Information, "Atenção")
            End If

            carregaFeedGridCategoriaContribuinte(cboCategoria.SelectedValue, cboCidadeLote.Text, CodUsuario)

            LimpaTela()

            'reposiciona a grid para a linha visualizada
            RetornaALinhaSelecionada()

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try

    End Sub



    Private Sub btnNaoContribuir_Click(sender As System.Object, e As System.EventArgs)

    End Sub

    Private Sub grdRemessa_CellContentDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdRemessa.CellContentDoubleClick

        Dim ClienteClicado As String
        Dim TelefoneSelecionado As String = ""
        Dim valorEspecial As Decimal
        Dim rn As RNRequisicao = New RNRequisicao
        Dim rnContribuinte As RNContribuinte = New RNContribuinte

        Try

            Me.Cursor = Cursors.WaitCursor

            ClienteClicado = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.Cod_EAN_CLIENTE).Value, String)

            TelefoneSelecionado = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.Telefone).Value, String)

            valorEspecial = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.Valor).Value, Decimal)

            PosicaoGrid = CType(grdRemessa.Rows(e.RowIndex).Cells(ColunasGridRemessa.numeroLinha).Value, Integer)

            For Each ContribuinteRequisicao As CContribuinte In lstContribuintes
                If ContribuinteRequisicao.Cod_EAN_Cliente = ClienteClicado Then

                End If
            Next


            If (e.RowIndex > -1) Then

                lstRetornoContribuintes = New SortableBindingList(Of CContribuinte)(rn.CarregaHistoricoContribuinte(ClienteClicado))

                If (lstRetornoContribuintes.Count > 0) Then

                    bindingSourceContribuite.DataSource = lstRetornoContribuintes

                    GrdHistorico.DataSource = bindingSourceContribuite.DataSource

                Else

                    GrdHistorico.DataSource = Nothing

                End If


                contribuinte = rnContribuinte.PesquisarPorCodBarra(ClienteClicado)

                If (contribuinte.Cod_EAN_Cliente = Nothing) Then
                    contribuinte = rnContribuinte.PesquisaClienteTabelaRetido(ClienteClicado)
                End If


                If Not contribuinte Is Nothing Then
                    'popula a tela
                    txtCodigo.Text = contribuinte.CodCliente
                    txtNomeCliente1.Text = contribuinte.NomeCliente1
                    txtNomeCliente2.Text = contribuinte.NomeCliente2
                    txtCodBarra.Text = contribuinte.Cod_EAN_Cliente
                    txtCnpjCpf.Text = contribuinte.CNPJ_CPF
                    txtRg.Text = contribuinte.IE_CI
                    txtEmail.Text = contribuinte.Email
                    txtTelefone1.Text = contribuinte.Telefone1
                    txtTelefone2.Text = contribuinte.Telefone2
                    txtEndereco.Text = contribuinte.Endereco
                    txtUF.Text = contribuinte.UF
                    txtCEP.Text = contribuinte.CEP

                    txtRegiao.Text = contribuinte.Cod_Regiao

                    txtDtNc.Text = contribuinte.DataNascimento
                    txtDtNcMp.Text = contribuinte.DT_NC_MP
                    txtDtCadastro.Text = contribuinte.DataCadastro
                    txtReferencia.Text = contribuinte.Referencia

                    txtObs.Text = contribuinte.Observacao

                    txtMelhorDia.Text = contribuinte.DiaLimite
                    txtValDoacao.Text = Convert.ToDouble(contribuinte.Valor).ToString("R$ #,###.00")
                    txtDDD.Text = contribuinte.DDD

                    CarregaComboCidades(contribuinte.Cod_Cidade)
                    CarregaComboCategoriasDetalhe(contribuinte.Cod_Categoria)
                    CarregaComboOperador(contribuinte.Cod_Operador)
                    CarregaComboBairros(contribuinte.Cod_Bairro)
                    CarregaComboEspecie(contribuinte.Cod_Especie)



                    If contribuinte.Descontinuado = "S" Then
                        chkInativo.Checked = True
                    Else
                        chkInativo.Checked = False
                    End If

                    If contribuinte.Nao_pedir_extra = "S" Then
                        chkNaoPedirExtra.Checked = True
                    Else
                        chkNaoPedirExtra.Checked = False
                    End If

                    If contribuinte.Nao_Pedir_Mensal = "S" Then
                        chkNaoPedirMensal.Checked = True
                    Else
                        chkNaoPedirMensal.Checked = False
                    End If

                    If contribuinte.Tel_desligado = "S" Then
                        chkTelDesl.Checked = True
                    Else
                        chkTelDesl.Checked = False
                    End If

                    txtNomClienteConcluir.Text = contribuinte.NomeCliente1
                    txtDiaConcluir.Text = contribuinte.DiaLimite
                    txtDDDConcluir.Text = contribuinte.DDD
                    txtTelefoneConcluir.Text = contribuinte.Telefone1
                    If cboCategoria.Text <> "ESPECIAL" Then
                        txtValorConcluir.Text = Convert.ToDouble(contribuinte.Valor).ToString("#,###.00")
                    Else
                        txtValorConcluir.Text = Convert.ToDouble(valorEspecial).ToString("#,###.00")
                    End If

                End If



            End If

            'VERIFICA SE A CATEGORIA FOR ESPECIAL OU MENSAL, HABILITA O CAMPO VALOR
            If contribuinte.Cod_Categoria = 3 Or contribuinte.Cod_Categoria = 4 Or contribuinte.Cod_Categoria = 13 Or contribuinte.Cod_Categoria = 14 Or contribuinte.Cod_Categoria = 15 Or contribuinte.Cod_Categoria = 10 Then

                txtValorConcluir.ReadOnly = False
                txtObsConcluir.ReadOnly = False
                txtDDDConcluir.ReadOnly = False
                txtTelefoneConcluir.ReadOnly = False
                txtNomClienteConcluir.ReadOnly = False

            Else

                txtObsConcluir.ReadOnly = False
                txtDDDConcluir.ReadOnly = False
                txtTelefoneConcluir.ReadOnly = False
                txtNomClienteConcluir.ReadOnly = False
                txtValorConcluir.ReadOnly = True

            End If

            Me.Cursor = Cursors.Default
            'contribuinte = rnContribuinte.PesquisarContribuintePorTelefone(telefone, 1)

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub


    Private Sub btnSalvar_Click_1(sender As System.Object, e As System.EventArgs) Handles btnSalvar.Click
        Dim contribuinte As CContribuinte = New CContribuinte

        Try

            contribuinte.CodCliente = txtCodigo.Text
            contribuinte.NomeCliente1 = txtNomeCliente1.Text
            contribuinte.NomeCliente2 = IIf(txtNomeCliente2.Text = "", txtNome.Text, txtNomeCliente2.Text)
            contribuinte.Cod_EAN_Cliente = txtCodBarra.Text
            contribuinte.CNPJ_CPF = IIf(txtCnpjCpf.Text = "", "1", txtCnpjCpf.Text)
            contribuinte.IE_CI = IIf(txtRg.Text = "", "1", txtRg.Text)
            contribuinte.Email = IIf(txtEmail.Text = "", "1", txtEmail.Text)
            contribuinte.Telefone1 = IIf(txtTelefone1.Text = "", "1", txtTelefone1.Text)
            contribuinte.Telefone2 = IIf(txtTelefone2.Text = "", txtTelefone1.Text, txtTelefone2.Text)
            contribuinte.Endereco = IIf(txtEndereco.Text = "", "1", txtEndereco.Text)
            contribuinte.UF = IIf(txtUF.Text = "", "MG", txtUF.Text)
            contribuinte.CEP = IIf(txtCEP.Text = "", "1", txtCEP.Text)

            If cboCidade.SelectedValue <= 0 Then
                contribuinte.Cod_Cidade = 1
            Else
                contribuinte.Cod_Cidade = cboCidade.SelectedValue
            End If

            If cboCidade.Text = "Selecione..." Then
                contribuinte.Cidade = "JUIZ DE FORA"
            Else
                contribuinte.Cidade = cboCidade.Text
            End If

            If cboBairro.SelectedValue <= 0 Then
                contribuinte.Cod_Bairro = 1
            Else
                contribuinte.Cod_Bairro = cboBairro.SelectedValue
            End If

            If cboBairro.Text = "Selecione..." Then
                contribuinte.Bairro = "CENTRO"
            Else
                contribuinte.Bairro = cboBairro.Text
            End If


            If cboOperador.SelectedValue <= 0 Then
                contribuinte.Cod_Operador = 1
            Else
                contribuinte.Cod_Operador = cboOperador.SelectedValue
            End If

            If cboOperador.Text = "Selecione..." Then
                contribuinte.Operador = "INSTITUIÇÃO"
            Else
                contribuinte.Operador = cboOperador.Text
            End If


            If cboOperador.SelectedValue <= 0 Then
                contribuinte.Cod_Usuario = 1
            Else
                contribuinte.Cod_Usuario = cboOperador.SelectedValue
            End If


            If cboCategoria.SelectedValue <= 0 Then
                contribuinte.Cod_Categoria = 3
            Else
                contribuinte.Cod_Categoria = cboCategoria.SelectedValue
            End If

            If cboEspecie.SelectedValue <= 0 Then
                contribuinte.Cod_Especie = 1
            Else
                contribuinte.Cod_Especie = cboEspecie.SelectedValue
            End If


            contribuinte.Cod_empresa = 1

            contribuinte.Cod_Regiao = IIf(txtRegiao.Text = "", 0, txtRegiao.Text)

            contribuinte.DataNascimento = IIf(txtDtNc.Text = "", "01/01/1900", txtDtNc.Text)
            contribuinte.DT_NC_MP = IIf(txtDtNcMp.Text = "", "01/01/1900", txtDtNcMp.Text)
            contribuinte.DataCadastro = IIf(txtDtCadastro.Text = "", "01/01/1900", txtDtCadastro.Text)
            contribuinte.Referencia = txtReferencia.Text

            contribuinte.Observacao = txtObs.Text

            If txtMelhorDia.Text = "" Then
                contribuinte.DiaLimite = 1
            Else
                contribuinte.DiaLimite = txtMelhorDia.Text
            End If

            If txtValDoacao.Text = "" Then
                contribuinte.Valor = 10
            Else
                contribuinte.Valor = txtValDoacao.Text
            End If

            contribuinte.DDD = txtDDD.Text

            If chkTelDesl.Checked = True Then
                contribuinte.Tel_desligado = "S"
            Else
                contribuinte.Tel_desligado = "N"
            End If

            If chkInativo.Checked = True Then
                contribuinte.Descontinuado = "S"
            Else
                contribuinte.Descontinuado = "N"
            End If

            If chkNaoPedirExtra.Checked = True Then
                contribuinte.Nao_pedir_extra = "S"
            Else
                contribuinte.Nao_pedir_extra = "N"
            End If

            If chkNaoPedirMensal.Checked = True Then
                contribuinte.Nao_Pedir_Mensal = "S"
            Else
                contribuinte.Nao_Pedir_Mensal = "N"
            End If

            If rnContribuinte.VerificaExistenciaContribuinte(contribuinte) = True Then
                rnContribuinte.AtualizaContribuinte(contribuinte)
                MsgBox("Dados Salvos com sucesso!")
            Else
                rnContribuinte.Salvar(contribuinte)
                MsgBox("Dados Salvos com sucesso!")
            End If


        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub LimpaTela()

        txtCodBarra.Text = ""
        txtCodigo.Text = ""
        txtNomeCliente1.Text = ""
        txtNomeCliente2.Text = ""
        txtDDD.Text = ""
        txtTelefone.Text = ""
        txtTelefone2.Text = ""
        txtCnpjCpf.Text = ""
        txtRg.Text = ""
        txtMelhorDia.Text = ""
        txtValorConcluir.Text = ""
        txtValDoacao.Text = ""
        txtDtNc.Text = ""
        txtDtNcMp.Text = ""
        txtDiaConcluir.Text = ""
        txtReferencia.Text = ""
        txtRegiao.Text = ""
        txtObs.Text = ""
        txtObsConcluir.Text = ""
        txtCEP.Text = ""
        txtUF.Text = ""
        txtEndereco.Text = ""
        txtNome.Text = ""
        txtTelefoneConcluir.Text = ""
        txtDDDConcluir.Text = ""
        txtValorConcluir.Text = ""

        txtNomClienteConcluir.Text = ""
        txtValorConcluir.Text = ""
        cboCategoriaConcluir.SelectedIndex = 0
        txtAutorizante.Text = ""
        txtObsConcluir.Text = ""
        txtCpf.Text = ""

        cboBairro.SelectedIndex = 0
        cboCategoriaDetalhe.SelectedIndex = 0
        cboCidade.SelectedIndex = 0
        cboEspecie.SelectedIndex = 0
        cboOperador.SelectedIndex = 0

    End Sub



    Private Sub btnNaoAtendeu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNaoAtendeu.Click
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim Daogenerico As GenericoDAO = New GenericoDAO
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable

        Try

            Me.Cursor = Cursors.WaitCursor

            ds = Daogenerico.BuscarOperador(CodUsuario)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If

            contribuinte.Cod_Usuario = CodUsuario
            contribuinte.Cod_opera = dt.Rows(0).Item(0)
            contribuinte.Operador = dt.Rows(0).Item(1)

            If rnRequisicao.CemigRecusaEspecialParaMensal(cboCategoria.SelectedValue, txtCodBarra.Text) = True Then
                MsgBox("Registro salvo com sucesso!", MsgBoxStyle.Information, "Atenção")
            Else
                MsgBox("Ocorreu um erro na inclusão do registro", MsgBoxStyle.Information, "Atenção")
            End If

            carregaFeedGridCategoriaContribuinte(cboCategoria.SelectedValue, cboCidadeLote.Text, CodUsuario)

            LimpaTela()

            'reposiciona a grid para a linha visualizada
            RetornaALinhaSelecionada()

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnEnviarCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEnviarCancelar.Click
        Dim Daogenerico As GenericoDAO = New GenericoDAO
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable
        Try

            Dim result = MessageBox.Show("Deseja enviar o número " & txtTelefoneConcluir.Text & " para cancelamento permanente?, Deseja continuar?", "Atenção",
                           MessageBoxButtons.YesNo,
                           MessageBoxIcon.Question)

            Me.Cursor = Cursors.WaitCursor


            If (result = DialogResult.Yes) Then
                ds = Daogenerico.BuscarOperador(CodUsuario)

                If (ds.Tables(0).Rows.Count > 0) Then
                    If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                        dt = ds.Tables(0)
                    End If
                End If

                contribuinte.Cod_Usuario = CodUsuario
                contribuinte.Cod_opera = dt.Rows(0).Item(0)
                contribuinte.Operador = dt.Rows(0).Item(1)


                'PEGA OS VALORES ATUALIZADOS NA TELA
                contribuinte.Telefone1 = txtTelefoneConcluir.Text
                contribuinte.Nome_contribuinte = txtNomClienteConcluir.Text
                contribuinte.NomeCliente1 = txtNomClienteConcluir.Text
                contribuinte.DDD = txtDDD.Text
                contribuinte.Telefone1 = txtTelefoneConcluir.Text
                contribuinte.Valor = txtValorConcluir.Text
                contribuinte.Observacao = txtObsConcluir.Text



                rnContribuinte.EnviaContribuinteParaCancelamento(contribuinte, CodUsuario)

            End If

            'reposiciona a grid para a linha visualizada
            RetornaALinhaSelecionada()

            Me.Cursor = Cursors.Default


        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub btnTelDesligado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTelDesligado.Click
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim Daogenerico As GenericoDAO = New GenericoDAO
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable

        Try

            Me.Cursor = Cursors.WaitCursor

            ds = Daogenerico.BuscarOperador(CodUsuario)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If

            contribuinte.Cod_Usuario = CodUsuario
            contribuinte.Cod_opera = dt.Rows(0).Item(0)
            contribuinte.Operador = dt.Rows(0).Item(1)


            Dim result = MessageBox.Show("Ao concluir essa ação, o mesmo ira ficar indisponivel na requisição, deseja continuar?", "Atenção",
                          MessageBoxButtons.YesNo,
                          MessageBoxIcon.Question)

            If (result = DialogResult.No) Then
                Exit Sub
            End If

            If rnRequisicao.TelefoneDesligadoCemig(txtTelefoneConcluir.Text, txtCodBarra.Text, txtDDDConcluir.Text) = True Then
                MsgBox("Registro salvo com sucesso!", MsgBoxStyle.Information, "Atenção")
            Else
                MsgBox("Ocorreu um erro na tarefa!", MsgBoxStyle.Information, "Atenção")
            End If

            carregaFeedGridCategoriaContribuinte(cboCategoria.SelectedValue, cboCidadeLote.Text, CodUsuario)

            LimpaTela()

            'reposiciona a grid para a linha visualizada
            RetornaALinhaSelecionada()

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub RetornaALinhaSelecionada()
        Dim texto As String = Nothing
        Dim linhaVazia As Integer = 0
        Dim contador As Integer = 0

        If PosicaoGrid > -1 Then

            linhaVazia = grdRemessa.Rows.Count - 1

            'percorre cada linha do DataGridView
            For Each linha As DataGridViewRow In grdRemessa.Rows

                'percorre cada célula da linha
                For Each celula As DataGridViewCell In grdRemessa.Rows(linha.Index).Cells
                    If Not contador = linhaVazia Then
                        'se a coluna for a coluna 1 (telefone) então verifica o criterio
                        If celula.ColumnIndex = 7 Then

                            If celula.Value.ToString <> "" Then
                                texto = IIf(celula.Value.ToString = Nothing, "", celula.Value.ToString)
                                'se o texto informado estiver contido na célula então seleciona toda linha
                                If texto.Contains(PosicaoGrid) Then
                                    'seleciona a linha
                                    grdRemessa.CurrentCell = celula
                                    grdRemessa.CurrentCell = grdRemessa.Rows(celula.RowIndex).Cells(2)
                                    Exit Sub
                                End If
                            End If
                            contador = contador + 1
                        End If
                    End If

                Next

            Next

        End If

    End Sub

    Private Sub btnOutraOperadora_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim rnRequisicao As RNRequisicao = New RNRequisicao
        Dim Daogenerico As GenericoDAO = New GenericoDAO
        Dim ds As DataSet = New DataSet
        Dim dt As DataTable = New DataTable

        Try

            Me.Cursor = Cursors.WaitCursor

            ds = Daogenerico.BuscarOperador(CodUsuario)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If

            contribuinte.Cod_Usuario = CodUsuario
            contribuinte.Cod_opera = dt.Rows(0).Item(0)
            contribuinte.Operador = dt.Rows(0).Item(1)


            Dim pergunta = MessageBox.Show("Confirma que o telefone com DDD " + txtDDDConcluir.Text + " e telefone " + txtTelefoneConcluir.Text + ",não é da operadora Oi? Ao confirmar o registro será excluido da requisição", "Atenção",
                      MessageBoxButtons.YesNo,
                      MessageBoxIcon.Question)

            If (pergunta = DialogResult.No) Then
                Exit Sub
            End If

            If rnRequisicao.UpdateOutraOperadora(txtDDDConcluir.Text, txtTelefoneConcluir.Text, txtCodBarra.Text) = True Then
                MsgBox("Registro salvo com sucesso!", MsgBoxStyle.Information, "Atenção")
            Else
                MsgBox("Ocorreu um erro na tarefa!", MsgBoxStyle.Information, "Atenção")
            End If

            carregaFeedGridCategoriaContribuinte(cboCategoria.SelectedValue, cboCidadeLote.Text, CodUsuario)

            LimpaTela()

            'reposiciona a grid para a linha visualizada
            RetornaALinhaSelecionada()

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

    Private Sub btnIncluirCemig_Click(sender As Object, e As EventArgs) Handles btnIncluirCemig.Click
        Dim contribuinte As New CContribuinte
        Dim rnContribuinte As New RNContribuinte
        Dim Analisecontribuinte As New CContribuinte

        Try
            If cboOperadorConcluir.SelectedIndex = 0 Then
                MsgBox("Por favor selecione um operador")
                Exit Sub
            End If

            If cboCategoriaConcluir.SelectedIndex = 0 Then
                MsgBox("Por favor selecione uma categoria")
                Exit Sub
            End If

            If txtDDDConcluir.Text = "" Then
                MsgBox("Por favor digite o DDD do contribuinte")
                Exit Sub
            End If

            If txtTelefoneConcluir.Text = "" Then
                MsgBox("Por favor digite o telefone do contribuinte")
                Exit Sub
            End If

            If txtValorConcluir.Text = "" Then
                MsgBox("Por favor digite o valor da contribuição")
                Exit Sub
            End If

            If LTrim(RTrim(txtDDD.Text.Length)) > 8 Then
                MsgBox("Por favor verifique o DDD informado, está fora do padrão")
                Exit Sub
            End If

            If txtNumInstalacao.Text = "" Then
                MsgBox("Por favor digite o Número de instalação")
                Exit Sub
            End If

            Dim result = MessageBox.Show("Deseja realmente incluir o aceite na CEMIG no valor de R$" & txtValorConcluir.Text & " para o contribuinte?. Deseja continuar?", "Atenção",
                              MessageBoxButtons.YesNo,
                              MessageBoxIcon.Question)

            If (result = DialogResult.No) Then
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor

            contribuinte.Telefone1 = txtTelefoneConcluir.Text
            contribuinte.DDD = txtDDDConcluir.Text
            contribuinte.Valor = txtValorConcluir.Text
            contribuinte.Cod_EAN_Cliente = txtCodBarra.Text
            contribuinte.Cod_Operador = cboOperadorConcluir.SelectedValue
            contribuinte.Operador = cboOperadorConcluir.Text
            contribuinte.Cod_Categoria = cboCategoriaConcluir.SelectedValue
            If IsNothing(contribuinte.NomeCliente1) Then
                contribuinte.NomeCliente1 = txtNomClienteConcluir.Text
            End If

            If chkNaoConforme.Checked = True Then
                contribuinte.Nao_Conforme = 1
            Else
                contribuinte.Nao_Conforme = 0
            End If

            'sempre vai pegar como aceite
            psituacao = "S"

            If rnContribuinte.Inclui_Numeros_Cemig(contribuinte, psituacao, "InCemig", txtMesRef.Text, txtNomClienteConcluir.Text, txtNumInstalacao.Text) = True Then
                rnContribuinte.Atualiza_Informacoes_Cemig(contribuinte, MesAtuacao, txtMesRef.Text, txtCpf.Text, txtAutorizante.Text, txtObsConcluir.Text, contribuinte.Valor)
                MsgBox("Registro salvo com sucesso!", MsgBoxStyle.Information, "Atenção")
                LimpaTela()
            Else
                MsgBox("Ocorreu um erro na inclusão do registro", MsgBoxStyle.Information, "Atenção")
            End If

            rnContribuinte.Inclui_Contatos(contribuinte, txtMesRef.Text, txtEmailContato.Text, txtWhatsApp.Text)

            LimpaTela()

            carregaFeedGridCategoriaContribuinte(cboCategoria.SelectedValue, cboCidadeLote.Text, CodUsuario)

            'reposiciona a grid para a linha visualizada
            RetornaALinhaSelecionada()

            Me.Cursor = Cursors.Default


        Catch ex As Exception

            ' Show the exception's message.
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)

        End Try
    End Sub

    Private Sub btnNaoContribuir_Click_1(sender As Object, e As EventArgs) Handles btnNaoContribuir.Click
        Try
            Dim rnRequisicao As RNRequisicao = New RNRequisicao
            Dim Daogenerico As GenericoDAO = New GenericoDAO
            Dim ds As DataSet = New DataSet
            Dim dt As DataTable = New DataTable


            Me.Cursor = Cursors.WaitCursor

            ds = Daogenerico.BuscarOperador(CodUsuario)

            If (ds.Tables(0).Rows.Count > 0) Then
                If ds.Tables(0).Rows(0).Table.Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If
            End If

            contribuinte.Cod_Usuario = CodUsuario
            contribuinte.Cod_opera = dt.Rows(0).Item(0)
            contribuinte.Operador = dt.Rows(0).Item(1)

            If rnRequisicao.ConfirmarContribuicaoCemig(contribuinte, "N") = True Then
                'SE A CATEGORIA FOR ESPECIAL VIRA MENSAL
                If cboCategoria.SelectedValue = 4 Then
                    rnRequisicao.RecusaEspecialParaMensalCemig(cboCategoria.SelectedValue, contribuinte.Cod_EAN_Cliente)
                End If
                MsgBox("Registro salvo com sucesso!", MsgBoxStyle.Information, "Atenção")
            Else
                MsgBox("Ocorreu um erro na inclusão do registro", MsgBoxStyle.Information, "Atenção")
            End If

            carregaFeedGridCategoriaContribuinte(cboCategoria.SelectedValue, cboCidadeLote.Text, CodUsuario)

            LimpaTela()

            'reposiciona a grid para a linha visualizada
            RetornaALinhaSelecionada()

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            MsgBox("Erro não identificado. Mensagem original:" & vbNewLine + ex.Message + "Linha do erro: " & vbNewLine + Err.Description + " - " + ex.ToString)
        End Try
    End Sub

End Class